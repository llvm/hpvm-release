# The HPVM Compiler Infrastcuture

[![Documentation Status](https://readthedocs.org/projects/hpvm/badge/?version=latest)](https://hpvm.readthedocs.io/en/latest/?badge=latest)
[![pipeline status](https://gitlab.engr.illinois.edu/llvm/hpvm-release/badges/main/pipeline.svg)](https://gitlab.engr.illinois.edu/llvm/hpvm-release/-/commits/main)

This repository contains the source code and documentation for the HPVM Compiler Infrastructure.

HPVM is a compiler for heterogeneous parallel system.
For more about what HPVM is, see [our website](https://publish.illinois.edu/hpvm-project/)
and publications:
[PPoPP'18 paper](https://dl.acm.org/doi/pdf/10.1145/3200691.3178493),
[OOPSLA'19 paper](https://dl.acm.org/doi/10.1145/3360612),
[PPoPP'21 paper](https://dl.acm.org/doi/10.1145/3437801.3446108).

HPVM is currently at **version 2.0**.
## Getting Started with HPVM

The HPVM compiler infrastructure builds on the [LLVM Compiler](https://llvm.org).

This quick start guide will help you get started with obtaining and building the HPVM source code.

For more detailed instructions on how to use HPVM and information on the different components, 
please refer to our [online documentation](https://hpvm.readthedocs.io/en/latest/).

### Checkout HPVM

The HPVM source code can be checked out by cloning our repository as follows:
```shell
git clone --recursive -b main https://gitlab.engr.illinois.edu/llvm/hpvm-release.git
cd hpvm-release
```

### Python Environment

It is strongly recommended to use some Python virtual environment, as HPVM will install a few Python packages during 
this installation process. These packages are only required for Tensor-domain extensions ApproxHPVM and ApproxTuner.

If you use Anaconda for package management, we provide a conda environment file that covers all Python and package 
requirements (hpvm/env.yaml can be found in the repository):

```shell
conda env create -n hpvm -f hpvm/env.yaml
```

This creates the conda environment hpvm. If you use this method, remember to activate the environment each time you enter a bash shell:
```shell
conda activate hpvm
```

### Run the install script

HPVM provides an install script that can be used to download the appropriate version of LLVM that
is required for the current HPVM version, and build HPVM. This can be done as follows:
```
cd hpvm
./install.sh [options]
```
some common options to the install script:
- `-h` for usage information.
- `--ninja` to use the Ninja build system.
- `-j<N>` to specify number of threads while building.
- `D<string>` to pass in CMAKE options. e.g. `DCMAKE_BUILD_TYPE=Release` will pass the `-DCMAKE_BUILD_TYPE=Release` option to cmake.  

If no options are provided, the installer will use an interactive mode.

Please visit our [Building HPVM](https://hpvm.readthedocs.io/en/latest/build-hpvm.html) page in the online documentation for further information.

## Support

All questions can be directed to [hpvm-dev@lists.cs.illinois.edu](mailto:hpvm-dev@lists.cs.illinois.edu).
