#!/bin/bash
# Run installer script and pass on args to installer that can parse them
"${BASH_SOURCE%/*}/scripts/hpvm_installer.py" "$@"
ret_code=$?
echo "Installer returned with code $ret_code"

## Check if HyperMapper is installed, if not install it.
if [ $ret_code -ne 0 ]
then
  read -p "HPVM was not installed, proceed with installing HyperMapper? [Y,n]: " INSTALL_HM
  INSTALL_HM=${INSTALL_HM:-y}
  if [[ $INSTALL_HM == "y" || $INSTALL_HM == "Y" ]]; then
    #install hypermapper
    echo "###################################################"
    echo Installing HyperMapper 
    echo "---------------------------------------------------"
    if ! command -v hypermapper &> /dev/null
    then
      pip3 install hypermapper
    else
      echo HyperMapper already exists.
    fi
    echo "###################################################"
    echo
    echo
  else
    echo "###################################################"
    echo Not Installing HyperMapper 
    echo "###################################################"
  fi
else
    #install hypermapper
    echo "###################################################"
    echo Installing HyperMapper 
    echo "---------------------------------------------------"
    if ! command -v hypermapper &> /dev/null
    then
      pip3 install hypermapper
    else
      echo HyperMapper already exists.
    fi
    echo "###################################################"
    echo
    echo
fi
exit $ret_code
