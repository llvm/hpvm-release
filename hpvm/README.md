## HPVM Repository Structure

The current directory consists of the following sub-directories and files:

 - `benchmarks`  : This directory contains the benchmarks for the HPVM compiler infrastructure.
 - `cmake`: Contains `cmake` related files which define additional variables needed to build `HPVM`.
 - `docs`:  Contains the documentation for `HPVM` and its various sub-projects. We use `Sphinx` for generating the reference documentation. A deployed version of the documentation can be found at https://hpvm.readthedocs.io/en/latest/.
 - `include`: This directory contains the C++ headers for the `HPVM`  compiler infrastructure.
 - `lib`: This directory contains the C++ source files for the `HPVM` compiler infrastructure.
 - `llvm_patches`: This directory contains the modified `LLVM` source files as well as a script to patch the corresponding source files in the `LLVM` source tree to enable building `HPVM`.
 - `projects`: This directory contains the various sub-projects within `HPVM` which make use of the core `HPVM` infrastructure.
 - `scripts`: Contains the script to build and install `HPVM` , and other utilities.
 - `test`: Contains the automated `llvm-lit` tests for the various transformation passes in `HPVM`.
 - `tools`: This directory contains the definition of various user-facing tools in `HPVM`.
 - `env.yaml`: Defines the `Python` dependencies needed to support the `HPVM` Tensor Extensions.
 - `install.sh`: Entry point script to build and install `HPVM`.
 - `set_paths.sh`: Defines the various environment variables needed to build `HPVM` as well as it's sub-projects.
 - `setup_environment.sh`: The script sets up the environment to use the `HPVM2FPGA` tool on Intel VLAB.
