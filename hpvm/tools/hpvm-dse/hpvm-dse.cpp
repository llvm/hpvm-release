#include <cstdint>
#include <experimental/filesystem>
#include <fcntl.h>
#include <fstream>
#include <future>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>
#include <omp.h>
#include <poll.h>
#include <stdlib.h>
#include <string>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <unordered_map>
#include <unordered_set>

#include "llvm/IRReader/IRReader.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/Transforms/Scalar/IndVarSimplify.h"
#include "llvm/Transforms/Scalar/InstSimplifyPass.h"
#include "llvm/Transforms/Scalar/MemCpyOptimizer.h"
#include "llvm/Transforms/Scalar/NewGVN.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Scalar/LoopSimplifyCFG.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#define DEBUG_TYPE "hpvm-dse"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/ValueMap.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"

#include "IRCodeGen/BuildDFG.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "CoreHPVM/DFGUtils.h"

#include "IRCodeGen/ClearDFG.h"
#include "Transforms/DFGTransforms/Flatten.h"
#include "Transforms/DFGTransforms/Fuse.h"
#include "Transforms/DFGTransforms/SequentializeAndFlatten.h"
#include "Transforms/DFGTransforms/SequentializeLeaf.h"
#include "Transforms/DFGTransforms/Tile.h"
#include "IRCodeGen/GenHPVM.h"
#include "SupportHPVM/LeafFinder.h"
#include "Transforms/Utils/HPVMBufferingPrivatization.h"
#include "Transforms/Utils/HPVMLoopTransforms.h"

#include "CBackend.h"
#include "hpvm-dse.h"
#include "json/json.hpp"

#define DEBUG_TOOL(X)                                                          \
  if (DebugTool == true) {                                                     \
    X;                                                                         \
  }

#ifndef LLVM_BUILD_DIR
#error LLVM_BUILD_DIR is not defined
#endif

#define STR_VALUE(X) #X
#define STRINGIFY(X) STR_VALUE(X)
#define LLVM_BUILD_DIR_STR STRINGIFY(LLVM_BUILD_DIR)

#define _MIN(X, Y) X < Y ? X : Y

using namespace llvm;
using namespace builddfg;
using namespace hpvm;
using namespace hpvmUtils;

using json = nlohmann::json;
namespace fs = std::experimental::filesystem;

#define SAMPLE_ITERATION_COUNT 1000

// (Arg, buffer size)
using BufferMapTy = std::map<DFNode *, std::map<unsigned, unsigned>>;
using UnrollMapTy =
    std::map<Function *, std::vector<std::pair<BasicBlock *, int>>>;

thread_local std::map<DFNode *, std::vector<Function *>> NodeToCalledF;
thread_local std::map<DFNode *, DFNode *> DFGMap;
thread_local std::map<DFNode *, unsigned> TileLvlMap;
thread_local std::map<DFNode *, std::map<unsigned, unsigned>> BlockSizeMap;
thread_local BufferMapTy BufferInMap;
thread_local BufferMapTy PrivMap;
thread_local std::map<DFNode *, std::vector<UnrollDimInfo>> UnrollDimMap;
thread_local UnrollMapTy UnrollLoopMap;
thread_local std::unordered_set<DFNode *> LoopFusionSet;
thread_local std::map<Function *, std::map<BasicBlock *, int>> TripCountMap;

int isReadOnly(Value *Arg, Module *);

struct ParamMaps {
  std::vector<DFLeafNode *> Members; // The leaves this applies to
  std::map<DFLeafNode *, std::vector<Function *>> NodeToCalledF;
  std::map<DFLeafNode *, unsigned> TileLvlMap;
  std::map<DFLeafNode *, std::map<unsigned, unsigned>> BlockSizeMap;
  std::map<DFLeafNode *, std::map<unsigned, unsigned>> BufferInMap;
  std::map<DFLeafNode *, std::map<unsigned, unsigned>> PrivMap;
  std::map<DFLeafNode *, std::vector<UnrollDimInfo>> UnrollDimMap;
  UnrollMapTy UnrollLoopMap;
  std::unordered_set<DFLeafNode *> LoopFusionSet;
  std::map<Function *, std::map<BasicBlock *, int>> TripCountMap;
};

// Value type must be default constructible
template <typename MapT>
static const typename MapT::mapped_type
LookupMap(const MapT &Map, const typename MapT::key_type &Key) {
  static_assert(std::is_default_constructible<typename MapT::mapped_type>(),
                "Needs default constructible value type");
  if (Map.count(Key) > 0)
    return Map.at(Key);
  else
    return typename MapT::mapped_type();
}

void dumpMap(std::map<BasicBlock *, int> &M, std::ostream &os) {
  os << "Dumping map:" << std::endl;
  os.flush();
  for (auto E : M) {
    BasicBlock *BB = E.first;
    int TC = E.second;
    os << "\t" << BB->getName().str() << " --> " << TC << std::endl;
    os.flush();
  }
}

template <typename Ty> void dumpVector(std::vector<Ty> TVec, std::ostream &os) {
  os << "Dumping vector: " << std::endl;
  os.flush();
  for (auto E : TVec) {
    os << *E << std::endl;
    os.flush();
  }
}

// Takes a line from the output CSV and looks up the sample in the
// AllSamples global. Returns -1 if not found
static int findSample(const std::string &PointLine, int NumParams,
                      std::map<std::string, int> &AllSamples) {
  std::istringstream Iss(PointLine);
  std::string ParamStr;
  std::string Sample;
  int param = 0;
  while (getline(Iss, ParamStr, ',')) {
    Sample += ParamStr + ',';
    if (++param == NumParams)
      break;
  }
  return AllSamples.find(Sample) != AllSamples.end() ? AllSamples.at(Sample)
                                                     : -1;
}

// Command Line Options
// Option to select which opts to include in DSE
enum Opts {
  bufferin = 0,
  argpriv,
  lunroll,
  lfusion,
  tiling,
  nfusion,
  tlp,
};

enum Targets { fpga = 0, gpu };

static hpvm::Target TargetToHPVMTarget(int t) {
  switch (t) {
  case fpga:
    return hpvm::FPGA_TARGET;
  case gpu:
    return hpvm::GPU_TARGET;
  default:
    hpvmUtils::fatalError("Unsupported device target");
  }
}

static bool shouldSequentializeTarget(int t) { return t == fpga; }

enum DOESampling { rnd = 0, slh, klh };

cl::OptionCategory HPVMHypermapperCat("HPVM-Hypermapper Options");

// Optimization List
// List of optimizations that need to be run on the bitcode file before code-gen
static cl::list<Opts>
    OptimizationList(cl::desc("Optimizations to enable for DSE.\n  If none "
                              "provided, all will be enabled:"),
                     cl::values(clEnumVal(bufferin, "Input buffering."),
                                clEnumVal(argpriv, "Argument privatization."),
                                clEnumVal(lunroll, "Loop unroll."),
                                clEnumVal(lfusion, "Loop fusion."),
                                clEnumVal(nfusion, "Node fusion."),
                                clEnumVal(tiling, "Node tiling."),
                                clEnumVal(tlp, "Concurrent node execution.")),
                     cl::cat(HPVMHypermapperCat));

// Selecting the type of sampling for DOE phase
static cl::opt<DOESampling>
    SamplingType(cl::desc("The type of sampling for HyperMapper to use in "
                          "DOE:"),
                 cl::values(clEnumVal(rnd, "Random Sampling (Default)."),
                            clEnumVal(slh, "Standard Lattin Hypercube."),
                            clEnumVal(klh, "K Latin Hypercube.")),
                 cl::cat(HPVMHypermapperCat), cl::init(rnd));

// # of CPUs for Hypermapper
static cl::opt<int> NumHMCPUs("num-hm-cpus",
                              cl::desc("How many CPUs Hypermapper may use"),
                              cl::value_desc("num cpus"), cl::init(0),
                              cl::cat(HPVMHypermapperCat));

// InputFilename - The filename to read from. Required.
static cl::opt<std::string> InputFilename(cl::Positional,
                                          cl::desc("<input bitcode file>"),
                                          cl::Required,
                                          cl::value_desc("filename"));

// -o OutputFolderName - The folder to generate output files in. Defaults to
// outdata.
static cl::opt<std::string>
    OutputFoldername("o", cl::desc("Specify output folder name"),
                     cl::value_desc("folder"), cl::init("outdata"),
                     cl::cat(HPVMHypermapperCat));

// -dse-iter - Set number of DSE iterations, default 20
static cl::opt<int>
    NumDSEIterations("dse-iter",
                     cl::desc("Number of DSE iterations;\nDefaults to 20"),
                     cl::value_desc("num iterations"), cl::init(20),
                     cl::cat(HPVMHypermapperCat));

// -dse-multiplier - Set the multiplier for number of samples in DOE phase.
static cl::opt<int> DOESampleMultiplier(
    "doe-multiplier",
    cl::desc("Multiplier (N) for DOE Samples.\nNum DoE Samples =\n\t\tN x "
             "\#Pramas if N > 1\n\t\t\#Pramas + 1 if N = 1.\nDefaults to 1."),
    cl::value_desc("N"), cl::init(1), cl::cat(HPVMHypermapperCat));

// -t - Set target device, default FPGA
static cl::opt<Targets> Target(cl::desc("The target device for DSE:"),
                               cl::values(clEnumVal(fpga, "FPGA (Default)."),
                                          clEnumVal(gpu, "GPU")),
                               cl::init(fpga), cl::cat(HPVMHypermapperCat));

// -t - Set target FPGA board, default A10GX
static cl::opt<std::string>
    Board("b", cl::desc("Target FPGA Board;\nDefaults to A10GX"),
          cl::value_desc("board"), cl::init("a10gx"),
          cl::cat(HPVMHypermapperCat));

// -tile-size - Set max tile size
static cl::opt<unsigned> MaxTileSize("tile-size",
                                     cl::desc("Max tile size;\nDefaults to 32"),
                                     cl::value_desc("tile size"), cl::init(32),
                                     cl::cat(HPVMHypermapperCat));

// -max-dim-uf - Set max dimension loop unroll factor
static cl::opt<unsigned> MaxDimUnrollFactor(
    "max-dim-uf",
    cl::desc(
        "Max Unroll Factor to use in DSE for dimension loops;\nDefaults to 5."),
    cl::value_desc("unroll factor"), cl::init(5), cl::cat(HPVMHypermapperCat));
// -max-dim-uf-options - Set max dimension loop unroll factor
static cl::opt<unsigned>
    MaxDimUFOptions("max-dim-uf-options",
                    cl::desc("Max number of Unroll Factor options for "
                             "dimension loops;\nDefaults to 3."),
                    cl::value_desc("num UFs"), cl::init(3),
                    cl::cat(HPVMHypermapperCat));

// -max-uf - Set max unroll factor
static cl::opt<unsigned> MaxUnrollFactor(
    "max-uf", cl::desc("Max Unroll Factor to use in DSE;\nDefaults to 10."),
    cl::value_desc("unroll factor"), cl::init(10), cl::cat(HPVMHypermapperCat));
// -max-uf-options - Set max unroll factor
static cl::opt<unsigned> MaxUFOptions(
    "max-uf-options",
    cl::desc("Max number of Unroll Factor options;\nDefaults to 3."),
    cl::value_desc("num UFs"), cl::init(3), cl::cat(HPVMHypermapperCat));

// -nodes - Set nodes for which to apply DSE
static cl::opt<std::string>
    Nodes("nodes",
          cl::desc("List of nodes (function names) for which DSE\nshould be "
                   "applied, separated by comma;\nLeave empty for all nodes."),
          cl::value_desc("nodes"), cl::init(""), cl::cat(HPVMHypermapperCat));

// -inline - enable inlining of all functions invoked by leaf nodes
static cl::opt<bool>
    EnableInlining("inline", cl::desc("Enable inlining;\nDefaults to true."),
                   cl::cat(HPVMHypermapperCat), cl::init(true));

// -timeout - set evaluation process timeout
static cl::opt<unsigned>
    TimeoutSeconds("timeout",
                   cl::desc("Timeout for evaluation in seconds;\n"
                            "Defaults to 900 (15 minutes)."),
                   cl::cat(HPVMHypermapperCat), cl::init(900));

// -test-no-hm - enable testing mode; no hypermapper
static cl::opt<bool> NoHM(
    "test-no-hm",
    cl::desc("Enable \"No HyperMapper\" testing mode;\nDefaults to false."),
    cl::cat(HPVMHypermapperCat), cl::init(false));
// -test-no-fork - enable testing mode; no forking
static cl::opt<bool>
    NoFork("test-no-fork",
           cl::desc("Enable \"No Forking\" testing mode;\nDefaults to false."),
           cl::cat(HPVMHypermapperCat), cl::init(false));
// -always-overwrite - Always overwrite output folder if it exists. Do not
// prompt for new folder name.
static cl::opt<bool>
    OverwriteOutput("always-overwrite",
                    cl::desc("Do not prompt when overwriting output folder."),
                    cl::cat(HPVMHypermapperCat), cl::init(false));
// -dbg - Enable debug tools for this tool, separate from --debug for LLVM
// passes.
static cl::opt<bool> DebugTool(
    "dbg", cl::desc("Enables debug prints for this tool;\nDefaults to FALSE."),
    cl::cat(HPVMHypermapperCat), cl::init(false));

// -csv - Enable printing out raw csv file for data.
static cl::opt<bool>
    CreateCSV("csv", cl::desc("Enable the creation of the raw data csv file."),
              cl::cat(HPVMHypermapperCat), cl::init(false));
// -no-multikernel - Disable Multi-kernel Opts
static cl::opt<bool> NoMultikernel(
    "no-multikernel",
    cl::desc(
        "Disable multikernel optimizations. Defaults to false.\nOnly used if no"
        "optimizations have been provided."),
    cl::cat(HPVMHypermapperCat), cl::init(false));

// -j - Batch size for samples
static cl::opt<int> BatchSize(
    "batch", cl::value_desc("n"),
    cl::desc("Set the batch size for sample processing;\nDefaults to 1."),
    cl::cat(HPVMHypermapperCat), cl::init(1));

// -util-threshold - cutoff resource utilization for DSE
static cl::opt<int>
    UtilThreshold("util-threshold", cl::value_desc("n"),
                  cl::desc("Cutoff Resource Utilization (%) after which a\n"
                           "design is considere invalid. Defaults to 95."),
                  cl::cat(HPVMHypermapperCat), cl::init(95));

// -multi-obj - use multiobjective DSE (exectime + resources)
static cl::opt<bool> MultiObjective(
    "multi-obj",
    cl::desc("Use a multi-objective DSE (Resources + ExecTime)\ninstead of "
             "single objective (ExecTime).\nDefaults to false"),
    cl::cat(HPVMHypermapperCat), cl::init(false));

//-synth - automatically run synthesis at the end of DSE.
static cl::opt<bool> Synthesize(
    "synth",
    cl::desc(
        "Automatically run synthesis at the end of DSE.\nDefaults to false."),
    cl::cat(HPVMHypermapperCat), cl::init(false));

//-emu - Use emulation.
static cl::opt<bool> Emulation("emu",
                               cl::desc("Use Emulation.\nDefaults to false."),
                               cl::cat(HPVMHypermapperCat), cl::init(false));

// -aoc-parallel=n - set parallel for AOC
static cl::opt<int>
    AOCParallel("aoc-parallel",
                cl::desc("Set the Parallel CLI of AOC when synthesizing;\n0 "
                         "means all threads.\nDefaults to 0."),
                cl::cat(HPVMHypermapperCat), cl::init(0));

// -egs=n - set the Epsilon Greedy Threshold
static cl::opt<double> EGS(
    "egs",
    cl::desc(
        "Sets the Epsilon Greedy Threshold for HyperMapper.\nDefaults to 0.1."),
    cl::cat(HPVMHypermapperCat), cl::init(0.1));

// -reps=n - set how many times to repeast DSE
static cl::opt<int>
    Reps("reps", cl::value_desc("num_reps"),
         cl::desc("Set how many times to repeat DSE.\nDefaults to 1."),
         cl::cat(HPVMHypermapperCat), cl::init(1));

// -j - Number of reps in parallel
static cl::opt<int>
    NumJobs("j", cl::value_desc("n"),
            cl::desc("Set the parallel job size (number of reps to run in "
                     "parallel);\nDefaults to <num_reps>."),
            cl::cat(HPVMHypermapperCat), cl::init(Reps));

// --global - Use global DSE params
static cl::opt<bool>
    GlobalParams("g", cl::desc("Use global DSE parameters; Defaults to FALSE."),
                 cl::cat(HPVMHypermapperCat), cl::init(false));

static cl::opt<std::string> CustomEvaluator(
    "custom-evaluator",
    cl::desc(
        "Path to script that takes the sample output directory and a thread id,\
 and prints the objective"),
    cl::value_desc("script"), cl::cat(HPVMHypermapperCat), cl::init(""));

// // Function that writes out the host module
// void writeOutputModule(Module &M, std::string MName) {

//   legacy::PassManager Passes;

//   std::error_code EC;
//   ToolOutputFile Out(MName.c_str(), EC, sys::fs::F_None);
//   if (EC) {
//     DEBUG_TOOL(errs() << EC.message() << "\n";);
//   }

//   Passes.add(createPrintModulePass(Out.os()));

//   Passes.run(M);

//   // Declare success.
//   Out.keep();
// }

// popen2 implementation adapted from:
// https://github.com/vi/syscall_limiter/blob/master/writelimiter/popen2.c
struct popen2 {
  pid_t child_pid;
  int from_child, to_child;
};

static int ForkExec(const char *cmdline) {
  pid_t pid = fork();
  if (pid != 0)
    return pid; // Error or return child pid
  execl("/bin/sh", "sh", "-c", cmdline, nullptr);
  perror("execl");
  exit(99);
  return -1;
}

static int ForkExec(const std::string &str) { return ForkExec(str.c_str()); }

int popen2(const char *cmdline, struct popen2 *childinfo) {
  pid_t p;
  int pipe_stdin[2], pipe_stdout[2];

  if (pipe(pipe_stdin))
    return -1;
  if (pipe(pipe_stdout))
    return -1;

  printf("pipe_stdin[0] = %d, pipe_stdin[1] = %d\n", pipe_stdin[0],
         pipe_stdin[1]);
  printf("pipe_stdout[0] = %d, pipe_stdout[1] = %d\n", pipe_stdout[0],
         pipe_stdout[1]);

  p = fork();
  if (p < 0)
    return p;   /* Fork failed */
  if (p == 0) { /* child */
    close(pipe_stdin[1]);
    dup2(pipe_stdin[0], 0);
    close(pipe_stdout[0]);
    dup2(pipe_stdout[1], 1);
    execl("/bin/sh", "sh", "-c", cmdline, 0);
    perror("execl");
    exit(99);
  }
  childinfo->child_pid = p;
  childinfo->to_child = pipe_stdin[1];
  childinfo->from_child = pipe_stdout[0];
  return 0;
}

// void fatalError(const std::string &msg) {
//   std::cerr << "FATAL: " << msg << std::endl;
//   exit(EXIT_FAILURE);
// }

// void warning(const std::string &msg) {
//   std::cerr << "WARNING: " << msg << std::endl;
// }

void createCSV(std::map<int, HMInputParam *> &InputParamsMap, int numParams,
               int Rep, std::string &csvfile_name, std::ofstream &csvfile) {
  std::string CurrentDir = fs::current_path();
  std::string OutputDir =
      CurrentDir + "/" + OutputFoldername + "/Rep." + std::to_string(Rep) + "/";
  assert(fs::exists(OutputDir) && "Output directory should have been created!");
  csvfile_name = OutputDir + "/out.csv";
  csvfile.open(csvfile_name);
  std::string header = "sample";
  for (int param = 0; param < numParams; param++) {
    std::string ParamStr = InputParamsMap[param]->getKey();
    header += ",";
    header += ParamStr;
  }
  if (MultiObjective)
    header += ",Resources";
  header += ",ExecTime,Valid";
  header += "\n";
  csvfile << header;
  csvfile.close();
}
std::string createjson(std::string AppName, int NumIterations,
                       std::vector<HMInputParam *> &InParams, int rep) {

  std::string CurrentDir = fs::current_path();
  std::string OutputDir =
      OutputFoldername + "/Rep." + std::to_string(rep) + "/";
  // assert(fs::exists(OutputDir) && "Expecting output directory to be
  // created!");
  json HMScenario;
  HMScenario["application_name"] = AppName;
  if (::Target == fpga && MultiObjective)
    HMScenario["optimization_objectives"] = {"Resources", "ExecTime"};
  else {
    HMScenario["optimization_objectives"] = {"ExecTime"};
    HMScenario["print_best"] = true;
  }
  HMScenario["hypermapper_mode"]["mode"] = "client-server";
  HMScenario["run_directory"] = CurrentDir;
  HMScenario["log_file"] = OutputDir + "/log_" + AppName + ".log";
  HMScenario["optimization_iterations"] = NumIterations;
  HMScenario["evaluations_per_optimization_iteration"] = BatchSize.getValue();
  HMScenario["models"]["model"] = "random_forest";
  HMScenario["number_of_cpus"] = int(NumHMCPUs);
  if (::Target == fpga) {
    json HMFeasibleOutput;
    HMFeasibleOutput["enable_feasible_predictor"] = true;
    HMFeasibleOutput["false_value"] = "0";
    HMFeasibleOutput["true_value"] = "1";
    HMScenario["feasible_output"] = HMFeasibleOutput;
  }

  HMScenario["output_data_file"] =
      OutputDir + "/" + AppName + "_output_data.csv";
  HMScenario["output_pareto_file"] =
      OutputDir + "/" + AppName + "_output_pareto.csv";
  HMScenario["output_image"]["output_image_pdf_file"] =
      OutputDir + "/" + AppName + "_output_image.pdf";

  json HMDOE;
  std::string DOEType;
  switch (SamplingType) {
  case rnd:
    DOEType = "random sampling";
    break;
  case slh:
    DOEType = "standard latin hypercube";
    break;
  case klh:
    DOEType = "k latin hypercube";
    break;
  }
  HMDOE["doe_type"] = DOEType; // "random sampling";

  int NumDOESamples =
      (DOESampleMultiplier == 1 ? InParams.size() + 1
                                : DOESampleMultiplier * InParams.size());
  HMDOE["number_of_samples"] = NumDOESamples;

  HMScenario["design_of_experiment"] = HMDOE;

  for (auto *InParam : InParams) {
    json HMParam;
    HMParam["parameter_type"] = getTypeAsString(InParam->getType());
    switch (InParam->getType()) {
    case Ordinal:
      HMParam["values"] = json(InParam->getVals());
      HMParam["parameter_default"] = InParam->getDefault();
      break;
    case Categorical: {
      std::vector<std::string> Categories;
      for (auto i : InParam->getVals()) {
        Categories.push_back(std::to_string(i));
      }
      HMParam["values"] = json(Categories);
      HMParam["parameter_default"] = std::to_string(InParam->getDefault());
      break;
    }
    default:
      hpvmUtils::fatalError("Only Ordinal and Categorical handled!");
      break;
    }
    HMScenario["input_parameters"][InParam->getKey()] = HMParam;
  }
  HMScenario["epsilon_greedy_threshold"] = EGS.getValue();

  //  std::cout << std::setw(4) << HMScenario << std::endl;
  std::ofstream HyperMapperScenarioFile;

  std::string JSonFileNameStr =
      CurrentDir + "/" + OutputDir + "/" + AppName + "_scenario.json";

  HyperMapperScenarioFile.open(JSonFileNameStr);
  if (HyperMapperScenarioFile.fail()) {
    hpvmUtils::fatalError("Unable to open file: " + JSonFileNameStr);
  }
  std::cout << "Writing JSON file to: " << JSonFileNameStr << std::endl;
  HyperMapperScenarioFile << std::setw(4) << HMScenario << std::endl;
  return JSonFileNameStr;
}

bool compareByDim(const UnrollDimInfo *A, const UnrollDimInfo *B) {
  return A->Dim > B->Dim;
}

void doUnrollLoop(const ParamMaps &PM, Function *F, std::ostream &os) {
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  auto &TTI = FAM.getResult<TargetIRAnalysis>(*F);

  auto UnrollCandidates = LookupMap(PM.UnrollLoopMap, F);
  // Unroll the loops in oposite order so that we always unroll inner loops
  // first. Assumption here is that the loops are stored in preorder.
  for (auto ucb = UnrollCandidates.rbegin(), uce = UnrollCandidates.rend();
       ucb != uce; ++ucb) {
    auto UnrollCandidate = *ucb;
    auto *HeaderBB = UnrollCandidate.first;
    auto UnrollFactor = UnrollCandidate.second;
    if (UnrollFactor == 1)
      continue;
    auto *L = LI.getLoopFor(HeaderBB);
    assert(L->getHeader() == HeaderBB && "Incorrect loop returned!");
    unsigned TripCount;
    unsigned TripMultiple;
    bool AllowRuntime = false;
    auto it = TripCountMap[F].find(HeaderBB);
    os << "\t\tUnrolling Loop with Header: " << HeaderBB->getName().str()
       << " by factor: " << UnrollFactor << std::endl;
    os.flush();
    if (it != TripCountMap[F].end()) {
      TripMultiple = it->second;
      TripCount = it->second;
      AllowRuntime = true;
      hpvm::unrollLoopByCount(L, LI, DT, SE, AC, TTI, TripCount, UnrollFactor,
                              TripMultiple, AllowRuntime);
      TripCountMap[F][HeaderBB] = TripCount / UnrollFactor;

    } else {
      TripCount = SE.getSmallConstantMaxTripCount(L);
      hpvm::unrollLoopByCount(L, LI, DT, SE, AC, TTI, TripCount, UnrollFactor);
    }
  }
}

void doUnrollDim(const ParamMaps &PM, DFLeafNode *leaf, std::ostream &os) {
  Function *F = leaf->getFuncPointer();
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  auto UnrollDimCandidates = LookupMap(PM.UnrollDimMap, leaf);
  auto &TTI = FAM.getResult<TargetIRAnalysis>(*F);

  for (auto ucb = UnrollDimCandidates.rbegin(),
            uce = UnrollDimCandidates.rend();
       ucb != uce; ++ucb) {
    auto UnrollDimCandidate = *ucb;
    // unsigned NumDims = UnrollDimCandidate.NodeDims;
    unsigned Dim = UnrollDimCandidate.Dim;
    unsigned UnrollFactor = UnrollDimCandidate.UnrollFactor;
    // unsigned TileLevel = LookupMap(PM.TileLvlMap, leaf);
    int LoopsToSkip = shouldSequentializeTarget(::Target) ? Dim : 0;
    // TODO: Incorporate tiling
    // if (TileLevel > 0)
    //  LoopsToSkip += NumDims * 2;
    Loop *LToUnroll;
    for (auto *L : LI.getLoopsInPreorder()) {
      if (LoopsToSkip == 0) {
        LToUnroll = L;
        break;
      }
      LoopsToSkip--;
    }
    auto *HeaderBB = LToUnroll->getHeader();
    unsigned TripCount;
    unsigned TripMultiple;
    bool AllowRuntime = false;
    auto it = TripCountMap[F].find(HeaderBB);
    os << "\t\tUnrolling Loop with Header: " << HeaderBB->getName().str()
       << " by factor: " << UnrollFactor << std::endl;
    os.flush();
    if (it != TripCountMap[F].end()) {
      TripMultiple = it->second;
      TripCount = it->second;
      AllowRuntime = true;
      hpvm::unrollLoopByCount(LToUnroll, LI, DT, SE, AC, TTI, TripCount,
                              UnrollFactor, TripMultiple, AllowRuntime);
      TripCountMap[F][HeaderBB] = TripCount / UnrollFactor;

    } else {
      TripCount = SE.getSmallConstantMaxTripCount(LToUnroll);
      hpvm::unrollLoopByCount(LToUnroll, LI, DT, SE, AC, TTI, TripCount,
                              UnrollFactor);
    }
    // unsigned TripCount = SE.getSmallConstantMaxTripCount(LToUnroll);
    // hpvm::unrollLoopByCount(LToUnroll, LI, DT, SE, AC, TripCount,
    // UnrollFactor);
  }
}

void doBI(const ParamMaps &PM, DFLeafNode *leaf) {
  Function *F = leaf->getFuncPointer();
  Module *M = F->getParent();
  auto BICandidates = LookupMap(PM.BufferInMap, leaf);
  for (auto &BICandidate : BICandidates) {
    unsigned ArgNum = BICandidate.first;
    unsigned BuffSize = BICandidate.second;
    Value *Arg = F->arg_begin() + ArgNum;
    if (isReadOnly(Arg, M) == 1) {
      ConstantInt *BuffSizeValue =
          ConstantInt::get(Type::getInt64Ty(M->getContext()), BuffSize);
      buffer_in(Arg, BuffSizeValue, F);
    }
  }
}

void doPriv(const ParamMaps &PM, DFLeafNode *leaf) {
  Function *F = leaf->getFuncPointer();
  Module *M = F->getParent();
  auto PrivCandidates = LookupMap(PM.PrivMap, leaf);
  for (auto &PrivCandidate : PrivCandidates) {
    unsigned ArgNum = PrivCandidate.first;
    unsigned BuffSize = PrivCandidate.second;
    Value *Arg = F->arg_begin() + ArgNum;
    ConstantInt *BuffSizeValue =
        ConstantInt::get(Type::getInt64Ty(M->getContext()), BuffSize);
    promote_priv(Arg, BuffSizeValue, F);
  }
}

// Takes the parameter maps mapping the new leaves
void OptimizeLeaf(ParamMaps &PM, DFLeafNode *NewLeaf, ValueToValueMapTy &VMap,
                  std::ostream &os, int SampleCount, int Rep) {
  int tid = omp_get_thread_num();
  os << "Optimizing Leaf: " << NewLeaf->getName().str() << std::endl;
  os.flush();
  printf("[%d:%d:%d]: Optimizing Leaf: %s\n", Rep, tid, SampleCount,
         NewLeaf->getName().str().c_str());
  Function *F = NewLeaf->getFuncPointer();

  // // Sequentialize leaf here. Any DFG transforms should be applied before
  // this
  // // point.
  // ValueToValueMapTy SeqVMap;
  // DFGTransform::Context TContext(*(F->getParent()), F->getContext());
  // LeafSequentializer *LS =
  //     new LeafSequentializer(TContext, NewLeaf, SeqVMap, 1);
  // LS->checkAndRun();

  // if (LS->hasCloned()) {
  //   // Need to use the SeqVMap to update the UnrollLoopMap
  //   Function *newF = NewLeaf->getFuncPointer();
  //   if (PM.UnrollLoopMap.find(F) != PM.UnrollLoopMap.end()) {
  //     std::vector<std::pair<BasicBlock *, int>> &MappedHeaders =
  //         PM.UnrollLoopMap[newF];
  //     for (auto &pair : PM.UnrollLoopMap[F]) {
  //       BasicBlock *CopiedHeader = pair.first;
  //       BasicBlock *MappedHeader = cast<BasicBlock>(SeqVMap[CopiedHeader]);
  //       int Factor = pair.second;
  //       auto Param = std::make_pair(MappedHeader, Factor);
  //       // Each loop should still only appear once
  //       assert(std::find(MappedHeaders.begin(), MappedHeaders.end(), Param)
  //       ==
  //              MappedHeaders.end());
  //       MappedHeaders.push_back(Param);
  //     }
  //     PM.UnrollLoopMap.erase(F);
  //   }
  //   // The leaf has a new func pointer, update F accordingly.
  //   F = newF;
  // }

  // LS->cleanup();
  auto start = std::chrono::steady_clock::now();
  auto end = std::chrono::steady_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

  // Remove loop guard branches
  if (shouldSequentializeTarget(::Target)) {
    os << "\tRemoving loop guard branches..." << std::endl;
    os.flush();
    printf("[%d:%d:%d]: \tRemoving loop guard branches...\n", Rep, tid,
           SampleCount);
    start = std::chrono::steady_clock::now();
    removeFuncLoopGuardBranches(F, 1 /* KeepI */);
    end = std::chrono::steady_clock::now();
    duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
    os.flush();
    printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
           duration.count());
    fflush(stdout);
  }

  os << "\tNormalizing..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tNormalizing...\n", Rep, tid, SampleCount);
  start = std::chrono::steady_clock::now();
  FunctionAnalysisManager FAM;
  LoopAnalysisManager LAM;
  ModuleAnalysisManager MAM;
  CGSCCAnalysisManager CAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  PB.registerLoopAnalyses(LAM);
  PB.registerModuleAnalyses(MAM);
  PB.registerCGSCCAnalyses(CAM);
  PB.crossRegisterProxies(LAM, FAM, CAM, MAM);
  FunctionPassManager FPM;
  FPM.addPass(LoopSimplifyPass());
  FPM.run(*F, FAM);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  os << "\tGetting loop trip counts..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tGetting loop trip counts...\n", Rep, tid, SampleCount);
  start = std::chrono::steady_clock::now();
  TripCountMap[F] = getFuncLoopTripCounts(F, 1);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  //  DEBUG_TOOL(dumpMap(TripCountMap[F]));
  os << "\tLowering mem-cpy instructions..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tLowering mem-cpy instructions...\n", Rep, tid,
         SampleCount);
  start = std::chrono::steady_clock::now();
  lowerFuncMemCpy(F, FAM.getResult<TargetIRAnalysis>(*F));
  FunctionPassManager FPM2;
  FPM2.addPass(SimplifyCFGPass());
  FPM2.addPass(LoopSimplifyPass());
  FPM2.run(*F, FAM);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  // DEBUG_TOOL(F->viewCFG());

  // First do Unrolling on Function loops
  os << "\tUnrolling loops..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tUnrolling loops...\n", Rep, tid, SampleCount);
  start = std::chrono::steady_clock::now();
  doUnrollLoop(PM, F, os);

  // DEBUG_TOOL(dumpMap(TripCountMap[F]));
  // F->viewCFG();

  // Also unroll loops in any called functions
  auto CalledFuncs = LookupMap(PM.NodeToCalledF, NewLeaf);
  for (auto *CalledF : CalledFuncs) {
    doUnrollLoop(PM, CalledF, os);
  }

  // Next, do Dim loops unrolling
  doUnrollDim(PM, NewLeaf, os);

  // Invalidate FunctionAnalysisManager after unrolling
  FAM.invalidate(*F,PreservedAnalyses::none());

  TripCountMap[F] = getFuncLoopTripCounts(F, 1);
  removeNZLoop(F);
  // Normalize after unrolling
  FunctionPassManager FPM3;
  // FPM3.addPass(SimplifyCFGPass());
  // FPM3.addPass(EarlyCSEPass(false));
  FPM3.addPass(InstCombinePass());
  FPM3.run(*F, FAM);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  // DEBUG_TOOL(F->viewCFG());
  // Next, do Fusion
  if (PM.LoopFusionSet.count(NewLeaf) > 0) {
    os << "\tFusing Loops..." << std::endl;
    os.flush();
    printf("[%d:%d:%d]: \tFusing Loops...\n", Rep, tid, SampleCount);
    start = std::chrono::steady_clock::now();
    hpvm::fuseFuncLoops(F);
    end = std::chrono::steady_clock::now();
    duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
    os.flush();
    printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
           duration.count());
    fflush(stdout);
  }

  // next do Input Buffering
  os << "\tBuffering inputs..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tBuffering inputs...\n", Rep, tid, SampleCount);
  start = std::chrono::steady_clock::now();
  doBI(PM, NewLeaf);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  // Finally, do Argument Privatization
  os << "\tPrivatizing arguments..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tPrivatizing arguments...\n", Rep, tid, SampleCount);
  start = std::chrono::steady_clock::now();
  doPriv(PM, NewLeaf);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  os << "\tSimplifying CFG..." << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \tSimplifying CFG...\n", Rep, tid, SampleCount);
  start = std::chrono::steady_clock::now();
  FunctionPassManager FPM4;
  FPM4.addPass(SimplifyCFGPass());
  FPM4.run(*F, FAM);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  os << "\t\tDuration: " << duration.count() << " ms" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  // DEBUG_TOOL(F->viewCFG());
  os << "Done!" << std::endl;
  os.flush();
  printf("[%d:%d:%d]: Done!\n", Rep, tid, SampleCount);
  fflush(stdout);
}
unsigned getTripCount(Loop *L, ScalarEvolution *SE) {
  Function *F = L->getHeader()->getParent();

  unsigned TripCount = SE->getSmallConstantMaxTripCount(L);

  if (TripCount == 0) {
    TripCount = TripCountMap[F][L->getHeader()];
    if (TripCount == 0) {
      errs() << "Loop: " << *L;
      hpvmUtils::fatalError("No trip count was provided for Loop!");
    }
  }
  return TripCount;
}
long long getLoopNestCycles(Loop *L, ScalarEvolution *SE,
                            std::vector<AOCLoopInfo> &IIVector,
                            int &LoopCounter, long long &SumL, int IIOuter,
                            int ParentTC, bool OuterSerialized) {
  assert(L->getLoopDepth() == IIVector[LoopCounter].Level &&
         "The loop level and loop depth don't match. We are not in the correct "
         "loops!\n");
  int CurLoopCounter = LoopCounter;
  unsigned LoopTC = getTripCount(L, SE);
  int Latency = IIVector[LoopCounter].Latency;
  int EffectiveInterleaving =
      OuterSerialized
          ? 1
          : std::min(IIVector[LoopCounter].MaxInterleaving, ParentTC);
  int LoopII = IIVector[LoopCounter].II / EffectiveInterleaving;
  bool Serialized = IIVector[LoopCounter].Serialized;
  int Spec = IIVector[LoopCounter].Spec;
  if (L->getSubLoops().empty()) {
    if (OuterSerialized)
      return (LoopTC + Spec) * LoopII + Latency;
    assert((IIOuter <= (LoopII * LoopTC)) &&
           "Found unsupported case where IIOuter > LoopII*LoopTC");
    SumL = Latency;
    return (LoopTC + Spec) * LoopII;
  }
  long long Cycles = 0;
  long long LocalSumL = Latency;
  for (auto *SL : L->getSubLoops()) {
    long long InnerSumL = 0;
    long long ChildCycles = getLoopNestCycles(
        SL, SE, IIVector, ++LoopCounter, InnerSumL, LoopII, LoopTC, Serialized);
    LocalSumL += InnerSumL;
    Cycles += ChildCycles * (LoopTC + Spec);
  }
  if (Serialized)
    Cycles += LocalSumL * (LoopTC + Spec);
  else {
    if (!OuterSerialized)
      assert((IIOuter <= (LoopII * LoopTC)) &&
             "Found unsupported case where IIOuter > LoopII*LoopTC");
    SumL += LocalSumL;
  }
  return Cycles;
}

// The execution time of a node is equal to the sum of the execution time of all
// the loop nests in it. The execution time of each loop nest is the II x
// #Iterations x CycleTime. We use a representative loop iteration
// count since most loops will be variable.
double calculateLeafNodeDuration(
    DFNode *C, std::map<std::string, std::vector<AOCLoopInfo>> &IIInfo,
    int minFmax, int SampleCount, int Rep, std::ostream &os) {
  assert(C->getKind() == DFNode::DFNodeKind::LeafNode &&
         "We can only calculate the duration of leaf nodes!");
  if (C->getTargetHint() != hpvm::FPGA_TARGET) {
    // Skip leaf nodes that are not FPGA target. Mark them as 0.
    int tid = omp_get_thread_num();
    printf("[%d:%d:%d]: Setting duration of non-FPGA leaf node to 0: %s\n", Rep,
           tid, SampleCount, C->getName().str().c_str());
    os << "Setting duration of non-FPGA leaf node to 0: " << C->getName().str()
       << std::endl;
    os.flush();
    fflush(stdout);
    return 0.0;
  }
  // DEBUG_TOOL(errs() << "Calculating execution time for leaf node: "
  //                   << C->getName() << "\n");
  int tid = omp_get_thread_num();
  printf("[%d:%d:%d]: Calculating execution time for leaf node: %s\n", Rep, tid,
         SampleCount, C->getName().str().c_str());
  os << "Calculating execution time for leaf node: " << C->getName().str()
     << std::endl;
  os.flush();
  fflush(stdout);
  Function *F = C->getFuncPointer();
  // TripCountMap[F] = getFuncLoopTripCounts(F);
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  std::string fname = F->getName().str();
  // fname += "_fpga_c";
  assert(IIInfo.find(fname) != IIInfo.end() && "Something weird happened!\n");
  auto &IIVector = IIInfo[fname];
  int LoopCounter = 0;
  long long Cycles = 0;
  // We need all the outermost loops in preorder.
  for (auto *L : LI.getLoopsInPreorder()) {
    if (L->getLoopDepth() == 1) {
      long long SumL = 0;
      Cycles += getLoopNestCycles(L, &SE, IIVector, LoopCounter, SumL, 0, 1, 0);
      Cycles += SumL;
      ++LoopCounter;
    }
  }
  double ExecTime = 1.0 * Cycles / (minFmax * 1000000);
  //  DEBUG_TOOL(std::cout << "==> Exec Time: " << ExecTime << "\n");
  return ExecTime;
}

void calculateChildGraphTimes(
    DFInternalNode *N, std::map<DFNode *, double> &NodeDuration,
    std::map<DFNode *, double> &NodeEFT, std::map<DFNode *, double> &NodeEST,
    std::map<std::string, std::vector<AOCLoopInfo>> &IIInfo, int minFmax,
    bool EnableTLP, int SampleCount, int Rep, std::ostream &os) {
  // Get the child graph of the internal node and sort the childer in preorder
  DFGraph *G = N->getChildGraph();
  G->sortChildren();
  // For each child, do the calculation. If internal node, invoke the function
  // recursively.
  double globalMaxEFT = 0.0;
  for (auto ci = G->begin(), ce = G->end(); ci != ce; ++ci) {
    DFNode *C = *ci;
    // If this is the entry node, its start time is the start time of its
    // parent, and its duration is 0. The root start time will be initialized
    // outside this function.
    if (C->isEntryNode()) {
      assert(C->getParent() == N && "The Parent of the Entry Node is not N!");
      NodeEST[C] = NodeEST[N];
      NodeDuration[C] = 0.0;
      NodeEFT[C] = NodeEST[C];
      continue;
    } else if (C->isExitNode()) {
      // For the exit node, we set the EFT to be the same as the EST, and then
      // record the end time so that we can use that to calculate the internal
      // node duration.
      assert(C->getParent() == N && "The Parent of the Entry Node is not N!");
      NodeEST[C] = globalMaxEFT;
      NodeDuration[C] = 0.0;
      NodeEFT[C] = NodeEST[C];
      NodeEFT[N] = NodeEFT[C];
      continue;
    }
    // For each node, find the largest Earliest Finish Time of all its
    // predecessors. That will be its Earliest Start Time.
    double maxEFT = NodeEFT[(*C->indfedge_begin())->getSourceDF()];
    for (auto ei = C->indfedge_begin() + 1, ee = C->indfedge_end(); ei != ee;
         ++ei) {
      DFNode *SN = (*ei)->getSourceDF();
      if (NodeEFT.find(SN) == NodeEFT.end())
        hpvmUtils::fatalError("Expecting all predecessors of node to already be visited!");
      if (EnableTLP) {
        maxEFT = std::max(maxEFT, NodeEFT[SN]);
      } else {
        maxEFT += NodeDuration[SN];
      }
    }
    NodeEST[C] = maxEFT;

    // For a leaf node, we calculate the estimated duration using IIInfo and
    // Fmax. For an internal node, we recursively call this function on the
    // internal node.
    if (C->getKind() == DFNode::DFNodeKind::LeafNode) {
      NodeDuration[C] =
          calculateLeafNodeDuration(C, IIInfo, minFmax, SampleCount, Rep, os);
    } else {
      NodeDuration[C] = 0.0;
      calculateChildGraphTimes(dyn_cast<DFInternalNode>(C), NodeDuration,
                               NodeEFT, NodeEST, IIInfo, minFmax, EnableTLP,
                               SampleCount, Rep, os);
      // assert(NodeDuration[C] != 0.0 &&
      //        "The internal node duration was not calculated!\n");
    }

    // The EFT is the EST + the duration.
    NodeEFT[C] = NodeEST[C] + NodeDuration[C];
    globalMaxEFT = std::max(globalMaxEFT, NodeEFT[C]);
  }

  // Finally, use the end time and start time to calculate the internal node's
  // duration.
  NodeDuration[N] = NodeEFT[N] - NodeEST[N];
}
// Estimate the execution time using avgII and minFmax. A DFG traversal will
// help calculate the execution time while taking into account concurrent nodes.
// For multiple DFGs we estimate the exec time as the sum of all
double EstimateExecTime(BuildDFG *DFG, std::vector<DFLeafNode *> Leaves,
                        std::map<std::string, std::vector<AOCLoopInfo>> &IIInfo,
                        int minFmax, bool EnableTLP, int SampleCount, int Rep,
                        std::ostream &os) {
  std::vector<DFInternalNode *> Roots = DFG->getRoots();
  std::map<DFNode *, double> NodeDuration;
  std::map<DFNode *, double> NodeEFT;
  std::map<DFNode *, double> NodeEST;
  double Total = 0.0;
  for (auto *Root : Roots) {
    NodeEST[Root] = 0.0;
    calculateChildGraphTimes(Root, NodeDuration, NodeEFT, NodeEST, IIInfo,
                             minFmax, EnableTLP, SampleCount, Rep, os);
    Total += NodeEFT[Root];
  }
  return Total;
}

class DisjointSet {
public:
  DFLeafNode *Find(DFLeafNode *node) { return nodes[FindIdx(node)]; }

  void Insert(DFLeafNode *node) {
    assert(nodeToIdx.count(node) == 0);
    nodeToIdx[node] = nodes.size();
    nodes.push_back(node);
    parentIdxes.push_back(-1);
  }

  void Merge(DFLeafNode *Parent, DFLeafNode *Child) {
    const int ParentIdx = FindIdx(Parent);
    const int ChildIdx = FindIdx(Child);
    parentIdxes[ChildIdx] = ParentIdx;
  }

  std::vector<DFLeafNode *> GetRoots() const {
    std::vector<DFLeafNode *> Res;
    for (size_t i = 0; i < nodes.size(); ++i)
      if (parentIdxes[i] == -1)
        Res.push_back(nodes[i]);
    return Res;
  }

private:
  int FindIdx(DFLeafNode *node) {
    int prevPrevIdx = -1;
    int prevIdx = -1;
    int idx = nodeToIdx.at(node);
    while (idx != -1) {
      if (prevPrevIdx != -1)
        parentIdxes[prevPrevIdx] = idx;
      prevPrevIdx = prevIdx;
      prevIdx = idx;
      idx = parentIdxes[idx];
    }
    return prevIdx;
  }

  std::vector<DFLeafNode *> nodes;
  std::unordered_map<DFLeafNode *, size_t> nodeToIdx;
  std::vector<int> parentIdxes;
};

static void MergeChildParams(ParamMaps &PM, DFLeafNode *Fusion,
                             DFLeafNode *Child, ValueToValueMapTy &VMap) {

  Function *FusionF = Fusion->getFuncPointer();
  Function *ChildF = Child->getFuncPointer();
  unsigned ChildArgCount = ChildF->arg_end() - ChildF->arg_begin();

  // Fusions call all its childrens calls
  if (PM.NodeToCalledF.count(Child) > 0) {
    for (Function *CalledF : PM.NodeToCalledF[Child]) {
      PM.NodeToCalledF[Fusion].push_back(CalledF);
    }
    PM.NodeToCalledF.erase(Child);
  }

  // TODO: TileLvlMap, BlockSizeMap

  // UnrollLoopMap: Map to new function and loop headers
  // There should be no duplicates

  if (PM.UnrollLoopMap.count(ChildF) > 0) {
    std::vector<std::pair<BasicBlock *, int>> &MappedHeaders =
        PM.UnrollLoopMap[FusionF];
    for (auto &pair : PM.UnrollLoopMap[ChildF]) {
      BasicBlock *CopiedHeader = pair.first;
      BasicBlock *MappedHeader = cast<BasicBlock>(VMap[CopiedHeader]);
      int Factor = pair.second;
      auto Param = std::make_pair(MappedHeader, Factor);

      // Each loop should still only appear once
      assert(std::find(MappedHeaders.begin(), MappedHeaders.end(), Param) ==
             MappedHeaders.end());
      MappedHeaders.push_back(Param);
    }
    PM.UnrollLoopMap.erase(ChildF);
  }

  // BI and Priv: find the mapped arguments and merge w/ max

  if (PM.BufferInMap.count(Child) > 0) {
    for (auto &pair : PM.BufferInMap[Child]) {
      unsigned CopiedArgPos = pair.first;
      unsigned CopiedSize = pair.second;

      assert(CopiedArgPos < ChildArgCount);
      Argument *CopiedArg = ChildF->arg_begin() + CopiedArgPos;
      if (Value *MappedArg = VMap.lookup(CopiedArg)) {
        if (Argument *NewArg = dyn_cast<Argument>(MappedArg)) {
          unsigned NewArgPos = NewArg - FusionF->arg_begin();

          // Will be zero initialized if not present
          unsigned &NewSize = PM.BufferInMap[Fusion][NewArgPos];
          NewSize = std::max(CopiedSize, NewSize);
        } else {
          errs() << "Mapping for buffered argument:\n"
                 << *CopiedArg << "\nwas a non-argument:" << *MappedArg << "\n";
        }
      } else {
        errs() << "Mapping for buffered argument:\n"
               << *CopiedArg << "\nwas not found\n";
      }
    }
    PM.BufferInMap.erase(Child);
  }

  if (PM.PrivMap.count(Child) > 0) {
    for (auto &pair : PM.PrivMap[Child]) {
      unsigned CopiedArgPos = pair.first;
      unsigned CopiedSize = pair.second;

      assert(CopiedArgPos < ChildArgCount);
      Argument *CopiedArg = ChildF->arg_begin() + CopiedArgPos;
      if (Value *MappedArg = VMap.lookup(CopiedArg)) {
        if (Argument *NewArg = dyn_cast<Argument>(MappedArg)) {
          unsigned NewArgPos = NewArg - FusionF->arg_begin();

          // Will be zero initialized if not present
          unsigned &NewSize = PM.PrivMap[Fusion][NewArgPos];
          NewSize = std::max(CopiedSize, NewSize);
        } else {
          errs() << "Mapping for buffered argument:\n"
                 << *CopiedArg << "\nwas a non-argument:" << *MappedArg << "\n";
        }
      } else {
        errs() << "Mapping for buffered argument:\n"
               << *CopiedArg << "\nwas not found\n";
      }
    }
    PM.PrivMap.erase(Child);
  }

  // TODO: UnrollDimMap
  if (PM.UnrollDimMap.count(Child) > 0) {
    std::vector<UnrollDimInfo> &MappedDimInfo = PM.UnrollDimMap[Fusion];
    for (auto &UDI : PM.UnrollDimMap[Child]) {
      int Dim = UDI.Dim;
      int Factor = UDI.UnrollFactor;
      int NodeDims = UDI.NodeDims;
      // check if this Dim has already been added from other child
      // If yes, keep the larger unroll factor.
      bool found = false;
      for (auto &FusedUDI : MappedDimInfo) {
        if (FusedUDI.Dim == Dim) {
          FusedUDI.UnrollFactor = std::max(FusedUDI.UnrollFactor, Factor);
          found = true;
          break;
        }
      }
      if (!found) {
        UnrollDimInfo Param = {Dim, Factor, NodeDims};
        MappedDimInfo.push_back(Param);
      }
    }
    PM.UnrollDimMap.erase(Child);
  }

  if (PM.LoopFusionSet.count(Child) > 0) {
    PM.LoopFusionSet.insert(Fusion);
    PM.LoopFusionSet.erase(Child);
  }

  // Like unroll parameters, no merges should be necessary
  if (PM.TripCountMap.count(ChildF) > 0) {
    std::map<BasicBlock *, int> &MyTripMap = PM.TripCountMap[FusionF];
    for (auto &pair : PM.TripCountMap[ChildF]) {
      assert(MyTripMap.count(pair.first) == 0);

      BasicBlock *NewHeader = cast<BasicBlock>(VMap[pair.first]);
      assert(NewHeader);
      MyTripMap[NewHeader] = pair.second;
    }
    PM.TripCountMap.erase(ChildF);
  }
}

static ParamMaps
fuseEdges(const std::vector<DFLeafNode *> &Leaves,
          const std::vector<std::pair<DFLeafNode *, DFLeafNode *>> &FusedEdges,
          int SampleCount, int Rep, std::ostream &os) {
  int count = 0;
  ValueToValueMapTy accMap;
  DisjointSet Fusions; // Will contain new leaves only
  ParamMaps PM;

  // Initialize mappings for existing leaves from global inputs
  for (auto Leaf : Leaves) {
    DFLeafNode *CopiedLeaf = cast<DFLeafNode>(DFGMap[Leaf]);
    Function *CopiedF = CopiedLeaf->getFuncPointer();
    Fusions.Insert(CopiedLeaf);
    if (NodeToCalledF.count(CopiedLeaf) > 0)
      PM.NodeToCalledF[CopiedLeaf] = NodeToCalledF[CopiedLeaf];
    if (UnrollLoopMap.count(CopiedF) > 0)
      PM.UnrollLoopMap[CopiedF] = UnrollLoopMap[CopiedF];
    if (BufferInMap.count(CopiedLeaf) > 0)
      PM.BufferInMap[CopiedLeaf] = BufferInMap[CopiedLeaf];
    if (PrivMap.count(CopiedLeaf) > 0)
      PM.PrivMap[CopiedLeaf] = PrivMap[CopiedLeaf];
    if (LoopFusionSet.count(CopiedLeaf) > 0)
      PM.LoopFusionSet.insert(CopiedLeaf);
    if (TripCountMap.count(CopiedF) > 0)
      PM.TripCountMap[CopiedF] = TripCountMap[CopiedF];
    if (UnrollDimMap.count(CopiedLeaf) > 0)
      PM.UnrollDimMap[CopiedLeaf] = UnrollDimMap[CopiedLeaf];
  }

  for (auto p : FusedEdges) {
    DFLeafNode *OldN1 = cast<DFLeafNode>(p.first);
    DFLeafNode *OldN2 = cast<DFLeafNode>(p.second);
    assert(std::find(Leaves.begin(), Leaves.end(), OldN1) != Leaves.end());
    assert(std::find(Leaves.begin(), Leaves.end(), OldN2) != Leaves.end());

    DFLeafNode *N1 = Fusions.Find(cast<DFLeafNode>(DFGMap[OldN1]));
    DFLeafNode *N2 = Fusions.Find(cast<DFLeafNode>(DFGMap[OldN2]));

    // Perform the fusion
    DFGTransform::Context TContext(*(N1->getFuncPointer()->getParent()),
                                   N1->getFuncPointer()->getContext());
    Fuse Fusor(TContext, {N1, N2});
    if (Fusor.checkAndRun()) {
      DFLeafNode *Fusion = Fusor.getNewNode();

      // Want to enforce the most fused node being the representative
      Fusions.Insert(Fusion);
      Fusions.Merge(Fusion, N1);
      Fusions.Merge(Fusion, N2);

      // Update parameter mappings
      std::unique_ptr<ValueToValueMapTy> Res = Fusor.getResVmap();
      MergeChildParams(PM, Fusion, N1, *Res);
      MergeChildParams(PM, Fusion, N2, *Res);

      count++;
    }
  }
  PM.Members = Fusions.GetRoots();

  int tid = omp_get_thread_num();
  printf("[%d:%d:%d]: %d fusion(s)\n", Rep, tid, SampleCount, count);
  os << count << "fusion(s)" << std::endl;
  os.flush();
  fflush(stdout);

  return PM;
}

static int OpenChild(const std::string &Command) {
  int Pid = ForkExec(Command);
  assert(Pid != 0);
  if (Pid < 0)
    errs() << "OpenChild: ForkExec failed\n";
  return Pid;
}

// Returns status
static int CloseChild(int Pid) {
  int status;
  if (waitpid(Pid, &status, 0) == -1) {
    errs() << "CloseChild: waitpid on child failed\n";
    exit(1);
  }
  if (!WIFEXITED(status)) {
    errs() << "CloseChild: Child exited abnormally\n";
    exit(1);
  }
  return WEXITSTATUS(status);
}

static void evaluateModuleWithAOC(HMObjective &OutObj,
                                  const std::string &SampleDir,
                                  const std::string &ModuleName, // Old
                                  int Rep, int SampleCount, llvm::Module &M,
                                  BuildDFG &DFG,
                                  const std::vector<DFLeafNode *> &newLeaves,
                                  bool EnableTLP, std::ostream &OutLog,
                                  std::mutex &TimeoutMutex, bool &DontKill) {

  using namespace std::chrono;

  int tid = omp_get_thread_num();

  std::string OCLFileName = SampleDir + ModuleName + ".kernels.cl";
  std::string NewKernelModuleName = SampleDir + ModuleName + ".kernels.ll";
  std::string HostModuleName = SampleDir + ModuleName + ".host.ll";
  std::string AOCRFileName = SampleDir + ModuleName + ".kernels.aocr";
  std::string AOCXFileName = SampleDir + ModuleName + ".kernels.aocx";
  dfg2llvm::runDFG2LLVM_FPGA(M, DFG, EnableTLP, Emulation, 0,
                             NewKernelModuleName, AOCXFileName);

  std::string command;
  std::string NewKernelOptModuleName =
      SampleDir + ModuleName + ".kernels.opt.ll";
  command = std::string(LLVM_BUILD_DIR_STR) + "/bin/opt --lowerswitch " + NewKernelModuleName + " -S -o " +
            NewKernelOptModuleName;
  OutLog << "Executing " << command << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Executing %s\n", Rep, tid, SampleCount, command.c_str());
  fflush(stdout);
  auto start = steady_clock::now();
  int OptPid = OpenChild(command);
  int OptStatus = CloseChild(OptPid);
  auto end = steady_clock::now();
  auto duration = duration_cast<milliseconds>(end - start);
  DEBUG_TOOL(
      printf("[%d:%d:%d]: status: %d\n", Rep, tid, SampleCount, OptStatus));
  OutLog << "status: " << OptStatus << std::endl;
  OutLog << "OPT Duration: " << duration.count() << " ms" << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: OPT Duration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  command = std::string(LLVM_BUILD_DIR_STR) + "/bin/llvm-ocl " +
            NewKernelOptModuleName + " -o " + OCLFileName;
  OutLog << "Executing " << command << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Executing %s\n", Rep, tid, SampleCount, command.c_str());
  fflush(stdout);
  start = steady_clock::now();
  int CLId = OpenChild(command);
  int CLStatus = CloseChild(CLId);
  end = steady_clock::now();
  duration = duration_cast<milliseconds>(end - start);
  DEBUG_TOOL(
      printf("[%d:%d:%d]: status: %d\n", Rep, tid, SampleCount, CLStatus));
  OutLog << "status: " << CLStatus << std::endl;
  OutLog << "OCLBE Duration: " << duration.count() << " ms" << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: OCLBE Duration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);

  if (CLStatus != 0 && !NoFork)
    exit(1);
  else if (CLStatus != 0 && NoFork)
    hpvmUtils::fatalError("[" + std::to_string(tid) + "]llvm-ocl failed!");

  command = "cd " + SampleDir + "; aoc -rtl " + OCLFileName + " -o " +
            AOCRFileName + " -board=" + Board +
            " -dont-error-if-large-area-est";
  OutLog << "Executing " << command << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Executing %s\n", Rep, tid, SampleCount, command.c_str());
  fflush(stdout);
  start = std::chrono::steady_clock::now();
  int AocId = OpenChild(command);

  /* Don't know how to pipe output nicely currently
     const int max_buffer = 1000;

     // fgets is MT-Safe
     if (false) {
     FILE *outstream = fdopen(Child.from_child, "r");
     char buffer[max_buffer];
     while (fgets(buffer, max_buffer, outstream)) {
     printf("[%d:%d:%d]: %s", Rep, tid, SampleCount, buffer);
     OutLog << buffer;
     OutLog.flush();
     fflush(stdout);
     }
     fclose(outstream);
     }
  */

  int AocStatus = CloseChild(AocId);
  end = steady_clock::now();
  {
    // At this point there isn't much uncertainty in running time
    std::lock_guard G(TimeoutMutex);
    DontKill = true;
  }
  duration = duration_cast<milliseconds>(end - start);
  DEBUG_TOOL(
      printf("[%d:%d:%d]: status: %d\n", Rep, tid, SampleCount, AocStatus));
  OutLog << "status: " << AocStatus << std::endl;
  OutLog << "AOC Duration: " << duration.count() << " ms" << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: AOC Duration: %d ms\n", Rep, tid, SampleCount,
         duration.count());
  fflush(stdout);
  if (AocStatus != 0 && !NoFork)
    exit(1);
  else if (AocStatus != 0 && NoFork)
    hpvmUtils::fatalError("[" + std::to_string(tid) + "]aoc failed!");

  std::string BuildDir = fs::path(AOCRFileName).remove_filename();
  std::string AOCFolder = fs::path(AOCRFileName).filename().stem();
  std::string ReportFolder = BuildDir + AOCFolder + "/reports";

  if (!fs::exists(ReportFolder + "/lib/json") ||
      !fs::exists(BuildDir + AOCFolder + ".aocr")) {
    OutLog << "AOC did not terminate correctly!" << std::endl;
    OutLog.flush();
    printf("[%d:%d:%d]: AOC did not terminate correctly!\n", Rep, tid,
           SampleCount);
    fflush(stdout);
    if (!NoFork)
      exit(1);
    else
      hpvmUtils::fatalError("Terminating!");
  }

  std::string NewReportFolder = SampleDir + "/report";
  OutLog << "Copying reports from " << ReportFolder << " to " << NewReportFolder
         << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]Copying reports from %s to %s\n", Rep, tid, SampleCount,
         ReportFolder.c_str(), NewReportFolder.c_str());
  fs::copy(ReportFolder, NewReportFolder, fs::copy_options::recursive);

  OutLog << "Deleting aoc output folder " << BuildDir + AOCFolder << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Deleting AOC output folder %s\n", Rep, tid, SampleCount,
         (BuildDir + AOCFolder).c_str());
  fs::remove_all(BuildDir + AOCFolder);

  std::string JSONFolder = NewReportFolder + "/lib/json";
  std::string FmaxIIFName = JSONFolder + "/fmax_ii.json";
  std::string LoopsFName = JSONFolder + "/loops.json";

  OutLog << "Reading Fmax/II report from " << FmaxIIFName << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Reading Fmax/II report from %s\n", Rep, tid, SampleCount,
         FmaxIIFName.c_str());
  std::ifstream FmaxIIReportFile(FmaxIIFName);
  std::string FmaxIIReportStr;

  OutLog << "Reading Loops report from " << LoopsFName << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Reading Loops report from %s\n", Rep, tid, SampleCount,
         LoopsFName.c_str());
  std::ifstream LoopsReportFile(LoopsFName);
  std::string LoopsReportStr;
  if (FmaxIIReportFile && LoopsReportFile) {
    std::ostringstream ss;
    ss << FmaxIIReportFile.rdbuf();
    FmaxIIReportStr = ss.str();
    FmaxIIReportFile.close();

    std::ostringstream newss;
    newss << LoopsReportFile.rdbuf();
    LoopsReportStr = newss.str();
    LoopsReportFile.close();

    json FmaxIIReportJSON = json::parse(FmaxIIReportStr);
    json LoopsReportJSON = json::parse(LoopsReportStr);

    json BasicBlocks = FmaxIIReportJSON["basicblocks"];
    json Functions = FmaxIIReportJSON["functions"];
    std::map<std::string, std::vector<AOCLoopInfo>> LoopIIInfo;
    std::vector<int> Fmax;

    for (auto &kernel : LoopsReportJSON["children"]) {
      std::string KernelName = std::string(kernel["name"]).substr(8);
      json loops = kernel["children"];

      std::unordered_map<int, int> BBNumToLatency;
      int BBNum = 0;
      int CurrLayer = 0;
      std::stack<int> HeaderStack;
      while (BasicBlocks.find(KernelName + ".B" + std::to_string(BBNum)) !=
             BasicBlocks.end()) {
        auto BB = BasicBlocks[KernelName + ".B" + std::to_string(BBNum)];
        int BBLayer = BB["loop_layer"];
        if (BBLayer == 0) {
          BBNum++;
          continue;
        } else if (BBLayer < CurrLayer) {
          int PopCount = CurrLayer - BBLayer;

          // At least one element needs to be left
          assert(HeaderStack.size() >= PopCount + 1);
          for (unsigned i = 0; i < PopCount; ++i)
            HeaderStack.pop();
          CurrLayer = BBLayer;
        }
        if (BB["is_loop_header"] == "Yes") {
          HeaderStack.push(BBNum);
          assert(BBLayer = CurrLayer + 1);
          CurrLayer = BBLayer;
        }
        int CurrHeader = HeaderStack.top();
        int Latency = BB["latency"];
        BBNumToLatency[CurrHeader] += Latency;
        BBNum++;
      }
      assert(BBNum > 0);

      KernelName = KernelName.substr(0, KernelName.find("_fpga"));
      // for (auto &loop : loops) {
      while (!loops.empty()) {
        auto loop = loops.front();
        std::string Name = loop["name"];
        size_t locB = Name.find_last_of('B');
        int BBNum = stoi(Name.substr(locB + 1));
        std::string PipelinedStr = loop["data"][0];
        bool Pipelined = PipelinedStr == "Yes" ? 1 : 0;
        std::string SpecStr = loop["data"][2].get<std::string>();
        int Spec = SpecStr == "n/a" ? 0 : stoi(SpecStr);
        std::string Details = loop["details"][0]["text"];
        bool Serial =
            !Pipelined || (Details.find("Serial exe") != std::string::npos);

        auto it = BasicBlocks.find(loop["name"]);
        assert(it != BasicBlocks.end() && "Couldn't find Key in BasicBlocks!");
        auto &value = it.value();
        assert(value["is_loop_header"] == "Yes");

        struct AOCLoopInfo AOCLI;
        AOCLI.BB = BBNum;
        AOCLI.II =
            Pipelined ? value["achieved_ii"].get<int>() : BBNumToLatency[BBNum];
        AOCLI.Level = value["loop_layer"].get<int>();
        AOCLI.Latency = BBNumToLatency[BBNum];
        AOCLI.MaxInterleaving = value["max_interleaving"].get<int>();
        AOCLI.Serialized = Serial;
        AOCLI.Spec = Spec;
        LoopIIInfo[KernelName].push_back(AOCLI);
        Fmax.push_back(stoi(value["achieved_fmax"].get<std::string>()));

        for (auto &SubLoop : loop["children"]) {
          loops.push_back(SubLoop);
        }
        loops.erase(loops.begin());
      }
    }

    for (auto &AOCLoopInfos : LoopIIInfo) {
      auto &LoopInfoVector = AOCLoopInfos.second;
      std::sort(LoopInfoVector.begin(), LoopInfoVector.end(),
                [](const AOCLoopInfo &L1, const AOCLoopInfo &L2) {
                  return L1.BB < L2.BB;
                });
    }
    DEBUG_TOOL(for (auto &AOCLoopInfos
                    : LoopIIInfo) {
      OutLog << AOCLoopInfos.first << std::endl;
      std::cout << AOCLoopInfos.first << std::endl;
      for (auto &AOCLI : AOCLoopInfos.second) {
        OutLog << AOCLI;
        OutLog.flush();
        std::cout << AOCLI;
        std::cout.flush();
      }
    });
    int minFmax = *std::min_element(Fmax.begin(), Fmax.end());

    printf("[%d:%d:%d]: Estimating ExecTime with Freq = %d\n", Rep, tid,
           SampleCount, minFmax);
    DEBUG_TOOL(OutLog << "Estimating Execution Time with Frequency: " << minFmax
                      << std::endl);
    start = steady_clock::now();
    double EstimatedExecTime =
        EstimateExecTime(&DFG, newLeaves, LoopIIInfo, minFmax, EnableTLP,
                         SampleCount, Rep, OutLog);
    end = steady_clock::now();
    duration = duration_cast<milliseconds>(end - start);
    OutLog << "Exec Time Estimate Duration: " << duration.count() << " ms"
           << std::endl;
    printf("[%d:%d:%d]: Exec Time Estimate Duration: %d ms\n", Rep, tid,
           SampleCount, duration.count());
    OutLog.flush();

    std::string SummaryFName = JSONFolder + "/area.json";
    OutLog << "Reading Area report from " << SummaryFName << std::endl;
    OutLog.flush();
    printf("[%d:%d:%d]: Reading Area report from %s\n", Rep, tid, SampleCount,
           SummaryFName.c_str());
    FmaxIIReportFile = std::ifstream(SummaryFName);
    if (FmaxIIReportFile) {
      std::ostringstream ss;
      ss << FmaxIIReportFile.rdbuf();
      FmaxIIReportStr = ss.str();
      FmaxIIReportFile.close();
    } else {
      printf("[%d:%d:%d]: Report file can't be opened, sample point invalid!\n",
             tid);
      OutLog << "Report file can't be opened, sample point invalid!"
             << std::endl;
      OutLog.flush();
      if (!NoFork)
        exit(1);
      else
        hpvmUtils::fatalError("[" + std::to_string(tid) + "]: Terminating!");
    }

    FmaxIIReportJSON = json::parse(FmaxIIReportStr);

    json ResourceUtil = FmaxIIReportJSON["total"];
    json ResourceMax = FmaxIIReportJSON["max_resources"];
    json ResourcePercent = FmaxIIReportJSON["total_percent"];

    int LUTUtil = ResourceUtil[0].get<int>();
    // int LUTMax = ResourceMax[0].get<int>();
    float LUTPercent = ResourcePercent[0].get<float>();

    int FFUtil = ResourceUtil[1].get<int>();
    // int FFMax = ResourceMax[1].get<int>();
    float FFPercent = ResourcePercent[1].get<float>();

    int RAMUtil = ResourceUtil[2].get<int>();
    // int RAMMax = ResourceMax[2].get<int>();
    float RAMPercent = ResourcePercent[2].get<float>();

    int DSPUtil = ResourceUtil[3].get<int>();
    // int DSPMax = ResourceMax[3].get<int>();
    float DSPPercent = ResourcePercent[3].get<float>();

    int MLABUtil = ResourceUtil[4].get<int>();
    // int MLABMax = ResourceMax[4].get<int>();
    float MLABPercent = ResourcePercent[4].get<float>();

    if (LUTPercent >= UtilThreshold || FFPercent >= UtilThreshold ||
        RAMPercent >= UtilThreshold || DSPPercent >= UtilThreshold ||
        MLABPercent >= UtilThreshold) {
      OutLog << "Utilization is higher than " << UtilThreshold << "%, invalid!"
             << std::endl;
      OutLog << "\tLUT: " << LUTPercent << ";\tFF: " << FFPercent
             << ";\t RAM: " << RAMPercent << ";\tDSP: " << DSPPercent
             << ";\tMLAB: " << MLABPercent << std::endl;
      OutLog.flush();
      printf("[%d:%d:%d]: Utilization is higher than %d%%, invalid!\n", Rep,
             tid, SampleCount, UtilThreshold.getValue());
      printf(
          "[%d:%d:%d]:\tLUT: %f%%;\tFF: %f%%;\tRAM: %f%%;\tDSP: %f%%;\tMLAB: "
          "%f%%\n",
          Rep, tid, SampleCount, LUTPercent, FFPercent, RAMPercent, DSPPercent,
          MLABPercent);
      OutObj.valid = 0;
    } else
      OutObj.valid = 1;

    OutObj.ExecTime = EstimatedExecTime;
    OutObj.Resources = LUTUtil + FFUtil + RAMUtil + DSPUtil + MLABUtil;

  } else {
    printf("[%d:%d:%d]: Report file can't be opened, sample point invalid!\n",
           Rep, tid, SampleCount);
    OutLog << "Report file can't be opened, sample point invalid!" << std::endl;
    OutLog.flush();
    if (!NoFork)
      exit(1);
    else
      hpvmUtils::fatalError("[" + std::to_string(tid) + "]: Terminating!");
  }
  OutLog << "Objectives: Resources: " << OutObj.Resources
         << "; ExecTime: " << OutObj.ExecTime << "; Valid: " << OutObj.valid
         << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Objectives: Resources: %d; ExecTime: %f; Valid: %d\n",
         Rep, tid, SampleCount, OutObj.Resources, OutObj.ExecTime,
         OutObj.valid);
  runDFG2LLVM_CPU(M, DFG);
  runClearDFG(M, DFG);
  hpvmUtils::writeOutputModule(M, HostModuleName);
}

static void evaluateModuleOnGPU(HMObjective &OutObj,
                                const std::string &SampleDir,
                                const std::string &ModuleName, // Old
                                int Rep, int SampleCount, llvm::Module &M,
                                BuildDFG &DFG,
                                const std::vector<DFLeafNode *> &newLeaves,
                                std::ostream &OutLog, std::mutex &TimeoutMutex,
                                bool &DontKill) {

  using namespace std::chrono;

  int tid = omp_get_thread_num();

  std::string OCLFileName = SampleDir + ModuleName + ".ll.kernels.cl";
  std::string NewKernelModuleName = SampleDir + ModuleName + ".ll.kernels.ll";
  std::string HostModuleName = SampleDir + ModuleName + ".host.ll";

  runDFG2LLVM_GPU(M, DFG, NewKernelModuleName);
  runDFG2LLVM_CPU(M, DFG);
  runClearDFG(M, DFG);
  hpvmUtils::writeOutputModule(M, HostModuleName);

  std::string Command = std::string(LLVM_BUILD_DIR_STR) + "/bin/llvm-ocl " +
                        NewKernelModuleName + " -o " + OCLFileName;
  OutLog << "Executing " << Command << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Executing %s\n", Rep, tid, SampleCount, Command.c_str());
  fflush(stdout);
  auto Start = steady_clock::now();
  int CLId = OpenChild(Command);
  int CLStatus = CloseChild(CLId);
  auto End = steady_clock::now();
  auto Duration = duration_cast<milliseconds>(End - Start);
  DEBUG_TOOL(
      printf("[%d:%d:%d]: status: %d\n", Rep, tid, SampleCount, CLStatus));
  OutLog << "status: " << CLStatus << std::endl;
  OutLog << "OCLBE Duration: " << Duration.count() << " ms" << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: OCLBE Duration: %d ms\n", Rep, tid, SampleCount,
         Duration.count());
  fflush(stdout);

  std::stringstream ComStr;
  ComStr << CustomEvaluator << " " << SampleDir << " " << Rep;
  Command = ComStr.str();
  OutLog << "Executing " << Command << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Executing %s\n", Rep, tid, SampleCount, Command.c_str());
  fflush(stdout);

  Start = steady_clock::now();
  // Only needs to be large enough to hold a number
  char OutputBuf[100];
  FILE *ChildFP = popen(Command.c_str(), "r");
  assert(fgets(OutputBuf, 100, ChildFP) != nullptr);
  int ChildStatus = pclose(ChildFP);
  End = steady_clock::now();
  Duration = duration_cast<milliseconds>(End - Start);

  if (ChildStatus == -1) {
    OutLog << "Could not close child properly" << std::endl;
    OutLog.flush();
    printf("[%d:%d:%d]: Could not close child properly\n", Rep, tid,
           SampleCount);
    exit(1);
  } else if (ChildStatus != 0) {
    OutLog << "Child terminated abnormally" << std::endl;
    OutLog.flush();
    printf("[%d:%d:%d]: Child terminated abnormally\n", Rep, tid, SampleCount);
    exit(1);
  }

  {
    std::lock_guard G(TimeoutMutex);
    DontKill = true;
  }

  DEBUG_TOOL(
      printf("[%d:%d:%d]: status: %d\n", Rep, tid, SampleCount, ChildStatus));
  OutLog << "status: " << ChildStatus << std::endl;
  OutLog << "Command Duration: " << Duration.count() << " ms" << std::endl;
  OutLog.flush();
  printf("[%d:%d:%d]: Command Duration: %d ms\n", Rep, tid, SampleCount,
         Duration.count());
  fflush(stdout);

  OutObj.ExecTime = std::strtod(OutputBuf, nullptr);
  OutObj.valid = 1;
}

HMObjective calculateObjective(std::vector<HPVMDSEParam *> &InputParams,
                               Module &M, std::vector<DFLeafNode *> Leaves,
                               int SampleCount, int Rep, std::ostream &OutLog,
                               int &CrashedPoints, double &MaxTime) {

  int tid = omp_get_thread_num();
  std::string CurrentDir = fs::current_path();
  std::string OutputDir =
      CurrentDir + "/" + OutputFoldername + "/Rep." + std::to_string(Rep) + "/";

  // Create a new folder in output folder for each sample
  std::string SampleDir =
      OutputDir + "Sample." + std::to_string(SampleCount) + "/";
  if (!fs::create_directory(SampleDir)) {
    hpvmUtils::fatalError("[" + std::to_string(tid) +
               "]: Unable to create Directory: " + SampleDir);
  }
  DEBUG_TOOL(printf("[%d:%d:%d]: SampleCount = %d\n", Rep, tid, SampleCount,
                    SampleCount));
  OutLog << "Thread " << tid << " Processing Sample " << SampleCount
         << std::endl;
  OutLog << "New directory created at: " << SampleDir << std::endl;
  OutLog.flush();
  // Prepare all the module and file names
  std::string ModuleName = M.getModuleIdentifier();
  // Extract out the path
  if (ModuleName.find("/") != std::string::npos)
    ModuleName = ModuleName.substr(ModuleName.find_last_of("/") + 1);
  if (ModuleName.find(".ll") != std::string::npos) {
    ModuleName = ModuleName.substr(0, ModuleName.find(".ll"));
  } else if (ModuleName.find(".bc") != std::string::npos) {
    ModuleName = ModuleName.substr(0, ModuleName.find(".bc"));
  }
  std::string OCLFileName = SampleDir + ModuleName + ".kernels.cl";
  std::string NewKernelModuleName = SampleDir + ModuleName + ".kernels.ll";
  std::string HostModuleName = SampleDir + ModuleName + ".host.ll";

  // Create the HM Objective as a shared memory that can be used in parent and
  // child processes
  HMObjective *Obj;
  int ShmID;
  ShmID = shmget(IPC_PRIVATE, sizeof(HMObjective), IPC_CREAT | 0666);
  if (ShmID < 0) {
    hpvmUtils::fatalError("[" + std::to_string(tid) + "]: shmget failed!\n");
  }

  Obj = (HMObjective *)shmat(ShmID, NULL, 0);
  if ((long)Obj == -1) {
    hpvmUtils::fatalError("[" + std::to_string(tid) + "]: shmat failed!");
  }

  // Fork a thraed that will attempt to compile the module with the provided
  // parameters
  pid_t pid;
  if (!NoFork)
    pid = fork();
  else
    pid = 0;
  if (pid == 0) {
    // Set process group so we can kill this process tree
    setpgid(0, 0);
    // Set up a timeout
    std::future<void> TimeoutTask;
    std::mutex TimeoutMutex;
    int ChildPid = 0;
    bool DontKill = false;
    if (!NoFork) {
      TimeoutTask = std::async(std::launch::async, [Rep, SampleCount, tid,
                                                    &OutLog, &DontKill,
                                                    &TimeoutMutex]() {
        std::this_thread::sleep_for(
            std::chrono::duration<unsigned>(TimeoutSeconds));
        OutLog << "Error: Timeout reached." << std::endl;
        OutLog.flush();
        printf("[%d:%d:%d]: Error: Timeout reached.\n", Rep, tid, SampleCount);
        fflush(stdout);

        std::lock_guard G(TimeoutMutex);
        if (!DontKill)
          kill(-getpid(), 9);
      });
    }

    auto OpenChild = [&ChildPid, &TimeoutMutex](const std::string &Command) {
      ChildPid = ForkExec(Command);
      assert(ChildPid != 0);
      if (ChildPid < 0)
        errs() << "OpenChild: ForkExec failed\n";
    };

    auto CloseChild = [&ChildPid, &TimeoutMutex]() {
      int status;
      if (waitpid(ChildPid, &status, 0) == -1) {
        errs() << "CloseChild: waitpid on child failed\n";
        exit(1);
      } else {
        ChildPid = 0;
      }
      if (!WIFEXITED(status)) {
        errs() << "CloseChild: Child exited abnormally\n";
        exit(1);
      }
      return WEXITSTATUS(status);
    };

    dumpVector(InputParams, OutLog);

    // Clear all the maps so they can be reused
    DFGMap.clear();
    TileLvlMap.clear();
    BlockSizeMap.clear();
    BufferInMap.clear();
    PrivMap.clear();
    UnrollDimMap.clear();
    UnrollLoopMap.clear();
    LoopFusionSet.clear();
    TripCountMap.clear();

    // Clone the module to keep the base module intact. This cloned module will
    // have the HPVM intrinsics, so we only need to run BuildDFG on it.
    ValueToValueMapTy VMap;
    std::unique_ptr<Module> newM = CloneModule(M, VMap);
    if (!newM.get())
      hpvmUtils::fatalError("[" + std::to_string(tid) + "]: Failed to clone module!\n");

    legacy::PassManager Passes;
    BuildDFG *newDFG = new BuildDFG();
    Passes.add(newDFG);
    Passes.run(*newM);

    std::vector<DFInternalNode *> Roots = newDFG->getRoots();

    // Get the leaf nodes in the cloned module
    LeafFinder *LFinder = new LeafFinder(
        *newM, *newDFG, TargetToHPVMTarget(::Target), Nodes.getValue());
    for (auto *Root : Roots) {
      LFinder->visit(Root);
    }
    const std::vector<DFLeafNode *> &newLeaves = LFinder->getLeaves();

    // Create a mapping between old module objects and new module objects
    for (auto *N : Leaves) {
      for (auto *N1 : newLeaves) {
        if (N->getName().equals(N1->getName())) {
          DFGMap[N] = N1;
          break;
        }
      }
    }

    // Gather and store all the paramters returned by HM
    std::vector<std::pair<DFLeafNode *, DFLeafNode *>> FusedEdges;

    // Variable for TLP
    bool EnableTLP;

    OutLog << "Printing Parameter Values:\n";
    OutLog.flush();
    for (auto *HPVMParam : InputParams) {
      OutLog << *HPVMParam << std::endl;
      OutLog.flush();
      DFNode *NOriginal = HPVMParam->getNode();
      DFNode *N = NULL;
      if (NOriginal != nullptr)
        N = DFGMap[dyn_cast<DFLeafNode>(NOriginal)];

      HPVMParamType ParamTy = HPVMParam->getType();
      switch (ParamTy) {
      case HPVMParamType::TilingParam: {
        TilingDSEParam *TilingParam = static_cast<TilingDSEParam *>(HPVMParam);
        unsigned Dim = TilingParam->getDim();
        int TileSize = TilingParam->getVal();
        BlockSizeMap[N][Dim] = TileSize;
        break;
      }
      case HPVMParamType::TileLvlParam: {
        TileLvlDSEParam *TileLvlParam =
            static_cast<TileLvlDSEParam *>(HPVMParam);
        unsigned Lvl = TileLvlParam->getVal();
        TileLvlMap[N] = Lvl;
        break;
      }
      case HPVMParamType::InBuffParam: {
        BufferingDSEParam *InBuffParam =
            static_cast<BufferingDSEParam *>(HPVMParam);

        unsigned ArgNum = InBuffParam->getArgNum();
        unsigned BuffSize = InBuffParam->getBuffSize();
        int ToBuffer = InBuffParam->getVal();
        if (ToBuffer)
          BufferInMap[N][ArgNum] = BuffSize;

        break;
      }
      case HPVMParamType::PrivParam: {
        BufferingDSEParam *PrivParam =
            static_cast<BufferingDSEParam *>(HPVMParam);

        unsigned ArgNum = PrivParam->getArgNum();
        unsigned BuffSize = PrivParam->getBuffSize();
        int ToPriv = PrivParam->getVal();
        if (ToPriv)
          PrivMap[N][ArgNum] = BuffSize;

        break;
      }
      case HPVMParamType::UnrollParam: {
        UnrollDSEParam *UnrollParam = static_cast<UnrollDSEParam *>(HPVMParam);

        int UnrollFactor = UnrollParam->getVal();
        UnrollDSEParam::UnrollType Type = UnrollParam->getType();
        switch (Type) {
        case UnrollDSEParam::UnrollType::ReplTy: {
          UnrollDimParam *UnrollDimP =
              static_cast<UnrollDimParam *>(UnrollParam);
          int Dim = UnrollDimP->getDim();
          UnrollDimInfo UDI;
          UDI.Dim = Dim;
          UDI.UnrollFactor = UnrollFactor;
          UDI.NodeDims = N->getNumOfDim();
          UnrollDimMap[N].push_back(UDI);
          break;
        }
        case UnrollDSEParam::UnrollType::LoopTy: {
          UnrollLoopParam *UnrollLoopP =
              static_cast<UnrollLoopParam *>(UnrollParam);
          BasicBlock *HeaderOriginal = UnrollLoopP->getLoopHeader();
          Function *FOriginal = UnrollLoopP->getF();
          BasicBlock *Header = dyn_cast<BasicBlock>(VMap[HeaderOriginal]);
          Function *F = dyn_cast<Function>(VMap[FOriginal]);
          UnrollLoopMap[F].push_back({Header, UnrollFactor});
          break;
        }
        }
        break;
      }
      case HPVMParamType::LoopFusionParam: {
        unsigned Value = HPVMParam->getVal();
        if (Value == 1)
          LoopFusionSet.insert(N);
        break;
      }
      case HPVMParamType::NodeFusionParam: {
        NodeFusionDSEParam *p = static_cast<NodeFusionDSEParam *>(HPVMParam);
        if (p->getVal() == 1)
          FusedEdges.push_back(std::make_pair(p->getN1(), p->getN2()));
        break;
      }
      case HPVMParamType::TLPParam: {
        assert(N == nullptr);
        unsigned Value = HPVMParam->getVal();
        assert((Value == 0 || Value == 1) && "TLP Param is wrong!");
        EnableTLP = Value;
        break;
      }

      default:
        hpvmUtils::fatalError("[" + std::to_string(tid) + "]: Unhandled Parameter Type!");
      }
    }

    auto start = std::chrono::steady_clock::now();
    OutLog << "\tFusing nodes..." << std::endl;
    OutLog.flush();
    printf("[%d:%d:%d]: \tFusing nodes...\n", Rep, tid, SampleCount);
    ParamMaps PM = fuseEdges(Leaves, FusedEdges, SampleCount, Rep, OutLog);

    {
      auto end = std::chrono::steady_clock::now();
      auto duration =
          std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
      OutLog << "\t\tDuration: " << duration.count() << " ms" << std::endl;
      OutLog.flush();
      printf("[%d:%d:%d]: \t\tDuration: %d ms\n", Rep, tid, SampleCount,
             duration.count());
      fflush(stdout);
    }

    for (auto *NewLeaf : PM.Members) {
      OptimizeLeaf(PM, NewLeaf, VMap, OutLog, SampleCount, Rep);
    }
    auto end = std::chrono::steady_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    OutLog << "Optimizations Duration: " << duration.count() << " ms"
           << std::endl;
    OutLog.flush();
    printf("[%d:%d:%d]: Optimizations Duration: %d ms\n", Rep, tid, SampleCount,
           duration.count());

    fflush(stdout);

    hpvmUtils::writeOutputModule(*newM, SampleDir + ModuleName + ".opt.ll");

    if (::Target == fpga) {
      evaluateModuleWithAOC(*Obj, SampleDir, ModuleName, Rep, SampleCount,
                            *newM, *newDFG, newLeaves, EnableTLP, OutLog,
                            TimeoutMutex, DontKill);
    } else if (::Target == gpu) {
      evaluateModuleOnGPU(*Obj, SampleDir, ModuleName, Rep, SampleCount, *newM,
                          *newDFG, newLeaves, OutLog, TimeoutMutex, DontKill);
    }
    delete LFinder;
    if (!NoFork)
      exit(0);
  }

  int status;
  if (!NoFork) {
    waitpid(pid, &status, 0);
    if (!WIFEXITED(status) || status != 0) {
      OutLog << "Evaluation crashed, returning invalid point!" << std::endl;
      OutLog.flush();
      printf("[%d:%d:%d]: Evaluation crashed, returning invalid point!\n", Rep,
             tid, SampleCount);
      // TODO: pick correct values for crashing point!
      Obj->valid = 0;
      Obj->ExecTime = MaxTime;
      Obj->Resources = 0;
      CrashedPoints++;
    } else {
      MaxTime = std::max(Obj->ExecTime, MaxTime);
    }
  }

  OutLog << "Main thread received: Sample: " << SampleCount
         << ": Resources: " << Obj->Resources << "; ExecTime: " << Obj->ExecTime
         << "; Valid: " << Obj->valid << std::endl;
  printf("[%d:%d:%d]: Main thread received: Sample: %d: Resources: %d; "
         "ExecTime: %f; "
         "Valid: %d\n",
         Rep, tid, SampleCount, SampleCount, Obj->Resources, Obj->ExecTime,
         Obj->valid);
  OutLog.flush();
  fflush(stdout);

  HMObjective ObjReturn = *Obj;
  shmdt((void *)Obj);
  shmctl(ShmID, IPC_RMID, NULL);
  return ObjReturn;
}
bool leafNeedsLFParam(DFNode *leaf) {
  Function *F = leaf->getFuncPointer();
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  int NumLoops = LI.getLoopsInPreorder().size();
  // Create an LF Parameter if the leaf has no dynamic replication && there are
  // more than one loops in the node function, or if leaf has 1D dynamic
  // replication and there are one or more loops in the node function, or if
  // leaf has > 1D dynamic replication.
  unsigned NumDims = leaf->getNumOfDim();
  auto DimLimits = leaf->getDimLimits();
  if (NumDims == 0) {
    if (NumLoops > 1)
      return true;
  } else if (NumDims == 1) {
    Value *DimSize = DimLimits[0];
    if (ConstantInt *ConstDimSize = dyn_cast<ConstantInt>(DimSize)) {
      if (ConstDimSize->getSExtValue() == 1) {
        if (NumLoops > 1)
          return true;
      } else {
        if (NumLoops > 0)
          return true;
      }
    } else {
      if (NumLoops > 0)
        return true;
    }
  } else {
    return true;
  }
  return false;
}
int getLoopFusionParam(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                       std::vector<HPVMDSEParam *> &HPVMParams) {

  // Function *F = leaf->getFuncPointer();
  // FunctionAnalysisManager FAM;
  // PassBuilder PB;
  // PB.registerFunctionAnalyses(FAM);
  // auto &LI = FAM.getResult<LoopAnalysis>(*F);
  // int NumLoops = LI.getLoopsInPreorder().size();
  // // Create an LF Parameter if the leaf has no dynamic replication && there
  // // are more than one loops in the node function, or if leaf has 1D dynamic
  // // replication and there are one or more loops in the node function, or if
  // // leaf has > 1D dynamic replication.
  // bool CreateLFParam = false;
  // unsigned NumDims = leaf->getNumOfDim();
  // auto DimLimits = leaf->getDimLimits();
  // if (NumDims == 0) {
  //   if (NumLoops > 1)
  //     CreateLFParam = true;
  // } else if (NumDims == 1) {
  //   Value *DimSize = DimLimits[0];
  //   if (ConstantInt *ConstDimSize = dyn_cast<ConstantInt>(DimSize)) {
  //     if (ConstDimSize->getSExtValue() == 1) {
  //       if (NumLoops > 1)
  //         CreateLFParam = true;
  //     } else {
  //       if (NumLoops > 0)
  //         CreateLFParam = true;
  //     }
  //   } else {
  //     if (NumLoops > 0)
  //       CreateLFParam = true;
  //   }
  // } else {
  //   CreateLFParam = true;
  // }

  if (leafNeedsLFParam(leaf)) {
    std::string ParamName = leaf->getName().str() + "_LF";
    HMInputParam *NewParam =
        new HMInputParam(ParamName, ParamType::Categorical);
    HPVMDSEParam *NewIntParam =
        new HPVMDSEParam(leaf, NewParam, HPVMParamType::LoopFusionParam);
    HPVMParams.push_back(NewIntParam);
    NewParam->setHPVMParam(NewIntParam);
    std::vector<int> Vals = {0, 1};
    NewParam->setVals(Vals);
    InParams.push_back(NewParam);
    NewParam->setDefault(0);
    return 1;
  }
  return 0;
}

int getGlobalLFParam(const std::vector<DFLeafNode *> &Leaves,
                     std::vector<HMInputParam *> &InParams,
                     std::vector<HPVMDSEParam *> &HPVMParams) {
  // Create an LF Parameter if the leaf has no dynamic replication && there are
  // more than one loops in the node function, or if leaf has 1D dynamic
  // replication and there are one or more loops in the node function, or if
  // leaf has > 1D dynamic replication.
  bool CreateLFParam = false;
  for (auto *leaf : Leaves) {
    if (leafNeedsLFParam(leaf)) {
      CreateLFParam = true;
      break;
    }
  }

  if (CreateLFParam) {
    std::string ParamName = "Global_LF";
    HMInputParam *NewParam =
        new HMInputParam(ParamName, ParamType::Categorical);
    HPVMDSEParam *NewIntParam = new HPVMDSEParam(
        nullptr, NewParam, HPVMParamType::LoopFusionParam, 0, true);
    HPVMParams.push_back(NewIntParam);
    NewParam->setHPVMParam(NewIntParam);
    std::vector<int> Vals = {0, 1};
    NewParam->setVals(Vals);
    InParams.push_back(NewParam);
    NewParam->setDefault(0);
    return 1;
  }
  return 0;
}

int getTilingLevelParam(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                        std::vector<HPVMDSEParam *> &HPVMParams) {
  std::string ParamName = leaf->getName().str() + "_TLvl";
  HMInputParam *NewParam = new HMInputParam(ParamName, ParamType::Ordinal);
  TileLvlDSEParam *NewIntParam =
      new TileLvlDSEParam(leaf, NewParam, HPVMParamType::TileLvlParam);
  HPVMParams.push_back(NewIntParam);
  NewParam->setHPVMParam(NewIntParam);
  std::vector<int> TileLvlVals;
  for (uint i = 0; i <= leaf->getNumOfDim(); ++i) {
    if (i < 3)
      TileLvlVals.push_back(i);
  }
  NewParam->setVals(TileLvlVals);
  InParams.push_back(NewParam);
  NewParam->setDefault(0);
  return 1;
}

int getTileSizeParam(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                     std::vector<HPVMDSEParam *> &HPVMParams, int i) {
  int DimIndex = leaf->getNumOfDim() - 1 - i;
  Value *Dim = leaf->getDimLimits()[DimIndex];
  std::string ParamName =
      leaf->getName().str() + "_TSz_D" + std::to_string(DimIndex);
  HMInputParam *NewParam = new HMInputParam(ParamName);
  TilingDSEParam *NewTilingParam = new TilingDSEParam(leaf, NewParam, DimIndex);
  HPVMParams.push_back(NewTilingParam);
  NewParam->setHPVMParam(NewTilingParam);
  std::vector<int> Range;
  if (ConstantInt *ConstDim = dyn_cast<ConstantInt>(Dim)) {
    unsigned DimVal = ConstDim->getZExtValue();
    unsigned tile_size = 2;
    do {
      Range.push_back(tile_size);
      tile_size += rand() % 10;
    } while (tile_size < DimVal / 2);
  } else {
    unsigned tile_size = 2;
    do {
      Range.push_back(tile_size);
      tile_size += rand() % 10;
    } while (tile_size < MaxTileSize);
  }
  NewParam->setVals(Range);
  NewParam->setType(ParamType::Ordinal);
  InParams.push_back(NewParam);
  NewParam->setDefault(
      2); // TODO: what is the right default value for tile size?
  return 1;
}

int isReadOnly(Value *Arg, Module *M) {
  bool unhandled = 0;
  int storeUsers = 0;
  int loadUsers = 0;

  Function *IVDepF = Intrinsic::getDeclaration(M, Intrinsic::hpvm_ivdep);

  // Find argument where all the uses are loads, i.e. no stores
  for (auto *u : Arg->users()) {
    if (GetElementPtrInst *GEPI = dyn_cast<GetElementPtrInst>(u)) {
      for (auto *uu : GEPI->users()) {
        if (StoreInst *SI = dyn_cast<StoreInst>(uu)) {
          storeUsers++;
        } else if (LoadInst *LI = dyn_cast<LoadInst>(uu)) {
          loadUsers++;
        } else {
          if (BitCastInst *BI = dyn_cast<BitCastInst>(uu)) {
            for (auto *biu : BI->users()) {
              if (StoreInst *SI = dyn_cast<StoreInst>(biu)) {
                storeUsers++;
              } else if (LoadInst *LI = dyn_cast<LoadInst>(biu)) {
                loadUsers++;
              } else {
                unhandled = 1;
              }
            }
          } else {
            unhandled = 1;
          }
        }
      }
    } else if (auto *Call = dyn_cast<CallInst>(u)) {
      // Check for intrinsics that don't change the pointer
      if (Call->getCalledFunction() == IVDepF)
        continue;
      unhandled = 1;
    } else {
      unhandled = 1;
    }
  }
  if (unhandled > 0) {
    return 3;
  } else if (storeUsers > 0) {
    return 2;
  } else if (loadUsers == 0) {
    return 0;
  } else
    return 1;
}

// We go through all the arguments of the leaf node function, and for all the
// pointer arguments that are read only (only contain loads), we check if
// their size is a constant, and if it is we add that argument along with its
// size as a candidate for input buffering.
int getInputBufferingParams(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                            std::vector<HPVMDSEParam *> &HPVMParams) {
  Function *F = leaf->getFuncPointer();
  Module *M = F->getParent();
  DataLayout DL = M->getDataLayout();
  int argnum = 0;
  int numParams = 0;
  for (auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
    Value *Arg = &*ai;
    // We only care about pointer arguments
    if (Arg->getType()->isPointerTy()) {
      if (isReadOnly(Arg, M) == 1) {
        Value *ArgSize = findArraySize(argnum, leaf);
        if (ArgSize == nullptr)
          continue;
        if (ConstantInt *ArgSizeConstInt = dyn_cast<ConstantInt>(ArgSize)) {
          unsigned ArgTypeSize =
              DL.getTypeSizeInBits(Arg->getType()->getPointerElementType()) / 8;
          unsigned numArrayElements =
              ArgSizeConstInt->getZExtValue() / ArgTypeSize;

          std::string ParamName =
              leaf->getName().str() + "_" + Arg->getName().str() + "_BuffIn";
          HMInputParam *NewParam =
              new HMInputParam(ParamName, ParamType::Categorical);
          BufferingDSEParam *NewIntParam =
              new BufferingDSEParam(argnum, numArrayElements, leaf, NewParam,
                                    HPVMParamType::InBuffParam);
          HPVMParams.push_back(NewIntParam);
          NewParam->setHPVMParam(NewIntParam);
          std::vector<int> InBuffRange = {0, 1};
          NewParam->setVals(InBuffRange);
          InParams.push_back(NewParam);
          NewParam->setDefault(0);
          numParams++;
        }
      }
    }
    argnum++;
  }

  return numParams;
}
int getGlobalIBParam(const std::vector<DFLeafNode *> &Leaves,
                     std::vector<HMInputParam *> &InParams,
                     std::vector<HPVMDSEParam *> &HPVMParams) {
  bool createIBParam = false;
  for (auto *leaf : Leaves) {
    Function *F = leaf->getFuncPointer();
    Module *M = F->getParent();
    DataLayout DL = M->getDataLayout();
    int argnum = 0;
    for (auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
      Value *Arg = &*ai;
      // We only care about pointer arguments
      if (Arg->getType()->isPointerTy()) {
        if (isReadOnly(Arg, M) == 1) {
          Value *ArgSize = findArraySize(argnum, leaf);
          if (ArgSize == nullptr)
            continue;
          if (ConstantInt *ArgSizeConstInt = dyn_cast<ConstantInt>(ArgSize)) {
            createIBParam = true;
            break;
          }
        }
      }
      argnum++;
    }
    if (createIBParam)
      break;
  }
  if (createIBParam) {
    std::string ParamName = "Global_BI";
    HMInputParam *NewParam =
        new HMInputParam(ParamName, ParamType::Categorical);
    BufferingDSEParam *NewIntParam = new BufferingDSEParam(
        0, 0, nullptr, NewParam, HPVMParamType::InBuffParam, 0, true);
    HPVMParams.push_back(NewIntParam);
    NewParam->setHPVMParam(NewIntParam);
    std::vector<int> InBuffRange = {0, 1};
    NewParam->setVals(InBuffRange);
    InParams.push_back(NewParam);
    NewParam->setDefault(0);

    return 1;
  }
  return 0;
}

int getGlobalAPParam(const std::vector<DFLeafNode *> &Leaves,
                     std::vector<HMInputParam *> &InParams,
                     std::vector<HPVMDSEParam *> &HPVMParams) {
  bool CreateAPParam = false;
  for (auto *leaf : Leaves) {
    Function *F = leaf->getFuncPointer();
    // Module *M = F->getParent();
    for (auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
      Value *Arg = &*ai;
      if (Arg->getType()->isPointerTy()) {
        if (F->getAttributes().hasAttribute(ai->getArgNo() + 1,
                                            Attribute::Priv)) {
          CreateAPParam = true;
          break;
        }
      }
    }
    if (CreateAPParam)
      break;
  }
  if (CreateAPParam) {
    std::string ParamName = "Global_AP";
    HMInputParam *NewParam =
        new HMInputParam(ParamName, ParamType::Categorical);
    BufferingDSEParam *NewIntParam = new BufferingDSEParam(
        0, 0, nullptr, NewParam, HPVMParamType::PrivParam, 0, true);
    HPVMParams.push_back(NewIntParam);
    NewParam->setHPVMParam(NewIntParam);
    std::vector<int> PrivRange = {0, 1};
    NewParam->setVals(PrivRange);
    InParams.push_back(NewParam);
    NewParam->setDefault(0);

    return 1;
  }
  return 0;
}
// Go through all the
int getPrivParams(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                  std::vector<HPVMDSEParam *> &HPVMParams) {
  Function *F = leaf->getFuncPointer();
  // Module *M = F->getParent();
  int argnum = 0;
  int numParams = 0;
  for (auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
    Value *Arg = &*ai;
    if (Arg->getType()->isPointerTy()) {
      if (F->getAttributes().hasAttribute(ai->getArgNo() + 1,
                                          Attribute::Priv)) {

        // Get buffer size from attribute
        unsigned numArrayElements =
            F->getAttribute(ai->getArgNo() + 1, Attribute::Priv).getBuffSize();

        std::string ParamName =
            leaf->getName().str() + "_" + Arg->getName().str() + "_Priv";
        HMInputParam *NewParam =
            new HMInputParam(ParamName, ParamType::Categorical);
        BufferingDSEParam *NewIntParam = new BufferingDSEParam(
            argnum, numArrayElements, leaf, NewParam, HPVMParamType::PrivParam);
        HPVMParams.push_back(NewIntParam);
        NewParam->setHPVMParam(NewIntParam);
        std::vector<int> PrivRange = {0, 1};
        NewParam->setVals(PrivRange);
        InParams.push_back(NewParam);
        NewParam->setDefault(0);
        numParams++;
      }
    }
    argnum++;
  }
  return numParams;
}

int getDimUnrollParam(Value *Dim, int i, DFNode *leaf,
                      std::vector<HMInputParam *> &InParams,
                      std::vector<HPVMDSEParam *> &HPVMParams) {

  int DimIndex = i;

  auto createUnrollDimParam = [leaf, DimIndex, &InParams,
                               &HPVMParams](unsigned TC) {
    std::string ParamName =
        leaf->getName().str() + "_UnrollDim" + std::to_string(DimIndex);
    HMInputParam *NewParam = new HMInputParam(ParamName);
    UnrollDimParam *NewUnrollParam =
        new UnrollDimParam(DimIndex, leaf->getFuncPointer(), leaf, NewParam);
    HPVMParams.push_back(NewUnrollParam);
    NewParam->setHPVMParam(NewUnrollParam);
    std::vector<int> Range; // = {3};
    Range.push_back(1);
    unsigned NumUFOptions = 1; // Limit number of unroll factor options to 3
    for (uint j = 2; j <= TC && j <= MaxDimUnrollFactor; ++j) {
      if (TC % j == 0) {
        Range.push_back(j);
        NumUFOptions++;
        if (NumUFOptions == MaxDimUFOptions)
          break;
      }
    }
    NewParam->setVals(Range);
    NewParam->setType(ParamType::Ordinal);
    InParams.push_back(NewParam);
    NewParam->setDefault(1);
  };
  if (ConstantInt *ConstDim = dyn_cast<ConstantInt>(Dim)) {
    unsigned DimVal = ConstDim->getZExtValue();
    if (DimVal <= 1) {
      return 0;
    }
    createUnrollDimParam(DimVal);
    return 1;
  } else {
    Intrinsic::ID ID =
        (DimIndex == 0)
            ? Intrinsic::hpvm_getNodeInstanceID_x
            : ((DimIndex == 1) ? Intrinsic::hpvm_getNodeInstanceID_y
                               : Intrinsic::hpvm_getNodeInstanceID_z);
    Function *F = leaf->getFuncPointer();
    Function *GetNodeInstanceIDFunc =
        Intrinsic::getDeclaration(F->getParent(), ID);
    for (auto *U : GetNodeInstanceIDFunc->users()) {
      IntrinsicInst *GetNodeInstanceIdII = dyn_cast<IntrinsicInst>(U);
      assert(GetNodeInstanceIdII && "Expecting user to be an IntrisicInst!");
      if (GetNodeInstanceIdII->getParent()->getParent() != F)
        continue;
      Value *GetNode = GetNodeInstanceIdII->getArgOperand(0);
      IntrinsicInst *GetNodeII = dyn_cast<IntrinsicInst>(GetNode);
      assert(GetNodeII &&
             "Something is not right with the getNumNodeInstances call!");
      if (GetNodeII->getIntrinsicID() != Intrinsic::hpvm_getNode)
        continue;
      for (auto *UU : GetNodeInstanceIdII->users()) {
        if (IntrinsicInst *NZII = dyn_cast<IntrinsicInst>(UU)) {
          if (NZII->getIntrinsicID() == Intrinsic::hpvm_nz_loop) {
            unsigned DimTripCount =
                dyn_cast<ConstantInt>(NZII->getOperand(1))->getZExtValue();
            createUnrollDimParam(DimTripCount);
            return 1;
          }
        }
      }
    }
  }
  return 0;
}

int getGlobalLUParam(std::vector<HMInputParam *> &InParams,
                     std::vector<HPVMDSEParam *> &HPVMParams) {
  std::string ParamName = "Global_LU";
  HMInputParam *NewParam = new HMInputParam(ParamName);
  UnrollLoopParam *NewUnrollParam = new UnrollLoopParam(
      nullptr, nullptr, nullptr, NewParam, /*UF*/ 0, /*Global*/ true);
  HPVMParams.push_back(NewUnrollParam);
  NewParam->setHPVMParam(NewUnrollParam);
  std::vector<int> Vals;
  Vals.push_back(1);
  unsigned NumUFOptions = 1;
  for (uint j = 2; j <= MaxUnrollFactor; ++j) {
    Vals.push_back(j);
    NumUFOptions++;
    if (NumUFOptions == MaxUFOptions)
      break;
  }
  NewParam->setVals(Vals);
  NewParam->setType(ParamType::Ordinal);
  InParams.push_back(NewParam);
  NewParam->setDefault(1);
  return 1;
}
int getLoopUnrollParam(Loop *L, int i, ScalarEvolution &SE, DFNode *leaf,
                       Function *F, std::vector<HMInputParam *> &InParams,
                       std::vector<HPVMDSEParam *> &HPVMParams,
                       std::map<llvm::BasicBlock *, int> &TripCountMap) {
  auto createLoopUnrollParam = [L, i, leaf, F, &HPVMParams, &InParams,
                                &TripCountMap](unsigned TripCount) {
    std::string FName =
        (!leaf->getName().equals(F->getName()) ? "_" + F->getName().str() : "");
    std::string ParamName =
        leaf->getName().str() + FName + "_UnrollL" + std::to_string(i);
    HMInputParam *NewParam = new HMInputParam(ParamName);
    UnrollLoopParam *NewUnrollParam =
        new UnrollLoopParam(L->getHeader(), F, leaf, NewParam);
    HPVMParams.push_back(NewUnrollParam);
    NewParam->setHPVMParam(NewUnrollParam);
    std::vector<int> Vals;
    Vals.push_back(1);
    unsigned NumUFOptions = 1;
    for (uint j = 2; j <= TripCount && j <= MaxUnrollFactor; ++j) {
      if (TripCount % j == 0) {
        Vals.push_back(j);
        NumUFOptions++;
        if (NumUFOptions == MaxUFOptions)
          break;
      }
    }
    NewParam->setVals(Vals);
    NewParam->setType(ParamType::Ordinal);
    InParams.push_back(NewParam);
    NewParam->setDefault(1);
  };
  Optional<Loop::LoopBounds> OptionalLB = L->getBounds(SE);
  if (OptionalLB.hasValue()) {
    Loop::LoopBounds LB = OptionalLB.getValue();
    Value *FinalValue = &LB.getFinalIVValue();
    Value *InitialValue = &LB.getInitialIVValue();
    if (ConstantInt *ConstValue = dyn_cast<ConstantInt>(FinalValue)) {
      if (ConstantInt *ConstInitialValue =
              dyn_cast<ConstantInt>(InitialValue)) {
        int FinalVal = ConstValue->getSExtValue();
        int InitialVal = ConstInitialValue->getSExtValue();
        unsigned TripCount = abs(FinalVal - InitialVal);
        createLoopUnrollParam(TripCount);
        return 1;
      }
    }
  }
  // Couldn't find a constant loop bound, check in tripcountmap
  BasicBlock *HeaderBB = L->getHeader();
  auto it = TripCountMap.find(HeaderBB);
  if (it != TripCountMap.end()) {
    unsigned TripCount = it->second;
    createLoopUnrollParam(TripCount);
    return 1;
  }
  return 0;
}

int getCalledFuncUnrollParams(DFNode *leaf, Function *F,
                              std::vector<HMInputParam *> &InParams,
                              std::vector<HPVMDSEParam *> &HPVMParams) {
  int numParams = 0;
  for (auto IB = inst_begin(F); IB != inst_end(F); ++IB) {
    Instruction *I = &*IB;
    if (CallInst *CI = dyn_cast<CallInst>(I)) {
      Function *CalledF = CI->getCalledFunction();
      if (!CalledF->isDeclaration()) {
        NodeToCalledF[leaf].push_back(CalledF);
        FunctionAnalysisManager FAM;
        PassBuilder PB;
        PB.registerFunctionAnalyses(FAM);
        auto &CalledSE = FAM.getResult<ScalarEvolutionAnalysis>(*CalledF);
        auto &CalledLI = FAM.getResult<LoopAnalysis>(*CalledF);
        // We need to canonicalize the loops to make sure each loop has its
        // own header
        FunctionPassManager FPM;
        FPM.addPass(LoopSimplifyPass());
        FPM.run(*CalledF, FAM);
        auto TripCountMap = getFuncLoopTripCounts(CalledF, 1);
        int i = 0;
        for (auto *LIB : CalledLI.getLoopsInPreorder()) {
          Loop *L = &*LIB;
          numParams += getLoopUnrollParam(L, i, CalledSE, leaf, CalledF,
                                          InParams, HPVMParams, TripCountMap);
          i++;
        }

        numParams +=
            getCalledFuncUnrollParams(leaf, CalledF, InParams, HPVMParams);
      }
    }
  }
  return numParams;
}

int getUnrollParams(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                    std::vector<HPVMDSEParam *> &HPVMParams) {
  int numParams = 0;
  std::vector<Value *> Dims = leaf->getDimLimits();

  Function *F = leaf->getFuncPointer();

  // First for Repl Factors
  if (shouldSequentializeTarget(::Target)) {
    int i = 0;
    for (auto *Dim : Dims) {
      numParams += getDimUnrollParam(Dim, i++, leaf, InParams, HPVMParams);
    }
  }

  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  // We need to canonicalize the loops to make sure each loop has its own
  // header
  FunctionPassManager FPM;
  // FPM.addPass(EarlyCSEPass(false));
  // FPM.addPass(SimplifyCFGPass());
  FPM.addPass(LoopSimplifyPass());
  FPM.run(*F, FAM);
  // Aquire any marked dynamic loop bounds
  auto TripCountMap = getFuncLoopTripCounts(F, 1);

  int i = 0;
  for (auto *LIB : LI.getLoopsInPreorder()) {
    Loop *L = &*LIB;
    numParams += getLoopUnrollParam(L, i, SE, leaf, F, InParams, HPVMParams,
                                    TripCountMap);
    i++;
  }
  numParams += getCalledFuncUnrollParams(leaf, F, InParams, HPVMParams);
  return numParams;
}

int getTileSizeParams(DFNode *leaf, std::vector<HMInputParam *> &InParams,
                      std::vector<HPVMDSEParam *> &HPVMParams) {
  int numParams = 0;
  for (uint i = 0; i < leaf->getNumOfDim(); ++i) {
    numParams += getTileSizeParam(leaf, InParams, HPVMParams, i);
  }
  return numParams;
}

static int getNodeFusionParams(const std::vector<DFLeafNode *> &Leaves,
                               DFLeafNode *N1,
                               std::vector<HMInputParam *> &InParams,
                               std::vector<HPVMDSEParam *> &HPVMParams) {
  int num = 0;
  for (auto it = N1->successors_begin(), e = N1->successors_end(); it != e;
       ++it) {
    if (DFLeafNode *N2 = dyn_cast<DFLeafNode>(*it)) {
      if (std::find(Leaves.begin(), Leaves.end(), N2) == Leaves.end())
        continue;
      DFGTransform::Context TContext(*(N1->getFuncPointer()->getParent()),
                                     N1->getFuncPointer()->getContext());
      if (!Fuse(TContext, {N1, N2}).canRun())
        continue;

      std::string ParamName =
          N1->getName().str() + "_Nd_F_" + N2->getName().str();
      HMInputParam *NewParam =
          new HMInputParam(ParamName, ParamType::Categorical);
      NodeFusionDSEParam *NFParam = new NodeFusionDSEParam(N1, N2, NewParam);
      NewParam->setHPVMParam(NFParam);
      NewParam->setVals({0, 1});
      InParams.push_back(NewParam);
      HPVMParams.push_back(NFParam);
      NewParam->setDefault(0);
      num++;
    }
  }
  return num;
}

static int getGlobalNFParams(const std::vector<DFLeafNode *> &Leaves,
                             std::vector<HMInputParam *> &InParams,
                             std::vector<HPVMDSEParam *> &HPVMParams) {
  bool CreateNFParam = false;
  for (auto *N1 : Leaves) {
    for (auto it = N1->successors_begin(), e = N1->successors_end(); it != e;
         ++it) {
      if (DFLeafNode *N2 = dyn_cast<DFLeafNode>(*it)) {
        if (std::find(Leaves.begin(), Leaves.end(), N2) == Leaves.end())
          continue;
        DFGTransform::Context TContext(*(N1->getFuncPointer()->getParent()),
                                       N1->getFuncPointer()->getContext());
        if (Fuse(TContext, {N1, N2}).canRun()) {
          CreateNFParam = true;
          break;
        }
      }
    }
    if (CreateNFParam)
      break;
  }
  if (CreateNFParam) {
    std::string ParamName = "Global_NF";
    HMInputParam *NewParam =
        new HMInputParam(ParamName, ParamType::Categorical);
    NodeFusionDSEParam *NFParam =
        new NodeFusionDSEParam(nullptr, nullptr, NewParam, 0, true);
    NewParam->setHPVMParam(NFParam);
    NewParam->setVals({0, 1});
    InParams.push_back(NewParam);
    HPVMParams.push_back(NFParam);
    NewParam->setDefault(0);
    return 1;
  }
  return 0;
}

static int getTLPParam(std::vector<DFLeafNode *> const &Leaves,
                       std::vector<HMInputParam *> &InParams,
                       std::vector<HPVMDSEParam *> &HPVMParams) {
  for (auto LI = Leaves.begin(); LI != Leaves.end(); ++LI) {
    DFLeafNode *Leaf1 = *LI;
    for (auto LJ = LI + 1; LJ != Leaves.end(); ++LJ) {
      DFLeafNode *Leaf2 = *LJ;
      if (!existsForwardPath(Leaf1, Leaf2) &&
          !existsForwardPath(Leaf2, Leaf1)) {
        LLVM_DEBUG(errs() << "Found parallel nodes, creating TLP param!\n");
        std::string ParamName = "TLP";
        HMInputParam *NewParam =
            new HMInputParam(ParamName, ParamType::Categorical);
        HPVMDSEParam *NewIntParam =
            new HPVMDSEParam(nullptr, NewParam, HPVMParamType::TLPParam);
        NewParam->setHPVMParam(NewIntParam);
        NewParam->setVals({0, 1});
        HPVMParams.push_back(NewIntParam);
        InParams.push_back(NewParam);
        NewParam->setDefault(0);
        return 1;
      }
    }
  }
  return 0;
}

// Function that will gather the input parameters
int collectInputParams(std::vector<DFLeafNode *> const &Leaves,
                       std::vector<HMInputParam *> &InParams,
                       std::vector<HPVMDSEParam *> &HPVMParams, bool Global) {
  bool EnableIB = 0;
  bool EnableAP = 0;
  bool EnableLU = 0;
  bool EnableLF = 0;
  bool EnableNT = 0;
  bool EnableNF = 0;
  bool EnableTLP = 0;

  // Check which opts are enabled.
  // If no opts provided in cmdline, enable all opts
  if (OptimizationList.size() == 0) {
    switch (::Target) {
    case fpga:
      EnableIB = EnableAP = EnableLU = EnableLF = EnableNT = 1;
      if (!NoMultikernel) {
        EnableNF = 1;
        EnableTLP = 1;
      }
      break;
    case gpu:
      EnableLU = EnableLF = EnableNT = EnableAP = 1;
      if (!NoMultikernel) {
        EnableNF = 1;
        EnableNT = 1;
      }
    }
  } else {
    for (auto &Opt : OptimizationList) {
      switch (Opt) {
      case bufferin:
        EnableIB = 1;
        break;
      case argpriv:
        EnableAP = 1;
        break;
      case lunroll:
        EnableLU = 1;
        break;
      case lfusion:
        EnableLF = 1;
        break;
      case tiling:
        EnableNT = 1;
        break;
      case nfusion:
        EnableNF = 1;
        break;
      case tlp:
        EnableTLP = 1;
        break;
      }
    }
  }
  // Gather params for every leaf node.
  int numParams = 0;
  if (EnableTLP)
    numParams += getTLPParam(Leaves, InParams, HPVMParams);
  if (Global) {
    if (EnableIB)
      numParams += getGlobalIBParam(Leaves, InParams, HPVMParams);
    if (EnableAP)
      numParams += getGlobalAPParam(Leaves, InParams, HPVMParams);
    if (EnableLU)
      numParams += getGlobalLUParam(InParams, HPVMParams);
    if (EnableLF)
      numParams += getGlobalLFParam(Leaves, InParams, HPVMParams);
    if (EnableNF)
      numParams += getGlobalNFParams(Leaves, InParams, HPVMParams);
  } else {
    for (auto *leaf : Leaves) {
      // Create Input Buffering Parameter
      if (EnableIB)
        numParams += getInputBufferingParams(leaf, InParams, HPVMParams);

      // Create Argument Privatization Parameter
      if (EnableAP)
        numParams += getPrivParams(leaf, InParams, HPVMParams);

      // Create Loop Unroll Parameters
      if (EnableLU)
        numParams += getUnrollParams(leaf, InParams, HPVMParams);

      // Create Loop Fusion Parameters
      if (EnableLF)
        numParams += getLoopFusionParam(leaf, InParams, HPVMParams);

      // if (EnableNT) {
      ////// Create Tiling Level Parameter
      ////numParams += getTilingLevelParam(leaf, InParams, HPVMParams);
      ////
      ////// Create Tile Size Parameters
      ////numParams += getTileSizeParams(leaf, InParams, HPVMParams);
      //}

      if (EnableNF) {
        numParams += getNodeFusionParams(Leaves, cast<DFLeafNode>(leaf),
                                         InParams, HPVMParams);
      }
    }
  }
  return numParams;
}

auto findHMParamByKey(std::vector<HMInputParam *> &InParams, std::string Key) {
  for (auto it = InParams.begin(); it != InParams.end(); ++it) {
    HMInputParam Param = **it;
    if (Param == Key) {
      return it;
    }
  }
  return InParams.end();
}

void setParamValue(HMInputParam *Param, int ParamVal) {
  HPVMDSEParam *HPVMParam = Param->getHPVMParam();
  HPVMParam->setVal(ParamVal);
}
void setParamValue(HPVMDSEParam *HPVMParam, int ParamVal) {
  HPVMParam->setVal(ParamVal);
}
int getParamValue(HMInputParam *Param) {
  HPVMDSEParam *HPVMParam = Param->getHPVMParam();
  return HPVMParam->getVal();
}

template <class T> void cleanupInput(std::vector<T *> ParamVector) {
  for (auto *p : ParamVector)
    delete p;
}

// // Go through all the instructions in function F and add callinsts to
// // CallsToInline. Also, for each CallInst recursively call this function to
// // inline the functions called within it.
// // TODO: can we get infinite recursion?
// void getCallsToInline(Function *F, std::vector<CallInst *> &CallsToInline) {
//   for (auto ib = inst_begin(F), ie = inst_end(F); ib != ie; ++ib) {
//     Instruction *I = &*ib;
//     if (auto *CI = dyn_cast<CallInst>(I)) {
//       Function *CalledF = CI->getCalledFunction();
//       if (!CalledF->isDeclaration()) {
//         CallsToInline.push_back(CI);
//         getCallsToInline(CalledF, CallsToInline);
//       }
//     }
//   }
// }

// // Inline all the functions called by all the leaf nodes.
// void InlineFunctions(std::vector<DFLeafNode *> Leaves) {
//   std::vector<CallInst *> CallsToInline;
//   for (auto *LN : Leaves) {
//     getCallsToInline(LN->getFuncPointer(), CallsToInline);
//   }
//   for (auto *CI : CallsToInline) {
//     Function *Callee = CI->getFunction();
//     InlineFunctionInfo IFI;
//     InlineFunction(*CI, IFI);
//     DEBUG_TOOL(Callee->dump());
//   }
// }

// Create a new HPVMDSEParam object equivalent to the one provided.
// If GlobalParams enabled, this will create an equivalent local param for each
// global param.
void createEquivParamsWithVal(HPVMDSEParam *HPVMParam, int Val,
                              std::vector<HPVMDSEParam *> &HPVMInParams,
                              std::vector<HPVMDSEParam *> &LocalHPVMParams) {
  HPVMParamType PTy = HPVMParam->getType();
  switch (PTy) {
  case HPVMParamType::TilingParam: {
    if (!GlobalParams) {
      TilingDSEParam *P = static_cast<TilingDSEParam *>(HPVMParam);
      unsigned Dim = P->getDim();
      TilingDSEParam *NewParam =
          new TilingDSEParam(P->getNode(), P->getHMIP(), Dim, Val);
      HPVMInParams.push_back(NewParam);
      return;
    } else {
      hpvmUtils::fatalError("GlobalParam not supported for Tiling yet!");
    }
  }
  case HPVMParamType::TileLvlParam: {
    if (!GlobalParams) {
      TileLvlDSEParam *P = static_cast<TileLvlDSEParam *>(HPVMParam);
      TileLvlDSEParam *NewParam =
          new TileLvlDSEParam(P->getNode(), P->getHMIP(), Val);
      HPVMInParams.push_back(NewParam);
      return;
    } else {
      hpvmUtils::fatalError("GlobalParam not supported for Tiling yet!");
    }
  }
  case HPVMParamType::InBuffParam: {
    auto createParam = [&HPVMInParams, &PTy, &Val](HPVMDSEParam *HPVMParam) {
      BufferingDSEParam *P = static_cast<BufferingDSEParam *>(HPVMParam);
      unsigned ArgNum = P->getArgNum();
      unsigned BuffSize = P->getBuffSize();
      BufferingDSEParam *NewParam =
          new BufferingDSEParam(ArgNum, BuffSize, P->getNode(), P->getHMIP(),
                                HPVMParam->getType(), Val);
      HPVMInParams.push_back(NewParam);
    };
    if (!GlobalParams) {
      createParam(HPVMParam);
      return;
    } else {
      for (auto *LocalHPVMParam : LocalHPVMParams) {
        if (LocalHPVMParam->getType() == HPVMParamType::InBuffParam) {
          createParam(LocalHPVMParam);
        }
      }
      return;
    }
  }
  case HPVMParamType::PrivParam: {
    auto createParam = [&HPVMInParams, &PTy, &Val](HPVMDSEParam *HPVMParam) {
      BufferingDSEParam *P = static_cast<BufferingDSEParam *>(HPVMParam);
      unsigned ArgNum = P->getArgNum();
      unsigned BuffSize = P->getBuffSize();
      BufferingDSEParam *NewParam =
          new BufferingDSEParam(ArgNum, BuffSize, P->getNode(), P->getHMIP(),
                                HPVMParam->getType(), Val);
      HPVMInParams.push_back(NewParam);
    };
    if (!GlobalParams) {
      createParam(HPVMParam);
      return;
    } else {
      for (auto *LocalHPVMParam : LocalHPVMParams) {
        if (LocalHPVMParam->getType() == HPVMParamType::PrivParam) {
          createParam(LocalHPVMParam);
        }
      }
      return;
    }
  }
  // case HPVMParamType::PrivParam: {
  //   auto createParam = [&HPVMInParams, &PTy, &Val](HPVMDSEParam *HPVMParam) {
  //     BufferingDSEParam *P = static_cast<BufferingDSEParam *>(HPVMParam);
  //     unsigned ArgNum = P->getArgNum();
  //     unsigned BuffSize = P->getBuffSize();
  //     BufferingDSEParam *NewParam = new BufferingDSEParam(
  //         ArgNum, BuffSize, P->getNode(), P->getHMIP(), PTy, Val);
  //     HPVMInParams.push_back(NewParam);
  //   };
  //   if (!GlobalParams) {
  //     createParam(HPVMParam);
  //     return;
  //   } else {
  //     for (auto *LocalHPVMParam : LocalHPVMParams) {
  //       if (LocalHPVMParam->getType() == HPVMParamType::PrivParam) {
  //         createParam(LocalHPVMParam);
  //       }
  //     }
  //     return;
  //   }
  // }
  case HPVMParamType::UnrollParam: {
    auto createParam = [&HPVMInParams, &PTy, &Val](HPVMDSEParam *HPVMParam) {
      UnrollDSEParam *P = static_cast<UnrollDSEParam *>(HPVMParam);
      UnrollDSEParam::UnrollType Type = P->getType();
      auto range = P->getHMIP()->getVals();
      int UnrollVal = Val;
      while (std::find(range.begin(), range.end(), UnrollVal) == range.end())
        UnrollVal--;
      switch (Type) {
      case UnrollDSEParam::UnrollType::ReplTy: {
        UnrollDimParam *PP = static_cast<UnrollDimParam *>(P);
        int Dim = PP->getDim();
        UnrollDimParam *NewParam = new UnrollDimParam(
            Dim, P->getF(), P->getNode(), P->getHMIP(), UnrollVal);
        HPVMInParams.push_back(NewParam);
        return;
      }
      case UnrollDSEParam::UnrollType::LoopTy: {
        UnrollLoopParam *PP = static_cast<UnrollLoopParam *>(P);
        BasicBlock *Header = PP->getLoopHeader();
        Function *F = PP->getF();
        UnrollLoopParam *NewParam = new UnrollLoopParam(
            Header, F, P->getNode(), P->getHMIP(), UnrollVal);
        HPVMInParams.push_back(NewParam);
        return;
      }
      }
    };
    if (!GlobalParams) {
      createParam(HPVMParam);
      return;
    } else {
      for (auto *LocalHPVMParam : LocalHPVMParams) {
        if (LocalHPVMParam->getType() == HPVMParamType::UnrollParam) {
          createParam(LocalHPVMParam);
        }
      }
      return;
    }
  }
  case HPVMParamType::LoopFusionParam: {
    auto createParam = [&HPVMInParams, &PTy, &Val](HPVMDSEParam *HPVMParam) {
      HPVMDSEParam *NewParam = new HPVMDSEParam(HPVMParam->getNode(),
                                                HPVMParam->getHMIP(), PTy, Val);
      HPVMInParams.push_back(NewParam);
    };
    if (!GlobalParams) {
      createParam(HPVMParam);
      return;
    } else {
      for (auto *LocalHPVMParam : LocalHPVMParams) {
        if (LocalHPVMParam->getType() == HPVMParamType::LoopFusionParam) {
          createParam(LocalHPVMParam);
        }
      }
      return;
    }
  }
  case HPVMParamType::NodeFusionParam: {
    auto createParam = [&HPVMInParams, &PTy, &Val](HPVMDSEParam *HPVMParam) {
      NodeFusionDSEParam *P = static_cast<NodeFusionDSEParam *>(HPVMParam);
      NodeFusionDSEParam *NewParam =
          new NodeFusionDSEParam(P->getN1(), P->getN2(), P->getHMIP(), Val);
      HPVMInParams.push_back(NewParam);
    };
    if (!GlobalParams) {
      createParam(HPVMParam);
      return;
    } else {
      for (auto *LocalHPVMParam : LocalHPVMParams) {
        if (LocalHPVMParam->getType() == HPVMParamType::NodeFusionParam) {
          createParam(LocalHPVMParam);
        }
      }
      return;
    }
  }
  case HPVMParamType::TLPParam: {
    HPVMDSEParam *NewParam =
        new HPVMDSEParam(HPVMParam->getNode(), HPVMParam->getHMIP(), PTy, Val);
    HPVMInParams.push_back(NewParam);
    return;
  }

  default:
    hpvmUtils::fatalError("Unhandled Parameter Type!");
  }
}

int main(int argc, char **argv) {

  srand(0);
  InitLLVM X(argc, argv);
  cl::ParseCommandLineOptions(argc, argv,
                              "HPVM-HyperMapper Client Tool. Input bitcode "
                              "file is application compiled with clang.");

  if (OutputFoldername.empty())
    hpvmUtils::fatalError("Output folder path must not be empty!");

  // Read in specified LLVM bitcode module
  LLVMContext Context;
  SMDiagnostic Err;
  std::unique_ptr<Module> M = parseIRFile(InputFilename, Err, Context);
  if (!M.get())
    hpvmUtils::fatalError("Failed to open module " + InputFilename);

  if (EGS < 0 || EGS > 1) {
    hpvmUtils::fatalError("Expecting EGS to be float between 0 and 1. Got: " +
               std::to_string(EGS));
  }
  // Prepare New Pass Manager
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  FunctionPassManager FPM;

  // Invoke the frontend passes to build DFG
  legacy::PassManager Passes;
  BuildDFG *DFG = new BuildDFG();
  Passes.add(new genhpvm::GenHPVM());
  Passes.add(DFG);
  Passes.run(*M);

  std::string ModuleName = M->getModuleIdentifier();
  if (ModuleName.find(".ll") != std::string::npos) {
    ModuleName = ModuleName.substr(0, ModuleName.find(".ll"));
  } else if (ModuleName.find(".bc") != std::string::npos) {
    ModuleName = ModuleName.substr(0, ModuleName.find(".bc"));
  }
  std::string HPVMModuleName = ModuleName + ".hpvm.ll";
  M->setModuleIdentifier(HPVMModuleName);

  std::vector<DFInternalNode *> Roots = DFG->getRoots();

  // Gather all the leaf nodes of the DFG.
  LeafFinder *LFinder =
      new LeafFinder(*M, *DFG, TargetToHPVMTarget(::Target), Nodes.getValue());
  for (auto *Root : Roots) {
    DEBUG_TOOL(Root->dump(0, 1, 0));
    LFinder->visit(Root);
  }
  std::vector<DFLeafNode *> Leaves = LFinder->getLeaves();
  if (shouldSequentializeTarget(::Target)) {
    DFGTransform::Context TContext(*M, Context);
    for (DFLeafNode *L : Leaves) {
      ValueToValueMapTy DummyMap;
      LeafSequentializer LS(TContext, L, DummyMap, 1);
      LS.checkAndRun();
    }
  }

  DEBUG_TOOL(errs() << "\n\nLeaves in module:\n");
  for (auto *Leaf : Leaves) {
    DEBUG_TOOL(Leaf->dump(0, 0, 0));
  }

  // Inline all functions that are invoked by leaf nodes
  if (EnableInlining)
    hpvmUtils::inlineFunctions(Leaves);

  // Vectors to store the HyperMapper Parameters and the corresponding HPVM
  // DSE parameters.
  std::vector<HMInputParam *> InParams;
  std::vector<HPVMDSEParam *> HPVMParams;
  // Gather the input parameters
  int numParams = collectInputParams(Leaves, InParams, HPVMParams, false);
  for (auto *param : InParams) {
    std::cout << "Param: " << *param << "\n";
  }
  for (auto *param : HPVMParams) {
    std::cout << "HPVM Param: " << *param << "\n";
  }
  std::vector<HMInputParam *> GlobalInParams;
  std::vector<HPVMDSEParam *> GlobalHPVMParams;
  // Gather the input parameters
  int numGlobalParams =
      collectInputParams(Leaves, GlobalInParams, GlobalHPVMParams, true);

  for (auto *param : GlobalInParams) {
    std::cout << "Param: " << *param << "\n";
  }
  for (auto *param : GlobalHPVMParams) {
    std::cout << "HPVM Param: " << *param << "\n";
  }

  // Setup the tool output directory
  std::string CurrentDir = fs::current_path();
  std::string OutputDir = CurrentDir + "/" + OutputFoldername + "/";
  bool CreateDir = false;
  if (fs::exists(OutputDir)) {
    // If we want to always overwrite the output, do it.
    // If not, prompt user for a new output folder.
    if (!OverwriteOutput) {
      std::cout << "Output directory (" << OutputDir
                << ") exists. If you continue, all contents will be "
                   "erased!"
                << std::endl;
      std::cout << "Are you sure you want to continue? [y/n]" << std::endl;
      char selection;
      std::cin >> selection;

      while (!std::cin.fail() && selection != 'y' && selection != 'n') {
        std::cout << "Invalid input. Please enter 'y' or 'n': " << std::endl;
        std::cin >> selection;
      }
      if (selection == 'y') {
        std::cout << "Output directory will be cleared!" << std::endl;
        // Delete directory then recreate it
        fs::remove_all(OutputDir);
        CreateDir = true;

      } else {
        do {
          std::cout << "Enter a new name for the output folder: " << std::endl;
          std::cin >> OutputFoldername;
          OutputDir = CurrentDir + "/" + OutputFoldername + "/";
          std::cout << "New directory will be created at: " << OutputDir
                    << std::endl;
          std::cout << "Are you sure you want to continue? [y/n]" << std::endl;
          std::cin >> selection;

          while (!std::cin.fail() && selection != 'y' && selection != 'n') {
            std::cout << "Invalid input. Please enter 'y' or 'n': "
                      << std::endl;
            std::cin >> selection;
          }
        } while (selection == 'n');
        CreateDir = true;
      }
    } else {
      // Delete directory then recreate it
      fs::remove_all(OutputDir);
      CreateDir = true;
    }

  } else {
    CreateDir = true;
  }

  if (CreateDir) {
    if (!fs::create_directory(OutputDir)) {
      hpvmUtils::fatalError("Unable to create Directory: " + OutputDir);
    }
    std::cout << "New directory created at: " << OutputDir << std::endl;
  }

  std::string AppName = fs::path(InputFilename.getValue()).stem();

  int NumIterations = ceil(NumDSEIterations * 1.0 / BatchSize);
#pragma omp parallel for num_threads(NumJobs.getValue()) schedule(dynamic)     \
    firstprivate(OutputDir)
  for (int r = 0; r < Reps; ++r) {

    std::map<std::string, int> AllSamples;
    int InvalidPoints = 0, CrashedPoints = 0;
    int SampleCount = 0;
    int numDSEParams = GlobalParams ? numGlobalParams : numParams;
    std::vector<HMInputParam *> InDSEParams =
        GlobalParams ? GlobalInParams : InParams;
    std::ofstream csvfile;
    std::string csvfile_name;
    std::string RepLogFile =
        OutputDir + "log_rep_" + std::to_string(r) + ".log";
    std::ofstream RepOutLog(RepLogFile, std::ios_base::app);
    OutputDir = OutputDir + "/Rep." + std::to_string(r) + "/";
    if (!fs::create_directory(OutputDir)) {
      hpvmUtils::fatalError("Unable to create Directory: " + OutputDir);
    }
#pragma omp critical
    {
      std::cout << "[" << r << "]:"
                << "New directory created at: " << OutputDir << std::endl;
      RepOutLog << "New directory created at: " << OutputDir << std::endl;
    }
    std::string JSonFileNameStr =
        createjson(AppName, NumIterations, InDSEParams, r);

    FILE *instream;
    FILE *outstream;
    struct popen2 hypermapper;
    if (!NoHM) {

      std::string cmd("hypermapper ");
      cmd += JSonFileNameStr;

#pragma omp critical
      {
        std::cout << "[" << r << "]:"
                  << "Executing command: " << cmd << std::endl;
        RepOutLog << "Executing command: " << cmd << std::endl;
      }
      popen2(cmd.c_str(), &hypermapper);

      instream = fdopen(hypermapper.from_child, "r");
      outstream = fdopen(hypermapper.to_child, "w");
    }
    const int max_buffer = 100000;
    char *buffer = nullptr;
    size_t length = 0;

    bool CSVCreated = false;
    bool ReadingBest = false;
    int i = 0;
    int BestSample;
    std::map<int, HMInputParam *> InputParamsMap;
    std::vector<int> SamplesToSynth;
    while (true) {
      int numRequests;
      std::string bufferStr;
      std::string response;
      size_t pos = 0;

      if (!NoHM) {
        getline(&buffer, &length, instream);
#pragma omp critical
        {
          std::cout << "[" << r << "]:"
                    << "Iteration: " << i << std::endl;
          std::cout << "[" << r << "]:"
                    << "Received: " << buffer;
          RepOutLog << "Iteration: " << i << std::endl;
          RepOutLog << "Received: " << buffer;
        }
        // Receiving Num Requests
        bufferStr = std::string(buffer);
        if (!bufferStr.compare("End of HyperMapper\n")) {
#pragma omp critical
          {
            std::cout << "[" << r << "]:"
                      << "Hypermapper completed!\n";
            std::cout << "[" << r << "]:"
                      << "Hypermapper completed!\n";
            RepOutLog << "Hypermapper completed!\n";
            RepOutLog << "Hypermapper completed!\n";
          }
          break;
        } else if (!bufferStr.compare("Best point found:\n")) {
          ReadingBest = true;
        } else {
          std::string NumReqStr = bufferStr.substr(bufferStr.find(' ') + 1);
          numRequests = stoi(NumReqStr);
          if (numRequests == 0) {
            hpvmUtils::fatalError("HyperMapper did not terminate graciously!");
          }
        }
      } else {
        std::cout << "Enter # Requests: ";
        std::cin >> numRequests;
      }
      // Receiving input param names
      if (!NoHM) {
        getline(&buffer, &length, instream);
        bufferStr = std::string(buffer);
        if (bufferStr.find("\n") == std::string::npos)
          hpvmUtils::fatalError("Overflow detected when reading HM output!");
#pragma omp critical
        {
          std::cout << "[" << r << "]:"
                    << "Received: " << buffer;
          RepOutLog << "Received: " << buffer;
        }
        // Create mapping for InputParam objects to keep track of order
        std::stringstream ISS;
        ISS << bufferStr;
        std::string ParamStr;
        int param = 0;
        while (std::getline(ISS, ParamStr, ',')) {
          if (ReadingBest && param == numDSEParams)
            break;
          else if (param == numDSEParams)
            hpvmUtils::fatalError("Something is wrong we should not enter this state!");
          ParamStr.erase(std::remove(ParamStr.begin(), ParamStr.end(), '\n'),
                         ParamStr.end());
          auto paramIt = findHMParamByKey(InDSEParams, ParamStr);
          if (paramIt != InDSEParams.end()) {
            InputParamsMap[param] = *paramIt;
            response += ParamStr;
            response += ",";
          } else {
            hpvmUtils::fatalError("Unknown parameter received!");
          }
          param++;
        }
        if (::Target == fpga && MultiObjective)
          response += "Resources,";
        response += "ExecTime";
        if (::Target == fpga)
          response += ",Valid";
        response += "\n";
      } else {
        std::cout << "Enter parameter names in order: " << std::endl;
        std::cin >> bufferStr;
        // Create mapping for InputParam objects to keep track of order
        std::stringstream ISS;
        ISS << bufferStr;
        std::string ParamStr;
        int param = 0;
        while (std::getline(ISS, ParamStr, ',')) {
          auto paramIt = findHMParamByKey(InDSEParams, ParamStr);
          if (paramIt != InDSEParams.end()) {
            InputParamsMap[param] = *paramIt;
          } else {
            hpvmUtils::fatalError("Unknown parameter received!");
          }
          param++;
        }
      }
      if (CreateCSV && !CSVCreated) {
        createCSV(InputParamsMap, numDSEParams, r, csvfile_name, csvfile);
        CSVCreated = true;
      }
      if (!ReadingBest) {
#pragma omp parallel num_threads(BatchSize.getValue())                         \
    shared(response, SampleCount)
        {
          char *buffer = nullptr;
          std::string bufferStr;

          double MaxTime = 0.0;
#pragma omp for firstprivate(InDSEParams) schedule(dynamic)
          for (int request = 0; request < numRequests; request++) {
            int LocalSample;
            // Thread private vector for DSE parameters
            std::vector<HPVMDSEParam *> HPVMInParams;
            int tid = omp_get_thread_num();
            // std::string CurrentDir = fs::current_path();
            // std::string OutputDir = CurrentDir + "/" + OutputFoldername +
            // "/";

            std::string TLogFile =
                OutputDir + "out_" + std::to_string(tid) + ".log";
            std::ofstream OutLog(TLogFile, std::ios_base::app);
#pragma omp critical
            {
              LocalSample = SampleCount++;
              DEBUG_TOOL(printf("[%d:%d:%d]: Processing sample: %d\n", r, tid,
                                LocalSample, LocalSample));
              RepOutLog << "[" << tid << ":" << LocalSample
                        << "]: Processing sample: " << LocalSample << "\n";
              if (!NoHM) {
                // Receiving paramter values
                size_t length;
                getline(&buffer, &length, instream);
                printf("[%d:%d:%d]: Received: %s\n", r, tid, LocalSample,
                       buffer);
                RepOutLog << "[" << tid << ":" << LocalSample
                          << "]: Received: " << buffer << "\n";
                OutLog << "Received: " << buffer << std::endl;
                OutLog.flush();
                fflush(stdout);
                bufferStr = std::string(buffer);
                std::stringstream ISS(bufferStr);
                std::string ParamValStr;
                int param = 0;
                while (std::getline(ISS, ParamValStr, ',')) {
                  ParamValStr.erase(
                      std::remove(ParamValStr.begin(), ParamValStr.end(), '\n'),
                      ParamValStr.end());
                  HPVMDSEParam *OrigParam =
                      InputParamsMap[param]->getHPVMParam();
                  createEquivParamsWithVal(OrigParam, stoi(ParamValStr),
                                           HPVMInParams, HPVMParams);
                  param++;
                }
              } else {
                printf(
                    "[%d:%d:%d]: Enter value std::string (comma separated):\n",
                    r, tid, LocalSample);
                std::cin >> bufferStr;
                OutLog << "Received: " << bufferStr << std::endl;
                OutLog.flush();
                fflush(stdout);
                std::stringstream ISS(bufferStr);
                std::string ParamValStr;
                int param = 0;
                while (std::getline(ISS, ParamValStr, ',')) {
                  if (param >= numDSEParams) {
                    hpvmUtils::warning("Too many params specified, truncated");
                    break;
                  }

                  HPVMDSEParam *OrigParam =
                      InputParamsMap[param]->getHPVMParam();
                  createEquivParamsWithVal(OrigParam, stoi(ParamValStr),
                                           HPVMInParams, HPVMParams);
                  param++;
                }
                if (::Target == fpga)
                  SamplesToSynth.push_back(LocalSample);
              }
            }
            // HMObjective Obj = {rand(), rand(), LocalSample%2};
            HMObjective Obj =
                calculateObjective(HPVMInParams, *M, Leaves, LocalSample, r,
                                   OutLog, CrashedPoints, MaxTime);
            std::string ParamStr;
            for (auto *InParam : HPVMInParams) {
              ParamStr += std::to_string(InParam->getVal()) + ",";
            }
            std::string ObjStr;
            if (::Target == fpga && MultiObjective) {
              ObjStr += std::to_string(Obj.Resources) + ",";
            }
            ObjStr += std::to_string(Obj.ExecTime);
            std::string Sample = ParamStr;
            AllSamples[Sample] = LocalSample;
            std::string LocalResponse = ParamStr + ObjStr;
            if (::Target == fpga)
              LocalResponse += "," + std::to_string(Obj.valid);
            LocalResponse += "\n";

#pragma omp critical
            {
              if (!NoHM) {
                if (::Target == fpga && !Obj.valid)
                  InvalidPoints++;
                response += LocalResponse;
              }

              if (CreateCSV) {
                csvfile.open(csvfile_name, std::ios_base::app);
                std::string csvstring = std::to_string(LocalSample) + ",";
                csvstring += LocalResponse;
                csvfile << csvstring;
                csvfile.close();
              }
            }
            cleanupInput(HPVMInParams);
            // SampleCount += _MIN(BatchSize.getValue(), numRequests -
            // request);
          }
          free(buffer);
        }

        if (!NoHM) {
#pragma omp critical
          {
            std::cout << "[" << r << "]:"
                      << "Response:\n"
                      << response;
            RepOutLog << "Response:\n" << response;
          }
          fputs(response.c_str(), outstream);
          fflush(outstream);
        } else {
          break;
        }
      } else {
        getline(&buffer, &length, instream);
#pragma omp critical
        printf("[%d]: Received: %s\n", r, buffer);
        RepOutLog << "Received: " << buffer << "\n";
        bufferStr = std::string(buffer);
        std::string Sample;
        BestSample = findSample(bufferStr, numDSEParams, AllSamples);
        printf("[%d]: Best Sample: %d\n", r, BestSample);
        RepOutLog << "Best Sample: " << BestSample << "\n";
        if (::Target == fpga)
          SamplesToSynth.push_back(BestSample);
        break;
      }

      i++;
    }
    if (!NoHM) {

      close(hypermapper.from_child);
      close(hypermapper.to_child);
#pragma omp critical
      {
        std::cout << "[" << r << "]:"
                  << "Total: " << SampleCount
                  << "; Valid: " << SampleCount - InvalidPoints
                  << "; Invalid (non-crash): " << InvalidPoints - CrashedPoints
                  << "; Crashed: " << CrashedPoints << std::endl;
        RepOutLog << "[" << r << "]:"
                  << "Total: " << SampleCount
                  << "; Valid: " << SampleCount - InvalidPoints
                  << "; Invalid (non-crash): " << InvalidPoints - CrashedPoints
                  << "; Crashed: " << CrashedPoints << std::endl;
      }
      FILE *fp;
      if (MultiObjective) {
        std::string cmdPareto("hm-compute-pareto ");
        cmdPareto += JSonFileNameStr;
#pragma omp critical
        {
          std::cout << "[" << r << "]:"
                    << "Executing " << cmdPareto << std::endl;
          RepOutLog << "Executing " << cmdPareto << std::endl;
        }
        fp = popen(cmdPareto.c_str(), "r");
        while (fgets(buffer, max_buffer, fp))
          printf("%s", buffer);
        cmdPareto = "hm-plot-pareto ";
        cmdPareto += JSonFileNameStr;
#pragma omp critical
        {
          std::cout << "[" << r << "]:"
                    << "Executing " << cmdPareto << std::endl;
          RepOutLog << "Executing " << cmdPareto << std::endl;
        }
        fp = popen(cmdPareto.c_str(), "r");
        while (fgets(buffer, max_buffer, fp))
          printf("%s", buffer);
        // std::string CurrentDir = fs::current_path();
        // std::string OutputDir = CurrentDir + "/" + OutputFoldername;
        std::string CSVPareto =
            OutputDir + "/" + AppName + "_output_pareto.csv";

        std::ifstream CSVParetoFile(CSVPareto);
        std::string Line;
        std::getline(CSVParetoFile, Line);
        while (std::getline(CSVParetoFile, Line)) {
          int ParetoSample = findSample(Line, numDSEParams, AllSamples);
#pragma omp critical
          {
            std::cout << "[" << r << "]:"
                      << "Pareto Sample: " << ParetoSample << std::endl;
            RepOutLog << "Pareto Sample: " << ParetoSample << std::endl;
            if (::Target == fpga)
              SamplesToSynth.push_back(ParetoSample);
          }
        }
        pclose(fp);
      }
    }
    if (::Target == fpga) {
      std::ofstream SynthPointsFile(OutputDir + "/synth_points.txt");
      for (int SS : SamplesToSynth) {
        SynthPointsFile << SS << " ";
      }
      SynthPointsFile.close();

      auto synthesizePoint = [&M, OutputDir, r, &RepOutLog](int SampleNum,
                                                            bool Emulation) {
        // auto TID = std::this_thread::get_id();
        // std::string CurrentDir = fs::current_path();
        // std::string OutputDir = CurrentDir + "/" + OutputFoldername + "/Rep."
        // + std::to_string(r) + "/";
        std::string SampleDir =
            OutputDir + "Sample." + std::to_string(SampleNum) + "/";
        // Prepare all the module and file names
        std::string ModuleName = M->getModuleIdentifier();
        // Extract out the path
        if (ModuleName.find("/") != std::string::npos)
          ModuleName = ModuleName.substr(ModuleName.find_last_of("/") + 1);
        if (ModuleName.find(".ll") != std::string::npos) {
          ModuleName = ModuleName.substr(0, ModuleName.find(".ll"));
        } else if (ModuleName.find(".bc") != std::string::npos) {
          ModuleName = ModuleName.substr(0, ModuleName.find(".bc"));
        }
        std::string OCLFileName = SampleDir + ModuleName + ".kernels.cl";
        std::string AOCXFileName = SampleDir + ModuleName + ".kernels.aocx";

        if (!fs::exists(SampleDir)) {
          std::string Message =
              "Sample directory does not exist: " + std::to_string(SampleNum) +
              "!";
          hpvmUtils::fatalError(Message);
        }

        std::string command =
            "cd " + SampleDir + "; aoc " + OCLFileName +
            (Emulation ? " -march=emulator " : "") + " -o " + AOCXFileName +
            " -board=" + Board +
            (AOCParallel > 0 ? " -parallel=" + std::to_string(AOCParallel)
                             : "");
#pragma omp critical
        {
          std::cout << "[" << r << ":" << SampleNum << "]: Executing "
                    << command << std::endl;
          RepOutLog << "Executing " << command << std::endl;
        }

        auto start = std::chrono::steady_clock::now();
        FILE *fp;
        fp = popen(command.c_str(), "r");
        const int max_buffer = 1000;
        char buffer[max_buffer];
        while (fgets(buffer, max_buffer, fp)) {
          std::cout << buffer;
        }
        auto status = pclose(fp);
        auto end = std::chrono::steady_clock::now();
        auto duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
#pragma omp critical
        {
          std::cout << "[" << r << ":" << SampleNum << "]: status: " << status
                    << std::endl;
          std::cout << "[" << r << ":" << SampleNum
                    << "]: AOC Duration: " << duration.count() << " ms"
                    << std::endl;
          RepOutLog << "[" << r << ":" << SampleNum << "]: status: " << status
                    << std::endl;
          RepOutLog << "[" << r << ":" << SampleNum
                    << "]: AOC Duration: " << duration.count() << " ms"
                    << std::endl;
        }
      };
      if (Synthesize && Emulation) {
        hpvmUtils::warning(
            "Both Emulation and Synthesize were enabled, neither will be run!");
      } else if (Synthesize || Emulation) {
        for (auto i = 0; i < SamplesToSynth.size(); ++i) {
          int Sample = SamplesToSynth[i];
          synthesizePoint(Sample, Emulation);
        }
      }
    }
    //    cleanupInput(InDSEParams);
  }
  delete LFinder;
  cleanupInput(InParams);
  cleanupInput(HPVMParams);
  cleanupInput(GlobalInParams);
  cleanupInput(GlobalHPVMParams);
  return 0;
}
int HMInputParam::count = 0;
