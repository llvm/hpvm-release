#ifndef HPVM_HYPERMAPPER_H
#define HPVM_HYPERMAPPER_H
#include <string>
#include <iostream>
#include <iomanip>

#include "SupportHPVM/DFGTreeTraversal.h"
#include "CoreHPVM/DFGraph.h"
#include "llvm/Analysis/LoopInfo.h"

using namespace dfg2llvm;

class HMInputParam;
class TilingDSEParam;
void fatalError(const std::string &msg);

enum ParamType { Real, Integer, Ordinal, Categorical };

std::ostream &operator<<(std::ostream &out, const ParamType &PT) {
  switch (PT) {
  case Real:
    out << "Real";
    break;
  case Integer:
    out << "Integer";
    break;
  case Ordinal:
    out << "Ordinal";
    break;
  case Categorical:
    out << "Categorical";
    break;
  }
  return out;
}

std::string getTypeAsString(const ParamType &PT) {
  std::string TypeString;
  switch (PT) {
  case Real:
    TypeString = "real";
    break;
  case Integer:
    TypeString = "integer";
    break;
  case Ordinal:
    TypeString = "ordinal";
    break;
  case Categorical:
    TypeString = "categorical";
    break;
  }

  return TypeString;
}

enum HPVMParamType {
  TilingParam,
  TileLvlParam,
  InBuffParam,
  PrivParam,
  UnrollParam,
  LoopFusionParam,
  NodeFusionParam,
  TLPParam,
  Other
};

std::ostream &operator<<(std::ostream &out, const HPVMParamType &PT) {
  switch (PT) {
  case TilingParam:
    out << "Tiling Parameter";
    break;
  case TileLvlParam:
    out << "Tiling Level Parameter";
    break;
  case InBuffParam:
    out << "Input Buffering Parameter";
    break;
  case PrivParam:
    out << "Privatization Parameter";
    break;
  case UnrollParam:
    out << "Unroll Parameter";
    break;
  case LoopFusionParam:
    out << "Loop Fusion Parameter";
    break;
  case NodeFusionParam:
    out << "Node Fusion Parameter";
    break;
  case TLPParam:
    out << "TLP / Concurrent Node Execution Parameter";
    break;
  default:
    out << "Unknown type parameter!";
    break;
  }
  return out;
}

class HPVMDSEParam {
private:
  llvm::DFNode *N;
  HMInputParam *HMIP;
  HPVMParamType Type;
  int Val;
  bool Global;

public:
  HPVMDSEParam(llvm::DFNode *_N = nullptr, HMInputParam *_HMIP = nullptr,
               HPVMParamType _Type = Other, int _Val = 0, bool _Global = false)
      : N(_N), HMIP(_HMIP), Type(_Type), Val(_Val), Global(_Global) {}
  llvm::DFNode *getNode() const { return N; }
  void setNode(llvm::DFNode *_N) { N = _N; }

  HMInputParam *getHMIP() const { return HMIP; }
  void setHMIP(HMInputParam *_HMIP) { HMIP = _HMIP; }

  HPVMParamType getType() const { return Type; }
  void setType(HPVMParamType _Type) { Type = _Type; }

  int getVal() const { return Val; }
  void setVal(int _Val) { Val = _Val; }

  friend std::ostream &operator<<(std::ostream &out, const HPVMDSEParam &Param);

  virtual void print(std::ostream &os) const {
    os << "\n  Value: " << getVal();
  }
};

class UnrollDSEParam : public HPVMDSEParam {
public:
  enum UnrollType { LoopTy, ReplTy };

private:
  Function *F;
  UnrollType Type;

public:
  UnrollDSEParam(UnrollType _Type, Function *_F = nullptr,
                 llvm::DFNode *_N = nullptr, HMInputParam *_HMIP = nullptr,
                 int _UF = 0, bool _Global = false)
      : HPVMDSEParam(_N, _HMIP, HPVMParamType::UnrollParam, _UF, _Global),
        F(_F), Type(_Type) {}

  UnrollType getType() const { return Type; }

  Function *getF() const { return F; }
};

class UnrollLoopParam : public UnrollDSEParam {
private:
  BasicBlock *Header;

public:
  UnrollLoopParam(BasicBlock *_Header, Function *_F = nullptr,
                  llvm::DFNode *_N = nullptr, HMInputParam *_HMIP = nullptr,
                  int _UF = 0, bool _Global = false)
      : UnrollDSEParam(UnrollDSEParam::UnrollType::LoopTy, _F, _N, _HMIP, _UF,
                       _Global),
        Header(_Header) {}

  BasicBlock *getLoopHeader() const { return Header; }

  void print(std::ostream &os) const {
    os << "\n  Function: " << (getF() ? getF()->getName().str() : "NULL");
    os << "\n  Loop Header: " << (Header ? Header->getName().str() : "NULL");
    os << "\n  Unroll Factor: " << getVal();
  }
};

class UnrollDimParam : public UnrollDSEParam {
private:
  int Dim;

public:
  UnrollDimParam(int _Dim, Function *_F = nullptr, llvm::DFNode *_N = nullptr,
                 HMInputParam *_HMIP = nullptr, int _UF = 0,
                 bool _Global = false)
      : UnrollDSEParam(UnrollDSEParam::UnrollType::ReplTy, _F, _N, _HMIP, _UF,
                       _Global),
        Dim(_Dim) {}

  int getDim() const { return Dim; }
  void setDim(int _Dim) { Dim = _Dim; }

  void print(std::ostream &os) const {
    os << "\n  Function: " << getF()->getName().str();
    os << "\n  Dim: " << Dim;
    os << "\n  Unroll Factor: " << getVal();
  }
};

class BufferingDSEParam : public HPVMDSEParam {
private:
  unsigned ArgNum;
  unsigned BuffSize;

public:
  BufferingDSEParam(unsigned _ArgNum, unsigned _BuffSize = 0,
                    llvm::DFNode *_N = nullptr, HMInputParam *_HMIP = nullptr,
                    HPVMParamType _type = HPVMParamType::InBuffParam,
                    int _TB = 0, bool _Global = false)
      : HPVMDSEParam(_N, _HMIP, _type, _TB, _Global), ArgNum(_ArgNum),
        BuffSize(_BuffSize) {}

  unsigned getBuffSize() const { return BuffSize; }
  void setBuffSize(unsigned _BuffSize) { BuffSize = _BuffSize; }

  unsigned getArgNum() const { return ArgNum; }
  void setArgNum(unsigned _ArgNum) { ArgNum = _ArgNum; }

  void print(std::ostream &os) const {
    os << "\n  Argument: " << ArgNum;
    os << "\n  Buffer Size: " << BuffSize;
    os << "\n  To Buffer: " << getVal();
  }
};

class TileLvlDSEParam : public HPVMDSEParam {
public:
  TileLvlDSEParam(llvm::DFNode *_N = nullptr, HMInputParam *_HMIP = nullptr,
                  int _TL = 0, bool _Global = false)
      : HPVMDSEParam(_N, _HMIP, HPVMParamType::TileLvlParam, _TL, _Global) {}

  void print(std::ostream &os) const { os << "\n  Val: " << getVal(); }
};

class NodeFusionDSEParam : public HPVMDSEParam {
private:
  DFLeafNode *N1;
  DFLeafNode *N2;

public:
  NodeFusionDSEParam(DFLeafNode *N1, DFLeafNode *N2,
                     HMInputParam *_HMIP = nullptr, int _V = 0,
                     bool _Global = false)
      : HPVMDSEParam(N1, _HMIP, HPVMParamType::NodeFusionParam, _V, _Global),
        N1(N1), N2(N2) {}

  DFLeafNode *getN1() const { return N1; }
  DFLeafNode *getN2() const { return N2; }

  void print(std::ostream &os) const override {
    HPVMDSEParam::print(os);
    os << "\n  Node 1: " << (N1 ? N1->getName().str() : "NULL");
    os << "\n  Node 2: " << (N2 ? N2->getName().str() : "NULL");
  }
};

class TilingDSEParam : public HPVMDSEParam {
private:
  unsigned Dim;

public:
  TilingDSEParam(llvm::DFNode *_N = nullptr, HMInputParam *_HMIP = nullptr,
                 unsigned _Dim = 0, unsigned _TS = 0)
      : HPVMDSEParam(_N, _HMIP, HPVMParamType::TilingParam, _TS), Dim(_Dim) {}

  unsigned getDim() const { return Dim; }
  void setDim(unsigned _Dim) { Dim = _Dim; }

  void print(std::ostream &os) const {
    os << "\n  Dim: " << getDim();
    os << "\n  TileSize: " << getVal();
  }
};

class HMInputParam {
private:
  std::string Name;
  std::string const Key;
  ParamType Type;
  int startVal;
  int endVal;
  std::vector<int> Vals;
  static int count;
  HPVMDSEParam *HPVMParam;
  int Default;

public:
  HMInputParam(std::string _Name = "", ParamType _Type = ParamType::Integer)
      : Name(_Name), Type(_Type), HPVMParam(nullptr), startVal(0), endVal(0),
        Key(_Name), Default(0) {}

  std::string getName() const { return Name; }
  void setName(std::string _Name) { Name = _Name; }

  ParamType getType() const { return Type; }
  void setType(ParamType _Type) { Type = _Type; }

  void setStartVal(int _Val) { startVal = _Val; }
  int getStartVal() const { return startVal; }

  void setEndVal(int _Val) { endVal = _Val; }
  int getEndVal() const { return endVal; }

  void setVals(std::vector<int> const &_Vals) { Vals = _Vals; }
  std::vector<int> getVals() const { return Vals; }

  void setDefault(int _Default) { Default = _Default; }
  int getDefault() { return Default; }

  std::string getKey() const { return Key; }

  void setHPVMParam(HPVMDSEParam *_HPVMParam) { HPVMParam = _HPVMParam; }
  HPVMDSEParam *getHPVMParam() const { return HPVMParam; }

  bool operator==(const std::string &_Key) {
    if (Key == _Key) {
      return true;
    } else {
      return false;
    }
  }

  bool operator==(const HMInputParam &IP) {
    if (Key == IP.getKey()) {
      return true;
    } else {
      return false;
    }
  }
  friend std::ostream &operator<<(std::ostream &out, const HMInputParam &IP) {
    out << IP.getKey() << ":";
    out << "\n  Name: " << IP.Name;
    out << "\n  Type: " << IP.Type;
    if (IP.getType() == ParamType::Ordinal ||
        IP.getType() == ParamType::Categorical) {
      out << "\n  Range: {";
      char separator[1] = "";
      for (auto i : IP.getVals()) {
        out << separator << i;
        separator[0] = ',';
      }
      out << "}";
    } else if (IP.getType() == ParamType::Integer) {
      out << "\n  Range: [" << IP.getStartVal() << ", " << IP.getEndVal()
          << "]";
    }
    out << "\n  HPVMParam:\n" << *(IP.getHPVMParam());
    return out;
  }
};

struct HMObjective {
  int Resources;
  double ExecTime;
  bool valid;
};

std::ostream &operator<<(std::ostream &out, const HMObjective &Obj) {
  std::cout << "\n\tResources: " << Obj.Resources
            << "\n\tExecTime: " << Obj.ExecTime << "\n\tValid: " << Obj.valid;
}

std::ostream &operator<<(std::ostream &out, const HPVMDSEParam &Param) {
  out << "Parameter: " << Param.getHMIP()->getKey();
  if (Param.getNode() != nullptr)
    out << "\n  Node: " << Param.getNode()->getName().str();
  out << "\n  Type: " << Param.getType();
  Param.print(out);

  return out;
}
struct UnrollDimInfo {
  int Dim;
  int UnrollFactor;
  int NodeDims;
};
struct AOCLoopInfo {
  int BB;
  uint Level;
  int II;
  int Latency;
  int Spec;
  bool Serialized;
  int MaxInterleaving;
};
std::ostream &operator<<(std::ostream &out, const AOCLoopInfo &AOCLI) {
  out << "BB: " << AOCLI.BB << std::endl;
  out << "  - Level: " << AOCLI.Level << std::endl;
  out << "  - II: " << AOCLI.II << std::endl;
  out << "  - Latency: " << AOCLI.Latency << std::endl;
  out << "  - Serialized: " << AOCLI.Serialized << std::endl;
  out << "  - MaxInterleaving: " << AOCLI.MaxInterleaving << std::endl;
  return out;
}
struct DSESample {
  std::string ParamValues;
  HMObjective Obj;
};
#endif
