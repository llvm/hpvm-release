# hpvm-dse

This tool invokes the design space exploration (DSE) framework that
tunes HeteroC++ applications automatically using the DFG and non-DFG
transformations. The tool can be used to target FPGA or GPU. The DSE
framework uses [HyperMapper (HM)](https://github.com/luinardi/hypermapper) to traverse the
design space.

## Usage

The snipped below shows the usage of the `hpvm-dse` tool with the most
common command line flags. Please see `hpvm-dse --help` for a full list
of command line options.

``` shell
hpvm-dse [OPTIONS] <input>

<input>: Input bitcode file (.ll or .bc) corresponding 
         to the output of clang.

OPTIONS:

--help,                     Print a help message that shows all options.
Target Device for DSE (only one can be selected):
    --fpga,                 Specify FPGA target. Default.
    --gpu,                  Specify GPU target.
-b=<s>,                     Specify target FPGA board. Default: "a10gx".
--batch=<n>,                Set the batch size for DSE processing. Default: 1.
--custom-evaluator=<path>,  Path to script that evaluates objective for a 
                            given target. Not needed for FPGA target.
--doe-multiplier=<n>,       Sets multiplier for DOE samples for HM.
                            Num DoE Samples =
                                "N x #Pramas if N > 1"
                                "#Pramas + 1 if N = 1".
                            Default: 1.
--dse-iter=<n>,             Set number of DSE iterations to perform.
                            Default: 20.
--max-uf=<n>,               Maximum unroll factor to use in DSE.
                            Default: 10.
--max-uf-options=<n>,       Maximum number of options to select for each 
                            unrolling parameter. Default: 3.
-o=<folder>,                Specify output folder name.
--synth,                    Automatically run synthesis at the end of DSE.
                            Default: false.
--timeout=<int>,            Set the timeout (in sec) for each evaluation.
                            An evaluation that times out will be invalid.
                            Default: 900. 
```
