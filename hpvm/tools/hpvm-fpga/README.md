# hpvm2fpga

This tool invokes the end-to-end fpga compiler and optimizer for HPVM.
It is used internally by `hcc` and `hpvm-clang` when targeting the FPGA.
The tool uses the HPVM DFG and non-DFG transformations to optimize
applications based on user-provided flags. The output of the tool is:

1.  The LLVM module for the host code (to be compiled using LLVM's X86
    backend),
2.  An OpenCL file containing the optimized FPGA kernels.
3.  Optionally, the FPGA bitstream that gets generated by synthesizing
    the kernels using `aoc`.

Additionally, the tool can support compiling with full synthesis,
partial syntehsis (i.e. RTL generation), or emulation using the Intel
FPGA SDK for OpenCL Emulator.

## Usage

``` shell
hpvm2fpga [OPTIONS] <input>

    <input>: Input bitcode file (.ll or .bc) corresponding 
             to the output of clang.

    OPTIONS:

    --board=<s>,    Specify FPGA board. Default: "a10gx".
    --bufferin,     Perform input buffering.
    --argpriv,      Perform argument privatization.
    --unroll,       Perform loop unrolling.
    --fuse,         Perform loop fusion.
    --nodefuse,     Perform node fusion.
    --uf=<int>,     Set the unroll factor.
    --run-aoc,      Run ``aoc`` to generate ``.aocx`` file.
    --emu,          Enable Emulation mode. i.e. ``aocx`` file 
                    and host binary will be configured to use 
                    the Intel FPGA Emulator.
    --rtl,          Only perform partial ``aoc`` compilation 
                    to generate RTL and compilation reports.
```
