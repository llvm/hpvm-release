//===-- hpvm-fpga.cpp - FPGA compiler for HPVM bit code -------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
///
/// \file
/// This file contains the tool HPVM-FPGA, a compiler for FPGA target from HPVM
/// bitcode.
///
//===----------------------------------------------------------------------===//

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/wait.h>
#include <unistd.h>
#include <experimental/filesystem>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <memory>
#include <cstdint>
#include <cstdio>

#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"

#define DEBUG_TYPE "hpvm-fpga"
#include "IRCodeGen/BuildDFG.h"
#include "Transforms/DFGTransforms/Fuse.h"
#include "Transforms/DFGTransforms/SequentializeLeaf.h"
#include "IRCodeGen/GenHPVM.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "SupportHPVM/DFGTreeTraversal.h"
#include "IRCodeGen/ClearDFG.h"
#include "Transforms/Utils/HPVMBufferingPrivatization.h"
#include "Transforms/Utils/HPVMLoopTransforms.h"
#include "SupportHPVM/LeafFinder.h"
#include "CoreHPVM/DFGUtils.h"

using namespace llvm;
using namespace builddfg;
using namespace genhpvm;
using namespace hpvm;
using namespace dfg2llvm;

#ifndef LLVM_BUILD_DIR
#error LLVM_BUILD_DIR is not defined
#endif

#define STR_VALUE(X) #X
#define STRINGIFY(X) STR_VALUE(X)
#define LLVM_BUILD_DIR_STR STRINGIFY(LLVM_BUILD_DIR)

// Global variable for storing inputModuleName
std::string inputModuleName;

// Command Line Options
enum Opts { bufferin = 0, argpriv, unroll, fuse, outermostunroll, nodefuse };

cl::OptionCategory HPVMFPGACat("HPVM-FPGA Options");

// InputFilename - The filename to read from. Required.
static cl::opt<std::string> InputFilename(cl::Positional,
                                     cl::desc("<input bitcode file>"),
                                     cl::Required, cl::value_desc("filename"));

// Optimization List
// List of optimizations that need to be run on the bitcode file before code-gen
static cl::list<Opts> OptimizationList(
    cl::desc("Available Optimizations:"),
    cl::values(clEnumVal(bufferin, "Input Buffering"),
               clEnumVal(argpriv, "Argument Privatization"),
               clEnumVal(unroll, "Loop Unroll"), clEnumVal(fuse, "Loop Fuse"),
               clEnumVal(outermostunroll, "Outermost Loop Unroll"),
               clEnumVal(nodefuse, "Node Fusion")),
    cl::cat(HPVMFPGACat));

// MaxIBSize - Input Buffering size threshold
static cl::opt<int> MaxIBSize(
    "ib-size", cl::desc("Maximum size of input to buffer. Defaults to 12000."),
    cl::value_desc("max IB size"), cl::init(12000), cl::cat(HPVMFPGACat));

// -uf - Loop Unroll Factor
static cl::opt<unsigned> UnrollFactor(
    "uf",
    cl::desc("Loop unroll factor for all loops. Optimizer will fully unroll "
             "any loops with tripcout <= uf. For tripcounts > uf and variable "
             "tripcounts, refer to -pu and -ru respectively. Defaults to 5."),
    cl::value_desc("unroll factor"), cl::init(5), cl::cat(HPVMFPGACat));

// -pu - Allow partial unroll
static cl::opt<bool>
    PartialUnroll("pu",
                  cl::desc("Flag to allow unrolling loops that have a constant "
                           "tripcount > uf. Defaults to FALSE."),
                  cl::init(false), cl::cat(HPVMFPGACat));

// -uo - Allow unrolling outer loops
static cl::opt<bool> OuterUnroll(
    "uo", cl::desc("Flag to allow unrolling outer loops. Defaults to FALSE."),
    cl::init(false), cl::cat(HPVMFPGACat));

// -ru - Allow partial unroll
static cl::opt<bool>
    RuntimeUnroll("ru",
                  cl::desc("Flag to allow unrolling loops that have a variabl "
                           "(runtime) tripcount. Defaults to FALSE."),
                  cl::init(false), cl::cat(HPVMFPGACat));

// -hm - The name of the output host module. Defaults to <input>.host.ll
static cl::opt<std::string> HostModuleName(
    "hm",
    cl::desc(
        "Specify output host module name. Defaults to <input>.hpvm -host.ll"),
    cl::value_desc("host module"), cl::cat(HPVMFPGACat), cl::init("-"));

// -km - The name of the output kernel module. Defaults to <input>.kernels.ll
static cl::opt<std::string>
    KernelModuleName("km",
                     cl::desc("Specify output kernel module name. Defaults to "
                              "<input>.hpvm -kernels.ll"),
                     cl::value_desc("kernel module"), cl::cat(HPVMFPGACat),
                     cl::init("-"));

// -clm - The name of the OpenCL module from llvm-ocl. Defaults to
// <input>.kernels.cl
static cl::opt<std::string>
    OpenCLModuleName("clm",
                     cl::desc("Specify OpenCL kernel module name. Defaults to "
                              "<input>.hpvm -kernels.cl"),
                     cl::value_desc("OpenCL module"), cl::cat(HPVMFPGACat),
                     cl::init("-"));

// -cxm - The name of the AOCX module from AOC. Defaults to <input>.kernels.aocx
static cl::opt<std::string>
    AOCXModuleName("cxm",
                   cl::desc("Specify OpenCL kernel module name. Defaults to "
                            "<input>.hpvm.kernels.aocx"),
                   cl::value_desc("OpenCL module"), cl::cat(HPVMFPGACat),
                   cl::init("-"));

// -dbg - Enable debug mode for tool, printing out intermediate modules.
static cl::opt<bool>
    DebugModules("dbg",
                 cl::desc("Enables printing out intermediate modules for "
                          "debugging. Defaults to FALSE."),
                 cl::cat(HPVMFPGACat), cl::init(false));

// -no-tlp - Disable TLP
static cl::opt<bool> NoTLP("no-tlp",
                           cl::desc("Disables TLP. Defaults to false."),
                           cl::cat(HPVMFPGACat), cl::init(false));

// --baseline - Disable TLP and ivdep
static cl::opt<bool>
    Baseline("baseline", cl::desc("Disables TLP and ivdep. Defaults to false."),
             cl::cat(HPVMFPGACat), cl::init(false));

// --baseline-no-restrict - Disable TLP and ivdep and restrict.
static cl::opt<bool> BaselineNoRestrict(
    "baseline-no-restrict",
    cl::desc("Disables TLP, ivdep, and restrict. Defaults to false."),
    cl::cat(HPVMFPGACat), cl::init(false));

// -emu - Enable emulation mode.
static cl::opt<bool> Emulation("emu", cl::desc("Enable Emulation mode."),
                               cl::cat(HPVMFPGACat), cl::init(false));

// -rtl - Enable RTL mode.
static cl::opt<bool> RTL("rtl", cl::desc("Enable RTL mode."),
                         cl::cat(HPVMFPGACat), cl::init(false));

// -run-aoc - Run AOC
static cl::opt<bool> RunAOC("run-aoc", cl::desc("Run AOC."),
                            cl::cat(HPVMFPGACat), cl::init(false));

// -board - Specify board name. defaults to a10gx
static cl::opt<std::string> Board("board",
                             cl::desc("Specify FPGA Board. Defaults to a10gx."),
                             cl::cat(HPVMFPGACat), cl::init("a10gx"));

static cl::opt<bool> DisableIVDEP(
    "disable-ivdep",
    cl::desc("Do not insert IVDEP when sequentializing.\nDefaults to false."),
    cl::cat(HPVMFPGACat), cl::init(false));

static cl::opt<bool> RemoveRestrict(
    "remove-restrict",
    cl::desc("Removes restrict from the leaf nodes.\nDefaults to false."),
    cl::cat(HPVMFPGACat), cl::init(false));

// -nodes - Set of nodes to optimize
static cl::opt<std::string> Nodes(
    "nodes",
    cl::desc(
        "List of nodes (function names) for which optimizations\nshould be "
        "applied, separated by comma;\nLeave empty for all nodes."),
    cl::value_desc("nodes"), cl::init(""), cl::cat(HPVMFPGACat));

// --only-host - only recompile the host code. Must be used with -cxm.
static cl::opt<bool> OnlyHost(
    "only-host",
    cl::desc(
        "Only compile host code.\nIf -cxm is provided, it will be used to set "
        "the AOCX file name in host code generation. Defaults to false."),
    cl::init(false), cl::cat(HPVMFPGACat));

// // Function that writes out the host module
// void writeOutputModule(Module &M, std::string MName) {

//   legacy::PassManager Passes;

//   std::error_code EC;
//   ToolOutputFile Out(MName.c_str(), EC, sys::fs::F_None);
//   if (EC) {
//     LLVM_DEBUG(errs() << EC.message() << "\n";);
//   }

//   Passes.add(createPrintModulePass(Out.os()));

//   Passes.run(M);

//   // Declare success.
//   Out.keep();
// }

// Find all the calls to inline in a particular function
void getCallsToInline(Function *F, std::vector<CallInst *> &CallsToInline) {
  for (auto ib = inst_begin(F), ie = inst_end(F); ib != ie; ++ib) {
    Instruction *I = &*ib;
    if (auto *CI = dyn_cast<CallInst>(I)) {
      Function *CalledF = CI->getCalledFunction();
      if (!CalledF->isDeclaration()) {
        CallsToInline.push_back(CI);
        getCallsToInline(CalledF, CallsToInline);
      }
    }
  }
}

// // Inline all the functions called by a leaf node
// static void InlineFunctions(DFLeafNode *Leaf) {
//   std::vector<CallInst *> CallsToInline;
//   getCallsToInline(Leaf->getFuncPointer(), CallsToInline);
//   for (auto *CI : CallsToInline) {
//     Function *Callee = CI->getFunction();
//     InlineFunctionInfo IFI;
//     InlineFunction(CI, IFI);
//   }
// }

// HPVM DFG Visitor that traverses the graph and applies all the opts to
// each FPGA leaf node.
class TT_FPGAOptimizer : public DFGTreeTraversal {
private:
  const DataLayout &DL;
  std::vector<Opts> OptList;

  // Virtual Functions
  void init() {}
  void initRuntimeAPI() {}
  void process(DFInternalNode *N);
  void process(DFLeafNode *N);

  // helper functions
  bool willFuseAfterUnroll(std::vector<Opts>::iterator i);
  void UnrollOutermostLoop(Function *F);

public:
  TT_FPGAOptimizer(Module &_M, BuildDFG &_DFG, std::vector<Opts> &_OptList)
      : DFGTreeTraversal(_M, _DFG), DL(_M.getDataLayout()), OptList(_OptList) {}

  void addToOptList(Opts _Op) { OptList.push_back(_Op); }
};

bool TT_FPGAOptimizer::willFuseAfterUnroll(std::vector<Opts>::iterator i) {
  bool foundFuse = false;
  do {
    if (*i == fuse) {
      foundFuse = true;
      break;
    }
    i++;
  } while (i != OptList.end());
  return foundFuse;
}

void TT_FPGAOptimizer::process(DFInternalNode *N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName()
                    << "\n");
}

void TT_FPGAOptimizer::UnrollOutermostLoop(Function *F) {

  FunctionAnalysisManager FAM;
  LoopAnalysisManager LAM;
  ModuleAnalysisManager MAM;
  CGSCCAnalysisManager CAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  // PB.registerLoopAnalyses(LAM);
  PB.registerModuleAnalyses(MAM);
  // PB.registerCGSCCAnalyses(CAM);
  PB.crossRegisterProxies(LAM, FAM, CAM, MAM);

  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  auto &TTI = FAM.getResult<TargetIRAnalysis>(*F);

  if (LI.begin()++ == LI.end()) {
    std::cout << "Function does not have single outermost loop!" << std::endl;
    return;
  }
  std::map<BasicBlock *, int> TripCountMap = getFuncLoopTripCounts(F, 0);
  Loop *LOM = *LI.begin();
  auto it = TripCountMap.find(LOM->getHeader());
  if (it != TripCountMap.end()) {
    if (RuntimeUnroll) {
      auto TripMultiple = it->second;
      auto TripCount = it->second;
      auto AllowRuntime = true;
      hpvm::unrollLoopByCount(LOM, LI, DT, SE, AC, TTI, TripCount,
                              (UnrollFactor > TripCount) ? TripCount
                                                         : UnrollFactor,
                              TripMultiple, AllowRuntime);
    }

  } else {
    auto TripCount = SE.getSmallConstantMaxTripCount(LOM);
    assert(TripCount != 0 && "Loop does not contain a trip count indicator!");
    hpvm::unrollLoopByCount(LOM, LI, DT, SE, AC, TTI, TripCount, UnrollFactor);
  }

  FunctionPassManager FPM;
  FPM.addPass(EarlyCSEPass(false)); // triggers some memory error
  FPM.addPass(SimplifyCFGPass());
  FPM.addPass(InstCombinePass());
  FPM.run(*F, FAM);
}

void TT_FPGAOptimizer::process(DFLeafNode *N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName()
                    << "\n");
  // Skip dummy node
  if (N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node\n");
    return;
  }

  // We are only interested in bufferin the arguments of nodes which
  // are targetted on the FPGA.
  if (N->getTargetHint() != hpvm::FPGA_TARGET) {
    LLVM_DEBUG(errs() << "Skipping non-fpga target node!\n");
    return;
  }

  // Get the Node function
  Function *F = N->getFuncPointer();

  if (RemoveRestrict) {
    for (auto &Arg : F->args()) {
      if (Arg.hasAttribute(Attribute::NoAlias)) {
        Arg.removeAttr(Attribute::NoAlias);
      }
    }
  }

  hpvmUtils::inlineFunctions(N);

  ValueToValueMapTy VMap;
  // We should sequentialize the leaf node first
  DFGTransform::Context TContext(*(N->getFuncPointer()->getParent()),
                                 N->getFuncPointer()->getContext());
  LeafSequentializer *LS =
      new LeafSequentializer(TContext, N, VMap, !DisableIVDEP);
  LS->checkAndRun();

  if (LS->hasCloned())
    F = N->getFuncPointer();

  LS->cleanup();

  // Run earlycse, simplifycfg, loopsimplify
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  FunctionPassManager FPM;
  FPM.addPass(EarlyCSEPass(false));
  FPM.addPass(SimplifyCFGPass());
  FPM.addPass(LoopSimplifyPass());
  FPM.run(*F, FAM);
  // auto &LI = FAM.getResult<LoopAnalysis>(*F);
  // auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  // auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  // auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  // Remove all the loop guard branches from this function
  removeFuncLoopGuardBranches(F, true);

  int OptCount = 0;
  for (auto OptI = OptList.begin(); OptI != OptList.end(); ++OptI) {
    auto Opt = *OptI;
    switch (Opt) {
    case bufferin:
      // First, check if we need to sequentialize and flatten this node
      std::cout << "Applying Input Buffering!\n";
      // Buffer the arguments of the node function.
      bufferNodeInputs(N, DL, MaxIBSize);
      if (DebugModules)
        hpvmUtils::writeOutputModule(
            M, inputModuleName + ".opt" + std::to_string(OptCount) + ".bi.ll");
      break;
    case argpriv:
      std::cout << "Applying Argument Privatization!\n";
      // Privatize the node's arguments.
      privatizeNodeArguments(N);
      if (DebugModules)
        hpvmUtils::writeOutputModule(
            M, inputModuleName + ".opt" + std::to_string(OptCount) + ".pv.ll");
      break;
    case unroll:
      std::cout << "Applying Loop Unroll!\n";
      unrollFuncLoops(F, UnrollFactor, PartialUnroll, RuntimeUnroll,
                      OuterUnroll || willFuseAfterUnroll(OptI));
      if (DebugModules)
        hpvmUtils::writeOutputModule(
            M, inputModuleName + ".opt" + std::to_string(OptCount) + ".ur.ll");
      break;
    case outermostunroll:
      std::cout << "Applying Outermost Loop Unroll!\n";
      UnrollOutermostLoop(F);
      if (DebugModules)
        hpvmUtils::writeOutputModule(
            M, inputModuleName + ".opt" + std::to_string(OptCount) + ".our.ll");
      break;
    case fuse:
      std::cout << "Applying Loop Fuse!\n";
      fuseFuncLoops(F);
      if (DebugModules)
        hpvmUtils::writeOutputModule(
            M, inputModuleName + ".opt" + std::to_string(OptCount) + ".fu.ll");
      break;
    default:
      // Anything handled outside this visitor
      break;
    }
  }

  removeNZLoop(F);
  LS->cleanup();
}

class LeafCollector final : public DFTreeTraversal {
public:
  void visit(DFLeafNode *Node) override {
    LLVM_DEBUG(errs() << "Found leaf node: "
                      << Node->getFuncPointer()->getName() << "\n");
    Nodes.push_back(Node);
  }

  const std::vector<DFLeafNode *> &result() const { return Nodes; }

private:
  std::vector<DFLeafNode *> Nodes;
};

int runCommand(std::string Command) {
  std::cout << "Command: " << Command << std::endl;
  FILE *const PFile = popen(Command.c_str(), "r");
  const int max_buffer = 1000;
  char buffer[max_buffer];
  while (fgets(buffer, max_buffer, PFile))
    printf("%s", buffer);
  int status = pclose(PFile);
  return status;
}
int main(int argc, char **argv) {

  InitLLVM X(argc, argv);
  cl::ParseCommandLineOptions(
      argc, argv,
      "HPVM2FPGA Tool. Input bitcode file is application compiled with clang.");
  // Check legality of Command Line Options
  if (Emulation && RTL) {
    hpvmUtils::fatalError("Cannot have both EMULATION and RTL enabled!");
  }

  if (Baseline) {
    DisableIVDEP = true;
    NoTLP = true;
  }
  if (BaselineNoRestrict) {
    DisableIVDEP = true;
    NoTLP = true;
    RemoveRestrict = true;
  }
  // Read in specified LLVM bitcode module
  LLVMContext Context;
  SMDiagnostic Err;
  std::unique_ptr<Module> M = parseIRFile(InputFilename, Err, Context);
  if (!M.get())
    hpvmUtils::fatalError("Failed to open module " + InputFilename);

  inputModuleName = M->getModuleIdentifier();
  if (inputModuleName.find(".ll") != std::string::npos) {
    inputModuleName = inputModuleName.substr(0, inputModuleName.find(".ll"));
  } else if (inputModuleName.find(".bc") != std::string::npos) {
    inputModuleName = inputModuleName.substr(0, inputModuleName.find(".bc"));
  }

  // Initialize the output file module names
  if (HostModuleName == "-") {
    HostModuleName = inputModuleName + ".hpvm.host.ll";
  }
  if (KernelModuleName == "-") {
    KernelModuleName = inputModuleName + ".hpvm.kernels.ll";
  }
  if (OpenCLModuleName == "-") {
    OpenCLModuleName = inputModuleName + ".hpvm.kernels.cl";
  }
  if (AOCXModuleName == "-") {
    AOCXModuleName = inputModuleName + ".hpvm.kernels.aocx";
  }
  std::string AOCRModuleName = AOCXModuleName.getValue();
  AOCRModuleName = AOCRModuleName.replace(AOCRModuleName.size() - 1, 1, "r");

  std::string KernelOptModuleName = inputModuleName + ".hpvm.kernels.opt.ll";
  std::string HPVMModuleName = inputModuleName + ".hpvm.ll";
  std::string KernelPreMemCpyModuleName =
      inputModuleName + ".hpvm.kernels.prememcpy.ll";
  // Run GenHPVM and BuildDFG on the module
  legacy::PassManager Passes;
  BuildDFG *DFG = new BuildDFG();
  Passes.add(new GenHPVM());
  Passes.add(DFG);
  Passes.run(*M);

  M->setModuleIdentifier(HPVMModuleName);

  if (DebugModules)
    hpvmUtils::writeOutputModule(*M, inputModuleName + ".hpvm.ll");

  // Get the DFG Roots
  std::vector<DFInternalNode *> Roots = DFG->getRoots();

  if (OnlyHost)
    OptimizationList.clear();

  if (std::find(OptimizationList.begin(), OptimizationList.end(), nodefuse) !=
      OptimizationList.end()) {
    LLVM_DEBUG(errs() << "Node fusion enabled\n");

    unsigned Fusions = 0;
    // Run iteratively until no more fusions are possible
    while (true) {
      DFInternalNode *Root = DFG->getRoot();
      LeafFinder LFinder(*M, *DFG, FPGA_TARGET, Nodes.getValue());
      LFinder.visit(Root);
      const std::vector<DFLeafNode *> &Nodes = LFinder.getLeaves();

      bool DidFuse = false;
      DFGTransform::Context TContext(*M, M->getContext());
      for (auto it1 = Nodes.begin(); !DidFuse && it1 != Nodes.end(); ++it1) {
        for (auto it2 = it1 + 1; !DidFuse && it2 != Nodes.end(); ++it2) {
          DFLeafNode *N1 = *it1;
          DFLeafNode *N2 = *it2;
          StringRef name1 = N1->getFuncPointer()->getName();
          ;
          StringRef name2 = N2->getFuncPointer()->getName();
          Fuse Fusor(TContext, {N1, N2});
          LLVM_DEBUG(errs() << "Testing " << name1 << " and " << name2 << "\n");
          if (Fusor.canRun()) {
            LLVM_DEBUG(errs()
                       << "Fusing " << name1 << " and " << name2 << "\n");
            Fusor.run();
            DidFuse = true;
            ++Fusions;

            // End the inner loop since the graph structure may be invalidated
            it1 = Nodes.end();
            it2 = Nodes.end();
          }
        }
      }
      if (!DidFuse)
        break;
    }
    errs() << "Applied " << Fusions << " node fusions\n";
  } else {
    LLVM_DEBUG(errs() << "Node fusion disabled\n");
  }

  // if (OptimizationList.size() > 0) {
  // Create the DFG Visitor to apply optimizations on the
  // leaf nodes
  TT_FPGAOptimizer *TTVisitor =
      new TT_FPGAOptimizer(*M, *DFG, OptimizationList);
  for (auto rootNode : Roots) {
    TTVisitor->visit(rootNode);
  }
  // }

  if (DebugModules)
    hpvmUtils::writeOutputModule(*M, inputModuleName + ".opt.ll");

  // Run Backends
  runDFG2LLVM_FPGA(*M, *DFG, !NoTLP.getValue(), Emulation.getValue(), OnlyHost,
                   KernelModuleName, AOCXModuleName, Board.c_str());
  if (DebugModules)
    hpvmUtils::writeOutputModule(*M, inputModuleName + ".dfg2llvmfpga.ll");
  runDFG2LLVM_CPU(*M, *DFG);
  if (DebugModules)
    hpvmUtils::writeOutputModule(*M, inputModuleName + ".dfg2llvmcpu.ll");

  // Clear the DFG
  runClearDFG(*M, *DFG);

  // Make sure that the module is not broken!
  assert(!verifyModule(*M, &errs(), 0) &&
         "Kernel Module is Broken! Aborting!\n");

  hpvmUtils::writeOutputModule(*M, HostModuleName);

  if (!OnlyHost) {
    FunctionAnalysisManager FAM;
    LoopAnalysisManager LAM;
    ModuleAnalysisManager MAM;
    CGSCCAnalysisManager CAM;
    PassBuilder PB;
    PB.registerFunctionAnalyses(FAM);
    PB.registerLoopAnalyses(LAM);
    PB.registerModuleAnalyses(MAM);
    PB.registerCGSCCAnalyses(CAM);
    PB.crossRegisterProxies(LAM, FAM, CAM, MAM);
    // Invoke llvm-ocl
    const std::string Command = std::string(LLVM_BUILD_DIR_STR) + "/bin/opt --lowerswitch " +
                                KernelModuleName + " -S -o " + KernelOptModuleName;
    std::cout << "Optimizing before cbe" << std::endl;
    int status = runCommand(Command);
    if (status != 0)
      hpvmUtils::fatalError("Error in opt");
    std::string CBECommand =
        std::string(LLVM_BUILD_DIR_STR) + "/bin/llvm-ocl " +
        KernelOptModuleName + " -o " + OpenCLModuleName;
    std::cout << "Invoking llvm-ocl" << std::endl;
    status = runCommand(CBECommand);
    if (status != 0)
      hpvmUtils::fatalError("Error in llvm-ocl");

    // Invoke aoc
    if (RunAOC) {
      std::string EmulatorStr = (Emulation ? " -march=emulator " : " ");
      std::string RTLStr = (RTL ? " -rtl " : " ");
      std::string OutModule =
          (RTL.getValue() ? AOCRModuleName : AOCXModuleName.getValue());
      const std::string CommandAOC = "aoc" + EmulatorStr + RTLStr +
                                     OpenCLModuleName + " -o " + OutModule +
                                     " -board=" + Board;
      std::cout << "Invoking aoc" << std::endl;
      int status = runCommand(CommandAOC);
      std::cout << "status: " << status << std::endl;
      if (status != 0)
        hpvmUtils::fatalError("aoc failed!");
    }
  }
  return 0;
}
