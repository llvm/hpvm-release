Tests
========================

Directory Organization
----------------------

The ``hpvm/test`` directory holds all tests in HPVM and is organized as follows:

* ``hpvm_pass/``: unit and regression tests for HPVM Passes, written in LLVM-bitcode.

* ``hpvm-tensor-rt/``: unit tests for the HPVM ``tensor_runtime``.
  This folder just contains the test fixtures and the test files to run.
  The actual test cases live under ``${project_root}/hpvm/projects/hpvm-tensor-rt/tests/``.
  
* ``hpvm_c_dnn/``: contains the HPVM-C version of ten (10) DNN benchmarks supported by ApproxHPVM. Each subfolder contains a DNN with 2 versions (2 ``.cpp`` files):
  the ``tensor``-targeted version which compiles to ``tensor_runtime``,
  and the ``cudnn``-targeted version which compiles to operators in ``cuDNN``
  (has ``_cudnn`` in name).


Running Test Cases
------------------

The easiest way to run tests is to use ``make`` targets,
which will also take care of all compilation of test cases and test fixtures.
The following targets runs these tests respectively:

* ``make -j check-hpvm-pass`` runs tests in ``hpvm_pass``: ``hpvm_pass/**/*.ll``.
  These are regression and unit tests for HPVM passes.

* ``make -j check-hpvm-tensor-rt`` checks the approximation implementations of ``tensor_runtime``.

* ``make -j check-hpvm-dnn`` runs all 20 DNN benchmarks under ``hpvm_c_dnn``
  (10 DNNs x 2 versions) and validates their accuracy.

  **Note** that this can take quite long due to the size of DNNs and datasets.
  Depending on your hardware capability, this test can take 5-30 minutes.
  Also, this is set to run sequentially out of GPU memory concerns.

These tests all use ``llvm-lit``.