 ; RUN: opt -load HPVMBuildDFG.so -load HPVMDFG2LLVM_FPGA.so -enable-new-pm=0 -S -dfg2llvm-fpga --hpvm-fpga-tlp=false <  %s | FileCheck %s
; ModuleID = 'test.ll'
source_filename = "test.cc"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%DFGArgs = type <{ i32*, i64, i32*, i64, i32*, i64 }>
%struct.out._Z9root_nodePimS_mS_m_extracted_reorder = type <{ i32*, i64, i32*, i64, i32*, i64 }>
%struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder = type <{ i32*, i64 }>
%struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder = type <{ i32*, i64 }>
%struct.out._Z9root_nodePimS_mS_m_extracted_3_reorder = type <{ i32*, i64, i32*, i64 }>
%emptyStruct = type <{}>

declare dso_local void @__hetero_hint(i32) local_unnamed_addr #0

; CHECK-LABEL: i32 @main(
; CHECK: call i8* @llvm_hpvm_ocl_initContext(i32
; CHECK-NEXT: call i8* @llvm_hpvm_ocl_launch(i8*
; CHECK-SAME: i1 false, i8*
; CHECK: call i8* @llvm_hpvm_ocl_launch(i8*
; CHECK-SAME: i1 false, i8*
; CHECK: call i8* @llvm_hpvm_ocl_launch(i8*
; CHECK-SAME: i1 false, i8*
; CHECK: call void @llvm_hpvm_ocl_clearContext(i8*

; CHECK-LABEL: @Z9root_nodePimS_mS_m_extracted_reorder_c(i32*
; CHECK-LABEL: @Z9root_nodePimS_mS_m_extracted_1_reorder_c(i32*
; CHECK-LABEL: @Z9root_nodePimS_mS_m_extracted_2_reorder_c(i32*
; CHECK-LABEL: @_Z9root_nodePimS_mS_m_extracted_3_reorder_c(i32*
; CHECK-LABEL: @_Z9root_nodePimS_mS_m_c(i32*
; CHECK-LABEL: @_Z9root_nodePimS_mS_m_extracted_3_reorder_c.4_c_c_c_c_c_c(i32*

; CHECK: call i8* @llvm_hpvm_ocl_argument_ptr(i8*
; CHECK: call void @llvm_hpvm_ocl_argument_scalar(i8* 
; CHECK: call i8* @llvm_hpvm_ocl_argument_ptr(i8*
; CHECK: call void @llvm_hpvm_ocl_argument_scalar(i8* 
; CHECK: call i8* @llvm_hpvm_ocl_argument_ptr(i8*
; CHECK:call void @llvm_hpvm_ocl_argument_scalar(i8* 
 

; CHECK: call i8* @llvm_hpvm_ocl_executeNode(i8*
; CHECK-SAME: i32 -1, i32* %waitlist

; CHECK: call i8* @llvm_hpvm_ocl_argument_ptr(i8*
; CHECK: call void @llvm_hpvm_ocl_argument_scalar(i8* 
; CHECK: call i8* @llvm_hpvm_ocl_argument_ptr(i8*
; CHECK: call void @llvm_hpvm_ocl_argument_scalar(i8*

; CHECK-NOT: store i32 0, i32* %waitlist.Z9root_nodePimS_mS_m_extracted_1_reorder_c.0
; CHECK: call i8* @llvm_hpvm_ocl_executeNode(i8*
; CHECK-SAME: i32* %waitlist.Z9root_nodePimS_mS_m_extracted_1_reorder_c.0, i32 0



; Function Attrs: mustprogress norecurse uwtable
define dso_local i32 @main() local_unnamed_addr #1 {
entry:
  call void @llvm.hpvm.init()
  %call = call noalias align 16 dereferenceable_or_null(200) i8* @malloc(i64 200) #7
  %call1 = call noalias align 16 dereferenceable_or_null(200) i8* @malloc(i64 200) #7
  %call2 = call noalias align 16 dereferenceable_or_null(200) i8* @malloc(i64 200) #7
  %0 = bitcast i8* %call2 to i32*
  %1 = bitcast i8* %call to i32*
  %2 = bitcast i8* %call1 to i32*
  %3 = alloca %DFGArgs, align 8
  %4 = getelementptr inbounds %DFGArgs, %DFGArgs* %3, i32 0, i32 0
  store i32* %0, i32** %4, align 8
  %5 = getelementptr inbounds %DFGArgs, %DFGArgs* %3, i32 0, i32 1
  store i64 200, i64* %5, align 8
  %6 = getelementptr inbounds %DFGArgs, %DFGArgs* %3, i32 0, i32 2
  store i32* %1, i32** %6, align 8
  %7 = getelementptr inbounds %DFGArgs, %DFGArgs* %3, i32 0, i32 3
  store i64 200, i64* %7, align 8
  %8 = getelementptr inbounds %DFGArgs, %DFGArgs* %3, i32 0, i32 4
  store i32* %2, i32** %8, align 8
  %9 = getelementptr inbounds %DFGArgs, %DFGArgs* %3, i32 0, i32 5
  store i64 200, i64* %9, align 8
  %10 = bitcast i32* %0 to i8*
  call void @llvm_hpvm_track_mem(i8* %10, i64 200)
  %11 = bitcast i32* %1 to i8*
  call void @llvm_hpvm_track_mem(i8* %11, i64 200)
  %12 = bitcast i32* %2 to i8*
  call void @llvm_hpvm_track_mem(i8* %12, i64 200)
  %13 = bitcast i8* %call2 to i8*
  %14 = bitcast %DFGArgs* %3 to i8*
  %graphID = call i8* @llvm.hpvm.launch(i8* bitcast (%emptyStruct (i32*, i64, i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_c to i8*), i8* %14, i1 false)
  call void @llvm.hpvm.wait(i8* %graphID)
  call void @llvm_hpvm_request_mem(i8* %13, i64 200)
  call void @llvm_hpvm_untrack_mem(i8* %10)
  call void @llvm_hpvm_untrack_mem(i8* %11)
  call void @llvm_hpvm_untrack_mem(i8* %12)
  call void @free(i8* %call) #7
  call void @free(i8* %call1) #7
  call void @free(i8* %call2) #7
  call void @llvm.hpvm.cleanup()
  ret i32 0
}

; Function Attrs: inaccessiblememonly mustprogress nofree nounwind willreturn
declare dso_local noalias noundef align 16 i8* @malloc(i64 noundef) local_unnamed_addr #2

declare dso_local i8* @__hetero_launch(i8*, ...) local_unnamed_addr #0

declare dso_local void @__hetero_wait(i8*) local_unnamed_addr #0

; Function Attrs: inaccessiblemem_or_argmemonly mustprogress nounwind willreturn
declare dso_local void @free(i8* nocapture noundef) local_unnamed_addr #3

; Function Attrs: nofree nounwind
declare dso_local noundef i32 @printf(i8* nocapture noundef readonly, ...) #4

; Function Attrs: nounwind
declare dso_local void @__hpvm__hint(i32) #5

; Function Attrs: nounind
declare dso_local i8* @__hpvm__createNodeND(i32, ...) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__return(i32, ...) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__attributes(i32, ...) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__init() #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__cleanup() #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__bindIn(i8*, i32, i32, i32) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__bindOut(i8*, i32, i32, i32) #5

; Function Attrs: nounwind
declare dso_local i8* @__hpvm__edge(i8*, i8*, i32, i32, i32, i32) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__push(i8*, i8*) #5

; Function Attrs: nounwind
declare dso_local i8* @__hpvm__pop(i8*) #5

; Function Attrs: nounwind
declare dso_local i8* @__hpvm__launch(i32, ...) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__wait(i8*) #5

; Function Attrs: nounwind
declare dso_local i8* @__hpvm__getNode() #5

; Function Attrs: nounwind
declare dso_local i8* @__hpvm__getParentNode(i8*) #5

; Function Attrs: nounwind
declare dso_local void @__hpvm__barrier() #5

; Function Attrs: nounwind
declare dso_local i8* @__hpvm__malloc(i64) #5

; Function Attrs: nounwind
declare dso_local i64 @__hpvm__getNodeInstanceID_x(i8*) #5

; Function Attrs: nounwind
declare dso_local i64 @__hpvm__getNodeInstanceID_y(i8*) #5

; Function Attrs: nounwind
declare dso_local i64 @__hpvm__getNodeInstanceID_z(i8*) #5

; Function Attrs: nounwind
declare dso_local i64 @__hpvm__getNumNodeInstances_x(i8*) #5

; Function Attrs: nounwind
declare dso_local i64 @__hpvm__getNumNodeInstances_y(i8*) #5

; Function Attrs: nounwind
declare dso_local i64 @__hpvm__getNumNodeInstances_z(i8*) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_add(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_sub(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_xchg(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_inc(i32*) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_dec(i32*) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_min(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_max(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_and(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_or(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local i32 @__hpvm__atomic_xor(i32*, i32) #5

; Function Attrs: nounwind
declare dso_local void @llvm_hpvm_track_mem(i8*, i64) #5

; Function Attrs: nounwind
declare dso_local void @llvm_hpvm_untrack_mem(i8*) #5

; Function Attrs: nounwind
declare dso_local void @llvm_hpvm_request_mem(i8*, i64) #5

; Function Attrs: mustprogress uwtable
define dso_local %struct.out._Z9root_nodePimS_mS_m_extracted_reorder @_Z9root_nodePimS_mS_m_extracted_reorder_c(i32* in out %ACC, i64 %ACC_Size, i32* in out %V1, i64 %V1_Size, i32* in out %V2, i64 %V2_Size) #6 {
newFuncRoot_cloned:
  %arrayidx_cloned = getelementptr inbounds i32, i32* %V1, i64 1
  %0 = load i32, i32* %arrayidx_cloned, align 4, !tbaa !8
  %arrayidx4_cloned = getelementptr inbounds i32, i32* %V2, i64 2
  %1 = load i32, i32* %arrayidx4_cloned, align 4, !tbaa !8
  %add_cloned = add nsw i32 %1, %0
  store i32 %add_cloned, i32* %ACC, align 4, !tbaa !8
  %returnStruct = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_reorder undef, i32* %ACC, 0
  %returnStruct1 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_reorder %returnStruct, i64 %ACC_Size, 1
  %returnStruct12 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_reorder %returnStruct1, i32* %V1, 2
  %returnStruct123 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_reorder %returnStruct12, i64 %V1_Size, 3
  %returnStruct1234 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_reorder %returnStruct123, i32* %V2, 4
  %returnStruct12345 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_reorder %returnStruct1234, i64 %V2_Size, 5
  ret %struct.out._Z9root_nodePimS_mS_m_extracted_reorder %returnStruct12345
}

; Function Attrs: mustprogress uwtable
define dso_local %struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder @_Z9root_nodePimS_mS_m_extracted_1_reorder_c(i32* in %ACC, i64 %ACC_Size, i32* in out %V1, i64 %V1_Size) #6 {
newFuncRoot_cloned:
  %arrayidx_clone_cloned = getelementptr inbounds i32, i32* %V1, i64 1
  %0 = load i32, i32* %ACC, align 4, !tbaa !8
  %1 = load i32, i32* %arrayidx_clone_cloned, align 4, !tbaa !8
  %add9_cloned = add nsw i32 %1, %0
  %arrayidx10_cloned = getelementptr inbounds i32, i32* %V1, i64 2
  store i32 %add9_cloned, i32* %arrayidx10_cloned, align 4, !tbaa !8
  %returnStruct = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder undef, i32* %V1, 0
  %returnStruct1 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder %returnStruct, i64 %V1_Size, 1
  ret %struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder %returnStruct1
}

; Function Attrs: mustprogress uwtable
define dso_local %struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder @_Z9root_nodePimS_mS_m_extracted_2_reorder_c(i32* in %ACC, i64 %ACC_Size, i32* in out %V2, i64 %V2_Size) #6 {
newFuncRoot_cloned:
  %arrayidx4_clone_cloned = getelementptr inbounds i32, i32* %V2, i64 2
  %0 = load i32, i32* %ACC, align 4, !tbaa !8
  %1 = load i32, i32* %arrayidx4_clone_cloned, align 4, !tbaa !8
  %add14_cloned = add nsw i32 %1, %0
  %arrayidx15_cloned = getelementptr inbounds i32, i32* %V2, i64 4
  store i32 %add14_cloned, i32* %arrayidx15_cloned, align 4, !tbaa !8
  %returnStruct = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder undef, i32* %V2, 0
  %returnStruct1 = insertvalue %struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder %returnStruct, i64 %V2_Size, 1
  ret %struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder %returnStruct1
}

; Function Attrs: nounwind
declare i8* @llvm.hpvm.createNode1D(i8*, i64) #7

; Function Attrs: nounwind
declare void @llvm.hpvm.bind.input(i8*, i32, i32, i1) #7

; Function Attrs: nounwind
declare i8* @llvm.hpvm.createEdge(i8*, i8*, i1, i32, i32, i1) #7

; Function Attrs: nounwind
declare void @llvm.hpvm.bind.output(i8*, i32, i32, i1) #7

; Function Attrs: mustprogress uwtable
define dso_local %struct.out._Z9root_nodePimS_mS_m_extracted_3_reorder @_Z9root_nodePimS_mS_m_extracted_3_reorder_c(i32* in out %ACC, i64 %ACC_Size, i32* in out %V1, i64 %V1_Size, i32* in out %V2, i64 %V2_Size) #6 {
newFuncRoot_cloned:
  %_Z9root_nodePimS_mS_m_extracted_reorder_c.node = call i8* @llvm.hpvm.createNode1D(i8* bitcast (%struct.out._Z9root_nodePimS_mS_m_extracted_reorder (i32*, i64, i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_reorder_c to i8*), i64 1)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i32 0, i32 0, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i32 1, i32 1, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i32 2, i32 2, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i32 3, i32 3, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i32 4, i32 4, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i32 5, i32 5, i1 false)
  %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node = call i8* @llvm.hpvm.createNode1D(i8* bitcast (%struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder (i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_1_reorder_c to i8*), i64 1)
  %output = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node, i1 true, i32 3, i32 3, i1 false)
  %output1 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node, i1 true, i32 2, i32 2, i1 false)
  %output2 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node, i1 true, i32 1, i32 1, i1 false)
  %output3 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node, i1 true, i32 0, i32 0, i1 false)
  call void @llvm.hpvm.bind.output(i8* %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node, i32 0, i32 0, i1 false)
  call void @llvm.hpvm.bind.output(i8* %_Z9root_nodePimS_mS_m_extracted_1_reorder_c.node, i32 1, i32 1, i1 false)
  %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node = call i8* @llvm.hpvm.createNode1D(i8* bitcast (%struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder (i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_2_reorder_c to i8*), i64 1)
  %output4 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node, i1 true, i32 5, i32 3, i1 false)
  %output5 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node, i1 true, i32 4, i32 2, i1 false)
  %output6 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node, i1 true, i32 1, i32 1, i1 false)
  %output7 = call i8* @llvm.hpvm.createEdge(i8* %_Z9root_nodePimS_mS_m_extracted_reorder_c.node, i8* %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node, i1 true, i32 0, i32 0, i1 false)
  call void @llvm.hpvm.bind.output(i8* %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node, i32 0, i32 2, i1 false)
  call void @llvm.hpvm.bind.output(i8* %_Z9root_nodePimS_mS_m_extracted_2_reorder_c.node, i32 1, i32 3, i1 false)
  ret %struct.out._Z9root_nodePimS_mS_m_extracted_3_reorder undef
}

; Function Attrs: nounwind
declare i8* @llvm.hpvm.createNode(i8*) #7

; Function Attrs: nounwind
declare void @llvm.hpvm.init() #7

; Function Attrs: nounwind
declare i8* @llvm.hpvm.launch(i8*, i8*, i1) #7

; Function Attrs: mustprogress uwtable
define dso_local %emptyStruct @_Z9root_nodePimS_mS_m_c(i32* in out %ACC, i64 %ACC_Size, i32* in out %V1, i64 %V1_Size, i32* in out %V2, i64 %V2_Size) #6 {
entry:
  %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node = call i8* @llvm.hpvm.createNode(i8* bitcast (%struct.out._Z9root_nodePimS_mS_m_extracted_3_reorder (i32*, i64, i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_3_reorder_c to i8*))
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node, i32 0, i32 0, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node, i32 1, i32 1, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node, i32 2, i32 2, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node, i32 3, i32 3, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node, i32 4, i32 4, i1 false)
  call void @llvm.hpvm.bind.input(i8* %_Z9root_nodePimS_mS_m_extracted_3_reorder_c.node, i32 5, i32 5, i1 false)
  ret %emptyStruct undef
}

; Function Attrs: nounwind
declare void @llvm.hpvm.wait(i8*) #7

; Function Attrs: nounwind
declare void @llvm.hpvm.cleanup() #7

attributes #0 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { mustprogress norecurse uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { inaccessiblememonly mustprogress nofree nounwind willreturn "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { inaccessiblemem_or_argmemonly mustprogress nounwind willreturn "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #4 = { nofree nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #6 = { mustprogress uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}
!hpvm_hint_fpga = !{!3, !4, !5}
!hpvm_hint_gpu = !{}
!hpvm_hint_cpu = !{!6, !7}
!hpvm_hint_cpu_gpu = !{}
!hpvm_hint_cudnn = !{}
!hpvm_hint_promise = !{}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git 553426bd58f1a103dbaad3430d7c248e3b5dd68c)"}
!3 = !{%struct.out._Z9root_nodePimS_mS_m_extracted_reorder (i32*, i64, i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_reorder_c}
!4 = !{%struct.out._Z9root_nodePimS_mS_m_extracted_1_reorder (i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_1_reorder_c}
!5 = !{%struct.out._Z9root_nodePimS_mS_m_extracted_2_reorder (i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_2_reorder_c}
!6 = !{%struct.out._Z9root_nodePimS_mS_m_extracted_3_reorder (i32*, i64, i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_extracted_3_reorder_c}
!7 = !{%emptyStruct (i32*, i64, i32*, i64, i32*, i64)* @_Z9root_nodePimS_mS_m_c}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !10, i64 0}
!10 = !{!"omnipotent char", !11, i64 0}
!11 = !{!"Simple C++ TBAA"}
