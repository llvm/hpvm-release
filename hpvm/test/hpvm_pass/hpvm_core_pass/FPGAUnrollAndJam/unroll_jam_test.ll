; RUN: opt -enable-new-pm=0 -load HPVMGenHPVM.so -genhpvm -load HPVMBuildDFG.so -buildDFG -load HPVMUnrollAndJam.so -hpvm-unrollandjam -ut=5 -S < %s | FileCheck %s
; ModuleID = 'fuj_two_test.c'
source_filename = "fuj_two_test.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.RootArg = type { i32*, i64, i32*, i64 }

; CHECK-LABEL: %struct.out.leaf_node_1 @leaf_node_1_c
; CHECK-NOT: for (
; CHECK: load {{.*}} %input
; CHECK: load {{.*}} %output
; CHECK: store {{.*}} %output
; CHECK-NOT: for (
; CHECK: store {{.*}} %output
; CHECK-NOT: for (
; CHECK: store {{.*}} %output
; CHECK-NOT: for (
; CHECK: store {{.*}} %output
; CHECK-NOT: for (
; CHECK: store {{.*}} %output
; CHECK-NOT: for (
; CHECK-NEXT: getelementptr {{.*}} %input, i64 1
; CHECK-NEXT: getelementptr {{.*}} %output, i64 1
; CHECK-NOT: for (
; CHECK: load {{.*}} %arrayidx
; CHECK-NEXT: load {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK-NEXT: getelementptr {{.*}} %input, i64 2
; CHECK-NEXT: getelementptr {{.*}} %output, i64 2
; CHECK-NOT: for (
; CHECK: load {{.*}} %arrayidx
; CHECK-NEXT: load {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK-NOT: for (
; CHECK: store {{.*}} %arrayidx6
; CHECK: ret



@str = private unnamed_addr constant [14 x i8] c"Starting HPVM\00", align 1
@str.3 = private unnamed_addr constant [16 x i8] c"Starting Launch\00", align 1
@str.4 = private unnamed_addr constant [14 x i8] c"HPVM finished\00", align 1

; Function Attrs: nounwind uwtable
define dso_local void @leaf_node_1(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 7) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  br label %for.cond1.preheader

for.cond1.preheader:                              ; preds = %entry, %for.cond.cleanup3
  %indvars.iv = phi i64 [ 0, %entry ], [ %indvars.iv.next, %for.cond.cleanup3 ]
  %arrayidx = getelementptr inbounds i32, i32* %input, i64 %indvars.iv
  %arrayidx6 = getelementptr inbounds i32, i32* %output, i64 %indvars.iv
  br label %for.body4

for.cond.cleanup:                                 ; preds = %for.cond.cleanup3
  call void (i32, ...) @__hpvm__return(i32 1, i32* %output) #4
  ret void

for.cond.cleanup3:                                ; preds = %for.body4
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond22.not = icmp eq i64 %indvars.iv.next, 3
  br i1 %exitcond22.not, label %for.cond.cleanup, label %for.cond1.preheader, !llvm.loop !3

for.body4:                                        ; preds = %for.cond1.preheader, %for.body4
  %j.020 = phi i32 [ 0, %for.cond1.preheader ], [ %inc, %for.body4 ]
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %1 = load i32, i32* %arrayidx6, align 4, !tbaa !6
  %add = add nsw i32 %1, %0
  store i32 %add, i32* %arrayidx6, align 4, !tbaa !6
  %inc = add nuw nsw i32 %j.020, 1
  %exitcond.not = icmp eq i32 %inc, 5
  br i1 %exitcond.not, label %for.cond.cleanup3, label %for.body4, !llvm.loop !10
}

declare dso_local void @__hpvm__hint(i32) local_unnamed_addr #1

declare dso_local void @__hpvm__attributes(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__return(i32, ...) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @internal_node_1(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64)* nonnull @leaf_node_1, i64 8) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

declare dso_local i8* @__hpvm__createNodeND(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__bindIn(i8*, i32, i32, i32) local_unnamed_addr #1

declare dso_local void @__hpvm__bindOut(i8*, i32, i32, i32) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @root(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 0, void (i32*, i64, i32*, i64)* nonnull @internal_node_1) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readnone %argv) local_unnamed_addr #0 {
entry:
  %call = call noalias align 16 dereferenceable_or_null(32) i8* @malloc(i64 32) #4
  %0 = bitcast i8* %call to %struct.RootArg*
  %in_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 1
  store i64 64, i64* %in_bytes, align 8, !tbaa !11
  %call2 = call noalias align 16 dereferenceable_or_null(256) i8* @malloc(i64 256) #4
  %1 = bitcast i8* %call to i8**
  store i8* %call2, i8** %1, align 16, !tbaa !15
  %out_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 3
  store i64 64, i64* %out_bytes, align 8, !tbaa !16
  %call5 = call noalias align 16 dereferenceable_or_null(256) i8* @malloc(i64 256) #4
  %output = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 2
  %2 = bitcast i32** %output to i8**
  store i8* %call5, i8** %2, align 16, !tbaa !17
  %puts = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([14 x i8], [14 x i8]* @str, i64 0, i64 0))
  call void (...) @__hpvm__init() #4
  %3 = load i8*, i8** %1, align 16, !tbaa !15
  %4 = load i64, i64* %in_bytes, align 8, !tbaa !11
  call void @llvm_hpvm_track_mem(i8* %3, i64 %4) #4
  %5 = load i8*, i8** %2, align 16, !tbaa !17
  %6 = load i64, i64* %out_bytes, align 8, !tbaa !16
  call void @llvm_hpvm_track_mem(i8* %5, i64 %6) #4
  %puts34 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([16 x i8], [16 x i8]* @str.3, i64 0, i64 0))
  %call12 = call i8* (i32, ...) @__hpvm__launch(i32 0, void (i32*, i64, i32*, i64)* nonnull @root, i8* %call) #4
  call void @__hpvm__wait(i8* %call12) #4
  %7 = load i8*, i8** %2, align 16, !tbaa !17
  %8 = load i64, i64* %out_bytes, align 8, !tbaa !16
  call void @llvm_hpvm_request_mem(i8* %7, i64 %8) #4
  %9 = load i8*, i8** %1, align 16, !tbaa !15
  call void @llvm_hpvm_untrack_mem(i8* %9) #4
  %10 = load i8*, i8** %2, align 16, !tbaa !17
  call void @llvm_hpvm_untrack_mem(i8* %10) #4
  call void (...) @__hpvm__cleanup() #4
  %puts35 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([14 x i8], [14 x i8]* @str.4, i64 0, i64 0))
  ret i32 0
}

; Function Attrs: inaccessiblememonly mustprogress nofree nounwind willreturn
declare dso_local noalias noundef align 16 i8* @malloc(i64 noundef) local_unnamed_addr #2

declare dso_local void @__hpvm__init(...) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_track_mem(i8*, i64) local_unnamed_addr #1

declare dso_local i8* @__hpvm__launch(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__wait(i8*) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_request_mem(i8*, i64) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_untrack_mem(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm__cleanup(...) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare noundef i32 @puts(i8* nocapture noundef readonly) local_unnamed_addr #3

attributes #0 = { nounwind uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { inaccessiblememonly mustprogress nofree nounwind willreturn "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nofree nounwind }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git af5a65906558294f157e2bbf69151b14b9d1becd)"}
!3 = distinct !{!3, !4, !5}
!4 = !{!"llvm.loop.mustprogress"}
!5 = !{!"llvm.loop.unroll.disable"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !8, i64 0}
!8 = !{!"omnipotent char", !9, i64 0}
!9 = !{!"Simple C/C++ TBAA"}
!10 = distinct !{!10, !4, !5}
!11 = !{!12, !14, i64 8}
!12 = !{!"", !13, i64 0, !14, i64 8, !13, i64 16, !14, i64 24}
!13 = !{!"any pointer", !8, i64 0}
!14 = !{!"long", !8, i64 0}
!15 = !{!12, !13, i64 0}
!16 = !{!12, !14, i64 24}
!17 = !{!12, !13, i64 16}
