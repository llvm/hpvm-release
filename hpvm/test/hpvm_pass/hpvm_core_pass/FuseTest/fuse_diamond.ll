; RUN: opt -enable-new-pm=0 -load HPVMIRCodeGen.so -genhpvm -load HPVMTargets.so -buildDFG -load HPVMDFGTransformPasses.so -fusetest -dfg2llvm-cpu -clearDFG -S < %s | FileCheck --implicit-check-not @mul_5 --implicit-check-not @mul_3 %s
; ModuleID = 'FusableH.c'
source_filename = "FusableH.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.RootArg = type { i32*, i64, i32*, i64, i64, i32*, i64, i32*, i64 }

; Diamond-shaped graph test.

; Data structures. No named struct is expected currently.
; CHECK-LABEL: %struct.RootArg =
; CHECK: %struct.out.sum = type <{ i32*, i64 }>

; Fused function signature
; CHECK-LABEL: define dso_local { i32*, i64, i32*, i64 } @Fused_mul_5_c_mul_3_c
; CHECK-SAME: i32*
; CHECK-SAME: i64
; CHECK-SAME: i32*
; CHECK-SAME: i64
; CHECK-SAME: i32*
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64

; Check that original operations appear
; CHECK: getelementptr inbounds i32, i32* %input, i64 %idx_x
; CHECK: load i32
; CHECK: mul nsw i32
; CHECK-SAME: 5
; CHECK: getelementptr inbounds i32, i32* %output
; CHECK-SAME: i64 %idx_x
; CHECK: store i32
; CHECK: br label

; CHECK: getelementptr inbounds i32, i32* %input, i64 %idx_x
; CHECK: load i32
; CHECK: mul nsw i32
; CHECK-SAME 3
; CHECK: getelementptr inbounds i32, i32* %output
; CHECK-SAME: i64 %idx_x
; CHECK: store i32

; Combined outputs
; CHECK: insertvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 0
; CHECK: insertvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 1
; CHECK: insertvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 2
; CHECK: insertvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 3
; CHECK: ret { i32*, i64, i32*, i64 }

; Call site
; CHECK-LABEL: call { i32*, i64, i32*, i64 } @Fused_mul_5_c_mul_3_c

; Out edges
; CHECK: extractvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 0
; CHECK: extractvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 1
; CHECK: extractvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 2
; CHECK: extractvalue { i32*, i64, i32*, i64 }
; CHECK-SAME: 3


@.str.3 = private unnamed_addr constant [10 x i8] c"%lu/=%lu\0A\00", align 1
@str = private unnamed_addr constant [17 x i8] c"Doing hpvm stuff\00", align 1
@str.4 = private unnamed_addr constant [20 x i8] c"Finished hpvm stuff\00", align 1
@str.5 = private unnamed_addr constant [2 x i8] c"*\00", align 1

; Function Attrs: nounwind uwtable
define dso_local void @init_input(i32* %input, i64 %in_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 0, i32 1, i32* %input) #6
  %call = call i8* (...) @__hpvm__getNode() #6
  %call1 = call i64 @__hpvm__getNodeInstanceID_x(i8* %call) #6
  %conv = trunc i64 %call1 to i32
  %arrayidx = getelementptr inbounds i32, i32* %input, i64 %call1
  store i32 %conv, i32* %arrayidx, align 4, !tbaa !3
  call void (i32, ...) @__hpvm__return(i32 2, i32* %input, i64 %in_bytes) #6
  ret void
}

declare dso_local void @__hpvm__hint(i32) local_unnamed_addr #1

declare dso_local void @__hpvm__attributes(i32, ...) local_unnamed_addr #1

; Function Attrs: argmemonly mustprogress nofree nosync nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare dso_local i8* @__hpvm__getNode(...) local_unnamed_addr #1

declare dso_local i64 @__hpvm__getNodeInstanceID_x(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm__return(i32, ...) local_unnamed_addr #1

; Function Attrs: argmemonly mustprogress nofree nosync nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind uwtable
define dso_local void @mul_5(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 1, i32* %input, i32 1, i32* %output) #6
  %call = call i8* (...) @__hpvm__getNode() #6
  %call1 = call i64 @__hpvm__getNodeInstanceID_x(i8* %call) #6
  %arrayidx = getelementptr inbounds i32, i32* %input, i64 %call1
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !3
  %mul = mul nsw i32 %0, 5
  %arrayidx2 = getelementptr inbounds i32, i32* %output, i64 %call1
  store i32 %mul, i32* %arrayidx2, align 4, !tbaa !3
  call void (i32, ...) @__hpvm__return(i32 2, i32* %output, i64 %out_bytes) #6
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local void @mul_3(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 1, i32* %input, i32 1, i32* %output) #6
  %call = call i8* (...) @__hpvm__getNode() #6
  %call1 = call i64 @__hpvm__getNodeInstanceID_x(i8* %call) #6
  %arrayidx = getelementptr inbounds i32, i32* %input, i64 %call1
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !3
  %mul = mul nsw i32 %0, 3
  %arrayidx2 = getelementptr inbounds i32, i32* %output, i64 %call1
  store i32 %mul, i32* %arrayidx2, align 4, !tbaa !3
  call void (i32, ...) @__hpvm__return(i32 2, i32* %output, i64 %out_bytes) #6
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local void @sum(i32* %input_5, i64 %in_bytes5, i32* %input_3, i64 %in_bytes3, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input_5, i32* %input_3, i32 1, i32* %output) #6
  %call = call i8* (...) @__hpvm__getNode() #6
  %call1 = call i64 @__hpvm__getNodeInstanceID_x(i8* %call) #6
  %arrayidx = getelementptr inbounds i32, i32* %input_5, i64 %call1
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !3
  %arrayidx2 = getelementptr inbounds i32, i32* %input_3, i64 %call1
  %1 = load i32, i32* %arrayidx2, align 4, !tbaa !3
  %add = add nsw i32 %1, %0
  %arrayidx3 = getelementptr inbounds i32, i32* %output, i64 %call1
  store i32 %add, i32* %arrayidx3, align 4, !tbaa !3
  call void (i32, ...) @__hpvm__return(i32 2, i32* %output, i64 %out_bytes) #6
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local void @root_wrapper(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes, i64 %num_elems, i32* %buffer_5, i64 %bytes_5, i32* %buffer_3, i64 %bytes_3) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 3, i32* %input, i32* %buffer_5, i32* %buffer_3, i32 4, i32* %input, i32* %output, i32* %buffer_5, i32* %buffer_3) #6
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* nonnull @init_input, i64 %num_elems) #6
  %call1 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64)* nonnull @mul_5, i64 %num_elems) #6
  %call2 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64)* nonnull @mul_3, i64 %num_elems) #6
  %call3 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64, i32*, i64)* nonnull @sum, i64 %num_elems) #6
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #6
  call void @__hpvm__bindIn(i8* %call1, i32 5, i32 2, i32 0) #6
  call void @__hpvm__bindIn(i8* %call1, i32 6, i32 3, i32 0) #6
  call void @__hpvm__bindIn(i8* %call2, i32 7, i32 2, i32 0) #6
  call void @__hpvm__bindIn(i8* %call2, i32 8, i32 3, i32 0) #6
  call void @__hpvm__bindIn(i8* %call3, i32 2, i32 4, i32 0) #6
  call void @__hpvm__bindIn(i8* %call3, i32 3, i32 5, i32 0) #6
  %call4 = call i8* @__hpvm__edge(i8* %call, i8* %call1, i32 1, i32 0, i32 0, i32 0) #6
  %call5 = call i8* @__hpvm__edge(i8* %call, i8* %call1, i32 1, i32 1, i32 1, i32 0) #6
  %call6 = call i8* @__hpvm__edge(i8* %call, i8* %call2, i32 1, i32 0, i32 0, i32 0) #6
  %call7 = call i8* @__hpvm__edge(i8* %call, i8* %call2, i32 1, i32 1, i32 1, i32 0) #6
  %call8 = call i8* @__hpvm__edge(i8* %call1, i8* %call3, i32 1, i32 0, i32 0, i32 0) #6
  %call9 = call i8* @__hpvm__edge(i8* %call1, i8* %call3, i32 1, i32 1, i32 1, i32 0) #6
  %call10 = call i8* @__hpvm__edge(i8* %call2, i8* %call3, i32 1, i32 0, i32 2, i32 0) #6
  %call11 = call i8* @__hpvm__edge(i8* %call2, i8* %call3, i32 1, i32 1, i32 3, i32 0) #6
  call void @__hpvm__bindOut(i8* %call3, i32 0, i32 0, i32 0) #6
  ret void
}

declare dso_local i8* @__hpvm__createNodeND(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__bindIn(i8*, i32, i32, i32) local_unnamed_addr #1

declare dso_local i8* @__hpvm__edge(i8*, i8*, i32, i32, i32, i32) local_unnamed_addr #1

declare dso_local void @__hpvm__bindOut(i8*, i32, i32, i32) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @root(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes, i64 %num_elems, i32* %buffer_5, i64 %bytes_5, i32* %buffer_3, i64 %bytes_3) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 3, i32* %input, i32* %buffer_5, i32* %buffer_3, i32 4, i32* %input, i32* %output, i32* %buffer_5, i32* %buffer_3) #6
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 0, void (i32*, i64, i32*, i64, i64, i32*, i64, i32*, i64)* nonnull @root_wrapper) #6
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 4, i32 4, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 5, i32 5, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 6, i32 6, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 7, i32 7, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 8, i32 8, i32 0) #6
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readnone %argv) local_unnamed_addr #0 {
entry:
  %input = alloca [1001 x i32], align 16
  %output = alloca [1001 x i32], align 16
  %buffer_3 = alloca [1001 x i32], align 16
  %buffer_5 = alloca [1001 x i32], align 16
  %arg = alloca %struct.RootArg, align 8
  %0 = bitcast [1001 x i32]* %input to i8*
  call void @llvm.lifetime.start.p0i8(i64 4004, i8* nonnull %0) #6
  %1 = bitcast [1001 x i32]* %output to i8*
  call void @llvm.lifetime.start.p0i8(i64 4004, i8* nonnull %1) #6
  %2 = bitcast [1001 x i32]* %buffer_3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4004, i8* nonnull %2) #6
  %3 = bitcast [1001 x i32]* %buffer_5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4004, i8* nonnull %3) #6
  %arraydecay = getelementptr inbounds [1001 x i32], [1001 x i32]* %input, i64 0, i64 0
  call void @llvm.memset.p0i8.i64(i8* noundef nonnull align 16 dereferenceable(4004) %0, i8 0, i64 4004, i1 false)
  %arraydecay1 = getelementptr inbounds [1001 x i32], [1001 x i32]* %output, i64 0, i64 0
  call void @llvm.memset.p0i8.i64(i8* noundef nonnull align 16 dereferenceable(4004) %1, i8 0, i64 4004, i1 false)
  %puts = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([17 x i8], [17 x i8]* @str, i64 0, i64 0))
  call void (...) @__hpvm__init() #6
  call void @llvm_hpvm_track_mem(i8* nonnull %0, i64 4004) #6
  call void @llvm_hpvm_track_mem(i8* nonnull %1, i64 4004) #6
  %arraydecay4 = getelementptr inbounds [1001 x i32], [1001 x i32]* %buffer_3, i64 0, i64 0
  call void @llvm_hpvm_track_mem(i8* nonnull %2, i64 4004) #6
  %arraydecay5 = getelementptr inbounds [1001 x i32], [1001 x i32]* %buffer_5, i64 0, i64 0
  call void @llvm_hpvm_track_mem(i8* nonnull %3, i64 4004) #6
  %4 = bitcast %struct.RootArg* %arg to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* nonnull %4) #6
  %input6 = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 0
  store i32* %arraydecay, i32** %input6, align 8, !tbaa !7
  %in_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 1
  store i64 4004, i64* %in_bytes, align 8, !tbaa !11
  %output7 = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 2
  store i32* %arraydecay1, i32** %output7, align 8, !tbaa !12
  %out_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 3
  store i64 4004, i64* %out_bytes, align 8, !tbaa !13
  %num_elems = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 4
  store i64 1001, i64* %num_elems, align 8, !tbaa !14
  %buffer_59 = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 5
  store i32* %arraydecay5, i32** %buffer_59, align 8, !tbaa !15
  %bytes_5 = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 6
  store i64 4004, i64* %bytes_5, align 8, !tbaa !16
  %buffer_311 = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 7
  store i32* %arraydecay4, i32** %buffer_311, align 8, !tbaa !17
  %bytes_3 = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 8
  store i64 4004, i64* %bytes_3, align 8, !tbaa !18
  %call13 = call i8* (i32, ...) @__hpvm__launch(i32 0, void (i32*, i64, i32*, i64, i64, i32*, i64, i32*, i64)* nonnull @root, %struct.RootArg* nonnull %arg) #6
  call void @__hpvm__wait(i8* %call13) #6
  call void (...) @__hpvm__cleanup() #6
  %puts47 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([20 x i8], [20 x i8]* @str.4, i64 0, i64 0))
  br label %for.body

for.cond.cleanup:                                 ; preds = %for.inc
  call void @llvm.lifetime.end.p0i8(i64 72, i8* nonnull %4) #6
  call void @llvm.lifetime.end.p0i8(i64 4004, i8* nonnull %3) #6
  call void @llvm.lifetime.end.p0i8(i64 4004, i8* nonnull %2) #6
  call void @llvm.lifetime.end.p0i8(i64 4004, i8* nonnull %1) #6
  call void @llvm.lifetime.end.p0i8(i64 4004, i8* nonnull %0) #6
  ret i32 0

for.body:                                         ; preds = %entry, %for.inc
  %i.049 = phi i64 [ 0, %entry ], [ %inc, %for.inc ]
  %arrayidx15 = getelementptr inbounds [1001 x i32], [1001 x i32]* %output, i64 0, i64 %i.049
  %5 = load i32, i32* %arrayidx15, align 4, !tbaa !3
  %arrayidx16 = getelementptr inbounds [1001 x i32], [1001 x i32]* %input, i64 0, i64 %i.049
  %6 = load i32, i32* %arrayidx16, align 4, !tbaa !3
  %add = shl nsw i32 %6, 3
  %cmp19 = icmp eq i32 %5, %add
  br i1 %cmp19, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %puts48 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([2 x i8], [2 x i8]* @str.5, i64 0, i64 0))
  br label %for.inc

if.else:                                          ; preds = %for.body
  %call27 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([10 x i8], [10 x i8]* @.str.3, i64 0, i64 0), i32 %5, i32 %add)
  br label %for.inc

for.inc:                                          ; preds = %if.then, %if.else
  %inc = add nuw nsw i64 %i.049, 1
  %exitcond.not = icmp eq i64 %inc, 1001
  br i1 %exitcond.not, label %for.cond.cleanup, label %for.body, !llvm.loop !19
}

; Function Attrs: argmemonly mustprogress nofree nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #3

; Function Attrs: nofree nounwind
declare dso_local noundef i32 @printf(i8* nocapture noundef readonly, ...) local_unnamed_addr #4

declare dso_local void @__hpvm__init(...) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_track_mem(i8*, i64) local_unnamed_addr #1

declare dso_local i8* @__hpvm__launch(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__wait(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm__cleanup(...) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare noundef i32 @puts(i8* nocapture noundef readonly) local_unnamed_addr #5

attributes #0 = { nounwind uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { argmemonly mustprogress nofree nosync nounwind willreturn }
attributes #3 = { argmemonly mustprogress nofree nounwind willreturn writeonly }
attributes #4 = { nofree nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { nofree nounwind }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (git@gitlab.engr.illinois.edu:llvm/hpvm.git 26f6fae1a471f56752ff541c65da637e652d19ae)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !9, i64 0}
!8 = !{!"RootArg", !9, i64 0, !10, i64 8, !9, i64 16, !10, i64 24, !10, i64 32, !9, i64 40, !10, i64 48, !9, i64 56, !10, i64 64}
!9 = !{!"any pointer", !5, i64 0}
!10 = !{!"long", !5, i64 0}
!11 = !{!8, !10, i64 8}
!12 = !{!8, !9, i64 16}
!13 = !{!8, !10, i64 24}
!14 = !{!8, !10, i64 32}
!15 = !{!8, !9, i64 40}
!16 = !{!8, !10, i64 48}
!17 = !{!8, !9, i64 56}
!18 = !{!8, !10, i64 64}
!19 = distinct !{!19, !20, !21}
!20 = !{!"llvm.loop.mustprogress"}
!21 = !{!"llvm.loop.unroll.disable"}
