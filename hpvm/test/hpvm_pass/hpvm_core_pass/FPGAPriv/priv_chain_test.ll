; RUN: opt -enable-new-pm=0 -load HPVMGenHPVM.so -genhpvm -load HPVMBuildDFG.so -buildDFG -load HPVMArgPriv.so -argpriv -S < %s | FileCheck %s 
; ModuleID = 'priv_chain_test.c'
source_filename = "priv_chain_test.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.RootArg = type { i32*, i64, i32*, i64, i32* }

; CHECK-LABEL: %struct.out.leaf_node_1 @leaf_node_1_c
; CHECK: %hpvm_fpga_local_output = alloca
; CHECK: %hpvm_fpga_local_input = alloca
; CHECK: for.body:
; CHECK-NOT: %input
; CHECK: getelementptr {{.*}} %hpvm_fpga_local_input
; CHECK-NOT: %input
; CHECK: for.body7:
; CHECK-NOT: %output
; CHECK: getelementptr {{.*}} %hpvm_fpga_local_input
; CHECK-NOT: %output
; CHECK-LABEL: ret %struct.out.leaf_node_1

@str = private unnamed_addr constant [14 x i8] c"Starting HPVM\00", align 1
@str.3 = private unnamed_addr constant [16 x i8] c"Starting Launch\00", align 1
@str.4 = private unnamed_addr constant [14 x i8] c"HPVM finished\00", align 1

; Function Attrs: nounwind uwtable
define dso_local void @leaf_node_1(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes, i32* nocapture %N) #0 {
entry:
  call void @__hpvm__hint(i32 7) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output, i32 2, i32* %input, i32 8, i32* %output, i32 8) #4
  br label %for.body

for.body:                                         ; preds = %entry, %for.body
  %indvars.iv = phi i64 [ 0, %entry ], [ %indvars.iv.next, %for.body ]
  %arrayidx = getelementptr inbounds i32, i32* %N, i64 %indvars.iv
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !3
  %arrayidx2 = getelementptr inbounds i32, i32* %input, i64 %indvars.iv
  store i32 %0, i32* %arrayidx2, align 4, !tbaa !3
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond.not = icmp eq i64 %indvars.iv.next, 20
  br i1 %exitcond.not, label %for.body7, label %for.body, !llvm.loop !7

for.body7:                                        ; preds = %for.body, %for.body7
  %indvars.iv55 = phi i64 [ %indvars.iv.next56, %for.body7 ], [ 0, %for.body ]
  %arrayidx9 = getelementptr inbounds i32, i32* %input, i64 %indvars.iv55
  %1 = load i32, i32* %arrayidx9, align 4, !tbaa !3
  %arrayidx11 = getelementptr inbounds i32, i32* %output, i64 %indvars.iv55
  store i32 %1, i32* %arrayidx11, align 4, !tbaa !3
  %indvars.iv.next56 = add nuw nsw i64 %indvars.iv55, 1
  %exitcond57.not = icmp eq i64 %indvars.iv.next56, 20
  br i1 %exitcond57.not, label %for.body19, label %for.body7, !llvm.loop !10

for.cond.cleanup18:                               ; preds = %for.body19
  call void (i32, ...) @__hpvm__return(i32 1, i32* %output) #4
  ret void

for.body19:                                       ; preds = %for.body7, %for.body19
  %indvars.iv60 = phi i64 [ %indvars.iv.next61, %for.body19 ], [ 0, %for.body7 ]
  %arrayidx21 = getelementptr inbounds i32, i32* %output, i64 %indvars.iv60
  %2 = load i32, i32* %arrayidx21, align 4, !tbaa !3
  %arrayidx23 = getelementptr inbounds i32, i32* %N, i64 %indvars.iv60
  store i32 %2, i32* %arrayidx23, align 4, !tbaa !3
  %indvars.iv.next61 = add nuw nsw i64 %indvars.iv60, 1
  %exitcond62.not = icmp eq i64 %indvars.iv.next61, 20
  br i1 %exitcond62.not, label %for.cond.cleanup18, label %for.body19, !llvm.loop !11
}

declare dso_local void @__hpvm__hint(i32) local_unnamed_addr #1

declare dso_local void @__hpvm__attributes(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__return(i32, ...) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @internal_node_1(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes, i32* nocapture readnone %N) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64, i32*)* nonnull @leaf_node_1, i64 8) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 4, i32 4, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

declare dso_local i8* @__hpvm__createNodeND(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__bindIn(i8*, i32, i32, i32) local_unnamed_addr #1

declare dso_local void @__hpvm__bindOut(i8*, i32, i32, i32) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @root(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes, i32* nocapture readnone %N) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 0, void (i32*, i64, i32*, i64, i32*)* nonnull @internal_node_1) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 4, i32 4, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readnone %argv) local_unnamed_addr #0 {
entry:
  %call = call noalias align 16 dereferenceable_or_null(40) i8* @malloc(i64 40) #4
  %0 = bitcast i8* %call to %struct.RootArg*
  %in_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 1
  store i64 32, i64* %in_bytes, align 8, !tbaa !12
  %call2 = call noalias align 16 dereferenceable_or_null(128) i8* @malloc(i64 128) #4
  %1 = bitcast i8* %call to i8**
  store i8* %call2, i8** %1, align 16, !tbaa !16
  %out_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 3
  store i64 32, i64* %out_bytes, align 8, !tbaa !17
  %call5 = call noalias align 16 dereferenceable_or_null(128) i8* @malloc(i64 128) #4
  %output = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 2
  %2 = bitcast i32** %output to i8**
  store i8* %call5, i8** %2, align 16, !tbaa !18
  %call8 = call noalias align 16 dereferenceable_or_null(128) i8* @malloc(i64 128) #4
  %N = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 4
  %3 = bitcast i32** %N to i8**
  store i8* %call8, i8** %3, align 16, !tbaa !19
  %puts = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([14 x i8], [14 x i8]* @str, i64 0, i64 0))
  call void (...) @__hpvm__init() #4
  %4 = load i8*, i8** %1, align 16, !tbaa !16
  %5 = load i64, i64* %in_bytes, align 8, !tbaa !12
  call void @llvm_hpvm_track_mem(i8* %4, i64 %5) #4
  %6 = load i8*, i8** %2, align 16, !tbaa !18
  %7 = load i64, i64* %out_bytes, align 8, !tbaa !17
  call void @llvm_hpvm_track_mem(i8* %6, i64 %7) #4
  %puts39 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([16 x i8], [16 x i8]* @str.3, i64 0, i64 0))
  %call15 = call i8* (i32, ...) @__hpvm__launch(i32 0, void (i32*, i64, i32*, i64, i32*)* nonnull @root, i8* %call) #4
  call void @__hpvm__wait(i8* %call15) #4
  %8 = load i8*, i8** %2, align 16, !tbaa !18
  %9 = load i64, i64* %out_bytes, align 8, !tbaa !17
  call void @llvm_hpvm_request_mem(i8* %8, i64 %9) #4
  %10 = load i8*, i8** %1, align 16, !tbaa !16
  call void @llvm_hpvm_untrack_mem(i8* %10) #4
  %11 = load i8*, i8** %2, align 16, !tbaa !18
  call void @llvm_hpvm_untrack_mem(i8* %11) #4
  call void (...) @__hpvm__cleanup() #4
  %puts40 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([14 x i8], [14 x i8]* @str.4, i64 0, i64 0))
  ret i32 0
}

; Function Attrs: inaccessiblememonly mustprogress nofree nounwind willreturn
declare dso_local noalias noundef align 16 i8* @malloc(i64 noundef) local_unnamed_addr #2

declare dso_local void @__hpvm__init(...) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_track_mem(i8*, i64) local_unnamed_addr #1

declare dso_local i8* @__hpvm__launch(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__wait(i8*) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_request_mem(i8*, i64) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_untrack_mem(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm__cleanup(...) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare noundef i32 @puts(i8* nocapture noundef readonly) local_unnamed_addr #3

attributes #0 = { nounwind uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { inaccessiblememonly mustprogress nofree nounwind willreturn "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nofree nounwind }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git af5a65906558294f157e2bbf69151b14b9d1becd)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.mustprogress"}
!9 = !{!"llvm.loop.unroll.disable"}
!10 = distinct !{!10, !8, !9}
!11 = distinct !{!11, !8, !9}
!12 = !{!13, !15, i64 8}
!13 = !{!"", !14, i64 0, !15, i64 8, !14, i64 16, !15, i64 24, !14, i64 32}
!14 = !{!"any pointer", !5, i64 0}
!15 = !{!"long", !5, i64 0}
!16 = !{!13, !14, i64 0}
!17 = !{!13, !15, i64 24}
!18 = !{!13, !14, i64 16}
!19 = !{!13, !14, i64 32}
