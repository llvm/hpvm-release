; RUN: opt -enable-new-pm=0 -load HPVMGenHPVM.so -genhpvm -load HPVMBuildDFG.so -buildDFG -load HPVMDFGTransformPasses.so -sequentializeflatten -S < %s | FileCheck %s
; ModuleID = 'flatten_one_test.c'
source_filename = "flatten_one_test.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.RootArg = type { i32*, i64, i32*, i64 }

; CHECK-LABEL: %struct.RootArg =
; CHECK-LABEL: %struct.out.leaf_node_1 =
; CHECK-NOT: %struct.out.internal_node_2 = 
; CHECK-LABEL: %struct.out.internal_node_1 =
; CHECK-LABEL: %struct.out.leaf_node_1
; CHECK-NOT: %struct.out.internal_node_2
; CHECK_NOT: %leaf_node_1_c.node = call i8*
; CHECK-LABEL: %struct.out.internal_node_1 
; CHECK-NOT: %internal_node_2_c.node = call
; CHECK: %leaf_node_1_c.node = call
; CHECK-NOT: %internal_node_2_c.node = call
; CHECK-LABEL: ret %struct.out.internal_node_1 

; CHECK_LABEL: %struct.out.root =

@str = private unnamed_addr constant [14 x i8] c"Starting HPVM\00", align 1
@str.3 = private unnamed_addr constant [16 x i8] c"Starting Launch\00", align 1
@str.4 = private unnamed_addr constant [14 x i8] c"HPVM finished\00", align 1

; Function Attrs: nounwind uwtable
define dso_local void @leaf_node_1(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 7) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %0 = load i32, i32* %input, align 4, !tbaa !3
  %add = add nsw i32 %0, 5
  store i32 %add, i32* %output, align 4, !tbaa !3
  call void (i32, ...) @__hpvm__return(i32 1, i32* nonnull %output) #4
  ret void
}

declare dso_local void @__hpvm__hint(i32) local_unnamed_addr #1

declare dso_local void @__hpvm__attributes(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__return(i32, ...) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @internal_node_2(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64)* nonnull @leaf_node_1, i64 1) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

declare dso_local i8* @__hpvm__createNodeND(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__bindIn(i8*, i32, i32, i32) local_unnamed_addr #1

declare dso_local void @__hpvm__bindOut(i8*, i32, i32, i32) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local void @internal_node_1(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 0, void (i32*, i64, i32*, i64)* nonnull @internal_node_2) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local void @root(i32* %input, i64 %in_bytes, i32* %output, i64 %out_bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #4
  call void (i32, ...) @__hpvm__attributes(i32 2, i32* %input, i32* %output, i32 1, i32* %output) #4
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 0, void (i32*, i64, i32*, i64)* nonnull @internal_node_1) #4
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 2, i32 2, i32 0) #4
  call void @__hpvm__bindIn(i8* %call, i32 3, i32 3, i32 0) #4
  call void @__hpvm__bindOut(i8* %call, i32 0, i32 0, i32 0) #4
  ret void
}

; Function Attrs: nounwind uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readnone %argv) local_unnamed_addr #0 {
entry:
  %call = call noalias align 16 dereferenceable_or_null(32) i8* @malloc(i64 32) #4
  %0 = bitcast i8* %call to %struct.RootArg*
  %in_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 1
  store i64 32, i64* %in_bytes, align 8, !tbaa !7
  %call2 = call noalias align 16 dereferenceable_or_null(128) i8* @malloc(i64 128) #4
  %1 = bitcast i8* %call to i8**
  store i8* %call2, i8** %1, align 16, !tbaa !11
  %out_bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 3
  store i64 32, i64* %out_bytes, align 8, !tbaa !12
  %call5 = call noalias align 16 dereferenceable_or_null(128) i8* @malloc(i64 128) #4
  %output = getelementptr inbounds %struct.RootArg, %struct.RootArg* %0, i64 0, i32 2
  %2 = bitcast i32** %output to i8**
  store i8* %call5, i8** %2, align 16, !tbaa !13
  %puts = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([14 x i8], [14 x i8]* @str, i64 0, i64 0))
  call void (...) @__hpvm__init() #4
  %3 = load i8*, i8** %1, align 16, !tbaa !11
  %4 = load i64, i64* %in_bytes, align 8, !tbaa !7
  call void @llvm_hpvm_track_mem(i8* %3, i64 %4) #4
  %5 = load i8*, i8** %2, align 16, !tbaa !13
  %6 = load i64, i64* %out_bytes, align 8, !tbaa !12
  call void @llvm_hpvm_track_mem(i8* %5, i64 %6) #4
  %puts34 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([16 x i8], [16 x i8]* @str.3, i64 0, i64 0))
  %call12 = call i8* (i32, ...) @__hpvm__launch(i32 0, void (i32*, i64, i32*, i64)* nonnull @root, i8* %call) #4
  call void @__hpvm__wait(i8* %call12) #4
  %7 = load i8*, i8** %2, align 16, !tbaa !13
  %8 = load i64, i64* %out_bytes, align 8, !tbaa !12
  call void @llvm_hpvm_request_mem(i8* %7, i64 %8) #4
  %9 = load i8*, i8** %1, align 16, !tbaa !11
  call void @llvm_hpvm_untrack_mem(i8* %9) #4
  %10 = load i8*, i8** %2, align 16, !tbaa !13
  call void @llvm_hpvm_untrack_mem(i8* %10) #4
  call void (...) @__hpvm__cleanup() #4
  %puts35 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([14 x i8], [14 x i8]* @str.4, i64 0, i64 0))
  ret i32 0
}

; Function Attrs: inaccessiblememonly mustprogress nofree nounwind willreturn
declare dso_local noalias noundef align 16 i8* @malloc(i64 noundef) local_unnamed_addr #2

declare dso_local void @__hpvm__init(...) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_track_mem(i8*, i64) local_unnamed_addr #1

declare dso_local i8* @__hpvm__launch(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__wait(i8*) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_request_mem(i8*, i64) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_untrack_mem(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm__cleanup(...) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare noundef i32 @puts(i8* nocapture noundef readonly) local_unnamed_addr #3

attributes #0 = { nounwind uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { inaccessiblememonly mustprogress nofree nounwind willreturn "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nofree nounwind }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git 222e91a5841d193b55a8c6246a50aa662b1a5fd6)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !10, i64 8}
!8 = !{!"", !9, i64 0, !10, i64 8, !9, i64 16, !10, i64 24}
!9 = !{!"any pointer", !5, i64 0}
!10 = !{!"long", !5, i64 0}
!11 = !{!8, !9, i64 0}
!12 = !{!8, !10, i64 24}
!13 = !{!8, !9, i64 16}
