; RUN: opt -enable-new-pm=0 -load HPVMIRCodeGen.so -genhpvm -load HPVMTargets.so -buildDFG -load HPVMDFGTransformPasses.so -stripminetest -dfg2llvm-cpu -clearDFG -S < %s | FileCheck %s
; ModuleID = 'Tile.c'
source_filename = "Tile.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.RootArg = type { i32*, i64, i64, i64 }

; Single-node tiling test with minimal connections

; CHECK-LABEL: %struct.RootArg =

; Leaf node signature
; We expect two extra parameters to give the iteration bounds (for edge case
; testing)
; CHECK-LABEL: define dso_local %emptyStruct @set_idx_c
; CHECK-SAME: i32*
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64

; Bounds check
; CHECK: call i64 @llvm_hpvm_cpu_getDimInstance(i32 1, i32 0)
; CHECK: mul i64
; CHECK: add i64
; CHECK: call i64 @llvm_hpvm_cpu_getDimInstance(i32 1, i32 1)
; CHECK: mul i64
; CHECK: add i64
; CHECK: icmp ult i64
; CHECK-SAME: %count.leaf.0
; CHECK: icmp ult i64
; CHECK-SAME: %count.leaf.1
; CHECK: and i1
; CHECK: br i1

; Index calculation
; CHECK: mul nsw i64
; CHECK-SAME: %count.leaf.1
; CHECK: add nsw i64

; Original behavior
; CHECK: getelementptr inbounds i32, i32* %output
; CHECK: store i32
; CHECK: call i32 (i8*, ...) @printf

; Nesting node
; We expect extra parameters here too to thread in the iteration count
; CHECK-LABEL: define internal %emptyStruct @set_idx_c_nest
; CHECK-SAME: i32*
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64
; CHECK-SAME: i64

; Leaf call
; CHECK: call %emptyStruct @set_idx

; Root node
; CHECK-LABEL: define %emptyStruct.0 @root

; Tile count computation
; CHECK: urem
; CHECK: icmp
; CHECK: udiv
; CHECK: add
; CHECK: urem
; CHECK: icmp
; CHECK: udiv
; CHECK: add
; CHECK: br label

; Nesting node call
; CHECK: call %emptyStruct @set_idx_c_nest

@.str = private unnamed_addr constant [34 x i8] c"i: %lu; j: %lu; n: %lu; idx: %lu\0A\00", align 1
@.str.4 = private unnamed_addr constant [11 x i8] c" %lu/=%lu \00", align 1
@str = private unnamed_addr constant [17 x i8] c"Doing hpvm stuff\00", align 1
@str.6 = private unnamed_addr constant [20 x i8] c"Finished hpvm stuff\00", align 1

; Function Attrs: nounwind uwtable
define dso_local void @set_idx(i32* %output, i64 %bytes) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 0, i32 1, i32* %output) #6
  %call = call i8* (...) @__hpvm__getNode() #6
  %call1 = call i8* @__hpvm__getParentNode(i8* %call) #6
  %call2 = call i64 @__hpvm__getNodeInstanceID_x(i8* %call) #6
  %call3 = call i64 @__hpvm__getNodeInstanceID_y(i8* %call) #6
  %call4 = call i64 @__hpvm__getNumNodeInstances_y(i8* %call) #6
  %mul = mul nsw i64 %call4, %call2
  %add = add nsw i64 %mul, %call3
  %conv = trunc i64 %add to i32
  %arrayidx = getelementptr inbounds i32, i32* %output, i64 %add
  store i32 %conv, i32* %arrayidx, align 4, !tbaa !3
  %call5 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([34 x i8], [34 x i8]* @.str, i64 0, i64 0), i64 %call2, i64 %call3, i64 %call4, i64 %add)
  ret void
}

declare dso_local void @__hpvm__hint(i32) local_unnamed_addr #1

declare dso_local void @__hpvm__attributes(i32, ...) local_unnamed_addr #1

; Function Attrs: argmemonly mustprogress nofree nosync nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare dso_local i8* @__hpvm__getNode(...) local_unnamed_addr #1

declare dso_local i8* @__hpvm__getParentNode(i8*) local_unnamed_addr #1

declare dso_local i64 @__hpvm__getNodeInstanceID_x(i8*) local_unnamed_addr #1

declare dso_local i64 @__hpvm__getNodeInstanceID_y(i8*) local_unnamed_addr #1

declare dso_local i64 @__hpvm__getNumNodeInstances_y(i8*) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare dso_local noundef i32 @printf(i8* nocapture noundef readonly, ...) local_unnamed_addr #3

; Function Attrs: argmemonly mustprogress nofree nosync nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind uwtable
define dso_local void @root(i32* %output, i64 %bytes, i64 %x, i64 %y) #0 {
entry:
  call void @__hpvm__hint(i32 1) #6
  call void (i32, ...) @__hpvm__attributes(i32 0, i32 1, i32* %output) #6
  %call = call i8* (i32, ...) @__hpvm__createNodeND(i32 2, void (i32*, i64)* nonnull @set_idx, i64 %x, i64 %y) #6
  call void @__hpvm__bindIn(i8* %call, i32 0, i32 0, i32 0) #6
  call void @__hpvm__bindIn(i8* %call, i32 1, i32 1, i32 0) #6
  ret void
}

declare dso_local i8* @__hpvm__createNodeND(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__bindIn(i8*, i32, i32, i32) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readnone %argv) local_unnamed_addr #0 {
entry:
  %data = alloca [199 x [99 x i32]], align 16
  %arg = alloca %struct.RootArg, align 8
  %0 = bitcast [199 x [99 x i32]]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 78804, i8* nonnull %0) #6
  call void @llvm.memset.p0i8.i64(i8* noundef nonnull align 16 dereferenceable(78804) %0, i8 0, i64 78804, i1 false)
  %puts = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([17 x i8], [17 x i8]* @str, i64 0, i64 0))
  call void (...) @__hpvm__init() #6
  call void @llvm_hpvm_track_mem(i8* nonnull %0, i64 78804) #6
  %1 = bitcast %struct.RootArg* %arg to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %1) #6
  %output = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 0
  %arrayidx2 = getelementptr inbounds [199 x [99 x i32]], [199 x [99 x i32]]* %data, i64 0, i64 0, i64 0
  store i32* %arrayidx2, i32** %output, align 8, !tbaa !7
  %bytes = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 1
  store i64 78804, i64* %bytes, align 8, !tbaa !12
  %x = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 2
  store i64 199, i64* %x, align 8, !tbaa !13
  %y = getelementptr inbounds %struct.RootArg, %struct.RootArg* %arg, i64 0, i32 3
  store i64 99, i64* %y, align 8, !tbaa !14
  %call3 = call i8* (i32, ...) @__hpvm__launch(i32 0, void (i32*, i64, i64, i64)* nonnull @root, %struct.RootArg* nonnull %arg) #6
  call void @__hpvm__wait(i8* %call3) #6
  call void (...) @__hpvm__cleanup() #6
  %puts34 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([20 x i8], [20 x i8]* @str.6, i64 0, i64 0))
  br label %for.cond5.preheader

for.cond5.preheader:                              ; preds = %entry, %for.cond.cleanup7
  %i.037 = phi i64 [ 0, %entry ], [ %inc19, %for.cond.cleanup7 ]
  %mul = mul nuw nsw i64 %i.037, 99
  br label %for.body8

for.cond.cleanup:                                 ; preds = %for.cond.cleanup7
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %1) #6
  call void @llvm.lifetime.end.p0i8(i64 78804, i8* nonnull %0) #6
  ret i32 0

for.cond.cleanup7:                                ; preds = %if.end
  %putchar = call i32 @putchar(i32 10)
  %inc19 = add nuw nsw i64 %i.037, 1
  %exitcond38.not = icmp eq i64 %inc19, 199
  br i1 %exitcond38.not, label %for.cond.cleanup, label %for.cond5.preheader, !llvm.loop !15

for.body8:                                        ; preds = %for.cond5.preheader, %if.end
  %j.036 = phi i64 [ 0, %for.cond5.preheader ], [ %inc, %if.end ]
  %add = add nuw nsw i64 %j.036, %mul
  %arrayidx10 = getelementptr inbounds [199 x [99 x i32]], [199 x [99 x i32]]* %data, i64 0, i64 %i.037, i64 %j.036
  %2 = load i32, i32* %arrayidx10, align 4, !tbaa !3
  %conv = sext i32 %2 to i64
  %cmp11 = icmp eq i64 %add, %conv
  br i1 %cmp11, label %if.then, label %if.else

if.then:                                          ; preds = %for.body8
  %putchar35 = call i32 @putchar(i32 42)
  br label %if.end

if.else:                                          ; preds = %for.body8
  %call16 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([11 x i8], [11 x i8]* @.str.4, i64 0, i64 0), i32 %2, i64 %add)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %inc = add nuw nsw i64 %j.036, 1
  %exitcond.not = icmp eq i64 %inc, 99
  br i1 %exitcond.not, label %for.cond.cleanup7, label %for.body8, !llvm.loop !18
}

; Function Attrs: argmemonly mustprogress nofree nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #4

declare dso_local void @__hpvm__init(...) local_unnamed_addr #1

declare dso_local void @llvm_hpvm_track_mem(i8*, i64) local_unnamed_addr #1

declare dso_local i8* @__hpvm__launch(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm__wait(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm__cleanup(...) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare noundef i32 @puts(i8* nocapture noundef readonly) local_unnamed_addr #5

; Function Attrs: nofree nounwind
declare noundef i32 @putchar(i32 noundef) local_unnamed_addr #5

attributes #0 = { nounwind uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { argmemonly mustprogress nofree nosync nounwind willreturn }
attributes #3 = { nofree nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #4 = { argmemonly mustprogress nofree nounwind willreturn writeonly }
attributes #5 = { nofree nounwind }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (git@gitlab.engr.illinois.edu:llvm/hpvm.git 056f0c24883102b36bb6bd2318ebd21d6d592e49)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !9, i64 0}
!8 = !{!"RootArg", !9, i64 0, !10, i64 8, !11, i64 16, !11, i64 24}
!9 = !{!"any pointer", !5, i64 0}
!10 = !{!"long", !5, i64 0}
!11 = !{!"long long", !5, i64 0}
!12 = !{!8, !10, i64 8}
!13 = !{!8, !11, i64 16}
!14 = !{!8, !11, i64 24}
!15 = distinct !{!15, !16, !17}
!16 = !{!"llvm.loop.mustprogress"}
!17 = !{!"llvm.loop.unroll.disable"}
!18 = distinct !{!18, !16, !17}
