import argparse
import json
import os
import sys
import warnings
import numpy as np
from collections import OrderedDict
from os import listdir
from os.path import join, splitext
import sys
import matplotlib as mpl
import matplotlib.pyplot as pp

import pandas as pd
from jsonschema import exceptions, Draft4Validator
from pkg_resources import resource_stream

from hypermapper import space
from hypermapper.utility_functions import extend_with_default, write_data_array

def load_data(file):
    data = pd.read_csv(file)

    data_array = {}
    for key in data:
        data_array[key] = data[key].tolist()
    return data_array

# Performs 1D clustering and picks the most representative point from each cluster
def k_means(xs, num_clusters):
    mx = max(xs)
    mn = min(xs)
    centroids = [xs[0]] + [xs[round(i/num_clusters * len(xs))] for i in range(1, num_clusters - 1)] + [xs[-1]]
    mapping = [0] * len(xs)
    changed = True
    while changed:
        changed = False
        # Update mappings
        for j, x in enumerate(xs):
            for i, c in enumerate(centroids):
                if abs(x - c) < abs(x - centroids[mapping[j]]):
                    mapping[j] = i
                    changed = True

        # Update centroids
        sums = [0] * num_clusters
        counts = [0] * num_clusters
        print("mapping: ", mapping)
        for x, c_i in zip(xs, mapping):
            counts[c_i] += 1
            sums[c_i] += x
        centroids = [s/c for s, c in zip(sums, counts)]
        print("centroids: ", centroids)
    reprs = [0]*num_clusters
    for i, (x, c_i) in enumerate(zip(xs, mapping)):
        c = centroids[c_i]
        if abs(x - c) < abs(xs[reprs[c_i]] - c):
            reprs[c_i] = i
    return reprs

def main(configuration_file, input_file, output_folder):

    # Read json configuration file
    if not configuration_file.endswith(".json"):
        _, file_extension = splitext(configuration_file)
        print(
            "Error: invalid file name. \nThe input file has to be a .json file not a %s"
            % file_extension
        )
        raise SystemExit
    with open(configuration_file, "r") as f:
        config = json.load(f)

    schema = json.load(resource_stream("hypermapper", "schema.json"))

    DefaultValidatingDraft4Validator = extend_with_default(Draft4Validator)
    try:
        DefaultValidatingDraft4Validator(schema).validate(config)
    except exceptions.ValidationError as ve:
        print("Failed to validate json:")
        print(ve)
        raise SystemExit

    param_space = space.Space(config)
    output_metric = param_space.get_optimization_parameters()[
        0
    ]  # only works for mono-objective
    feasibility_flag = param_space.get_feasible_parameter()[
        0
    ]  # returns a list, we just want the name

    data_array = load_data(input_file)
    incumbent_data_array = {}
    for key in data_array:
        incumbent_data_array[key] = []
    incumbent = float("inf")
    incumbent_idx = 0
    plot_points = []
    for idx in range(len(data_array[output_metric])):
        if feasibility_flag is not None:
            if data_array[feasibility_flag][idx] and incumbent > data_array[output_metric][idx]:
                incumbent = data_array[output_metric][idx]
                incumbent_idx = idx
                for key in data_array:
                    incumbent_data_array[key].append(data_array[key][incumbent_idx])
        else:
            if incumbent > data_array[output_metric][idx]:
                incumbent = data_array[output_metric][idx]
                incumbent_idx = idx
                for key in data_array:
                    incumbent_data_array[key].append(data_array[key][incumbent_idx])
        plot_points.append(incumbent)
    
    if len(incumbent_data_array[output_metric]) > 5:
        reprs = k_means(incumbent_data_array[output_metric][1:-1], 3)
        reprs = [i + 1 for i in reprs] # Because it's from a slice
        print(reprs)
        clustered_data_array = {}
        for key in data_array:
            clustered_data_array[key] = []
        for i in [0] + reprs + [-1]:
            print(i)
            for key in incumbent_data_array:
                clustered_data_array[key].append(incumbent_data_array[key][i])
    else:
        clustered_data_array = incumbent_data_array
    output_file = output_folder + "/regret_points.csv"
    if os.path.exists(output_file):
        os.remove(output_file)
    print(incumbent_data_array[output_metric])
    print(clustered_data_array[output_metric])
    write_data_array(param_space, clustered_data_array, output_file)

    mpl.rcParams.update({"font.size": 40})
    pp.rcParams['figure.figsize'] = [16,12]
    pp.plot(plot_points, linewidth=6)
    pp.xlabel('Number of Evaluations')
    pp.ylabel('Execution Time (s)')
    pp.savefig(output_folder+'/regret_graph.pdf')

    mpl.rcParams.update({"font.size": 40})
    pp.rcParams['figure.figsize'] = [16,12]
    pp.plot(plot_points, linewidth=6)
    pp.xlabel('Number of Evaluations')
    pp.ylabel('Execution Time (s)')
    pp.semilogy()
    pp.yticks(ticks=[100,1000,10000], labels=['100', '1e3', '1e4'])
    pp.savefig(output_folder+'/regret_graph_log.pdf')

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3])
