llvm-ocl
========

An LLVM-to-OpenCL back end tool that takes in an LLVM bit code file 
which has metadata and Intrinsics to mark OpenCL properties, and generates
an OpenCL file out of it. This tool is based off of Julia Computing's resurrected
 LLVM "C" backend [here](https://github.com/JuliaComputingOSS/llvm-cbe).
