## HPVM Projects

This directory contains the subdirectories for `HPVM` sub-projects which are built on top the the `HPVM` compiler infrastructure. They consist of:

- `hetero-c++`: A high level parallel dialect of C/C++ which describes hierarchical Task-level and Data-level parallelism. It compiles through `HPVM` to target `CPU`, `GPU` and the `FPGA`.
- `hpvm-profiler`: Defines the python package for profiling `HPVM` approximation configurations for the `HPVM` Tensor Extensions.
- `hpvm-rt`: Defines the `HPVM` runtime which manages the execution of `HPVM` programs on heterogeneous compute devices.
- `keras`: Defines the `keras` frontend for `HPVM` Tensor Extensions which compiles neural network models through `HPVM`.
- `llvm-ocl`: Tool to generate the `OpenCL` kernel file from `LLVM` bitcode. This is used by the `GPU` and `FPGA` benchmarks.
- `torch2hpvm`: Defines the `onnx` frontend for `HPVM` Tensor Extensions which compiles `PyTorch` nearal network models through `HPVM`.
