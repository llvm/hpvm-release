#include "heterocc.h"

void root(int* A, size_t sizeA, int* B, size_t sizeB) {
  auto s = __hetero_section_begin();

  for (int i = 0; i < 10; i++) {
    __hetero_parallel_loop(1, 1, A, sizeA, 1, A, sizeA);
    A[i] = i * i;
  }

  for (int i = 0; i < 15; i++) {
    __hetero_parallel_loop(1, 2, A, sizeA, B, sizeB, 1, B, sizeB);
    B[i] = i + A[i];
  }

  __hetero_section_end(s);
}

int main() {
  int* A = new int[15];
  for (int i = 0; i < 15; i++) A[i] = i;

  int* B = new int[15];

  size_t sizeA = sizeof(int) * 15;
  size_t sizeB = sizeof(int) * 15;

  auto l = __hetero_launch((void*)root, 2, A, sizeA, B, sizeB, 2, A, sizeA, B, sizeB);
  __hetero_wait(l);

  delete [] A;
  delete [] B;

  return 0;
}
