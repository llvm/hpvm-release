#include "heterocc.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define ROW 20
#define COL 30


#ifndef DEVICE
#define DEVICE hetero::CPU_TARGET
#endif


void pipeline(int* P1, std::size_t P1_Size, int* P2, std::size_t P2_Size,
              int* P3, std::size_t P3_Size, int* Res, std::size_t Res_Size,  
              int* min, std::size_t min_size, int* max, std::size_t max_size,
              double* StdDev, std::size_t StdDevSize, 
              double* Mean, std::size_t MeanSize, std::size_t num_pixels){

    void* Section =  __hetero_section_begin();


    void* MinTask =__hetero_task_begin(3, P1, P1_Size, min, min_size, num_pixels, 1, min, min_size, "MinNode");
    {
        int _min = P1[0];

        for(int i = 0; i < num_pixels; i++){
            if(P1[i] < _min){
                _min = P1[i];
            }
        }

        *min = _min;

    }

    __hetero_task_end(MinTask);


    void* MaxTask =__hetero_task_begin(3, P1, P1_Size, max, max_size, num_pixels, 1, max, max_size, "MaxNode");
    {
        int _max = P1[0];

        for(int i = 0; i < num_pixels; i++){
            if(P1[i] > _max){
                _max = P1[i];
            }
        }

        *max = _max;

    }

    __hetero_task_end(MaxTask);

    void* ScaleWrapper = __hetero_task_begin(4, P1, P1_Size, min, min_size , max, max_size, num_pixels, 1, P1, P1_Size , "ScaleWrapperNode");
    void* ScaleSection = __hetero_section_begin();
    for(int i = 0; i < num_pixels ; i++){
        __hetero_parallel_loop(1, 4, P1, P1_Size, min, min_size , max, max_size, num_pixels, 1, P1, P1_Size , "ScaleNode");

        __hetero_hint(DEVICE);
        P1[i] = (P1[i] - (*min)) / (*max - *min);

    }
    __hetero_section_end(ScaleSection);
    __hetero_task_end(ScaleWrapper);



    void* ReluWrapper = __hetero_task_begin( 2, P2, P2_Size, num_pixels, 1, P2, P2_Size , "ReluWrapperNode");
    void* ReluSection = __hetero_section_begin();
    for(int i = 0; i < num_pixels; i++){
        __hetero_parallel_loop(1, 2, P2, P2_Size, num_pixels, 1, P2, P2_Size , "ReluNode");

        __hetero_hint(DEVICE);
        if(P2[i] < 0) P2[i] = 0;
    }
    __hetero_section_end(ReluSection);
    __hetero_task_end(ReluWrapper);



    void* MeanAndDevTask =__hetero_task_begin(4, P3, P3_Size, Mean, MeanSize, StdDev, StdDevSize,
            num_pixels ,2 , Mean, MeanSize, StdDev, StdDevSize, "MeanAndDevNode");
    {

        int sum = 0;

        for(int i = 0; i < num_pixels; i++){
            sum += P3[i];
        }

        *Mean = ((double) sum) / num_pixels;

        double variance = 0.0;


        for(int i = 0; i < num_pixels; i++){
            double diff = (P3[i] - *Mean);
            variance +=  diff * diff  ;
        }

        variance = variance / num_pixels;

        *StdDev = sqrt(variance);

    }
    __hetero_task_end(MeanAndDevTask);



    void* NormalizeWrapper = __hetero_task_begin( 4, P3, P3_Size, Mean, MeanSize, StdDev, StdDevSize, num_pixels, 1, P3, P3_Size , "NormalizeWrapperNode");
    void* NormalizeSection = __hetero_section_begin();

    for(int i = 0; i < num_pixels ; i++){
        __hetero_parallel_loop(1, 4, P3, P3_Size, Mean, MeanSize, StdDev, StdDevSize, num_pixels,
                1, 
                P3, P3_Size , "NormalizeNode");

        __hetero_hint(DEVICE);
        P3[i] = (P3[i] - (*Mean)) / (*StdDev);
    }

    __hetero_section_end(NormalizeSection);
    __hetero_task_end(NormalizeWrapper);



    void* RMSWrapper = __hetero_task_begin( 5, P1, P1_Size, P2, P2_Size, P3, P3_Size, Res, Res_Size, num_pixels, 1, Res, Res_Size , "RootMeanSqWrapperNode");

    void* RMSSection = __hetero_section_begin();

    for(int i = 0; i < num_pixels; i++){
        __hetero_parallel_loop(1, 5, P1, P1_Size, P2, P2_Size, P3, P3_Size, Res, Res_Size, num_pixels, 1, Res, Res_Size , "RootMeanSqNode");

        __hetero_hint(DEVICE);
        double sum = (P1[i]* P1[i]) + (P2[i]* P2[i]) + (P3[i]*P3[i]);
        double avg = sum / 3.0;

        Res[i] = sqrt(avg);
    }

    __hetero_section_end(RMSSection);
    __hetero_task_end(RMSWrapper);



    __hetero_section_end(Section);

} 



int main(int argc, char** argv){

    int row = ROW;
    int col = COL;

    if(argc == 3){
        row = atoi(argv[1]);
        col = atoi(argv[2]);
    }

    

    std::size_t ImageSize = sizeof(int) * row * col;
    std::size_t num_pixels = row * col ;


    int* Im1 = (int*) malloc(ImageSize);
    int* Im2 = (int*) malloc(ImageSize);
    int* Im3 = (int*) malloc(ImageSize);
    int* Res = (int*) malloc(ImageSize);


    int* Min = (int*) malloc(sizeof(int));
    int* Max = (int*) malloc(sizeof(int));

    double* StdDev  = (double*) malloc(sizeof(double));
    double* Mean = (double*) malloc(sizeof(double));

    for(int i = 0; i < num_pixels; i++){
        int val = i % 15;
        Im1[i] = val;
        Im2[i] = val;
        Im3[i] = val;
        Res[i] = val;
    }




    printf("Launching pipeline dataflow graph! \n");
    void* PipelineDFG = __hetero_launch(
            /* Root Function */
            (void*) pipeline,
            /* Num Input Pairs */ 9,
            Im1, ImageSize, Im2, ImageSize, Im3, ImageSize,
            Res, ImageSize, Min, sizeof(int), Max, sizeof(int),
            StdDev, sizeof(double), Mean, sizeof(double),
            num_pixels,
            /* Num Output Pairs */ 1,
            Res, ImageSize
            );

    // Blocking call which waits
    // for the execution of MatmulDFG to complete.
    __hetero_wait(PipelineDFG);
    printf("DFG finished executing!\n");


    for(int i = 0; i < row; i++){
        for(int j =0; j < col; j++){
            printf("%d %s",Res[i*col+j], (j == col-1) ? "\n":"" );
        }
    }



    free(Im1);
    free(Im2);
    free(Im3);
    free(Res);
    return 0;


}

