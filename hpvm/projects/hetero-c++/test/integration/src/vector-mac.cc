#include "heterocc.h"
#include <stdio.h>


#define NUM_VEC_ELEM  50


#ifndef DEVICE
#define DEVICE hetero::CPU_TARGET
#endif

void vector_mac(int* ACC, std::size_t ACC_Size,
        int* V1, std::size_t V1_Size, 
        int* V2, std::size_t V2_Size){

    void* Section =  __hetero_section_begin();


    // Adding a wrapper task to enable compilation
    // to the GPU and CPU
    void* Wrapper = __hetero_task_begin(
            /* Num Input Pairs */ 3,
            ACC, ACC_Size, V1, V1_Size, V2, V2_Size,
            /* Num Output Pairs */ 1,
            ACC, ACC_Size,
            /* Optional Node Name */ "vector_mac_wrapper"
            );
    {
        void* InnerSection =  __hetero_section_begin();
        {
            for(int i = 0; i < (ACC_Size/sizeof(int)); i++){
                __hetero_parallel_loop(
                        /* Num Parallel Enclosing loops */ 1,
                        /* Num Input Pairs */ 3,
                        ACC, ACC_Size, V1, V1_Size, V2, V2_Size,
                        /* Num Output Pairs */ 1,
                        ACC, ACC_Size,
                        /* Optional Node Name */ "vector_mac_parallel_loop"
                        );

                __hetero_hint(/* TARGET */ DEVICE);

                ACC[i] += V1[i] * V2[i];


            }

        }

        __hetero_section_end(InnerSection);
    } __hetero_task_end(Wrapper);

    __hetero_section_end(Section);

}

int main(){

    std::size_t Vec_Size = sizeof(int) * NUM_VEC_ELEM;

    int* A = (int*) malloc(sizeof(int)* NUM_VEC_ELEM);
    int* B = (int*) malloc(sizeof(int)* NUM_VEC_ELEM);
    int* C = (int*) malloc(sizeof(int)* NUM_VEC_ELEM);

    // Setting initial values
    for(int k = 0; k < NUM_VEC_ELEM; k++){
        A[k] = 5;
        B[k] = -1;
        C[k] = 0;
    }



    printf("Launching vector multiply and accumlate dataflow graph! \n");
    void* MacDFG = __hetero_launch(
            /* Root Function */
            (void*) vector_mac,
            /* Num Input Pairs */ 3,
            C, Vec_Size, A, Vec_Size, B, Vec_Size,
            /* Num Output Pairs */ 1,
            C, Vec_Size
            );

    // Blocking call which waits
    // for the execution of MacDFG to complete.
    __hetero_wait(MacDFG);
    printf("DFG finished executing!\n");


    printf("Printing Accumalated Vector:\n");
    for(int i = 0; i < NUM_VEC_ELEM; i++){
        printf("%d  %s",C[i], (i == (NUM_VEC_ELEM - 1) ? "\n" : " "));
    }



    free(A);
    free(B);
    free(C);
    return 0;


}
