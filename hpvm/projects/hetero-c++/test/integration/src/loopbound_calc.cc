#include <stdlib.h>
#include <stdio.h>
#include "heterocc.h"

void dummy(size_t bytes_prod, size_t matrix_dim) {
    void* Section = __hetero_section_begin();

    int loop_size = matrix_dim / sizeof(int);

    for(int i = 0; i < loop_size; ++i){
        __hetero_parallel_loop(
        /* Number of parallel enclosing loops */ 1,
        /* Number of Input Buffer Pairs */ 1, bytes_prod,
        /* Number of Output Buffer Pairs */ 1, bytes_prod);
        printf("success");

    }

    __hetero_section_end(Section);
}

int main() {
  size_t x = 3, y = 7;
  auto l = __hetero_launch((void*)dummy, 2, x, y, 0);
  __hetero_wait(l);

  return 0;
}
