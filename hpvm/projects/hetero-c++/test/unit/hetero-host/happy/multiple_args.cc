#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>


void fn(int* ptr, std::size_t ptrSz,int* ptr2, std::size_t ptrSz2)
{
  auto s = __hetero_section_begin();

  // Runs in Parallel
  auto t1 = __hetero_task_begin(1,ptr,ptrSz,1,ptr,ptrSz);
  int idx1 = (ptrSz-sizeof(int)/sizeof(int));

  ptr[idx1] = 5;
  __hetero_task_end(t1);

  // Runs in Parallel
  auto t2 = __hetero_task_begin(1,ptr2,ptrSz2,1,ptr2,ptrSz2);  
  int idx2 = (ptrSz2-(sizeof(int)))/sizeof(int);

  ptr2[idx2] = 3;
  __hetero_task_end(t2);


  // Should automatically infer dependency with both t1 and t2 
  auto t3 = __hetero_task_begin(2,ptr,ptrSz,ptr2,ptrSz2,2,ptr,ptrSz,ptr2,ptrSz2);  
  ptr[idx1] = 2;
  ptr2[idx2] = 2;
  __hetero_task_end(t3);


  __hetero_section_end(s);

}




int main(){

    std::size_t P1Size = sizeof(int)*10;
    int* P1 = (int*) malloc(P1Size);


    std::size_t P2Size = sizeof(int)*30;
    int* P2 = (int*) malloc(P2Size);

    void* DFG = __hetero_launch((void*) fn, 
            2,  P1, P1Size, P2, P2Size,
            2,  P1, P1Size, P2, P2Size);


    __hetero_wait(DFG);




    return 0;
}
