#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>




void RootFn(int* ptr, std::size_t pSz)
{
    auto s = __hetero_section_begin();
    auto t = __hetero_task_begin(1, ptr, pSz, 1, ptr, pSz);

    int idx = (pSz-sizeof(int))/(sizeof(int));
    ptr[idx] = -1;

    __hetero_task_end(t);
    __hetero_section_end(s);

}


int main(){

    std::size_t PSize = sizeof(int)*10;
    int* P = (int*) malloc(PSize);

    void* DFG = __hetero_launch((void*) RootFn, 
            1,  P, PSize,
            1,  P, PSize);


    __hetero_wait(DFG);




    return 0;
}
