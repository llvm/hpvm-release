#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>



__attribute__((noinline))
void RootFn(int* ptr, std::size_t pSz)
{

    void* launch_begin = __hetero_launch_begin(1, ptr, pSz, 1,  ptr, pSz);
    {
        auto s = __hetero_section_begin();
        auto t = __hetero_task_begin(1, ptr, pSz, 1, ptr, pSz);

        int idx = (pSz-sizeof(int))/(sizeof(int));
        ptr[idx] = -1;

        __hetero_task_end(t);
        __hetero_section_end(s);

    }
    __hetero_launch_end(launch_begin);
}


int main(){

    std::size_t PSize = sizeof(int)*10;
    int* P = (int*) malloc(PSize);

    RootFn(P, PSize);




    return 0;
}
