#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>




void RootFn()
{
    auto s = __hetero_section_begin();

    for(int i = 0 ; i < 100; i++){
        __hetero_parallel_loop(1, 0, 0, "empty_loop");
        printf("Hello World!\n");
    }

    __hetero_section_end(s);

}


int main(){


    void* DFG = __hetero_launch((void*) RootFn, 
            0, 0);


    __hetero_wait(DFG);




    return 0;
}
