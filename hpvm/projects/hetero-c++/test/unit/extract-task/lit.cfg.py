# -*- Python -*-

# Configuration file for the 'lit' test runner.

import os
import sys
import re
import platform
import subprocess

import lit.util
import lit.formats
from lit.llvm import llvm_config
from lit.llvm.subst import FindTool
from lit.llvm.subst import ToolSubst

import lit.formats

config.name = "HeteroC++"
config.test_format = lit.formats.ShTest("0")
config.suffixes = ['.ll']
