/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/* 
 * Simple connection of Dataflow graphs.
 * The ChildDFG DFG will become a child 
 * graph of the RootFn DFG.
 */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>



void ChildDFG2(int* ptr, std::size_t pSz){
    auto s = __hetero_section_begin();
    auto t = __hetero_task_begin(1, ptr, pSz, 1, ptr, pSz);

    int idx = (pSz-sizeof(int))/(sizeof(int));
    ptr[idx] = -1;



    __hetero_task_end(t);
    __hetero_section_end(s);
}

void ChildDFG(int* ptr, std::size_t pSz){
    auto s = __hetero_section_begin();
    auto t = __hetero_task_begin(1, ptr, pSz, 1, ptr, pSz);

    ChildDFG2(ptr, pSz);


    __hetero_task_end(t);
    __hetero_section_end(s);
}

void RootFn(int* ptr, std::size_t pSz)
{
    auto s = __hetero_section_begin();
    auto t = __hetero_task_begin(1, ptr, pSz, 1, ptr, pSz);

    ChildDFG(ptr,pSz);

    __hetero_task_end(t);
    __hetero_section_end(s);

}


