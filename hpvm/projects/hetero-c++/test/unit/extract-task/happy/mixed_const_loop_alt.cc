#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>

#define block_size 32

void dummy(double * __restrict m1, size_t bytes_m1,
                double * __restrict m2, size_t bytes_m2,
                double * __restrict prod, size_t bytes_prod,
                size_t matrix_dim) {
        void* Section = __hetero_section_begin();
        int mod_bound = matrix_dim / block_size;
        for(int i = 0; i < mod_bound; i += 1) {
                for(int j = 0; j < matrix_dim; j += 1) {
                        __hetero_parallel_loop(2, 4, m1, bytes_m1, m2, bytes_m2, matrix_dim, prod, bytes_prod, 1, prod, bytes_prod, "mixed_loop");
                        printf("it works\n");
                }
        }
        __hetero_section_end(Section);
}
