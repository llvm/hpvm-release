#include <stdio.h>
#include <stdlib.h>
#include "heterocc.h"

void root(int** Aptr, size_t Aptr_size, int* A, size_t A_size, int64_t n) {
  auto s = __hetero_section_begin();

  for (int64_t i = 0; i < n; i++) {
    for (int64_t j = 0;  j < n; j++) {
      __hetero_parallel_loop(2, 3, Aptr, Aptr_size, A, A_size, n, 1, A, A_size);
      Aptr[i][j] = i * j;
    }
  }

  __hetero_section_end(s);
}

int main(int argc, char** argv) {
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <n>\n", argv[0]);
    exit(1);
  }

  int64_t n = atoi(argv[1]);

  int** Aptr = new int*[n]; size_t Aptr_size = sizeof(int*) * n;
  int* A = new int[n * n];  size_t A_size = sizeof(int) * n * n;
  for (int i = 0; i < n; i++) Aptr[i] = &(A[i * n]);

  auto r = __hetero_launch((void*) root, 3, Aptr, Aptr_size, A, A_size, n, 1, A, A_size);
  __hetero_wait(r);

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      printf("%2d ", Aptr[i][j]);
    }
    printf("\n");
  }

  delete [] A;
  delete [] Aptr;

  return 0;
}
