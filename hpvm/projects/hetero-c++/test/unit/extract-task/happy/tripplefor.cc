/***************************************************************************
 *  *cr
 *   *cr            (C) Copyright 2010 The Board of Trustees of the
 *    *cr                        University of Illinois
 *     *cr                         All Rights Reserved
 *      *cr
 *       ***************************************************************************/

/* 
 *  * Nearly minimal for loop
 *   */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>


void forloop(int* ptr, std::size_t ptrSz, std::size_t M, std::size_t N, std::size_t K)
{
    auto s = __hetero_section_begin();

    for (int m = 0; m < M; ++m) {
            for(int n = 0; n < N ; ++n){
                for(int k =0; k < K; ++k){
                    __hetero_parallel_loop(/* NumLoops */ 3 ,4, ptr, ptrSz, M, N, K , 4, ptr, ptrSz, M, N, K);
                    int idx = (m+n)*k % 10;
                    ptr[idx] = m*n+k;

                }

            
            }

    }
    __hetero_section_end(s);
    
}

