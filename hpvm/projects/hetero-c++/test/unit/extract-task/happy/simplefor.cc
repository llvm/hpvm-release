/***************************************************************************
 *  *cr
 *   *cr            (C) Copyright 2010 The Board of Trustees of the
 *    *cr                        University of Illinois
 *     *cr                         All Rights Reserved
 *      *cr
 *       ***************************************************************************/

/* 
 *  * Nearly minimal for loop
 *   */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>


void forloop(int* ptr, std::size_t ptrSz)
{
    auto s = __hetero_section_begin();

    for (std::size_t  m = 0; m < (ptrSz/sizeof(int)); ++m) {
        __hetero_parallel_loop(1, 1, ptr, ptrSz, 1, ptr, ptrSz);
        if(m % 2)
            ptr[m] = m+1;
        else
            ptr[m] = m;
    }
    __hetero_section_end(s);
    
}

