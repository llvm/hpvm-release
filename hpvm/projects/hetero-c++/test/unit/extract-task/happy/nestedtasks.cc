/***************************************************************************
*cr
*cr            (C) Copyright 2010 The Board of Trustees of the
*cr                        University of Illinois
*cr                         All Rights Reserved
*cr
***************************************************************************/

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>


void fn(int* p, std::size_t pSz)
{

    auto s1 = __hetero_section_begin();
    {
        auto t1 = __hetero_task_begin(1, p, pSz, 1, p, pSz);
        {
            auto ss1 = __hetero_section_begin();
            {
                auto st1 = __hetero_task_begin(1, p, pSz, 1, p, pSz);
                {
                    int idx1 = (pSz-sizeof(int)/sizeof(int));
                    p[idx1] = 1;

                }
                __hetero_task_end(st1);
                auto st2 = __hetero_task_begin(1,p,pSz , 1, p, pSz);
                {
                    int idx2 = (pSz-sizeof(int)/sizeof(int));
                    p[idx2] = 2;

                }
                __hetero_task_end(st2);

            }
            __hetero_section_end(ss1);

        }
        __hetero_task_end(t1);

        auto t2 = __hetero_task_begin(1, p, pSz, 1, p, pSz);
        {
            int idx3 = (pSz-sizeof(int)/sizeof(int));
            p[idx3] = 3;

        }
        __hetero_task_end(t2);

    }
    __hetero_section_end(s1);


}
