/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/* 
 * 3 Tasks where the first task t1's outputs are partially
 * used by t2 and t3. Both t3 and t2 should be infered to be dependent
 * on t1.
 */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>

void fn(int* ptr, std::size_t ptrSz,int* ptr2, std::size_t ptrSz2)
{
  auto s = __hetero_section_begin();

  // t1 should have outgoing edges to both t2 and t3
  auto t1 = __hetero_task_begin(2,ptr,ptrSz,ptr2,ptrSz2,2,ptr,ptrSz,ptr2,ptrSz2);
  int idx1 = (ptrSz-sizeof(int))/sizeof(int);
  int idx2 = (ptrSz2-(3*sizeof(int)))/sizeof(int);

  ptr[idx1] = 5;
  ptr2[idx2] = 3;
  __hetero_task_end(t1);


  // Should infer dependency to t1
  auto t2 = __hetero_task_begin(1,ptr2,ptrSz2,1,ptr2,ptrSz2);  
  ptr2[idx2] = 3;
  __hetero_task_end(t2);


  // Should infer dependency to t1
  auto t3 = __hetero_task_begin(1,ptr,ptrSz,1,ptr,ptrSz);  
  ptr[idx1] = 2;
  __hetero_task_end(t3);


  __hetero_section_end(s);

}
