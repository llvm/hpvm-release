/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/* 
 * 3 tasks where t1 and t2 are infered to be Parallel while
 * t3 should have edges from both t1 and t2
 */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>

void fn(int* ptr, std::size_t ptrSz,int* ptr2, std::size_t ptrSz2)
{
  auto s = __hetero_section_begin();

  // Runs in Parallel
  auto t1 = __hetero_task_begin(1,ptr,ptrSz,1,ptr,ptrSz);

  __hpvm_priv(1, ptr, 64);

  int idx1 = (ptrSz-sizeof(int)/sizeof(int));

  ptr[idx1] = 5;
  __hetero_task_end(t1);


  __hetero_section_end(s);

}
