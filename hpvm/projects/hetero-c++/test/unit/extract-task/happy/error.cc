#include <heterocc.h>

void launch(int n, int* A, size_t sizeA, int* B, size_t sizeB, int* C,
            size_t sizeC) {
  auto l = __hetero_launch_begin(4, n, A, sizeA, B, sizeB, C, sizeC,
                                 3, A, sizeA, B, sizeB, C, sizeC);
  auto s = __hetero_section_begin();

  for (int i = 0; i < n+1; i++) {
    __hetero_parallel_loop(1, 3, A, sizeA, B , sizeB, n, 1, B, sizeB, "node1");
    if (i < n)
      B[i] = A[i] * 2;
    else B[i] = -1;

  }

#if 1
  for (int i = 0; i < n-1; i++) {
    __hetero_parallel_loop(1, 3, A, sizeA, B, sizeB , n, 1, C, sizeC, "node2");
    C[i] = A[i] * A[i+1];
  }
#endif

  __hetero_section_end(s);
  __hetero_launch_end(l);
}

int main() {
  int n = 10;
  int* A = new int[n];
  int* B = new int[n+1];
  int* C = new int[n-1];

  for (int i = 0; i < n; i++) A[i] = i + 1;

  launch(n, A, sizeof(int) * n, B, sizeof(int) * (n+1), C, sizeof(int) * (n-1));

  delete [] A; delete [] B; delete [] C;
  return 0;
}

