/***************************************************************************
 *  *cr
 *   *cr            (C) Copyright 2010 The Board of Trustees of the
 *    *cr                        University of Illinois
 *     *cr                         All Rights Reserved
 *      *cr
 *       ***************************************************************************/

/* 
 *  * Nearly minimal for loop
 *   */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>


void forloop(int* ptr, std::size_t ptrSz, std::size_t M, std::size_t N)
{
    auto s = __hetero_section_begin();

    for (int m = 0; m < (ptrSz/sizeof(int))/4; ++m) {
        __hetero_parallel_loop(1, 3, ptr, ptrSz, M, N, 3, ptr, ptrSz, M, N);
        auto s1 = __hetero_section_begin();

            for(int n = 0; n < (ptrSz/sizeof(int))/3 ; ++n){
                __hetero_parallel_loop(1, 3, ptr, ptrSz, M, N , 3, ptr, ptrSz, M, N);
                int idx = (m+n) % 10;
                ptr[idx] = m*n;
            
            }
        __hetero_section_end(s1);

    }
    __hetero_section_end(s);
    
}

