#include <stdio.h>
#include <stdlib.h>
#include "heterocc.h"

void root(int* A, size_t size_A, long int n) {
    auto s = __hetero_section_begin();
    auto t = __hetero_task_begin(2, A, size_A, n, 1, A, size_A);
    auto si = __hetero_section_begin();

    for (long int i = 0; i <= n; i++) {
        __hetero_parallel_loop(1, 2, A, size_A, n, 1, A, size_A);
        A[i] *= A[i];

    }

    __hetero_section_end(si);
    __hetero_task_end(t);
    __hetero_section_end(s);

}

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <n>\n", argv[0]);
        exit(1);

    }

    long int n = atoi(argv[1]);
    int* A = new int[n+1];
    for (int i = 0; i <= n; i++) A[i] = i;
    size_t size_A = sizeof(int) * (n+1);

    auto dfg = __hetero_launch((void*)root, 2, A, size_A, n, 1, A, size_A);
    __hetero_wait(dfg);

    for (int i = 0; i <= n; i++) { printf("%2d ", A[i]);  } printf("\n");
    printf("Expected\n");
    for (int i = 0; i <= n; i++) { printf("%2d ", i*i);  } printf("\n");

    delete [] A;
    return 0;

}
