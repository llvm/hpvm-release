/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/* 
 * Single Task which overwrites the last value
 * in an array.
 */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>


void RootFn(int* __restrict__ ptr, std::size_t pSz)
{
    auto s = __hetero_section_begin();
    auto t = __hetero_task_begin(1, ptr, pSz, 1, ptr, pSz);

    int idx = (pSz-sizeof(int))/(sizeof(int));
    ptr[idx] = -1;

    __hetero_task_end(t);
    __hetero_section_end(s);

}

