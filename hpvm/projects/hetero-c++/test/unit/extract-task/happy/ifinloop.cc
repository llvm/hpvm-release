#include <stdlib.h>
#include <stdio.h>
#include "heterocc.h"

void func(int* arr, size_t size_arr, long int n) {
    auto s = __hetero_section_begin();
    for (long int i = 0; i < n; i++) {
        __hetero_parallel_loop(1, 2, arr, size_arr, n, 1, arr, size_arr);
        if (arr[i] == 0) { arr[i] = -1;  }

    }
    __hetero_section_end(s);

}

int main(int argc, char** argv) {
    if (argc != 2) { 
        fprintf(stderr, "Usage: %s <n>\n", argv[0]);
        exit(1);

    }

    long int n = atoi(argv[1]);

    int* arr = new int[n];
    for (int i = 0; i < n; i++) arr[i] = i % 5 == 0 ? 0 : i;
    size_t size_arr = sizeof(int) * n;

    auto root = __hetero_launch((void*)func, 2, arr, size_arr, n, 1, arr, size_arr);
    __hetero_wait(root);

    printf("Actual Output:\n");
    for (int i = 0; i < n; i++) printf("%2d ", arr[i]);
    printf("\n");
    printf("Expected Output:\n");
    for (int i = 0; i < n; i++) printf("%2d ", i % 5 == 0 ? -1 : i);
    printf("\n");

    delete [] arr;
    return 0;

}
