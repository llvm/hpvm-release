/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/* 
 * 3 tasks that are dependent on each other with multiple
 * pointer dependencies
 */

#include "heterocc.h"
#include <stdlib.h>
#include <stdio.h>

void fn(int* ptr, std::size_t ptrSz,int* ptr2, std::size_t ptrSz2)
{
  auto s = __hetero_section_begin();

  auto t = __hetero_task_begin(2,ptr,ptrSz,ptr2,ptrSz2,2,ptr,ptrSz,ptr2,ptrSz2);
  int idx1 = (ptrSz-sizeof(int))/sizeof(int);
  int idx2 = (ptrSz2-(2*sizeof(int)))/sizeof(int);

  ptr[idx1] = 5;
  ptr2[idx2] = 5;
  __hetero_task_end(t);

  // Should automatically infer dependency with t
  auto t2 = __hetero_task_begin(2,ptr,ptrSz,ptr2,ptrSz2,2,ptr,ptrSz,ptr2,ptrSz2);  
  ptr[idx1] = 3;

  int idx3 = (ptrSz2-(3*sizeof(int)))/sizeof(int);
  ptr2[idx3] = 3;
  __hetero_task_end(t2);


  // Should automatically infer dependency with t2
  auto t3 = __hetero_task_begin(2,ptr,ptrSz,ptr2,ptrSz2,2,ptr,ptrSz,ptr2,ptrSz2);  
  ptr[idx1] = 2;

  int idx4 = (ptrSz2-(1*sizeof(int)))/sizeof(int);
  ptr2[idx4] = 2;
  __hetero_task_end(t3);

  __hetero_section_end(s);
}
