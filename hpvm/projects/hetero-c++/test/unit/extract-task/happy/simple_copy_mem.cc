#include <stdio.h>
#include <stdlib.h>
#include "heterocc.h"

void root(int* Aptr, size_t Aptr_size, size_t n) {
  auto s = __hetero_section_begin();

      void* t = __hetero_task_begin(2, Aptr, Aptr_size, n, 1, Aptr, Aptr_size);
      Aptr[1] = 0;
      __hetero_copy_mem(Aptr, Aptr, n);
      __hetero_task_end(t);

  __hetero_section_end(s);
}

int main(int argc, char** argv) {
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <n>\n", argv[0]);
    exit(1);
  }

  int64_t n = atoi(argv[1]);

  malloc(64);

  size_t A_size = sizeof(int) * n * n; 
  int* A = (int*) __hetero_malloc(A_size);  

  auto r = __hetero_launch((void*) root, 2,  A, A_size, n, 1, A, A_size);
  __hetero_wait(r);

  __hetero_request_mem(A, A_size);


  __hetero_free(A);


  return 0;
}
