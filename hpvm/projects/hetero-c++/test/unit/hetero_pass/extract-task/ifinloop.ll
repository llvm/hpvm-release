; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/ifinloop.cc'
source_filename = "happy/ifinloop.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@stderr = external dso_local local_unnamed_addr global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [15 x i8] c"Usage: %s <n>\0A\00", align 1
@.str.2 = private unnamed_addr constant [5 x i8] c"%2d \00", align 1
@str = private unnamed_addr constant [15 x i8] c"Actual Output:\00", align 1
@str.5 = private unnamed_addr constant [17 x i8] c"Expected Output:\00", align 1

; CHECK-LABEL: void @_Z4funcPiml(
; CHECK-NOT:  %call = tail call i8* @__hpvm_parallel_section_begin()
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %arr, i32 1, i32* %arr)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i64)* @_Z4funcPiml.for.body_reorder, i64 %n)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 2, i32 2, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)

; CHECK-LABEL: void @_Z4funcPiml.for.body_reorder(
; CHECK: call void @__hpvm__hint(
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %arr, i32 1, i32* %arr)
; CHECK: %arrayidx_cloned = getelementptr inbounds i32, i32* %arr, i64 %__hpvm__getNodeInstanceID_x_cloned
; CHECK: call void (i32, ...) @__hpvm__return(i32 2, i32* %arr, i64 %size_arr)




; Function Attrs: uwtable
define dso_local void @_Z4funcPiml(i32* %arr, i64 %size_arr, i64 %n) #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %cmp13 = icmp sgt i64 %n, 0
  br i1 %cmp13, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.inc, %entry
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void

for.body:                                         ; preds = %entry, %for.inc
  %i.014 = phi i64 [ %inc, %for.inc ], [ 0, %entry ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 2, i32* %arr, i64 %size_arr, i64 %n, i32 1, i32* %arr, i64 %size_arr)
  %arrayidx = getelementptr inbounds i32, i32* %arr, i64 %i.014
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !2
  %cmp1 = icmp eq i32 %0, 0
  br i1 %cmp1, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  store i32 -1, i32* %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %inc = add nuw nsw i64 %i.014, 1
  %exitcond = icmp eq i64 %inc, %n
  br i1 %exitcond, label %for.cond.cleanup, label %for.body
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

; Function Attrs: norecurse uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readonly %argv) local_unnamed_addr #2 {
entry:
  %cmp = icmp eq i32 %argc, 2
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !6
  %1 = load i8*, i8** %argv, align 8, !tbaa !6
  %call = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i64 0, i64 0), i8* %1) #10
  tail call void @exit(i32 1) #11
  unreachable

if.end:                                           ; preds = %entry
  %arrayidx1 = getelementptr inbounds i8*, i8** %argv, i64 1
  %2 = load i8*, i8** %arrayidx1, align 8, !tbaa !6
  %call2 = tail call i32 @atoi(i8* %2) #12
  %conv = sext i32 %call2 to i64
  %3 = tail call { i64, i1 } @llvm.umul.with.overflow.i64(i64 %conv, i64 4)
  %4 = extractvalue { i64, i1 } %3, 1
  %5 = extractvalue { i64, i1 } %3, 0
  %6 = select i1 %4, i64 -1, i64 %5
  %call3 = tail call i8* @_Znam(i64 %6) #13
  %7 = bitcast i8* %call3 to i32*
  %cmp575 = icmp sgt i32 %call2, 0
  br i1 %cmp575, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.body, %if.end
  %mul = shl nsw i64 %conv, 2
  %call8 = tail call i8* (i8*, ...) @__hpvm_launch(i8* bitcast (void (i32*, i64, i64)* @_Z4funcPiml to i8*), i32 2, i8* nonnull %call3, i64 %mul, i64 %conv, i32 1, i8* nonnull %call3, i64 %mul)
  tail call void @__hpvm_wait(i8* %call8)
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @str, i64 0, i64 0))
  %cmp1372 = icmp sgt i32 %call2, 0
  br i1 %cmp1372, label %for.body15, label %for.cond.cleanup14

for.body:                                         ; preds = %if.end, %for.body
  %indvars.iv81 = phi i64 [ %indvars.iv.next82, %for.body ], [ 0, %if.end ]
  %8 = trunc i64 %indvars.iv81 to i32
  %rem = urem i32 %8, 5
  %cmp6 = icmp eq i32 %rem, 0
  %9 = trunc i64 %indvars.iv81 to i32
  %cond = select i1 %cmp6, i32 0, i32 %9
  %arrayidx7 = getelementptr inbounds i32, i32* %7, i64 %indvars.iv81
  store i32 %cond, i32* %arrayidx7, align 4, !tbaa !2
  %indvars.iv.next82 = add nuw nsw i64 %indvars.iv81, 1
  %exitcond83 = icmp eq i64 %indvars.iv.next82, %conv
  br i1 %exitcond83, label %for.cond.cleanup, label %for.body

for.cond.cleanup14:                               ; preds = %for.body15, %for.cond.cleanup
  %putchar = tail call i32 @putchar(i32 10)
  %puts68 = tail call i32 @puts(i8* getelementptr inbounds ([17 x i8], [17 x i8]* @str.5, i64 0, i64 0))
  %cmp2770 = icmp sgt i32 %call2, 0
  br i1 %cmp2770, label %for.body29, label %for.cond.cleanup28

for.body15:                                       ; preds = %for.cond.cleanup, %for.body15
  %indvars.iv78 = phi i64 [ %indvars.iv.next79, %for.body15 ], [ 0, %for.cond.cleanup ]
  %arrayidx17 = getelementptr inbounds i32, i32* %7, i64 %indvars.iv78
  %10 = load i32, i32* %arrayidx17, align 4, !tbaa !2
  %call18 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i32 %10)
  %indvars.iv.next79 = add nuw nsw i64 %indvars.iv78, 1
  %exitcond80 = icmp eq i64 %indvars.iv.next79, %conv
  br i1 %exitcond80, label %for.cond.cleanup14, label %for.body15

for.cond.cleanup28:                               ; preds = %for.body29, %for.cond.cleanup14
  %putchar69 = tail call i32 @putchar(i32 10)
  tail call void @_ZdaPv(i8* nonnull %call3) #14
  ret i32 0

for.body29:                                       ; preds = %for.cond.cleanup14, %for.body29
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body29 ], [ 0, %for.cond.cleanup14 ]
  %11 = trunc i64 %indvars.iv to i32
  %rem30 = urem i32 %11, 5
  %cmp31 = icmp eq i32 %rem30, 0
  %12 = trunc i64 %indvars.iv to i32
  %cond35 = select i1 %cmp31, i32 -1, i32 %12
  %call36 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i32 %cond35)
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %conv
  br i1 %exitcond, label %for.cond.cleanup28, label %for.body29
}

; Function Attrs: nofree nounwind
declare dso_local i32 @fprintf(%struct._IO_FILE* nocapture, i8* nocapture readonly, ...) local_unnamed_addr #3

; Function Attrs: noreturn nounwind
declare dso_local void @exit(i32) local_unnamed_addr #4

; Function Attrs: inlinehint nounwind readonly uwtable
define available_externally dso_local i32 @atoi(i8* nonnull %__nptr) local_unnamed_addr #5 {
entry:
  %call = tail call i64 @strtol(i8* nocapture nonnull %__nptr, i8** null, i32 10) #15
  %conv = trunc i64 %call to i32
  ret i32 %conv
}

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.umul.with.overflow.i64(i64, i64) #6

; Function Attrs: nobuiltin nofree
declare dso_local noalias nonnull i8* @_Znam(i64) local_unnamed_addr #7

declare dso_local i8* @__hpvm_launch(i8*, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_wait(i8*) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare dso_local i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #3

; Function Attrs: nobuiltin nounwind
declare dso_local void @_ZdaPv(i8*) local_unnamed_addr #8

; Function Attrs: nofree nounwind
declare dso_local i64 @strtol(i8* readonly, i8** nocapture, i32) local_unnamed_addr #3

; Function Attrs: nofree nounwind
declare i32 @puts(i8* nocapture readonly) local_unnamed_addr #9

; Function Attrs: nofree nounwind
declare i32 @putchar(i32) local_unnamed_addr #9

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { norecurse uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nofree nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable }
attributes #7 = { nobuiltin nofree "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nofree nounwind }
attributes #10 = { cold }
attributes #11 = { noreturn nounwind }
attributes #12 = { nounwind readonly }
attributes #13 = { builtin }
attributes #14 = { builtin nounwind }
attributes #15 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
