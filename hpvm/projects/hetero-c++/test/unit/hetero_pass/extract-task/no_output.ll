; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/no_output.cc'
source_filename = "happy/no_output.cc"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [19 x i8] c"Inside dummy: %lu\0A\00", align 1

; CHECK-LABEL: void @_Z4rootPim
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %A, i32 0)
; CHECK: %__hpvm__createNodeND1 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, 
; CHECK: call void @__hpvm__bindIn(
; CHECK: call void @__hpvm__bindIn(

; CHECK-LABEL: @_Z4rootPim.extracted_reorder(
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %A, i32 0)
; CHECK: %call2_cloned = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i64 0, i64 0), i64 %sizeA)
; CHECK: call void (i32, ...) @__hpvm__return(i32 0)


; Function Attrs: mustprogress uwtable
define dso_local void @_Z4rootPim(i32* %A, i64 %sizeA) #0 {
entry:
  %call = call i8* @__hetero_section_begin()
  %call1 = call i8* (i32, ...) @__hetero_task_begin(i32 1, i32* %A, i64 %sizeA, i32 0)
  %call2 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i64 0, i64 0), i64 %sizeA)
  call void @__hetero_task_end(i8* %call1)
  call void @__hetero_section_end(i8* %call)
  ret void
}

declare dso_local i8* @__hetero_section_begin() local_unnamed_addr #1

declare dso_local i8* @__hetero_task_begin(i32, ...) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare dso_local noundef i32 @printf(i8* nocapture noundef readonly, ...) local_unnamed_addr #2

declare dso_local void @__hetero_task_end(i8*) local_unnamed_addr #1

declare dso_local void @__hetero_section_end(i8*) local_unnamed_addr #1

; Function Attrs: mustprogress norecurse uwtable
define dso_local i32 @main() local_unnamed_addr #3 {
entry:
  %call = call noalias nonnull dereferenceable(4) i8* @_Znam(i64 4) #7
  %0 = bitcast i8* %call to i32*
  store i32 13, i32* %0, align 4, !tbaa !3
  %call1 = call i8* (i8*, ...) @__hetero_launch(i8* bitcast (void (i32*, i64)* @_Z4rootPim to i8*), i32 1, i8* nonnull %call, i64 4, i32 0)
  call void @__hetero_wait(i8* %call1)
  %putchar = call i32 @putchar(i32 10)
  call void @_ZdaPv(i8* %call) #8
  ret i32 0
}

; Function Attrs: nobuiltin allocsize(0)
declare dso_local nonnull i8* @_Znam(i64) local_unnamed_addr #4

declare dso_local i8* @__hetero_launch(i8*, ...) local_unnamed_addr #1

declare dso_local void @__hetero_wait(i8*) local_unnamed_addr #1

; Function Attrs: nobuiltin nounwind
declare dso_local void @_ZdaPv(i8*) local_unnamed_addr #5

; Function Attrs: nofree nounwind
declare noundef i32 @putchar(i32 noundef) local_unnamed_addr #6

attributes #0 = { mustprogress uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { nofree nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { mustprogress norecurse uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #4 = { nobuiltin allocsize(0) "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { nobuiltin nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #6 = { nofree nounwind }
attributes #7 = { builtin allocsize(0) }
attributes #8 = { builtin nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git 02fb671875220238bc41376434ebd070dc5f7f43)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C++ TBAA"}
