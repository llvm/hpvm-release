; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/nestedtasks.cc'
source_filename = "happy/nestedtasks.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; CHECK-LABEL: void @_Z2fnPim(
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %p, i32 1, i32* %p)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: %__hpvm__createNodeND9 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z2fnPim.extracted.3_reorder, i64 1)
; CHECK: %__hpvm__edge8 = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND, i8* %__hpvm__createNodeND9, i32 1, i32 1, i32 1, i32 0)
; CHECK:  %__hpvm__edge = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND, i8* %__hpvm__createNodeND9, i32 1, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND9, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND9, i32 1, i32 1, i32 0)


; CHECK-LABEL: void @_Z2fnPim.extracted.2_reorder(
; CHECK: %__hpvm__createNodeND3 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z2fnPim.extracted_reorder, i64 1)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND3, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND3, i32 1, i32 1, i32 0)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z2fnPim.extracted.1_reorder, i64 1)
; CHECK: %__hpvm__edge2 = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND3, i8* %__hpvm__createNodeND, i32 1, i32 1, i32 1, i32 0)
; CHECK: %__hpvm__edge = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND3, i8* %__hpvm__createNodeND, i32 1, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)


; CHECK-LABEL: void @_Z2fnPim.extracted.3_reorder(
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %p, i32 1, i32* %p)
; CHECK: call void (i32, ...) @__hpvm__return(i32 2, i32* %p, i64 %pSz)


; CHECK-LABEL: void @_Z2fnPim.extracted_reorder(
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %p, i32 1, i32* %p)
; CHECK: call void (i32, ...) @__hpvm__return(i32 2, i32* %p, i64 %pSz)


; CHECK-LABEL: void @_Z2fnPim.extracted.1_reorder(
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %p, i32 1, i32* %p)
; CHECK: call void (i32, ...) @__hpvm__return(i32 2, i32* %p, i64 %pSz)


; Function Attrs: uwtable
define dso_local void @_Z2fnPim(i32* %p, i64 %pSz) local_unnamed_addr #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %call1 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 1, i32* %p, i64 %pSz, i32 1, i32* %p, i64 %pSz)
  %call2 = tail call i8* @__hpvm_parallel_section_begin()
  %call3 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 1, i32* %p, i64 %pSz, i32 1, i32* %p, i64 %pSz)
  %conv = shl i64 %pSz, 32
  %sext = add i64 %conv, -4294967296
  %idxprom = ashr exact i64 %sext, 32
  %arrayidx = getelementptr inbounds i32, i32* %p, i64 %idxprom
  store i32 1, i32* %arrayidx, align 4, !tbaa !2
  tail call void @__hpvm_task_end(i8* %call3)
  %call4 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 1, i32* %p, i64 %pSz, i32 1, i32* %p, i64 %pSz)
  store i32 2, i32* %arrayidx, align 4, !tbaa !2
  tail call void @__hpvm_task_end(i8* %call4)
  tail call void @__hpvm_parallel_section_end(i8* %call2)
  tail call void @__hpvm_task_end(i8* %call1)
  %call9 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 1, i32* %p, i64 %pSz, i32 1, i32* %p, i64 %pSz)
  store i32 3, i32* %arrayidx, align 4, !tbaa !2
  tail call void @__hpvm_task_end(i8* %call9)
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local i8* @__hpvm_task_begin(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_task_end(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
