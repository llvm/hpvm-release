; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/example.cc'
source_filename = "happy/example.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@stderr = external dso_local local_unnamed_addr global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [15 x i8] c"Usage: %s <n>\0A\00", align 1
@.str.1 = private unnamed_addr constant [5 x i8] c"%2d \00", align 1


; CHECK-LABEL: void @_Z4rootPPimS_ml(
; CHECK-NOT: %call = tail call i8* @__hpvm_parallel_section_begin()
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 2, i32** %Aptr, i32* %A, i32 1, i32* %A)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 2, void (i32**, i64, i32*, i64, i64)* @_Z4rootPPimS_ml.for.body4.lr.ph_reorder, i64 %n, i64 %n)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 2, i32 2, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 3, i32 3, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 4, i32 4, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)

; CHECK-LABEL: void @_Z4rootPPimS_ml.for.body4.lr.ph_reorder(
; CHECK: call void @__hpvm__hint(
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_y_cloned = call i64 @__hpvm__getNodeInstanceID_y(i8* %__hpvm__getNode_cloned)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 2, i32** %Aptr, i32* %A, i32 1, i32* %A)
; CHECK: %mul_cloned = mul nsw i64 %__hpvm__getNodeInstanceID_y_cloned, %__hpvm__getNodeInstanceID_x_cloned
; CHECK: call void (i32, ...) @__hpvm__return(i32 2, i32* %A, i64 %A_size)

 



; Function Attrs: uwtable
define dso_local void @_Z4rootPPimS_ml(i32** %Aptr, i64 %Aptr_size, i32* %A, i64 %A_size, i64 %n) #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %cmp25 = icmp sgt i64 %n, 0
  br i1 %cmp25, label %for.body4.lr.ph, label %for.cond.cleanup

for.body4.lr.ph:                                  ; preds = %entry, %for.cond.cleanup3
  %i.026 = phi i64 [ %inc7, %for.cond.cleanup3 ], [ 0, %entry ]
  %arrayidx = getelementptr inbounds i32*, i32** %Aptr, i64 %i.026
  br label %for.body4

for.cond.cleanup:                                 ; preds = %for.cond.cleanup3, %entry
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void

for.cond.cleanup3:                                ; preds = %for.body4
  %inc7 = add nuw nsw i64 %i.026, 1
  %exitcond28 = icmp eq i64 %inc7, %n
  br i1 %exitcond28, label %for.cond.cleanup, label %for.body4.lr.ph

for.body4:                                        ; preds = %for.body4, %for.body4.lr.ph
  %j.024 = phi i64 [ 0, %for.body4.lr.ph ], [ %inc, %for.body4 ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 2, i32 3, i32** %Aptr, i64 %Aptr_size, i32* %A, i64 %A_size, i64 %n, i32 1, i32* %A, i64 %A_size)
  %mul = mul nsw i64 %j.024, %i.026
  %conv = trunc i64 %mul to i32
  %0 = load i32*, i32** %arrayidx, align 8, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %0, i64 %j.024
  store i32 %conv, i32* %arrayidx5, align 4, !tbaa !6
  %inc = add nuw nsw i64 %j.024, 1
  %exitcond = icmp eq i64 %inc, %n
  br i1 %exitcond, label %for.cond.cleanup3, label %for.body4
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

; Function Attrs: norecurse uwtable
define dso_local i32 @main(i32 %argc, i8** nocapture readonly %argv) local_unnamed_addr #2 {
entry:
  %cmp = icmp eq i32 %argc, 2
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !2
  %1 = load i8*, i8** %argv, align 8, !tbaa !2
  %call = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i64 0, i64 0), i8* %1) #10
  tail call void @exit(i32 1) #11
  unreachable

if.end:                                           ; preds = %entry
  %arrayidx1 = getelementptr inbounds i8*, i8** %argv, i64 1
  %2 = load i8*, i8** %arrayidx1, align 8, !tbaa !2
  %call2 = tail call i32 @atoi(i8* %2) #12
  %conv = sext i32 %call2 to i64
  %3 = tail call { i64, i1 } @llvm.umul.with.overflow.i64(i64 %conv, i64 8)
  %4 = extractvalue { i64, i1 } %3, 1
  %5 = extractvalue { i64, i1 } %3, 0
  %6 = select i1 %4, i64 -1, i64 %5
  %call3 = tail call i8* @_Znam(i64 %6) #13
  %7 = bitcast i8* %call3 to i32**
  %mul = shl nsw i64 %conv, 3
  %mul4 = mul nsw i64 %conv, %conv
  %8 = tail call { i64, i1 } @llvm.umul.with.overflow.i64(i64 %mul4, i64 4)
  %9 = extractvalue { i64, i1 } %8, 1
  %10 = extractvalue { i64, i1 } %8, 0
  %11 = select i1 %9, i64 -1, i64 %10
  %call5 = tail call i8* @_Znam(i64 %11) #13
  %12 = bitcast i8* %call5 to i32*
  %mul6 = shl nsw i64 %conv, 2
  %mul7 = mul i64 %mul6, %conv
  %cmp981 = icmp sgt i32 %call2, 0
  br i1 %cmp981, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.body, %if.end
  %call14 = tail call i8* (i8*, ...) @__hpvm_launch(i8* bitcast (void (i32**, i64, i32*, i64, i64)* @_Z4rootPPimS_ml to i8*), i32 3, i8* nonnull %call3, i64 %mul, i8* nonnull %call5, i64 %mul7, i64 %conv, i32 1, i8* nonnull %call5, i64 %mul7)
  tail call void @__hpvm_wait(i8* %call14)
  %cmp1878 = icmp sgt i32 %call2, 0
  br i1 %cmp1878, label %for.body25.lr.ph, label %delete.notnull

for.body:                                         ; preds = %if.end, %for.body
  %indvars.iv87 = phi i64 [ %indvars.iv.next88, %for.body ], [ 0, %if.end ]
  %mul11 = mul nsw i64 %indvars.iv87, %conv
  %arrayidx12 = getelementptr inbounds i32, i32* %12, i64 %mul11
  %arrayidx13 = getelementptr inbounds i32*, i32** %7, i64 %indvars.iv87
  store i32* %arrayidx12, i32** %arrayidx13, align 8, !tbaa !2
  %indvars.iv.next88 = add nuw nsw i64 %indvars.iv87, 1
  %exitcond89 = icmp eq i64 %indvars.iv.next88, %conv
  br i1 %exitcond89, label %for.cond.cleanup, label %for.body

for.body25.lr.ph:                                 ; preds = %for.cond.cleanup, %for.cond.cleanup24
  %indvars.iv84 = phi i64 [ %indvars.iv.next85, %for.cond.cleanup24 ], [ 0, %for.cond.cleanup ]
  %arrayidx27 = getelementptr inbounds i32*, i32** %7, i64 %indvars.iv84
  br label %for.body25

for.cond.cleanup24:                               ; preds = %for.body25
  %putchar = tail call i32 @putchar(i32 10)
  %indvars.iv.next85 = add nuw nsw i64 %indvars.iv84, 1
  %exitcond86 = icmp eq i64 %indvars.iv.next85, %conv
  br i1 %exitcond86, label %delete.notnull, label %for.body25.lr.ph

for.body25:                                       ; preds = %for.body25, %for.body25.lr.ph
  %indvars.iv = phi i64 [ 0, %for.body25.lr.ph ], [ %indvars.iv.next, %for.body25 ]
  %13 = load i32*, i32** %arrayidx27, align 8, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %13, i64 %indvars.iv
  %14 = load i32, i32* %arrayidx29, align 4, !tbaa !6
  %call30 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i64 0, i64 0), i32 %14)
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %conv
  br i1 %exitcond, label %for.cond.cleanup24, label %for.body25

delete.notnull:                                   ; preds = %for.cond.cleanup24, %for.cond.cleanup
  tail call void @_ZdaPv(i8* nonnull %call5) #14
  tail call void @_ZdaPv(i8* nonnull %call3) #14
  ret i32 0
}

; Function Attrs: nofree nounwind
declare dso_local i32 @fprintf(%struct._IO_FILE* nocapture, i8* nocapture readonly, ...) local_unnamed_addr #3

; Function Attrs: noreturn nounwind
declare dso_local void @exit(i32) local_unnamed_addr #4

; Function Attrs: inlinehint nounwind readonly uwtable
define available_externally dso_local i32 @atoi(i8* nonnull %__nptr) local_unnamed_addr #5 {
entry:
  %call = tail call i64 @strtol(i8* nocapture nonnull %__nptr, i8** null, i32 10) #15
  %conv = trunc i64 %call to i32
  ret i32 %conv
}

; Function Attrs: nounwind readnone speculatable
declare { i64, i1 } @llvm.umul.with.overflow.i64(i64, i64) #6

; Function Attrs: nobuiltin nofree
declare dso_local noalias nonnull i8* @_Znam(i64) local_unnamed_addr #7

declare dso_local i8* @__hpvm_launch(i8*, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_wait(i8*) local_unnamed_addr #1

; Function Attrs: nofree nounwind
declare dso_local i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #3

; Function Attrs: nobuiltin nounwind
declare dso_local void @_ZdaPv(i8*) local_unnamed_addr #8

; Function Attrs: nofree nounwind
declare dso_local i64 @strtol(i8* readonly, i8** nocapture, i32) local_unnamed_addr #3

; Function Attrs: nofree nounwind
declare i32 @putchar(i32) local_unnamed_addr #9

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { norecurse uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nofree nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable }
attributes #7 = { nobuiltin nofree "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nofree nounwind }
attributes #10 = { cold }
attributes #11 = { noreturn nounwind }
attributes #12 = { nounwind readonly }
attributes #13 = { builtin }
attributes #14 = { builtin nounwind }
attributes #15 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
