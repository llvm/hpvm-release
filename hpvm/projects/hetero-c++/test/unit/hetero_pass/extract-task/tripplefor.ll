; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/tripplefor.cc'
source_filename = "happy/tripplefor.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"


; CHECK-LABEL: void @_Z7forloopPimmmm(
; CHECK-NOT: %call = tail call i8* @__hpvm_parallel_section_begin()
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 3, void (i32*, i64, i64, i64, i64)* @_Z7forloopPimmmm.for.cond1.preheader_reorder, i64 %M, i64 %N, i64 %K)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 2, i32 2, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 3, i32 3, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 4, i32 4, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 2, i32 2, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 3, i32 3, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 4, i32 4, i32 0)


; CHECK-LABEL: void @_Z7forloopPimmmm.for.cond1.preheader_reorder(
; CHECK: call void @__hpvm__hint(
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_y_cloned = call i64 @__hpvm__getNodeInstanceID_y(i8* %__hpvm__getNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_z_cloned = call i64 @__hpvm__getNodeInstanceID_z(i8* %__hpvm__getNode_cloned)
; CHECK: %0 = add nuw nsw i64 %__hpvm__getNodeInstanceID_y_cloned, %__hpvm__getNodeInstanceID_x_cloned
; CHECK: %mul11_cloned = mul i64 %__hpvm__getNodeInstanceID_y_cloned, %__hpvm__getNodeInstanceID_x_cloned
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: call void (i32, ...) @__hpvm__return(i32 5, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i64 %K)



; Function Attrs: uwtable
define dso_local void @_Z7forloopPimmmm(i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i64 %K) local_unnamed_addr #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %cmp47 = icmp eq i64 %M, 0
  br i1 %cmp47, label %for.cond.cleanup, label %for.cond1.preheader.lr.ph

for.cond1.preheader.lr.ph:                        ; preds = %entry
  %cmp344 = icmp eq i64 %N, 0
  %cmp842 = icmp eq i64 %K, 0
  br label %for.cond1.preheader

for.cond1.preheader:                              ; preds = %for.cond.cleanup4, %for.cond1.preheader.lr.ph
  %indvars.iv55 = phi i64 [ 0, %for.cond1.preheader.lr.ph ], [ %indvars.iv.next56, %for.cond.cleanup4 ]
  br i1 %cmp344, label %for.cond.cleanup4, label %for.cond6.preheader

for.cond.cleanup:                                 ; preds = %for.cond.cleanup4, %entry
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void

for.cond6.preheader:                              ; preds = %for.cond1.preheader, %for.cond.cleanup9
  %indvars.iv51 = phi i64 [ %indvars.iv.next52, %for.cond.cleanup9 ], [ 0, %for.cond1.preheader ]
  br i1 %cmp842, label %for.cond.cleanup9, label %for.body10.lr.ph

for.body10.lr.ph:                                 ; preds = %for.cond6.preheader
  %0 = add nuw nsw i64 %indvars.iv51, %indvars.iv55
  %mul11 = mul i64 %indvars.iv51, %indvars.iv55
  %1 = trunc i64 %0 to i32
  br label %for.body10

for.cond.cleanup4:                                ; preds = %for.cond.cleanup9, %for.cond1.preheader
  %indvars.iv.next56 = add nuw i64 %indvars.iv55, 1
  %exitcond57 = icmp eq i64 %indvars.iv.next56, %M
  br i1 %exitcond57, label %for.cond.cleanup, label %for.cond1.preheader

for.cond.cleanup9:                                ; preds = %for.body10, %for.cond6.preheader
  %indvars.iv.next52 = add nuw i64 %indvars.iv51, 1
  %exitcond54 = icmp eq i64 %indvars.iv.next52, %N
  br i1 %exitcond54, label %for.cond.cleanup4, label %for.cond6.preheader

for.body10:                                       ; preds = %for.body10, %for.body10.lr.ph
  %indvars.iv = phi i64 [ 0, %for.body10.lr.ph ], [ %indvars.iv.next, %for.body10 ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 3, i32 4, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i64 %K, i32 4, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i64 %K)
  %2 = trunc i64 %indvars.iv to i32
  %mul = mul nsw i32 %2, %1
  %rem = urem i32 %mul, 10
  %3 = add i64 %indvars.iv, %mul11
  %idxprom = zext i32 %rem to i64
  %arrayidx = getelementptr inbounds i32, i32* %ptr, i64 %idxprom
  %4 = trunc i64 %3 to i32
  store i32 %4, i32* %arrayidx, align 4, !tbaa !2
  %indvars.iv.next = add nuw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %K
  br i1 %exitcond, label %for.cond.cleanup9, label %for.body10
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
