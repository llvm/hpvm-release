; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/error.cc'
source_filename = "happy/error.cc"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [6 x i8] c"node1\00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"node2\00", align 1

; CHECK-LABEL: void @_Z6launchiPimS_mS_m(
; CHECK: %0 = sext i32 %n to i64
; CHECK: %1 = add i32 %n, 1
; CHECK: %wide.trip.count = zext i32 %1 to i64
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64, i32)* @node1, i64 %wide.trip.count)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 1, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 2, i32 1, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 3, i32 2, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 4, i32 3, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 0, i32 4, i32 0)
; CHECK: %cmp968 = icmp sgt i32 %n, 1
; CHECK: %2 = add i32 %n, -1
; CHECK: %wide.trip.count72 = zext i32 %2 to i64
; CHECK: %__hpvm__createNodeND4 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64, i32, i32*, i64)* @node2, i64 %wide.trip.count72)

; Function Attrs: mustprogress uwtable
define dso_local void @_Z6launchiPimS_mS_m(i32 %n, i32* %A, i64 %sizeA, i32* %B, i64 %sizeB, i32* %C, i64 %sizeC) local_unnamed_addr #0 {
entry:
  %call = call i8* (i32, ...) @__hetero_launch_begin(i32 4, i32 %n, i32* %A, i64 %sizeA, i32* %B, i64 %sizeB, i32* %C, i64 %sizeC, i32 3, i32* %A, i64 %sizeA, i32* %B, i64 %sizeB, i32* %C, i64 %sizeC)
  %call1 = call i8* @__hetero_section_begin()
  %cmp.not65 = icmp slt i32 %n, 0
  br i1 %cmp.not65, label %for.cond8.preheader, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  %0 = sext i32 %n to i64
  %1 = add i32 %n, 1
  %wide.trip.count = zext i32 %1 to i64
  br label %for.body

for.cond8.preheader:                              ; preds = %for.inc, %entry
  %cmp968 = icmp sgt i32 %n, 1
  br i1 %cmp968, label %for.body11.preheader, label %for.cond.cleanup10

for.body11.preheader:                             ; preds = %for.cond8.preheader
  %2 = add i32 %n, -1
  %wide.trip.count72 = zext i32 %2 to i64
  br label %for.body11

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %indvars.iv = phi i64 [ 0, %for.body.preheader ], [ %indvars.iv.next, %for.inc ]
  call void (i32, ...) @__hetero_parallel_loop(i32 1, i32 3, i32* %A, i64 %sizeA, i32* %B, i64 %sizeB, i32 %n, i32 1, i32* %B, i64 %sizeB, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i64 0, i64 0))
  %cmp2 = icmp slt i64 %indvars.iv, %0
  br i1 %cmp2, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %arrayidx = getelementptr inbounds i32, i32* %A, i64 %indvars.iv
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !3
  %mul = shl nsw i32 %3, 1
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %mul.sink = phi i32 [ %mul, %if.then ], [ -1, %for.body ]
  %arrayidx4 = getelementptr inbounds i32, i32* %B, i64 %indvars.iv
  store i32 %mul.sink, i32* %arrayidx4, align 4, !tbaa !3
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond.not = icmp eq i64 %indvars.iv.next, %wide.trip.count
  br i1 %exitcond.not, label %for.cond8.preheader, label %for.body, !llvm.loop !7

for.cond.cleanup10:                               ; preds = %for.body11, %for.cond8.preheader
  call void @__hetero_section_end(i8* %call1)
  call void @__hetero_launch_end(i8* %call)
  ret void

for.body11:                                       ; preds = %for.body11.preheader, %for.body11
  %indvars.iv70 = phi i64 [ 0, %for.body11.preheader ], [ %indvars.iv.next71, %for.body11 ]
  call void (i32, ...) @__hetero_parallel_loop(i32 1, i32 3, i32* %A, i64 %sizeA, i32* %B, i64 %sizeB, i32 %n, i32 1, i32* %C, i64 %sizeC, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i64 0, i64 0))
  %arrayidx13 = getelementptr inbounds i32, i32* %A, i64 %indvars.iv70
  %4 = load i32, i32* %arrayidx13, align 4, !tbaa !3
  %indvars.iv.next71 = add nuw nsw i64 %indvars.iv70, 1
  %arrayidx16 = getelementptr inbounds i32, i32* %A, i64 %indvars.iv.next71
  %5 = load i32, i32* %arrayidx16, align 4, !tbaa !3
  %mul17 = mul nsw i32 %5, %4
  %arrayidx19 = getelementptr inbounds i32, i32* %C, i64 %indvars.iv70
  store i32 %mul17, i32* %arrayidx19, align 4, !tbaa !3
  %exitcond73.not = icmp eq i64 %indvars.iv.next71, %wide.trip.count72
  br i1 %exitcond73.not, label %for.cond.cleanup10, label %for.body11, !llvm.loop !10
}

declare dso_local i8* @__hetero_launch_begin(i32, ...) local_unnamed_addr #1

declare dso_local i8* @__hetero_section_begin() local_unnamed_addr #1

declare dso_local void @__hetero_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hetero_section_end(i8*) local_unnamed_addr #1

declare dso_local void @__hetero_launch_end(i8*) local_unnamed_addr #1

; Function Attrs: mustprogress norecurse uwtable
define dso_local i32 @main() local_unnamed_addr #2 {
entry:
  %call = call noalias nonnull dereferenceable(40) i8* @_Znam(i64 40) #5
  %0 = bitcast i8* %call to i32*
  %call2 = call noalias nonnull dereferenceable(44) i8* @_Znam(i64 44) #5
  %call4 = call noalias nonnull dereferenceable(36) i8* @_Znam(i64 36) #5
  br label %for.body

for.cond.cleanup:                                 ; preds = %for.body
  %1 = bitcast i8* %call4 to i32*
  %2 = bitcast i8* %call2 to i32*
  call void @_Z6launchiPimS_mS_m(i32 10, i32* nonnull %0, i64 40, i32* nonnull %2, i64 44, i32* nonnull %1, i64 36)
  call void @_ZdaPv(i8* %call) #6
  call void @_ZdaPv(i8* %call2) #6
  call void @_ZdaPv(i8* %call4) #6
  ret i32 0

for.body:                                         ; preds = %entry, %for.body
  %indvars.iv = phi i64 [ 0, %entry ], [ %indvars.iv.next, %for.body ]
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %arrayidx = getelementptr inbounds i32, i32* %0, i64 %indvars.iv
  %3 = trunc i64 %indvars.iv.next to i32
  store i32 %3, i32* %arrayidx, align 4, !tbaa !3
  %exitcond.not = icmp eq i64 %indvars.iv.next, 10
  br i1 %exitcond.not, label %for.cond.cleanup, label %for.body, !llvm.loop !11
}

; Function Attrs: nobuiltin allocsize(0)
declare dso_local nonnull i8* @_Znam(i64) local_unnamed_addr #3

; Function Attrs: nobuiltin nounwind
declare dso_local void @_ZdaPv(i8*) local_unnamed_addr #4

attributes #0 = { mustprogress uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { mustprogress norecurse uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nobuiltin allocsize(0) "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #4 = { nobuiltin nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { builtin allocsize(0) }
attributes #6 = { builtin nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git 22a1ae7b4fa64f5be0fd660cf36d70cadd101c5e)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C++ TBAA"}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.mustprogress"}
!9 = !{!"llvm.loop.unroll.disable"}
!10 = distinct !{!10, !8, !9}
!11 = distinct !{!11, !8, !9}
