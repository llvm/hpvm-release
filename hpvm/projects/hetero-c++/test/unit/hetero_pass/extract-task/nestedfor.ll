; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/nestedfor.cc'
source_filename = "happy/nestedfor.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; CHECK-LABEL: void @_Z7forloopPimmm(
; CHECK: call void @__hpvm__hint(
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i64, i64)* @_Z7forloopPimmm.for.body_reorder, i64 %M)

; CHECK-LABEL: void @_Z7forloopPimmm.for.body6_reorder(
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getParentNode_cloned = call i8* @__hpvm__getParentNode(i8* %__hpvm__getNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_x1_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getParentNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)

; CHECK-LABEL: void @_Z7forloopPimmm.for.body_reorder(
; CHECK: %__hpvm__createNodeND_cloned = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i64, i64)* @_Z7forloopPimmm.for.body6_reorder, i64 %N)


; Function Attrs: uwtable
define dso_local void @_Z7forloopPimmm(i32* %ptr, i64 %ptrSz, i64 %M, i64 %N) local_unnamed_addr #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %cmp38 = icmp eq i64 %M, 0
  br i1 %cmp38, label %for.cond.cleanup, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %cmp436 = icmp eq i64 %N, 0
  br label %for.body

for.cond.cleanup:                                 ; preds = %for.cond.cleanup5, %entry
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void

for.body:                                         ; preds = %for.cond.cleanup5, %for.body.lr.ph
  %indvars.iv42 = phi i64 [ 0, %for.body.lr.ph ], [ %indvars.iv.next43, %for.cond.cleanup5 ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N)
  %call1 = tail call i8* @__hpvm_parallel_section_begin()
  br i1 %cmp436, label %for.cond.cleanup5, label %for.body6.preheader

for.body6.preheader:                              ; preds = %for.body
  %0 = trunc i64 %indvars.iv42 to i32
  br label %for.body6

for.cond.cleanup5:                                ; preds = %for.body6, %for.body
  tail call void @__hpvm_parallel_section_end(i8* %call1)
  %indvars.iv.next43 = add nuw i64 %indvars.iv42, 1
  %exitcond44 = icmp eq i64 %indvars.iv.next43, %M
  br i1 %exitcond44, label %for.cond.cleanup, label %for.body

for.body6:                                        ; preds = %for.body6, %for.body6.preheader
  %indvars.iv = phi i64 [ 0, %for.body6.preheader ], [ %indvars.iv.next, %for.body6 ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N)
  %1 = add nuw nsw i64 %indvars.iv, %indvars.iv42
  %2 = trunc i64 %1 to i32
  %rem = urem i32 %2, 10
  %3 = trunc i64 %indvars.iv to i32
  %mul = mul nsw i32 %3, %0
  %idxprom = zext i32 %rem to i64
  %arrayidx = getelementptr inbounds i32, i32* %ptr, i64 %idxprom
  store i32 %mul, i32* %arrayidx, align 4, !tbaa !2
  %indvars.iv.next = add nuw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %N
  br i1 %exitcond, label %for.cond.cleanup5, label %for.body6
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
