; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/multiple-out-dep.cc'
source_filename = "happy/multiple-out-dep.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; CHECK-LABEL: void @_Z2fnPimS_m(
; CHECK-NOT:  %call = tail call i8* @__hpvm_parallel_section_begin()
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 2, i32* %ptr, i32* %ptr2, i32 2, i32* %ptr, i32* %ptr2)
; CHECK: %__hpvm__createNodeND9 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i32*, i64)* @_Z2fnPimS_m.extracted_reorder, i64 1)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND9, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND9, i32 1, i32 1, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND9, i32 2, i32 2, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND9, i32 3, i32 3, i32 0)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z2fnPimS_m.extracted.1_reorder, i64 1)
; CHECK: %__hpvm__edge6 = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND9, i8* %__hpvm__createNodeND, i32 1, i32 3, i32 1, i32 0)
; CHECK: %__hpvm__edge = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND9, i8* %__hpvm__createNodeND, i32 1, i32 2, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: %__hpvm__createNodeND10 = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z2fnPimS_m.extracted.2_reorder, i64 1)
; CHECK: %__hpvm__edge8 = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND9, i8* %__hpvm__createNodeND10, i32 1, i32 1, i32 1, i32 0)
; CHECK: %__hpvm__edge7 = call i8* @__hpvm__edge(i8* %__hpvm__createNodeND9, i8* %__hpvm__createNodeND10, i32 1, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND10, i32 0, i32 2, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND10, i32 1, i32 3, i32 0)

; Function Attrs: uwtable
define dso_local void @_Z2fnPimS_m(i32* %ptr, i64 %ptrSz, i32* %ptr2, i64 %ptrSz2) local_unnamed_addr #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %call1 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 2, i32* %ptr, i64 %ptrSz, i32* %ptr2, i64 %ptrSz2, i32 2, i32* %ptr, i64 %ptrSz, i32* %ptr2, i64 %ptrSz2)
  %sub = shl i64 %ptrSz, 30
  %0 = add i64 %sub, -4294967296
  %idxprom = ashr i64 %0, 32
  %arrayidx = getelementptr inbounds i32, i32* %ptr, i64 %idxprom
  store i32 5, i32* %arrayidx, align 4, !tbaa !2
  %sub2 = shl i64 %ptrSz2, 30
  %1 = add i64 %sub2, -12884901888
  %idxprom5 = ashr i64 %1, 32
  %arrayidx6 = getelementptr inbounds i32, i32* %ptr2, i64 %idxprom5
  store i32 3, i32* %arrayidx6, align 4, !tbaa !2
  tail call void @__hpvm_task_end(i8* %call1)
  %call7 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 1, i32* %ptr2, i64 %ptrSz2, i32 1, i32* %ptr2, i64 %ptrSz2)
  store i32 3, i32* %arrayidx6, align 4, !tbaa !2
  tail call void @__hpvm_task_end(i8* %call7)
  %call10 = tail call i8* (i32, ...) @__hpvm_task_begin(i32 1, i32* %ptr, i64 %ptrSz, i32 1, i32* %ptr, i64 %ptrSz)
  store i32 2, i32* %arrayidx, align 4, !tbaa !2
  tail call void @__hpvm_task_end(i8* %call10)
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local i8* @__hpvm_task_begin(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_task_end(i8*) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
