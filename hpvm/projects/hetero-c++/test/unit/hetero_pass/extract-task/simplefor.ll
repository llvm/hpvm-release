; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/simplefor.cc'
source_filename = "happy/simplefor.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"



; CHECK-LABEL: @_Z7forloopPim(
; CHECK-NOT:  %call = tail call i8* @__hpvm_parallel_section_begin(
; CHECK-NOT: tail call void @__hpvm_parallel_section_end(i8* %call)
; CHECK-NOT: tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 1, i32* %ptr, i64 %ptrSz, i32 1, i32* %ptr, i64 %ptrSz)
; CHECK: call void @__hpvm__hint(
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z7forloopPim.for.body_reorder, i64 %div)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindIn(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 0, i32 0, i32 0)
; CHECK: call void @__hpvm__bindOut(i8* %__hpvm__createNodeND, i32 1, i32 1, i32 0)


; CHECK-LABEL: @_Z7forloopPim.for.body_reorder(
; CHECK: call void @__hpvm__hint(
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: call void (i32, ...) @__hpvm__return(i32 2, i32* %ptr, i64 %ptrSz)

; Function Attrs: uwtable
define dso_local void @_Z7forloopPim(i32* %ptr, i64 %ptrSz) local_unnamed_addr #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %div = lshr i64 %ptrSz, 2
  %cmp15 = icmp eq i64 %div, 0
  br i1 %cmp15, label %for.cond.cleanup, label %for.body

for.cond.cleanup:                                 ; preds = %for.body, %entry
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void

for.body:                                         ; preds = %entry, %for.body
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.body ], [ 0, %entry ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 1, i32* %ptr, i64 %ptrSz, i32 1, i32* %ptr, i64 %ptrSz)
  %arrayidx2 = getelementptr inbounds i32, i32* %ptr, i64 %indvars.iv
  %0 = trunc i64 %indvars.iv to i32
  %1 = trunc i64 %indvars.iv to i32
  %2 = and i32 %1, 1
  %storemerge = add i32 %2, %0
  store i32 %storemerge, i32* %arrayidx2, align 4, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp eq i64 %indvars.iv.next, %div
  br i1 %exitcond, label %for.cond.cleanup, label %for.body
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
