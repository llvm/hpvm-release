; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/testfor.cc'
source_filename = "happy/testfor.cc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; CHECK-LABEL: void @_Z7forloopPimmm(
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i64, i64)* @_Z7forloopPimmm.for.body_reorder, i64 %div1)

; CHECK-LABEL: void @_Z7forloopPimmm.for.body9_reorder(
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getParentNode_cloned = call i8* @__hpvm__getParentNode(i8* %__hpvm__getNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_x1_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getParentNode_cloned)
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: call void (i32, ...) @__hpvm__return(i32 4, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N)



; CHECK-LABEL: void @_Z7forloopPimmm.for.body_reorder(
; CHECK: call void @__hpvm__hint(i32 1)
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: call void (i32, ...) @__hpvm__attributes(i32 1, i32* %ptr, i32 1, i32* %ptr)
; CHECK: %__hpvm__createNodeND_cloned = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64, i64, i64)* @_Z7forloopPimmm.for.body9_reorder, i64 %umax_clone_clone_cloned)

; Function Attrs: uwtable
define dso_local void @_Z7forloopPimmm(i32* %ptr, i64 %ptrSz, i64 %M, i64 %N) local_unnamed_addr #0 {
entry:
  %call = tail call i8* @__hpvm_parallel_section_begin()
  %div1 = lshr i64 %ptrSz, 4
  %cmp41 = icmp eq i64 %div1, 0
  br i1 %cmp41, label %for.cond.cleanup, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %div6 = udiv i64 %ptrSz, 12
  %0 = icmp ugt i64 %ptrSz, 11
  br label %for.body

for.cond.cleanup:                                 ; preds = %for.cond.cleanup8, %entry
  tail call void @__hpvm_parallel_section_end(i8* %call)
  ret void

for.body:                                         ; preds = %for.cond.cleanup8, %for.body.lr.ph
  %indvars.iv45 = phi i64 [ 0, %for.body.lr.ph ], [ %indvars.iv.next46, %for.cond.cleanup8 ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N)
  %call2 = tail call i8* @__hpvm_parallel_section_begin()
  br i1 %0, label %for.body9.preheader, label %for.cond.cleanup8

for.body9.preheader:                              ; preds = %for.body
  %1 = trunc i64 %indvars.iv45 to i32
  br label %for.body9

for.cond.cleanup8:                                ; preds = %for.body9, %for.body
  tail call void @__hpvm_parallel_section_end(i8* %call2)
  %indvars.iv.next46 = add nuw nsw i64 %indvars.iv45, 1
  %exitcond = icmp eq i64 %indvars.iv.next46, %div1
  br i1 %exitcond, label %for.cond.cleanup, label %for.body

for.body9:                                        ; preds = %for.body9.preheader, %for.body9
  %indvars.iv = phi i64 [ 0, %for.body9.preheader ], [ %indvars.iv.next, %for.body9 ]
  tail call void (i32, ...) @__hpvm_parallel_loop(i32 1, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N, i32 3, i32* %ptr, i64 %ptrSz, i64 %M, i64 %N)
  %2 = add nuw nsw i64 %indvars.iv, %indvars.iv45
  %3 = trunc i64 %2 to i32
  %rem = urem i32 %3, 10
  %4 = trunc i64 %indvars.iv to i32
  %mul = mul nsw i32 %4, %1
  %idxprom = zext i32 %rem to i64
  %arrayidx = getelementptr inbounds i32, i32* %ptr, i64 %idxprom
  store i32 %mul, i32* %arrayidx, align 4, !tbaa !2
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %cmp7 = icmp ugt i64 %div6, %indvars.iv.next
  br i1 %cmp7, label %for.body9, label %for.cond.cleanup8
}

declare dso_local i8* @__hpvm_parallel_section_begin() local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_loop(i32, ...) local_unnamed_addr #1

declare dso_local void @__hpvm_parallel_section_end(i8*) local_unnamed_addr #1

attributes #0 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 9.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git cc53005888a6505bfc04198e3fed9b43cf0778cb)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
