; RUN: hcc  -declsfile ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -extract-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/simple_non_zero.cc'
source_filename = "happy/simple_non_zero.cc"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"


; CHECK-LABEL: @_Z7forloopPim(
; CHECK: %umax = call i64 @llvm.umax.i64(i64 %div, i64 1)
; CHECK:  %__hpvm__createNodeND = call i8* (i32, ...) @__hpvm__createNodeND(i32 1, void (i32*, i64)* @_Z7forloopPim.for.body_reorder, i64 %umax)

; CHECK-LABEL: void @_Z7forloopPim.for.body_reorder(
; CHECK: %__hpvm__getNode_cloned = call i8* @__hpvm__getNode()
; CHECK: %__hpvm__getNodeInstanceID_x_cloned = call i64 @__hpvm__getNodeInstanceID_x(i8* %__hpvm__getNode_cloned)
; CHECK: call void (i64, ...) @__hpvm__isNonZeroLoop(i64 %__hpvm__getNodeInstanceID_x_cloned, i32 4)



; Function Attrs: mustprogress uwtable
define dso_local void @_Z7forloopPim(i32* %ptr, i64 %ptrSz) local_unnamed_addr #0 {
entry:
  %call = call i8* @__hetero_section_begin()
  %cmp17.not = icmp ult i64 %ptrSz, 4
  br i1 %cmp17.not, label %for.cond.cleanup, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  %div = lshr i64 %ptrSz, 2
  %umax = call i64 @llvm.umax.i64(i64 %div, i64 1)
  br label %for.body

for.cond.cleanup:                                 ; preds = %for.body, %entry
  call void @__hetero_section_end(i8* %call)
  ret void

for.body:                                         ; preds = %for.body.preheader, %for.body
  %m.018 = phi i64 [ %inc, %for.body ], [ 0, %for.body.preheader ]
  call void (i32, ...) @__hetero_parallel_loop(i32 1, i32 1, i32* %ptr, i64 %ptrSz, i32 1, i32* %ptr, i64 %ptrSz)
  call void (i64, ...) @__hetero_isNonZeroLoop(i64 %m.018, i32 4) #4
  %conv1 = trunc i64 %m.018 to i32
  %0 = trunc i64 %m.018 to i32
  %1 = and i32 %0, 1
  %conv.sink = add i32 %1, %conv1
  %arrayidx = getelementptr inbounds i32, i32* %ptr, i64 %m.018
  store i32 %conv.sink, i32* %arrayidx, align 4, !tbaa !3
  %inc = add nuw nsw i64 %m.018, 1
  %exitcond.not = icmp eq i64 %inc, %umax
  br i1 %exitcond.not, label %for.cond.cleanup, label %for.body, !llvm.loop !7
}

declare dso_local i8* @__hetero_section_begin() local_unnamed_addr #1

declare dso_local void @__hetero_parallel_loop(i32, ...) local_unnamed_addr #1

; Function Attrs: nounwind
declare dso_local void @__hetero_isNonZeroLoop(i64, ...) local_unnamed_addr #2

declare dso_local void @__hetero_section_end(i8*) local_unnamed_addr #1

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare i64 @llvm.umax.i64(i64, i64) #3

attributes #0 = { mustprogress uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { nounwind "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nofree nosync nounwind readnone speculatable willreturn }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git 9889edf485aaf4ff269553ef1f23dc330a4dff43)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C++ TBAA"}
!7 = distinct !{!7, !8, !9}
!8 = !{!"llvm.loop.mustprogress"}
!9 = !{!"llvm.loop.unroll.disable"}
