; RUN: hcc  -declsfile  ../tools/hpvm/projects/hetero-c++/lib/HPVMCFunctionDeclarations/HPVMCFunctionDeclarations.bc -control-only -S  < %s | FileCheck %s  
; ModuleID = 'happy/main_inline.cc'
source_filename = "happy/main_inline.cc"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"


; CHECK-LABEL: @main(
; CHECK: %root_func = bitcast void (i8*, i64)* @main.extracted_reorder to i8*
; CHECK: call void @llvm_hpvm_track_mem(i8* %6, i64 %2)
; CHECK: %__hpvm__launch = call i8* (i32, ...) @__hpvm__launch(
; CHECK: call void @__hpvm__wait(i8* %__hpvm__launch)
; CHECK: call void @llvm_hpvm_request_mem(
; CHECK: call void @llvm_hpvm_untrack_mem(
; CHECK: call void @__hpvm__cleanup()

; Function Attrs: mustprogress norecurse uwtable
define dso_local i32 @main() local_unnamed_addr #0 {
entry:
  %call = call noalias align 16 dereferenceable_or_null(40) i8* @malloc(i64 40) #3
  %0 = bitcast i8* %call to i32*
  %call1 = call i8* (i32, ...) @__hetero_launch_begin(i32 1, i8* %call, i64 40, i32 1, i8* %call, i64 40)
  %call2 = call i8* @__hetero_section_begin()
  %call3 = call i8* (i32, ...) @__hetero_task_begin(i32 1, i8* %call, i64 40, i32 1, i8* %call, i64 40)
  %arrayidx = getelementptr inbounds i32, i32* %0, i64 9
  store i32 -1, i32* %arrayidx, align 4, !tbaa !3
  call void @__hetero_task_end(i8* %call3)
  call void @__hetero_section_end(i8* %call2)
  call void @__hetero_launch_end(i8* %call1)
  ret i32 0
}

; Function Attrs: inaccessiblememonly mustprogress nofree nounwind willreturn
declare dso_local noalias noundef align 16 i8* @malloc(i64 noundef) local_unnamed_addr #1

declare dso_local i8* @__hetero_launch_begin(i32, ...) local_unnamed_addr #2

declare dso_local i8* @__hetero_section_begin() local_unnamed_addr #2

declare dso_local i8* @__hetero_task_begin(i32, ...) local_unnamed_addr #2

declare dso_local void @__hetero_task_end(i8*) local_unnamed_addr #2

declare dso_local void @__hetero_section_end(i8*) local_unnamed_addr #2

declare dso_local void @__hetero_launch_end(i8*) local_unnamed_addr #2

attributes #0 = { mustprogress norecurse uwtable "frame-pointer"="none" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { inaccessiblememonly mustprogress nofree nounwind willreturn "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { "frame-pointer"="none" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{!"clang version 13.0.0 (https://gitlab.engr.illinois.edu/llvm/hpvm.git a559c3fc5579f24202614fba580b005b8906b3df)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C++ TBAA"}
