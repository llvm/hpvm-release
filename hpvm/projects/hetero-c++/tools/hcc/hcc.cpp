//===- hpvm-extract-loop.cpp -- Extract parallel loop into own function ---===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM
// Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This program extracts a parallel loop or task identified by a hpvm marker
// into its own function.  This is intended for testing how well the function
// extraction utility in LLVM identifies incoming arguments and return values,
// and to design how those can be packaged into DF edge for HPVM.
//
//===---------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Analysis/CallPrinter.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Transforms/Scalar/IndVarSimplify.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Utils.h"
#include "llvm/Transforms/Utils/SimplifyCFGOptions.h"


#include "HPVMCGenPass.h"
#include "HCCVerifierPass.h"

#include <string>
#include <iostream>
#include <memory>
using namespace llvm;

namespace llvm {
  // FIXME: Hack to create LoopSimplifyPass: This function is not public
  extern Pass* createLoopSimplifyPass();
}

// Option category used by certain command-line options, below
cl::OptionCategory ExtractCat("hcc Options");

// InputFilename - The filename to read from. Defaults to stdin.
static cl::opt<std::string> InputFilename(cl::Positional,
                                          cl::desc("<input bitcode file>"),
                                          cl::init("-"),
                                          cl::value_desc("filename"));

// -o OutputFilename - The filename to write to. Defaults to stdout.
static cl::opt<std::string> OutputFilename("o",
					   cl::desc("Specify output filename"),
					   cl::value_desc("filename"),
					   cl::init("-"), cl::cat(ExtractCat));

// -S - Write output as LLVM assembly (.ll), not the default bitcode (.bc)
static cl::opt<bool> OutputAssembly("S",
				    cl::desc("Write output as LLVM assembly"),
				    cl::Hidden, cl::cat(ExtractCat));

// -f - Force binary output on terminals
static cl::opt<bool> Force("f", cl::desc("Enable binary output on terminals"),
			   cl::cat(ExtractCat));

// -dot-dfg - Write out the final dataflow graph as a dot file
static cl::opt<bool> VisualizeDataFlowGraph("dot-dfg",
				cl::desc("Write final dataflow-graph graph to dot file"),
					cl::cat(ExtractCat));

// -extract-only - Only run the extract-task pipeline in the HPVMCGenPass
static cl::opt<bool> RunExtractOnly("extract-only",
				cl::desc("Only run the extract-task pipeline in the HPVMCGenPass"),
                cl::init(false),
				cl::cat(ExtractCat));

// -control-only - Only run the control-task pipeline in the HPVMCGenPass
static cl::opt<bool> RunControlOnly("control-only",
				cl::desc("Only run the control-task pipeline in the HPVMCGenPass"),
                cl::init(false),
				cl::cat(ExtractCat));


// -santise-funcs - Replace the '.' in function names with '_'
static cl::opt<bool> RunSanitise("sanitise-funcs",
				cl::desc("Remove the . character from function names and replace them with _"),
                cl::init(false),
				cl::cat(ExtractCat));

// -use0D - For single instance Tasks, use 0D createNode calls instead of 1D 
static cl::opt<bool> Use0D("use0D",
				cl::desc("Create a 0D node in createNode instead of a 1D node with replication factor 1"),
                cl::init(false),
				cl::cat(ExtractCat));


// -remove-dummy - Remove nodes which simply propogate inputs and outputs to child dfg 
static cl::opt<bool> RemoveDummy("remove-dummy",
				cl::desc("Remove redundant nodes in DFG and directly connect edges"),
                cl::init(false),
				cl::cat(ExtractCat));

// -no-return-sizes - Only propogate pointers through edges 
static cl::opt<bool> NoRetSizes("no-return-sizes",
				cl::desc("Only propogate pointers through edges"),
                cl::init(false),
				cl::cat(ExtractCat));

void fatalError(const StringRef& progName, const StringRef& msg) {
  std::cerr << progName.str() << ": FATAL: " << msg.str() << std::endl;
  exit(1);
}

int writeModuleToOutput(legacy::PassManager& Passes, Module& M)
{
  return 0;
}


int main(int argc, char** argv)
{
  InitLLVM X(argc, argv);

  LLVMContext Context;
  cl::ParseCommandLineOptions(argc, argv, "parallel loop extractor\n");

  // Read in specified LLVM bitcode module
  SMDiagnostic Err;
  std::unique_ptr<Module> M = parseIRFile(InputFilename, Err, Context);
  if (!M.get())
    fatalError(argv[0], "Failed to open module " + InputFilename);

  HPVMCGenPassOpts ExtractOpts;


  // Which sub-passes to run in
  // HPVMCGenPass:
  //  0: All Subpasses
  //  1: HPVMExtractTask only
  //  2: HPVMCGenLocator only
  int HPVMCGenMode = 0;
  ExtractOpts.Mode = FULL;
  ExtractOpts.SanatiseFunctionNames = RunSanitise;
  ExtractOpts.Use0DNode = Use0D;
  ExtractOpts.RemoveDummyNodes = RemoveDummy ;
  ExtractOpts.NoReturnSizes = NoRetSizes;
  ExtractOpts.PrintDot = VisualizeDataFlowGraph;

  
  if(RunControlOnly && RunExtractOnly)
      fatalError(argv[0],"Can not enable control-only and extract-only simultaneously.");
  else if (RunControlOnly){
      HPVMCGenMode = 2;
      ExtractOpts.Mode = LOCATE_ONLY;
  } else if (RunExtractOnly){
      HPVMCGenMode = 1;
      ExtractOpts.Mode = EXTRACT_ONLY;
  }

  // Find the marker function in the input module
  // findAndExtractTasks(*M);
	legacy::PassManager Passes;


	Passes.add(createIndVarSimplifyPass());
	Passes.add(createLowerSwitchPass());
	Passes.add(createLoopSimplifyPass());
    Passes.add(new HCCVerifierPass());
	Passes.add(new HPVMCGenPass(/* Subpass configurations */ ExtractOpts));

    auto ftor = std::function<bool(const Function &)>(
            [] (const Function &F){
                return true;
    });


    SimplifyCFGOptions SCFGOpt; // Use default setings

    Passes.add(createCFGSimplificationPass(SCFGOpt,
               ftor));




  
  // Write out the module to specified output file
  // writeModuleToOutput(Passes, *M);

	// Open output file for writing module out
  std::error_code EC;
  ToolOutputFile Out(OutputFilename, EC, sys::fs::OF_None);
  if (EC) {
    errs() << EC.message() << '\n';
    return 1;
  }
  
  // Run passes for final output.
  // Use bitcode writer or assembly writer pass to write out the module
  if (OutputAssembly)
    Passes.add(createPrintModulePass(Out.os(), "",
				     true /*PreserveAssemblyUseListOrder*/));
  else if (Force || !CheckBitcodeOutputToConsole(Out.os()))
    Passes.add(createBitcodeWriterPass(Out.os(),
				       true /*PreserveBitcodeUseListOrder*/));

	// Run the full pass sequence for code generation and writing output
	Passes.run(*M);
  Out.keep();

  // Success!
  return 0;
}

