//===- extract-task.cpp -- Extract a parallel task into node function ----===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM
// Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===---------------------------------------------------------------------===//
//
// This file extracts a parallel loop or task identified by a hpvm marker
// into its own node function, repeating that for all marked tasks.
//
//===---------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/ADT/PostOrderIterator.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "HPVMCGenContext.h"
#include "HPVMCGen.h"
#include "HPVMCGenFunctions.h"
#include "DFGUtils.h"
#include "llvm/IR/Constants.h"



#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;


const string LaunchBeginMarker = "__hetero_launch_begin";
const string LaunchEndMarker = "__hetero_launch_end";

const string HeteroLaunchName = "__hetero_launch";
const string HeteroWaitName = "__hetero_wait";



// Legacy names
const string LegacyLaunchBeginMarker = "__hpvm_launch_begin";
const string LegacyLaunchEndMarker = "__hpvm_launch_end";
const string LegacyLaunchName = "__hpvm_launch";
const string LegacyHeteroWaitName = "__hpvm_wait";


template <typename T>
struct LaunchRegion {

    LaunchRegion(T *Begin, T *End) : Begin(Begin), End(End) {}

    LaunchRegion(const LaunchRegion<T> &Orig) : Begin(Orig.Begin), End(Orig.End) {}

    bool operator<(const LaunchRegion<T> &val) const {
        return Begin < val.Begin;
    }

    T *Begin;
    T *End;
};


struct HPVMLaunchExtractor {
    public:
        HPVMLaunchExtractor(Module& M) : M(M)
    {
        LLVM_DEBUG(errs()<<"===== HPVM Launch Extractor =====\n");

        LaunchBegin = M.getFunction(LaunchBeginMarker);
        LaunchEnd = M.getFunction(LaunchEndMarker);


        LegacyLaunchBegin = M.getFunction(LegacyLaunchBeginMarker);
        LegacyLaunchEnd = M.getFunction(LegacyLaunchEndMarker);

        if(!LaunchBegin && !LegacyLaunchBegin){
            LLVM_DEBUG(errs()<<"No launch marker in module, skipping pass\n");
            return;
        }

        assert((LaunchEnd || LegacyLaunchEnd) && "LaunchBegin with no LaunchEnd in module!\n");

        LLVM_DEBUG(errs()<<"getLaunchMarkers\n");
        auto MarkersInstructions =  getLaunchMarkers();

        LLVM_DEBUG(errs()<<"copyOuterDefsInLaunch\n");
        copyOuterDefsInLaunch(MarkersInstructions);

        LLVM_DEBUG(errs()<<"splitLaunchBoundary\n");
        auto MarkersBlocks =  splitLaunchBoundary(MarkersInstructions); 

        LLVM_DEBUG(errs()<<"extractLaunch\n");
        extractLaunch(MarkersBlocks);


        removeMarkers();




    }


        vector<LaunchRegion<Instruction>> getLaunchMarkers();

        vector<LaunchRegion<BasicBlock>> splitLaunchBoundary( 
                vector<LaunchRegion<Instruction>> LaunchMarkers);

        void extractLaunch(vector<LaunchRegion<BasicBlock>> LaunchBlockPairs);

        void createHPVMLaunch(Function* LaunchF, CallInst* LC);

        void copyOuterDefsInLaunch(vector<LaunchRegion<Instruction>> LaunchMarkers);
        
        void removeMarkers();





    private:
        Module &M;
        Function* LaunchBegin;
        Function* LaunchEnd;

        Function* LegacyLaunchBegin;
        Function* LegacyLaunchEnd;

        SetVector<BasicBlock*> buildExtractionSet(LaunchRegion<BasicBlock> Region);

};


vector<LaunchRegion<Instruction>> HPVMLaunchExtractor::getLaunchMarkers(){
    vector<LaunchRegion<Instruction>> markers;

    if (LaunchBegin) {
      for(User* LaunchBeginUser : LaunchBegin->users()){
          CallInst* LC = dyn_cast<CallInst>(LaunchBeginUser);

          if(!LC) continue;

          assert(LC->hasOneUse() && "Launch Begin marker call does not have exactly one user");

          CallInst* LE = dyn_cast<CallInst>(LC->user_back());

          assert(LE && (LE->getCalledFunction() == LaunchEnd) 
                  && "Launch Begin Must be used by Launch End marker");


          // Insert into markers
          markers.emplace_back(LC, LE);
      }
    }

    // Legacy support for older marker names
    if (LegacyLaunchBegin) {
      for(User* LaunchBeginUser : LegacyLaunchBegin->users()){
          CallInst* LC = dyn_cast<CallInst>(LaunchBeginUser);

          if(!LC) continue;

          assert(LC->hasOneUse() && "Launch Begin marker call does not have exactly one user");

          CallInst* LE = dyn_cast<CallInst>(LC->user_back());

          assert(LE && (LE->getCalledFunction() == LegacyLaunchEnd) 
                  && "Launch Begin Must be used by Launch End marker");


          // Insert into markers
          markers.emplace_back(LC, LE);

      }
    }
    return markers;
}


vector<LaunchRegion<BasicBlock>> HPVMLaunchExtractor::splitLaunchBoundary(
        vector<LaunchRegion<Instruction>> LaunchMarkers){
    vector<LaunchRegion<BasicBlock>> RegionBlocks;

    LLVM_DEBUG(errs() <<"Number of Launch Markers: "<<LaunchMarkers.size() << "\n");

    for(LaunchRegion<Instruction> LR: LaunchMarkers){
        Instruction* Begin = LR.Begin;
        Instruction* End = LR.End;

        Function* Parent = Begin->getParent()->getParent();

        LLVM_DEBUG(errs() << "Start Instruction: "<< *Begin<<"\n");
        LLVM_DEBUG(errs() << "End Instruction: "<< *End<<"\n");

        // Split the basic block at the Begin Marker
        BasicBlock* BeginBB = Begin->getParent()->splitBasicBlock(Begin);

        
        End->getParent()->splitBasicBlock(End);
        BasicBlock* EndBB = End->getParent();

        End->getParent()->splitBasicBlock(End);

        End->eraseFromParent();

        LLVM_DEBUG(errs() << "After splitting:\n"<<*Parent<<"\n");


        RegionBlocks.emplace_back(BeginBB, EndBB);
    }

    return RegionBlocks;
}


void HPVMLaunchExtractor::extractLaunch(
        vector<LaunchRegion<BasicBlock>> LaunchBlockPairs){

  auto getRegionBBs = [&] (LaunchRegion<BasicBlock> R) -> vector<BasicBlock*> {
    vector<BasicBlock*> BBs;

    Function* CallerF = R.Begin->getParent();

    DominatorTree DT(*CallerF);
    PostDominatorTree PDT(*CallerF);

    // BBs.push_back(R.Begin);
    LLVM_DEBUG(CallerF->dump());
    // Bruteforce to find all BBs dominated by begin and post-dominated by end
    for (auto &BB : *R.Begin->getParent()) {
      LLVM_DEBUG(dbgs() << "\n" << BB.getName() << " dominated by begin: " << DT.dominates(R.Begin, & BB));
      LLVM_DEBUG(dbgs() << "\n" << BB.getName() << " post-dominated by end: " << PDT.dominates(R.End, & BB));
      
      // if (DT.dominates(R.Begin, &BB) && PDT.dominates(R.End, &BB))

      if((DT.dominates(R.Begin, &BB) && PDT.dominates(R.End, &BB))||
              (DT.dominates(R.Begin, &BB) && BBPrecedesEnd(&BB, R.Begin, R.End))){
        BBs.push_back(&BB);
      }
    }

    return BBs;
  };


  for(LaunchRegion<BasicBlock> LR: LaunchBlockPairs){
      vector<BasicBlock*> regionBBs = getRegionBBs(LR);

      DominatorTree DT(*LR.Begin->getParent());

      CodeExtractor CE(regionBBs, &DT,
              false, nullptr, nullptr, nullptr, false,
              true);

      if(!CE.isEligible()){
          HPVMFatalError("Code Extractor unable to extract Launch Region");
      }

      CodeExtractorAnalysisCache  CEAC(*LR.Begin->getParent());
      Function* ExtractedLaunchF = CE.extractCodeRegion(CEAC);

      assert(ExtractedLaunchF && "Launch Function must exist");

      CallInst* CallToLaunchF = cast<CallInst>(ExtractedLaunchF->user_back());

      LLVM_DEBUG(errs()<<" Call to extracted Launch Function: "<<*CallToLaunchF<<"\n");

      ValueToValueMapTy ArgMap;
      std::set<Argument*> EmptySet;

      Function* ReorderedF = CreateClone(ExtractedLaunchF, EmptySet, ArgMap);

      CallInst* ReorderedCall = CloneCallInst(CallToLaunchF, ReorderedF, 
              EmptySet, ArgMap);

      
      LLVM_DEBUG(dbgs()<<"Trying to erase "<<*CallToLaunchF<<"\n");
      // CallToLaunchF->replaceAllUsesWith(UndefValue::get(RI->getType()));

      CallToLaunchF->eraseFromParent();

      LLVM_DEBUG(dbgs()<<"Trying to erase "<<*ExtractedLaunchF<<"\n");

      ExtractedLaunchF->eraseFromParent();


      createHPVMLaunch(ReorderedF, ReorderedCall);

    }

}

void DFSHelper(LaunchRegion<BasicBlock> &Region, SetVector<BasicBlock*> &BBs, BasicBlock *curr) {
    if (!curr) { return; }

    if (curr == Region.End) {                       // if block is the ending basic block
        BBs.insert(curr);
        return;
    }

    Instruction *CurrBBTerminator = curr->getTerminator();
    if (isa<UnreachableInst>(CurrBBTerminator)) {    // if basic block ends in an unreachable
        BBs.insert(curr);
        return;
    }

    BranchInst *CurrBBBranchInst = dyn_cast<BranchInst>(CurrBBTerminator);
    if (!CurrBBBranchInst) { return; }              // if basic block branches

    BBs.insert(curr);
    for (size_t I = 0; I < CurrBBBranchInst->getNumSuccessors(); ++I) {
        BasicBlock *Successor = CurrBBBranchInst->getSuccessor(I);
        DFSHelper(Region, BBs, Successor);
    } 

    return;
}

SetVector<BasicBlock*> HPVMLaunchExtractor::buildExtractionSet(LaunchRegion<BasicBlock> Region) {
    SetVector<BasicBlock*> BBs;
    DFSHelper(Region, BBs, Region.Begin);
    return BBs;
}

void HPVMLaunchExtractor::createHPVMLaunch(Function* LaunchF, CallInst* LC){

    Function* HeteroLaunch = M.getFunction(HeteroLaunchName);
    Function* HeteroWait = M.getFunction(HeteroWaitName);
    Type* i8PTy = Type::getInt8PtrTy(M.getContext());
    Type* voidTy = Type::getVoidTy(M.getContext());
    Type* i32Ty = Type::getInt32Ty(M.getContext());



    // Create HeteroLaunch

    if(!HeteroLaunch){
        // First argument is function pointer
        vector<Type*> Args = {i8PTy};
        FunctionType* HeteroLaunchTy = FunctionType::get(i8PTy,Args, /* isVarArgs */ true);

        HeteroLaunch = Function::Create(HeteroLaunchTy, Function::ExternalLinkage, HeteroLaunchName, M);
    }

    // Create HeteroLaunch
    if(!HeteroWait){
        vector<Type*> Args = {i8PTy};
        FunctionType* HeteroWaitTy = FunctionType::get(voidTy,Args, /* isVarArgs */ false);

        HeteroWait = Function::Create(HeteroWaitTy, Function::ExternalLinkage, HeteroWaitName, M);
    }

    LLVM_DEBUG(errs()<<"HeteroLaunch:"<<*HeteroLaunch<<"HeteroWait:"<<*HeteroWait<<"\n");

    CallInst* LaunchMarker = getMarkerCall(LaunchF);

    assert(LaunchMarker && isLaunchBeginMarker(LaunchMarker) && "Valid Launch begin marker not found");

    vector<Value*> LaunchArgs;
    BitCastInst* BI = new BitCastInst(LaunchF, i8PTy,"root_func",LC);
    LaunchArgs.push_back(BI);

    auto ArgIter =  LaunchMarker->arg_begin();



    ConstantInt* NI = dyn_cast<ConstantInt>(*ArgIter);
    ArgIter++;

    if(!NI){
        HPVMFatalError("Launch Begin Marker must have constant number of input buffers");
    }


    int numNI = NI->getSExtValue();
    LaunchArgs.push_back(ConstantInt::get(Type::getInt32Ty(M.getContext()), numNI));

    while(numNI){
        Argument* ArgI = dyn_cast<Argument>(*ArgIter); 

        if(!ArgI) HPVMFatalError("Marker argument must be caller function argument");

        LaunchArgs.push_back(
                LC->getArgOperand(ArgI->getArgNo())
                );

        ArgIter++;

        // Pointer Value and Its size are one pair
        if(ArgI->getType()->isPointerTy()){
            Argument* SizeArg = dyn_cast<Argument>(*ArgIter); 

            if(!SizeArg) HPVMFatalError("Marker argument must be caller function argument");

            if(SizeArg->getType()->isPointerTy()) HPVMFatalError("Argument following pointer must be scalar");

            LaunchArgs.push_back(
                LC->getArgOperand(SizeArg->getArgNo())
                );

            ArgIter++;

        }
        numNI--;
    }

    ConstantInt* NO = dyn_cast<ConstantInt>(*ArgIter);
    ArgIter++;

    if(!NO){
        HPVMFatalError("Launch Begin Marker must have constant number of output buffers");
    }

    int numNO = NO->getSExtValue();
    LaunchArgs.push_back(ConstantInt::get(Type::getInt32Ty(M.getContext()), numNO));


    while(numNO){
        Argument* ArgO = dyn_cast<Argument>(*ArgIter); 

        if(!ArgO) HPVMFatalError("Marker argument must be caller function argument");

        LaunchArgs.push_back(
                LC->getArgOperand(ArgO->getArgNo())
                );

        ArgIter++;

        // Pointer Value and Its size are one pair
        if(ArgO->getType()->isPointerTy()){
            Argument* SizeArg = dyn_cast<Argument>(*ArgIter); 

            if(!SizeArg) HPVMFatalError("Marker argument must be caller function argument");

            if(SizeArg->getType()->isPointerTy()) HPVMFatalError("Argument following pointer must be scalar");

            LaunchArgs.push_back(
                LC->getArgOperand(SizeArg->getArgNo())
                );

            ArgIter++;

        }
        numNO--;
    }

    LLVM_DEBUG(errs()<<"Printing Launch Args:\n");

    for(Value* V : LaunchArgs){
        LLVM_DEBUG(errs()<<"\t"<<*V<<"\n");
    }

    ArrayRef<Value*> NewLaunchArgs(LaunchArgs);
    CallInst* NewLaunch = CallInst::Create(HeteroLaunch, NewLaunchArgs,"new_launch",LC);

    // Insert HeteroWait marker
    //

    vector<Value*> WaitArgs = {NewLaunch};


    CallInst* NewWait = CallInst::Create(HeteroWait, ArrayRef<Value*>(WaitArgs),"",LC);

    LC->eraseFromParent();
}

void HPVMLaunchExtractor::copyOuterDefsInLaunch(vector<LaunchRegion<Instruction>> LaunchMarkers){

    for(auto& LR: LaunchMarkers){

        Function* LaunchParent = LR.Begin->getParent()->getParent();
        ReversePostOrderTraversal<Function *> RPOT(LaunchParent);
        auto RPOBB = RPOT.begin();
        auto RPOBBEnd = RPOT.end();

        Instruction* StopPoint = nullptr;

        auto getNextInst = [&] (Instruction* I) -> Instruction* {
            // In the case where we skip to the
            // ending bounds on the current task
            while(I->getParent() != *RPOBB && RPOBB != RPOBBEnd){
                StopPoint = nullptr;
                RPOBB++;
            }

            BasicBlock* NextB = nullptr;
            // Must update RPOBB to next basic block
            if(StopPoint == nullptr){
                // special for starting case
                if(RPOBB != RPOT.begin()){
                    // Move to next basic block in
                    // traversal
                    RPOBB++;
                } 
                NextB = *RPOBB;
                StopPoint = NextB->getFirstNonPHI();
            } else {
                StopPoint = StopPoint->getNextNonDebugInstruction();
            }  

            while(!StopPoint || isa<BranchInst>(StopPoint) || isa<UnreachableInst>(StopPoint)){
                RPOBB++;
                NextB = *RPOBB;
                StopPoint = NextB->getFirstNonPHI();
            }

            return StopPoint;
        };
        
        Instruction* NextI = LR.Begin;
        StopPoint = NextI;
        
        ValueToValueMapTy VMap;
        ValueToValueMapTy RevVMap;

        LLVM_DEBUG(errs()<<"Launch Region Before copying: "<<*LaunchParent<<"\n");
        while((NextI = getNextInst(NextI)) != LR.End){
            LLVM_DEBUG(errs()<<"[ NextI ]: "<<*NextI<<"\n");
            copyDefInRegion(NextI, LR.Begin, LR.End, VMap, RevVMap);
        }
        LLVM_DEBUG(errs()<<"Launch Region After copying: "<<*LaunchParent<<"\n");


    }
}

void HPVMLaunchExtractor::removeMarkers(){
    std::vector<Instruction*> ToErase;

    for(auto* User: LaunchBegin->users()){
        Instruction* I = dyn_cast<Instruction>(User);

        if(I)
            ToErase.push_back(I);
    }


    for(auto EI : ToErase){
        EI->eraseFromParent();
    }
}


void findAndExtractLaunch(Module &M)
{
    ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

    // Set up the code generator context
    HPVMCGenContext cgenContext(M);


    HPVMLaunchExtractor Program(M);

    // Print final module for testing and debugging
    LLVM_DEBUG(M.dump());
}
