//===- HPVMCGenPass.cpp - HPVM-C generation module passes -------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// LLVMCGenPass.cpp -- Implementation of passes for HPVM-C generation
//
//===----------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Scalar.h"
#include "HPVMCGenPass.h"
#include "HPVMCGen.h"
#include "HPVMCGenContext.h"
#include "HPVMExtractTask.h"
#include "HPVMExtractLaunch.h"
#include "HPVMCGenLocator.h"
#include "HPVMDotGraph.h"
#include "NestedDotGraph.h"
#include "DummyNodeElimination.h"
#include "ArgAttrPropogate.h"

using namespace llvm;

namespace llvm {
  extern char &LoopSimplifyID;
}

HPVMCGenPass::HPVMCGenPass() :
  ModulePass(ID), cgenContext(nullptr) //HPVMCGenMode(0)
{
	initializeHPVMCGenPassPass(*PassRegistry::getPassRegistry());
}

HPVMCGenPass::HPVMCGenPass(HPVMCGenPassOpts PassOpts) :
  ModulePass(ID), cgenContext(nullptr), Opts(PassOpts) //HPVMCGenMode(Mode)
{
	initializeHPVMCGenPassPass(*PassRegistry::getPassRegistry());
}

bool HPVMCGenPass::runOnModule(Module& M) {
	// Initialize the CGenContext object to be used for the entire code-gen
	cgenContext = new HPVMCGenContext(M);


    if (Opts.Mode == FULL || Opts.Mode == LOCATE_ONLY) { 
	    // Insert init, cleanup, launch, and wait
        // Find and replace launch marker functions into their own functions
        HPVMCGenLocator locator(*cgenContext);
        locator.extractConstants();
        findAndExtractLaunch(M);
	    locator.insertStaticCalls();

    } 

    if (Opts.Mode == FULL || Opts.Mode == EXTRACT_ONLY){
  	    // Find and replace tasks and loops with nodes and edges
  	    findAndExtractTasks(M, Opts.SanatiseFunctionNames, Opts.Use0DNode, Opts.NoReturnSizes);
    }


    
    if(Opts.Mode == LOCATE_ONLY || Opts.Mode == EXTRACT_ONLY){
        return true;
    }


    // Propogate parameter attributes
    // down the DFG.
    HPVMArgProp(M);

    RootRemoveBindOut(M);



    if(Opts.PrintDot){
        DFG2NestedDot(M,"_dfg");
    }

    // Disabled for now, as it needs's 
    // more thorough testing
    if(Opts.RemoveDummyNodes){
        removeDummyNodes(M);
        if(Opts.PrintDot)
            DFG2NestedDot(M, "_post_dummy_elim");
    }

    

	return true; // Success!
}

void HPVMCGenPass::getAnalysisUsage(AnalysisUsage &AU) const {
	// AU.addRequired<LoopInfoWrapperPass>();
	AU.addRequiredID(llvm::LoopSimplifyID);
	// AU.addRequired<ScalarEvolutionWrapperPass>();
	// AU.addRequired<TargetLibraryInfoWrapperPass>();
}

INITIALIZE_PASS(HPVMCGenPass, "hpvmcgen", "HPVM-C code generation pass",
                false, false)

char HPVMCGenPass::ID = 0;

