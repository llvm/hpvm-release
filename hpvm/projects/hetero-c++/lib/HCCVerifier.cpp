//===- HCCVerifier.cpp --- HeteroC++ Verification Analysis ------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Idenfitier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HCCVerifier.cpp -- Analyzes a HeteroC++ program to ensure it is valid and
/// reports and errors
//
//===----------------------------------------------------------------------===//

#include "llvm/Analysis/ValueTracking.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Scalar.h"

#include "HCCVerifier.h"

#include <string>
#include <cassert>
#include <iostream>
#include <queue>

#define DEBUG_TYPE "hcc-verifier"

using namespace llvm;

using InstrVectorTy = SmallVector<const Instruction *>;
static InstrVectorTy getInterveningInstructions(
    const Loop &OuterLoop, const Loop &InnerLoop, ScalarEvolution &SE);
enum LoopNestEnum { PerfectLoopNest, ImperfectLoopNest,
                    InvalidLoopStructure, OuterLoopLowerBoundUnknown };

static bool loopPerfectize(Loop& OuterLoop, Loop& InnerLoop, ScalarEvolution&);

static void reportErrorOnInst(std::string str, Instruction* inst) {
  // If the debug info is non-existance just print the instruction name if it
  // has one, and if not print the entire instruction. This makes sure we
  // somehow show where the error is coming from, and tries to do so in the
  // best way
  if (inst->getDebugLoc()) {
    inst->getDebugLoc().print(errs());
  } else if (inst->hasName()) {
    errs() << "In " << inst->getFunction()->getName() << " instruction "
           << inst->getName();
  } else {
    errs() << "In " << inst->getFunction()->getName() << " instruction "
           << *inst;
  }
  errs() << " : " << str << "\n";
}

HCCVerifier::HCCVerifier(Module& Mod) : M(Mod), errors(false) {
  lookupMarkers();
  initMarkerInstructions();
  findMarkers();
}

void HCCVerifier::lookupMarkers() {
  std::map<std::string, MarkerType> markers;
  markers["__hpvm_parallel_section_begin"] = BEGIN_SECTION;
  markers["__hetero_section_begin"]        = BEGIN_SECTION;
  markers["__hpvm_parallel_section_end"]   =   END_SECTION;
  markers["__hetero_section_end"]          =   END_SECTION;

  markers["__hpvm_task_begin"]             = BEGIN_TASK;
  markers["__hetero_task_begin"]           = BEGIN_TASK;
  markers["__hpvm_task_end"]               =   END_TASK;
  markers["__hetero_task_end"]             =   END_TASK;

  markers["__hpvm_launch_begin"]           = BEGIN_LAUNCH;
  markers["__hetero_launch_begin"]         = BEGIN_LAUNCH;
  markers["__hpvm_launch_end"]             =   END_LAUNCH;
  markers["__hetero_launch_end"]           =   END_LAUNCH;

  markers["__hpvm_launch"]                 = LAUNCH;
  markers["__hetero_launch"]               = LAUNCH;
  markers["__hpvm_wait"]                   =   WAIT;
  markers["__hetero_wait"]                 =   WAIT;

  markers["__hpvm_parallel_loop"]          = LOOP;
  markers["__hetero_parallel_loop"]        = LOOP;

  for (auto const& funcInfo : markers) {
    std::string funcName = funcInfo.first;
    MarkerType funcType = funcInfo.second;

    Function* funcPtr = M.getFunction(funcName);
    if (funcPtr != nullptr) {
      markerFunctions[funcPtr] = funcType;
    }
  }
}

void HCCVerifier::initMarkerInstructions() {
  markerInstructions[BEGIN_SECTION] = std::set<CallInst*>();
  markerInstructions[END_SECTION]   = std::set<CallInst*>();
  markerInstructions[BEGIN_TASK]    = std::set<CallInst*>();
  markerInstructions[END_TASK]      = std::set<CallInst*>();
  markerInstructions[BEGIN_LAUNCH]  = std::set<CallInst*>();
  markerInstructions[END_LAUNCH]    = std::set<CallInst*>();
  markerInstructions[LAUNCH]        = std::set<CallInst*>();
  markerInstructions[WAIT]          = std::set<CallInst*>();
  markerInstructions[LOOP]          = std::set<CallInst*>();
}

void HCCVerifier::findMarkers() {
  for (Function& F : M) {
    for (BasicBlock& BB : F) {
      for (Instruction& I : BB) {
        CallInst* CI = dyn_cast<CallInst>(&I);
        if (CI != nullptr) {
          auto it = markerFunctions.find(CI->getCalledFunction());
          if (it != markerFunctions.end()) {
            markerInstructions[it->second].insert(CI);
          }
        }
      }
    }
  }
}

void HCCVerifier::setupAnalysis(Function* func) {
  AnalysisCache.emplace(func,
        std::unique_ptr<AnalysisInfo>(new AnalysisInfo(func, DTCache)));
}

LoopInfo& HCCVerifier::getLoopInfo(Function* func) {
  auto it = AnalysisCache.find(func);
  if (it != AnalysisCache.end()) {
    return it->second.get()->LI;
  }

  setupAnalysis(func);
  return AnalysisCache[func].get()->LI;
}

ScalarEvolution& HCCVerifier::getScalarEvolution(Function* func) {
  auto it = AnalysisCache.find(func);
  if (it != AnalysisCache.end()) {
    return it->second.get()->SE;
  }

  setupAnalysis(func);
  return AnalysisCache[func].get()->SE;
}

bool HCCVerifier::verify() {
  verifyBeginMarkers(BEGIN_SECTION);
  verifyBeginMarkers(BEGIN_TASK);
  verifyBeginMarkers(BEGIN_LAUNCH);
  verifyLoopMarkers();

  // At this point markedRegions and markedLoops contain the regions and loops
  // that we identified as being valid. We now want to ensure that these nesting
  // is proper and there is no code outside of the appropriate regions
  verifyNesting();

  // Check args of all __hetero_launch()
  for (CallInst* ci : markerInstructions[LAUNCH]) {
    checkEdgeArguments(ci, 1);
  }

  // Report unmatched sections/tasks/launch end
  std::vector<MarkerType> endMarkers = {END_SECTION, END_TASK, END_LAUNCH};
  for (MarkerType m : endMarkers) {
    for (CallInst* unmatched : markerInstructions[m]) {
      errors = true;
      reportErrorOnInst("End marker does not match a begin marker",
        unmatched);
    }
  }

  return !errors;
}

void HCCVerifier::verifyBeginMarkers(MarkerType marker) {
  assert((marker == BEGIN_SECTION || marker == BEGIN_TASK
       || marker == BEGIN_LAUNCH)
       && "verifyBeginMarkers only works on markers 'begin' markers for paired markers");

  bool expectArgs = !(marker == BEGIN_SECTION);
  MarkerType endMarker = marker == BEGIN_SECTION ? END_SECTION
                       : marker == BEGIN_TASK    ? END_TASK
                                                 : END_LAUNCH;
  std::set<CallInst*>* endSet = &(markerInstructions[endMarker]);

  for (CallInst* ci : markerInstructions[marker]) {
    if (expectArgs) checkEdgeArguments(ci, 0);

    bool foundEnd = false;
    for (User* use : ci->users()) {
      CallInst* callUse = dyn_cast<CallInst>(use);
      auto it = endSet->find(callUse);
      if (!callUse || it == endSet->end()) {
        errors = true;
        Instruction* instUse = dyn_cast<Instruction>(use);
        if (instUse)
          reportErrorOnInst("Begin marker used by non-end marker", instUse);
        else
          reportErrorOnInst("Begin marker used by non-end marker", ci);
        continue;
      }

      Instruction* instUse = dyn_cast<Instruction>(use);
      assert(instUse && "CallInst is a subclass of Instruction");

      if (foundEnd) {
        errors = true;
        reportErrorOnInst("Begin marker has a previous end marker", instUse);
      } else if (!DTCache.postDominates(callUse, ci)) {
        // We don't have to check dominates, that is implied by the use and
        // valid SSA form
        errors = true;
        reportErrorOnInst("End marker is not reached on all paths from begin "
                          "to return", instUse);
      } else {
        // If the region isn't valid, don't include it for nesting analysis
        markedRegions[ci->getFunction()].insert(std::make_pair(ci, callUse));
      }

      foundEnd = true;
      endSet->erase(it);
    }

    if (!foundEnd) {
      errors = true;
      reportErrorOnInst("Begin marker has no end", ci);
    }
  }
}

void HCCVerifier::verifyLoopMarkers() {
  // Verify loop markers (arguments and appropriate number of containing loops)
  for (CallInst* ci : markerInstructions[LOOP]) {
    checkEdgeArguments(ci, 1);

    Value* arg = ci->getArgOperand(0);
    ConstantInt* loopArg = dyn_cast<ConstantInt>(arg);
    int64_t numLoops = loopArg ? loopArg->getSExtValue() : 0;

    if (numLoops <= 0 || numLoops > 3) {
      errors = true;
      reportErrorOnInst("First argument of loop marker must be a constant "
                        "integer 1, 2, or 3", ci);
      continue;
    }

    Function* func = ci->getFunction();
    LoopInfo& LI = getLoopInfo(func);
    ScalarEvolution& SE = getScalarEvolution(func);

    BasicBlock* bb = ci->getParent();
    Loop* loop = LI.getLoopFor(bb);

    unsigned loopDepth = loop ? loop->getLoopDepth() : 0;
    if (loopDepth < numLoops) {
      errors = true;
      reportErrorOnInst("Loop marker is not contained within "
                        + std::to_string(numLoops) + " loop"
                        + (numLoops == 1 ? "" : "s"), ci);
      continue;
    }

    // Check that the marker is the first (non-phi/debug) instruction in the
    // loop's header
    if (loop->getHeader()->getFirstNonPHIOrDbg() != ci) {
      errors = true;
      reportErrorOnInst("Loop marker must be the first instruction in the loop",
        ci);
      continue;
    }

    // Check that the loop is in canonical form
    if (!loop->isCanonical(SE)) {
      errors = true;
      reportErrorOnInst("Loop canonicalization failed", ci);
      continue;
    }

    // Check that the loop nest of this marker is perfect
    if (numLoops > 1) {
      // Traverse up to find the outermost loop
      Loop* innerLoop = loop;
      Loop* outerLoop = loop;
      for (int i = 1; i < numLoops; i++) {
        outerLoop = outerLoop->getParentLoop();
        assert(outerLoop && "Unexpectedly could not find enough loops");
      }
      if (!LoopNest::arePerfectlyNested(*outerLoop, *innerLoop, SE)) {
        LLVM_DEBUG(dbgs() << "Loop nest is not perfect. Checking if can "
                             "be perfectize\n");
        if (!loopPerfectize(*outerLoop, *innerLoop, SE)) {
          errors = true;
          reportErrorOnInst("Parallel loop nest is not perfect", ci);
          continue;
        }
      }

      // Track the valid loops we find for nesting analysis
      markedLoops[func].insert(std::make_tuple(outerLoop, innerLoop, ci));
    } else {
      markedLoops[func].insert(std::make_tuple(loop, loop, ci));
    }
  }
}

void HCCVerifier::checkEdgeArguments(CallInst* inst, int skip) {
  int i = 0, remainingArgs = 0, argumentListNum = 0;
  bool foundLast = false, expectPointerSize = false;
  for (Value* arg : inst->args()) {
    if (foundLast) {
      errors = true;
      reportErrorOnInst("Too many arguments (at argument " + std::to_string(i)
                        + ")", inst);
      return;
    }

    if (expectPointerSize) {
      Type* type = arg->getType();
      if (type->isIntegerTy()) {
        remainingArgs -= 1;
        expectPointerSize = false;
      } else {
        errors = true;
        reportErrorOnInst("Argument " + std::to_string(i) + " expected to be "
                          "the size of the preceding pointer but does not have"
                          " integer type", inst);
      }
    } else if (i >= skip && remainingArgs == 0) {
      // Since we are not looking for arguments, this should either be an
      // integer constant telling us how many input/output/private there are
      // or a string constant giving the extracted function a name
      // It can only be that if we've already seen input and output

      if (argumentListNum == 2 && dyn_cast<ConstantInt>(arg) == nullptr) {
        // We've already handled input and output, so all that's left is the
        // string literal for the function name
        ConstantExpr* constExp = dyn_cast<ConstantExpr>(arg);
        GlobalVariable* globalVar =
          constExp ? (constExp->getNumOperands() > 0
                     ? dyn_cast<GlobalVariable>(constExp->getOperand(0))
                     : nullptr)
                   : nullptr;
        ConstantDataArray* cda =
          globalVar ? (globalVar->hasInitializer()
                      ? dyn_cast<ConstantDataArray>(globalVar->getInitializer())
                      : nullptr)
                    : nullptr;
        if (cda && cda->isCString()) {
          // We track all of the names assigned to the nodes so that we can
          // check (later) that we don't create names that conflict with
          // existing names in the module or with other node names
          nodeNames.insert(cda->getAsCString());
          foundLast = true;
        } else {
          errors = true;
          reportErrorOnInst("Expected argument " + std::to_string(i)
                          + " to be a string literal", inst);
          return;
        }
      } else {
        argumentListNum++;
        ConstantInt* constVal = dyn_cast<ConstantInt>(arg);
        int64_t val;
        if (constVal && (val = constVal->getSExtValue()) >= 0) {
          remainingArgs = val;
        } else {
          errors = true;
          reportErrorOnInst("Argument " + std::to_string(i) + " should be "
                            + "the non-negative integer number of "
                            + (argumentListNum == 1 ? "input" : "output")
                            + " arguments", inst);
          return;
        }
      }
    } else if (i >= skip) {
      Type* ty = arg->getType();
      if (ty->isPointerTy()) {
        expectPointerSize = true;
      } else {
        remainingArgs--;
      }
    }

    i++;
  }

  if (argumentListNum < 2) {
    errors = true;
    reportErrorOnInst("Marker does not specify number of input and output "
                      "arguments", inst);
  }
  if (expectPointerSize) {
    errors = true;
    reportErrorOnInst("Last argument is missing its size", inst);
  }
  if ((!expectPointerSize && remainingArgs > 0)
      || (expectPointerSize && remainingArgs > 1)) {
    errors = true;
    reportErrorOnInst("Expected "
                      + std::to_string(remainingArgs - expectPointerSize)
                      + " more arguments", inst);
  }
}

// Ensures that the structure of a region is a Section which contains
// Tasks and Loops which contain Sections, and so on
// Then, if there are no errors, verifies that there is no extraneous code
// Returns false if an error is encountered
bool HCCVerifier::verifyNesting(RegionTree& region) {
  if (region.type != RegionTree::Section) {
    errors = true;
    reportErrorOnInst("Expected a parallel section at this level",
      region.marker);
    return false;
  } else {
    if (region.contains.size() == 0) {
      errors = true;
      reportErrorOnInst("Empty parallel sections not allowed", region.marker);
      return false;
    }

    // At some point when there are too many errors we just abandon checking
    // them because it's likely that parts of the program we're ignoring may
    // "resolve" these issues
    for (RegionTree& nested : region.contains) {
      if (nested.type != RegionTree::Task && nested.type != RegionTree::Loop) {
        errors = true;
        reportErrorOnInst("Parallel section can only contain tasks and loops",
          nested.marker);
        return false;
      } else {
        // And then verify that regions nested within this task/loop satisfy
        // these same properties
        bool noErrors = true;
        for (RegionTree& inner : nested.contains) {
          noErrors = verifyNesting(inner) && noErrors;
        }

        if (!noErrors) return false;
      }
    }

    return true;
  }
}

static Instruction* getFirstNonDebug(BasicBlock* block) {
  Instruction* inst = &*(block->begin());
  while (inst && inst->isDebugOrPseudoInst()) {
    inst = inst->getNextNonDebugInstruction();
  }
  return inst;
}

// Finds the first instruction in the basic block which is not a marker and no
// instruction after it in the block is either
Instruction* HCCVerifier::getFirstNonMarker(BasicBlock* block) {
  // Traverse from the end, stopping when we encounter a marker
  Instruction* current = block->getTerminator();
  Instruction* result = current;
  while (current) {
    if (CallInst* call = dyn_cast<CallInst>(current)) {
      if (markerFunctions.find(call->getCalledFunction())
          != markerFunctions.end()) break;
    }
    result = current;
    current = current->getPrevNonDebugInstruction();
  }
  return result;
}

bool HCCVerifier::isLoopCondition(RegionTree const& region, Instruction* instr) {
  Loop* curLoop = region.innerLoop;
  // Traverse through the loop-nest
  while (true) {
    if (instr == curLoop->getLatchCmpInst()) return true;
    if (curLoop->isGuarded()) {
      BranchInst* guardBranch = curLoop->getLoopGuardBranch();
      if (guardBranch->isConditional() && guardBranch->getCondition() == instr)
        return true;
    }

    if (curLoop == region.outerLoop) break;
    else curLoop = curLoop->getParentLoop();
  }
  return false;
}

bool HCCVerifier::allUsesLoopCondition(RegionTree const& region, Instruction* instr) {
  // Don't handle phi's since they can lead to infinite recursion of this
  // analysis (since they can depend on themselves or otherwise have circular
  // dependences
  if (dyn_cast<PHINode>(instr)) return false;

  bool hasUse = false; // Handles branches which don't have uses
  for (User* user : instr->users()) {
    hasUse = true;
    Instruction* useInst = dyn_cast<Instruction>(user);
    if (!useInst) return false;
    if (!isLoopCondition(region, useInst)
      && !allUsesLoopCondition(region, useInst))
      return false;
  }
  return hasUse;
}

// Verifies that a well-nested region does not use any values it is not allowed
// to access (so does not use arguments not included in the region) and
// identifies uses of values calculated outside of the region and verifies that
// they are calculated in a completely pure manner (no memory accesses or
// side-effects as these instructions will be rematerialized in each task that
// uses them leading to possibly multiple executions in a different program
// order
// Returns true if there are no errors and false otherwise
//
// outerInputs is the set of values that can be used by the containing region
// which is used for loops where the bounds may not be a value that can be
// used in the loop body but can be used by the containing region
bool HCCVerifier::verifyUses(RegionTree& region, std::set<Value*> inputs,
                             std::set<Value*>* outerInputs) {
  assert((region.type != RegionTree::Loop || outerInputs)
         && "verifyUses() requires outerInputs for loops");
  if (region.contains.empty()) {
    // A region with code in it, so actually check the operands used
    // Track the values from outside the region used in it (and track in the
    // bool whether the value is used by the loop condition for parallel loops)
    std::set<std::pair<Instruction*, bool>> outsideValues;

    Instruction* start = region.start->getNextNonDebugInstruction();
    Instruction* end = region.end;
    if (!start) {
      assert(region.type == RegionTree::Loop && "Only loop region should have "
             "a terminator as its starting instruction");
      start = getFirstNonMarker(region.outerLoop->getLoopPreheader());
    }

    std::queue<Instruction*> worklist; worklist.push(start);
    std::set<Instruction*> processed;

    bool badUse = false;

    while (!worklist.empty()) {
      Instruction* current = worklist.front(); worklist.pop();

      if (processed.find(current) != processed.end()) continue;
      if (current == end) continue;

      processed.insert(current);

      // Instructions that control loop execution iterations are allowed to
      // access values that are accessible to the outer region
      bool isLoopCond = region.type == RegionTree::Loop
                      && (isLoopCondition(region, current)
                          || allUsesLoopCondition(region, current));

      // Valid operands are in the list of inputs, instructions dominated by
      // the start of the region, a Constant, or a BasicBlock
      for (Value* op : current->operands()) {
        if (inputs.find(op) == inputs.end()
            && (!isLoopCond || outerInputs->find(op) == outerInputs->end())) {
          Instruction* inst = dyn_cast<Instruction>(op);
          Constant* const_  = dyn_cast<Constant>(op);
          BasicBlock* block = dyn_cast<BasicBlock>(op);
          MetadataAsValue* meta = dyn_cast<MetadataAsValue>(op);

          if (const_ || (inst && (inst == region.start
                                  || DTCache.dominates(region.start, inst)))
              || block || meta) {
            continue;
          } else if (inst && !inst->mayHaveSideEffects()
               && !inst->mayReadOrWriteMemory()) {
            // We track "legal" external references, i.e. ones generated
            // (likely) by LICM or CSE, these are only instructions that are
            // side-effect free and don't touch memory, because these are the
            // only ones that are safe to then rematerialize in the task
            // (possibly in multiple tasks or multiple iterations)
            outsideValues.insert(std::make_pair(inst, isLoopCond));
          } else {
            errors = true;
            reportErrorOnInst("Cannot use value defined outside of this "
                              "task/loop region", current);
            badUse = true;
          }
        }
      }

      // And find the instructions we can reach from here
      Instruction* next = current->getNextNonDebugInstruction();
      if (!next) {
        BranchInst* branch = dyn_cast<BranchInst>(current);
        if (branch) {
          for (BasicBlock* succ : branch->successors()) {
            Instruction* inst = getFirstNonDebug(succ);
            if (inst) worklist.push(inst);
          }
        }
      } else {
        worklist.push(next);
      }
    }

    // Convert to a vector so we can add elements to it as we traverse
    std::vector<std::pair<Instruction*, bool>>
        values(outsideValues.begin(), outsideValues.end());

    // We now process all of the operands that are values from instructions
    // outside of the region or that otherwise come from outside of the region.
    for (unsigned int i = 0; i < values.size(); i++) {
      Instruction* inst = values[i].first;
      bool isLoopCond = values[i].second;
      for (Value* op : inst->operands()) {
        if (inputs.find(op) == inputs.end()
            && (!isLoopCond || outerInputs->find(op) == outerInputs->end())) {
          Instruction* ins  = dyn_cast<Instruction>(op);
          Constant* const_  = dyn_cast<Constant>(op);
          BasicBlock* block = dyn_cast<BasicBlock>(op);
          MetadataAsValue* meta = dyn_cast<MetadataAsValue>(op);

          if (const_ || block || meta) { continue; }
          if (ins && !ins->mayHaveSideEffects()
              && !ins->mayReadOrWriteMemory()) {
            // Add to the list to verify, to make sure that all operands these
            // instructions depend on satisfy these properties
            std::pair<Instruction*, bool> pair = std::make_pair(ins, isLoopCond);
            auto f = outsideValues.find(pair);
            if (f == outsideValues.end()) {
              outsideValues.insert(pair);
              values.push_back(pair);
            }
          } else {
            errors = true;
            reportErrorOnInst("Cannot use value defined outside of this "
                              "task/loop region", inst);
            badUse = true;
          }
        }
      }
    }

    return !badUse;
  } else {
    bool nestedErrors = false;
    // Map from the first instruction in a region to the first instruction
    // after the region
    std::map<Instruction*, Instruction*> regionInstructions;

    // A region containing other regions, first process inner regions then
    // check for interveaning code
    for (RegionTree& nested : region.contains) {
      if (nested.type == RegionTree::Section) {
        // For parallel sections, the acceptable values are the same as the
        // region containing it
        nestedErrors = !verifyUses(nested, inputs, nullptr) || nestedErrors;
      } else {
        bool argErrors = false;
        std::set<Value*> nestedInputs; // Inputs into the nested region
        int skip = nested.type == RegionTree::Loop ? 1 : 0;
        int lists = 2, count = 0, i = 0;
        for (Value* val : nested.marker->args()) {
          if (skip > 0) { --skip; i++; continue; }
          if (count == 0 && lists == 0) break; // Ignore extras at end
          if (count == 0) {
            // Now we'll get a number of arguments we expect
            ConstantInt* c = dyn_cast<ConstantInt>(val);
            assert(c && "Constant expression required in marker function");
            count = c->getSExtValue();
            lists--;
          } else {
            if (inputs.find(val) == inputs.end()) {
              errors = true;
              reportErrorOnInst("Argument " + std::to_string(i) + " is not an "
                                "argument to the region containing it",
                                nested.marker);
              argErrors = true;
            }
            nestedInputs.insert(val);
            // Don't count pointers, just scalars and the sizes of pointers
            if (!val->getType()->isPointerTy()) count--;
          }
          i++;
        }

        if (!argErrors) {
          nestedErrors = verifyUses(nested, nestedInputs, &inputs)
                       || nestedErrors;
        } else {
          nestedErrors = true;
        }
      }

      if (nested.type == RegionTree::Loop) {
        regionInstructions[nested.start] = nested.end;
      } else {
        regionInstructions[nested.start] =
          nested.end->getNextNonDebugInstruction();
      }
    }

    // If all of the regions are valid, we make sure that there are no
    // side-effecting instructions outside of those regions (detecting code
    // outside of a marked region that shouldn't be there)
    if (!nestedErrors) {
      Instruction* current = region.start;
      Instruction* end = region.end;
      if (region.type == RegionTree::Loop) {
        current = getFirstNonDebug(region.innerLoop->getHeader());
        end = region.innerLoop->getExitingBlock()->getTerminator();
      }

      bool extraInstructions = false;
      std::set<BasicBlock*> reached;
      std::set<BasicBlock*> toSearch;
      while (current != end) {
        reached.insert(current->getParent());
        auto f = toSearch.find(current->getParent());
        if (f != toSearch.end()) {
          toSearch.erase(f);
        }

        if (current == region.marker) {
          current = current->getNextNonDebugInstruction();
        } else {
          auto f = regionInstructions.find(current);
          if (f != regionInstructions.end()) {
            current = f->second;
          } else {
            Instruction* next = current->getNextNonDebugInstruction();
            if (current->mayHaveSideEffects()
                || current->mayReadOrWriteMemory()) {
              errors = true;
              reportErrorOnInst("Instruction is outside of a marked region",
                                current);
              extraInstructions = true;
            }
            if (next) {
              current = next;
            } else {
              BranchInst* branch = dyn_cast<BranchInst>(current);
              if (!branch) {
                errors = true;
                reportErrorOnInst("Encountered non-branch terminator in "
                                  "marked region", current);
                extraInstructions = true;
              } else {
                // Add the basic blocks we can reach from this branch to the
                // list to explore (if we haven't already). We also don't add
                // back-edges since they should already have been processed
                // (and if not, only because we skipped over the body because
                // it was a parallel loop)
                for (BasicBlock* succ : branch->successors()) {
                  if (reached.find(succ) == reached.end()
                    && (succ != branch->getParent() 
                       && !DTCache.dominates(succ, branch->getParent()))) {
                    toSearch.insert(succ);
                  }
                }
                // And then pick some unprocessed basic block to process
                assert(!toSearch.empty()
                  && "Cannot explore further but have not located region end");
                BasicBlock* bb = *(toSearch.begin());
                toSearch.erase(toSearch.begin());
                current = getFirstNonDebug(bb);
              }
            }
          }
        }
      }
      if (extraInstructions) return false;
    } else {
      return false;
    }
  }
  return true;
}

// Verifies that each task and loop is completely contained within a parallel
// region and that parallel regions are top-level or within a task, loop, or
// launch
void HCCVerifier::verifyNesting() {
  // The regions and loops should form a tree by the contains property. To
  // determine this property, we first compute whether each region is contained
  // by each other (this is quadratic in the number of regions, which seems
  // expensive, but I'm not sure there's another way).

  // First, find all of the functions with markers
  std::set<Function*> functions;
  for (auto const& it : markedRegions) functions.insert(it.first);
  for (auto const& it : markedLoops)   functions.insert(it.first);

  for (Function* func : functions) {
    auto fR = markedRegions.find(func);
    std::vector<std::pair<CallInst*, CallInst*>> regions =
      fR != markedRegions.end()
      ? std::vector<std::pair<CallInst*, CallInst*>>(fR->second.begin(), fR->second.end())
      : std::vector<std::pair<CallInst*, CallInst*>>();
    int numRegions = regions.size();

    auto fL = markedLoops.find(func);
    std::vector<std::tuple<Loop*, Loop*, CallInst*>> loops =
      fL != markedLoops.end()
      ? std::vector<std::tuple<Loop*, Loop*, CallInst*>>(fL->second.begin(), fL->second.end())
      : std::vector<std::tuple<Loop*, Loop*, CallInst*>>();
    int numLoops = loops.size();

    std::vector<std::tuple<Instruction*, Instruction*, CallInst*>> allRegions;
    for (auto it : regions) {
      allRegions.push_back(std::make_tuple(it.first, it.second, it.first));
    }
    for (auto it : loops) {
      Loop* loop = std::get<0>(it);
      BasicBlock* preheader = loop->getLoopPreheader();

      // Beginning of this region is the first instruction in the pre-header
      // that isn' the marker for some other HeteroC++ region
      Instruction* begins = getFirstNonMarker(preheader);

      if (!loop->getExitingBlock() || !std::get<1>(it)->getExitingBlock()) {
        errors = true;
        reportErrorOnInst("Parallel loop is only allowed one exit, are there "
                          "breaks?", std::get<2>(it));
        continue;
      }

      Instruction* ends = loop->getExitingBlock()->getTerminator();
      allRegions.push_back(std::make_tuple(begins, ends, std::get<2>(it)));
    }

    int numTotal = numRegions + numLoops;
    std::list<RegionTree> nestedRegions;
    for (int i = 0; i < numTotal; i++) {
      // Construct a node to represent this region
      std::tuple<Instruction*, Instruction*, CallInst*> region = allRegions[i];
      RegionTree node;
      node.start = std::get<0>(region);
      node.end = std::get<1>(region);
      node.marker = std::get<2>(region);
      if (i < numRegions) {
        CallInst* call = regions[i].first;
        Function* func = call->getCalledFunction();
        if (markerFunctions[func] == BEGIN_SECTION) {
          node.type = RegionTree::Section;
        } else if (markerFunctions[func] == BEGIN_LAUNCH) {
          node.type = RegionTree::Launch;
        } else if (markerFunctions[func] == BEGIN_TASK) {
          node.type = RegionTree::Task;
        } else {
          std::cerr << "Error: unknown marker\n";
          LLVM_DEBUG(call->dump());
          abort();
        }
      } else {
        node.type = RegionTree::Loop;
        node.outerLoop = std::get<0>(loops[i - numRegions]);
        node.innerLoop = std::get<1>(loops[i - numRegions]);
      }

      bool inserted = false;
      auto other = nestedRegions.begin();
      std::list<RegionTree>* list = &nestedRegions;
      while (other != list->end() && !inserted) {
        bool domOtherStart = DTCache.dominates(other->start, node.start);
        bool domThisStart  = DTCache.dominates(node.start, other->start);
        bool postDomOtherEnd = DTCache.postDominates(other->end, node.end);
        bool postDomThisEnd  = DTCache.postDominates(node.end, other->end);
        // Check if the end of one region dominates the start of another
        // An explicit equality check is needed since the DominatorTree
        // doesn't handle it correctly (equality may arise due to loops)
        bool domOtherEnd = other->end == node.start
                          || DTCache.dominates(other->end, node.start);
        bool domThisEnd  = node.end == other->start
                          || DTCache.dominates(node.end, other->start);

        if (domOtherStart && postDomOtherEnd) {
          // other node contains this one, so we'll insert it into the other
          // node's contains list (or the list of one of those nodes)
          list = &(other->contains);
          other = list->begin();
        } else if (domThisStart && postDomThisEnd) {
          // this node contains the other, place the other node in this ones
          // contains list and continue to check other nodes, since there may
          // be other children of this current node in the list already
          node.contains.push_back(*other);
          other = list->erase(other);
        } else if ((domOtherStart && !domOtherEnd)
                || (domThisStart && !domThisEnd)) {
          // improper nesting (if one of the starts dominates the other, but
          // its end does not dominate the start of the other, they must be
          // nested, but we know it must be improper since the two cases above
          // are not true)
          errors = true;
          reportErrorOnInst("Marked region is not properly nested with others",
                            other->marker);
          LLVM_DEBUG(dbgs() << "Other marker: " << *(node.marker)
                            << "\nBeginning: " << *(node.start)
                            << "\nEnd: " << *(node.end)
                            << "Region marker: " << *(other->marker)
                            << "\nBeginning:  " << *(other->start)
                            << "\nEnd: " << *(other->end) << "\n";);
          // Since the nesting is wrong, just pretend we handled it and break
          inserted = true;
        } else {
          // no nesting between them, proceed to next region
          ++other;
        }
      }

      // If we never inserted the node, add it to the current list
      if (!inserted) {
        list->push_back(node);
      }
    }

    // If there is more than one nest of markers, the outermost must be launches
    bool expectOuterLaunch = nestedRegions.size() != 1;
    // Verify that the nesting order if correct (outermost is launch or
    // section, then each section only contains tasks and loops and each
    // task/loop contains only sections
    if (!errors) {
      // If there are errors up to this point, don't proceed with this analysis
      // as it's likely we're missing one or more regions and so any errors we
      // report here are likely to be caused by other issues
      for (RegionTree& region : nestedRegions) {
        if (region.type != RegionTree::Launch && expectOuterLaunch) {
          errors = true;
          reportErrorOnInst("Expected this region to be a launch, a wrapping "
                            "parallel section may be missing", region.marker);
        } else if (region.type == RegionTree::Launch) {
          // Compute the list of values passed through into the launch region
          std::set<Value*> passedThroughValues;
          CallInst* launch = dyn_cast<CallInst>(region.start);
          assert(launch && "Begin of launch region must be a call");

          int numExpect = 0, lists = 2;
          for (Value* v : launch->args()) {
            if (numExpect == 0 && lists == 0) break;
            if (numExpect == 0) {
              ConstantInt* c = dyn_cast<ConstantInt>(v);
              assert(c && "Expected constant expression in launch");
              numExpect = c->getSExtValue();
              lists--;
            } else {
              passedThroughValues.insert(v);
              // Don't decrement on pointers to get the size
              if (!v->getType()->isPointerTy())
                numExpect--;
            }
          }

          for (RegionTree& nested : region.contains) {
            if (verifyNesting(nested)) {
              verifyUses(nested, passedThroughValues, nullptr);
            }
          }
        } else {
          std::set<Value*> inputValues;
          for (Argument& v : func->args()) {
            inputValues.insert(&v);
          }

          if (verifyNesting(region)) {
            // Passing null here is fine since verifyNesting enforces the
            // region being a section
            verifyUses(region, inputValues, nullptr);
          }
        }
      }
    }
  }
}

// FIXME: Most of the following code was taken (almost verbatim) from the LLVM
// 14 source tree for LoopNestAnalysis, when HPVM is updated to a version with
// these features, the functions here should be removed. The loopPerfectize
// function is custom, but everything else should be taken from LLVM
static bool checkLoopsStructure(const Loop &OuterLoop, const Loop &InnerLoop,
                                ScalarEvolution &SE) {
  // The inner loop must be the only outer loop's child.
  if ((OuterLoop.getSubLoops().size() != 1) ||
      (InnerLoop.getParentLoop() != &OuterLoop))
    return false;

  // We expect loops in normal form which have a preheader, header, latch...
  if (!OuterLoop.isLoopSimplifyForm() || !InnerLoop.isLoopSimplifyForm())
    return false;

  const BasicBlock *OuterLoopHeader = OuterLoop.getHeader();
  const BasicBlock *OuterLoopLatch = OuterLoop.getLoopLatch();
  const BasicBlock *InnerLoopPreHeader = InnerLoop.getLoopPreheader();
  const BasicBlock *InnerLoopLatch = InnerLoop.getLoopLatch();
  const BasicBlock *InnerLoopExit = InnerLoop.getExitBlock();

  // We expect rotated loops. The inner loop should have a single exit block.
  if (OuterLoop.getExitingBlock() != OuterLoopLatch ||
      InnerLoop.getExitingBlock() != InnerLoopLatch || !InnerLoopExit)
    return false;

  // Returns whether the block `ExitBlock` contains at least one LCSSA Phi node.
  auto ContainsLCSSAPhi = [](const BasicBlock &ExitBlock) {
    return any_of(ExitBlock.phis(), [](const PHINode &PN) {
      return PN.getNumIncomingValues() == 1;
    });
  };

  // Returns whether the block `BB` qualifies for being an extra Phi block. The
  // extra Phi block is the additional block inserted after the exit block of an
  // "guarded" inner loop which contains "only" Phi nodes corresponding to the
  // LCSSA Phi nodes in the exit block.
  auto IsExtraPhiBlock = [&](const BasicBlock &BB) {
    return BB.getFirstNonPHI() == BB.getTerminator() &&
           all_of(BB.phis(), [&](const PHINode &PN) {
             return all_of(PN.blocks(), [&](const BasicBlock *IncomingBlock) {
               return IncomingBlock == InnerLoopExit ||
                      IncomingBlock == OuterLoopHeader;
             });
           });
  };

  const BasicBlock *ExtraPhiBlock = nullptr;
  // Ensure the only branch that may exist between the loops is the inner loop
  // guard.
  if (OuterLoopHeader != InnerLoopPreHeader) {
    const BasicBlock &SingleSucc =
        LoopNest::skipEmptyBlockUntil(OuterLoopHeader, InnerLoopPreHeader);

    // no conditional branch present
    if (&SingleSucc != InnerLoopPreHeader) {
      const BranchInst *BI = dyn_cast<BranchInst>(SingleSucc.getTerminator());

      if (!BI || BI != InnerLoop.getLoopGuardBranch())
        return false;

      bool InnerLoopExitContainsLCSSA = ContainsLCSSAPhi(*InnerLoopExit);

      // The successors of the inner loop guard should be the inner loop
      // preheader or the outer loop latch possibly through empty blocks.
      for (const BasicBlock *Succ : BI->successors()) {
        const BasicBlock *PotentialInnerPreHeader = Succ;
        const BasicBlock *PotentialOuterLatch = Succ;

        // Ensure the inner loop guard successor is empty before skipping
        // blocks.
        if (Succ->getInstList().size() == 1) {
          PotentialInnerPreHeader =
              &LoopNest::skipEmptyBlockUntil(Succ, InnerLoopPreHeader);
          PotentialOuterLatch =
              &LoopNest::skipEmptyBlockUntil(Succ, OuterLoopLatch);
        }

        if (PotentialInnerPreHeader == InnerLoopPreHeader)
          continue;
        if (PotentialOuterLatch == OuterLoopLatch)
          continue;

        // If `InnerLoopExit` contains LCSSA Phi instructions, additional block
        // may be inserted before the `OuterLoopLatch` to which `BI` jumps. The
        // loops are still considered perfectly nested if the extra block only
        // contains Phi instructions from InnerLoopExit and OuterLoopHeader.
        if (InnerLoopExitContainsLCSSA && IsExtraPhiBlock(*Succ) &&
            Succ->getSingleSuccessor() == OuterLoopLatch) {
          // Points to the extra block so that we can reference it later in the
          // final check. We can also conclude that the inner loop is
          // guarded and there exists LCSSA Phi node in the exit block later if
          // we see a non-null `ExtraPhiBlock`.
          ExtraPhiBlock = Succ;
          continue;
        }

        LLVM_DEBUG(
          dbgs() << "Inner loop guard successor " << Succ->getName()
                 << " doesn't lead to inner loop preheader or "
                    "outer loop latch.\n";
        );
        return false;
      }
    }
  }

  // Ensure the inner loop exit block lead to the outer loop latch possibly
  // through empty blocks.
  if ((!ExtraPhiBlock ||
       &LoopNest::skipEmptyBlockUntil(InnerLoop.getExitBlock(),
                                      ExtraPhiBlock) != ExtraPhiBlock) &&
      (&LoopNest::skipEmptyBlockUntil(InnerLoop.getExitBlock(),
                                      OuterLoopLatch) != OuterLoopLatch)) {
    LLVM_DEBUG(
        dbgs() << "Inner loop exit block " << *InnerLoopExit
               << " does not directly lead to the outer loop latch.\n";
    );
    return false;
  }

  return true;
}

static CmpInst *getOuterLoopLatchCmp(const Loop &OuterLoop) {

  const BasicBlock *Latch = OuterLoop.getLoopLatch();
  assert(Latch && "Expecting a valid loop latch");

  const BranchInst *BI = dyn_cast<BranchInst>(Latch->getTerminator());
  assert(BI && BI->isConditional() &&
         "Expecting loop latch terminator to be a branch instruction");

  CmpInst *OuterLoopLatchCmp = dyn_cast<CmpInst>(BI->getCondition());
  LLVM_DEBUG(
      if (OuterLoopLatchCmp) {
        dbgs() << "Outer loop latch compare instruction: " << *OuterLoopLatchCmp
               << "\n";
      });
  return OuterLoopLatchCmp;
}

static CmpInst *getInnerLoopGuardCmp(const Loop &InnerLoop) {

  BranchInst *InnerGuard = InnerLoop.getLoopGuardBranch();
  CmpInst *InnerLoopGuardCmp =
      (InnerGuard) ? dyn_cast<CmpInst>(InnerGuard->getCondition()) : nullptr;

  LLVM_DEBUG(
      if (InnerLoopGuardCmp) {
        dbgs() << "Inner loop guard compare instruction: " << *InnerLoopGuardCmp
               << "\n";
      });
  return InnerLoopGuardCmp;
}

static bool checkSafeInstruction(const Instruction &I,
                                 const CmpInst *InnerLoopGuardCmp,
                                 const CmpInst *OuterLoopLatchCmp,
                                 Optional<Loop::LoopBounds> OuterLoopLB) {

  bool IsAllowed =
      isSafeToSpeculativelyExecute(&I) || isa<PHINode>(I) || isa<BranchInst>(I);
  if (!IsAllowed)
    return false;
  // The only binary instruction allowed is the outer loop step instruction,
  // the only comparison instructions allowed are the inner loop guard
  // compare instruction and the outer loop latch compare instruction.
  if ((isa<BinaryOperator>(I) && &I != &OuterLoopLB->getStepInst()) ||
      (isa<CmpInst>(I) && &I != OuterLoopLatchCmp && &I != InnerLoopGuardCmp)) {
    return false;
  }
  return true;
}

static LoopNestEnum analyzeLoopNestForPerfectNest(
    const Loop &OuterLoop, const Loop &InnerLoop, ScalarEvolution &SE) {

  assert(!OuterLoop.isInnermost() && "Outer loop should have subloops");
  assert(!InnerLoop.isOutermost() && "Inner loop should have a parent");
  LLVM_DEBUG(dbgs() << "Checking whether loop '" << OuterLoop.getName()
                    << "' and '" << InnerLoop.getName()
                    << "' are perfectly nested.\n");

  // Determine whether the loops structure satisfies the following requirements:
  //  - the inner loop should be the outer loop's only child
  //  - the outer loop header should 'flow' into the inner loop preheader
  //    or jump around the inner loop to the outer loop latch
  //  - if the inner loop latch exits the inner loop, it should 'flow' into
  //    the outer loop latch.
  if (!checkLoopsStructure(OuterLoop, InnerLoop, SE)) {
    LLVM_DEBUG(dbgs() << "Not perfectly nested: invalid loop structure.\n");
    return InvalidLoopStructure;
  }

  // Bail out if we cannot retrieve the outer loop bounds.
  auto OuterLoopLB = OuterLoop.getBounds(SE);
  if (OuterLoopLB == None) {
    LLVM_DEBUG(dbgs() << "Cannot compute loop bounds of OuterLoop: "
                      << OuterLoop << "\n";);
    return OuterLoopLowerBoundUnknown;
  }

  CmpInst *OuterLoopLatchCmp = getOuterLoopLatchCmp(OuterLoop);
  CmpInst *InnerLoopGuardCmp = getInnerLoopGuardCmp(InnerLoop);

  // Determine whether instructions in a basic block are one of:
  //  - the inner loop guard comparison
  //  - the outer loop latch comparison
  //  - the outer loop induction variable increment
  //  - a phi node, a cast or a branch
  auto containsOnlySafeInstructions = [&](const BasicBlock &BB) {
    return llvm::all_of(BB, [&](const Instruction &I) {
      bool IsSafeInstr = checkSafeInstruction(I, InnerLoopGuardCmp,
                                              OuterLoopLatchCmp, OuterLoopLB);
      if (IsSafeInstr) {
        LLVM_DEBUG(
          dbgs() << "Instruction: " << I << "\nin basic block:" << BB
                 << "is unsafe.\n";
        );
      }
      return IsSafeInstr;
    });
  };

  // Check the code surrounding the inner loop for instructions that are deemed
  // unsafe.
  const BasicBlock *OuterLoopHeader = OuterLoop.getHeader();
  const BasicBlock *OuterLoopLatch = OuterLoop.getLoopLatch();
  const BasicBlock *InnerLoopPreHeader = InnerLoop.getLoopPreheader();

  if (!containsOnlySafeInstructions(*OuterLoopHeader) ||
      !containsOnlySafeInstructions(*OuterLoopLatch) ||
      (InnerLoopPreHeader != OuterLoopHeader &&
       !containsOnlySafeInstructions(*InnerLoopPreHeader)) ||
      !containsOnlySafeInstructions(*InnerLoop.getExitBlock())) {
    LLVM_DEBUG(dbgs() << "Not perfectly nested: code surrounding inner loop is "
                         "unsafe\n";);
    return ImperfectLoopNest;
  }

  LLVM_DEBUG(dbgs() << "Loop '" << OuterLoop.getName() << "' and '"
                    << InnerLoop.getName() << "' are perfectly nested.\n");

  return PerfectLoopNest;
}

static InstrVectorTy getInterveningInstructions(
    const Loop &OuterLoop, const Loop &InnerLoop, ScalarEvolution &SE) {
  InstrVectorTy Instr;
  switch (analyzeLoopNestForPerfectNest(OuterLoop, InnerLoop, SE)) {
  case PerfectLoopNest:
    LLVM_DEBUG(dbgs() << "The loop Nest is Perfect, returning empty "
                         "instruction vector. \n";);
    return Instr;

  case InvalidLoopStructure:
    LLVM_DEBUG(dbgs() << "Not perfectly nested: invalid loop structure. "
                         "Instruction vector is empty.\n";);
    return Instr;

  case OuterLoopLowerBoundUnknown:
    LLVM_DEBUG(dbgs() << "Cannot compute loop bounds of OuterLoop: "
                      << OuterLoop << "\nInstruction vector is empty.\n";);
    return Instr;

  case ImperfectLoopNest:
    break;
  }

  // Identify the outer loop latch comparison instruction.
  auto OuterLoopLB = OuterLoop.getBounds(SE);

  CmpInst *OuterLoopLatchCmp = getOuterLoopLatchCmp(OuterLoop);
  CmpInst *InnerLoopGuardCmp = getInnerLoopGuardCmp(InnerLoop);

  auto GetUnsafeInstructions = [&](const BasicBlock &BB) {
    for (const Instruction &I : BB) {
      if (!checkSafeInstruction(I, InnerLoopGuardCmp, OuterLoopLatchCmp,
                                OuterLoopLB)) {
        Instr.push_back(&I);
        LLVM_DEBUG(
          dbgs() << "Instruction: " << I << "\nin basic block:" << BB
                 << "is unsafe.\n";
        );
      }
    }
  };

  // Check the code surrounding the inner loop for instructions that are deemed
  // unsafe.
  const BasicBlock *OuterLoopHeader = OuterLoop.getHeader();
  const BasicBlock *OuterLoopLatch = OuterLoop.getLoopLatch();
  const BasicBlock *InnerLoopPreHeader = InnerLoop.getLoopPreheader();
  const BasicBlock *InnerLoopExitBlock = InnerLoop.getExitBlock();

  GetUnsafeInstructions(*OuterLoopHeader);
  GetUnsafeInstructions(*OuterLoopLatch);
  GetUnsafeInstructions(*InnerLoopExitBlock);

  if (InnerLoopPreHeader != OuterLoopHeader) {
    GetUnsafeInstructions(*InnerLoopPreHeader);
  }
  return Instr;
}

// Determines whether it is possible to perfectize a loop.
// For simplicity (since this is mostly just to undo LICM) we only allow
// instructions that are side effect free and are not memory accesses.
// Then, the SSA form gives us that any operands used in the pre-header will
// not change during execution of the inner loops, so this transformation is
// sound
// Returns true if it is possible to perfectize.
static bool loopPerfectize(Loop& OuterLoop, Loop& InnerLoop,
                           ScalarEvolution& SE) {
  SmallVector<const Instruction*> insts =
    getInterveningInstructions(OuterLoop, InnerLoop, SE);
  LLVM_DEBUG(dbgs() << "Attempting to perfectize loop nest\n");
  for (const Instruction* inst : insts) {
    LLVM_DEBUG(dbgs() << "Intervening Instruction: "; inst->dump());
    // Check that the instruction is pure and does not access memory
    if (inst->mayReadOrWriteMemory() || inst->mayHaveSideEffects()) {
      LLVM_DEBUG(dbgs() << "Instruction is not pure or accesses memory\n");
      return false;
    }
  }

  return true;
}
