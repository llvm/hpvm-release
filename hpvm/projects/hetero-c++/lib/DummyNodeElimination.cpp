//===- extract-task.cpp -- Extract a parallel task into node function ----===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM
// Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===---------------------------------------------------------------------===//
//
// This file extracts a parallel loop or task identified by a hpvm marker
// into its own node function, repeating that for all marked tasks.
//
//===---------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "DFGUtils.h"
#include "llvm/IR/Constants.h"



#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>
#include <fstream>
#include <iostream>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;
using std::queue;

const string CreateNodeNDMarkerFuncName = "__hpvm__createNodeND";
const string EdgeFuncName = "__hpvm__edge";
const string LaunchFuncName = "__hpvm__launch";
const string BindInFuncName = "__hpvm__bindIn";
const string BindOutFuncName = "__hpvm__bindOut";




void DummyFatalError(const StringRef &msg) {
  std::cerr << "hpvm-dummy: FATAL: " << msg.str() << std::endl;
  exit(1);
  return;
}

void DummyWarnError(const StringRef &msg) {
  std::cerr << "hpvm-dummy: WARNING: " << msg.str() << std::endl;
  return;
}

struct HPVMDummy {
    public:
        HPVMDummy(Module &M): M(M){

            createNodeFunc = M.getFunction(CreateNodeNDMarkerFuncName);
            edgeFunc = M.getFunction(EdgeFuncName);
            launchFunc = M.getFunction(LaunchFuncName);
            bindInFunc = M.getFunction(BindInFuncName);
            bindOutFunc = M.getFunction(BindOutFuncName);

            if(!createNodeFunc){
                DummyFatalError("Unable to find definition for:\t"+CreateNodeNDMarkerFuncName);
            }

            if(!edgeFunc){
                DummyFatalError("Unable to find definition for:\t"+EdgeFuncName);
            }

            if(!launchFunc){
                DummyFatalError("Unable to find definition for:\t"+LaunchFuncName);
            }

            if(!bindInFunc){
                DummyFatalError("Unable to find definition for:\t"+BindInFuncName);
            }

            if(!bindOutFunc){
                DummyFatalError("Unable to find definition for:\t"+BindOutFuncName);
            }


            for(auto* User: launchFunc->users()){
                CallInst* CI = dyn_cast<CallInst>(User);

                if(!CI) continue;
                 dbgs()<<"Launch Instruction: "<<*CI<<"\n";

                runOnDFGRoot(CI);
            
            }



        }

    private:
        Function* createNodeFunc;
        Function* edgeFunc;
        Function* launchFunc;
        Function* bindInFunc;
        Function* bindOutFunc;

        Module &M;

        void runOnDFGRoot(CallInst* Launch);

        void populateQueue(CallInst* CI, queue<CallInst*>& Q);
        
        

};

void HPVMDummy::runOnDFGRoot(CallInst* Launch){

    // Launch and CreateNodeND calls have the function at
    // the same index position
    queue<CallInst*> WorkList;

    populateQueue(Launch, WorkList);


    while(!WorkList.empty()){
        CallInst* Pre = WorkList.front();
        WorkList.pop();


        Function* PreFn = getCreateNodeNDFunction(Pre);
        vector<CallInst*> Children = getChildRootsCalls(PreFn);

        bool reprocess = false;

        for(CallInst* Child : Children){
            bool isChildDummy = isChildNodeRedundant(Child);
            reprocess |= isChildDummy;

            if(!isChildDummy) continue;

            Function* ChildFn = getCreateNodeNDFunction(Child);


            CallInst* Post = getChildRootsCalls(ChildFn)[0];

            errs()<<"Connecting "<< PreFn->getName()<<" directly to "<<getCreateNodeNDFunction(Post)->getName()<<"\n";

            // Connect Pre to Post directly
            removeRedundantNode(Pre, Child, Post);
        }

        if(reprocess)
            WorkList.push(Pre);
    }
}

void HPVMDummy::populateQueue(CallInst* CI, queue<CallInst*>& Q){

    Q.push(CI);

    vector<CallInst*> Children = getChildRootsCalls(getCreateNodeNDFunction(CI));

    for(CallInst* Ch : Children){
        Q.push(Ch);
        populateQueue(Ch, Q);
    }
}



void removeDummyNodes(Module &M)
{
  ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

  HPVMDummy Dummy(M);
}
