// #include "DFGUtils.h"
#include "DAGInfo.h"


using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;
using std::queue;



class HPVMArgAttrProp {
    public:
        HPVMArgAttrProp(Module &M) : Mod(M) {
            LLVM_DEBUG(errs() << "=== HPVMArgAttrProp ===\n");

            Function* HPVMLaunch = Mod.getFunction(LaunchFuncName);
            if(!HPVMLaunch){
                LLVM_DEBUG(errs()<<"Launch Function not found, returning early...\n");
                return;
            }

            for(auto* User : HPVMLaunch->users()){
                if(CallInst* CI = dyn_cast<CallInst>(User)){
                    Function* RootFunc = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());
                    assert(RootFunc && "__hpvm__launch must have a specified function for the root node");

                    LaunchCalls.push_back(CI);
                    RootNodes.insert(RootFunc);
                }
            }

            DAGInfo = HPVMDAGInfoAnalysis(Mod);

            for(Function* Root : RootNodes){
                LLVM_DEBUG(errs()<<"runOnNode on root "<<Root->getName()<<"\n");
                runOnNode(Root);
            }




        }

        void runOnNode(Function* Node);

    private:
        Module& Mod;
        HPVMDAGInfo* DAGInfo;
        std::set<Function*> RootNodes;
        vector<CallInst*> LaunchCalls;
};

void HPVMArgAttrProp::runOnNode(Function* Node){

    if(!DAGInfo->isInternalNode(Node)) return;

    vector<CallInst*> BindIns = DAGInfo->getBindInCalls(Node);

    for(CallInst* BI: BindIns){
        Argument* ParentArg = DAGInfo->getArgForBindIn(BI);

        ConstantInt* ChildArgNum = dyn_cast<ConstantInt>(BI->getArgOperand(2));
        assert(ChildArgNum && "BindIn must have a constant ic value");

        int child_arg_idx = ChildArgNum->getSExtValue();


        CallInst* CreateNodeCall = dyn_cast<CallInst>(BI->getArgOperand(0));

        Function* ChildNode = getCreateNodeNDFunction(CreateNodeCall);

        Argument* ChildArg = dyn_cast<Argument>((ChildNode->arg_begin())+child_arg_idx);

        copyAttributes(ParentArg, ChildArg);

    }


    vector<CallInst*> Edges = DAGInfo->getEdgeCalls(Node);

    for(CallInst* E: Edges){
        Argument* ParentArg = DAGInfo->getArgForEdge(E);

        ConstantInt* DstArgNum = dyn_cast<ConstantInt>(E->getArgOperand(4));
        assert(DstArgNum && "Edge must have a constant dp value");

        int dst_arg_idx = DstArgNum->getSExtValue();
        

        CallInst* CreateNodeCall = dyn_cast<CallInst>(E->getArgOperand(1));

        Function* ChildNode = getCreateNodeNDFunction(CreateNodeCall);

        Argument* ChildArg = dyn_cast<Argument>((ChildNode->arg_begin())+dst_arg_idx);

        copyAttributes(ParentArg, ChildArg);

    }

    vector<CallInst*> CreateNodeNDCalls = DAGInfo->getCreateNodeNDCalls(Node);

    for(CallInst* CreateNode : CreateNodeNDCalls){
        runOnNode(getCreateNodeNDFunction(CreateNode));
    }
}


void HPVMArgProp(Module& M){

  LLVM_DEBUG(errs() << "=== HPVMArgProp ===\n");
  ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

  new HPVMArgAttrProp(M);
}
