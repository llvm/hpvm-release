//===- HPVMCGenContext.cpp - HPVM-C generation context ----------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HPVMCGenContext.cpp -- Implementation of HPVMCGenContext
//
//===----------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/SourceMgr.h"
#include "HPVMCGenPass.h"
#include "HPVMCGen.h"
#include "HPVMCGenContext.h"
#include "HPVMExtractTask.h"


static cl::opt<std::string> DeclsFilename("declsfile", cl::Optional,
  cl::desc("HPVM-C declarations file"),
  cl::init("HPVMCFunctionDeclarations.ll"));

void llvm::HPVMFatalError(const std::string& msg) {
  std::cerr << "HPVMCGen: FATAL: " << msg << std::endl;
  exit(1);
}

void llvm::HPVMWarnError(const std::string& msg) {
  std::cerr << "HPVMCGen: WARNING: " << msg << std::endl;
}

std::unique_ptr<Module> 
  HPVMCGenContext::loadHPVMCFunctionDeclsFile(const std::string& pathname) {
  LLVMContext& Context = M.getContext();
  SMDiagnostic Err;
  std::unique_ptr<Module> importedM = parseIRFile(pathname, Err, Context);
  if (!importedM)
    Err.print("HPVMCGen: Failed to open HPVM-C declarations file.", errs());
  return importedM;
}

int HPVMCGenContext::importHPVMCFunctionDecls(const Module& fromModule) {
  for (auto& GG: fromModule) {
    // Skip if GG is not a function or if it has a function body
    if (! isa<Function>(GG) || GG.getBasicBlockList().size() > 0)
      continue;
      
    LLVM_DEBUG(std::cout << "Copying in " << GG.getName().str() << std::endl);

    // First make sure the function doesn't already exist in current module
    if (M.getFunction(GG.getName())) {
      LLVM_DEBUG("Trying to insert HPVM-C function decl for " +
                     GG.getName().str() + "twice?");
    }
    else {
      // Now insert an exact copy of the function declaration
      FunctionCallee callee = M.getOrInsertFunction(GG.getName(),
          GG.getFunctionType(), GG.getAttributes());

      // Mark the function as dso_local because it will be linked in statically
      assert(isa<Function>(callee.getCallee()) && "callee not a Function?");
      Function* HPVMCFunc = static_cast<Function*>(callee.getCallee());
      HPVMCFunc->setDSOLocal(true);
      
      // And create the HPVMFuncDescriptor for it, which also registers it
      (void) new HPVMFuncDescriptor(getFunctionList(),
                                    GG.getName(), *GG.getFunctionType());
    }
  }
  return 0; // FIXME: Replace fatal error above with warning and return error
}

HPVMCGenContext::HPVMCGenContext(Module& _M, bool NoModuleFile) :
      M(_M), llvmContext(_M.getContext()), IRB(llvmContext)

#ifdef EXPLICIT_FUNC_DESCRIPTORS
      ,

      //
      // Internal node functions
      hpvm_func_edge(funcList, "__hpvm__edge", 6, false),
      hpvm_func_createNodeND(funcList, "__hpvm__createNodeND", 2, true),
      hpvm_func_bindIn(funcList, "__hpvm__bindIn", 4, false),
      hpvm_func_bindOut(funcList, "__hpvm__bindOut", 4, false),
      hpvm_func_hint(funcList, "__hpvm__hint", 1, false),
      hpvm_func_attributes(funcList, "__hpvm__attributes", 1, true),

      // Leaf node functions
      hpvm_func_getNode(funcList, "__hpvm__getNode", 0, false),
      hpvm_func_getParentNode(funcList, "__hpvm__getParentNode", 1, false),
      hpvm_func_barrier(funcList, "__hpvm__barrier", 0, false),
      hpvm_func_malloc(funcList, "__hpvm__malloc", 1, false),
      hpvm_func_return (funcList, "__hpvm__return ", 1, true),
      hpvm_func_getNodeInstanceID_x(funcList,
                            "__hpvm__getNodeInstanceID_x", 1, false),
      hpvm_func_getNodeInstanceID_y(funcList,
                            "__hpvm__getNodeInstanceID_y", 1, false),
      hpvm_func_getNodeInstanceID_z(funcList,
                            "__hpvm__getNodeInstanceID_z", 1, false),
      hpvm_func_getNumNodeInstances_x(funcList,
                            "__hpvm__getNumNodeInstances_x", 1, false),
      hpvm_func_getNumNodeInstances_y(funcList,
                            "__hpvm__getNumNodeInstances_y", 1, false),
      hpvm_func_getNumNodeInstances_z(funcList,
                            "__hpvm__getNumNodeInstances_z", 1, false),

      // Atomics
      hpvm_func_atomic_add(funcList, "__hpvm__atomic_add", 2, false),
      hpvm_func_atomic_sub(funcList, "__hpvm__atomic_sub", 2, false),
      hpvm_func_atomic_xchg(funcList, "__hpvm__atomic_xchg", 2, false),
      hpvm_func_atomic_min(funcList, "__hpvm__atomic_min", 2, false),
      hpvm_func_atomic_max(funcList, "__hpvm__atomic_max", 2, false),
      hpvm_func_atomic_and(funcList, "__hpvm__atomic_and", 2, false),
      hpvm_func_atomic_or(funcList, "__hpvm__atomic_or", 2, false),
      hpvm_func_atomic_xor(funcList, "__hpvm__atomic_xor", 2, false),

      // Misc Functions (undocumented in the HPVM-C specification)
      hpvm_func_sin(funcList, "__hpvm__sin", 0, false),
      hpvm_func_cos(funcList, "__hpvm__cos", 0, false),

      // Host functions
      hpvm_func_init(funcList, "__hpvm__init", 0, false),
      hpvm_func_cleanup(funcList, "__hpvm__cleanup", 0, false),
      hpvm_func_launch(funcList, "__hpvm__launch", 3, false),
      hpvm_func_wait(funcList, "__hpvm__wait", 1, false),
      hpvm_func_push(funcList, "__hpvm__push", 2, false),
      hpvm_func_pop(funcList, "__hpvm__pop", 1, false),
      hpvm_func_llvm_trackMemory(funcList, "llvm_hpvm_track_mem", 2, false),
      hpvm_func_llvm_untrackMemory(funcList, "llvm_hpvm_untrack_mem", 1, false),
      hpvm_func_llvm_requestMemory(funcList, "llvm_hpvm_request_mem", 2, false)
#endif
  {}

HPVMCGenContext::HPVMCGenContext(Module& _M) :
      M(_M), llvmContext(_M.getContext()), IRB(llvmContext) {
  
  // Open specified decls file as an LLVM module
  //
  std::unique_ptr<Module> importedM = loadHPVMCFunctionDeclsFile(DeclsFilename);
  if (!importedM.get())
    HPVMFatalError("Failed to open module " + DeclsFilename);

  // Import HPVM-C function declarations from the imported module into this one
  //
  if (importHPVMCFunctionDecls(*importedM) != 0)
    HPVMFatalError("Failed to import declarations from module" + DeclsFilename);
}
