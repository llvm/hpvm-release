//===- extract-task.cpp -- Extract a parallel task into node function ----===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM
// Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===---------------------------------------------------------------------===//
//
// This file extracts a parallel loop or task identified by a hpvm marker
// into its own node function, repeating that for all marked tasks.
//
//===---------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/PostOrderIterator.h"
#include "HPVMCGenContext.h"
#include "HPVMCGen.h"
#include "HPVMCGenFunctions.h"
#include "DFGUtils.h"
#include "DominatorTreeCache.h"
#include "llvm/IR/Constants.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"



#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;

// FIXME: Use LLVM's name mangling library to construct the mangled name
const string LoopMarkerFuncNameVariadic = "__hetero_parallel_loop";
const string ParSectionBeginMarkerFuncName = "__hetero_section_begin";
const string ParSectionEndMarkerFuncName = "__hetero_section_end";
const string TaskEndMarkerFuncName = "__hetero_task_end";
const string TaskBeginMarkerFuncNameVariadic = "__hetero_task_begin";
const string CreateNodeNDMarkerFuncName = "__hpvm__createNodeND";
const string NonZeroMarkerFuncName = "__hpvm__isNonZeroLoop";

const string LegacyLoopMarkerFuncNameVariadic = "__hpvm_parallel_loop";
const string LegacyParSectionBeginMarkerFuncName = "__hpvm_parallel_section_begin";
const string LegacyParSectionEndMarkerFuncName = "__hpvm_parallel_section_end";
const string LegacyTaskEndMarkerFuncName = "__hpvm_task_end";
const string LegacyTaskBeginMarkerFuncNameVariadic = "__hpvm_task_begin";

// Used for Debugging. If true, removes extracted CallInsts
// once Nodes are created.
bool KeepExtractedCalls = false;



void fatalError(const StringRef &msg) {
  std::cerr << "extract-task: FATAL: " << msg.str() << std::endl;
  exit(1);
  return;
}

void warnError(const StringRef &msg) {
  std::cerr << "extract-task: WARNING: " << msg.str() << std::endl;
  return;
}

// FIXME: Janky unchecked variant to polyfill for C++17. Will fail silently if
// used incorrectly, so use it correctly!
template <typename... Ts>
class variant {
public:
  variant() : Storage(new char[std::max({sizeof(Ts)...})]) {}

  ~variant() {
    delete[] Storage;
  }

  template <typename Requested>
  void operator=(const Requested &Val) {
    Data = static_cast<void*>(new(Storage) Requested(Val));
  }

  template <typename Requested>
  void operator=(Requested &&Val) {
    Data = static_cast<void*>(new(Storage) Requested(std::move(Val)));
  }

  template <typename Requested>
  Requested& get() {
    return *static_cast<Requested*>(Data);
  }

private:
  // We keep two separate pointers to avoid undefined behavior on pointer casts
  // Pointer to the constructed object
  void *Data;
  // Pointer to the backing storage for all the objects
  char *Storage;
};

// Represents a region of code bounded by type T*, where T may be Instruction
// or BasicBlock
template <typename T>
struct Region {

  //Region() {}

  Region(T *Begin, T *End) : Begin(Begin), End(End) {}

  Region(const Region<T> &Orig) : Begin(Orig.Begin), End(Orig.End) {}

  bool operator<(const Region<T> &val) const {
    return Begin < val.Begin;
  }

  T *Begin;
  T *End;
};

// A parameter to a HPVM Dataflow Node
// may be a Scalar Value, otherwise
// it may be a Pointer Value, in which case
// the memory size must also be provided.
struct BufferParameter {
  Value *Buffer;
  Value *Size;

  unsigned size() {
      unsigned ParameterSize = 0;

      if(Buffer) ParameterSize++;
      if(Size) ParameterSize++;

      return ParameterSize;
  }
};

enum TaskType {
    LEAF,
    INTERNAL_DIRECT,
    INTERNAL_ACROSS
};

template <typename LocationType>
struct Task {
  Task(const LocationType &Location, CallInst* Marker) : Location(Location), 
    Type(TaskType::LEAF), AttributeCall(nullptr), NumParallelEnclosing(1), MarkerCall(Marker), Name("") {}

  // Dependencies must be manually initialized when using this constructor
  template <typename T>
  Task(const Task<T> &Orig,
       const function<LocationType(T)> &TransformLocation)
    : Location(TransformLocation(Orig.Location)), Inputs(Orig.Inputs), Outputs(Orig.Outputs),
  Type(Orig.Type), AttributeCall(Orig.AttributeCall), ReturnCalls(Orig.ReturnCalls), MarkerCall(Orig.MarkerCall), NumParallelEnclosing(Orig.NumParallelEnclosing) , Name(Orig.Name)  {}

  virtual bool isLoop() {return false;}
  bool isInternalNode() {return Type == TaskType::INTERNAL_DIRECT || Type == TaskType::INTERNAL_ACROSS; }

  LocationType Location;
  vector<pair<Task<LocationType>*,unsigned>> Dependencies;
  vector<pair<Task<LocationType>*,unsigned>> RevDependencies;

  CallInst* MarkerCall;
  int NumParallelEnclosing;
  vector<BufferParameter> Inputs;
  vector<BufferParameter> Outputs;
  TaskType Type;
  std::string Name;
  CallInst* AttributeCall; // __hpvm_attributes call
  vector<CallInst*> ReturnCalls; // container to store both __hpvm_return and bindout
};


// Parallel Task child class for Parallel Loops. Gets converted into
// 1 Dimensional HPVM Nodes.
template <typename LocationType>
struct LoopTask : public Task<LocationType> {

    LoopTask(const LocationType &Location, CallInst* MarkerCall) : Task<LocationType>(Location, MarkerCall),
    L(nullptr)  {}

    LoopTask(const LocationType &Location, Loop *ParallelLoop) : Task<LocationType>(Location, nullptr),
    L(ParallelLoop) {}

    template <typename T>
    LoopTask(const Task<T> &Orig,
       const function<LocationType(T)> &TransformLocation)
    : Task<LocationType>(Orig,TransformLocation) {}



    bool isLoop() {return true;}

    Loop* L;
};

// Parallel Task child class for Sequential Code. Gets converted into
// 0 Dimensional HPVM Nodes.
template <typename LocationType>
struct SeqTask : public Task<LocationType> {
    SeqTask(const LocationType &Location, CallInst* MarkerCall) : Task<LocationType>(Location, MarkerCall){}

    template <typename T>
    SeqTask(const Task<T> &Orig,
       const function<LocationType(T)> &TransformLocation)
    : Task<LocationType>(Orig,TransformLocation) {}

    bool isLoop() {return false;}
};


template <typename LocationType>
struct Section {
  Section() : BeginMarker(nullptr), EndMarker(nullptr),  isOuterMostSec(false) {}
  Section(CallInst* SecBegin, CallInst* SecEnd) : BeginMarker(SecBegin), EndMarker(SecEnd), isOuterMostSec(false) {}

  
  // Depending on whether a Task is a Loop or Sequential Type, apply different transformations
  template <typename T>
  Section(const Section<T> &Orig, const function<LocationType(T)> &TransformLocation, const function<LocationType(T)> &TransformLocationLoop) : BeginMarker(Orig.BeginMarker), EndMarker(Orig.EndMarker), isOuterMostSec(Orig.isOuterMostSec)
  {
    // Create the new tasks and record the mapping from old task to new task
    map<Task<T>*, Task<LocationType>*> TaskMap;
    for (const auto &OrigTask : Orig.Tasks) {
        if(OrigTask.get()->isLoop()){
            unique_ptr<Task<LocationType>> NewTask = std::make_unique<LoopTask<LocationType>>(*OrigTask, TransformLocationLoop);
            TaskMap[OrigTask.get()] = NewTask.get();
            Tasks.emplace_back(std::move(NewTask));
        } else {
            unique_ptr<Task<LocationType>> NewTask = std::make_unique<SeqTask<LocationType>>(*OrigTask, TransformLocation);
            TaskMap[OrigTask.get()] = NewTask.get();
            Tasks.emplace_back(std::move(NewTask));
      
        }
    }

    // Use the mapping from old task to new task to set the dependencies
    // correctly, since the Task constructor doesn't set dependencies on its own
    for (const auto &OrigTask : Orig.Tasks) {
      auto NewTask = TaskMap[OrigTask.get()];
      for (auto Dependency : OrigTask->Dependencies) {
        auto NewDep = std::make_pair(TaskMap[Dependency.first],Dependency.second);
        NewTask->Dependencies.push_back(NewDep); 
      }
      for (auto RevDependency : OrigTask->RevDependencies) {
        auto NewDep = std::make_pair(TaskMap[RevDependency.first], RevDependency.second);
        NewTask->RevDependencies.push_back(NewDep);
      }
    }

    //Location = TransformLocation(Orig.Location);

  }

 template <typename T>
  Section(const Section<T> &Orig, const function<LocationType(T)> &TransformLocation) : Section(Orig, TransformLocation, TransformLocation) {}


  Section(Section &&Orig) = default;

  void RemoveBindOuts(){
      while(BindOuts.size()){
          CallInst* OutCall = BindOuts.back();
          BindOuts.pop_back();

          OutCall->eraseFromParent();
      }
  }

  CallInst* BeginMarker;
  CallInst* EndMarker;

  // LocationType Location;
  vector<unique_ptr<Task<LocationType>>> Tasks;
  vector<CallInst*> BindOuts;
  map<Value*,unsigned> ArgToBindOutMap; // Mapping Section Formal Arguments to Bind Out Indices
  bool isOuterMostSec;
};



struct HPVMProgram {
public:
  HPVMProgram(Module &M, DominatorTreeCache &DTCache, bool SanatiseFunc, bool Use0D, bool NoReturnSize)
    : M(M), DTCache(DTCache), SanatiseFunc(SanatiseFunc), Use0D(Use0D), NoRetSize(NoReturnSize)
  {

    
    replaceLegacyWithNewFunctions();
    
    LLVM_DEBUG(dbgs()<<"GetMarkerFunctionBounds\n");
    getMarkerFunctionBounds();

    LLVM_DEBUG(dbgs()<<"constructSections\n");
    transform(&HPVMProgram::constructSections);

    LLVM_DEBUG(dbgs()<<"splitMarkerBoundary\n");
    transform(&HPVMProgram::splitMarkerBoundary);
    LLVM_DEBUG(dbgs()<<"extractSectionsAndTasks\n");
    transform(&HPVMProgram::extractSectionsAndTasks);


    LLVM_DEBUG(dbgs()<<"Printing Debug\n");
    printDebugInfo();
    LLVM_DEBUG(dbgs()<<"Insert intrinsics\n");
    transform(&HPVMProgram::insertHPVMIntrinsics);
  }

private:

  void replaceLegacyWithNewFunctions();


  // Checks for a call to the function EndMarker with parameter BeginI. BeginI
  // must dominate the call, and the call must post-dominate BeginI
  Instruction* getCorrespondingEndMarker(Instruction *BeginI, Function *EndMarker);

  // Checks that all of the provided regions are properly nested
  bool isCorrectlyNested(const vector<Region<Instruction>> &Bounds);

  // Initializes Data to the first stage, containing two sets of bounds, where
  // the first is for parallel sections, and the second is for tasks
  void getMarkerFunctionBounds();

  // Extracts the dependencies, inputs, and outputs from the begin marker function
  // call and sets the appropriate fields in the provided task. Takes the section
  // containing the task as a parameter to resolve dependencies
  void getTaskInformation(
    const Section<Region<Instruction>> &S, Task<Region<Instruction>> &T, Instruction* TaskMarker);


  // Identifies the linear dependencies from the Tasks within a Parallel Section
  // and stores this dependency information inside the Task object
  void getTaskDependency(
    const Section<Region<Instruction>> &S);

  // Identify which of the CalledFunctions Formal Arguments
  // are Local values in the Callers context. These values can
  // not be Bound to with BindIn/BindOut Calls.
  std::set<Argument*> GetExcludedArgs(CallInst* CI, CallInst* AttCall,
        CallInst* RetCall);

  // Create a Clone of a function with a subset of the arguments. 
  // Any Argument in Exclude will not be carried over and any of it's
  // uses will be removed.
  //Function* CreateClone(Function* Orig, std::set<Argument*> Exclude, ValueToValueMapTy& ArgMap);

  // Once a function has been cloned with a subset of it's original
  // formal arguments, create a CallInst using those subset of 
  // actual parameters from OrigCall.
  /*
  CallInst* CloneCallInst(CallInst* OrigCall, Function* NewF, 
          std::set<Argument*>& Exclude, ValueToValueMapTy& ArgMap);*/

  // Replace Loop iterations with single iteration. Induction variable
  // becomes the getNodeInstanceID value for 1D Nodes.
  //CallInst* parallelizeLoop(Loop* ExtractedLoop, Loop* InnerLoop, Value* InductionVar, CallInst* ExtractedCall);
  CallInst* parallelizeLoop(vector<Loop*>& LoopNest, vector<Value*>& InductionVars, vector<Value*>& Limits, CallInst* ExtractedCall);


  //CallInst* getMarkerCall(Function* F);


  // For each node function in the DFGs, 
  // insert hint calls for the target backend.
  void insertTargetHints(int Hint,const  Section<CallInst*>& S);


  // Any address calculation used inside a task must be contained inside the
  // task boundaries. In case the instructions are outside the boundaries, 
  // we must copy the instructions and correctly replace uses. We replace all
  // forward uses with the cloned instruction.
  void copyGEPCalculationInTask(Instruction* I, Region<Instruction> Bounds, ValueToValueMapTy &VMap,
          ValueToValueMapTy& RevVMap, CallInst* MarkerCall);


  // When Cloning Parallel Sections, use the marker functions
  // of it's contained tasks to infer the pairs of pointers
  // and their corresponding sizes.
  //std::vector<Value*> getSectionArgOrdering(Function* SecFn);


  // Constructs Section objects from the raw list of bounds
  vector<Section<Region<Instruction>>> constructSections(
    const pair<vector<Region<Instruction>>, vector<Region<Instruction>>> &Bounds);

  // Removes marker functions and instead delimits sections and tasks with BBs
  vector<Section<Region<BasicBlock>>> splitMarkerBoundary(
    const vector<Section<Region<Instruction>>> &Sections);

  // Extracts sections and tasks into their own functions and replaces them with
  // function calls
  vector<Section<CallInst*>> extractSectionsAndTasks(
    const vector<Section<Region<BasicBlock>>> &Sections);

  // Extracts Loops into their own functions and replaces them with function
  // calls
  CallInst* extractLoopContainingInst(HPVMCGenContext& cgenContext,
     Instruction& I);

  // Populate Tasks with updated mapping to hpvm intrinsics. Due 
  // to function cloning, instruction mappings may be outdated and need
  // tp be updated.
 void populateTaskIntrinsicsInfo(
  vector<Section<CallInst*>> &Sections);

 // Insert HPVM Edges between dependent tasks. Create
 // appropriate Bind In and Bind Out calls. For Internal
 // Node Tasks, Align the Binds to transfer the correct values.
 // Finally replace internal nodes __hpvm__return calls with Bindouts.
  int insertHPVMIntrinsics(
    vector<Section<CallInst*>> &Sections);

  bool isCreateNodeCall(CallInst* CI);

  // Once the marker calls are no longer needed, we
  // erase all their uses from the modules
  void removeMarkerCalls();

  bool isOuterMostParallelSection(Instruction* II);


  // Helper functions to identify which HPVM
  // Dataflow graphs are streaming vs non-streaming.
  std::set<Function*> getStreamingRootNodes();
  std::set<Function*> getNonStreamingRootNodes();


  // Remove any __hpvm__bindout calls from
  // a node function.
  void removeBindOuts(Function* F);


  // Identify whether Function F contains an
  // HPVM DataFlow Graph/Node defined by
  // a __hpvm_parallel_section_begin call.
  bool isHPVMDataFlowGraph(Function* F);


  void replaceIndirectCall(Function* F);


  void moveSubLoopEntryCheck(Loop* ParentLoop, Instruction* IB);

  std::pair<CallInst*, CallInst*> GetAttributeAndReturn(Function* F);


  // Once the parallel loop has been extracted, the loop's guard branch
  // needs to be removed (in the parent function). This function replaces
  // the guard condition with an unconditional branch to 'Direct'.
  void removeParallelLoopGuard(BasicBlock* Preheader, BasicBlock* Direct);

  // Returns whether the loop
  // contains a call to __hpvm_parallel_loop.
  bool isLoopParallel(Loop* L);


  // For nested rectangular loops, returns the boolean
  // if the parent loop is also parallel.
  bool isParentLoopParallel(Instruction* I);

  // For multiple nested loops, verify nested parallel loop structure
  // and return vectors of Loop Trip counts and Induction Variables
  // from the outer-most loops to the inner most loops.
  void getNDLoopLimits(Loop* L, ScalarEvolution& SE, vector<Value*>& Limits,
          vector<Value*>& InductionVariables, int numLoops);

  // For nested loops, return the vector of loops
  // from outer most to innermost, for those loops
  // which are parallel.
  vector<Loop*> getNestedParallelLoops(Loop* L);

  // Internal HPVM intrinsic to
  // maintain argument ordering across 
  // clones of functions.
  CallInst* createHPVMOrderCall(Instruction* IB, vector<Value*> Args);

  void replaceLoopTripCount(Instruction* I, ValueToValueMapTy& VMap,
          ValueToValueMapTy& RevVMap, int NumEnclosingLoops);

  void replaceOuterLoopIVInChild(CallInst* ExtractedCall,CallInst* getNodeCall, std::set<Argument*> TripCount);


  // Identify whether an instruction is part of the loops
  // preheader. As the loop preheader is not part of the loop
  // we would want to erase any instructions in it.
  bool isPartOfLoopHeader(Instruction* I);
  void clearLoopPreheader(Instruction* I);
  void updateLoopUses(Instruction* I, ValueToValueMapTy& RevVMap);

  bool isValueIV(Value* V);

  bool isGetNodeIdxCall(Value* V);

  Instruction* findImmediateLoopMarker(Instruction* I);
  
  // Get the type of the argument of a member function pointer
  template <typename U>
  struct class_member_fn_arg { using type = void; };
  template <typename Ret, typename Class, typename Arg>
  struct class_member_fn_arg<Ret(Class::*)(Arg)> { using type = Arg; };

  // Transforms the internal representation of the program in Data from one
  // stage to another according to the provided function of type Transformer
  template <typename Transformer>
  void transform(Transformer T) {
    using RawInputType = typename class_member_fn_arg<Transformer>::type;
    using InputType =
      typename std::remove_const<typename std::remove_reference<RawInputType>::type>::type;
    Data = (this->*T)(Data.get<InputType>());
  }

  void printDebugInfo();

  // Evolving representation of the program, where the last type in the variant
  // corresponds to the final stage in the pipeline
  variant<
    pair<vector<Region<Instruction>>, vector<Region<Instruction>>>,
    vector<Section<Region<Instruction>>>,
    vector<Section<Region<BasicBlock>>>,
    vector<Section<CallInst*>>> Data;

  Module &M;
  DominatorTreeCache &DTCache;
  bool SanatiseFunc;
  bool Use0D;
  bool NoRetSize;
  map<CallInst*, CallInst*> CallToNode;
  // Set of the result of cloning functions.
  // Used to ensure correct CallInst is returned
  // due to arbritrary traversals of tasks and internal nodes.
  std::set<Function*> ClonedFunctions;


  map<Function*, CallInst*> NodeToHint;

};

Instruction* HPVMProgram::getCorrespondingEndMarker(
  Instruction *BeginI, Function *EndMarker)
{
  if (!EndMarker) {
    fatalError("No end marker function corresponding to this begin marker function!");
  }

  unsigned numUses = BeginI->getNumUses();
  if (numUses > 1) {
    // Tasks could be dependent on other tasks
    //fatalError("Multiple uses of return value of begin marker function!");
  } else if (numUses < 1) {
    fatalError("No end marker function corresponding to this begin marker function!");
  }

  Instruction *EndI = NULL;
  for(auto *User : BeginI->users()){
      CallInst *BI = dyn_cast<CallInst>(User); 
      if(BI && BI->getCalledFunction() == EndMarker){
          EndI = (Instruction*) BI;
          break;
      }
  }

  //Instruction *EndI = dyn_cast<Instruction>(*BeginI->user_begin());

  // Make sure that EndI is a call to EndMarker
  CallInst *EndCall = dyn_cast<CallInst>(EndI);
  if (!EndCall || EndCall->getCalledFunction() != EndMarker) {
    fatalError("Tag returned by begin marker function not used in end marker function!");
  }

  if (!DTCache.dominates(BeginI, EndI)) {
    warnError("Begin marker function does not dominate end marker function");
  }

  if (!DTCache.postDominates(EndI, BeginI)) {
     warnError("End marker function does not post-dominate begin marker function");
  }

  return EndI;
}

bool HPVMProgram::isCorrectlyNested(const vector<Region<Instruction>> &Bounds)
{
  // For each input pair, check that any other pair of bounds in the same
  // function is either fully contained or fully not contained
  auto isContained = [&] (Instruction *I, Region<Instruction> Bound) {
    return DTCache.dominates(Bound.Begin, I) && DTCache.postDominates(Bound.End, I);
  };

  for (const auto &Bound : Bounds) {
    for (const auto &CheckBound : Bounds) {
      if (Bound.Begin == CheckBound.Begin)
        continue;
      if (Bound.Begin->getParent()->getParent() != CheckBound.Begin->getParent()->getParent())
        continue;

      bool BeginContained = isContained(CheckBound.Begin, Bound);
      bool EndContained = isContained(CheckBound.End, Bound);
      if (BeginContained ^ EndContained) {
        LLVM_DEBUG(dbgs() << "Blocks formed by begin/end pairs do not nest correctly:\n");
        LLVM_DEBUG(dbgs() << "A: " << *Bound.Begin << "\n" << "B: " << *Bound.End << "\n\n");
        LLVM_DEBUG(dbgs() << "C: " << *CheckBound.Begin << "\n" << "D: " << *CheckBound.End << "\n\n");
        LLVM_DEBUG(dbgs() << "C is" << (BeginContained ? "" : " not") << " contained within AB\n");
        LLVM_DEBUG(dbgs() << "D is" << (EndContained ? "" : " not") << " contained within AB\n");
        return false;
      }
    }
  }
  return true;
}

void HPVMProgram::getMarkerFunctionBounds() {
  Function *ParSectionBeginMarker = M.getFunction(ParSectionBeginMarkerFuncName);
  Function *ParSectionEndMarker = M.getFunction(ParSectionEndMarkerFuncName);
  Function *TaskBeginVarMarker = M.getFunction(TaskBeginMarkerFuncNameVariadic);
  Function *TaskEndMarker = M.getFunction(TaskEndMarkerFuncName);


  Function *LoopBeginMarker = M.getFunction(LoopMarkerFuncNameVariadic);

 

  // Verify that the number of begin and end calls are equal
  auto checkNumBeginEndCallsEqual = [&] (Function *BeginMarker, Function *EndMarker)
  {
    unsigned NumBeginCalls = BeginMarker ? BeginMarker->getNumUses() : 0;
    unsigned NumEndCalls = EndMarker ? EndMarker->getNumUses() : 0;
    if (NumBeginCalls != NumEndCalls) {
      fatalError("Number of calls to begin marker function not equal to number of"
        " calls to end marker function!");
    }
  };
  checkNumBeginEndCallsEqual(ParSectionBeginMarker, ParSectionEndMarker);
  checkNumBeginEndCallsEqual(TaskBeginVarMarker, TaskEndMarker);


  // Find the corresponding begin/end calls for the specified marker functions
  auto findBounds = [&] (Function *BeginMarker, Function *EndMarker)
    -> vector<Region<Instruction>>
  {
    auto Bounds = vector<Region<Instruction>>();
    for (auto *User : BeginMarker->users()) {
      Instruction *BeginI = dyn_cast<Instruction>(User);
      if (!BeginI) {
        fatalError("HPVM marker function used by non-instruction");
      }

      Instruction *EndI = getCorrespondingEndMarker(BeginI, EndMarker);

      Bounds.push_back(Region<Instruction>(BeginI, EndI));
    }
    return Bounds;
  };

  auto findLoopBounds = [&](Function *BeginMarker)-> vector<Region<Instruction>>
  {
    auto Bounds = vector<Region<Instruction>>();
    for (auto *User : BeginMarker->users()) {
      Instruction *BeginI = dyn_cast<Instruction>(User);
      if (!BeginI) {
        fatalError("HPVM marker function used by non-instruction\n");
      }

      // First operand must be a constant integer value specifying
      // the number of enclosing loops. That value must be > 0 and <= 3.
      CallInst* LoopBegin = dyn_cast<CallInst>(BeginI);

      if(!LoopBegin){
          HPVMFatalError("Parallel Loop marker used in a non-call inst context");
      }

      ConstantInt* NumLoops = dyn_cast<ConstantInt>(LoopBegin->getArgOperand(0));

      if(!NumLoops){
          HPVMFatalError("Parallel loop marker first operand must be a constant integer value");
      }

      int numEnclosingLoops = NumLoops->getSExtValue();

      if(numEnclosingLoops < 1 || numEnclosingLoops > 3){
          HPVMFatalError("Non-supported number of parallel loops");
      }
      
      Function* ParentF = BeginI->getParent()->getParent();
      auto &DT = DTCache.get(ParentF).DT;
      LoopInfo* LIp = new LoopInfo(DT);
      LoopInfo& LI = *LIp;
      LI.analyze(DT);
      Loop* myLoop = LI.getLoopFor(BeginI->getParent());
      if (! myLoop) {
        HPVMFatalError("No natural loop enclosing marker function call?\n");
      }

      for(int k = 1; k < numEnclosingLoops; k++){
          myLoop = myLoop->getParentLoop();

          if(!myLoop){
              HPVMFatalError("More Parallel loops specified than present in loop nest");
          }


      }
     

      BasicBlock* LoopExitBlock =  myLoop ->getLoopLatch();
      if(!LoopExitBlock){
          HPVMFatalError("Loop does not have unique exiting block\n");
      }

      BasicBlock* LoopEntryBlock = myLoop->getLoopPredecessor();
      if(!LoopEntryBlock){
          HPVMFatalError("Loop does not have unique entry block\n");
      }

      LLVM_DEBUG(errs()<<"Loop entry block: "<<*LoopEntryBlock<<"\n");
      LLVM_DEBUG(errs()<<"Loop exit block: "<<*LoopExitBlock<<"\n");


      Instruction* BeginBound = myLoop->getHeader()->getFirstNonPHI();

      Instruction *EndI = LoopExitBlock->getTerminator()->getPrevNonDebugInstruction();
      LLVM_DEBUG(errs()<<"Loop marker Beginning inst: "<<*BeginI<<"\nEnding inst:\t"<<*EndI<<"\n");
      // errs()<<"Loop marker Beginning inst: "<<*BeginBound<<"\nEnding inst:\t"<<*EndI<<"\n";


      Bounds.push_back(Region<Instruction>(BeginI, EndI));

    }
    return Bounds;
  };

  vector<Region<Instruction>> ParallelSectionBounds, TaskBounds;
  if (ParSectionBeginMarker)
    ParallelSectionBounds = findBounds(ParSectionBeginMarker, ParSectionEndMarker);
  if (TaskBeginVarMarker)
    TaskBounds = findBounds(TaskBeginVarMarker, TaskEndMarker);

  if(LoopBeginMarker){
      auto LoopTaskBounds = findLoopBounds(LoopBeginMarker);
      TaskBounds.insert(TaskBounds.end(), LoopTaskBounds.begin(), LoopTaskBounds.end());
  }

  // Check for proper nesting of all begin/end statements
  vector<Region<Instruction>> AllBounds;
  AllBounds.insert(AllBounds.end(), ParallelSectionBounds.begin(), ParallelSectionBounds.end());
  AllBounds.insert(AllBounds.end(), TaskBounds.begin(), TaskBounds.end());
  if (!isCorrectlyNested(AllBounds)) {
    warnError("Begin and end marker functions are not correctly nested!");
  }

  Data = std::make_pair(ParallelSectionBounds, TaskBounds);
}

void HPVMProgram::getTaskInformation(
  const Section<Region<Instruction>> &S, Task<Region<Instruction>> &T, Instruction* TaskMarker)
{
  auto *MarkerCall = dyn_cast<CallInst>(/*T.Location.Begin*/ TaskMarker);

  const auto Inputs = 0, Outputs = 1, OptionalName = 2;
  auto State = Inputs - 1;
  auto NumRemaining = 0L;

  // Input/Output pointer for buffer may first bitcast
  // and then be used. Use bitcasted instruction
  auto getBufferBitCast = [](Value* I){
      Value* BitCasted = I;

      if(isa<Argument>(I))
          return I;

      for(auto *User : I->users()){
          Instruction* UseInst = dyn_cast<BitCastInst>(User);

          if(!UseInst) continue;
          BitCasted = UseInst;
          break;
      }
      return BitCasted;
  };

  auto Arg = MarkerCall->arg_begin();

  if(isParallelLoopMarker(MarkerCall)){
      // First argument is enclosing loop number
      Arg++;
  }

  
  for (; Arg != MarkerCall->arg_end(); ++Arg) {
    if (NumRemaining == 0) {
      
      State++;
        
        // OptionalName argument provided
        // for the node function this task
        // will make.
        if(State == OptionalName ){

            Value* ArgVal = Arg->get();
            LLVM_DEBUG(errs() << "OptionalNameArg: "<<*ArgVal<<" "<<*ArgVal->getType()<<"\n");
            ConstantExpr* StrGep = dyn_cast<ConstantExpr>(ArgVal);

            if(!StrGep) HPVMFatalError("Unsupported argument type for node function name");


            // Various checks on optional name
            GlobalVariable* Str = dyn_cast<GlobalVariable>(StrGep->getOperand(0));
            
            //if(!Str) HPVMFatalError("Optional name argument must be of string type");
            ConstantDataArray* CDA = dyn_cast<ConstantDataArray>(Str->getInitializer());

            if(!CDA) HPVMFatalError("Optional name argument must be a CONSTANT string");

            T.Name = CDA->getAsCString();

            LLVM_DEBUG(errs()<< "Task name: "<<T.Name<<"\n");
            State++;
            continue;
        }

      ConstantInt *NumArgs;
      if (!(NumArgs = dyn_cast<ConstantInt>(&*Arg))) {
        fatalError("Expected number of arguments as const int in marker function call");
      }

      NumRemaining = NumArgs->getSExtValue();
      

      // If 0 arguments of type State is used
      if(NumRemaining == 0) continue;
      else  Arg++;
    }


    switch (State) {
      case Inputs: {
        auto Param = BufferParameter();
        Param.Buffer = getBufferBitCast((Arg)->get());

        // If the current type of value is a pointer
        // type, the next argument must be it's size
        if(Param.Buffer->getType()->isPointerTy()){
            Arg++;
            Param.Size = Arg->get();
        } else { // If the current value is a Scalar LLVM Value (e.g int, float, etc)
           Param.Size = nullptr; 
        }

        T.Inputs.push_back(Param);
        break;
      }
      case Outputs: {
        auto Param = BufferParameter();
        Param.Buffer = getBufferBitCast((Arg)->get());

        // If the current type of value is a pointer
        // type, the next argument must be it's size
        if(Param.Buffer->getType()->isPointerTy()){
            Arg++;
            Param.Size = Arg->get();
        } else { // If the current value is a Scalar LLVM Value (e.g int, float, etc)
           Param.Size = nullptr; 
        }
        T.Outputs.push_back(Param);
        break;
      }
    }

    NumRemaining--;
  }

  if (NumRemaining != 0) {
    string StateStr;
    switch (State) {
      case Inputs: StateStr = "inputs"; break;
      case Outputs: StateStr = "outputs"; break;
      case OptionalName: StateStr = "name";break;
    }
    fatalError("Incorrect number of arguments specified in marker function for " + StateStr);
  }
}

void HPVMProgram::getTaskDependency(
        const Section<Region<Instruction>> &S){
    vector<Task<Region<Instruction>>*> sortedTasks;

    // Sort Tasks in dominance order using Dominator Tree
    auto cmpTasks = [&](Task<Region<Instruction>>* t1, Task<Region<Instruction>>* t2) -> bool
    {
        return DTCache.dominates(t1->Location.Begin,  t2->Location.Begin);
    };

    for(auto &CandidateTask: S.Tasks){
        sortedTasks.push_back(CandidateTask.get());
    }

    sort(sortedTasks, cmpTasks);





    // Task t2 is dependent on t1 if t2's input buffer is the same as t1's output 
    // buffer and t1 dominates t2 (within the same parallel section) 
    auto isDep = [&](Task<Region<Instruction>>* curTask, Task<Region<Instruction>>* prevTask, std::set<Value*>& visited ) 
        -> vector<unsigned> // returns a vector of indices corresponding __hpvm__return indices 
    {
        vector<unsigned> DepPair;
        
        for(auto &buffs : curTask->Inputs){
            // Offset refers to the number of LLVM Values preceeding
            // the current BufferParameter
            unsigned offset = 0;
            for(unsigned j = 0; j < prevTask->Outputs.size(); j++){
                auto pbuffs = prevTask->Outputs[j];
                if(buffs.Buffer == pbuffs.Buffer && visited.find(buffs.Buffer) == visited.end()){
                    visited.insert(buffs.Buffer);


                    DepPair.push_back(offset);

                    
                    // If the current Parameter is a pointer
                    // followed by its size
                    if(pbuffs.Size && !NoRetSize){
                        DepPair.push_back(offset+1);
                    }
                }

                if(!NoRetSize){
                    // Increment offset by the current paramters size (i.e. either 1 or 2)
                    offset += pbuffs.size();
                } else {
                    offset += 1;
                }
            }
        }
        return DepPair;
    };


    for(auto it = sortedTasks.rbegin(); it != sortedTasks.rend(); ++it){
        auto CurTask = *it;

        // Input buffer may be coming from 
        // multiple tasks, so we only consider
        // the closest preceeding task per that value
        std::set<Value *> Accounted; 
        for(auto ri = it; ri != sortedTasks.rend(); ++ri){
            if(ri == it)
                continue;
            auto PrevTask = *ri;

            auto Deps = isDep(CurTask,PrevTask, Accounted);

            if(Deps.size()){
                
                for(auto k: Deps){
                    CurTask->Dependencies.push_back(std::make_pair(PrevTask,k));
                    PrevTask->RevDependencies.push_back(std::make_pair(CurTask,k));
                }
                LLVM_DEBUG(dbgs() << "Task "<<*(CurTask->Location.Begin) << " depends on task "<< *(PrevTask->Location.Begin)<<"\n");
                
            }


        }
    }
    
}


vector<Section<Region<Instruction>>> HPVMProgram::constructSections(
  const pair<vector<Region<Instruction>>, vector<Region<Instruction>>> &Bounds)
{
  auto &ParallelSectionBounds = Bounds.first;
  auto &TaskBounds = Bounds.second;

  // Helper that maps begin marker function calls to their corresponding bound
  map<const Instruction*, const Region<Instruction>*> BeginIToTaskBound;
  for (const auto &Bound : TaskBounds) {
    auto BeginI = Bound.Begin;


    CallInst* BeginCall = dyn_cast<CallInst>(BeginI);

    BeginIToTaskBound[BeginI] = &Bound;
    
  }

  
  vector<Section<Region<Instruction>>> Sections;
  unsigned RemainingTaskBounds = TaskBounds.size();

  for (auto &SectionBounds : ParallelSectionBounds) {
    // Create a new section for this set of bounds
    Sections.emplace_back(dyn_cast<CallInst>(SectionBounds.Begin), dyn_cast<CallInst>(SectionBounds.End));
    auto &CurSection = Sections.back();
    
    std::set<BasicBlock*> Visited;
    vector<BasicBlock*> ToVisit; 
    auto FinalBlock = SectionBounds.End->getParent();
    Function* SectionParent = SectionBounds.End->getParent()->getParent();

    ReversePostOrderTraversal<Function *> RPOT(SectionParent);
    auto RPOBB = RPOT.begin();
    auto RPOBBEnd = RPOT.end();

    Instruction* StopPoint = nullptr;

    auto getNextInst = [&] (Instruction* I) -> Instruction* {
        // In the case where we skip to the
        // ending bounds on the current task
        while(I->getParent() != *RPOBB && RPOBB != RPOBBEnd){
            // StopPoint = nullptr;
            RPOBB++;
        }

        BasicBlock* NextB = nullptr;
        // Must update RPOBB to next basic block
        if(StopPoint == nullptr){
            // special for starting case
            if(RPOBB != RPOT.begin()){
                // Move to next basic block in
                // traversal
                RPOBB++;
            } 
            NextB = *RPOBB;
            StopPoint = NextB->getFirstNonPHI();
        } else {
            StopPoint = StopPoint->getNextNonDebugInstruction();
        }  

        while(!StopPoint /*|| isa<BranchInst>(StopPoint)*/ || isa<UnreachableInst>(StopPoint)){
            RPOBB++;
            NextB = *RPOBB;
            StopPoint = NextB->getFirstNonPHI();
        }

        return StopPoint;
    };


    
    // Parallel Sections may contain Parallel Tasks or Parallel Loop. Before reaching 
    // the start to any of these begin markers, there can be no call to any function.
    // We traverse the control flow assuming conditional code flow. For loops we ensure that
    // the Section End block is visited after all blocks are visited.
    auto CurInst = SectionBounds.Begin;
    StopPoint = CurInst;
    
    LLVM_DEBUG(errs()<<"Begin Traversal for SectionBounds.Begin "<<*SectionBounds.Begin<<"\n");
    Instruction *NextI;
    while ((NextI = getNextInst(CurInst)) != SectionBounds.End) {
      LLVM_DEBUG(errs()<<"NextI: "<<*NextI<<"\n");
      // Make sure this instruction is a call to task_begin
      if (BeginIToTaskBound.find(NextI) == BeginIToTaskBound.end()) {
        //HPVMWarnError("Parallel section contains non-task code");
      }


      auto CI = dyn_cast<CallInst>(NextI);
      // Possibly zext, bitcast, etc.
      if(!CI){
          CurInst = NextI;
          continue;
      }


      Function* Callee = CI->getCalledFunction();




      // Skip functions which start with llvm.
      if(Callee && Callee->isIntrinsic()){
          CurInst = NextI;
          continue;
      }

      // TODO: If Loop Bounds are a result of a function call, this would
      // fail.
      if(!Callee || !(isParallelLoopMarker(CI) ||
                  isTaskBeginMarker(CI))){
          LLVM_DEBUG(errs()<<*CI<<"\n");
          LLVM_DEBUG(errs()<<"SectionBounds.Begin: "<<*SectionBounds.Begin<<"\n");
          HPVMFatalError("Parallel Section contains Function call outside tasks or loop");
      }

      bool isLoopType = isParallelLoopMarker(CI); 
      int num_parallel_loops = 0;

      
      assert(BeginIToTaskBound.find(NextI) != BeginIToTaskBound.end() && "Unable to find corresponding task bound");

      auto TaskBound = *BeginIToTaskBound[NextI];



      unique_ptr<Task<Region<Instruction>>> CurTask;
      if(isLoopType){
          CurTask = std::make_unique<LoopTask<Region<Instruction>>>(TaskBound, CI);
          ConstantInt* NumLoops = dyn_cast<ConstantInt>(CI->getArgOperand(0));
          assert(NumLoops && "First operand of parallel loop marker must be a constant integer");
          CurTask->NumParallelEnclosing = NumLoops->getSExtValue(); 
          num_parallel_loops = NumLoops->getSExtValue();
          LLVM_DEBUG(errs()<<"Parallel loop has "<<CurTask->NumParallelEnclosing<<" loops...\n");
      } else {
          CurTask = std::make_unique<SeqTask<Region<Instruction>>>(TaskBound, CI);
      }


      ValueToValueMapTy VMap;
      ValueToValueMapTy RevVMap;
      Instruction* Ahead = NextI->getNextNonDebugInstruction();

      // Copy in conditional condition calculation inside task
      if(BranchInst* Branch = dyn_cast<BranchInst>(Ahead)){
          LLVM_DEBUG(errs()<<"AHEAD FOR BEGIN\n");
          if(!isLoopType && Branch->isConditional()){
              LLVM_DEBUG(errs()<<"Ahead "<<*Branch<<" is a conditional branch\n");
              copyGEPCalculationInTask(Ahead , TaskBound, VMap, RevVMap, CI);
            }
      }


      // Check if Current task is an Internal Node if it's next instruction is 
      // start of a parallel section OR if it contains a call to a function
      // which contains a Dataflow graph.
      CurInst = getNextInst(NextI);
      CallInst *Call = dyn_cast<CallInst>(CurInst);

      bool isInternal = false;
      if(Call && 
              isParSectionBeginMarker(Call) 
                ){
          CurTask->Type = TaskType::INTERNAL_DIRECT;
          isInternal = true;
      } else if (Call && 
              isHPVMDataFlowGraph(Call->getCalledFunction()) ){
          CurTask->Type = TaskType::INTERNAL_ACROSS;
          isInternal = true;

      }
      

      // Extract the dependencies, inputs, and outputs from the begin marker
      getTaskInformation(CurSection, *CurTask, NextI);

      // Mark this task as belonging to this parallel section
      CurSection.Tasks.push_back(std::move(CurTask));
      RemainingTaskBounds--;



      // Iterate over leaf nodes and identify if any GEP definition is defined 
      // outside current region. If so copy the instructions inside current region
      // before extraction.
      while(!isInternal && CurInst != TaskBound.End
              && !isa<ReturnInst>(CurInst)) {

        copyGEPCalculationInTask(CurInst , TaskBound, VMap, RevVMap, CI);


        
        CurInst = getNextInst(CurInst);
        LLVM_DEBUG(errs()<<"copyGep CurInst: "<<*CurInst<<", with task bound end: "<<*TaskBound.End<<"\n");
      }

      if(isLoopType && !isInternal){
        copyGEPCalculationInTask(CurInst , TaskBound, VMap, RevVMap, CI);
        /* clear preheader */
        // clearLoopPreheader(CurInst);
        updateLoopUses(CurInst, RevVMap);

        
        /* Loop TripCount should exist outside of loop
         * so the createNodeND call can use the value as the limit*/
        replaceLoopTripCount(CurInst, VMap, RevVMap, num_parallel_loops);

      }


      
      LLVM_DEBUG(dbgs()<<"Done Copying GEP\n");
      LLVM_DEBUG(errs()<<"Task was internal? "<<isInternal<<"\n");
      LLVM_DEBUG(errs()<<*CurInst->getParent()->getParent()<<"\n");

      Visited.insert(TaskBound.Begin->getParent());
      // Jump ahead to the corresponding task_end
      CurInst = TaskBound.End;
      LLVM_DEBUG(errs()<<"CurInst: "<<*CurInst<<"\n");
      
      //Need when even after skipping we're part
      //of the same basic block
      StopPoint = CurInst;

    }

    getTaskDependency(CurSection);
    CurSection.isOuterMostSec = isOuterMostParallelSection(SectionBounds.Begin);
  }

  if (RemainingTaskBounds != 0) {
    fatalError("Not all tasks contained within parallel sections");
  }

  return Sections;
}


bool HPVMProgram::isOuterMostParallelSection(Instruction* II){
    CallInst* SecBeginCall = dyn_cast<CallInst>(II);
    Function* SecBeginF = M.getFunction(ParSectionBeginMarkerFuncName);
    assert(SecBeginCall 
            && (SecBeginCall->getCalledFunction() == SecBeginF)
            && "Instruction must be a Parallel Section Begin");

    Function* IIParentF = II->getParent()->getParent();

    for(auto* User: SecBeginF->users()){
        CallInst* U = dyn_cast<CallInst>(User);
        
        if(!U) continue;

        if(U == SecBeginCall) continue;

        // Must be from the same Parent Function
        if(U->getParent()->getParent() != IIParentF) continue;


        if(DTCache.dominates(U,II))
            return false;
    }


    return true;
}

vector<Section<Region<BasicBlock>>> HPVMProgram::splitMarkerBoundary(
  const vector<Section<Region<Instruction>>> &Sections)
{
  // Split basic blocks at marker function boundaries and return the newly
  // formed Region of BasicBlocks
  auto splitBlocks = std::function<Region<BasicBlock>(Region<Instruction>)>(
    [&] (Region<Instruction> Bound)
  {
    auto CI = dyn_cast<CallInst>(Bound.Begin);

    // If Task is a Loop then leave code splitting to code extractor
    if(CI && isParallelLoopMarker(CI))    
    {
        return Region<BasicBlock>(Bound.Begin->getParent(), Bound.End->getParent());
    }
    auto BeginBB = Bound.Begin->getParent();
    auto NewBeginBB = BeginBB->splitBasicBlock(Bound.Begin);

    auto EndBB = Bound.End->getParent();
    EndBB->splitBasicBlock(Bound.End);

    // Split this basic block again to create a "boundary" block
    auto NewEndBB = Bound.End->getParent();
    Bound.End->getParent()->splitBasicBlock(Bound.End);

    // Reset the DTCache for this function
    DTCache.reset(Bound.Begin->getParent()->getParent());

    LLVM_DEBUG(errs()<<"BeginBB: "<<*NewBeginBB<<"\n");
    LLVM_DEBUG(errs()<<"EndBB: "<<*NewEndBB<<"\n");


    LLVM_DEBUG(errs()<<*NewEndBB->getParent()<<"\n");

    // Remove the marker functions
    Bound.End->eraseFromParent();

    Bound.Begin->replaceAllUsesWith(UndefValue::get(Bound.Begin->getType()));

    
    
    


    return Region<BasicBlock>(NewBeginBB, NewEndBB);
  });

  vector<Section<Region<BasicBlock>>> Output;
  for (auto &OldSection : Sections) {
    Output.push_back(Section<Region<BasicBlock>>(OldSection, splitBlocks));
  }

  
  return Output;
}

vector<Section<CallInst*>> HPVMProgram::extractSectionsAndTasks(
  const vector<Section<Region<BasicBlock>>> &Sections)
{

  HPVMCGenContext cgenContext(M);
  HPVMCGenAttributes cgenAtt(cgenContext);
  HPVMCGenReturn cgenRet(cgenContext);
  HPVMCGenBindOut cgenBindOut(cgenContext);
  HPVMCGenCreateNodeND cgenCreateNodeND(cgenContext);



  // Helper Functions For Maintaining mappings to
  // __hpvm__return and __hpvm__attribute calls. 
  map<Region<BasicBlock>,CallInst*> retMap;
  map<Region<BasicBlock>,CallInst*> attMap;


  // TODO: Replace templatized Task transformation code to operate
  // on Task<LocationType> instead of LocationType to utilize
  // task information.


  auto insertVec = [&] (vector<Value*>& vec, Value* V){
      // Insert if V is non-null and not already in vec
      if(V && std::find(vec.begin(), vec.end(), V) == vec.end()){
          vec.push_back(V);

      }
  };

  // Inserting __hpvm__attributes and __hpvm__return calls for Parallel
  // Sections. Note that the __hpvm__return call is temporary and is used
  // to faciliate function cloning. 
  //
  // Attributes for Parallel Section:
  //    Any Pointer Argument part of a Section's inner Tasks __hpvm__attribute
  //    call would be considered a in argument for __hpvm__attributes.
  //
  //    For out attributes, any pointer argument which is part of a tasks
  //    __hpvm__return call AND has no outgoing edge towards another task
  //    will be considered an out argument. This can be infered if a particular 
  //    bindout index is not part of a Tasks RevDep map.
  auto insertSecAttributesAndReturn = [&] (const Section<Region<BasicBlock>>& s){
      if(!s.isOuterMostSec){
          return;
      }

      std::set<Value*> AttIn;
      std::set<Value*> AttOut;
      
      vector<Value*> ReturnVals;

      // As Parallel Section's do not
      // have any arguments, we can't infer
      // ordering dependencies. Hence
      // we take the union of the contained
      // tasks as a proxy
      vector<Value*> Ordering;



      for(auto &Task: s.Tasks){
          auto t = Task.get();


          for(auto &ibuff : t->Inputs){
              if(ibuff.Buffer->getType()->isPointerTy()){
                AttIn.insert(ibuff.Buffer);
              }

              insertVec(Ordering,ibuff.Buffer);
              insertVec(Ordering, ibuff.Size);
          }

          unsigned ptrIdx = 0;
          for (unsigned i = 0; i < t->Outputs.size(); i++){

              bool isOut = true; 
              for (auto RevDepPair: t->RevDependencies){
                  if(RevDepPair.second == ptrIdx){
                      isOut = false;
                      break;
                  }

              }


              if(!NoRetSize)
                ptrIdx += t->Outputs[i].size();
              else
                ptrIdx += 1;

              if(!isOut) continue;

              if(t->Outputs[i].Buffer->getType()->isPointerTy())
                AttOut.insert(t->Outputs[i].Buffer);

              insertVec(ReturnVals,t->Outputs[i].Buffer);
              if(!NoRetSize)
                insertVec(ReturnVals, t->Outputs[i].Size);


              insertVec(Ordering,t->Outputs[i].Buffer);
              insertVec(Ordering, t->Outputs[i].Size);


          }

      }

      vector<Value*> AttInVec;
      vector<Value*> AttOutVec;

      
      for (auto V: AttIn){
          AttInVec.push_back(V);
      }


      for (auto V: AttOut){
          AttOutVec.push_back(V);
      }



      auto SecAtt = cgenAtt.insertCall(s.BeginMarker->getNextNode(), AttInVec, AttOutVec, vector<Value*>());


  };


  // Insert __hpvm__return and __hpvm__attributes prior to extraction
  auto insertAttributesAndReturn = [&](Task<Region<BasicBlock>>* t){
      // Input and Outputs for __hpvm__attributes 
      vector<Value*> Inputs;
      vector<Value*> Outputs;



      // List of Value* corresponding to Pointers and their sizes.
      // Used for creating __hpvm_return and __hpvm__bindout calls.
      vector<Value*> OutWithSizes;


      vector<Value*> PrivArguments;

      
      // Used for sorting Argument ordering when
      // cloning the extraction function to follow
      // (ptr1, ptr1Sz, ptr2, ptr2Sz, ...) ordering
      // for HPVM backends.
      //vector<Value*> PtrPairs;

      for(auto &ibuff : t->Inputs){
          assert(ibuff.Buffer && "Buffer value must be non-null");
          LLVM_DEBUG(errs()<<"Input buffer: "<<*ibuff.Buffer<<"\n");
          if(ibuff.Buffer->getType()->isPointerTy())
            Inputs.push_back(ibuff.Buffer);


      }
      for(auto &obuff : t->Outputs){
          if(obuff.Buffer->getType()->isPointerTy())
            Outputs.push_back(obuff.Buffer);

          OutWithSizes.push_back(obuff.Buffer); 

          // If the obuff is a Ptr+PtrSz
          if(obuff.Size && !NoRetSize)
            OutWithSizes.push_back(obuff.Size);




      }

      // If the task corresponds to a leaf node,
      // check if __hpvm__priv call is there
      // and populate the PrivArguments
      // vector.
      if(!t->isInternalNode()){
          Region<BasicBlock>& TaskRegion = t->Location;

          vector<Instruction*> ToErase;
          for(auto &BB : *TaskRegion.Begin->getParent()){
            if((DTCache.dominates(TaskRegion.Begin, &BB) && DTCache.postDominates(TaskRegion.End, &BB))||
                    (DTCache.dominates(TaskRegion.Begin, &BB) && BBPrecedesEnd(&BB, TaskRegion.Begin, TaskRegion.End))){
                for(auto &I : BB){
                    if(CallInst* CI = dyn_cast<CallInst>(&I)){
                        Function* CF = CI->getCalledFunction();

                        // Found a priv call for leaf node
                        if(CF && isPrivCall(CI)){
                            getHPVMPrivArgs(CI, PrivArguments);
                            ToErase.push_back(CI);
                        }


                        if(CF && isNonZeroCall(CI)){
                            if(CF->getName().str() != NonZeroMarkerFuncName){
                                CF->setName(NonZeroMarkerFuncName);
                            }
                        }

                    }

                }

            }
          }

          for(auto* EI : ToErase){
              EI->eraseFromParent();
          }


      }


      t->AttributeCall = cgenAtt.insertCall(t->Location.Begin->getFirstNonPHI(),Inputs, Outputs, PrivArguments);

      // Insert output buffer sizes into an HPVM return or Bind Out call
      // At this stage, regardless of whether the DFG Node is an Internal Node
      // or Leaf Node, we insert __hpvm__returns. This aids later stages in identifying
      // task dependencies. Later on internal nodes replace these return calls with
      // corresponding bind outs.
      t->ReturnCalls.push_back(cgenRet.insertCall(t->Location.End->getTerminator(),OutWithSizes));

      retMap[t->Location] = t->ReturnCalls.back();
      attMap[t->Location] = t->AttributeCall;

    };

  for(auto &Section : Sections){
    for(auto &CandidateTask: Section.Tasks){
      insertAttributesAndReturn(CandidateTask.get());
    }
    insertSecAttributesAndReturn(Section);
  }


  // Get the list of exiting basic blocks for an extracted
  // function.
  auto getReturnBlocks = [&](Function* F) -> vector<BasicBlock*> {
      vector<BasicBlock*> exits;

      for(auto bi = F->begin(), be = F->end(); bi != be; bi++){
          BasicBlock* Block = &*bi;

          if(isa<ReturnInst>(Block->getTerminator())){
              exits.push_back(Block);
          }
      }

      return exits;

  };

  // Propgate __hpvm_return call to possibly multiple returning 
  // blocks in an extracted function.
  auto propogateReturns = [&](Function* F, CallInst* RetInst){

      if(!F || !RetInst) return;

      vector<BasicBlock*> ExitBlocks = getReturnBlocks(F);
      for(auto BB : ExitBlocks){
          CallInst::Create(RetInst,{}, BB->getTerminator());
      }

      RetInst->eraseFromParent();

  }; 




  // Returns a vector of all the BBs contained within a Region<BasicBlock>
  auto getRegionBBs = [&] (Region<BasicBlock> R) -> vector<BasicBlock*> {
    vector<BasicBlock*> BBs;

    // Bruteforce to find all BBs dominated by begin and post-dominated by end
    for (auto &BB : *R.Begin->getParent()) {
      if((DTCache.dominates(R.Begin, &BB) && DTCache.postDominates(R.End, &BB))||
              (DTCache.dominates(R.Begin, &BB) && BBPrecedesEnd(&BB, R.Begin, R.End)))
        BBs.push_back(&BB);
    }

    return BBs;
  };

  // Get the CallInst operand Index for Value V.
  auto getValueIndexCI = [&](CallInst* CI, Value* V)-> int {
        int ici = 0;
        for(; ici <CI->getNumArgOperands(); ici++){
            Value* valArg = CI->getArgOperand(ici);

            if(valArg == V){
                return ici;
            }
        }
        HPVMFatalError("Value isnt part of Function Call");
        return ici;
    };

  auto mapCIValueToFormalArg = [&] (Value* V, CallInst* CI) -> Argument* {
      Argument* FormalArg = nullptr;

      if(!CI->hasArgument(V)){
          LLVM_DEBUG(dbgs()<<*CI<<" does not have "<<*V<<"as it's argument...\n");
          return FormalArg;
      }

      int idx = getValueIndexCI(CI,V);
      LLVM_DEBUG(dbgs()<<*CI<<" does have have "<<*V<<"as it's argument at idx"<<idx <<"\n");

      Function* F = CI->getCalledFunction();

      return dyn_cast<Argument>((F->arg_begin() + idx));

  };


  


  map<CallInst*, CallInst*> attCall;
  map<CallInst*, CallInst*> retCall;

  // Extracts the BBs contained within the region into a function and replaces
  // the BBs with a call to that function, which is returned
  auto extractBBs = std::function<CallInst*(Region<BasicBlock>)>(
    [&] (Region<BasicBlock> Bounds)
  {
    // Get the BBs contained within the bounds
    auto ParentF = Bounds.Begin->getParent();
    auto RegionBBs = getRegionBBs(Bounds);

    DTCache.reset(ParentF);


    LLVM_DEBUG(errs()<<*ParentF<<"\n");

    LLVM_DEBUG(errs()<<"Starting Bound:"<<*Bounds.Begin<<"\nEnding Bounds: "<<*Bounds.End<<"\n");

    // Extract the basic blocks into their own function
    auto CE = CodeExtractor(RegionBBs, &DTCache.get(ParentF).DT, 
            /* Aggregate Args = */ false, /* BFI */ nullptr, nullptr, nullptr, /* AllowVarArgs */ false,
            /* Allow Alloca = */ true);
    if (!CE.isEligible())
      fatalError("Failed to extract section or task!");

    CodeExtractorAnalysisCache  CEAC(*ParentF);
    auto ExtractedF = CE.extractCodeRegion(CEAC);
    DTCache.reset(ParentF);

    CallInst* NewCall = dyn_cast<CallInst>(ExtractedF->user_back());

    if(attMap.find(Bounds) != attMap.end()){
        CallInst* Attribute = attMap[Bounds];
        CallInst* Return = retMap[Bounds];

        attCall[NewCall] = Attribute;
        retCall[NewCall] = Return;
    }

    CallInst* CleanedCall = NewCall;
    Function* NewF = ExtractedF;

    LLVM_DEBUG(errs()<<"Extracted Function: "<<*NewF<<"\n");


    // Return the CallInst that calls the new function
    return CleanedCall;
  });

  // Identify Loop Call Marker and call appropriate extractor
  auto extractLoops = std::function<CallInst*(Region<BasicBlock>)>(
          [&] (Region<BasicBlock> Bounds){

          CallInst* RetInst = nullptr;

          for(auto bi = Bounds.Begin->begin(), be = Bounds.Begin->end(); 
                  bi != be; ){
            Instruction* II = &*bi;
            bi++;
            if(CallInst* CI = dyn_cast<CallInst>(II)){
                if(isParallelLoopMarker(CI)){
                    RetInst =  extractLoopContainingInst(cgenContext, *CI);
                    break;
                    
                }

            }
          }

          assert(RetInst && "Unable to find loop marker");
          return RetInst;

   });


  // For Internal Node Task's which contain calls to HPVMDFGNodes h,
  // replace the indirect call to the h's parent with a call to h.

  vector<int> indices;
  for(auto i = 0;i< Sections.size(); i++){
      indices.push_back(i);
  }


  auto cmpSectionsRev = [&](int s1, int s2) -> bool {

     auto& S1 = Sections[s1];
     auto& S2 = Sections[s2];

     Function* S1Caller = S1.BeginMarker->getParent()->getParent();
     Function* S2Caller = S2.BeginMarker->getParent()->getParent();

     if(S1Caller != S2Caller) return false;

     CallInst* S1Marker = S1.BeginMarker;
     CallInst* S2Marker = S2.BeginMarker;

     // Same function, then we check if either dominates the other

     auto &DT = DTCache.get(S1Caller).DT;

     if(DT.dominates(S1Marker, S2Marker)){
         return false;
     } else if (DT.dominates(S2Marker, S1Marker)) {
         return true;
     }

     return false;

  };

  sort(indices, cmpSectionsRev);


  vector<Section<CallInst*>> Output;

  for(auto i : indices){
    auto &CurSection = Sections[i];
    Output.push_back(Section<CallInst*>(CurSection, extractBBs, extractLoops));
  }

  // For Internal Node Task's which contain calls to HPVMDFGNodes h,
  // replace the indirect call to the h's parent with a call to h.
  for(auto &Section : Output){
    for(auto &CandidateTask: Section.Tasks){

        if(!CandidateTask->isInternalNode()) continue;

        Function* TaskFn = CandidateTask->Location->getCalledFunction();
    }
  }

  auto getInternalNodeCalls = [&](CallInst* CI, std::set<Function*>& Vec){
      Function* F = CI->getCalledFunction();
      for(auto &BB: *F){
          for(auto &II: BB){
              CallInst* C = dyn_cast<CallInst>(&II);
              if(!C) continue;

              Function* CalledFunc = C->getCalledFunction();
              if(!CalledFunc || isParSectionBeginMarker(C) ||
                      isParSectionEndMarker(C)) continue;
              if(C) Vec.insert(C->getCalledFunction());
          }
      }
  };



  auto getCallsForFunction = [&](Function* F) -> vector<CallInst*> {
    vector<CallInst*> calls;

    for(auto* User: F->users()){
        CallInst* call = dyn_cast<CallInst>(User);

        if(call){
            calls.push_back(call);
        }
    }

    return calls;

  };


  auto getCallsInFunction = [&](Function* F) -> vector<CallInst*> {
    vector<CallInst*> calls;

      for(auto &BB: *F){
          for(auto &II: BB){

              CallInst* C = dyn_cast<CallInst>(&II);
              if(!C) continue;
              calls.push_back(C);
          }
      }


    return calls;

  };

  auto cmpSections = [&](int s1, int s2) -> bool {

      Section<CallInst*>& S1 = Output[s1];
      Section<CallInst*>& S2 = Output[s2];


      std::set<Function*> InternalCallsS1;
      std::set<Function*> InternalCallsS2;

      for(auto &Task1 : S1.Tasks){
          if(Task1->isInternalNode()){
              getInternalNodeCalls(Task1->Location,InternalCallsS1);
          }
      }


     for(auto &Task2 : S2.Tasks){
          if(Task2->isInternalNode()){
              getInternalNodeCalls(Task2->Location, InternalCallsS2);
          }
     }

     Function* S1Caller = S1.BeginMarker->getParent()->getParent();
     Function* S2Caller = S2.BeginMarker->getParent()->getParent();

     LLVM_DEBUG(errs() << " CmpSection on "<< *S1.BeginMarker << " and " << *S2.BeginMarker<<"\n");
     LLVM_DEBUG(errs() << " CmpSection parents "<< S1Caller->getName() << " and " << S2Caller->getName()<<"\n");


     vector<CallInst*> S1CallerCalls = getCallsInFunction(S1Caller);
     vector<CallInst*> S2CallerCalls = getCallsInFunction(S2Caller);

     for(CallInst* S1C : S1CallerCalls){
         if(S1C->getCalledFunction() == S2Caller){

             LLVM_DEBUG(errs() << "Functions called by S1's Caller include S2's parent\n");
             return true;
         }
     }


     for(CallInst* S2C : S2CallerCalls){
         if(S2C->getCalledFunction() == S1Caller){


             LLVM_DEBUG(errs() << "Functions called by S2's Caller include S1's parent\n");
             return false;
         }
     }

    LLVM_DEBUG(errs() << "Neither case\n");

     for(Function* C : InternalCallsS1){
         LLVM_DEBUG(errs()<< "S1 Calls:\t"<<C->getName() <<"\n");
     }


     for(Function* C : InternalCallsS2){
         LLVM_DEBUG(errs()<< "S2 Calls:\t"<<C->getName() <<"\n");
     }

     LLVM_DEBUG(errs() << "Module\n: "<< (*S1Caller->getParent())<<"\n END\n");


     // If Parallel Section S2 was initially defined inside one of the
     // Task boundaries for children of S1.
     if(InternalCallsS1.find(S2Caller/*S2.Location->getCalledFunction()*/) != InternalCallsS1.end()){
         LLVM_DEBUG(errs() << "S1 internal calls contain S2's parent\n");
         return true; 
     }

     // If one of the tasks within S1 contains a call to the caller
     // function of Parallel Section S2. This would occur if
     // user has specified aa HPVM DFG inside another function.
    
     // Similar logic for reverse pair.
    if(InternalCallsS2.find(S1Caller/*S1.Location->getCalledFunction()*/) != InternalCallsS2.end()){
         LLVM_DEBUG(errs() << "S2 internal calls contain S1's parent\n");
         return false;
    }



    LLVM_DEBUG(errs() << "Fall Through Case\n");
    

    return true;


  };

  sort(indices, cmpSections);


  // Delete any pre-cloned function which
  // is no longer needed. TODO: DCE removes this?
  vector<Function*> RemoveOrigFunctions;





  auto NOP = std::function<CallInst*(CallInst*)>(
          [&] (CallInst* LoopCall){

          LLVM_DEBUG(errs () << "Invoking NOP \n");

          map<Function*, CallInst*> ParentToCreateNodeND;

          Function* LF = LoopCall->getCalledFunction();
          CallInst* ClonedCall = LoopCall;
          for(auto * Users: LF->users()){
            CallInst* CI = dyn_cast<CallInst>(Users);

            if(!CI) continue;

            // Skip any createNodeND call
            if(isCreateNodeCall(CI) ){
                ParentToCreateNodeND[CI->getParent()->getParent()] = CI;
                continue;
            }

            Function* ParentF = CI->getParent()->getParent();

            if(ClonedFunctions.find(ParentF) != ClonedFunctions.end()){
                ClonedCall = CI;
            }
          }

          if(ParentToCreateNodeND.find(ClonedCall->getParent()->getParent()) == ParentToCreateNodeND.end()){
              LLVM_DEBUG(errs()<<"Parent: "<<*ClonedCall->getParent()->getParent()<<"\n");
              HPVMFatalError("Loop Task createNodeCall not found");
          }
          CallInst* CreateNodeCall = ParentToCreateNodeND[ClonedCall->getParent()->getParent()];
          CallToNode[ClonedCall] = CreateNodeCall;

          LLVM_DEBUG(errs() << "Returning NOP loop call: "<<(*ClonedCall)<< "in parent "<< (*ClonedCall->getParent()->getParent())<<"\n");

          return ClonedCall;

          });

  auto ReorderArguments = std::function<CallInst*(CallInst*)>(
          [&] (CallInst* OrigCall){
          LLVM_DEBUG(errs () << "Invoking ReorderArguments \n");
          
          if(attCall.find(OrigCall) == attCall.end()){
            LLVM_DEBUG(dbgs()<<"Not Cloning "<<*OrigCall<<"\n");
            return OrigCall;
          }


          Function* OrigF = OrigCall->getCalledFunction();

          
          CallInst* AttributeCall = attCall[OrigCall];
          CallInst* RetCall = retCall[OrigCall];

          auto toExclude = GetExcludedArgs(OrigCall, AttributeCall, RetCall);

          ValueToValueMapTy ArgMap;

          Function* NewF = CreateClone(OrigF, toExclude, ArgMap);


          if(NewF != OrigF){
              ClonedFunctions.insert(NewF);
              RemoveOrigFunctions.push_back(OrigF);
              LLVM_DEBUG(dbgs()<<"Inserting "<<OrigF->getName()<<" into delete list\n");

          }  else {
              LLVM_DEBUG(dbgs()<<"Did not clone :"<<NewF->getName()<<"\n");
          }


          CallInst* CallToReturn = nullptr;

          vector<CallInst*> ToErase;

          if(NewF != OrigF){
              for(auto* Users: OrigF->users()){
                  CallInst* CI = dyn_cast<CallInst>(Users);

                  if(!CI) continue;

                  if(CI->getCalledFunction() != OrigF) continue;

                  CallInst* NewCall = CloneCallInst(CI, NewF, toExclude, ArgMap);


                  Function* CallerF = NewCall->getParent()->getParent();

                  if(ClonedFunctions.find(CallerF) != ClonedFunctions.end()){
                      CallToReturn = NewCall; // Call to return must be from a cloned function
                  } 

                  ToErase.push_back(CI);
              }

              for(auto CI: ToErase){
                  CI->eraseFromParent();
              }
          }

          if(!CallToReturn){
              CallToReturn = dyn_cast<CallInst>(NewF->user_back());
              LLVM_DEBUG(dbgs()<<"Null Call To Return made into "<<*CallToReturn<<"\n");
          }



          // Create HPVM CreateNodeND calls for Sequential Tasks.
          vector<Value*> emptyVec;


          if(CallToNode.find(CallToReturn) == CallToNode.end()){
            CallToNode[CallToReturn] = cgenCreateNodeND.insertCallND(CallToReturn, NewF, emptyVec);
          }

          LLVM_DEBUG(dbgs()<<"Changed CallToReturn  to " <<*CallToReturn<<" in "<<CallToReturn->getParent()->getParent()->getName()<<"\n ");

          return CallToReturn;
  });

  LLVM_DEBUG(dbgs()<<"Indices:\t");
  for(auto i : indices){
      LLVM_DEBUG(dbgs()<<i<<" ");
  }
  LLVM_DEBUG(dbgs()<<"\n");

  vector<Section<CallInst*>> ClonedOutput;
  for(auto i : indices){
      Section<CallInst*> & SecI = Output[i]; 
      ClonedOutput.push_back(Section<CallInst*>(SecI, ReorderArguments, NOP));
  }



  vector<Value*> emptyVec;




  // TODO: Shouldn't GlobalDCE  do this for us?
  // Deleting pre-cloned versions of functions
  for(int j = RemoveOrigFunctions.size()-1; j >=0; j--){
      LLVM_DEBUG(dbgs()<<"Deleting:\t"<<RemoveOrigFunctions[j]->getName()<<"\n");
      RemoveOrigFunctions[j]->eraseFromParent();
  }
      

  // Due to function cloning, internal maps to
  // hpvm instrinsics may point to the precloned
  // instructions. Hence re-calibrate these maps.
  populateTaskIntrinsicsInfo(ClonedOutput);

  removeMarkerCalls();

  return ClonedOutput;
}

void HPVMProgram::populateTaskIntrinsicsInfo(
  vector<Section<CallInst*>> &Sections)
{
    for(auto &CurSection : Sections){
        
        
        for(auto &CurTask : CurSection.Tasks){


            CurTask->AttributeCall = nullptr;
            CurTask->ReturnCalls.clear();

            assert(CurTask->Location && "Task with null location");
            LLVM_DEBUG(errs() << "CurTask Location: "<< (*CurTask->Location)<<"\n");
            Function* TaskFn = CurTask->Location->getCalledFunction();

            for(auto &BB: *TaskFn){
                for(auto &I: BB){
                    CallInst* CI = dyn_cast<CallInst>(&I);
                    if(!CI) continue;
                    if(!CI->getCalledFunction()) continue;

                    if(CI && CI->getCalledFunction()->getName() == "__hpvm__return")
                        CurTask->ReturnCalls.push_back(CI);
                    else if(CI && CI->getCalledFunction()->getName() == "__hpvm__attributes")
                        CurTask->AttributeCall = CI;

                }
            }


        }
    }
}


int HPVMProgram::insertHPVMIntrinsics(
  vector<Section<CallInst*>> &Sections)
{

    // Helper Map of Extracted function calls to Task objects
    map<CallInst*, Task<CallInst*>*> RevTaskMap;

    // Helper Map of Section Node function call to Section objects
    map<Function*, Section<CallInst*>*> RevSectionMap;
    for(auto &CurSection : Sections){
        // RevSectionMap[Section.BeginMarker] = &CurSection;
        for(auto &CurTask : CurSection.Tasks){
            RevTaskMap[CurTask->Location] = CurTask.get();
            RevSectionMap[CurTask->Location->getParent()->getParent()] = &CurSection;
        }
    }





    // Get formal argument of function at index
    auto getArg = [](Function* F, unsigned idx) -> Argument* {
        Argument* arg = dyn_cast<Argument>((F->arg_begin() + idx));
        return arg;
    };

    // Get the CallInst operand Index for Value V.
    auto getValueIndexCI = [](CallInst* CI, Value* V)-> unsigned {
        unsigned ici = 0;
        for(; ici <CI->getNumArgOperands(); ici++){
            Value* valArg = CI->getArgOperand(ici);

            if(valArg == V){
                return ici;
            }
        }
        HPVMFatalError("Value isnt part of Function Call");
        return ici;
    };

    
    // Context Objects used to insert hpvm instrinsics
    HPVMCGenContext cgenContext(M);
	HPVMCGenCreateNodeND cgenCreateNodeND(cgenContext);
    HPVMCGenBindIn cgenBindIn(cgenContext);
    HPVMCGenBindOut cgenBindOut(cgenContext);
    HPVMCGenAttributes cgenAtt(cgenContext);


    // Given a task calculate which Value* corresponds to a particular 
    // Node output index
    auto getValueForDepIndex =  [&](CallInst* Fn, unsigned idx) -> Value*
    {
        Task<CallInst*>* t = RevTaskMap[Fn];
        
        Value* ActualVal;
        CallInst* RetInst = t->ReturnCalls[0];
        
        // +1 to skip the first argument which corresponds
        // to the number of return values in __hpvm__return
        ActualVal = RetInst->getArgOperand(1+idx); 


        return ActualVal;
    };


    // Given two Dependent Tasks, find the Destination index for DestTask (CurTask) given 
    // SrcTask (PrevTask) and it's __hpvm__return index for a Value
    auto getEdgeDestIndex = [&](CallInst* CurTask, CallInst* PrevTask, unsigned PrevRetIndex){
        
        Value* OutValue = getValueForDepIndex(PrevTask,PrevRetIndex);
        LLVM_DEBUG(dbgs()<<"OutValue for: "<<*PrevTask<<" at index "<<PrevRetIndex<<": "<<*OutValue<<"\n");
        Argument* RetArg = dyn_cast<Argument>(OutValue);

        if(!RetArg){
            HPVMFatalError("HPVMExtractTask: RetArg is NULL");
        }

        Value* EdgeVal = PrevTask->getArgOperand(RetArg->getArgNo());
        unsigned j = getValueIndexCI(CurTask, EdgeVal);

        return j;

    };


    // Empty vector for 0D Nodes
    vector<Value* > emptyVec;
    
    for(auto &CurSection : Sections){
        
        // Pair of CallInst to Node Function and Index:
        // Any particular value is a candidate for bindout if:
        //
        //  1) It is part of a task which is not a dependency of any
        // other task.
        //
        //  2) It is part of an __hpvm__return of a Task which is not
        // an edge to any other task




        vector<pair<CallInst*, unsigned>> BindOuts;


        // Vectors to store Parallel Section Input and Outputs for
        // __hpvm__attributes call
        vector<Value*> SecInputs;
        vector<Value*> SecOutputs;


        for(auto &CurTask : CurSection.Tasks){
            //Function* TaskFn = CurTask->Location->getCalledFunction();
            HPVMCGenEdge cgenEdge(cgenContext);
            CallInst* CurTaskCI = CurTask->Location;
            Function* CurTaskFn = CurTaskCI->getCalledFunction();

            // If the task/loop has a specified name,
            // override the node function name to that.
            if(CurTask->Name != ""){
                CurTaskFn->setName(CurTask->Name);
            }

            LLVM_DEBUG(dbgs()<<"   Task: "<<*CurTaskCI<<"\n");




            // Set for values in CurTask CallInst 
            // whose values are inserted into edges
            std::set<Value*> edgesFor;

            // Insert Edge Calls for Dependencies
            for(auto DepPair: CurTask->Dependencies){

                CallInst* PrevTaskCI = DepPair.first->Location;
                //Function* PrevFn = PrevTaskCI->getCalledFunction();

                CallInst* CurTaskCI = CurTask->Location;
                Instruction* nextInst = (Instruction*) CurTaskCI->getNextNode();

                unsigned retIndex = DepPair.second;


                unsigned inIndex = getEdgeDestIndex(CurTaskCI, PrevTaskCI, retIndex);

                CallInst* srcNode = CallToNode[PrevTaskCI];
                CallInst* dstNode = CallToNode[CurTaskCI];

    
                cgenEdge.insertCall(nextInst, srcNode, dstNode,/* replType: all-to-all */ 1, retIndex , inIndex,/* isStreaming: false */ 0);

                edgesFor.insert(CurTaskCI->getArgOperand(inIndex));



            }





            // Insert BindIn Calls for remaining arguments
            for(unsigned i=0; i < CurTaskCI->getNumArgOperands();i++){
                Value* CurArgValue = CurTaskCI->getArgOperand(i);

                if(edgesFor.find(CurArgValue) != edgesFor.end())
                    continue;

                Argument* CallerArg = dyn_cast<Argument>(CurArgValue);

                if(!CallerArg){
                    LLVM_DEBUG(dbgs()<<"BindIn Value: "<<*CurArgValue<<" is not caller argument, i.e. it is locally defined\n");
                    continue;
                }

                CallInst* ChildNode = CallToNode[CurTaskCI];

                CallInst* BindCall = cgenBindIn.insertCall(CurTaskCI, ChildNode, CallerArg->getArgNo(),
                       getValueIndexCI(CurTaskCI,CurArgValue) ,0);
                LLVM_DEBUG(errs()<<"BindIn call created: "<<*BindCall<<"\n");

                // __hpvm__attributes only takes pointer types
                if(!CurArgValue->getType()->isPointerTy())
                    continue;

                // BindIn Calls form __hpvm__attributes input for Current Parallel Section
                if(std::find(SecInputs.begin(), SecInputs.end(),CurArgValue) 
                        == SecInputs.end()){
                    SecInputs.push_back(CurArgValue);
                }

            }



            // list of Node output indices which
            // have been used in edges
            std::set<unsigned> RetUsed;

            // Possibily Temporary __hpvm_return call which
            // will be removed if Task is an internal node
            CallInst* RetInst = CurTask->ReturnCalls[0];

            
            for(auto RevDepPair: CurTask->RevDependencies){
                RetUsed.insert(RevDepPair.second);
            }

                       
            unsigned PtrIdx = 0;
            // Those Node output indices not in RetUsed become Bind Outs
            for(unsigned t = 0; t < CurTask->Outputs.size(); t++){
                unsigned SzIdx = PtrIdx+1;

                


                if(RetUsed.find(PtrIdx) == RetUsed.end()){
                    BindOuts.push_back(std::make_pair(CurTaskCI,PtrIdx));

                    Argument* PtrArg = dyn_cast<Argument>(RetInst->getArgOperand(1+PtrIdx));

                    Argument* SectionFormalArg = dyn_cast<Argument>(CurTaskCI->getArgOperand(PtrArg->getArgNo()));

                    CurSection.ArgToBindOutMap[SectionFormalArg] = BindOuts.size() - 1;


                }

                if(CurTask->Outputs[t].Size && !NoRetSize && RetUsed.find(SzIdx) == RetUsed.end()){
                    BindOuts.push_back(std::make_pair(CurTaskCI,SzIdx));
                    Argument* SzArg = dyn_cast<Argument>(RetInst->getArgOperand(1+SzIdx));

                    Argument* SectionFormalArg = dyn_cast<Argument>(CurTaskCI->getArgOperand(SzArg->getArgNo()));

                    CurSection.ArgToBindOutMap[SectionFormalArg] = BindOuts.size() - 1;

                }

                if(!NoRetSize)
                    PtrIdx += CurTask->Outputs[t].size();
                else
                    PtrIdx += 1;
            }


        }

        // Insert BindOut Calls for candidate Bindouts inside Parallel Section
        for(unsigned bi = 0; bi < BindOuts.size(); bi++){
            auto bindPair = BindOuts[bi];
            CallInst* NodeFn = CallToNode[bindPair.first];
            unsigned retIndex = bindPair.second;
            Instruction* insertBefore = NodeFn->getParent()->getTerminator();
            cgenBindOut.insertCall(insertBefore,
                    NodeFn,bi,retIndex,0);
        }
        
        
    }

    // Now that we have inserted all edges for the tasks
    // , we need to replace the __hpvm_return in Internal Nodes
    // with corresponding Bind Outs;
    for(auto &CurSection : Sections){

        for(auto &CurTask: CurSection.Tasks){
            if(!CurTask->isInternalNode()) continue;

            CallInst* TaskReturn = CurTask->ReturnCalls[0];
            Function* TaskFn = CurTask->Location->getCalledFunction();
            CurTask->ReturnCalls.pop_back();

            // Need to add BindIn's and BindOuts
            if(CurTask->Type == TaskType::INTERNAL_ACROSS){

                CallInst* AcrossCall = nullptr;
                for(auto &BB: *TaskFn){
                    for(auto& I: BB){
                        CallInst* CI = dyn_cast<CallInst>(&I);
                        if(CI && (CI->getCalledFunction()->getName() != 
                                    "__hpvm__return") && (CI->getCalledFunction()->getName() != 
                                    "__hpvm__attributes")){
                            AcrossCall = CI;
                            break;
                        }
                    }
                    if(AcrossCall) break;
                }

                assert(AcrossCall && "Internal Task of type across doesn't have a call inst.");

                std::vector<Value*> emptyVec;

                CallInst* AcrossNodeNDCall = cgenCreateNodeND.insertCallND
		        (AcrossCall, AcrossCall->getCalledFunction(), emptyVec);

                for(unsigned i = 0; i < AcrossCall->getNumArgOperands(); i++){
                    Value* CurArgValue = AcrossCall->getArgOperand(i);

                    Argument* CallerArg = dyn_cast<Argument>(CurArgValue);

                    if(!CallerArg){
                        LLVM_DEBUG(dbgs()<<"BindIn Value: "<<*CurArgValue<<" is not caller argument, i.e. it is locally defined\n");
                        HPVMFatalError("Across call argument must come through bindins");
                    }

                    cgenBindIn.insertCall(AcrossCall, AcrossNodeNDCall, CallerArg->getArgNo(),
                           getValueIndexCI(AcrossCall,CurArgValue) ,0);


                }

                for(unsigned i = 1; i < TaskReturn->getNumArgOperands(); i++){
                    Value* CurArgValue = TaskReturn->getArgOperand(i);

                    Argument* CallerArg = dyn_cast<Argument>(CurArgValue);

                    if(!CallerArg){
                        LLVM_DEBUG(dbgs()<<"Bindout Value: "<<*CurArgValue<<" is not caller argument, i.e. it is locally defined\n");
                        HPVMFatalError("Across call argument must come through bindout");
                    }

                    auto AcrossSec = RevSectionMap[AcrossCall->getCalledFunction()];

                    unsigned AcrossArgNo = getValueIndexCI(AcrossCall, CurArgValue);

                    Argument* AcrossFormalArg = getArg(AcrossCall->getCalledFunction(), AcrossArgNo);
                    cgenBindOut.insertCall(TaskReturn,
                        AcrossNodeNDCall,i-1,AcrossSec->ArgToBindOutMap[AcrossFormalArg],0);



                }


                // Remove original Call statement now that
                // createNodeND call is inserted
                AcrossCall->eraseFromParent();

                // Add BindIns and BindOuts to AcrossNode

            }
            TaskReturn->eraseFromParent();

        }
    }

    
    std::set<Function*> NonStreamingRoots = getNonStreamingRootNodes();

    for(auto &CurSection : Sections){
        
        insertTargetHints(/* CPU_TARGET */ 1, CurSection);

    }



    // For hpvm leaf nodes,
    // replace 0D create nodes with
    // 1D nodes with replication factor
    // 1
    if(!Use0D){
        for(auto &CurSection : Sections){
            for(auto &CurTask: CurSection.Tasks){
                if(CurTask->isInternalNode() || CurTask->isLoop()) continue;

                assert(CallToNode.find(CurTask->Location) 
                        != CallToNode.end() && "Leaf node not found in CallToNode");

                CallInst* OrigCreateNode = CallToNode[CurTask->Location];

                vector<Value*> OneD = {ConstantInt::get(Type::getInt64Ty(M.getContext()), 1)};
                
                CallInst* NewCreateNode = cgenCreateNodeND.insertCallND(CurTask->Location, CurTask->Location->getCalledFunction(), OneD);

                NewCreateNode->removeFromParent();

                ReplaceInstWithInst(OrigCreateNode, NewCreateNode);

            }
        }


    }

    // Remove any Extracted CallInsts since 
    // Node functions have been created.
    if(!KeepExtractedCalls){
        for(auto &CurSection : Sections){
            for(auto &CurTask: CurSection.Tasks){
                CurTask->Location->eraseFromParent();
            }
        }



    }

    

    return 0;
}

void HPVMProgram::printDebugInfo() {
  auto &Sections = Data.get<vector<Section<CallInst*>>>();
  LLVM_DEBUG(dbgs() << "\n");
  for (const auto &CurSection : Sections) {
    // dbgs() << "Section: " << *CurSection.Location <<" in parent F " << CurSection.Location->getParent()->getParent()->getName()<< "\n";
    for (const auto &CurTask : CurSection.Tasks) {
      LLVM_DEBUG(dbgs()<<"Is Task Loop ?:\t"<<(CurTask->isLoop())<<"\n");
      LLVM_DEBUG(dbgs()<<"Is Task Internal ?:\t"<<(CurTask->isInternalNode())<<"\n");
      LLVM_DEBUG(dbgs() << "\t" << *CurTask->Location <<" in parent F "<<CurTask->Location->getParent()->getParent()->getName() <<"\n");
    }
    LLVM_DEBUG(dbgs() << "\n");
  }
}

// After Function is extracted into seperate Call, 
// lifetime counter variables may be added to 
// Function Arguments.
std::set<Argument*> HPVMProgram::GetExcludedArgs(CallInst* CI, CallInst* AttCall,
        CallInst* RetCall){

    std::set<Argument*> Exclude;

    if(!AttCall || !RetCall){
        return Exclude;
    }

    
    Function* F = CI->getCalledFunction();

    LLVM_DEBUG(errs()<<"Exclude arg func:\t"<<*F<<"\n");

    CallInst* MarkerCall = getMarkerCall(F);

    if(!MarkerCall)
        return Exclude;


    for(auto ai = F->arg_begin(), ae = F->arg_end();
            ai != ae; ai++){
        Argument* AI = &*ai;

        if(!AttCall->hasArgument(AI) && !RetCall->hasArgument(AI)
                && !MarkerCall->hasArgument(AI)){
            LLVM_DEBUG(dbgs()<<"Excluded Args: "<<*AI<<"\n");
            Exclude.insert(AI);
        }
    }
    return Exclude;

}




void HPVMProgram::copyGEPCalculationInTask(Instruction* I, Region<Instruction> Bounds, ValueToValueMapTy &VMap, ValueToValueMapTy& RevVMap, 
        CallInst* MarkerCall){

    if(isa<PHINode>(I)) return;

    // Check whether instruction def falls outside task boundaries
    auto isOutsideBoundary = [&](Instruction* V){
        return /*!isa<PHINode>(V) &&*/ (DTCache.dominates(Bounds.End, V) || DTCache.dominates(V,Bounds.Begin));
    };

    vector<Value*> toClone;

    std::set<Value*> Cloned;
    std::queue<Value*> CloneWorkList;


    // For the current instruction, if any of it's non-constant
    // operands are defined outside the task boundaries,
    // we need to clone them inside the boundaries.
    // Iteratively repeat the steps for subsequent operands.
    for(unsigned i =0; i < I->getNumOperands();i++){
        if(isa<Constant>(I->getOperand(i))) continue;
            
        Instruction* OI = dyn_cast<Instruction>(I->getOperand(i));

        if(OI && isOutsideBoundary(OI)){
            CloneWorkList.push(OI);
        }
    }

    // Clone instructions which are defined
    // outside of task boundaries.
    while(CloneWorkList.size()){
        Value* item = CloneWorkList.front();
        CloneWorkList.pop();



        // If already inserted
        if(Cloned.find(item) != Cloned.end()) continue;
        if(isa<PHINode>(item)) continue;


        
        Cloned.insert(item);

        if(isa<Constant>(item)){
            continue;
        }


        if(VMap.find(item) != VMap.end()) continue;
        LLVM_DEBUG(dbgs()<<"Checking item: "<<*item<<" for original instruction "<<*I<<"\n");

        
        if(isa<Argument>(item)){
            VMap[item] = item;
            RevVMap[item] = item;
        } else if(Instruction* Ins = dyn_cast<Instruction>(item)){

            // If the instruction is inside the boundaries,
            // no need to clone
            if(!isOutsideBoundary(Ins)) continue;



            for(unsigned idx = 0; idx < Ins->getNumOperands(); idx++){
                CloneWorkList.push(Ins->getOperand(idx));
            }
            toClone.push_back(Ins);
        }
    }

    auto isOperandOf = [](Instruction* Inst, Value* VI) -> bool {
        for(Value* Op : Inst->operands()){
            if(Op == VI) return true;
        }
        return false;

    };

    // Sort to clone according to domination tree order
    auto cmpValues = [&](Value* V1, Value* V2)-> bool {
        auto I1 = dyn_cast<Instruction>(V1);
        auto I2 = dyn_cast<Instruction>(V2);

        if(!I1 || !I2){
            HPVMFatalError("Unknown ValueTy used in cloneInst");
        }
        
        bool hasArg = isOperandOf(I2, V1);

        return DTCache.dominates(I1, I2) || hasArg ;
    };

    sort(toClone, cmpValues);


    

    Instruction* InsertBefore = MarkerCall->getNextNonDebugInstruction();


    while(isa<AllocaInst>(InsertBefore)){
        InsertBefore = InsertBefore->getNextNonDebugInstruction();
    }

    for(Value* V: toClone){
        if(isa<PHINode>(V)){
            VMap[V] = V;
            RevVMap[V] = V;
            continue;
        }
        Instruction* VI = cast<Instruction>(V);
        Instruction* CVI = VI->clone();
        //CVI->insertBefore(I);
        if(isa<AllocaInst>(VI)){
            CVI->insertBefore(MarkerCall->getNextNonDebugInstruction());
        } else {
            CVI->insertBefore(InsertBefore);
        }
        CVI->setName(V->getName()+"_clone");

        Value* CV = CVI;

        VMap[V] = CV; 
        RevVMap[CV] = V;

    }

    DTCache.reset(I->getParent()->getParent());





    // Replace all cloned instruction forward  uses original outside 
    // instruction with new inst.
    for(unsigned idx = 0; idx < toClone.size(); idx++){
        Value* V = toClone[idx];

        Instruction* VI = cast<Instruction>(V);
        Instruction* CVI = cast<Instruction>(VMap[V]);
        
        std::set<BasicBlock*> included;
        auto shouldReplace = [&](Use &U)->bool {
            auto useInst = U.getUser();
            Instruction* IU = dyn_cast<Instruction>(useInst);

            if(!IU) return false;


            return DTCache.dominates(CVI ,IU) ; 
        };

        LLVM_DEBUG(dbgs()<<"Checking uses for "<<*VI<<"\n");
        for(auto ui = VI->use_begin(); ui != VI->use_end(); ){
            auto U = &*ui;
            ui++;
            Instruction* UI = dyn_cast<Instruction>(U->getUser());
            LLVM_DEBUG(dbgs()<<"Iterating over uses: "<<*U<<" "<<*UI<<"\n");

            if(shouldReplace(*U)){
                LLVM_DEBUG(dbgs()<<"Should replace "<<*U<<" "<<*UI<<" with "<<*CVI<<"\n");
                U->set(CVI);
            }
        }


        DTCache.reset(I->getParent()->getParent());
    }

    // Reset cache after replacing uses
    DTCache.reset(I->getParent()->getParent());

}

void HPVMProgram::removeMarkerCalls(){
    const string markers[5] = {ParSectionEndMarkerFuncName, TaskEndMarkerFuncName,
        ParSectionBeginMarkerFuncName, TaskBeginMarkerFuncNameVariadic, 
        LoopMarkerFuncNameVariadic
    };

    vector<CallInst*> ToRemove;
    vector<Function*> FToRemove;

    for (auto marker : markers){
        Function* F = M.getFunction(marker);

        if(!F) continue;

        for(auto *User: F->users()){
            CallInst* CI = dyn_cast<CallInst>(User);

            if(!CI) continue;

            ToRemove.push_back(CI);
        }

        FToRemove.push_back(F);

    }

    LLVM_DEBUG(dbgs()<<"Number of Markers to Erase: "<<ToRemove.size()<<"\n");
    for(unsigned i =0; i < ToRemove.size(); ++i){
        ToRemove[i]->eraseFromParent();
    }

    for(unsigned i =0; i < FToRemove.size(); ++i){
        FToRemove[i]->eraseFromParent();
    }


}

std::pair<CallInst*, CallInst*> HPVMProgram::GetAttributeAndReturn(Function* F)
{
    CallInst *AttCall = nullptr, *RetCall = nullptr;

    for(auto& BB: *F){
        for(auto& I: BB){
            CallInst* CI = dyn_cast<CallInst>(&I);
            if(CI && CI->getCalledFunction()->getName() == "__hpvm__return")
                RetCall = CI;
            else if(CI && CI->getCalledFunction()->getName() == "__hpvm__attributes")
                AttCall = CI;
        }
    }

    return std::make_pair(AttCall,RetCall);

}


// For debugging, print the instruction that invokes a marker function
// Make this a separate function because the dump() method isn't
// controlled by LLVM_DEBUG!
//
void debugPrintMarkerInfo(const Instruction &I)
{
  LLVM_DEBUG(dbgs() << "Success: Found user of HPVM marker function: " << "\n");
  LLVM_DEBUG(I.dump());
  LLVM_DEBUG(dbgs() << "\n" << " in function: "
         << I.getParent()->getParent()->getName().str()
         << "\n");
}

// Identify the loop induction variable and replace it with
// a __hpvm__getNodeInstanceID call
CallInst* HPVMProgram::parallelizeLoop(/*Loop* ExtractedLoop, Loop* InnerLoop*/ vector<Loop*>& LoopNest,/*Value* InductionVar*/ vector<Value*>& InductionVars, vector<Value*>& Limits, CallInst* ExtractedCall)
{
    assert(LoopNest.size() && "LoopNest must not be empty");
    BasicBlock* EntryBlock = *LoopNest[0]->block_begin();


    Function* OrigF = EntryBlock->getParent();

    HPVMCGenContext cgenContext(M);
    HPVMCGenGetNode cgenGetNode(cgenContext);
    HPVMCGenGetNodeInstanceID cgenGetID(cgenContext);
    HPVMCGenGetNumNodeInstances cgenGetNumNode(cgenContext);

    CallInst* getNodeCall = cgenGetNode.insertCall(OrigF->getEntryBlock().getTerminator());

    for(int i = 0; i < Limits.size(); i++){
        CallInst* getIDCall = cgenGetID.insertCall(OrigF->getEntryBlock().getTerminator(),
            getNodeCall, i);

        CallInst* getNumCall = cgenGetNumNode.insertCall(OrigF->getEntryBlock().getTerminator(),
            getNodeCall, i);

        Value* InductionVar = InductionVars[i];

        Loop* L = LoopNest[i];

        // For polyhedral loop nests, the main loop
        // body will be shared hence only need
        // to perform this computation once.
        if(true){
            BasicBlock *LoopIncoming, *LoopLatch;
            BasicBlock* Header = L->getHeader();

            LLVM_DEBUG(errs()<<*cast<Instruction>(InductionVar)->getParent()->getParent()<<"\n");
            if(!L->getIncomingAndBackEdge(LoopIncoming,LoopLatch)){
                //HPVMFatalError("Loop in non-canonical form");
            
                // Manually set Loop Incoming and Latch blocks
                LoopIncoming = getIDCall->getParent();


                for(BasicBlock *Pred: predecessors(Header)){
                    if(Pred != LoopIncoming){
                        LoopLatch = Pred;
                        break;
                    }
                }
            }

            BasicBlock* LoopExit = L->getUniqueExitBlock();

            if(!LoopExit){
                BranchInst* LatchBr = dyn_cast<BranchInst>(LoopLatch->getTerminator());
                
                for(unsigned j = 0 ; j < LatchBr->getNumSuccessors(); j++){
                    BasicBlock* Succ = LatchBr->getSuccessor(j);
                    if(Succ != Header){
                        LoopExit = Succ;
                        break;
                    }
                }
            }

            BasicBlock* Preheader = L->getLoopPreheader();

            Instruction* OldBr = LoopLatch->getTerminator();
            BranchInst::Create(LoopExit,OldBr);
            OldBr->eraseFromParent();

            bool isInnerMost = (i == (Limits.size() - 1)); 
            // Make the multi-dimensional loop nest into one straightline 
            // loop body execution
            BranchInst* OldHeaderBr = dyn_cast<BranchInst>(Header->getTerminator());
            if(!isInnerMost && OldHeaderBr && OldHeaderBr->isConditional()){
                LLVM_DEBUG(errs()<< "Replace "<<*OldHeaderBr <<" to unconditional...\n");
                BasicBlock* NonLatchBlock = nullptr;

                for(int k = 0; k < OldHeaderBr->getNumSuccessors(); k++){
                    BasicBlock* kSucc = OldHeaderBr->getSuccessor(k);

                    if(kSucc != LoopLatch){
                        NonLatchBlock = kSucc;
                        break;
                    }
                }

                if(NonLatchBlock){
                    BranchInst::Create(NonLatchBlock,OldHeaderBr);
                    OldHeaderBr->eraseFromParent();
                }



            }
             


            LLVM_DEBUG(errs()<<"Loop Header: "<<*Header<<"\n");
            if(Preheader){
                LLVM_DEBUG(errs()<<"Loop PreHeader: "<<*Preheader<<"\n");
            }
            LLVM_DEBUG(errs()<<"Loop Incoming Block: "<<*LoopIncoming<<"\n"); 
            LLVM_DEBUG(errs()<<"Loop Latch Block: "<<*LoopLatch<<"\n"); 
            LLVM_DEBUG(errs()<<"Loop Exit: "<<*LoopExit);
        }


        IntegerType* IVTy = dyn_cast<IntegerType>(InductionVar->getType());
        assert(IVTy && "Induction Variable must be of integer type");

        Value* MatchedType = castIntegerToBitwidth(getIDCall, getIDCall->getNextNode(), IVTy->getBitWidth());


        auto shouldReplaceIV = [&](Use &U)->bool {
            auto useInst = U.getUser();
            Instruction* IU = dyn_cast<Instruction>(useInst);

            if(!IU) return false;
            Instruction* IVInst = dyn_cast<Instruction>(MatchedType);

            return IU && DTCache.dominates(IVInst, IU);

        };


        InductionVar->replaceUsesWithIf(MatchedType, shouldReplaceIV);
        IntegerType* LimTy = dyn_cast<IntegerType>(InductionVar->getType());

        cast<Instruction>(InductionVar)->eraseFromParent();

        assert(LimTy && "Loop bounds  must be of integer type");

        Value* MatchedLimitType = castIntegerToBitwidth(getNumCall, getNumCall->getNextNode(), LimTy->getBitWidth());

        auto shouldReplaceLimit = [&](Use &U)->bool {
            auto useInst = U.getUser();
            Instruction* IU = dyn_cast<Instruction>(useInst);

            if(!IU) return false;
            if(IU->getParent()->getParent() != OrigF) return false;
            if(isHPVMGraphIntrinsic(IU)) return false;

            Instruction* LimInst = dyn_cast<Instruction>(MatchedLimitType);

            return IU && DTCache.dominates(LimInst, IU);

        };


        Limits[i]->replaceUsesWithIf(MatchedLimitType, shouldReplaceLimit);




        LLVM_DEBUG(errs()<<"Transformed Function: "<<*OrigF<<"\n");


    }



    auto Instrinsics = GetAttributeAndReturn(OrigF);
    CallInst* Attributes = Instrinsics.first;
    CallInst* Return = Instrinsics.second;

    // Identify TripCount Argument in Original Function
    auto TripCount = GetExcludedArgs(ExtractedCall, Attributes, Return);

    ValueToValueMapTy ArgMap;

    //For nested parallel loops, replace any 'exclude' arg which may
    //actually be the outer loops induction variable with parents 
    //getNodeInstanceIDx
    replaceOuterLoopIVInChild(ExtractedCall, getNodeCall, TripCount);

    // Create a Clone of the function without the tripcount argument.
    // and in the requried argument ordering
    Function* NewF = CreateClone(OrigF, TripCount,ArgMap);

    // Create a call to this new function with the correct subset of actual 
    // parameters
    CallInst* NewCall = CloneCallInst(ExtractedCall, NewF, TripCount,ArgMap);

    return NewCall;
}

bool HPVMProgram::isPartOfLoopHeader(Instruction* I){

    Function* ParentF = I->getParent()->getParent();
    auto &DT = DTCache.get(ParentF).DT;

    // Look for the immediately enclosing loop of the call instruction
    LoopInfo* LI = new LoopInfo(DT);
    LI->analyze(DT);

    Loop* loop = LI->getLoopFor(I->getParent());

    if(!loop) return false;

    LLVM_DEBUG(errs()<<"LOOP EXISTS\n");

    LLVM_DEBUG(errs()<<"CurInst: "<<*I<<"\n");
    LLVM_DEBUG(errs()<<"Loop preheader: "<<*loop->getLoopPreheader()<<" Current Parent: "<<*I->getParent()<<"\n");

    return (loop->getLoopPreheader() == I->getParent());
}



void HPVMProgram::clearLoopPreheader(Instruction* I){

    Function* ParentF = I->getParent()->getParent();
    auto &DT = DTCache.get(ParentF).DT;

    // Look for the immediately enclosing loop of the call instruction
    LoopInfo* LI = new LoopInfo(DT);
    LI->analyze(DT);

    Loop* loop = LI->getLoopFor(I->getParent());

    if(!loop) return;

    LLVM_DEBUG(errs()<<"CurInst: "<<*I<<"\n");
    //errs()<<"Loop preheader: "<<*loop->getLoopPreheader()<<" Current Parent: "<<*I->getParent()<<"\n";
    BasicBlock* LoopPreheader = loop->getLoopPreheader();

    vector<Instruction*> toErase;

    LLVM_DEBUG(errs()<<"LOOP PREHEADER:"<<*LoopPreheader<<"\n");
    for(auto& II: *LoopPreheader){
        if(isa<BranchInst>(&II)) continue;
        CallInst* CI = dyn_cast<CallInst>(&II);
        if(CI && CI->getCalledFunction()){

            if(isParSectionBeginMarker(CI) || 
                    isParallelLoopMarker(CI) ||
                    isTaskBeginMarker(CI) ||
                    isLaunchBeginMarker(CI)){
                continue;
            }
        }
        if(isa<PHINode>(&II)) continue;

        toErase.push_back(&II);
    }

    for(auto RI: toErase){
        RI->replaceAllUsesWith(UndefValue::get(RI->getType()));
        RI->eraseFromParent();
    }

}


// Any Alloca outside the parallel loops will be moved inside 
// the Loops header. Explicitly update any Alloca Uses inside 
// the loop.
void HPVMProgram::updateLoopUses(Instruction* I, ValueToValueMapTy& RevVMap){

    Function* ParentF = I->getParent()->getParent();
    auto &DT = DTCache.get(ParentF).DT;

    LoopInfo* LI = new LoopInfo(DT);
    LI->analyze(DT);

    Loop* loop = LI->getLoopFor(I->getParent());

    if(!loop) return;

    LLVM_DEBUG(errs()<<"processing updateLoopUses\n");
    BasicBlock* LoopHeader = loop->getHeader();

    for(auto& II: *LoopHeader){
        if(AllocaInst* AI = dyn_cast<AllocaInst>(&II)){
            auto shouldReplace = [&](Use &U)->bool {
                auto useInst = U.getUser();
                Instruction* IU = dyn_cast<Instruction>(useInst);

                if(!IU) return false;


                return DTCache.dominates(AI ,IU) ; 
            };

            LLVM_DEBUG(errs()<<"Alloca Inst: "<<*AI<<"\n");

            if(RevVMap.find(AI) == RevVMap.end()) continue;


            Value* RAI = RevVMap[AI];

            for(auto ui = RAI->use_begin(); ui != RAI->use_end(); ){
                auto U = &*ui;
                ui++;
                Instruction* UI = dyn_cast<Instruction>(U->getUser());

                if(shouldReplace(*U)){
                    U->set(AI);
                }
            }


            if(RAI->use_empty()){
                cast<Instruction>(RAI)->eraseFromParent();
            }

        }

    }

}

// FIXME: Refactor this so that it returns the loop.
// It should not be returning the #iterations, which is a code-gen task.
//
CallInst* HPVMProgram::extractLoopContainingInst(HPVMCGenContext& cgenContext,
     Instruction& I)
{
  LLVM_DEBUG(debugPrintMarkerInfo(I));

  // Need the dom tree for both loop analysis and code extraction
  Function* ParentF = I.getParent()->getParent();

  LLVM_DEBUG(errs()<<"extractLoopContainingInst on: "<<*ParentF<<"\n");
  auto &DT = DTCache.get(ParentF).DT;
  
  CallInst* LoopMarker = dyn_cast<CallInst>(&I);

  // Look for the immediately enclosing loop of the call instruction
  LoopInfo* LIp = new LoopInfo(DT);
  LoopInfo& LI = *LIp;
  LI.analyze(DT);
  Loop* myLoop = LI.getLoopFor(static_cast<CallInst &>(I).getParent());
  if (! myLoop) {
    warnError("No natural loop enclosing marker function call?");
		return nullptr;
  }

  LLVM_DEBUG(myLoop->print(dbgs()));


  moveSubLoopEntryCheck(myLoop,&I);
  

  Loop* InnerMostLoop = myLoop;

  ConstantInt* C = dyn_cast<ConstantInt>(LoopMarker->getArgOperand(0));
  int numLoops = C->getSExtValue();


  vector<Loop*> LoopNest;
  std::vector<Value*> dimSizeVec;



  
  AssumptionCache AC(*ParentF);
  TargetLibraryInfoImpl *TLIImpl =
    new TargetLibraryInfoImpl(Triple(ParentF->getParent()->getTargetTriple()));
  TargetLibraryInfo TLI(*TLIImpl);
  ScalarEvolution SE(*ParentF, TLI, AC, DT, LI);

  BasicBlock* LoopNestPred = nullptr;
  Loop* CurrentLoop = myLoop;
  for(int l = 1; l < numLoops; l++){
      CurrentLoop = CurrentLoop->getParentLoop();
  }
  LoopNestPred = CurrentLoop->getLoopPredecessor();

  // CreateNodeND calculation

  for(int l = 0; l < numLoops; l++){

	  assert(myLoop->isCanonical(SE) && "Did simplifyLoop succeed or fail?!");
	  Optional<Loop::LoopBounds> loopBounds = myLoop->getBounds(SE);
	  if (!loopBounds.hasValue()) { HPVMWarnError("Cannot compute loop bounds"); }
	  Value& Limit = loopBounds->getFinalIVValue();
      Instruction* II = dyn_cast<Instruction>(&Limit);

      Value* TripCount = nullptr;
      
      // If the trip count value is now moved inside the
      // calculation.
      if(II && myLoop->contains(II)){
        LLVM_DEBUG(errs()<<"Found LoopNest pred:\n"<<*LoopNestPred);
        assert(LoopNestPred && "LoopNest has no predecessor");
        Value* LoopLimit = copyValueChain(II, LoopNestPred->getTerminator());
        LLVM_DEBUG(errs() << "Updated Loop function:\n"<<*ParentF<<"\n");
        TripCount = LoopLimit;
      } else {
        TripCount = &Limit;
      }

      Value* inductionVar = myLoop->getInductionVariable(SE);


      if(isLoopInclusive(myLoop, inductionVar)){
        Constant* One = ConstantInt::get(inductionVar->getType(), 1);
        TripCount = BinaryOperator::Create(Instruction::Add, TripCount, One ,  "trip.count.", LoopNestPred->getTerminator());
      }

      dimSizeVec.insert(dimSizeVec.begin() , TripCount);

      bool outer_most = l == (numLoops - 1);

      LoopNest.push_back(myLoop);

      if(!outer_most){
          BranchInst* LoopGuard = getLoopGuard(myLoop);
          if(LoopGuard){
              Value* LoopCond = LoopGuard->getCondition();
              Value* NewCond = copyValueChain(LoopCond, LoopGuard);
              LoopGuard->setCondition(NewCond);
          }
          myLoop = myLoop->getParentLoop();
          assert(myLoop && "ParentLoop must be a loop");
      }
      
  }



	assert(myLoop->isCanonical(SE) && "Did simplifyLoop succeed or fail?!");
	Optional<Loop::LoopBounds> loopBounds = myLoop->getBounds(SE);
	if (!loopBounds.hasValue()) { HPVMWarnError("Cannot compute loop bounds"); }
	Value& numIters = loopBounds->getFinalIVValue();
    Value* FinalTripCount = &numIters;
    Instruction* II = dyn_cast<Instruction>(&numIters);
      

    Value* inductionVar = myLoop->getInductionVariable(SE);


    BasicBlock* loopPred  = myLoop->getLoopPredecessor();


    LLVM_DEBUG(errs()<<"Is Loop Inclusive: "<<isLoopInclusive(myLoop, inductionVar)<<"\n");

    // We increment the numIters by 1 to get the expression
    // for the incremented trip count
    if(isLoopInclusive(myLoop, inductionVar)){
        Constant* One = ConstantInt::get(inductionVar->getType(), 1);
        Value* NewTripCount = BinaryOperator::Create(Instruction::Add, &numIters, One ,  "trip.count.", loopPred->getTerminator());
        FinalTripCount = NewTripCount;
    }


    //dimSizeVec.insert(dimSizeVec.begin(), FinalTripCount);


    // Deduplicate BasicBlocks in Loop BB list. Not doing so
    // causes an assertion violation when extracting the loop.
    auto vecCheck = myLoop->getBlocksVector();
    for(auto vi = vecCheck.begin(), ve = vecCheck.end(); vi != ve; ){
        BasicBlock* bb = *vi;

        if(std::count(vecCheck.begin(),++vi,bb) > 1){
            myLoop->removeBlockFromLoop(bb);
        }
    }

    vecCheck = myLoop->getBlocksVector();

    
    LLVM_DEBUG(errs()<<"Loop PredBlock: "<<*loopPred<<"\n");

    


  // Perform the actual code extraction and get back the extracted function
  CodeExtractor codeExt(DT, *myLoop, /*aggregateArgs*/ false, /* AllowAlloca */ true);
  Function* extractedFunc = nullptr;

  if(!codeExt.isEligible()){
      HPVMFatalError("Loop Code Extraction failed...\n");
  }


  CodeExtractorAnalysisCache  CEAC(*ParentF);
  if ((extractedFunc = codeExt.extractCodeRegion(CEAC)) == nullptr)
    HPVMFatalError("Function extraction failed for loop marked parallel?");

	// Get the newly inserted function call to the extracted function
	if (! extractedFunc->hasOneUse())
		warnError("Extracted function has multiple uses?");
	Value* extractedFuncUser = *extractedFunc->user_begin();
	CallInst* callToExtractedFunc = dyn_cast<CallInst>(extractedFuncUser);
	if (! callToExtractedFunc)
		warnError("Function user is not a call instruction?");
			

	// Get the number of iterations of the extracted loop.
	// Assumes the loop is in canonical form (run LoopSimplify pass before this)
	// Replace the new function call with the createNodeND HPVM-C operation
	HPVMCGenCreateNodeND cgenCreateNodeND(cgenContext);
	//dimSizeVec.push_back(&numIters);


    LLVM_DEBUG(errs()<<*callToExtractedFunc->getCalledFunction()<<"\n");

    vector<Value*> Limits;
    vector<Value*> InductionVars;

    getNDLoopLimits(InnerMostLoop, SE, Limits, InductionVars, numLoops);

   

    // The outer-most included loop in the parallel
    // region will have been extracted and so, need's to be
    // added seperately.
    Limits.push_back(FinalTripCount);//&numIters);
    std::reverse(Limits.begin(), Limits.end());

    InductionVars.push_back(inductionVar);
    std::reverse(InductionVars.begin(), InductionVars.end());

    std::reverse(LoopNest.begin(), LoopNest.end());



    for(int k =0; k < Limits.size(); k++){
        LLVM_DEBUG(errs()<<"Limit "<<*Limits[k]<<"\n");
        LLVM_DEBUG(errs()<<"Induct "<<*InductionVars[k]<<"\n");

    }



    std::set<Instruction*> toRemove;

    // If extracted function returns an i1 (bool), which 
    // dictates a control flow decision, replace those Br
    // with unconditional branch insts (i.e. to cases assuming
    // no exception was thrown).

    for(auto *User : callToExtractedFunc->users()){
        BranchInst* BI = dyn_cast<BranchInst>(User);
        if(BI && BI->isConditional()){
            BranchInst::Create(BI->getSuccessor(1), BI);
            toRemove.insert(BI);
            LLVM_DEBUG(dbgs()<<"User of extracted loop:\n\t"<<*BI<<"\n");
        }
    }

    for(auto ri = toRemove.begin(), re = toRemove.end();
            ri != re; ){
        Instruction* Ins = *ri;
        ri++;
        Ins->eraseFromParent();
    }

  

    // Replace the loop guard with unconditionaal
    // entry into the loop.
    BasicBlock* BL = callToExtractedFunc->getParent();

    LLVM_DEBUG(errs()<<"Pre cleaning loop parent:  "<<*BL->getParent()<<"\n");



    if(loopPred)
        removeParallelLoopGuard(loopPred, BL);

    LLVM_DEBUG(errs()<<"Post cleaning loop parent:  "<<*BL->getParent()<<"\n");

    
    
    // LLVM's code extractor extracts the entire loop into the extracted function. Replace
    // entire Loop with a specific 1D instance. Also clone function to remove any
    // redudant arguments such as TripCount.
    CallInst* CleanedCall = parallelizeLoop(LoopNest,InductionVars, Limits, callToExtractedFunc);

    LLVM_DEBUG(dbgs()<<"Completed Sequentialize Loop!\n");

    callToExtractedFunc->replaceAllUsesWith(CleanedCall);
    callToExtractedFunc->eraseFromParent();

    CallInst* createNodeNDCall = cgenCreateNodeND.insertCallND
		(CleanedCall, CleanedCall->getCalledFunction(), dimSizeVec);

    LLVM_DEBUG(errs()<<"CreateNodeND "<<*createNodeNDCall<<" in parent "<<*CleanedCall->getParent()->getParent()<<"\n");
    LLVM_DEBUG(errs()<<"My Body: "<<*CleanedCall->getCalledFunction()<<"\n");


    // Add the re-ordered cloned function to the list of
    // ordered functions
    ClonedFunctions.insert(CleanedCall->getCalledFunction());



	// Finally, clear the DTCache and return the CallInst for createNodeND
	DTCache.reset(ParentF);
	return CleanedCall;
}


bool HPVMProgram::isCreateNodeCall(CallInst* CI){
    if(!CI) return false;

    return (CI->getCalledFunction()->getName() == "__hpvm__createNodeND");

}

void HPVMProgram::insertTargetHints(int Hint, const Section<CallInst*>& S){
    HPVMCGenContext cgenContext(M);
    HPVMCGenHint cgenHint(cgenContext);
    const StringRef FuncName("__hpvm__hint");
    Function *HPVMHintFunc = M.getFunction(FuncName);


    const StringRef HeteroName("__hetero_hint");
    Function *HeteroHintFunc = M.getFunction(HeteroName);

    if(HeteroHintFunc && HPVMHintFunc){
        HeteroHintFunc->replaceAllUsesWith(HPVMHintFunc);
    } else if(HeteroHintFunc && !HPVMHintFunc){
        HeteroHintFunc->setName("__hpvm__hint");

    }

    auto insertHint = [&] (Function* F){
        auto& entry = F->getEntryBlock();
        Instruction* InsertBefore = entry.getFirstNonPHI();

        Constant *constHintType = ConstantInt::get(Type::getInt32Ty(M.getContext()), Hint);

        vector<Value*> Args;
        Args.push_back(constHintType);


        if(NodeToHint.find(F) == NodeToHint.end() && !FuncContainsCall(F, HPVMHintFunc)) {
            CallInst* HintCall = cgenHint.insertCall(InsertBefore, Args);
            NodeToHint[F] = HintCall;
        }

    };


    for(auto &Task: S.Tasks){
        insertHint(Task->Location->getCalledFunction());
        insertHint(Task->Location->getParent()->getParent());
        if(SanatiseFunc){
            cleanFunctionName(Task->Location->getCalledFunction());
            cleanFunctionName(Task->Location->getParent()->getParent());

        }
    }

}

std::set<Function*> HPVMProgram::getStreamingRootNodes(){
    std::set<Function*> roots;

    Function* HPVMLaunch = M.getFunction("__hpvm__launch");

    if(!HPVMLaunch){
        HPVMWarnError("HPVM Program doesn't contain __hpvm__launch call");
        return roots;
    }

    for(auto* User: HPVMLaunch->users()){
        CallInst* CI = dyn_cast<CallInst>(User);
        assert(CI && "__hpvm__launch is used by non CallInst");

        Function* RootFn = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());

        if(!RootFn) HPVMFatalError("Malformed __hpvm__launch function");

        ConstantInt* C = dyn_cast<ConstantInt>(CI->getArgOperand(0));

        if(!C) HPVMFatalError("__hpvm__launch streaming type must be a constant int.");

        bool isStreaming = (C->getSExtValue() == 1);

        if(isStreaming){
            roots.insert(RootFn);
        }
    }
    return roots;
}

std::set<Function*> HPVMProgram::getNonStreamingRootNodes(){
    std::set<Function*> roots;

    Function* HPVMLaunch = M.getFunction("__hpvm__launch");

    if(!HPVMLaunch){
        HPVMWarnError("HPVM Program doesn't contain __hpvm__launch call");
        return roots;
    }

    for(auto* User: HPVMLaunch->users()){
        CallInst* CI = dyn_cast<CallInst>(User);
        assert(CI && "__hpvm__launch is used by non CallInst");

        Function* RootFn = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());

        if(!RootFn) HPVMFatalError("Malformed __hpvm__launch function");

        ConstantInt* C = dyn_cast<ConstantInt>(CI->getArgOperand(0));

        if(!C) HPVMFatalError("__hpvm__launch streaming type must be a constant int.");

        bool isStreaming = (C->getSExtValue() == 1);

        if(!isStreaming){
            roots.insert(RootFn);
        }
    }
    return roots;
}

void HPVMProgram::removeBindOuts(Function* F){
    vector<Instruction*> toRemove;

    for(auto& BB: *F){
        for(auto& II: BB){
            CallInst* CI = dyn_cast<CallInst>(&II);
            if(!CI) continue;

            if(CI->getCalledFunction()->getName() == "__hpvm__bindOut")
                toRemove.push_back(&II);
        }
    }

    for(int i = 0; i < toRemove.size(); i++){
        toRemove[i]->eraseFromParent();
    }
}

bool HPVMProgram::isHPVMDataFlowGraph(Function* F){
    assert(F && "NULL is not a valid Dataflow graph");


    // Currently, the function must be defined
    // within the same module.
    if(F->isDeclaration()){
        return false;
    }

    for(auto& BB: *F){
        for(auto& II: BB){
            CallInst* CI = dyn_cast<CallInst>(&II);

            if(!CI) continue;

            if(!CI->getCalledFunction()) continue;

            if(isParSectionBeginMarker(CI))
                return true;

            if(CI->getCalledFunction()->getName()  ==  CreateNodeNDMarkerFuncName)
                return true;
        }
    }
    return false;
}


void HPVMProgram::replaceIndirectCall(Function* F){

    CallInst* SecCall = nullptr;
    CallInst* SecParentCall = nullptr;

    for(auto& BB: *F){
        for(auto& II: BB){
            CallInst* CI = dyn_cast<CallInst>(&II);

            if(!CI) continue;

            // Not the type of Internal Node task we want
            if(isHPVMDataFlowGraph(CI->getCalledFunction())){
                return;
            }

            if(CI->getCalledFunction()->isDeclaration()) continue;

            SecParentCall = CI;
            break;
        }

        if(SecParentCall) break;
    }

    // Repeat Steps for SecParentCall to find the relavent
    // parallel section.

    for(auto& BB: *(SecParentCall->getCalledFunction())){
        for(auto& II: BB){
            CallInst* CI = dyn_cast<CallInst>(&II);

            if(!CI) continue;

            if(isHPVMDataFlowGraph(CI->getCalledFunction())){
                SecCall = CI;
                break;

            }

        }

        if(SecCall) break;
    }

    // Match the arguments values across function boundaries. For this
    // we must match each value.

    Function* SecParentF = SecParentCall->getCalledFunction();
    Function* SecF = SecCall->getCalledFunction();


    vector<Value*> CallArgs;

    for(int i = 0; i < SecCall->getNumArgOperands(); i++){
        Argument* AI = dyn_cast<Argument>(SecCall->getArgOperand(i));

        if(!AI){
            HPVMFatalError("Node argument must be an argument type.");
        }

        Value* DirectValue = SecParentCall->getArgOperand(AI->getArgNo());

        Argument* DA = dyn_cast<Argument>(DirectValue);

        if(!DA){
            HPVMFatalError("Node argument must be an argument type.");
        }

        CallArgs.push_back(DirectValue);
    }

    ArrayRef<Value*> NewArgs(CallArgs);
    CallInst* NewCall = CallInst::Create(SecF->getFunctionType(), SecF, NewArgs,"", SecParentCall);
    SecParentCall->eraseFromParent();

}

void HPVMProgram::removeParallelLoopGuard(BasicBlock* Preheader, BasicBlock* Direct){
    BasicBlock* GuardBB = Preheader->getUniquePredecessor();

    if(GuardBB){
        BranchInst* Br = dyn_cast<BranchInst>(GuardBB->getTerminator());

        if(Br->isUnconditional()){
            return;
        }

        BasicBlock* LeftSucc = Br->getSuccessor(0);
        BasicBlock* RightSucc = Br->getSuccessor(1);

        BasicBlock* NewSucc = nullptr;

        if(LeftSucc == Direct){
            NewSucc = LeftSucc;
        } else if (RightSucc == Direct){
            NewSucc = RightSucc;
        } else {

            while(BranchInst* LBR = dyn_cast<BranchInst>(LeftSucc->getTerminator())){
                if(!LBR->isUnconditional()){
                    NewSucc = RightSucc;
                    break;
                }

                if(LBR->getSuccessor(0) == Direct){
                    NewSucc = Br->getSuccessor(0);
                    break;
                }

                LeftSucc = LBR->getSuccessor(0);

            }

            // If left branch led to a return
            if(!NewSucc){
                NewSucc = Br->getSuccessor(1);
            }

        }
        


        if(Br){
            BranchInst* NewBr = BranchInst::Create(/*Direct*/ NewSucc, Br);
            Br->eraseFromParent();
            LLVM_DEBUG(errs()<<"NEW BR INST: "<<*NewBr<<"\n"<<*NewBr->getParent()<<"\n");
        }

    }
}
bool HPVMProgram::isParentLoopParallel(Instruction* I){
    BasicBlock* LoopBB = I->getParent();
    Function* ParentF = LoopBB->getParent();

    auto &DT = DTCache.get(ParentF).DT;

    LoopInfo* LI = new LoopInfo(DT);
    LI->analyze(DT);

    Loop* CurrentLoop = LI->getLoopFor(LoopBB);

    if(!CurrentLoop){
        HPVMFatalError("__hpvm_parallel_loop invoked outside of a loop");
    }


    Loop* ParentLoop = CurrentLoop->getParentLoop();

    // If the current loop
    // is the outer most loop
    if(!ParentLoop)
        return false;

    bool isParallel = isLoopParallel(ParentLoop);

    if(!isParallel)
        HPVMFatalError("Inner Loop declared to be parallel while outer loop is sequential");

    return isParallel;

}



void HPVMProgram::getNDLoopLimits(Loop* L, ScalarEvolution& SE, 
        vector<Value*>& Limits, vector<Value*>& InductionVars, int numLoops){

    Loop* CurrentLoop = L;
    while(CurrentLoop && (--numLoops)){
        if(!CurrentLoop->isCanonical(SE)){
            break;
        }
        assert(CurrentLoop->isCanonical(SE) && "simpliyfyLoop transform not successful");

        Optional<Loop::LoopBounds> loopBounds = CurrentLoop->getBounds(SE);

        if(!loopBounds.hasValue()) { HPVMFatalError("Parallel Loops tripcount not resolvable by SE"); }

        Value& numIters = loopBounds->getFinalIVValue();
        Value* inductionVar = CurrentLoop->getInductionVariable(SE);

        // errs()<<numIters<<" "<<*inductionVar<<"\n";

        Limits.push_back(&numIters);
        InductionVars.push_back(inductionVar);

        CurrentLoop = CurrentLoop->getParentLoop();
    };

    assert(Limits.size() <= 3 && "HPVM currently supports 3D nodes");


}

bool HPVMProgram::isLoopParallel(Loop* L){
    BasicBlock* LoopPred = L->getLoopPredecessor();

    if(!LoopPred){
        HPVMFatalError("Outer Loop does not have a unique predecessor!");
    }

    BasicBlock* LoopBody = LoopPred->getUniqueSuccessor();

    for(auto & II: *LoopBody){
        if(CallInst* CI = dyn_cast<CallInst>(&II)){
            if(isParallelLoopMarker(CI))
                return true;
        }
    }
    return false;
}

vector<Loop*> HPVMProgram::getNestedParallelLoops(Loop* L){
    vector<Loop*> ParallelLoops;

    Loop* Current = L;

    while(Current){
        if(isLoopParallel(Current))
            ParallelLoops.push_back(Current);
        else
            break;

        Loop* ImmediateChild = nullptr;

        SmallVector<Loop*,4> ChildLoops = Current->getLoopsInPreorder(); 

        for(Loop* CL: ChildLoops){
            if(CL == Current) continue;

            // Each Loop should only have child loop (if child loop is parallel)
            if(CL->getParentLoop() == Current){
                assert(!ImmediateChild && "Parallel Parent Loop has multiple parallel child sibling loops");
                ImmediateChild = CL;
            }
        }

        Current = ImmediateChild;
    }

    return ParallelLoops;
}

CallInst* HPVMProgram::createHPVMOrderCall(Instruction* IB, vector<Value*> Args){
    Function* OF = M.getFunction("__hpvm__order");
    Type* i32Ty = Type::getInt32Ty(M.getContext());
    Type* i8PTy = Type::getInt8PtrTy(M.getContext());
    Type* voidTy = Type::getVoidTy(M.getContext());


    if(!OF){
        vector<Type*> Args = {i32Ty};

        FunctionType* OrderTy = FunctionType::get(voidTy,Args, /* isVarArgs */ true);


        OF = Function::Create(OrderTy, Function::ExternalLinkage, "__hpvm__order", M);

        LLVM_DEBUG(errs()<<"Order Func: "<<*OF<<"\n");
    }

    Constant* numArgs = ConstantInt::get(i32Ty,Args.size());

    Args.insert(Args.begin(), numArgs);

    return CallInst::Create(OF, Args, "", IB);

}

void HPVMProgram::replaceLoopTripCount(Instruction* I, ValueToValueMapTy& VMap,
        ValueToValueMapTy& RevVMap, int NumEnclosingLoops){

    LLVM_DEBUG(errs() << "Replace Loop Trip Count ");
    Function* ParentF = I->getParent()->getParent();
    auto &DT = DTCache.get(ParentF).DT;

    // Look for the immediately enclosing loop of the call instruction
    LoopInfo* LI = new LoopInfo(DT);
    LI->analyze(DT);

    Loop* loop = LI->getLoopFor(I->getParent());

    if(!loop) return;

    LLVM_DEBUG(errs() << "replace loop trip count loop found\n");


    AssumptionCache AC(*ParentF);
    TargetLibraryInfoImpl *TLIImpl = new TargetLibraryInfoImpl(Triple(ParentF->getParent()->getTargetTriple()));
    TargetLibraryInfo TLI(*TLIImpl);
    ScalarEvolution SE(*ParentF, TLI, AC, DT, *LI);

    for(int i = 0; i < NumEnclosingLoops; i++){

        LLVM_DEBUG(errs() << "ReplaceLoopTripCount invoked on: "<<*loop<<"\n");
        assert(loop->isCanonical(SE) && "Did simplifyLoop succeed or fail?!");
        Optional<Loop::LoopBounds> loopBounds = loop->getBounds(SE);
        if (!loopBounds.hasValue()) { HPVMWarnError("Cannot compute loop bounds"); }
        Value* numIters = &loopBounds->getFinalIVValue();

        if(isa<Argument>(numIters)){

            LLVM_DEBUG(errs() << "Final loop bound is an argument\n");
            continue;
        }

        LLVM_DEBUG(errs()<<"Is loop depth? "<<loop->getLoopDepth()<<"\n");
        Loop* ParentLoop = loop->getParentLoop();



        bool isOuterMost = true;

        if(ParentLoop){
            LLVM_DEBUG(errs() << "Parent Loop Depth: "<< ParentLoop->getLoopDepth()<<"\n");
            isOuterMost = ParentLoop->getHeader() == loop->getHeader()  
                || (!DTCache.dominates(ParentLoop->getHeader(), loop->getHeader())); // Natural loops require outer loop header to dominate inner loop header
        }




        LLVM_DEBUG(errs()<<"Before replacing trip counts:\n"<<*ParentF<<"\n");

        // If the TripCount was calculated locally
        // in the function
        if( isOuterMost && RevVMap.find(numIters) != RevVMap.end()){
            LLVM_DEBUG(errs()<<"Non parent loop case\n");


            LLVM_DEBUG(errs()<<"NumIters: " << *numIters <<"\n");
            Value* OrigCount = RevVMap[numIters];
            LLVM_DEBUG(errs()<<"OrigCount: " << *OrigCount <<"\n");

            numIters->replaceAllUsesWith(OrigCount);

            //numIters->replaceUsesWithIf(OrigCount, shouldReplace);

        } else if ( !isOuterMost && RevVMap.find(numIters) != RevVMap.end()){
            LLVM_DEBUG(errs()<<"Parent loop case\n");
            LLVM_DEBUG(errs()<<"Parent loop is same as loop? "<<(ParentLoop == loop)<<"\n");
            LLVM_DEBUG(errs()<<"Parentloop: "<<*ParentLoop<<", child loop "<<*loop<<"\n");
            // Copy Trip Count computation to parent loop
            Instruction* IB = ParentLoop->getHeader()->getFirstNonPHI();
            Value* NewCount = copyValueChain(numIters, IB);
            LLVM_DEBUG(errs() << "NumIters: "<<*numIters<<"\n");
            
            LLVM_DEBUG(errs() << "New Count: "<<*NewCount<<"\n");
            numIters->replaceAllUsesWith(NewCount);


            //numIters->replaceUsesWithIf(NewCount, shouldReplace);

        }


        //loop = loop->getParentLoop();
        loop = LI->getLoopFor(loop->getHeader());
    }

    LLVM_DEBUG(errs()<<"After replacing trip counts:\n"<<*ParentF<<"\n");

}

bool HPVMProgram::isGetNodeIdxCall(Value* V){
    CallInst* CI = dyn_cast<CallInst>(V);

    if(!CI) return false;

    Function* Fn = CI->getCalledFunction();

    if(!Fn) return false;

    return Fn->getName().startswith(StringRef("__hpvm__getNodeInstanceID_"));
}

bool HPVMProgram::isValueIV(Value* V){
    if(!isa<PHINode>(V) || !isa<Instruction>(V)) return false;

    Instruction* I = dyn_cast<Instruction>(V);

    LLVM_DEBUG(errs()<<"Checking if "<<*I<<" is an IV for Function "<<*I->getParent()->getParent()<<"\n");

    Function* ParentF = I->getParent()->getParent();
    // explicity reset cache for parent function
    DTCache.reset(ParentF);

    auto &DT = DTCache.get(ParentF).DT;

    // Look for the immediately enclosing loop of the call instruction
    LoopInfo* LI = new LoopInfo(DT);
    LI->analyze(DT);

    Loop* loop = LI->getLoopFor(I->getParent());

    if(!loop) return false;

    LLVM_DEBUG(errs()<<"Part of loop...\n");


    AssumptionCache AC(*ParentF);
    TargetLibraryInfoImpl *TLIImpl =
        new TargetLibraryInfoImpl(Triple(ParentF->getParent()->getTargetTriple()));
    TargetLibraryInfo TLI(*TLIImpl);
    ScalarEvolution SE(*ParentF, TLI, AC, DT, *LI);


	assert(loop->isCanonical(SE) && "Did simplifyLoop succeed or fail?!");
	Optional<Loop::LoopBounds> loopBounds = loop->getBounds(SE);
	if (!loopBounds.hasValue()) { HPVMWarnError("Cannot compute loop bounds"); }
    Value* inductionVar = loop->getInductionVariable(SE);

    LLVM_DEBUG(errs()<<"Induction Var: "<<*inductionVar<<"\n");



    return (inductionVar == V);
}


void HPVMProgram::replaceOuterLoopIVInChild(CallInst* ExtractedCall,CallInst* getNodeCall, std::set<Argument*> TripCount){

    LLVM_DEBUG(errs()<<"Invoked replaceOuterLoopIVInChild on "<<*ExtractedCall<<"\n");
    HPVMCGenContext cgenContext(M);
    HPVMCGenGetParentNode cgenGetParentNode(cgenContext);
    HPVMCGenGetNodeInstanceID cgenGetID(cgenContext);


    CallInst* ParentNode = nullptr;

    for(Argument* E : TripCount){

        LLVM_DEBUG(errs()<<"Checking E: "<<*E<<"\n"); 
        unsigned ArgNo = E->getArgNo();

        Value* ActualParameter = ExtractedCall->getArgOperand(ArgNo);

        LLVM_DEBUG(errs()<<"Actual Parameter: "<<*ActualParameter<<"\n");

        if(!(isValueIV(ActualParameter) || isGetNodeIdxCall(ActualParameter))) continue;

        LLVM_DEBUG(errs()<<"Exclude argument: "<<*E<<" is outer loops induction variable ...\n");

        if(!ParentNode){
            ParentNode = cgenGetParentNode.insertCall(getNodeCall->getNextNode(), getNodeCall);
        }


        CallInst* ParentID = cgenGetID.insertCall(ParentNode->getNextNode(), ParentNode, 0);


        E->replaceAllUsesWith(ParentID);

    }

}

Instruction* HPVMProgram::findImmediateLoopMarker(Instruction* I){
    Function* F = I->getParent()->getParent();

    ReversePostOrderTraversal<Function *> RPOT(F);
    auto RPOBB = RPOT.begin();
    auto RPOBBEnd = RPOT.end();

    Instruction* Trav = I;


    // Move to the specific basic block
    while(Trav->getParent() != *RPOBB && RPOBB != RPOBBEnd){
        RPOBB++;
    }


    Instruction* LoopMarker = nullptr;
    while(!LoopMarker){
        
        if(CallInst* CI = dyn_cast<CallInst>(Trav)){
            if(isParallelLoopMarker(CI)){
                LoopMarker = Trav;
            }
        }

        Trav = Trav->getNextNode();

        while(!Trav){
            RPOBB++;
            BasicBlock* NextB = *RPOBB;
            Trav = NextB->getFirstNonPHI();
        }
    }

    return LoopMarker;

}


void HPVMProgram::replaceLegacyWithNewFunctions(){
    LLVM_DEBUG(errs() << "replaceLegacyWithNewFunctions\n");

    // Replace loop markers
    Function* LegacyLoopMarker = M.getFunction(LegacyLoopMarkerFuncNameVariadic); 
    Function* LoopMarker = M.getFunction(LoopMarkerFuncNameVariadic); 

    if(LegacyLoopMarker && LoopMarker){
        LegacyLoopMarker->replaceAllUsesWith(LoopMarker);
        LLVM_DEBUG(errs() << "replaced loop markers\n");
    } else if (LegacyLoopMarker){
        LegacyLoopMarker->setName(LoopMarkerFuncNameVariadic);
    }


    // Replace task begin markers
    Function* LegacyTaskMarker = M.getFunction(LegacyTaskBeginMarkerFuncNameVariadic); 
    Function* TaskMarker = M.getFunction(TaskBeginMarkerFuncNameVariadic); 

    if(LegacyTaskMarker && TaskMarker){
        LegacyTaskMarker->replaceAllUsesWith(TaskMarker);
        LLVM_DEBUG(errs() << "replaced task markers\n");
    } else if(LegacyTaskMarker){
        LegacyTaskMarker->setName(TaskBeginMarkerFuncNameVariadic);
    }


    // Replace task end markers
    LegacyTaskMarker = M.getFunction(LegacyTaskEndMarkerFuncName); 
    TaskMarker = M.getFunction(TaskEndMarkerFuncName); 

    if(LegacyTaskMarker && TaskMarker){
        LegacyTaskMarker->replaceAllUsesWith(TaskMarker);
        LLVM_DEBUG(errs() << "replaced task markers\n");
    } else if(LegacyTaskMarker){
        LegacyTaskMarker->setName(TaskEndMarkerFuncName);
    }

    // Replace section begin  markers
    Function* LegacySectionMarker = M.getFunction(LegacyParSectionBeginMarkerFuncName); 
    Function* SectionMarker = M.getFunction(ParSectionBeginMarkerFuncName); 

    if(LegacySectionMarker && SectionMarker){
        LegacySectionMarker->replaceAllUsesWith(SectionMarker);
        LLVM_DEBUG(errs() << "replaced section markers\n");
    } else if(LegacySectionMarker){
        LegacySectionMarker->setName(ParSectionBeginMarkerFuncName);
    }


    // Replace section end  markers
    LegacySectionMarker = M.getFunction(LegacyParSectionEndMarkerFuncName); 
    SectionMarker = M.getFunction(ParSectionEndMarkerFuncName); 

    if(LegacySectionMarker && SectionMarker){
        LegacySectionMarker->replaceAllUsesWith(SectionMarker);
        LLVM_DEBUG(errs() << "replaced section markers\n");
    } else if(LegacySectionMarker){
        LegacySectionMarker->setName(ParSectionEndMarkerFuncName);
    }

    LLVM_DEBUG(errs() << M <<"\n");
}


void HPVMProgram::moveSubLoopEntryCheck(Loop* ParentLoop, Instruction* IB){
    for(Loop* SubLoop : ParentLoop->getSubLoopsVector()){
        if(BranchInst* Br = getLoopGuard(SubLoop)){
            Value* NewCond = copyValueChain(Br->getCondition(), Br);
            Br->setCondition(NewCond);
        }
    }
}


void findAndExtractTasks(Module &M, bool SanatiseFunctionNames,bool Use0DNode, bool NoReturnSize)
{
  ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

  // Set up the code generator context
  HPVMCGenContext cgenContext(M);

  // Set up cache for DominatorTree and PostDominatorTree analyses
  DominatorTreeCache DTCache;

  HPVMProgram Program(M, DTCache, SanatiseFunctionNames, Use0DNode, NoReturnSize);

  // Print final module for testing and debugging
  LLVM_DEBUG(M.dump());
}
