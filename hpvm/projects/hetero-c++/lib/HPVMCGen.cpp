//===- HPVMCGen.cpp - HPVM-C generation -------------------------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// LLVMCGen.cpp -- Contains code for HPVM-C generation
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/ADT/StringRef.h"
#include "HPVMCGen.h"
#include "HPVMCGenContext.h"
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <cstddef>
using namespace llvm;

// Insert a call with the explicit function arguments specified.
// Checks that the specified arguments match the function signature.
// *INCOMPLETE*: We only check the number of arguments for now.
//
CallInst* HPVMCGenBase::insertCall(Instruction* insertBefore,
        const StringRef& HPVMCFuncName,
        std::vector<Value *>& funcArgs)
{
    getIRB().SetInsertPoint(insertBefore);
    // const StringRef& HPVMCFuncName = theContext.getHPVMCFuncName(baseFuncName);
    // const HPVMCGenContext::HPVMFuncDescriptor* funcDesc = theContext.
    // getDesc(HPVMCFuncName);
    // if (funcDesc->isVarArg()) {
    // std::cerr << "HPVMCGenBase: Warning: VarArgs functions must be "
    // << "checked by a subclass, then call insertCallUnchecked" << std::endl;
    // return nullptr;
    // }

    // Get the function declaration needed for this call
    //
    Function* callee = theContext.getFunction(HPVMCFuncName);
    if (callee == nullptr)
        HPVMFatalError("Function decl for " + HPVMCFuncName.str() + " missing!");

    // Use the function declaration in funcDesc to check numArgs
    //
    // if (funcDesc->getNumArgs() != funcArgs.size())
    // HPVMFatalError("Function call to " + HPVMCFuncName.str() +
    // " has incorrect #args");


    // Insert the requested function call at the specified insertion point
    CallInst* newCallInst;
    if(callee->getReturnType()->isVoidTy()){
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, ArrayRef<Value*>(funcArgs));
    }else {
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, ArrayRef<Value*>(funcArgs), HPVMCFuncName);
    }



    return newCallInst;
}
CallInst* HPVMCGenBase::insertCall(Instruction* insertBefore,
        const StringRef& HPVMCFuncName,
        ArrayRef<Value *>& funcArgs)
{
    getIRB().SetInsertPoint(insertBefore);
    // const StringRef& HPVMCFuncName = theContext.getHPVMCFuncName(baseFuncName);
    // const HPVMCGenContext::HPVMFuncDescriptor* funcDesc = theContext.
    // getDesc(HPVMCFuncName);
    // if (funcDesc->isVarArg()) {
    // std::cerr << "HPVMCGenBase: Warning: VarArgs functions must be "
    // << "checked by a subclass, then call insertCallUnchecked" << std::endl;
    // return nullptr;
    // }

    // Get the function declaration needed for this call
    //
    Function* callee = theContext.getFunction(HPVMCFuncName);
    if (callee == nullptr)
        HPVMFatalError("Function decl for " + HPVMCFuncName.str() + " missing!");

    // Use the function declaration in funcDesc to check numArgs
    //
    // if (funcDesc->getNumArgs() != funcArgs.size())
    // HPVMFatalError("Function call to " + HPVMCFuncName.str() +
    // " has incorrect #args");


    // Insert the requested function call at the specified insertion point
    //
    //
    CallInst* newCallInst;
    if(callee->getReturnType()->isVoidTy()){
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs);
    }else {
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs, HPVMCFuncName);
    }



    return newCallInst;
}

// Insert a call with only the arugment numbers specified.
// There is no type information, so this cannot check the function signature.
// The signature must be checked in a subclass that uses this method.
//
CallInst* HPVMCGenBase::insertCallUnchecked(Instruction* insertBefore,
        const StringRef& funcName,
        Function* nodeFunc,
        std::vector<int>& argNumbers)
{
    getIRB().SetInsertPoint(insertBefore);
    const StringRef& HPVMCFuncName = funcName;
    Function* callee = theContext.getFunction(HPVMCFuncName);


    std::vector<Value *> funcArgsVec;
    IntegerType* integerType = IntegerType::getInt32Ty(theContext.getLLVMContext());
    for (unsigned i = 0; i < argNumbers.size(); i++) {
        ConstantInt* toAppend = ConstantInt::get(integerType, argNumbers[i]);
        funcArgsVec.push_back(cast<Value>(toAppend));
    }
    ArrayRef<Value *> funcArgs(funcArgsVec);

    CallInst* newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs, HPVMCFuncName);
    return newCallInst;
}

CallInst* HPVMCGenBase::insertCallUnchecked(Instruction* insertBefore, 
        const StringRef& funcName,
        Function* nodeFunc,
        ArrayRef<Value *>& funcArgs) {
    getIRB().SetInsertPoint(insertBefore);
    //TODO: temp fix on HPVMCFuncName
    const StringRef& HPVMCFuncName = funcName;//theContext.getHPVMCFuncName(funcName);
    Function* callee = theContext.getFunction(HPVMCFuncName);

    CallInst* newCallInst;
    if (callee->getReturnType()->isVoidTy()) {
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs);
    } else {
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs, HPVMCFuncName);
    }

    //CallInst* newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs, HPVMCFuncName);
    return newCallInst;
}



CallInst* HPVMCGenBase::insertCallUnchecked(Instruction* insertBefore, 
        const StringRef& funcName,
        Function* nodeFunc,
        std::vector<Value *>& funcArgs) {
    getIRB().SetInsertPoint(insertBefore);
    //TODO: temp fix on HPVMCFuncName
    const StringRef& HPVMCFuncName = funcName;//theContext.getHPVMCFuncName(funcName);
    Function* callee = theContext.getFunction(HPVMCFuncName);

    CallInst* newCallInst;
    if (callee->getReturnType()->isVoidTy()) {
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, ArrayRef<Value*>(funcArgs));
    } else {
        newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, ArrayRef<Value*>(funcArgs), HPVMCFuncName);
    }

    //CallInst* newCallInst = getIRB().CreateCall(callee->getFunctionType(), callee, funcArgs, HPVMCFuncName);
    return newCallInst;
}

AllocaInst* HPVMCGenBase::insertAlloca(Instruction* insertBefore, std::vector<Value *>& args) {

    // If trying to launch a graph which takes no 
    // input, return an empty alloca
    if(args.empty()){
        LLVM_DEBUG(errs() << "Warning: Attempting to launch a graph which takes 0 inputs ... "<< "\n");
        Type* voidTy = Type::getInt8Ty(theContext.getLLVMContext());
        getIRB().SetInsertPoint(insertBefore);
        return getIRB().CreateAlloca(voidTy);
    }

    std::vector<Type *> argTypesVec;
    for(auto *argument : args) {
        argTypesVec.push_back(argument->getType());
    }


    ArrayRef<Type *> argTypes(argTypesVec);
    StructType *StructTy = StructType::create(argTypes, "DFGArgs", true);

    getIRB().SetInsertPoint(insertBefore);
    AllocaInst* newAlloca = getIRB().CreateAlloca(StructTy);

    for (unsigned i = 0; i < args.size(); i++) {
        /* Store insts to initialize structs */
        Value *GEP = getIRB().CreateStructGEP(newAlloca, i);
        getIRB().CreateStore(args[i], GEP);
    }

    return newAlloca;
}

LoadInst* HPVMCGenBase::insertConstInt(Instruction *insertBefore, ConstantInt *const_int) {
    getIRB().SetInsertPoint(insertBefore);

    AllocaInst *alloca = getIRB().CreateAlloca(const_int->getType());
    StoreInst *store = getIRB().CreateStore(const_int, alloca);
    LoadInst *load = getIRB().CreateLoad(alloca);

    return load;
}


// Insert a call to the CreateNodeND function.
// 
CallInst* HPVMCGenCreateNodeND::insertCallND(Instruction* insertBefore, 
        Function* nodeFunc,  
        std::vector<Value *>& dimSizeVec) {
    const StringRef funcName("__hpvm__createNodeND");

    // Get the HPVM-C function for createNodeND to check the types
    Function* createNodeNDFunc = theContext.getModule().getFunction(funcName);
    if (! createNodeNDFunc) HPVMWarnError("CreateNodeND function not found?");

    FunctionType* funcType = createNodeNDFunc->getFunctionType();
    if (! funcType->isVarArg()) HPVMFatalError("CreateNodeND function not VarArgs?");

    // Create the args list for __hpvm__CreateNodeND and insert the call
    //    #dimensions = dimSizeVec.size()
    //    Node function = nodeFunc
    //    size of dim. 0 = dimSize[0] ...
    std::vector<Value *> argsVec;
    IntegerType* dimSizeType = IntegerType::get(theContext.getLLVMContext(), 32); 
    ConstantInt* dimSizeConst = ConstantInt::get(dimSizeType, dimSizeVec.size()); 
    argsVec.push_back(dimSizeConst);
    argsVec.push_back(nodeFunc);

    IntegerType* I64Type = IntegerType::get(theContext.getLLVMContext(), 64); 
    for (auto d: dimSizeVec){
        if(!d->getType()->isIntegerTy(64)){

            IntegerType* SizeTy = dyn_cast<IntegerType>(d->getType());
            assert(SizeTy && "Dimension size must be an integer type");
            Value* CastToI64 = nullptr;

            if(SizeTy->getBitWidth() > 64){
                // trunc
                CastToI64 = new TruncInst(d, I64Type, "trunc_", insertBefore);
            } else {

                CastToI64 = new SExtInst(d, I64Type, "sext_", insertBefore);
            }
            argsVec.push_back(CastToI64);

        } else {
            argsVec.push_back(d);
        }
    }
    ArrayRef<Value *> argsList(argsVec); // safe: argsList not needed after return
    CallInst* newCall = this->insertCall(insertBefore, funcName, argsList);
    if (! newCall) HPVMWarnError("CreateNodeND function not VarArgs?");

    return newCall;
}


