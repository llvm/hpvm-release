//===- extract-task.cpp -- Extract a parallel task into node function ----===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM
// Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===---------------------------------------------------------------------===//
//
// This file extracts a parallel loop or task identified by a hpvm marker
// into its own node function, repeating that for all marked tasks.
//
//===---------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "DFGUtils.h"
#include "llvm/IR/Constants.h"



#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>
#include <fstream>
#include <iostream>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;


const string CreateNodeNDFuncName = "__hpvm__createNodeND";
const string EdgeFuncName = "__hpvm__edge";
const string LaunchFuncName = "__hpvm__launch";
const string BindInFuncName = "__hpvm__bindIn";
const string BindOutFuncName = "__hpvm__bindOut";




void DotFatalError(const StringRef &msg) {
  std::cerr << "hpvm-2-dot: FATAL: " << msg.str() << std::endl;
  exit(1);
  return;
}

void DotWarnError(const StringRef &msg) {
  std::cerr << "hpvm-2-dot: WARNING: " << msg.str() << std::endl;
  return;
}

struct HPVMDotProgram {
    public:
        HPVMDotProgram(Module &M, string suffix): M(M){
            LLVM_DEBUG(dbgs()<<"----- HPVMDotProgram ----\n");

            createNodeFunc = M.getFunction(CreateNodeNDFuncName);
            edgeFunc = M.getFunction(EdgeFuncName);
            launchFunc = M.getFunction(LaunchFuncName);
            bindInFunc = M.getFunction(BindInFuncName);
            bindOutFunc = M.getFunction(BindOutFuncName);

            if(!createNodeFunc){
                DotFatalError("Unable to find definition for:\t"+CreateNodeNDFuncName);
            }

            if(!edgeFunc){
                DotFatalError("Unable to find definition for:\t"+EdgeFuncName);
            }

            if(!launchFunc){
                DotFatalError("Unable to find definition for:\t"+LaunchFuncName);
            }

            if(!bindInFunc){
                DotFatalError("Unable to find definition for:\t"+BindInFuncName);
            }

            if(!bindOutFunc){
                DotFatalError("Unable to find definition for:\t"+BindOutFuncName);
            }


            for(auto* User: launchFunc->users()){
                CallInst* CI = dyn_cast<CallInst>(User);

                if(!CI) continue;
                LLVM_DEBUG(dbgs()<<"Launch Instruction: "<<*CI<<"\n");

                Function* RootFn = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());

                if(!RootFn) DotFatalError("Malformed launch function!");


                string dot_str = runOnDFGRoot(RootFn);
                std::ofstream dotFile;

                string fname = RootFn->getName().str()+ suffix + ".dot";

                dbgs()<<"Writing graph to file: "<<fname<< " ...\n";
                dotFile.open(fname);
                dotFile<<dot_str;
                dotFile.close();


            }



        }

    private:
        Function* createNodeFunc;
        Function* edgeFunc;
        Function* launchFunc;
        Function* bindInFunc;
        Function* bindOutFunc;

        Module &M;

        string runOnDFGRoot(Function* RootFn);
        
        // Returns the 'dot' strings for the
        // subgraph NodeFn
        string runOnNode(Function* NodeFn);

        string dotChildNodes(vector<Function*> Children);


        string dotBindIn(CallInst* BI);
        string dotBindOut(CallInst* BO);
        string dotEdge(CallInst* E);


        string sanatiseString(string name);

};

string HPVMDotProgram::runOnDFGRoot(Function* RootFn){
    string dfg_str = "digraph "+ sanatiseString(RootFn->getName().str()) +"{\n";

    dfg_str += runOnNode(RootFn);

    dfg_str +="}";


    return dfg_str;

}


string HPVMDotProgram::runOnNode(Function* NodeFn){
    
    vector<string> entries; 
    
    vector<Function*> ChildNodes = getChildRoots(NodeFn);

    // All children should be rendered on the same
    // level.
    entries.push_back(
            dotChildNodes(ChildNodes));

    for (inst_iterator I = inst_begin(NodeFn), E = inst_end(NodeFn); I != E; ++I){
        Instruction* II = &*I;

        CallInst* CI = dyn_cast<CallInst>(II);

        if(!CI) continue;

        Function* CF = CI->getCalledFunction();

        if(!CF) continue;

        if(CF == bindInFunc){
            entries.push_back(dotBindIn(CI));
        } else if (CF == bindOutFunc){
            entries.push_back(dotBindOut(CI));
        } else if (CF == edgeFunc){
            entries.push_back(dotEdge(CI));
        }
        
    }


    for(Function* ChildF: ChildNodes){
        entries.push_back(runOnNode(ChildF));
    }

    string dfg_dot_str = "";

    for(auto str : entries){
        dfg_dot_str += str;
    }

    return dfg_dot_str;
}


string HPVMDotProgram::dotChildNodes(vector<Function*> Children){
    if (!Children.size()) return "";

    string subgraph_str = "subgraph {\nrank = same;";

    for(Function* ChildF: Children){
        subgraph_str += sanatiseString(ChildF->getName().str() +";");
    }

    subgraph_str += "\n}";

    return subgraph_str;
}


string HPVMDotProgram::dotBindIn(CallInst* BI){
    string bindin_str = "edge [color=red]\n";

    
    Function* ParentF = BI->getParent()->getParent();

    CallInst* CreateNodeCall = dyn_cast<CallInst>(BI->getArgOperand(0));

    Function* ChildF = dyn_cast<Function>(CreateNodeCall->getArgOperand(1)->stripPointerCasts());

    bindin_str += sanatiseString(ParentF->getName().str() + " -> " + ChildF->getName().str() + " ");

    bindin_str += "[style=bold ";

    ConstantInt* ParentIndex = dyn_cast<ConstantInt>(BI->getArgOperand(1));
    ConstantInt* ChildIndex = dyn_cast<ConstantInt>(BI->getArgOperand(2));


    Value* ParentValue = &*(ParentF->arg_begin() + ParentIndex->getSExtValue());
    Value* ChildValue = &*(ChildF->arg_begin() + ChildIndex->getSExtValue());

    // Get's difficult to read graph, too verbose
    // bindin_str += ",label=\""+ (ParentValue->getName().str()) +" -> "+(ChildValue->getName().str())+"\"";

    bindin_str += "]\n";

    return bindin_str;
}

string HPVMDotProgram::dotBindOut(CallInst* BO){
    string bindout_str = "edge [color=green]\n";

    
    Function* ParentF = BO->getParent()->getParent();

    CallInst* CreateNodeCall = dyn_cast<CallInst>(BO->getArgOperand(0));

    Function* ChildF = dyn_cast<Function>(CreateNodeCall->getArgOperand(1)->stripPointerCasts());

    bindout_str += sanatiseString(ChildF->getName().str() + " -> " + ParentF->getName().str() + " ");

    bindout_str += "[style=bold ";

    ConstantInt* ParentIndex = dyn_cast<ConstantInt>(BO->getArgOperand(2));
    ConstantInt* ChildIndex = dyn_cast<ConstantInt>(BO->getArgOperand(1));


    Value* ParentValue = &*(ParentF->arg_begin() + ParentIndex->getSExtValue());
    
    // [GET CHILD VALUE FOR BINDOUT]
    Value* ChildValue = &*(ChildF->arg_begin() + ChildIndex->getSExtValue());

    //bindout_str += ",label=\""+ (ChildValue->getName()) +" -> "+(ParentValue->getName())+"\"";

    bindout_str += "]\n";

    return bindout_str;
}

string HPVMDotProgram::dotEdge(CallInst* E){
    string edge_str = "edge [color=blue]\n";

    CallInst* SrcCreateNodeCall = dyn_cast<CallInst>(E->getArgOperand(0));
    CallInst* DstCreateNodeCall = dyn_cast<CallInst>(E->getArgOperand(1));


    Function* SrcF = dyn_cast<Function>(SrcCreateNodeCall->getArgOperand(1)->stripPointerCasts());
    Function* DstF = dyn_cast<Function>(DstCreateNodeCall->getArgOperand(1)->stripPointerCasts());


    edge_str += sanatiseString(SrcF->getName().str() + " -> " + DstF->getName().str() + " ");

    edge_str += "[style=bold ";

    //ConstantInt* ParentIndex = dyn_cast<ConstantInt>(BI->getArgOperand(1));
    //ConstantInt* ChildIndex = dyn_cast<ConstantInt>(BI->getArgOperand(2));


    //Value* ParentValue = &*(ParentF->arg_begin() + ParentIndex->getSExtValue());
    //Value* ChildValue = &*(ChildF->arg_begin() + ChildIndex->getSExtValue());

    //bindin_str += "label=\""+ (ParentValue->getName()) +" -> "+(ChildValue->getName())+"\"";

    edge_str += "]\n";

    return edge_str;
}

// dot does not allow '.' in their names. Hence substitute it with
// an underscore '_' .
string HPVMDotProgram::sanatiseString(string name){
    std::replace(name.begin(), name.end(), '.','_');
    return name;

}




void DFG2Dot(Module &M, string suffix)
{
  ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

  HPVMDotProgram Dot(M,suffix);

}
