//===- DominatorTreeCache.cpp - Caches dominator and post-dominator trees -===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===---------------------------------------------------------------------===//
//
// @file
// DominatorTreeCache.cpp -- Provides definitions for the dominator tree cache
//
//===---------------------------------------------------------------------===//

#include "DominatorTreeCache.h"

DominatorTreeCache::DominatorTreePair& DominatorTreeCache::get(Function *F) {
  if (Cache.find(F) == Cache.end())
    Cache.emplace(F, std::unique_ptr<DominatorTreePair>(new DominatorTreePair(F)));
  return *Cache[F].get();
}

bool DominatorTreeCache::dominates(BasicBlock *A, BasicBlock *B) {
  auto &DT = get(A->getParent()).DT;
  return DT.dominates(A, B);
}

bool DominatorTreeCache::dominates(Instruction *A, Instruction *B) {
  auto &DT = get(A->getParent()->getParent()).DT;
  return DT.dominates(A, B);
}

bool DominatorTreeCache::postDominates(Instruction *A, Instruction *B) {
  auto &PDT = get(A->getParent()->getParent()).PDT;
  // PDT is not implemented as nicely as DT and only accepts basic blocks, so
  // we manually check the case where both instructions are in the same BBs
  if (!PDT.dominates(A->getParent(), B->getParent())) {
    return false;
  }
  if (A->getParent() == B->getParent()) {
    for (Instruction &I : *B->getParent()) {
      if (&I == B) {
        return true;
      } else if (&I == A) {
        return false;
      }
    }
  }
  return true;
}

bool DominatorTreeCache::postDominates(BasicBlock *A, BasicBlock *B) {
  auto &PDT = get(A->getParent()).PDT;
  return PDT.dominates(A, B);
}

void DominatorTreeCache::reset() {
  Cache.clear();
}

void DominatorTreeCache::reset(Function *F) {
  Cache.erase(F);
}
