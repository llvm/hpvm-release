/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

#include "CoreHPVM/HPVMHint.h"

extern "C" {
#include <stdio.h>

void __hpvm__hint(hpvm::Target) noexcept;

extern void *__hpvm__createNodeND(unsigned, ...) noexcept;
extern void __hpvm__return(unsigned, ...) noexcept;

extern void __hpvm__attributes(unsigned, ...) noexcept;
extern void __hpvm__init() noexcept;
extern void __hpvm__cleanup() noexcept;

extern void __hpvm__bindIn(void *, unsigned, unsigned, unsigned) noexcept;
extern void __hpvm__bindOut(void *, unsigned, unsigned, unsigned) noexcept;
extern void *__hpvm__edge(void *, void *, unsigned, unsigned, unsigned,
                   unsigned) noexcept;

extern void __hpvm__push(void *, void *) noexcept;
extern void *__hpvm__pop(void *) noexcept;
extern void *__hpvm__launch(unsigned, ...) noexcept;
extern void __hpvm__wait(void *) noexcept;

extern void *__hpvm__getNode() noexcept;
extern void *__hpvm__getParentNode(void *) noexcept;
extern void __hpvm__barrier() noexcept;
extern void *__hpvm__malloc(long) noexcept;
extern long __hpvm__getNodeInstanceID_x(void *) noexcept;
extern long __hpvm__getNodeInstanceID_y(void *) noexcept;
extern long __hpvm__getNodeInstanceID_z(void *) noexcept;
extern long __hpvm__getNumNodeInstances_x(void *) noexcept;
extern long __hpvm__getNumNodeInstances_y(void *) noexcept;
extern long __hpvm__getNumNodeInstances_z(void *) noexcept;

// Atomic
// signed int
extern int __hpvm__atomic_add(int *, int) noexcept;
extern int __hpvm__atomic_sub(int *, int) noexcept;
extern int __hpvm__atomic_xchg(int *, int) noexcept;
extern int __hpvm__atomic_inc(int *) noexcept;
extern int __hpvm__atomic_dec(int *) noexcept;
extern int __hpvm__atomic_min(int *, int) noexcept;
extern int __hpvm__atomic_max(int *, int) noexcept;
extern int __hpvm__atomic_and(int *, int) noexcept;
extern int __hpvm__atomic_or(int *, int) noexcept;
extern int __hpvm__atomic_xor(int *, int) noexcept;

extern void llvm_hpvm_track_mem(void *, size_t) noexcept;
extern void llvm_hpvm_untrack_mem(void *) noexcept;
extern void llvm_hpvm_request_mem(void *, size_t) noexcept;

void DummyUserFunction()
{
  printf("%p", (void *) __hpvm__hint);
  printf("%p", (void *) __hpvm__createNodeND);
  printf("%p", (void *) __hpvm__return);
  printf("%p", (void *) __hpvm__attributes);
  printf("%p", (void *) __hpvm__init);
  printf("%p", (void *) __hpvm__cleanup);

  printf("%p", (void *) __hpvm__bindIn);
  printf("%p", (void *) __hpvm__bindOut);
  printf("%p", (void *) __hpvm__edge);

  printf("%p", (void *) __hpvm__hint);

  printf("%p", (void *) __hpvm__createNodeND);
  printf("%p", (void *) __hpvm__return);

  printf("%p", (void *) __hpvm__attributes);
  printf("%p", (void *) __hpvm__init);
  printf("%p", (void *) __hpvm__cleanup);

  printf("%p", (void *) __hpvm__bindIn);
  printf("%p", (void *) __hpvm__bindOut);
  printf("%p", (void *) __hpvm__edge);

  printf("%p", (void *) __hpvm__push);
  printf("%p", (void *) __hpvm__pop);
  printf("%p", (void *) __hpvm__launch);
  printf("%p", (void *) __hpvm__wait);

  printf("%p", (void *) __hpvm__getNode);
  printf("%p", (void *) __hpvm__getParentNode);
  printf("%p", (void *) __hpvm__barrier);
  printf("%p", (void *) __hpvm__malloc);
  printf("%p", (void *) __hpvm__getNodeInstanceID_x);
  printf("%p", (void *) __hpvm__getNodeInstanceID_y);
  printf("%p", (void *) __hpvm__getNodeInstanceID_z);
  printf("%p", (void *) __hpvm__getNumNodeInstances_x);
  printf("%p", (void *) __hpvm__getNumNodeInstances_y);
  printf("%p", (void *) __hpvm__getNumNodeInstances_z);

  printf("%p", (void *) __hpvm__atomic_add);
  printf("%p", (void *) __hpvm__atomic_sub);
  printf("%p", (void *) __hpvm__atomic_xchg);
  printf("%p", (void *) __hpvm__atomic_inc);
  printf("%p", (void *) __hpvm__atomic_dec);
  printf("%p", (void *) __hpvm__atomic_min);
  printf("%p", (void *) __hpvm__atomic_max);
  printf("%p", (void *) __hpvm__atomic_and);
  printf("%p", (void *) __hpvm__atomic_or);
  printf("%p", (void *) __hpvm__atomic_xor);

  printf("%p", (void *) llvm_hpvm_track_mem);
  printf("%p", (void *) llvm_hpvm_untrack_mem);
  printf("%p", (void *) llvm_hpvm_request_mem);
}

} // extern "C"
