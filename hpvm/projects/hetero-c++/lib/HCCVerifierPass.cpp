//===- HCCVerifierPass.cpp --- HeteroC++ Verificationr Pass -----*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-Licence-Idenfitier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HCCVerifierPass.cpp -- Verifies that a HeteroC++ program (in LLVM IR) is
/// valid
//
//===----------------------------------------------------------------------===//

#include "llvm/Pass.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Scalar.h"
#include "HCCVerifierPass.h"
#include <iostream>

#define DEBUG_TYPE "hcc-verifier-pass"

using namespace llvm;

HCCVerifierPass::HCCVerifierPass() :
  ModulePass(ID)
{
  initializeHCCVerifierPassPass(*PassRegistry::getPassRegistry());
}

bool HCCVerifierPass::runOnModule(Module& M) {
  LLVM_DEBUG(dbgs() << "HCCVerifierPass Running\n");
  HCCVerifier verifier(M);
  bool res = verifier.verify();
  if (!res) {
    errs() << "HeteroC++ Verification Failed. Aborting Compilation\n";
    exit(1);
  }

  LLVM_DEBUG(dbgs() << "HCCVerifierPass Finished\n");
  return false;
}

INITIALIZE_PASS(HCCVerifierPass, "hcc-verify",
                "HeteroC++ verification pass", true, true)

char HCCVerifierPass::ID = 0;
