#include "HPVMCGenFunctions.h"

CallInst *HPVMCGenInit::insertCall(Instruction * InsertPoint) {
    const StringRef FuncName("__hpvm__init");
    Function *HpvmInit = theContext.getModule().getFunction(FuncName);
    if (!HpvmInit) { 
        HPVMFatalError("__hpvm__init function not found"); 
    }

    ArrayRef<Value *> args = { };
    CallInst * NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}

CallInst *HPVMCGenCleanup::insertCall(Instruction * InsertPoint) {
    const StringRef CleanupFuncName("__hpvm__cleanup");
    Function *HpvmCleanup = theContext.getFunction(CleanupFuncName);
    if (!HpvmCleanup) {
        HPVMFatalError("__hpvm__cleanup function not found");
    }

    ArrayRef<Value *> args = { };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, CleanupFuncName, args);
    return NewCall;
}

CallInst *HPVMCGenBindIn::insertCall(Instruction *InsertPoint, CallInst *NodeInst, unsigned ip, unsigned ic, unsigned isStream) {
    // Get function & check if function exists within module
    const StringRef FuncName("__hpvm__bindIn");
    Function *HpvmBindIn = theContext.getModule().getFunction(FuncName);
    if (!HpvmBindIn) {
        HPVMFatalError("__hpvm__bindIn function not found.");
    }

    Function *NodeFunc = dyn_cast<Function>(NodeInst->getArgOperand(1));//NodeInst->getCalledFunction();
    if (!NodeFunc) {
        HPVMFatalError("Could not find function from CallInst argument.");
    }

    Function *ParentFunc = NodeInst->getParent()->getParent();

    //unsigned IpNum = ip->getArgNo();
    Argument* ipArg = dyn_cast<Argument>(ParentFunc->arg_begin() + ip);

    if(!ipArg){
        HPVMFatalError("BindIn ip index doesn't correspond to Argument");
    }

    Type *IpType = ipArg->getType();
    if (!IpType) { 
        HPVMFatalError("Could not get type of internal node argument."); 
    }

    //unsigned IcNum = ic->getArgNo();

    Argument* icArg = dyn_cast<Argument>(NodeFunc->arg_begin() + ic);
    if(!icArg){
        HPVMFatalError("BindIn ic index doesn't correspond to Argument");
    }

    Type *IcType = icArg->getType();
    if (!IcType) { 
        HPVMFatalError("Could not get type of leaf node argument"); 
    }

    if (IpType != IcType) {
        HPVMFatalError("Argument types do not match!");
    }
    
    if (Bind_Ins.find(icArg) != Bind_Ins.end()) {
        HPVMFatalError("There is already an argument binding into the child argument!");
    } else {
        Bind_Ins[icArg] = ipArg;
    }

    IntegerType* integerType = IntegerType::getInt32Ty(theContext.getLLVMContext());


    
    std::vector<Value*> arguments;
    arguments.push_back(NodeInst);
    arguments.push_back(ConstantInt::get(integerType,ip));
    arguments.push_back(ConstantInt::get(integerType,ic));
    arguments.push_back(ConstantInt::get(integerType,isStream)); // TODO Check if isStream assumed or passed to this function


    CallInst *NewCall = HPVMCGenBase::insertCallUnchecked(InsertPoint, FuncName, NodeFunc, arguments);
    return NewCall;
}

CallInst * HPVMCGenBindOut::insertCall(Instruction *InsertPoint, CallInst *NodeInst, unsigned op, unsigned oc, unsigned isStream) {
    const StringRef FuncName("__hpvm__bindOut");
    Function *HpvmBindOut = theContext.getModule().getFunction(FuncName);
    if (!HpvmBindOut) {
        HPVMFatalError("__hpvm__bindIn function not found.");
    }

    Function *NodeFunc = dyn_cast<Function>(NodeInst->getArgOperand(1));
    if (!NodeFunc) {
        HPVMFatalError("Could not find function from CallInst argument.");
    }


    IntegerType* integerType = IntegerType::getInt32Ty(theContext.getLLVMContext());

    std::vector<Value*> arguments;
    arguments.push_back(NodeInst);
    arguments.push_back(ConstantInt::get(integerType,oc));
    arguments.push_back(ConstantInt::get(integerType,op));
    arguments.push_back(ConstantInt::get(integerType,isStream));




    CallInst *NewCall = this->insertCallUnchecked(InsertPoint, FuncName, NodeFunc, arguments);
    return NewCall;
}

// CallInst src and dst are createNodeND calls
CallInst* HPVMCGenEdge::insertCall(Instruction * InsertPoint, CallInst *src, CallInst *dst, unsigned ReplType, unsigned sp, unsigned dp, unsigned isStream) {
    if (!InsertPoint || !src || !dst) {
        HPVMFatalError("One or more arguments were nullptr!");
    }

    const StringRef FuncName("__hpvm__edge");
    Function * HpvmEdge = theContext.getModule().getFunction(FuncName);
    if (!HpvmEdge) {
        HPVMFatalError("__hpvm__edge function not found in context");
    }

    auto getArg = [](Function* F, unsigned idx) {
        Argument* arg = dyn_cast<Argument>((F->arg_begin() + idx));
        return arg;
    };

    auto getHPVMReturn = [&](Function* F){
        for(auto& BB: *F){
            for(auto& I: BB){
                if(CallInst* CI = dyn_cast<CallInst>(&I)){
                    Function* CallFn = CI->getCalledFunction();
                    if(CallFn && CallFn->getName() == "__hpvm__return"){
                        return CI;
                    }
                }
            }
        }

        return (CallInst*) NULL;
    };


    Function *SrcFunc = dyn_cast<Function>(src->getArgOperand(1));
    if (!SrcFunc) {
        HPVMFatalError("Could not find function from CallInst source argument.");
    }

    Function *DstFunc = dyn_cast<Function>(dst->getArgOperand(1));
    if (!DstFunc) {
        HPVMFatalError("Could not find function from CallInst destination argument.");
    }

    CallInst* srcRet = getHPVMReturn(SrcFunc);
    if(!srcRet){
        HPVMFatalError("Src function has no hpvm_return call to insert edge!");
    }

    Argument* spArg = dyn_cast<Argument>(srcRet->getArgOperand(1+sp));
    Type *SpType = spArg->getType();
    if (!SpType) {
        HPVMFatalError("Could not get type of source node ouput");
    }

    Argument* dpArg = dyn_cast<Argument>(getArg(DstFunc,dp));

    Type *DpType = dpArg->getType();
    if (!DpType) {
        HPVMFatalError("Could not get type of destination node input");
    }

    if (SpType != DpType) {
        HPVMFatalError("Argument types do not match!");
    }

    // TODO: check if edge already exists
    if (Edges.find(spArg) != Edges.end()) {
        HPVMFatalError("Edge already exists");
    } else {
        Edges[spArg] = dpArg;
    }


    IntegerType* integerType = IntegerType::getInt32Ty(theContext.getLLVMContext());

    std::vector<Value* > arguments;
    arguments.push_back( src);
    arguments.push_back( dst);
    arguments.push_back(ConstantInt::get(integerType,ReplType));
    arguments.push_back(ConstantInt::get(integerType,sp));
    arguments.push_back(ConstantInt::get(integerType,dp));
    arguments.push_back(ConstantInt::get(integerType,isStream));


    CallInst *NewCall = this->insertCallUnchecked(InsertPoint, FuncName, HpvmEdge, arguments);
    return NewCall;
}

CallInst* HPVMCGenAttributes::insertCall(Instruction* InsertPoint, std::vector<Value *> inputs, std::vector<Value *> outputs, std::vector<Value*> privs) {
    const StringRef FuncName("__hpvm__attributes");
    Function * HpvmAttributes = theContext.getModule().getFunction(FuncName);
    if (!HpvmAttributes) {
        HPVMFatalError("__hpvm__attributes function not found in context");
    }

    unsigned inputSize = inputs.size();
    unsigned outputSize = outputs.size();

    std::vector<Value *> argsVec;
    Value *InputSize = ConstantInt::get(IntegerType::getInt32Ty(theContext.getLLVMContext()), inputSize);
    argsVec.push_back(InputSize);
    for(unsigned i = 0; i < inputSize; i++) {
        argsVec.push_back(inputs[i]);
    }
    
    Value *OutputSize = ConstantInt::get(IntegerType::getInt32Ty(theContext.getLLVMContext()), outputSize);
    argsVec.push_back(OutputSize);
    for(unsigned i = 0; i < outputSize; i++) {
        argsVec.push_back(outputs[i]);
    }

    if(privs.size()){
        Value *PrivSize = ConstantInt::get(IntegerType::getInt32Ty(theContext.getLLVMContext()), privs.size() / 2);
        argsVec.push_back(PrivSize);

        for(Value* PrivVal : privs){
            argsVec.push_back(PrivVal);
        }

    }



    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, argsVec);
    return NewCall;
}

CallInst *HPVMCGenReturn::insertCall(Instruction *InsertPoint, std::vector<Value *> returnValues) {
    const StringRef FuncName("__hpvm__return");
    Function * HpvmReturn = theContext.getModule().getFunction(FuncName);
    if (!HpvmReturn) {
        HPVMFatalError("__hpvm__return function not found in context");
    }

    Value *numReturnValues = ConstantInt::get(IntegerType::getInt32Ty(theContext.getLLVMContext()), returnValues.size());
    returnValues.insert(returnValues.begin(), numReturnValues);

    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, returnValues);
    return NewCall; 
}

CallInst *HPVMCGenGetNode::insertCall(Instruction *InsertPoint) {
    const StringRef FuncName("__hpvm__getNode");
    Function * HpvmGetNode = theContext.getModule().getFunction(FuncName);
    if (!HpvmGetNode) {
        HPVMFatalError("__hpvm__getNode function not found in context");
    }

    ArrayRef<Value *> args;
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}

CallInst *HPVMCGenGetParentNode::insertCall(Instruction *InsertPoint, Value *Node) {
    const StringRef FuncName("__hpvm__getParentNode");
    Function * HpvmGetNode = theContext.getModule().getFunction(FuncName);
    if (!HpvmGetNode) {
        HPVMFatalError("__hpvm__getParentNode function not found in context");
    }

    std::vector<Value*> arguments = { Node };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, arguments);
    return NewCall;
}

CallInst *HPVMCGenGetNodeInstanceID::insertCall(Instruction* InsertPoint, Value * Node, unsigned dimension) {
    if(dimension > 2) {
        HPVMFatalError("Detected invalid dimension: " + std::to_string(dimension));
    }

    std::string id_func = "";
    switch(dimension) {
        case 0:
            id_func = "__hpvm__getNodeInstanceID_x";
            break;
        
        case 1:
            id_func = "__hpvm__getNodeInstanceID_y";
            break;

        case 2:
            id_func = "__hpvm__getNodeInstanceID_z";
            break;
    }

    const StringRef FuncName(id_func);


    Function * HpvmGetNode = theContext.getModule().getFunction(FuncName);
    if (!HpvmGetNode) {
        HPVMFatalError("__hpvm__getNodeInstanceID function not found in context");
    }
    
    std::vector<Value*>  args = { Node };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}

CallInst *HPVMCGenGetNumNodeInstances::insertCall(Instruction* InsertPoint, Value * Node, unsigned dimension) {
    if(dimension > 2) {
        HPVMFatalError("Detected invalid dimension: " + std::to_string(dimension));
    }

    std::string dim;
    switch(dimension) {
        case 0:
            dim = "__hpvm__getNumNodeInstances_x";
            break;
        
        case 1:
            dim = "__hpvm__getNumNodeInstances_y";
            break;

        case 2:
            dim = "__hpvm__getNumNodeInstances_z";
            break;
    }


    const StringRef FuncName(dim);
    Function * HpvmGetNode = theContext.getModule().getFunction(FuncName);
    if (!HpvmGetNode) {
        HPVMFatalError("__hpvm__getNumNodeInstances function not found in context");
    }
    
    std::vector<Value*> args = { cast<Value>(Node) };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}

CallInst* HPVMCGenLaunch::insertCall(Instruction* InsertPoint, unsigned isStream, Value* rootGraph, std::vector<Value *> args) {
    const StringRef FuncName("__hpvm__launch");
    Function * HpvmLaunch = theContext.getModule().getFunction(FuncName);
    if (!HpvmLaunch) {
        HPVMFatalError("__hpvm__launch function not found in context");
    }
    // Make isStream into a value
    Value *isStreamArg = ConstantInt::get(IntegerType::getInt32Ty(theContext.getLLVMContext()), isStream);


    AllocaInst *allocaInst = HPVMCGenBase::insertAlloca(InsertPoint, args);
    Value *argsStruct = allocaInst->getOperand(0);


    std::vector<Value*> vec_args = { isStreamArg, cast<Value>(rootGraph), allocaInst };

    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, vec_args);

    return NewCall;
}

CallInst* HPVMCGenWait::insertCall(Instruction* InsertPoint, Value* Graph) {
    const StringRef FuncName("__hpvm__wait");
    Function *HpvmWait = theContext.getModule().getFunction(FuncName);
    if (!HpvmWait) {
        HPVMFatalError("__hpvm__wait function not found in context");
    }

    std::vector<Value *> arguments = { Graph };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, arguments);
    return NewCall;
}

CallInst* HPVMCGenPush::insertCall(Instruction* InsertPoint, Value* Graph, std::vector<Value *> args) {
    const StringRef FuncName("__hpvm__push");
    Function * HpvmPop = theContext.getModule().getFunction(FuncName);
    if (!HpvmPop) {
        HPVMFatalError("__hpvm__push function not found in context");
    }

    AllocaInst *allocaInst = HPVMCGenBase::insertAlloca(InsertPoint, args);
    Value *argsStruct = allocaInst->getOperand(0);

    std::vector<Value *> arguments = { cast<Value>(Graph), argsStruct };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, arguments);
    return NewCall;
}

CallInst* HPVMCGenPop::insertCall(Instruction* InsertPoint, Value* Graph) {
    const StringRef FuncName("__hpvm__pop");
    Function * HpvmPop = theContext.getModule().getFunction(FuncName);
    if (!HpvmPop) {
        HPVMFatalError("__hpvm__pop function not found in context");
    }

    std::vector<Value *> arguments = { Graph };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, arguments);
    return NewCall;
}



CallInst* HPVMCGenTrackMem::insertCall(Instruction* InsertPoint, Value *Pointer, Value *Size) {
    const StringRef FuncName("llvm_hpvm_track_mem");
    Function * HpvmTrack= theContext.getModule().getFunction(FuncName);

    if (!HpvmTrack) {
        HPVMFatalError("llvm_hpvm_track_mem function not found in context");
    }

    std::vector<Value *> arguments = { Pointer, Size };
    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, arguments);
    return NewCall;
}

CallInst* HPVMCGenUnTrackMem::insertCall(Instruction* InsertPoint, std::vector<Value *> args) {
    const StringRef FuncName("llvm_hpvm_untrack_mem");
    Function * HpvmUnTrack= theContext.getModule().getFunction(FuncName);
    if (!HpvmUnTrack) {
        HPVMFatalError("llvm_hpvm_untrack_mem function not found in context");
    }


    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}


CallInst* HPVMCGenRequestMem::insertCall(Instruction* InsertPoint, std::vector<Value *> args) {
    const StringRef FuncName("llvm_hpvm_request_mem");
    Function * HpvmReq = theContext.getModule().getFunction(FuncName);
    if (!HpvmReq) {
        HPVMFatalError("llvm_hpvm_request_mem function not found in context");
    }

    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}



CallInst* HPVMCGenHint::insertCall(Instruction* InsertPoint, std::vector<Value *> args) {
    const StringRef FuncName("__hpvm__hint");
    Function * HpvmReq = theContext.getModule().getFunction(FuncName);
    if (!HpvmReq) {
        HPVMFatalError("__hpvm__hint function not found in context");
    }

    CallInst *NewCall = HPVMCGenBase::insertCall(InsertPoint, FuncName, args);
    return NewCall;
}
