#include "DAGInfo.h"
void HPVMDAGInfo::printDAGInfoHelper(Function* NodeFunction, string prefix){
    LLVM_DEBUG(errs() <<prefix<<" Function: "<<NodeFunction->getName()<<"\n");

    NodeInfo* NDInfo = getOrInsertNodeInfo(NodeFunction);


    if(NDInfo->isInternalNode){
        LLVM_DEBUG(errs() << prefix <<" NodeType: Internal\n");
        LLVM_DEBUG(errs() << prefix <<" Edges:\n");
        for(CallInst* Edge : NDInfo->EdgeCalls){
            LLVM_DEBUG(errs() << prefix <<"\t"<<*Edge<<" "
                    << "("<< *getArgForEdge(Edge) <<")"<<"\n");
        }

        LLVM_DEBUG(errs() << prefix <<" BindIns:\n");
        for(CallInst* BI : NDInfo->BindInCalls){
            LLVM_DEBUG(errs() << prefix <<"\t"<<*BI<<" "
                    <<"("<< *getArgForBindIn(BI)<<")"<<"\n");
        }


        LLVM_DEBUG(errs() << prefix <<" BindOut:\n");
        for(CallInst* BO : NDInfo->BindOutCalls){
            LLVM_DEBUG(errs() << prefix <<"\t"<<*BO<<" "
                    <<"("<< *getArgForBindOut(BO)<<")"<<"\n");
        }

        for(CallInst* CreateNodeNDCall : NDInfo->CreateNodeNDCalls){
            printDAGInfoHelper(getCreateNodeNDFunction(CreateNodeNDCall), prefix+"-----------------------");
        }


    } else {
        LLVM_DEBUG(errs() << prefix <<" NodeType: Leaf\n");
        LLVM_DEBUG(errs() << prefix <<" Attributes : "<<*(NDInfo->AttributeCall)<<"\n");
        LLVM_DEBUG(errs() << prefix <<" Return : "<<*(NDInfo->ReturnCall)<<"\n");
        LLVM_DEBUG(errs() << prefix <<"\n");
    }

}

void HPVMDAGInfo::printDAGInfo(){

    for(Function* Root: RootNodes){
        printDAGInfoHelper(Root, "--");
    }

}


NodeInfo* HPVMDAGInfo::getOrInsertNodeInfo(Function* F){
    if(NodeInfoMap.find(F) == NodeInfoMap.end()){
        NodeInfoMap[F] = new NodeInfo(F);

    }

    return NodeInfoMap[F];
}


Argument* HPVMDAGInfo::getParentArgumentForInputIdx(Function* Node, int input_idx, Function* ParentNode){
    LLVM_DEBUG(errs() << "getParentArgumentForInputIdx invoked on "<<ParentNode->getName() <<" for child node "<<Node->getName()<<" and input idx "<< input_idx << "\n");
    NodeInfo* ParentNDInfo = getOrInsertNodeInfo(ParentNode);



    // Get formal argument of function at index
    auto getArg = [](Function* F, unsigned idx) -> Argument* {
        Argument* arg = dyn_cast<Argument>((F->arg_begin() + idx));
        return arg;
    };


    // The value incoming through input_idx may come through
    // a bind in call or an edge call in the parent. Hence
    // we first check through bind in and then edges.


    for(CallInst* BI : ParentNDInfo->BindInCalls){
        CallInst* NodeCall = dyn_cast<CallInst>(BI->getArgOperand(0));
        Function* NodeFn = getCreateNodeNDFunction(NodeCall);

        if(NodeFn == Node){
            ConstantInt* DstIdx = dyn_cast<ConstantInt>(BI->getArgOperand(2));
            assert(DstIdx && "BindIn must use constant values");
            int dst_idx_value = DstIdx->getSExtValue();

            if(dst_idx_value != input_idx) continue;

            ConstantInt* SrcIdx = dyn_cast<ConstantInt>(BI->getArgOperand(1));
            assert(SrcIdx && "BindIn must use constant values");
            int src_idx_value = SrcIdx->getSExtValue();

            // We can directly identify the parent's sp argument
            return getArg(ParentNode, src_idx_value);
        }

    }

    // If the incoming value was instead through an edge

    for(CallInst* E : ParentNDInfo->EdgeCalls){

        // Node must be on the dst side of the edge
        CallInst* NodeCall = dyn_cast<CallInst>(E->getArgOperand(1));
        Function* NodeFn = getCreateNodeNDFunction(NodeCall);

        if(NodeFn != Node) continue;

        ConstantInt* DstIdx = dyn_cast<ConstantInt>(E->getArgOperand(4));
        assert(DstIdx && "Edge must use constant values");
        int dst_idx_value = DstIdx->getSExtValue();

        if(dst_idx_value != input_idx) continue;

        // Correspondign edge found! Now use the parent node's edge map
        // to get the corresponding argument. Topological ordering ensures that
        // the edge would have been updated correctly by previous nodes.

        map<CallInst*,Argument*>& EdgeMap = ParentNDInfo->EdgeMap;

        assert(EdgeMap.find(E) != EdgeMap.end() && "Edge information should have been processed!");

        return EdgeMap[E];

    }

    assert(false && "Illegal HPVM DAG description");
    return nullptr;

    

}

void HPVMDAGInfo::updateOutGoingFlows(Function* Node, int output_idx, Argument* CallerArg, Function* ParentNode){
    NodeInfo* ParentNDInfo = getOrInsertNodeInfo(ParentNode);

    for(CallInst* E : ParentNDInfo->EdgeCalls){

        // Node must be on the src side of the edge
        CallInst* NodeCall = dyn_cast<CallInst>(E->getArgOperand(0));
        Function* NodeFn = getCreateNodeNDFunction(NodeCall);

        if(NodeFn != Node) continue;

        ConstantInt* SrcIdx = dyn_cast<ConstantInt>(E->getArgOperand(3));
        assert(SrcIdx && "Edge must use constant values");
        int src_idx_value = SrcIdx->getSExtValue();

        if(src_idx_value != output_idx) continue;


        map<CallInst*,Argument*>& EdgeMap = ParentNDInfo->EdgeMap;
        EdgeMap[E] = CallerArg;

    }


    for(CallInst* BO : ParentNDInfo->BindOutCalls){

        CallInst* NodeCall = dyn_cast<CallInst>(BO->getArgOperand(0));
        Function* NodeFn = getCreateNodeNDFunction(NodeCall);

        if(NodeFn != Node) continue;

        ConstantInt* SrcIdx = dyn_cast<ConstantInt>(BO->getArgOperand(1));
        assert(SrcIdx && "BindOut must use constant values");
        int src_idx_value = SrcIdx->getSExtValue();

        if(src_idx_value != output_idx) continue;


        map<CallInst*,Argument*>& BOMap = ParentNDInfo->BindOutMap;
        BOMap[BO] = CallerArg;

        ConstantInt* DstIdx = dyn_cast<ConstantInt>(BO->getArgOperand(2));
        assert(DstIdx && "BindOut must use constant values");
        int dst_idx_value = DstIdx->getSExtValue();

        // int parent_output_idx = dst_idx_value;

        // Recurse upwards and propogate flow.


    }

}



void HPVMDAGInfo::runOnNode(Function* Node){

    NodeInfo* NDInfo = getOrInsertNodeInfo(Node);


    // First process entire sub graph (if internal node)
    for(CallInst* CreateNodeCall : NDInfo->CreateNodeNDCalls){
        Function* ChildNode = getCreateNodeNDFunction(CreateNodeCall);
        runOnNode(ChildNode);
    }

    // If the current function is the leaf node
    // use the __hpvm__return call to map the
    // incoming value to an edge or a bindout call
    // in the parent's context
    if(!NDInfo->isInternalNode){
        CallInst* ReturnCall = NDInfo->ReturnCall;

        for(unsigned i = 1; i < ReturnCall->getNumArgOperands(); i++){
            int output_idx = i - 1; // the index outgoing from the current node
            Argument* RetArg = dyn_cast<Argument>(ReturnCall->getArgOperand(i));

            assert(RetArg && "Value used in __hpvm__return must be an argument of the caller");

            int input_idx = RetArg->getArgNo(); // the index incoming into the current node

            vector<Function*> CallerNodes = getParentNodes(Node);

            // May have multiple distinct callers
            for(Function* ParentNode : CallerNodes){

                Argument* CallerArg = getParentArgumentForInputIdx(Node, input_idx, ParentNode);


                // Update any corresponding out going flow in parent node.
                // Could be an out-going edge or a bindout (in which case
                // we recurse upwards)
                updateOutGoingFlows(Node, output_idx, CallerArg, ParentNode);
            }
        }

    } else {
        // If the current node, N is an internal node, it's child subgraph would
        // have already updated the out-going edges/bindout's for N. We simply propogate
        // BindOut information upwards the graph.

        map<CallInst*, Argument*>& BOMap = NDInfo->BindOutMap;
        for(CallInst* BO: NDInfo->BindOutCalls){

            vector<Function*> CallerNodes = getParentNodes(Node);
            // Incase of the root node which may not have any parent nodes

            if(!CallerNodes.size()) return;

            // Topological ordering ensures that all incoming edges
            // and flows must have been processed before the outgoing
            // edges for each node.
            assert(BOMap.find(BO) != BOMap.end() && "Bindout should have been updated by child subgraph");

            Argument* CurrentNodeArg = BOMap[BO];
            int input_idx =  CurrentNodeArg->getArgNo();

            for(Function* ParentNode : CallerNodes){

                Argument* CallerArg = getParentArgumentForInputIdx(Node, input_idx, ParentNode);

                ConstantInt* OutDstIdx = dyn_cast<ConstantInt>(BO->getArgOperand(2));

                assert(OutDstIdx && "BindOut intrinsic must have a constant integer for op argument");

                int parent_output_idx = OutDstIdx->getSExtValue();

                updateOutGoingFlows(Node, parent_output_idx, CallerArg, ParentNode);

            }


        }

    }



}


Argument* HPVMDAGInfo::getArgForBindIn(CallInst* BI){
    Function* CF = BI->getCalledFunction();
    assert(CF && CF->getName() == BindInFuncName && "Callee is not BindIn");

    // We do not need NodeInfo for this function
    // since we can directly identify the corresponding
    // parent argument.
    Function* Caller = BI->getParent()->getParent();

    ConstantInt* OP = dyn_cast<ConstantInt>(BI->getArgOperand(1));

    assert(OP && "Bindin call must have a constant integer for ip argument");

    int parent_input_idx = OP->getSExtValue();

    return dyn_cast<Argument>((Caller->arg_begin()) + parent_input_idx);

}

Argument* HPVMDAGInfo::getArgForBindOut(CallInst* BO){
    Function* CF = BO->getCalledFunction();
    assert(CF && CF->getName() == BindOutFuncName && "Callee is not BindOut");

    Function* Caller = BO->getParent()->getParent();

    NodeInfo* NDInfo = getOrInsertNodeInfo(Caller);

    map<CallInst* , Argument*>& BOMap = NDInfo->BindOutMap;

    assert(BOMap.find(BO) != BOMap.end() && "BindOut argument not known");

    return BOMap[BO];

}

Argument* HPVMDAGInfo::getArgForEdge(CallInst* E){
    Function* CF = E->getCalledFunction();
    assert(CF && CF->getName() == EdgeFuncName && "Callee is not HPVM Edge");

    Function* Caller = E->getParent()->getParent();

    NodeInfo* NDInfo = getOrInsertNodeInfo(Caller);

    map<CallInst* , Argument*>& EdgeMap = NDInfo->EdgeMap;

    assert(EdgeMap.find(E) != EdgeMap.end() && "Edge argument not known");

    return EdgeMap[E];

}

bool HPVMDAGInfo::isInternalNode(Function* Node){
    NodeInfo* NDInfo = getOrInsertNodeInfo(Node);

    return NDInfo->isInternalNode;
}


vector<CallInst*> HPVMDAGInfo::getBindInCalls(Function* Node){
    NodeInfo* NDInfo = getOrInsertNodeInfo(Node);
    return NDInfo->BindInCalls;

}

vector<CallInst*> HPVMDAGInfo::getBindOutCalls(Function* Node){
    NodeInfo* NDInfo = getOrInsertNodeInfo(Node);
    return NDInfo->BindOutCalls;
}

vector<CallInst*> HPVMDAGInfo::getEdgeCalls(Function* Node){
    NodeInfo* NDInfo = getOrInsertNodeInfo(Node);
    vector<CallInst*> Edges;

    // EdgeCalls is a set
    for(CallInst* E : NDInfo->EdgeCalls){
        Edges.push_back(E);
    }

    return Edges;
}

vector<CallInst*> HPVMDAGInfo::getCreateNodeNDCalls(Function* Node){
    NodeInfo* NDInfo = getOrInsertNodeInfo(Node);
    return NDInfo->CreateNodeNDCalls;
}


vector<Function*> HPVMDAGInfo::getParentNodes(Function* NodeFn){
    NodeInfo* NDInfo = getOrInsertNodeInfo(NodeFn);
    vector<Function*> Parents;
    std::set<Function*>& NodeParentsSet = NDInfo->Parents;
    Parents.insert(Parents.end(), NodeParentsSet.begin(), NodeParentsSet.end());
    return Parents;
}



vector<Function*> HPVMDAGInfo::getSubGraph(Function* NodeFn){
    std::set<Function*> NodeSet; // To handle duplication of nodes
    queue<Function*> Pipeline;
    Pipeline.push(NodeFn);

    while(!Pipeline.empty()){
        Function* Node = Pipeline.front();
        Pipeline.pop();

        NodeSet.insert(Node);

        auto ChildCreateNodes = getCreateNodeNDCalls(Node);


        for(CallInst* CI : ChildCreateNodes){
            Pipeline.push(getCreateNodeNDFunction(CI));
        }
    }

    vector<Function*> Nodes;

    Nodes.insert(Nodes.end(), NodeSet.begin(), NodeSet.end());

    return Nodes;

}

void HPVMDAGInfo::sortNodesInTopologicalOrder(vector<Function*> Nodes){
    auto cmpNodes = [&](Function* F1, Function* F2) -> bool {

        vector<Function*> Parents = getParentNodes(F2);

        for(Function* Parent : Parents){
            if(Parent == F1) return true;
        }

        return false;
    };


    sort(Nodes, cmpNodes);

}

vector<Function*> HPVMDAGInfo::getDAGInTopologicalOrder(Function* RootNode){
    vector<Function*> Nodes = getSubGraph(RootNode);
    sortNodesInTopologicalOrder(Nodes);
    return Nodes;
}


HPVMDAGInfo* HPVMDAGInfoAnalysis(Module &M)
{
  LLVM_DEBUG(errs() << "=== HPVMDAGInfoAnalysis ===\n");
  ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

  auto* HPVMInfo = new HPVMDAGInfo(M);

  return HPVMInfo;

}
