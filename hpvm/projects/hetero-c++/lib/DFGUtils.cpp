#include "DFGUtils.h"

Function* getCreateNodeNDFunction(CallInst* CI){
    return dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());
}


// Return a list of immediate children nodes
// of a particular Node function F.
vector<Function*> getChildRoots(Function* F){

    vector<Function*> Children;

    for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
        Instruction* II = &*I;

        CallInst* CI = dyn_cast<CallInst>(II);

        if(CI && CI->getCalledFunction() && CI->getCalledFunction()->getName() == "__hpvm__createNodeND"){

            Function* Child = getCreateNodeNDFunction(CI); // dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());
            Children.push_back(Child);
        }
    }

    return Children;
}

// Return a list of immediate children nodes
// calls of a particular Node function F.
vector<CallInst*> getChildRootsCalls(Function* F){

    vector<CallInst*> Children;

    for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
        Instruction* II = &*I;

        CallInst* CI = dyn_cast<CallInst>(II);
        if(CI && CI->getCalledFunction() && CI->getCalledFunction()->getName() == "__hpvm__createNodeND"){
            Children.push_back(CI);
        }
    }

    return Children;
}

// Return whether a node is a 0D, 1D, 2D, or 3D node
int getTotalNumDimension(CallInst* NodeCall){
    Value* DimValue = NodeCall->getArgOperand(0);

    ConstantInt* C = dyn_cast<ConstantInt>(DimValue);

    assert(C && "__hpvm__createNodeND number of dimensions must be a constant integer");

    return C->getSExtValue();
}


Value* getNDDimensionValue(int N, CallInst* CI){
    assert(N < getTotalNumDimension(CI) && "Dimension access out of bound");
    return CI->getArgOperand(1+N);
}

vector<CallInst*> getDataFlowCalls(CallInst* NodeCall, string Intrinsic){

    Function* F = NodeCall->getParent()->getParent();
    vector<CallInst*> Calls;

    for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
        Instruction* II = &*I;

        CallInst* CI = dyn_cast<CallInst>(II);
        if(CI && (CI->getCalledFunction()->getName() == Intrinsic) && CI->hasArgument(NodeCall)){
            Calls.push_back(CI);
        }
    }
    return Calls;
}




bool isCreateNodeND(CallInst* CI){
    Function* CF = CI->getCalledFunction();

    if(CF && CF->getName() == "__hpvm__createNodeND"){
        return true;
    }
    return false;
}


// A DFG node is redundant if it only has one child
// node, and the dimensionality of that child node
// is the same as the parent.
bool isChildNodeRedundant(CallInst* NodeCall){
    Function* NodeF = getCreateNodeNDFunction(NodeCall);

    vector<CallInst*> ChildNodes = getChildRootsCalls(NodeF); 

    
    if(ChildNodes.size() != 1) return false;

    CallInst* ChildCall = ChildNodes[0];

    int ParentDim = getTotalNumDimension(NodeCall);
    int ChildDim = getTotalNumDimension(ChildCall);

    if(ParentDim != ChildDim) return false;

    vector<CallInst*> ChildBindIns = getDataFlowCalls(ChildCall,"__hpvm__bindIn");

    // Sort the bind ins according to child nodes formal
    // argument order
    auto BindCmp = [] (CallInst* C1, CallInst* C2) -> bool {

        ConstantInt* N1 = dyn_cast<ConstantInt>(C1->getArgOperand(2));
        ConstantInt* N2 = dyn_cast<ConstantInt>(C2->getArgOperand(2));

        return N1->getSExtValue() < N2->getSExtValue();
    };

    sort(ChildBindIns, BindCmp);


    for(int i = 0 ; i < ChildDim; i++){
        
        Value* NDDimLimit = getNDDimensionValue(i, ChildCall); 

        Argument* NDArg = dyn_cast<Argument>(NDDimLimit);
        // Conservatively, if any of the
        // dimension limits are locally
        // calculated, they can be different
        if (!NDArg)
            return false;

        // Identify the formal argument corresponding
        // to this dimensions limits
        int formalArgIdx = NDArg->getArgNo();

        CallInst* BindInForArg = ChildBindIns[formalArgIdx];

        ConstantInt* OP = dyn_cast<ConstantInt>(BindInForArg->getArgOperand(1));

        int parentFormalArgIdx = OP->getSExtValue();

        Value* PLimit = getNDDimensionValue(i,NodeCall);

        Argument* PNDArg = dyn_cast<Argument>(PLimit);

        if(!PNDArg)
            return false;


        // if the dimension limits along a particular
        // axis do not match, then they are not
        // redundant
        if (PNDArg->getArgNo() != parentFormalArgIdx)
            return false;

    }

    return true;
}

bool isInternalNode(Function* F){
    return getChildRoots(F).size() != 0;

}

void mapParentOutputToChild(CallInst* Parent, CallInst* Child, map<int,int>& BindOutMap){
    vector<CallInst*> BindOuts =  getDataFlowCalls(Child, "__hpvm__bindOut");

    for(CallInst* BindOut : BindOuts){
        ConstantInt* ChildId = dyn_cast<ConstantInt>(BindOut->getArgOperand(1)); 
        ConstantInt* ParentId = dyn_cast<ConstantInt>(BindOut->getArgOperand(2)); 

        assert(ChildId && ParentId && "BindOut indices must be constant integers");

        BindOutMap[ParentId->getSExtValue()] = ChildId->getSExtValue();
    }

}

void mapParentInputToChild(CallInst* Parent, CallInst* Child, map<int,int>& BindInMap){
    vector<CallInst*> BindIns =  getDataFlowCalls(Child, "__hpvm__bindIn");

    for(CallInst* BindIn : BindIns){
        ConstantInt* ChildId = dyn_cast<ConstantInt>(BindIn->getArgOperand(2)); 
        ConstantInt* ParentId = dyn_cast<ConstantInt>(BindIn->getArgOperand(1)); 

        assert(ChildId && ParentId && "BindIn indices must be constant integers");

        BindInMap[ParentId->getSExtValue()] = ChildId->getSExtValue();
    }

}


// Remove Mid from the DFG and correctly match from Pre to Post 
// At this point we know that the Post's 'shape' is the same as Mids
// , though it's argument ordering may vary. 
//
// We can reuse any existing hpvm intrinsics in Pre which were originally
// for Mid, however we will approriately update any indices to match
// the structure of Post.
void removeRedundantNode(CallInst* Pre, CallInst* Mid, CallInst* Post){

    Function* PostFn = Post->getCalledFunction();

    Module* M = Pre->getParent()->getParent()->getParent();

    // Mapping the BindOut from output of 
    // Mid (inside the function called by Pre) to  child of Mid (Post)
    map<int,int> BindOutIdxMap;
    map<int,int> BindInIdxMap;

    // Populate maps
    mapParentOutputToChild(Mid, Post, BindOutIdxMap);
    mapParentInputToChild(Mid, Post, BindInIdxMap);


    // Update BindIn calls
    vector<CallInst*> BindIns =  getDataFlowCalls(Mid, "__hpvm__bindIn");

    for(CallInst* BI : BindIns){
        ConstantInt* OrigIdx = dyn_cast<ConstantInt>(BI->getArgOperand(2));
        ConstantInt* NewIdx = ConstantInt::get(Type::getInt32Ty(M->getContext())
                ,BindInIdxMap[OrigIdx->getSExtValue()]);

        BI->setArgOperand(2, NewIdx);
    }



    // Update BindOut Calls
    vector<CallInst*> BindOuts =  getDataFlowCalls(Mid, "__hpvm__bindOut");

    for(CallInst* BO : BindOuts){
        ConstantInt* OrigIdx = dyn_cast<ConstantInt>(BO->getArgOperand(1));
        ConstantInt* NewIdx = ConstantInt::get(Type::getInt32Ty(M->getContext())
                ,BindOutIdxMap[OrigIdx->getSExtValue()]);

        BO->setArgOperand(1, NewIdx);
    }


    // Update Edges, we have to modify both incoming and outgoing
    vector<CallInst*> Edges =  getDataFlowCalls(Mid, "__hpvm__edge");

    for(CallInst* E : Edges){
        
        // If this is an outgoing edge from Mid
        if(E->getArgOperand(0) == Mid){
            ConstantInt* OrigIdx = dyn_cast<ConstantInt>(E->getArgOperand(3));
            ConstantInt* NewIdx = ConstantInt::get(Type::getInt32Ty(M->getContext())
                ,BindOutIdxMap[OrigIdx->getSExtValue()]);

            // i.e. replace sp
            E->setArgOperand(3, NewIdx);
        } else {
            // else it's an incomming edge into Mid
            ConstantInt* OrigIdx = dyn_cast<ConstantInt>(E->getArgOperand(4));
            ConstantInt* NewIdx = ConstantInt::get(Type::getInt32Ty(M->getContext())
                ,BindInIdxMap[OrigIdx->getSExtValue()]);

            // i.e. replace dp
            E->setArgOperand(4, NewIdx);
        }
            
    }


    
    
    // Update CreateNodeND Call
    Mid->setArgOperand(1, getCreateNodeNDFunction(Post));
   
}

bool isParSectionBeginMarker(CallInst* CI){
    return (CI->getCalledFunction()->getName() == "__hpvm_parallel_section_begin"
            || CI->getCalledFunction()->getName() == "__hetero_section_begin");
}


bool isParSectionEndMarker(CallInst* CI){
    return (CI->getCalledFunction()->getName() == "__hpvm_parallel_section_end"
            || CI->getCalledFunction()->getName() == "__hetero_section_end");

}

bool isParallelLoopMarker(CallInst* CI){
    return (CI->getCalledFunction()->getName() == "__hpvm_parallel_loop"
            || CI->getCalledFunction()->getName() == "__hetero_parallel_loop"
);
}

bool isTaskBeginMarker(CallInst* CI){
    return (CI->getCalledFunction()->getName() == "__hpvm_task_begin" 
            || CI->getCalledFunction()->getName() == "__hetero_task_begin");
;
}

bool isLaunchBeginMarker(CallInst* CI){
    return (CI->getCalledFunction()->getName() == "__hpvm_launch_begin"
            || CI->getCalledFunction()->getName() == "__hetero_launch_begin");
}

CallInst* getMarkerCall(Function* F){
    CallInst* Marker = nullptr;
    for(auto& BB: *F){
        for(auto& II: BB){
            CallInst* CI = dyn_cast<CallInst>(&II);

            if(!CI) continue;
            if(isParSectionBeginMarker(CI)){
                return Marker;
            }
            if(isParallelLoopMarker(CI)
                    ||  isTaskBeginMarker(CI)
                    ||  isLaunchBeginMarker(CI)){
                Marker = CI;
                return CI;
            }

        }
    }
    return Marker;
}


vector<Value*> getSectionArgOrdering(Function* SecFn){
    vector<Value*> Ordering;

    //Function* SecFn = CI->getCalledFunction();
    for(auto &BB: *SecFn){
        for(auto &I: BB){
            CallInst* CI = dyn_cast<CallInst>(&I);
            if(!CI) continue;

            if(CI->getCalledFunction()->getName() == "__hpvm__order"){

                for(int i = 1; i < CI->getNumArgOperands(); i++){
                    Ordering.push_back(CI->getArgOperand(i));
                }

            }
        }
    }

    return Ordering;
}


Function* CreateClone(Function* Orig, std::set<Argument*> Exclude, ValueToValueMapTy& ArgMap){
    
    //If No Argument to Exclude then
    // return original function
    if(!Exclude.size()){
        LLVM_DEBUG(dbgs()<<"Exclude Empty!\n");
    }
    // Get the CallInst operand Index for Value V.
    auto getValueIndexCI = [&](CallInst* CI, Value* V)-> int {
        int ici = 0;
        for(; ici <CI->getNumArgOperands(); ici++){
            Value* valArg = CI->getArgOperand(ici);

            if(valArg == V){
                return ici;
            }
        }
        LLVM_DEBUG(dbgs()<<"CallInst: "<<*CI<<"\n");
        LLVM_DEBUG(dbgs()<<"Value giving error: "<<*V<<"\n");
        assert(false && "Value isnt part of Function Call");
        return ici;
    };

    CallInst* OrigMarker = getMarkerCall(Orig);
    vector<Value*> Ordering;

    if(!OrigMarker){
        LLVM_DEBUG(dbgs()<<"No Marker Found...\n");
        Ordering = getSectionArgOrdering(Orig);
        //return Orig;
    }

    vector<Type*> NewArgTypes;
    vector<Argument*> NewArgs;

    // Assuming maps original value to cloned value
    ValueToValueMapTy VMap;


    // Identify Argument Types to include in new clone
    for(auto ai = Orig->arg_begin(), ae = Orig->arg_end();
            ai != ae; ai++){
        Argument* OrigArg = &*ai;

        if(Exclude.find(OrigArg) != Exclude.end()){
            continue;
        } 

        NewArgs.push_back(OrigArg);
    }

    auto cmpArgs = [&] (Argument* A1, Argument* A2) -> bool {
        if(OrigMarker){
            int idx1 = getValueIndexCI(OrigMarker, (Value*) A1);//std::find(Ordering.begin(), Ordering.end(),A1) - Ordering.begin();
            int idx2 = getValueIndexCI(OrigMarker, (Value*) A2);//std::find(Ordering.begin(), Ordering.end(),A2) - Ordering.begin();

            return (idx1 < idx2);
        } else {
            int idx1 = std::find(Ordering.begin(), Ordering.end(),(Value*) A1) - Ordering.begin();
            int idx2 = std::find(Ordering.begin(), Ordering.end(),(Value*) A2) - Ordering.begin();
            return (idx1 < idx2);
        }
    };

    // Sort new prototype to have 
    // required argument ordering for HPVM
    // Backends
    sort(NewArgs, cmpArgs);

    for(auto Arg: NewArgs){
        NewArgTypes.push_back(Arg->getType());
    }


    // Create New Function Type with only non-excluded arguments
    FunctionType* OrigFType = Orig->getFunctionType();
    FunctionType* NewFType = FunctionType::get(OrigFType->getReturnType(),
            NewArgTypes, OrigFType->isVarArg() );

    Function* NewFunction = Function::Create(NewFType, Function::ExternalLinkage);//Orig->getLinkage());
    NewFunction->copyAttributesFrom(Orig);
    Orig->getParent()->getFunctionList().push_back(NewFunction);

    NewFunction->setName(Orig->getName()+"_reorder");
    LLVM_DEBUG(dbgs()<<"New Function: "<<*NewFunction<<"\n");
    

    unsigned idx = 0;
    for(auto na = NewFunction->arg_begin(); na != NewFunction->arg_end();
            na++){

        Argument* NArg = &*na;
        Argument* OArg = NewArgs[idx++];

        NArg->setName(OArg->getName());

        VMap[OArg] = NArg;
        ArgMap[OArg] = NArg;
    }


    LLVM_DEBUG(dbgs()<<"Modified New Prototype: "<<*NewFunction<<"\n");
    //dbgs()<<"Original Function:\n"<<*Orig<<"\n";

    // Check whether an instruction should be removed
    // in cloned function
    auto CleanInst = [&](Instruction* ii){
        for(unsigned j =0; j < ii->getNumOperands(); j++){
            Argument* ValArg = dyn_cast<Argument>(ii->getOperand(j));

            if(!ValArg) continue;

            if(Exclude.find(ValArg) == Exclude.end()) continue;

            return true;

        }
        return false;
    };

    // Iterate over BasicBlock and remove Exclude arguments insts
    auto CleanBasicBlock = [&](BasicBlock* bb, ValueToValueMapTy& vmap){
        for(auto II= bb->begin(), IE = bb->end();
                II != IE; ){
            Instruction* CurInst = &*II;
            II++;

            // If Instruction has to be removed
            if(CleanInst(CurInst)){
                //dbgs()<<"Instruction to remove: "<<*CurInst<<"\n"; 
                //vmap[CurInst]->replaceAllUsesWith(UndefValue::get(vmap[CurInst]->getType()));
                //dbgs()<<"vmap[CurInst]: "<<*vmap[CurInst]<<"\n";
                Instruction* MappedInst = dyn_cast<Instruction>(vmap[CurInst]);

                if(MappedInst){
                    LLVM_DEBUG(dbgs()<<"Removing :"<<*MappedInst<<"\n");
                    MappedInst->replaceAllUsesWith(UndefValue::get(MappedInst->getType()));
                    MappedInst->eraseFromParent();
                }

            }         
        }

    };


    map<BasicBlock*, BasicBlock*> BBMap;
    // For each BasicBlock in the Original
    // Function make a copy and remove any instructions
    // which reference Exlcuded Arguments
    for(auto &BB: *Orig){
        BasicBlock* newBB = CloneBasicBlock(&BB, VMap,"_cloned", NewFunction);
        VMap[&BB] = newBB;
        BBMap[&BB] = newBB;

        if(Exclude.size())
            CleanBasicBlock(&BB,VMap);
    }

    for(auto &BB: *Orig){
        BasicBlock* newBB = BBMap[&BB];
        if(/*true ||*/ BB.hasAddressTaken()){
            Constant *OldBBAddr = BlockAddress::get(const_cast<Function*>(Orig), const_cast<BasicBlock*>(&BB));
            VMap[OldBBAddr] = BlockAddress::get(NewFunction, newBB);
        }
    }

    for(auto BB = cast<BasicBlock>(VMap[&Orig->front()])->getIterator(), BE = NewFunction->end();
            BB != BE; ++BB){
        //dbgs()<<*BB<<"\n";
        for(auto &II : *BB){
            LLVM_DEBUG(dbgs()<<II<<"\n");
            //if(isCreateNodeCall(dyn_cast<CallInst>(&II))) continue;

            LLVM_DEBUG(errs()<<"II has "<<II.getNumOperands()<<"\n");
            for(int l = 0; l < II.getNumOperands(); l++){
                Value* lop = II.getOperand(l);
                if(VMap.find(lop) == VMap.end()){
                    LLVM_DEBUG(errs()<<*lop<<" not in VMap "<<", orig inst "<<II<<"\n");
                }
            }
            LLVM_DEBUG(errs()<<"New Function "<<*NewFunction<<"\n");
            LLVM_DEBUG(errs()<<"Old Function"<<*Orig<<"\n");

            LLVM_DEBUG(errs()<<"New BB"<<*BB<<"\n");

            RemapInstruction(&II, VMap);
        }
    }



    if(OrigMarker){
        CallInst* NewFMarker = getMarkerCall(NewFunction);
        //OrigMarker->eraseFromParent();
        //NewFMarker->eraseFromParent();
    }


    
    LLVM_DEBUG(dbgs()<<"Old Function: "<<*Orig<<"\n");
    LLVM_DEBUG(dbgs()<<"Extracted Function: "<<*NewFunction<<"\n");

    return NewFunction;
    

}



CallInst* CloneCallInst(CallInst* OrigCall, Function* NewF, 
        std::set<Argument*>& Exclude, ValueToValueMapTy& ArgMap){

    Function* Orig = OrigCall->getCalledFunction();
    int newArgNo = OrigCall->getNumArgOperands() - Exclude.size();
    LLVM_DEBUG(dbgs()<<"New Argument number:\t"<<newArgNo<<"\n");
    vector<Value*> NewCallArgs(newArgNo);


    


    int origCallIdx = 0;
    // Identify Argument Types to include in new clone
    for(auto ai = Orig->arg_begin(), ae = Orig->arg_end();
            ai != ae; ai++, origCallIdx++){
        Argument* OrigArg = &*ai;

        if(Exclude.find(OrigArg) != Exclude.end()){
            continue;
        } 

        Argument* NArg = cast<Argument>(ArgMap[OrigArg]);

        int newCallIdx = NArg->getArgNo();


        NewCallArgs[newCallIdx] = OrigCall->getArgOperand(origCallIdx);
        LLVM_DEBUG(dbgs()<<"Inserting Original argument at idx\t"<<origCallIdx<<" into New Argument at idx:\t"<<newCallIdx<<"\n");
    }

    ArrayRef<Value*> NewCallValue(NewCallArgs); 
    CallInst* NewCall = CallInst::Create(NewF->getFunctionType(), NewF,NewCallValue);

    NewCall->insertAfter(OrigCall);

    LLVM_DEBUG(dbgs()<<"Old Call:\t"<<*OrigCall<<"\n");
    LLVM_DEBUG(dbgs()<<"New Call:\t"<<*NewCall<<"\n");



    return NewCall;
}

bool FuncContainsCall(Function *ToSearch, Function *Contain) {
    for (auto InstIter = inst_begin(ToSearch); InstIter != inst_end(ToSearch); ++InstIter) {
        CallInst *CI = dyn_cast<CallInst>(&*InstIter);
        
        if (CI) {
            Function *F = CI->getCalledFunction();
            if (F == Contain) {
                return true;
            }
        }
    }

    return false;
}


// Check if BB (which is known to be dominated by Begin) visits
// Begin before visiting End in post order traversal. This is to facilitate
// Post domination check in code extraction when identifying basic blocks to
// extract.
bool BBPrecedesEnd(BasicBlock* BB, BasicBlock* Begin, BasicBlock* End){
    std::queue<BasicBlock*> PostOrdTrav;
    
    PostOrdTrav.push(BB);

    while(PostOrdTrav.size()){
        BasicBlock* Trav = PostOrdTrav.front();
        PostOrdTrav.pop();


        if(Trav == Begin)
            return true;

        if(Trav == End)
            return false;


        for(BasicBlock* Pred : predecessors(Trav)){
            PostOrdTrav.push(Pred);
        }
    }

    assert(false && "Begin must dominate BB");
    return false;
}


void getHPVMPrivArgs(CallInst* CI,std::vector<Value*>& PrivArguments){
    ConstantInt* C = dyn_cast<ConstantInt>(CI->getArgOperand(0));

    assert(C && "__hpvm_priv first argument must be a constant integer");

    unsigned numPairs = C->getSExtValue();

    assert(CI->getNumArgOperands() % 2 && "__hpvm_priv call must have an odd number of arguments");

    for(unsigned i = 0; i < numPairs; i++){
        unsigned ptrIdx = 1 + (2*i);

        Value* PtrVal = CI->getArgOperand(ptrIdx);
        Value* SizeVal = CI->getArgOperand(ptrIdx+1);

        assert(PtrVal->getType()->isPointerTy() 
                && "Pointer argument expected in __hpvm_priv call");

        assert(isa<ConstantInt>(SizeVal) && "Expected Constant Integer Size value in __hpvm_priv call");

        PrivArguments.push_back(PtrVal);
        PrivArguments.push_back(SizeVal);

    }


}




void cleanFunctionName(Function* F){
    assert(F && "Passed NULL to cleanFunctionName");

    std::string FuncName = F->getName().str();
    std::replace(FuncName.begin(), FuncName.end(), '.','_');

    F->setName(FuncName);

}




void copyDefInRegion(Instruction* I,Instruction* BeginMarker, Instruction* EndMarker, ValueToValueMapTy &VMap, ValueToValueMapTy& RevVMap){

    if(isa<PHINode>(I)) return;

    Function* ParentF = I->getParent()->getParent();

    DominatorTree DT(*ParentF);
    // PostDominatorTree PDT(*ParentF);

    // Check whether instruction def falls outside task boundaries
    auto isOutsideBoundary = [&](Instruction* V) -> bool {
        return (DT.dominates(EndMarker, V) || DT.dominates(V,BeginMarker)) 
            && (!cast<CallInst>(BeginMarker)->hasArgument(V));
    };

    vector<Value*> toClone;

    std::set<Value*> Cloned;
    std::queue<Value*> CloneWorkList;


    // For the current instruction, if any of it's non-constant
    // operands are defined outside the task boundaries,
    // we need to clone them inside the boundaries.
    // Iteratively repeat the steps for subsequent operands.
    for(unsigned i =0; i < I->getNumOperands();i++){
        LLVM_DEBUG(errs()<<"Operand "<<i<<" of "<<*I<<" is "<<*I->getOperand(i)<<"\n");
        if(isa<Constant>(I->getOperand(i))) continue;
            
        LLVM_DEBUG(errs()<<"Operand "<<i<<" of "<<*I<<" is "<<" not constant!"<<"\n");
        Instruction* OI = dyn_cast<Instruction>(I->getOperand(i));

        if(OI && isOutsideBoundary(OI)){
            LLVM_DEBUG(errs()<<"Operand "<<i<<" of "<<*I<<" is "<<"outside boundary!"<<"\n");

            CloneWorkList.push(OI);
        }
    }

    // Clone instructions which are defined
    // outside of task boundaries.
    while(CloneWorkList.size()){
        Value* item = CloneWorkList.front();
        CloneWorkList.pop();



        // If already inserted
        if(Cloned.find(item) != Cloned.end()) continue;

        
        Cloned.insert(item);

        if(isa<Constant>(item)){
            continue;
        }


        if(VMap.find(item) != VMap.end()) continue;
        LLVM_DEBUG(dbgs()<<"Checking item: "<<*item<<" for original instruction "<<*I<<"\n");

        
        if(isa<Argument>(item)){
            VMap[item] = item;
            RevVMap[item] = item;
        } else if(Instruction* Ins = dyn_cast<Instruction>(item)){

            // If the instruction is inside the boundaries,
            // no need to clone
            if(!isOutsideBoundary(Ins)) continue;



            for(unsigned idx = 0; idx < Ins->getNumOperands(); idx++){
                CloneWorkList.push(Ins->getOperand(idx));
            }
            toClone.push_back(Ins);
        }
    }

    auto isOperandOf = [](Instruction* Inst, Value* VI) -> bool {
        for(Value* Op : Inst->operands()){
            if(Op == VI) return true;
        }
        return false;

    };

    // Sort to clone according to domination tree order
    auto cmpValues = [&](Value* V1, Value* V2)-> bool {
        auto I1 = dyn_cast<Instruction>(V1);
        auto I2 = dyn_cast<Instruction>(V2);

        if(!I1 || !I2){
            assert(false && "Unknown ValueTy used in cloneInst");
        }
        
        bool hasArg = isOperandOf(I2, V1);

        return DT.dominates(I1, I2) || hasArg ;
    };

    sort(toClone, cmpValues);


    

    Instruction* InsertBefore = BeginMarker->getNextNonDebugInstruction();

    while(isa<AllocaInst>(InsertBefore)){
        InsertBefore = InsertBefore->getNextNonDebugInstruction();
    }

    for(Value* V: toClone){
        LLVM_DEBUG(errs()<<"toClone: "<<*V<<"\n");
        if(isa<PHINode>(V)){
            VMap[V] = V;
            RevVMap[V] = V;
            continue;
        }
        Instruction* VI = cast<Instruction>(V);
        Instruction* CVI = VI->clone();
        //CVI->insertBefore(I);
        if(isa<AllocaInst>(VI)){
            CVI->insertBefore(BeginMarker->getNextNonDebugInstruction());
        } else {
            CVI->insertBefore(InsertBefore);
        }
        CVI->setName(V->getName()+"_clone");

        Value* CV = CVI;

        VMap[V] = CV; 
        RevVMap[CV] = V;

        for(unsigned idx = 0; idx < CVI->getNumOperands(); idx++){
            Value* OrigOp = CVI->getOperand(idx);
            if(isa<Constant>(OrigOp) || isa<Argument>(OrigOp)) continue;
            
            if(VMap.find(OrigOp) != VMap.end()){
                Value* ClonedOp = VMap[OrigOp];
                Instruction* OrigI = dyn_cast<Instruction>(OrigOp);
                Instruction* ClonedI = dyn_cast<Instruction>(ClonedOp);
                   
                if(DT.dominates(ClonedI, CVI))
                    CVI->setOperand(idx, ClonedOp);
            }
        }
    }

    DT.recalculate(*ParentF);





    // Replace all cloned instruction forward  uses original outside 
    // instruction with new inst.
    for(unsigned idx = 0; idx < toClone.size(); idx++){
        Value* V = toClone[idx];

        Instruction* VI = cast<Instruction>(V);
        Instruction* CVI = cast<Instruction>(VMap[V]);
        
        std::set<BasicBlock*> included;
        auto shouldReplace = [&](Use &U)->bool {
            auto useInst = U.getUser();
            Instruction* IU = dyn_cast<Instruction>(useInst);

            if(!IU) return false;


            return DT.dominates(CVI ,IU) ; 
        };

        LLVM_DEBUG(dbgs()<<"Checking uses for "<<*VI<<"\n");
        for(auto ui = VI->use_begin(); ui != VI->use_end(); ){
            auto U = &*ui;
            ui++;
            Instruction* UI = dyn_cast<Instruction>(U->getUser());
            LLVM_DEBUG(dbgs()<<"Iterating over uses: "<<*U<<" "<<*UI<<"\n");

            if(shouldReplace(*U)){
                LLVM_DEBUG(dbgs()<<"Should replace "<<*U<<" "<<*UI<<" with "<<*CVI<<"\n");
                U->set(CVI);
            }
        }


        DT.recalculate(*ParentF);
    }

    // Reset cache after replacing uses
    DT.recalculate(*ParentF);
    //dbgs()<<*I->getParent()->getParent()<<"\n";

}


void copyAttributes(Argument* From, Argument* To){


    for(int Attr = Attribute::AttrKind::None ; Attr != Attribute::AttrKind::EndAttrKinds ; Attr++){
        Attribute::AttrKind AttrCast = static_cast<Attribute::AttrKind>(Attr);
        if(From->hasAttribute(AttrCast)){
            LLVM_DEBUG(errs()<<"Copying attribute "<<Attr<<" from "<<*From<<" to "<<*To<<"\n");
            To->addAttr(AttrCast);
        }
    }


}


// Starting from root function, take the arguments
// associated with the top level arguments and propogate
// down the DFG.
void PropogateArgAttributes(Function* Root){
    vector<CallInst*> ChildNodes = getChildRootsCalls(Root);

    // Get formal argument of function at index
    auto getArg = [](Function* F, unsigned idx) -> Argument* {
        Argument* arg = dyn_cast<Argument>((F->arg_begin() + idx));
        return arg;
    };
    for(CallInst* Child : ChildNodes){
        map<int,int> BindInMap;
        mapParentInputToChild(nullptr, Child, BindInMap);

        Function* ChildFn = dyn_cast<Function>(Child->getArgOperand(1)->stripPointerCasts());

        
        for(auto& BI : BindInMap){
            Argument* ParentArg = getArg(Root, BI.first);
            Argument* ChildArg = getArg(ChildFn, BI.second);

            LLVM_DEBUG(errs()<<"BindInArg: "<<BI.first<<", "<<BI.second<<"\n");
            LLVM_DEBUG(errs()<<"Parent Function: "<<*Root<<"ChildFn: "<<*ChildFn<<"\n");

            assert(ParentArg && ChildArg && "Exceeded number of arguments available");

            copyAttributes(ParentArg, ChildArg);

        }

    }

    for(CallInst* Child : ChildNodes){
        PropogateArgAttributes(cast<Function>(Child->getArgOperand(1)));
    }

}

Value* copyValueChain(Value* V, Instruction* InsertBefore){
    Instruction* I = dyn_cast<Instruction>(V);

    Function* ParentF = I->getParent()->getParent();

    DominatorTree DT(*ParentF);



    vector<Value*> toClone;

    std::set<Value*> Cloned;
    std::queue<Value*> CloneWorkList;


    // For the current instruction, if any of it's non-constant
    // operands are defined outside the task boundaries,
    // we need to clone them inside the boundaries.
    // Iteratively repeat the steps for subsequent operands.
    for(unsigned i =0; i < I->getNumOperands();i++){
        LLVM_DEBUG(errs()<<"Operand "<<i<<" of "<<*I<<" is "<<*I->getOperand(i)<<"\n");
        if(isa<Constant>(I->getOperand(i))) continue;
            
        LLVM_DEBUG(errs()<<"Operand "<<i<<" of "<<*I<<" is "<<" not constant!"<<"\n");
        Instruction* OI = dyn_cast<Instruction>(I->getOperand(i));

        if(OI){
            LLVM_DEBUG(errs()<<"Operand "<<i<<" of "<<*I<<" is "<<"outside boundary!"<<"\n");

            CloneWorkList.push(OI);
        }

    }
    CloneWorkList.push(I);


    // Clone instructions which are defined
    // outside of task boundaries.
    while(CloneWorkList.size()){
        Value* item = CloneWorkList.front();
        CloneWorkList.pop();



        // If already inserted
        if(Cloned.find(item) != Cloned.end()) continue;
        if(isa<PHINode>(item)) continue;

        
        Cloned.insert(item);

        if(isa<Constant>(item)){
            continue;
        }



        
        if(Instruction* Ins = dyn_cast<Instruction>(item)){

            for(unsigned idx = 0; idx < Ins->getNumOperands(); idx++){
                CloneWorkList.push(Ins->getOperand(idx));
            }
            toClone.push_back(Ins);
        }
    }

    auto isOperandOf = [](Instruction* Inst, Value* VI) -> bool {
        for(Value* Op : Inst->operands()){
            if(Op == VI) return true;
        }
        return false;

    };

    // Sort to clone according to domination tree order
    auto cmpValues = [&](Value* V1, Value* V2)-> bool {
        auto I1 = dyn_cast<Instruction>(V1);
        auto I2 = dyn_cast<Instruction>(V2);

        if(!I1 || !I2){
            assert(false && "Unknown ValueTy used in cloneInst");
        }
        
        bool hasArg = isOperandOf(I2, V1);

        return DT.dominates(I1, I2) || hasArg ;
    };

    sort(toClone, cmpValues);

    ValueToValueMapTy VMap;

    for(Value* V: toClone){
        LLVM_DEBUG(errs()<<"toClone: "<<*V<<"\n");
        if(isa<PHINode>(V)){
            continue;
        }
        Instruction* VI = cast<Instruction>(V);
        Instruction* CVI = VI->clone();
        CVI->insertBefore(InsertBefore);
        CVI->setName(V->getName()+"_clone");

        Value* CV = CVI;

        VMap[VI] = CV;


    }

    DT.recalculate(*ParentF);


    // Replace all cloned instruction forward  uses original outside 
    // instruction with new inst.
    for(unsigned idx = 0; idx < toClone.size(); idx++){
        Value* V = toClone[idx];

        Instruction* VI = cast<Instruction>(V);
        Instruction* CVI = cast<Instruction>(VMap[V]);
        
        std::set<BasicBlock*> included;
        auto shouldReplace = [&](Use &U)->bool {
            auto useInst = U.getUser();
            Instruction* IU = dyn_cast<Instruction>(useInst);

            if(!IU) return false;


            return DT.dominates(CVI ,IU) ; 
        };

        LLVM_DEBUG(dbgs()<<"Checking uses for "<<*VI<<"\n");
        for(auto ui = VI->use_begin(); ui != VI->use_end(); ){
            auto U = &*ui;
            ui++;
            Instruction* UI = dyn_cast<Instruction>(U->getUser());
            LLVM_DEBUG(dbgs()<<"Iterating over uses: "<<*U<<" "<<*UI<<"\n");

            if(shouldReplace(*U)){
                LLVM_DEBUG(dbgs()<<"Should replace "<<*U<<" "<<*UI<<" with "<<*CVI<<"\n");
                U->set(CVI);
            }
        }


        DT.recalculate(*ParentF);
    }



    return VMap[V];

}

void PropogateArgsWrapper(Module& M){
    LLVM_DEBUG(errs()<<"=== PropogateArgsWrapper ===\n");
    Function* HPVMLaunch = M.getFunction("__hpvm__launch");

    if(!HPVMLaunch) return;


    std::set<Function*> RootFns;
    for(auto* User: HPVMLaunch->users()){
        CallInst* CI = dyn_cast<CallInst>(User);
        if(!CI) continue;

        Function*  Root = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());

        assert(Root && "Launch must be called with known function");

        RootFns.insert(Root);

    }

    for(Function* Root: RootFns){
        LLVM_DEBUG(errs()<<"Propogating attributes for RootFn: "<<Root->getName()<<"\n");
        PropogateArgAttributes(Root);

    }


}


// Adapted from LLVM's implementation
BranchInst* getLoopGuard(Loop* L){
    BasicBlock* Preheader = L->getLoopPreheader();
    BasicBlock* Latch = L->getLoopLatch();

    BasicBlock* GuardBB = Preheader->getUniquePredecessor();
    if(!GuardBB){
        return nullptr;
    }

    BranchInst* GuardBI = dyn_cast<BranchInst>(GuardBB->getTerminator());
    if(!GuardBI || GuardBI->isUnconditional()){
        return nullptr;
    }

    return GuardBI;

}



void RootRemoveBindOut(Module& M){

    LLVM_DEBUG(errs()<<"\n=== RootRemoveBindOut ===\n");
    Function* LaunchF = M.getFunction("__hpvm__launch"); 

    if(!LaunchF) return;

    std::set<Function*> RootF;

    for(auto* User : LaunchF->users()){
        if(CallInst* LaunchCall = dyn_cast<CallInst>(User)){
            Function* Root = dyn_cast<Function>(LaunchCall->getArgOperand(1)->stripPointerCasts());

            assert(Root && "First operand of launch call must be a function");

            RootF.insert(Root);
        }
    }

    std::vector<Instruction*> ToRemove;

    for(Function* F : RootF){

        for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
            CallInst* CI = dyn_cast<CallInst>(&*I);
            if(CI && CI->getCalledFunction()){
                Function* CF = dyn_cast<Function>(CI->getCalledFunction());
                
                if(CF && CF->getName() == "__hpvm__bindOut"){
                    ToRemove.push_back(CI);
                }
            }
            
        }
    }

    for(Instruction* I : ToRemove){
        I->eraseFromParent();
    }
}

std::vector<Function*> getParentNodes(Function* ChildNode){
    std::vector<Function*> Callers;
    for(auto* User : ChildNode->users()){
        if(CallInst* CI = dyn_cast<CallInst>(User)){
            Function* Callee = CI->getCalledFunction();
            if(Callee && Callee->getName() == "__hpvm__createNodeND"){
                Callers.push_back(CI->getParent()->getParent());
            }
        }

    }

    return Callers;
}

BasicBlock* getLoopLatch(Loop* L){
    BasicBlock*  LoopLatch;
    BasicBlock* LoopHeader = L->getHeader();

    for(BasicBlock* LB: L->blocks()){
        BranchInst* LoopBranch = dyn_cast<BranchInst>(LB->getTerminator());
        if(LoopBranch && LoopBranch->isConditional()){
            for(BasicBlock* HeaderPred : predecessors(LoopHeader)){
                if(HeaderPred == LB){
                    return LB;
                }
            }

        }
    }
    return nullptr;

}

// Taken from LLVM-13 implementation
ICmpInst* getLatchICmpInst(Loop* L){
    if (BasicBlock *Latch = getLoopLatch(L))
        if (BranchInst *BI = dyn_cast<BranchInst>(Latch->getTerminator()))
            if (BI->isConditional())
                return dyn_cast<ICmpInst>(BI->getCondition());
    return nullptr;
}


bool isLoopInclusive(Loop* L, Value* InductionVar){
    ICmpInst* BranchCond = getLatchICmpInst(L);

    if(BranchCond->isEquality()){
        for(int i =0; i < BranchCond->getNumOperands(); i++){
            if(InductionVar == BranchCond->getOperand(i)){
                return true;
            }
        }
    }
    return false;

}



bool isPrivCall(CallInst* CI){
    Function* CF = CI->getCalledFunction();

    if(!CF) return false;


    return (CF->getName() == "__hpvm_priv") ||(CF->getName() == "__hetero_priv") ;
}



bool isNonZeroCall(CallInst* CI){
    Function* CF = CI->getCalledFunction();

    if(!CF) return false;


    return (CF->getName() == "__hpvm__isNonZeroLoop") ||(CF->getName() == "__hetero_isNonZeroLoop" ) ;

}


Value* castIntegerToBitwidth(Value* V, Instruction* InsertBefore, int BV){
    LLVMContext& LC = V->getContext();


    IntegerType* IType = IntegerType::get(LC, BV); 

    if(V->getType()->isIntegerTy(BV)){
        return V;
    } else {
        IntegerType* SizeTy = dyn_cast<IntegerType>(V->getType());
        assert(SizeTy && "Dimension size must be an integer type");
        Value* CastToI = nullptr;

        if(SizeTy->getBitWidth() > BV){
            return new TruncInst(V, IType, "trunc_", InsertBefore);
        } else {
            return new SExtInst(V, IType, "sext_", InsertBefore);
        }
    }
}


bool isHPVMGraphIntrinsic(Value* V){
    CallInst* CI = dyn_cast<CallInst>(V);
    if(!CI) return false;

    Function* CF = CI->getCalledFunction();

    if(!CF) return false;

    if(isTaskBeginMarker(CI)) return true;
    if(isParallelLoopMarker(CI)) return true;
    if(isLaunchBeginMarker(CI)) return true;



    if(CF->getName().str() == "__hpvm__attributes" || CF->getName().str() == "__hpvm__return" 
        || CF->getName().str() == "__hpvm__order"
            ){
        return true;
    }

    return false;

}

