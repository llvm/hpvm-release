
#include "llvm/Pass.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/Constants.h"

#include "DAGInfo.h"
#include "NestedDotGraph.h"



#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>
#include <fstream>
#include <iostream>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;


const string CreateNodeNDFuncName = "__hpvm__createNodeND";

struct DotProperties {
    string id;
    string label;
    bool is_cluster;
    vector<string> edges;
    std::set<DotProperties*> imm_children;

    


    string getENTRYlabel(){
        return label+"ENTRY";

    }

    string getEXITlabel(){
        return label+"EXIT";
    }

    string render(){
        if(is_cluster) {
            string start = "subgraph "+id+"{\n";

            string dot_str = "label = "+ label +"\n";

            dot_str += getENTRYlabel()+"[label = \"ENTRY\"]\n";
            dot_str += getEXITlabel()+"[label = \"EXIT\"]\n";

            for(DotProperties* C : imm_children){
                dot_str += C->render();
            }

            for(string edge : edges){
                dot_str += edge + "\n";
                LLVM_DEBUG(errs() << "Edge: "<<edge<<"\n");
            }

            string end = "} \n";

            return start + dot_str + end;

        } else {

            return id + "\n";

        }



    }
};

struct HPVMNestedDotProgram {
    public:
        HPVMNestedDotProgram(Module &M, string suffix): M(M){
            LLVM_DEBUG(dbgs()<<"----- HPVMNestedDotProgram ----\n");

            createNodeFunc = M.getFunction(CreateNodeNDFuncName);
            edgeFunc = M.getFunction(EdgeFuncName);
            launchFunc = M.getFunction(LaunchFuncName);
            bindInFunc = M.getFunction(BindInFuncName);
            bindOutFunc = M.getFunction(BindOutFuncName);


            DAGInfo = HPVMDAGInfoAnalysis(M);

            for(auto* User: launchFunc->users()){
                CallInst* CI = dyn_cast<CallInst>(User);

                if(!CI) continue;
                LLVM_DEBUG(dbgs()<<"Launch Instruction: "<<*CI<<"\n");

                Function* RootFn = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());



                string dot_str = runOnDFGRoot(RootFn);
                std::ofstream dotFile;

                string fname = RootFn->getName().str()+ suffix + ".dot";

                dbgs()<<"Writing graph to file: "<<fname<< " ...\n";
                dotFile.open(fname);
                dotFile<<dot_str;
                dotFile.close();


            }



        }

    private:
        Function* createNodeFunc;
        Function* edgeFunc;
        Function* launchFunc;
        Function* bindInFunc;
        Function* bindOutFunc;


        string sanatiseString(string name);

        Module &M;

        HPVMDAGInfo* DAGInfo;

        std::map<Function*, DotProperties*> DotMap;

        string runOnDFGRoot(Function* RootFn);

        DotProperties* getDotProperty(Function* F);
};



DotProperties* HPVMNestedDotProgram::getDotProperty(Function* F){
    if(DotMap.find(F) == DotMap.end()){
        DotProperties* DP = new DotProperties;

        bool is_internal = DAGInfo->isInternalNode(F);

        DP->is_cluster = is_internal;
        DP->label = F->getName().str();

        if(is_internal){
            DP->id = "\"cluster "+ DP->label + "\"";
        } else {
            DP->id = DP->label;
        }

        DotMap[F] = DP;

    }

    return DotMap[F];
}


string HPVMNestedDotProgram::sanatiseString(string name){
    std::replace(name.begin(), name.end(), '.','_');
    return name;

}

string HPVMNestedDotProgram::runOnDFGRoot(Function* RootFn){

    vector<Function*> Nodes = DAGInfo->getDAGInTopologicalOrder(RootFn);

    for(Function* Node : Nodes){


        auto Parents = DAGInfo->getParentNodes(Node);
        DotProperties* DP = getDotProperty(Node);

        for(Function* Parent : Parents){
            DotProperties* PDP = getDotProperty(Parent);

            PDP->imm_children.insert(DP);
        }

        vector<CallInst*> BIs = DAGInfo->getBindInCalls(Node);
        for(CallInst* BI : BIs){
            LLVM_DEBUG(errs() << "Handling Bind In"<<"\n");
            CallInst* ChildCreateND = dyn_cast<CallInst>(BI->getArgOperand(0)->stripPointerCasts());
            Function* ChildFunc = getCreateNodeNDFunction(ChildCreateND);
            if(!ChildFunc) continue;

            DotProperties* CDP = getDotProperty(ChildFunc);
            string dst_label = CDP->is_cluster ? CDP->getENTRYlabel() : CDP->label;
            DP->edges.push_back(DP->getENTRYlabel() +"->"+ dst_label);

        }


        vector<CallInst*> BOs = DAGInfo->getBindOutCalls(Node);
        for(CallInst* BO : BOs){
            LLVM_DEBUG(errs() << "Handling Bind Out"<<"\n");

            CallInst* ChildCreateND = dyn_cast<CallInst>(BO->getArgOperand(0)->stripPointerCasts());
            Function* ChildFunc = getCreateNodeNDFunction(ChildCreateND);
            if(!ChildFunc) continue;

            DotProperties* CDP = getDotProperty(ChildFunc);

            string src_label = CDP->is_cluster ? CDP->getEXITlabel() : CDP->label;
            DP->edges.push_back( src_label + "->" + DP->getEXITlabel()+ "\n");

        }


        vector<CallInst*> Edges = DAGInfo->getEdgeCalls(Node);
        for(CallInst* E : Edges){

            LLVM_DEBUG(errs() << "Handling Edge"<<"\n");
            CallInst* SrcCall = dyn_cast<CallInst>(E->getArgOperand(0)->stripPointerCasts());
            Function* SrcFunc = getCreateNodeNDFunction(SrcCall);
            if(!SrcFunc) continue;


            CallInst* DstCall = dyn_cast<CallInst>(E->getArgOperand(1)->stripPointerCasts());
            Function* DstFunc = getCreateNodeNDFunction(DstCall);
            if(!DstFunc) continue;

            DotProperties* SDP = getDotProperty(SrcFunc);
            DotProperties* DDP = getDotProperty(DstFunc);

            string src_label = SDP->is_cluster ? SDP->getEXITlabel() : SDP->label;
            string dst_label = DDP->is_cluster ? DDP->getENTRYlabel() : DDP->label;

            DP->edges.push_back( src_label+ "->" + dst_label+ "\n");

        }



    }

    DotProperties* RDP = getDotProperty(RootFn);

    string dot_graph = "digraph \"HPVM DAG\" {\n";
    dot_graph += "label="+RDP->label+"\n";
    dot_graph += "compound=true;\n";
    dot_graph += "style=\"rounded\"";

    dot_graph += RDP->render();
    dot_graph += "}\n";

    return sanatiseString(dot_graph);
}


void DFG2NestedDot(Module &M, string suffix)
{
  ExitOnError ExitOnErr(M.getName().str() + ": error reading input: ");

  HPVMNestedDotProgram Dot(M,suffix);

}
