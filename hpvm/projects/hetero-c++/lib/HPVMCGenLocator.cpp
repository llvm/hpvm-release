#include "HPVMCGenLocator.h"
#include "DFGUtils.h"
#include <string>
#include <stdio.h>

/**
 * Driver function. Calls all insertion tasks.
 */
int HPVMCGenLocator::insertStaticCalls() {

  LLVM_DEBUG(dbgs()<<"\n HPVMCGenLocator pass \n");
  LocatorInfo* LI = new LocatorInfo;
  LI->InitInst = insertInit();
  LI->CleanupInst = insertCleanup();
  insertLaunch(LI);
  insertWait(LI);
  insertTrackMem(LI);
  insertRequestMem(LI);
  insertUnTrackMem(LI);
  insertCopyMem();
  return 0;
}

/**
 * Utility function to get all function calls of a given mangled name
 * @param func_name mangled function name (constant func names declared at top of .h file)
 * @return vector of all CallInsts of that function
 **/
std::vector<CallInst*> HPVMCGenLocator::getMarkerCalls(std::string func_name) {
  std::vector<CallInst *> calls;
  Function* func = theContext.getModule().getFunction(func_name);

  if (!func) {
      char buff[1024];
      sprintf(buff, "No such function call in current module: %s\n", func_name);
      LLVM_DEBUG(HPVMWarnError(buff));
      return std::vector<CallInst*>();
  }

  for(auto *User : func->users()){
    CallInst* CI = dyn_cast<CallInst>(User);

    if(CI){
        calls.push_back(CI);
    }
  }

  return calls;
}

/**
 * put launch and wait in main. because it Needs to dominate push and pops
 */
CallInst *HPVMCGenLocator::insertInit() {
  
  Function* initFunc = theContext.getFunction("__hpvm__init");
  for(auto* User : initFunc->users()){
      CallInst* CI = dyn_cast<CallInst>(User);
      if (CI) {
          LLVM_DEBUG(dbgs()<<"__hpvm__init call already exists in the module, returning: " << *CI <<" in function "<<*(CI->getParent()->getParent())<<"\n");
          return CI;
      }
  }
  const StringRef mainFuncName("main");
  Function *mainFunc = theContext.getFunction(mainFuncName);
  if (!mainFunc) {
    HPVMFatalError("Could not locate main function!");
  }
  Instruction *insertPoint = mainFunc->getEntryBlock().getFirstNonPHI();
  HPVMCGenInit init(theContext);
  return init.insertCall(insertPoint);
}

std::vector<Instruction *> findExitCalls(Function *mainFunc) {
  std::vector<Instruction *> exitCalls;
  for (BasicBlock &BB : *mainFunc) {
    for (Instruction &I : BB) {
      if (isa<ReturnInst>(I)) {
        exitCalls.push_back(&I);
      }
    }
  }
  return exitCalls;
}

/**
 * If no cleanup call exists, inserts one into the main function
 */
CallInst *HPVMCGenLocator::insertCleanup() {
  Function* cleanupFunc = theContext.getFunction("__hpvm__cleanup");
  for(auto* User : cleanupFunc->users()) {
    CallInst* CI = dyn_cast<CallInst>(User);

    if(CI){
      LLVM_DEBUG(dbgs()<<"__hpvm__cleanup call already exists in the module, returning: "<< *CI <<" in function "<<*(CI->getParent()->getParent())<<"\n");
      return CI;
    }

  }
  const StringRef mainFuncName("main");
  Function *mainFunc = theContext.getFunction(mainFuncName);
  if (!mainFunc) {
    HPVMFatalError("Could not locate main function!");
  }

  HPVMCGenCleanup cleanup(theContext);
  auto exitCalls = findExitCalls(mainFunc);
  for (auto Inst : exitCalls) {
    cleanup.insertCall(Inst);
  }
  return nullptr;
}

/*
    __hpvm__launch(launchFunc, arg1, arg2, …);
    - make packed struct - DONE through insert function
    - wrap function being launched in an internal node
    - Create a new wrapper function which acts as an internal node
        - Create node ND
        - Binds
    - replace this function with the new function
    - look at sgemm hpvm__launch
    - Bind in argument from packed struct
    - Bind the out argument to the parent node
    - I feel like I was told a better way to get first instruction in a function
   but i dont remember it
    - __DFG__launch__(launchFunc, inArgs, …);

*/

/**
 * 1) make a packed struct
 * 2) wrap 
 * 
 */
CallInst *HPVMCGenLocator::insertLaunch(LocatorInfo *LI) {
  const StringRef dfgLaunchName("__hpvm__launch");
  Function *launchFunc = theContext.getFunction(dfgLaunchName);
  if (!launchFunc) {
    HPVMFatalError("Could not locate __hpvm__launch function!");
  }

  HPVMCGenLaunch genLaunch(theContext);
  std::vector<CallInst *> LaunchCalls = getMarkerCalls(MARKER_LAUNCH);
  std::vector<CallInst *> HeteroLaunchCalls = getMarkerCalls(HETERO_MARKER_LAUNCH);
  LaunchCalls.insert(LaunchCalls.end(), HeteroLaunchCalls.begin(), HeteroLaunchCalls.end());


  for (unsigned c = 0; c < LaunchCalls.size(); c++) {
    CallInst *LC = LaunchCalls[c];
    HPVMDFGInfo *DI = new HPVMDFGInfo;



    std::vector<Value *> LaunchArgs;

    int State = -1;
    const int Input = 0;
    const int Output = 1;
    

    unsigned NumRemaining = 0;

    auto ai = LC->arg_begin();
    // First argument is the root node function
    Function* RootFn = dyn_cast<Function>(ai->get()->stripPointerCasts());

    if(!RootFn){
        HPVMFatalError("Initial Argument to __hpvm__launch must be a function");
    }

    LLVM_DEBUG(dbgs()<<"Root Node Function: \n"<<*RootFn<<"\n");
    DI->RootFn = RootFn;
    ai++;

    auto ae = LC->arg_end();
    
    for(int formalArgNo = 0 ; ai != ae;){
        if (NumRemaining == 0){
            ConstantInt *NumArgs = dyn_cast<ConstantInt>(&*ai);

            if(!NumArgs){
                HPVMFatalError("Launch Instruction is not in correct format!");
            }

            ai++;
            State++;

            NumRemaining = NumArgs->getSExtValue();

            if(!NumRemaining) continue;


        }
        
        auto BF = BufferType();


        BF.Pointer = ai->get();
        ai++;


        if(BF.Pointer->getType()->isPointerTy()){
            BF.Size = ai->get();
            ai++;
        } else {
            BF.Size = nullptr;
        }

        switch (State){
            case Input:
                {
                    assert(formalArgNo < RootFn->getFunctionType()->getNumParams() && "Exceeding formal argument index no.");
                    Argument* FormalArg = &*(RootFn->arg_begin() + formalArgNo);
                    Type* formalArgType = FormalArg->getType();
                    Type* actualArgType = BF.Pointer->getType();

                    // Create a bitcast,integercast inst to match type
                    if(actualArgType != formalArgType){
                        LLVM_DEBUG(dbgs()<<"Actual Parameter Type: ");
                        LLVM_DEBUG(BF.Pointer->getType()->dump());
                        LLVM_DEBUG(dbgs()<<"\nFormal Parameter Type: ");
                        LLVM_DEBUG(formalArgType->dump());

                        // Need a trunc or sext inst
                        if(actualArgType->isIntegerTy() && formalArgType->isIntegerTy()){
                            IntegerType* actualInt = dyn_cast<IntegerType>(actualArgType);
                            IntegerType* formalInt = dyn_cast<IntegerType>(formalArgType);

                            Instruction* matchInt = nullptr;
                            if(actualInt->getBitWidth() > formalInt->getBitWidth()){
                                // trunc
                                matchInt = new TruncInst(BF.Pointer, formalArgType, "trunc_",LC);

                            } else {
                                // sext
                                matchInt = new SExtInst(BF.Pointer, formalArgType, "sext_",LC);
                            }

                            LLVM_DEBUG(dbgs()<<"Integer trunc/sext: "<<*matchInt<<"\n");
                            BF.Pointer = matchInt;

                        } else {
                            BitCastInst* BI = new BitCastInst(BF.Pointer, formalArgType, "",LC);
                            BF.Pointer = BI;
                            LLVM_DEBUG(dbgs()<<"Bitcasting new argument: "<<*BI<<"\n");
                        
                        }
                    }

                    
                    DI->Inputs.push_back(BF);
                    LaunchArgs.push_back(BF.Pointer);
                    if(BF.Pointer->getType()->isPointerTy())
                        LaunchArgs.push_back(BF.Size);
                }
                break;
            case Output:
                DI->Outputs.push_back(BF);
                break;
        }

        

        formalArgNo += BF.size();

        NumRemaining--;
    }

    if(NumRemaining != 0){
        std::string StateStr;
        switch (State) {
            case Input: StateStr = "inputs"; break;
            case Output: StateStr = "outputs"; break;
        }
        HPVMFatalError("Incorrect number of arguments specified in marker function for " + StateStr);
    }

    CallInst *newCall = genLaunch.insertCall(LC, (unsigned)0, LC->getArgOperand(0)->stripPointerCasts(), LaunchArgs); /* pass correct argument */
    cast<Value>(LC)->replaceAllUsesWith(cast<Value>(newCall));
    LC->eraseFromParent();

    DI->LaunchInst = newCall;
    
    // Insert the DFGInfo into the Modules LocatorInfo list
    LI->DFGInfos.push_back(DI);
  }

  return nullptr; 
}

std::vector<CallInst *> getWaitMarkerCalls(HPVMCGenContext *theContext) {
  std::vector<CallInst *> dfgWaits;
  for (Function &Func : theContext->getModule()) {
    for (BasicBlock &BB : Func) {
      for (Instruction &Inst : BB) {
        if (isa<CallInst>(Inst)) {
          CallInst *callInst = &cast<CallInst>(Inst);
          Function *called = callInst->getCalledFunction();
          if (called && called->getName() == StringRef("__hpvm__wait")) {
            dfgWaits.push_back(callInst);
          }

        }
      }
    }
  }
  return dfgWaits;
}

CallInst *HPVMCGenLocator::insertWait(LocatorInfo *LI) {
  const StringRef dfgWaitName("__hpvm__wait");
  
  Function *waitFunc = theContext.getFunction(dfgWaitName);
  if (!waitFunc) {
    HPVMFatalError("Could not locate __hpvm__wait function!");
  }

  HPVMCGenWait genWait(theContext);

  for(auto DI: LI->DFGInfos){
      CallInst *CI = dyn_cast<CallInst>(DI->LaunchInst->user_back());

      if(!CI || (CI->getCalledFunction()->getName() != MARKER_WAIT &&  CI->getCalledFunction()->getName() != HETERO_MARKER_WAIT)){
          HPVMFatalError("HPVMLaunch used by non-wait instruction");
      }

      Value *Graph = CI->getArgOperand(0);
      DI->WaitInst = genWait.insertCall(CI, Graph);
      CI->eraseFromParent();
  }

  return nullptr; 
}

void HPVMCGenLocator::insertTrackMem(LocatorInfo *LI){

    HPVMCGenTrackMem cgenTrackMem(theContext);

    Function* HeteroMalloc = theContext.getModule().getFunction(HeteroMallocName);

    // if HeteroMalloc not used, 
    // we rely on legacy insertion.
    if(!HeteroMalloc){
        for(auto DI: LI->DFGInfos){

            Instruction* InsertBefore = DI->LaunchInst;

            for(auto& buff : DI->Inputs){

                // If it's a scalar value continue
                if(!buff.Size) continue;
                
                Type* i8PTy = Type::getInt8PtrTy(theContext.getModule().getContext());
                Type *i64Ty = Type::getInt64Ty(theContext.getModule().getContext());

                BitCastInst* BI = new BitCastInst(buff.Pointer,i8PTy,"",InsertBefore);

                buff.Pointer = BI;

                if (buff.Size->getType() != i64Ty) {
                  SExtInst* BI_size = new SExtInst(buff.Size, i64Ty, "", InsertBefore);

                  buff.Size = BI_size;
                }

                DI->TrackMems.push_back(cgenTrackMem.insertCall(InsertBefore, buff.Pointer, buff.Size));


            }

        }

    } else {
        for(auto* User : HeteroMalloc->users()){
            std::string MallocName = "aligned_malloc";
            Function* MallocFunc = theContext.getModule().getFunction(MallocName);

            if(!MallocFunc){

                Type* i8PTy = Type::getInt8PtrTy(theContext.getModule().getContext());
                Type *i64Ty = Type::getInt64Ty(theContext.getModule().getContext());
                vector<Type*> MallocArgTy = {i64Ty};
                FunctionType* MallocTy = FunctionType::get(i8PTy, MallocArgTy, false );
                MallocFunc = Function::Create(MallocTy, Function::ExternalLinkage, MallocName,
                        theContext.getModule());

            }

            assert(MallocFunc && "malloc not present in module");
            if(CallInst* HM = dyn_cast<CallInst>(User)){
                Value* Size = HM->getArgOperand(0);
                std::vector<Value*> SizeArg = {Size};

                CallInst* GenMalloc = CallInst::Create(MallocFunc->getFunctionType(), MallocFunc, ArrayRef<Value*>(SizeArg) , "hpvm.malloc.", HM);
                HM->replaceAllUsesWith(GenMalloc);
                cgenTrackMem.insertCall(HM, GenMalloc, Size);
                HM->eraseFromParent();
            }
        }

    }
}

void HPVMCGenLocator::insertRequestMem(LocatorInfo *LI){

    HPVMCGenRequestMem cgenReqMem(theContext);


    Function* HeteroRequest = theContext.getModule().getFunction(HeteroRequestName);

    if(!HeteroRequest){

        for(auto DI: LI->DFGInfos){

            Instruction* InsertBefore = DI->WaitInst->getNextNonDebugInstruction();

            for(auto &buff : DI->Outputs){

                // If it's a scalar value continue
                if(!buff.Size) continue;

                LLVM_DEBUG(dbgs()<<"Requesting Output for "<<*buff.Pointer<<" with size "<<*buff.Size<<"\n"); 

                Type* i8PTy = Type::getInt8PtrTy(theContext.getModule().getContext());
                Type *i64Ty = Type::getInt64Ty(theContext.getModule().getContext());

                BitCastInst* BI = new BitCastInst(buff.Pointer,i8PTy,"",DI->LaunchInst);

                buff.Pointer = BI;

                if (buff.Size->getType() != i64Ty) {
                  SExtInst* BI_size = new SExtInst(buff.Size, i64Ty, "", DI->LaunchInst);

                  buff.Size = BI_size;
                }


                std::vector<Value*> pair;
                pair.push_back(buff.Pointer);
                pair.push_back(buff.Size);

                DI->RequestMems.push_back(cgenReqMem.insertCall(InsertBefore, pair));

            }

        }

    } else {

        for(auto* User : HeteroRequest->users()){
            if(CallInst* HM = dyn_cast<CallInst>(User)){
                Value* Mem = HM->getArgOperand(0);
                Value* Size = HM->getArgOperand(1);
                std::vector<Value*> Args = {Mem,Size};
                cgenReqMem.insertCall(HM, Args);
                HM->eraseFromParent();


            }
        }

    }
}

void HPVMCGenLocator::insertCopyMem(){
    Function* HeteroCopyMem = theContext.getModule().getFunction("__hetero_copy_mem");

    if(!HeteroCopyMem) return;

    //HeteroCopyMem->setName("llvm_hpvm_copy_mem");

    Type* i64Ty = Type::getInt64Ty(theContext.getModule().getContext());
    Type* i8PTy = Type::getInt8PtrTy(theContext.getModule().getContext());
    Type* voidTy = Type::getVoidTy(theContext.getModule().getContext());

    vector<Type*> ArgsTy = {i8PTy, i8PTy, i64Ty};
    FunctionType* HpvmCopyMemTy = FunctionType::get(voidTy, ArgsTy, false);

    Function* HpvmCopyMem = Function::Create(HpvmCopyMemTy, Function::ExternalLinkage, "llvm_hpvm_copy_mem", theContext.getModule());

    HeteroCopyMem->replaceAllUsesWith(HpvmCopyMem);

}

void HPVMCGenLocator::insertUnTrackMem(LocatorInfo *LI){

    HPVMCGenUnTrackMem cgenUnTrackMem(theContext);

  
    Function* HeteroFree = theContext.getModule().getFunction(HeteroFreeName);

    if(!HeteroFree){

        for(auto DI: LI->DFGInfos){

            Instruction* InsertBefore =  DI->WaitInst->getNextNonDebugInstruction();

            // Insert Before should be a non request mem instruction
            while(auto CI = dyn_cast<CallInst>(InsertBefore)){
                if(CI->getCalledFunction()->getName() != "llvm_hpvm_request_mem")
                    break;
                InsertBefore = InsertBefore->getNextNonDebugInstruction();
           }


            for(auto buff : DI->Inputs){
                // If it's a scalar value continue
                if(!buff.Size) continue;

                std::vector<Value*> pair;
                pair.push_back(buff.Pointer);

                DI->UnTrackMems.push_back(cgenUnTrackMem.insertCall(InsertBefore, pair));

            }

        }
    } else {

        for(auto* User : HeteroFree->users()){
            if(CallInst* HM = dyn_cast<CallInst>(User)){
                Value* Mem = HM->getArgOperand(0);
                std::vector<Value*> Args = {Mem};
                cgenUnTrackMem.insertCall(HM, Args);
                HM->eraseFromParent();


            }
        }

    }
}

void HPVMCGenLocator::extractConstants() {
    LLVM_DEBUG(errs () << "Extracting Constants ..." << "\n");
    HPVMCGenExtractConstants extractor(theContext);
    std::vector<CallInst*> launch_calls = getMarkerCalls(MARKER_LAUNCH_BEGIN);
    std::vector<CallInst*> hetero_calls = getMarkerCalls(HETERO_MARKER_LAUNCH_BEGIN);

    launch_calls.insert(launch_calls.end(), hetero_calls.begin() , hetero_calls.end());

    for (CallInst *launch_call : launch_calls) {
      DominatorTree tree(*launch_call->getParent()->getParent());
  
      for (size_t i = 0; i < launch_call->getNumArgOperands(); i++) {
        Value *curr_arg = launch_call->getArgOperand(i);

        if (curr_arg->getType()->isPointerTy()) {
          assert(i + 1 < launch_call->getNumArgOperands() && "Pointer was last argument in function call. need to specify size");
          Value *next_arg = launch_call->getArgOperand(i + 1);
          ConstantInt *next = dyn_cast<ConstantInt>(next_arg);
          if (!next) { continue; }

          LoadInst *stored_value = extractor.insertCall(launch_call, next);
          
          // launch_call->setArgOperand(i, stored_value);

          for (User *user : curr_arg->users()) {
            CallInst *call = dyn_cast<CallInst>(user);
            if (!call) { continue; }
            if (!(tree.dominates(launch_call, call) || call == launch_call)) { continue; }

            if (isParallelLoopMarker(call) || isTaskBeginMarker(call) || isLaunchBeginMarker(call)) {
              for (unsigned j = 0; j < call->getNumArgOperands(); j++) {
                if (call->getArgOperand(j) ==  curr_arg) {
                  assert(j + 1 < call->getNumArgOperands() && "Pointer was last argument in function call. need to specify size");
                  call->setArgOperand(j + 1, stored_value);
                }
              }
            }
          }
        }
    }
  }
}
