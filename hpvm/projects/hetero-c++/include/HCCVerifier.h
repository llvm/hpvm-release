//===- HCCVerifier.h -- HeteroC++ Verification Functions --------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HCCVerifier.h -- Contains declarations for HeteroC++ verification
//
//===----------------------------------------------------------------------===//

#ifndef __HCC_VERIFIER_H_
#define __HCC_VERIFIER_H_

#include "DominatorTreeCache.h"

#include "llvm/ADT/Triple.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopNestAnalysis.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/IR/Module.h"

#include <map>
#include <set>
#include <utility>
#include <tuple>

using namespace llvm;

namespace llvm {

class HCCVerifier {
private:
  typedef enum { BEGIN_SECTION, BEGIN_TASK, BEGIN_LAUNCH,
                   END_SECTION,   END_TASK,   END_LAUNCH,
                   LAUNCH, LOOP, WAIT } MarkerType;

  std::map<Function*, MarkerType> markerFunctions;
  std::map<MarkerType, std::set<CallInst*>> markerInstructions;
  std::set<StringRef> nodeNames;

  std::map<Function*, std::set<std::pair<CallInst*, CallInst*>>> markedRegions;
  std::map<Function*, std::set<std::tuple<Loop*, Loop*, CallInst*>>> markedLoops;
  Module& M;

  DominatorTreeCache DTCache;
  struct AnalysisInfo {
    AnalysisInfo(Function* func, DominatorTreeCache& DTCache)
        : LI(DTCache.get(func).DT), AC(*func),
          TLII(Triple(func->getParent()->getTargetTriple())),
          TLI(TLII), SE(*func, TLI, AC, DTCache.get(func).DT, LI) {}

    LoopInfo LI;
    AssumptionCache AC;
    TargetLibraryInfoImpl TLII;
    TargetLibraryInfo TLI;
    ScalarEvolution SE;
  };
  std::map<Function*, std::unique_ptr<AnalysisInfo>> AnalysisCache;

  bool errors;

public:
  HCCVerifier(Module& M);
  bool verify();

private:
  void lookupMarkers();
  void initMarkerInstructions();
  void findMarkers();

  void setupAnalysis(Function* func);
  LoopInfo& getLoopInfo(Function* func);
  ScalarEvolution& getScalarEvolution(Function* func);

  void verifyBeginMarkers(MarkerType marker);
  void verifyLoopMarkers();
  void checkEdgeArguments(CallInst* inst, int skip = 0);

  struct RegionTree {
    Instruction* start;
    Instruction* end;
    CallInst* marker;
    enum { Launch, Section, Task, Loop } type;
    llvm::Loop* outerLoop;
    llvm::Loop* innerLoop;

    std::list<RegionTree> contains;
  };

  bool verifyNesting(RegionTree& region);
  void verifyNesting();
  bool verifyUses(RegionTree& region, std::set<Value*> inputs,
                  std::set<Value*>* outerInputs);

  bool isLoopCondition(RegionTree const& region, Instruction* instr);
  bool allUsesLoopCondition(RegionTree const& region, Instruction* instr);

  Instruction* getFirstNonMarker(BasicBlock* block);
};

}

#endif // #ifndef __HCC_VERIFIER_H_
