#include "HPVMCGenFunctions.h"
#include "HPVMCGen.h"
#include <vector>

const std::string MARKER_LAUNCH = "__hpvm_launch";
const std::string MARKER_WAIT = "__hpvm_wait";
const std::string MARKER_LAUNCH_BEGIN = "__hpvm_launch_begin";
const std::string MARKER_LAUNCH_END = "__hpvm_launch_end";
const std::string MARKER_TASK_BEGIN = "__hpvm_task_begin";




const std::string HETERO_MARKER_LAUNCH = "__hetero_launch";
const std::string HETERO_MARKER_WAIT = "__hetero_wait";
const std::string HETERO_MARKER_LAUNCH_BEGIN = "__hetero_launch_begin";
const std::string HETERO_MARKER_LAUNCH_END = "__hetero_launch_end";
const std::string HETERO_MARKER_TASK_BEGIN = "__hetero_task_begin";


const std::string HeteroMallocName = "__hetero_malloc";
const std::string HeteroFreeName = "__hetero_free";
const std::string HeteroRequestName = "__hetero_request_mem";

// Pairing Pointers and their Corresponding
// Sizes into a struct;
struct BufferType {
    Value *Pointer;
    Value *Size;

    BufferType(Value* P, Value* S): Pointer(P), Size(S) {}
    BufferType() : Pointer(nullptr), Size(nullptr) {}

    unsigned size(){
        unsigned bsize = 0;

        if(Pointer) bsize++;
        if(Size) bsize++;

        return bsize;
    }
};

// Maintains the HPVM Intrinsic information
// for a single launch instance for a 
// particular Launch of a DFG.
struct HPVMDFGInfo {
    std::vector<BufferType> Inputs;
    std::vector<BufferType> Outputs;

    std::vector<CallInst*> TrackMems;
    std::vector<CallInst*> RequestMems;
    std::vector<CallInst*> UnTrackMems;


    CallInst *LaunchInst;
    CallInst *WaitInst;

    Function* RootFn;
};

// Stores the references to potentially
// multiple HPVMDFGInfo's
struct LocatorInfo {
    CallInst *InitInst;
    CallInst *CleanupInst;
    std::vector<HPVMDFGInfo*> DFGInfos;
};





class HPVMCGenLocator {
private: 
    HPVMCGenContext& theContext;

    std::vector<CallInst*> getMarkerCalls(std::string func_name);

    CallInst* insertInit();

    CallInst* insertCleanup();

    CallInst* insertLaunch(LocatorInfo *LI);
    
    CallInst* insertWait(LocatorInfo *LI);

    void insertTrackMem(LocatorInfo *LI);

    void insertRequestMem(LocatorInfo *LI);

    void insertUnTrackMem(LocatorInfo *LI);

    void insertCopyMem();

public:
    HPVMCGenLocator(HPVMCGenContext& context) : theContext(context) {}

    int insertStaticCalls();

    void extractConstants();
};
