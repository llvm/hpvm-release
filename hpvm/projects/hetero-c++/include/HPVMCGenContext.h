//===- HPVMCGenContext.h - HPVM-C generation context ------------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HPVMCGenContext.h -- Contains declarations for class LLVMCGenContext
//
//===----------------------------------------------------------------------===//

#ifndef __HPVM_CGEN_CONTEXT_H__
#define __HPVM_CGEN_CONTEXT_H__

#include "llvm/IR/IRBuilder.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Support/Error.h"
#include <iostream>
#include <string>
using namespace llvm;

#define DEBUG_TYPE "HPVMCGen"

namespace llvm {

	extern void HPVMFatalError(const std::string& msg);
	extern void HPVMWarnError(const std::string& msg);

// Use a name macro for function names with __hpvm__ prefix
//
// #  define HPVM_FUNC_NAME(callName) ("__hpvm__" #callName)

// A map from function name (constant string) to function descriptor object
//
template<typename FuncDescriptorTy>
struct HPVMFunctionList : public StringMap<const FuncDescriptorTy*> {
  HPVMFunctionList() : StringMap<const FuncDescriptorTy*>() { }
};

// HPVMCGenContext : Unique container object for global code-gen state
//
class HPVMCGenContext {
public: // ---Public data types---

  // Function descriptor object records signature of each HPVM-C function
  //
  struct HPVMFuncDescriptor {
    const std::string funcName;
    const FunctionType& funcType; // FunctionTypes are immutable, so ref is safe
    // unsigned numArgs;
    // bool hasVariableNumArgs;
    HPVMFuncDescriptor(HPVMFunctionList<HPVMFuncDescriptor>& functionList,
          const StringRef& name, const FunctionType& funcType) :
      funcName(name), funcType(funcType) {
        functionList[name] = this;
      }
    unsigned getNumArgs() const { return funcType.getNumParams(); }
    bool     isVarArg()   const { return funcType.isVarArg(); }
  };

  typedef HPVMFunctionList<HPVMFuncDescriptor> HPVMFunctionListTy;

private: // ---Private code-generator state---
  Module& M;                    // Input Module
  LLVMContext& llvmContext;     // LLVMContext
  IRBuilder<> IRB;              // IRBuilder used for codegen in this context
  HPVMFunctionListTy funcList;  // All HPVM-C function descriptors

#undef EXPLICIT_FUNC_DESCRIPTORS
#ifdef EXPLICIT_FUNC_DESCRIPTORS
  // function descriptor objects for every HPVM-C function
  //
  // Internal node functions
  const HPVMFuncDescriptor hpvm_func_edge;
  const HPVMFuncDescriptor hpvm_func_createNodeND;
  const HPVMFuncDescriptor hpvm_func_bindIn;
  const HPVMFuncDescriptor hpvm_func_bindOut;
  const HPVMFuncDescriptor hpvm_func_hint;
  const HPVMFuncDescriptor hpvm_func_attributes;

  // Leaf node functions
  const HPVMFuncDescriptor hpvm_func_getNode;
  const HPVMFuncDescriptor hpvm_func_getParentNode;
  const HPVMFuncDescriptor hpvm_func_barrier;
  const HPVMFuncDescriptor hpvm_func_malloc;
  const HPVMFuncDescriptor hpvm_func_return;
  const HPVMFuncDescriptor hpvm_func_getNodeInstanceID_x;
  const HPVMFuncDescriptor hpvm_func_getNodeInstanceID_y;
  const HPVMFuncDescriptor hpvm_func_getNodeInstanceID_z;
  const HPVMFuncDescriptor hpvm_func_getNumNodeInstances_x;
  const HPVMFuncDescriptor hpvm_func_getNumNodeInstances_y;
  const HPVMFuncDescriptor hpvm_func_getNumNodeInstances_z;

  // Atomics
  const HPVMFuncDescriptor hpvm_func_atomic_add;
  const HPVMFuncDescriptor hpvm_func_atomic_sub;
  const HPVMFuncDescriptor hpvm_func_atomic_xchg;
  const HPVMFuncDescriptor hpvm_func_atomic_min;
  const HPVMFuncDescriptor hpvm_func_atomic_max;
  const HPVMFuncDescriptor hpvm_func_atomic_and;
  const HPVMFuncDescriptor hpvm_func_atomic_or;
  const HPVMFuncDescriptor hpvm_func_atomic_xor;

  // Misc Functions
  const HPVMFuncDescriptor hpvm_func_sin;
  const HPVMFuncDescriptor hpvm_func_cos;

  // Host functions
  const HPVMFuncDescriptor hpvm_func_init;
  const HPVMFuncDescriptor hpvm_func_cleanup;
  const HPVMFuncDescriptor hpvm_func_launch;
  const HPVMFuncDescriptor hpvm_func_wait;
  const HPVMFuncDescriptor hpvm_func_push;
  const HPVMFuncDescriptor hpvm_func_pop;
  const HPVMFuncDescriptor hpvm_func_llvm_trackMemory;
  const HPVMFuncDescriptor hpvm_func_llvm_untrackMemory;
  const HPVMFuncDescriptor hpvm_func_llvm_requestMemory;
#endif

  // Methods to import HPVM-C function declarations
  std::unique_ptr<Module>
      loadHPVMCFunctionDeclsFile(const std::string& pathname);
  int importHPVMCFunctionDecls(const Module& fromModule);

public: // ---Public methods---

  // Only one constructor. Takes a reference to a valid LLVM Module.
  // Creates all the constant function descriptor objects which register
  // themselves in the function list.
  //
  HPVMCGenContext(Module& _M);
  HPVMCGenContext(Module& _M, bool NoModuleFile);

  // Prevent copying or assignment of this object
  //
  HPVMCGenContext(const HPVMCGenContext&) = delete;
  HPVMCGenContext& operator=(const HPVMCGenContext&) = delete;

  // Accessors for the contents of this object
  //
  Module& getModule() { return M; }
  LLVMContext& getLLVMContext() { return llvmContext; }
  IRBuilder<>& getIRB() { return IRB; }
        HPVMFunctionListTy& getFunctionList()       { return funcList; }
  const HPVMFunctionListTy& getFunctionList() const { return funcList; }
  const HPVMFuncDescriptor *getDesc(const StringRef& funcName) {
    if (getFunctionList().count(funcName) == 0)
      HPVMFatalError("HPVMCGenContext: Invalid HPVM-C function name attempted");
    return getFunctionList().lookup(funcName);
  }

  // Get proper HPVM-C function name from base function name,
  // e.g., proper name for base name "edge" is "__hpvm__edge".
  // The 3 run-time functions are special cases.
  //
  const StringRef getHPVMCFuncName(const StringRef& baseName) {
    if (baseName.equals("track_mem")) return "llvm_hpvm_track_mem";
    if (baseName.equals("untrack_mem")) return "llvm_hpvm_untrack_mem";
    if (baseName.equals("request_mem")) return "llvm_hpvm_request_mem";
    return std::string("__hpvm__") += baseName;
  }

  Function* getFunction(const StringRef& HPVMFuncName) {
    // assert(0 && "getFunction not yet implemented");
    Function* hpvmFunc = M.getFunction(HPVMFuncName);
    if (! hpvmFunc)  {
      std::cout << "WARNING: Function " << HPVMFuncName.str();
      HPVMWarnError(" not found in current module.");
    }
    return hpvmFunc;
  }
};

} // end namespace llvm


#endif  // ifndef __HPVM_CGEN_CONTEXT_H__

