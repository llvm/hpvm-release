//===- HPVMCGen.h - HPVM-C generation common declarations -------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// LLVMCGen.h -- Contains common declarations for HPVM-C generation
//
//===----------------------------------------------------------------------===//

#ifndef __HPVM_CGEN_H__
#define __HPVM_CGEN_H__

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/StringRef.h"
#include "HPVMCGenContext.h"
#include <vector>
using namespace llvm;

namespace llvm {

  // Base generator class for generating HPVM-C function calls.
  // Common functionality includes:
  // + Hold a reference to the HPVMCGenContext
  // + Check function call signature, including number and types of operands
  // + Use LLVM IRBuilder to insert instructions
  //
  class HPVMCGenBase {
    protected:
      HPVMCGenContext& theContext;

    protected:
      // Insert a call with the explicit function arguments specified.
      // Checks that the specified arguments match the function signature.
      //
      CallInst* insertCall(Instruction* insertBefore,
        const StringRef& baseFuncName,
        ArrayRef<Value *>& funcArgs);


      CallInst* insertCall(Instruction* insertBefore,
        const StringRef& baseFuncName,
        std::vector<Value *>& funcArgs);

      // Insert a call with only the arugment numbers specified.
      // There is no type information, so this cannot check the function signature.
      // The signature must be checked in a subclass that uses this method.
      //
      CallInst* insertCallUnchecked(Instruction* insertBefore,
        const StringRef& funcName,
        Function* nodeFunc,
        std::vector<int>& argNumbers);

      CallInst* insertCallUnchecked(Instruction* insertBefore, 
        const StringRef& funcName,
        Function* nodeFunc,
        ArrayRef<Value *>& funcArgs);


      CallInst* insertCallUnchecked(Instruction* insertBefore, 
        const StringRef& funcName,
        Function* nodeFunc,
        std::vector<Value *>& funcArgs);

      AllocaInst* insertAlloca(
        Instruction* insertBefore,
        std::vector<Value *>& args);

      LoadInst* insertConstInt(
        Instruction *insertBefore, 
        ConstantInt *const_int);

    public:
      HPVMCGenBase(HPVMCGenContext& HCGC) : theContext(HCGC) {}

      IRBuilder<>& getIRB() { return theContext.getIRB(); }
  };

// Generator subclass for createNodeND operation
//
class HPVMCGenCreateNodeND: public HPVMCGenBase {
	public:	
		HPVMCGenCreateNodeND(HPVMCGenContext& HCGC) : HPVMCGenBase(HCGC) {}
		
		// Insert a call with the explicit function arguments specified.
		// Creates a 1D node with size `dimSize'.
		//	
		CallInst* insertCallND(Instruction* insertBefore, 
												   Function* NodeFunc,	
												   std::vector<Value *>& dimSize);
};

} // end namespace llvm

#endif // ifndef __HPVM_CGEN_H__
