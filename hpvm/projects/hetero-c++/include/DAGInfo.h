#include "llvm/Pass.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "DFGUtils.h"
#include "llvm/IR/Constants.h"



#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>
#include <fstream>
#include <iostream>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;
using std::queue;


const string DAGCreateNodeNDFuncName = "__hpvm__createNodeND";
const string LaunchFuncName = "__hpvm__launch";
const string EdgeFuncName = "__hpvm__edge";
const string BindInFuncName = "__hpvm__bindIn";
const string BindOutFuncName = "__hpvm__bindOut";
const string AttributesFuncName = "__hpvm__attributes";
const string ReturnFuncName = "__hpvm__return";



struct NodeInfo {

    NodeInfo(Function* F) : NodeFn(F) {
        CreateNodeNDCalls = getChildRootsCalls(F);
        isInternalNode = (CreateNodeNDCalls.size() > 0);

        for(CallInst* CreateNodeCall : CreateNodeNDCalls){
            vector<CallInst*> Edges = getDataFlowCalls(CreateNodeCall, EdgeFuncName);
            
            // To remove duplicate insertion
            // when inserting edge call from src and
            // dst side.
            for(CallInst* Edge: Edges){
                EdgeCalls.insert(Edge);
            }

            vector<CallInst*> BindIns = getDataFlowCalls(CreateNodeCall, BindInFuncName);
            BindInCalls.insert(BindInCalls.end(), BindIns.begin(), BindIns.end());


            vector<CallInst*> BindOuts = getDataFlowCalls(CreateNodeCall, BindOutFuncName);
            BindOutCalls.insert(BindOutCalls.end(), BindOuts.begin(), BindOuts.end());

        }

        // Topologically sort the create node calls
        auto cmpCreateNodeCalls = [&](CallInst* C1, CallInst* C2){
            for(CallInst* EdgeCall : EdgeCalls){
                if(EdgeCall->getArgOperand(0) == C1 && EdgeCall->getArgOperand(1) == C2){
                    return true;
                }
            }
            return false;

        };


        sort(CreateNodeNDCalls, cmpCreateNodeCalls);


        for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
            Instruction* II = &*I;

            CallInst* CI = dyn_cast<CallInst>(II);
            if(CI && CI->getCalledFunction() && (CI->getCalledFunction()->getName() == AttributesFuncName) ){
                AttributeCall = CI;
            }

            if(CI && CI->getCalledFunction() && (CI->getCalledFunction()->getName() == ReturnFuncName) ){
                ReturnCall = CI;
            }
        }

        for(auto* User: NodeFn->users()){
            CallInst* CI = dyn_cast<CallInst>(User);
            if(!CI || !isCreateNodeND(CI)) continue;
            Parents.insert(CI->getParent()->getParent());
        }

        


    }

    Function* NodeFn;
    vector<CallInst*> CreateNodeNDCalls;
    std::set<CallInst*> EdgeCalls;
    vector<CallInst*> BindInCalls;
    vector<CallInst*> BindOutCalls;
    std::set<Function*> Parents;
    CallInst* AttributeCall;
    CallInst* ReturnCall;
    bool isInternalNode;
    map<CallInst* , Argument*> EdgeMap; // Mapping a specific __hpvm__edge call to a specific argument in caller
    map<CallInst* , Argument*> BindOutMap; // Mapping a specific __hpvm__bindOut call to a specific argument in caller

};

class HPVMDAGInfo {
    public:
        HPVMDAGInfo(Module &M) : Mod(M) {
            LLVM_DEBUG(errs() << "=== HPVMDAGInfo ===\n");

            Function* HPVMLaunch = Mod.getFunction(LaunchFuncName);
            if(!HPVMLaunch){
                LLVM_DEBUG(errs()<<"Launch Function not found, returning early...\n");
                return;
            }

            for(auto* User : HPVMLaunch->users()){
                if(CallInst* CI = dyn_cast<CallInst>(User)){
                    Function* RootFunc = dyn_cast<Function>(CI->getArgOperand(1)->stripPointerCasts());
                    assert(RootFunc && "__hpvm__launch must have a specified function for the root node");

                    LaunchCalls.push_back(CI);
                    RootNodes.insert(RootFunc);
                }
            }


            for(Function* Root : RootNodes){
                LLVM_DEBUG(errs()<<"runOnNode on root "<<Root->getName()<<"\n");
                runOnNode(Root);
            }

            printDAGInfo();

        }


        void runOnNode(Function* Node);

        NodeInfo* getOrInsertNodeInfo(Function* F);

        void printDAGInfo();

        Argument* getParentArgumentForInputIdx(Function* Node, int input_idx, Function* ParentNode);
        void updateOutGoingFlows(Function* Node, int output_idx, Argument* CallerArg, Function* ParentNode);


        // Getters for querying information

        Argument* getArgForBindIn(CallInst* BI);
        Argument* getArgForBindOut(CallInst* BO);
        Argument* getArgForEdge(CallInst* E);

        bool isInternalNode(Function* Node);

        vector<CallInst*> getBindInCalls(Function* Node);
        vector<CallInst*> getBindOutCalls(Function* Node);
        vector<CallInst*> getEdgeCalls(Function* Node);
        vector<CallInst*> getCreateNodeNDCalls(Function* Node);

        vector<Function*> getParentNodes(Function* NodeFn);

        vector<Function*> getDAGInTopologicalOrder(Function* RootNode);

        vector<Function*> getSubGraph(Function* NodeFn);

        void sortNodesInTopologicalOrder(vector<Function*> Nodes);



    private:
        void printDAGInfoHelper(Function* NodeFunction, string prefix);

        Module &Mod;
        vector<CallInst*> LaunchCalls;
        std::set<Function*> RootNodes;
        map<Function* , NodeInfo* > NodeInfoMap;
};

extern HPVMDAGInfo* HPVMDAGInfoAnalysis(Module& M);
