#include "HPVMCGen.h"
#include <vector>
#include <unordered_map>
#include <string>

using namespace llvm;

class HPVMCGenInit : public HPVMCGenBase {
  public:
    HPVMCGenInit(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

    CallInst* insertCall(Instruction * InsertPoint);
};

class HPVMCGenCleanup : public HPVMCGenBase {
  public:
    HPVMCGenCleanup(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

    CallInst* insertCall(Instruction * InsertPoint);
};

class HPVMCGenBindIn : public HPVMCGenBase {
  private:

    std::unordered_map<Argument *, Argument *> Bind_Ins; // <Child, Parent>

  public:
    HPVMCGenBindIn(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

    CallInst* insertCall(Instruction *InsertPoint, CallInst *N, unsigned ip, unsigned ic, unsigned isStream);
};

class HPVMCGenBindOut : public HPVMCGenBase {
  private:
  
    std::unordered_map<Argument *, Argument *> Bind_Outs; // <Child, Parent>

  public:
    HPVMCGenBindOut(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) {  }

    CallInst* insertCall(Instruction *InsertPoint, CallInst *NodeInst, unsigned op, unsigned oc, unsigned isStream);
};

class HPVMCGenEdge : public HPVMCGenBase {
  private:

  std::unordered_map<Argument *, Argument *> Edges;

  public:
    HPVMCGenEdge(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

    CallInst* insertCall(Instruction * InsertPoint, CallInst *src, CallInst *dst, unsigned ReplType, unsigned sp, unsigned dp, unsigned isStream);
};

class HPVMCGenAttributes : public HPVMCGenBase {
  private:

  public:
  HPVMCGenAttributes (HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, std::vector<Value *> inputs, std::vector<Value *> outputs, std::vector<Value*> priv);
};

class HPVMCGenReturn : public HPVMCGenBase {
  private:

  public:
  HPVMCGenReturn(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, std::vector<Value *> returnValues);
};

class HPVMCGenGetNode : public HPVMCGenBase {
  private:

  public:
  HPVMCGenGetNode(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint);
};

class HPVMCGenGetParentNode : public HPVMCGenBase {
  private:

  public:
  HPVMCGenGetParentNode(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction *InsertPoint, Value *Node);
};

class HPVMCGenGetNodeInstanceID : public HPVMCGenBase {
  private:

  public:
  HPVMCGenGetNodeInstanceID(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, Value * Node, unsigned dimension);
};

class HPVMCGenGetNumNodeInstances : public HPVMCGenBase {
  private:

  public:
  HPVMCGenGetNumNodeInstances(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, Value * Node, unsigned dimension);
};

class HPVMCGenLaunch : public HPVMCGenBase {
  private:

  public:
  HPVMCGenLaunch(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, unsigned isStream, Value* rootGraph, std::vector<Value*> args);
};

class HPVMCGenWait : public HPVMCGenBase {
  private:

  public:
  HPVMCGenWait(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, Value* Graph);
};

class HPVMCGenPush : public HPVMCGenBase {
  private:

  public:
  HPVMCGenPush(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, Value* Graph, std::vector<Value *> args);
};

class HPVMCGenPop : public HPVMCGenBase {
  private:

  public:
  HPVMCGenPop(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, Value* Graph);
};

class HPVMCGenTrackMem : public HPVMCGenBase {
  private:

  public:
  HPVMCGenTrackMem(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, Value *Pointer, Value *Size);
};

class HPVMCGenUnTrackMem : public HPVMCGenBase {
  private:

  public:
  HPVMCGenUnTrackMem(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, std::vector<Value *> args);
};

class HPVMCGenRequestMem : public HPVMCGenBase {
  private:

  public:
  HPVMCGenRequestMem(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, std::vector<Value *> args);
};

class HPVMCGenHint : public HPVMCGenBase {
  private:

  public:
  HPVMCGenHint(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  CallInst* insertCall(Instruction* InsertPoint, std::vector<Value *> args);
};

class HPVMCGenExtractConstants : public HPVMCGenBase {
  private:

  public:
  HPVMCGenExtractConstants(HPVMCGenContext & HCGC) : HPVMCGenBase(HCGC) { }

  LoadInst* insertCall(Instruction* InsertPoint, ConstantInt *const_int) {
      return insertConstInt(InsertPoint, const_int);
  }
};
