#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemorySSA.h"
#include "llvm/Analysis/MemorySSAUpdater.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Attributes.h"


#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <algorithm>
#include <queue>

using namespace llvm;

using std::string;
using std::vector;
using std::map;
using std::unique_ptr;
using std::function;
using std::pair;
using std::sort;


#define DEBUG_TYPE "DFGUtils" 


Function* getCreateNodeNDFunction(CallInst* CI);

vector<Function*> getChildRoots(Function* F);

vector<CallInst*> getChildRootsCalls(Function* F);


int getTotalNumDimension(CallInst* NodeCall);
    

Value* getNDDimensionValue(int N, CallInst* CI);

vector<CallInst*> getDataFlowCalls(CallInst* NodeCall, string Intrinsic);

bool isChildNodeRedundant(CallInst* NodeCall);

bool isInternalNode(Function* F);

void mapParentOutputToChild(CallInst* Parent, CallInst* Child, map<int,int>& BindOutMap);

void mapParentInputToChild(CallInst* Parent, CallInst* Child, map<int,int>& BindInMap);


void removeRedundantNode(CallInst* Pre, CallInst* Mid, CallInst* Post);


bool isParSectionBeginMarker(CallInst* CI);


bool isCreateNodeND(CallInst* CI);


bool isParSectionEndMarker(CallInst* CI);

bool isParallelLoopMarker(CallInst* CI);

bool isTaskBeginMarker(CallInst* CI);

bool isLaunchBeginMarker(CallInst* CI);

bool FuncContainsCall(Function *ToSearch, Function *ToFind);

CallInst* getMarkerCall(Function*);

vector<Value*> getSectionArgOrdering(Function* SecFn);

Function* CreateClone(Function* Orig, std::set<Argument*> Exclude, ValueToValueMapTy& ArgMap);

CallInst* CloneCallInst(CallInst* OrigCall, Function* NewF, 
          std::set<Argument*>& Exclude, ValueToValueMapTy& ArgMap);

bool BBPrecedesEnd(BasicBlock* BB, BasicBlock* Begin, BasicBlock* End);
void getHPVMPrivArgs(CallInst* CI,std::vector<Value*>& PrivArguments);

void cleanFunctionName(Function* F);

void copyDefInRegion(Instruction* I,Instruction* BeginMarker, Instruction* EndMarker, ValueToValueMapTy &VMap, ValueToValueMapTy& RevVMap);

void copyAttributes(Argument* From, Argument* To);


void PropogateArgAttributes(Function* Root);

void PropogateArgsWrapper(Module& M);

Value* copyValueChain(Value* V, Instruction* IB);

BranchInst* getLoopGuard(Loop* L);

void RootRemoveBindOut(Module& M);

std::vector<Function*> getParentNodes(Function* ChildNode);

BasicBlock* getLoopLatch(Loop* L);
ICmpInst* getLatchICmpInst(Loop* L);
bool isLoopInclusive(Loop* L, Value* InductionVar);


bool isPrivCall(CallInst* CI);


bool isNonZeroCall(CallInst* CI);
Value* castIntegerToBitwidth(Value* V, Instruction* InsertBefore, int BV);


bool isHPVMGraphIntrinsic(Value* V);
