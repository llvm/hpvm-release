//===- HCCVerifierPass.h ----- HeteroC++ Verification Pass ------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HCCVerifierPass.h -- Contains declarations for the HeteroC++ verification
/// pass
//
//===----------------------------------------------------------------------===//

#ifndef __HCC_VERIFIER_PASS_H_
#define __HCC_VERIFIER_PASS_H_

#include "llvm/Pass.h"
#include "HCCVerifier.h"

using namespace llvm;

namespace llvm {
  
// Module pass for verifying a Hetero-C++ program
class HCCVerifierPass : public ModulePass {
public:
  HCCVerifierPass();
  bool runOnModule(Module &M) override;
  static char ID;
};

void initializeHCCVerifierPassPass(PassRegistry &);

}

#endif // ifndef __HCC_VERIFIER_PASS_H_
