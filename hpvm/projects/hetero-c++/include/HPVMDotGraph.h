//===- extract-task.h -- Extract a parallel task into node function ------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM
// Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===---------------------------------------------------------------------===//
//
// This file extracts a parallel loop or task identified by a hpvm marker
// into its own node function, repeating that for all marked tasks.
// The marker function must be named "hpvm_parallel_loop".
//
//===---------------------------------------------------------------------===//

#include "llvm/IR/Module.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include <string>
using namespace llvm;


extern void DFG2Dot(Module& M, std::string suffix);
