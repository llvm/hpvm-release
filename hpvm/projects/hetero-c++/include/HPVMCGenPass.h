//===- HPVMCGenPass.h - HPVM-C generation module passes -------*- C++ -*-===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===----------------------------------------------------------------------===//
//
/// @file
/// HPVMCGenPass.h -- Contains declarations of passes for HPVM-C generation
//
//===----------------------------------------------------------------------===//

#ifndef __HPVM_CGEN_PASS_H__
#define __HPVM_CGEN_PASS_H__

#include "llvm/Pass.h"
using namespace llvm;

namespace llvm {

  class HPVMCGenContext;
  class Module;
  class AnalysisUsage;

  enum HPVMCGenMode {
      FULL,
      EXTRACT_ONLY,
      LOCATE_ONLY
  };

  struct HPVMCGenPassOpts {
      HPVMCGenMode Mode;
      bool SanatiseFunctionNames;
      bool Use0DNode;
      bool RemoveDummyNodes;
      bool NoReturnSizes;
      bool PrintDot;
  };

// Module pass for translation to HPVM-C
//
class HPVMCGenPass: public ModulePass {
  HPVMCGenContext* cgenContext;
  // int HPVMCGenMode;
  HPVMCGenPassOpts Opts;

public:
	HPVMCGenPass();
    HPVMCGenPass(HPVMCGenPassOpts);

public:
	bool runOnModule(Module &M) override;
		
  void getAnalysisUsage(AnalysisUsage &AU) const override;

	static char ID;
};

void initializeHPVMCGenPassPass(PassRegistry &);
  
}

#endif // ifndef __HPVM_CGEN_PASS_H__ 
