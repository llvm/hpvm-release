//===- DominatorTreeCache.h -- Caches dominator and post-dominator trees --===//
//
// Part of the HPVM Project. License TBD.
// SPDX-License-Identifier: TBD
//
//===---------------------------------------------------------------------===//
//
// @file
// DominatorTreeCache.h -- Provides declarations for the dominator tree cache
//
//===---------------------------------------------------------------------===//

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include <map>

using namespace llvm;

class DominatorTreeCache {
public:
  struct DominatorTreePair {
    DominatorTreePair(Function *F) : DT(*F), PDT(*F) {}
    DominatorTree DT;
    PostDominatorTree PDT;
  };

  DominatorTreePair& get(Function *F);
  bool dominates(BasicBlock *A, BasicBlock *B);
  bool dominates(Instruction *A, Instruction *B);
  bool postDominates(Instruction *A, Instruction *B);
  bool postDominates(BasicBlock *A, BasicBlock *B);
  void reset();
  void reset(Function *F);

private:
  std::map<Function*, std::unique_ptr<DominatorTreePair>> Cache;
};

