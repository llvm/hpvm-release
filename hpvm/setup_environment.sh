#!/bin/bash
# This is a script for setting up the environment when using HPVM2FPGA tools on Intel VLAB.
# Usage: ./setup_environment.sh [class]
#   * class: the class of fpga machine on Intel VLAB. Defaults to "fpga-pac-a10".

module load cmake
module load gnu8
module load opencv
export TERM=xterm-256color
[ "$1" ] && class="$1" || class=fpga-pac-a10
echo "Setting up environment for $class"
source /export/fpga/bin/setup-fpga-env $class
