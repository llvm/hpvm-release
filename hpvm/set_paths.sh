#!/bin/bash

# These paths can be modified by the HPVM user
CUDA_TOOLKIT_PATH=""  # Set this to the root of your CUDA Installation
export CUDA_TOOLKIT_PATH=$CUDA_TOOLKIT_PATH
if [ -z "$CUDA_TOOLKIT_PATH" ]
then
    echo "ERROR: SET CUDA_TOOLKIT_PATH to the Root of your CUDA Installation"
else
    CUDA_INCLUDE_PATH=$CUDA_TOOLKIT_PATH/include
    CUDA_LIB_PATH=$CUDA_TOOLKIT_PATH/lib64/
    echo "Setting environment paths..."

    # Setting CUDA paths here
    export CUDA_TOOLKIT_ROOT_DIR=$CUDA_TOOLKIT_PATH
    export CUDA_BIN_PATH=$CUDA_TOOLKIT_PATH
    export CUDA_INCLUDE_PATH=$CUDA_INCLUDE_PATH
    export CUDNN_PATH=$CUDA_LIB_PATH
    export LIBRARY_PATH=$CUDA_LIB_PATH:$LIBRARY_PATH
    export LD_LIBRARY_PATH=$CUDA_LIB_PATH:$LD_LIBRARY_PATH
    echo "Finished setting environment paths!"
fi

# This part came from fpga branch
SH="$(readlink -f /proc/$$/exe)"
if [[ "$SH" == "/bin/zsh" ]]; then
  DIR="${0:A:h}"
else
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
fi
echo "Adding to PATH: $DIR/build/bin"
export PATH=$DIR/build/bin:$PATH
echo "Exporting HPVM_ROOT = $DIR"
export HPVM_ROOT=$DIR
echo "Exporting HYPERMAPPER_HOME = $HPVM_ROOT/projects/hypermapper"
export HYPERMAPPER_HOME=$HPVM_ROOT/projects/hypermapper
echo "Exporting LLVM_SRC_ROOT = $HPVM_ROOT/llvm"
export LLVM_SRC_ROOT=$HPVM_ROOT/llvm
