## HPVM Lib

This directory contains the C++ source files for the `HPVM`infrastruture. This are divided into the following categories:

- `CoreHPVM`: Consists of the common Code Generation files used by the `HPVM` backend targets. Additionally, it defines various utilities which can be used within the specific target backend passes.
- `IRCodeGen`: Defines the transformation passes which generate the `HPVM` IR and Dataflow graph representation from `LLVM` bitcode files written using `HPVM-C`.
- `Targets`: Contains the instances of the specific backend targets `HPVM` compiles to.
- `Transforms`: Defines the transformation passes which perform either DFG to DFG transformations, or transformations on `LLVM-IR`.
