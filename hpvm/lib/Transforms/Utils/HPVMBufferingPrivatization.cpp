#define DEBUG_TYPE "hpvmoptutils"
#include "Transforms/Utils/HPVMBufferingPrivatization.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

using namespace hpvm;
using namespace llvm;

// Function that checks if argument users are all Loads (used for BufferIn).
bool hpvm::checkArgUsers(Value *Arg) {
  bool unhandled = 0;
  int hasStoreUsers = 0;
  for (auto u : Arg->users()) {
    LLVM_DEBUG(errs() << "Argument User: " << *u << "\n");
    if (GetElementPtrInst *GEPI = dyn_cast<GetElementPtrInst>(u)) {
      for (auto uu : GEPI->users()) {
        if (StoreInst *SI = dyn_cast<StoreInst>(uu)) {
          LLVM_DEBUG(errs() << "GEPI is used by a store!\n");
          hasStoreUsers++;
        } else if (LoadInst *LI = dyn_cast<LoadInst>(uu)) {
          LLVM_DEBUG(errs() << "GEPI is used by a load!\n");
        } else {
          LLVM_DEBUG(errs() << "GEPI user: " << *uu << "\n");
          if (BitCastInst *BI = dyn_cast<BitCastInst>(uu)) {
            for (auto biu : BI->users()) {
              if (StoreInst *SI = dyn_cast<StoreInst>(biu)) {
                LLVM_DEBUG(errs() << "BitCast is used by a store!\n");
                hasStoreUsers++;
              } else if (LoadInst *LI = dyn_cast<LoadInst>(biu)) {
                LLVM_DEBUG(errs() << "BitCast is used by a load!\n");
              } else {
                LLVM_DEBUG(errs() << "BitCast user: " << *biu << "\n");
                unhandled = 1;
                LLVM_DEBUG(errs() << "Unhandled BitCast user\n");
              }
            }
          } else {
            LLVM_DEBUG(errs() << "GEPI user: " << *uu << "\n");
            unhandled = 1;
            LLVM_DEBUG(errs() << "Unhandled GEPI user\n");
          }
        }
      }
    } else if (CallInst *CI = dyn_cast<CallInst>(u)) {    
        if (IntrinsicInst *II = dyn_cast<IntrinsicInst>(CI)) {
          if (II->getIntrinsicID() == Intrinsic::hpvm_ivdep) {
            LLVM_DEBUG(errs() << "Argument is used by ivdep!\n");
          } else {
            unhandled = 1;
            LLVM_DEBUG(errs() << "Unhandled Intrinsic Instuction user\n");
          }     
        } else {
          unhandled = 1;
          LLVM_DEBUG(errs() << "Unhandled CallInst user: " << *CI << "\n");
        }
    } else if (LoadInst *LI = dyn_cast<LoadInst>(u)) {
      LLVM_DEBUG(errs() << "User is a load!\n");
    } else {
      unhandled = 1;
      LLVM_DEBUG(errs() << "Unhandled Argument user!\n");
    }
  }
  if (hasStoreUsers > 0) {
    LLVM_DEBUG(errs() << "Skipping argument that has store users!\n");
    return false;
  } else if (unhandled) {
    LLVM_DEBUG(errs() << "Skipping unhandled argument!\n");
    return false;
  }
  return true;
}

// Function that goes through all arguments of an HPVM node function, and buffers
// the ones that are read only and have a constant size with # elements < threshold
// Node has to be sequentialized. MaxIBSize is the threshold.
void hpvm::bufferNodeInputs(DFNode *N, const DataLayout &DL, unsigned MaxIBSize) {
  assert(N->isSequential() && "Can only buffer inputs for sequential(ized) nodes.\n");
  Function *F = N->getFuncPointer();
  Module *M = F->getParent();
  // Go through all the params and check for input arrays
  LLVM_DEBUG(errs() << "Processing argument list for function: " << F->getName()
               << "\n");
  int argnum = 0;
  for (auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
    Value *Arg = &*ai;
    // We only care about pointer arguments
    if (Arg->getType()->isPointerTy()) {
      LLVM_DEBUG(errs() << "Found argument: " << argnum << "--" << (*Arg) << "\n");

      // Find argument where all the uses are loads, i.e. no stores
      if (checkArgUsers(Arg)) {

        LLVM_DEBUG(errs() << "Attempting to buffer Argument: " << *Arg << "\n");
        
        // Check array size in host.
        Value *ArgSize = findArraySize(argnum, N);
        assert(ArgSize != nullptr && "findArraySize didn't return a Value!");
        
        LLVM_DEBUG(errs() << "Argument Size Value: " << *ArgSize << "\n");
        
        // Check if array size is constant
        if (ConstantInt *ArgSizeConstInt = dyn_cast<ConstantInt>(ArgSize)) {
          // Get number of elements from the array size.
          // ArgType Size in Bytes
          unsigned ArgTypeSize =
              DL.getTypeSizeInBits(Arg->getType()->getPointerElementType()) / 8;
          // Num Elts = ArgSize (bytes) / ArgTypeSize (bytes)
          unsigned numArrayElements =
              ArgSizeConstInt->getZExtValue() / ArgTypeSize;
          // Create a Value for NumElts
          Value *localArraySize = ConstantInt::get(
              Type::getInt64Ty(M->getContext()), numArrayElements);
          LLVM_DEBUG(errs() << "ArgTypeSize: " << ArgTypeSize
                       << "; numArrayElements: " << numArrayElements
                       << "; localArraySize: " << *localArraySize << "\n");
          // If number of elements is less than threshold, buffer the input.
          if (numArrayElements < MaxIBSize) {
            LLVM_DEBUG(errs() << "Buffering Argument: " << *Arg << "\n");
            hpvm::buffer_in(Arg, localArraySize, F);
          }
        } else {
          LLVM_DEBUG(errs()
                << "Skipping argument because array size is not a constant!\n");
        }
      }
    }
    argnum++;
  }
}

// Function that buffers Argument and replaces uses with new local variable
void hpvm::buffer_in(Value *Arg, Value *Arg_sz, Function *F) {
  LLVM_DEBUG(errs() << "Original function" << *F);

  // Create local buffer of correct size using alloca
  BasicBlock *EntryBlock = &(F->getEntryBlock());
  IRBuilder<> Builder(EntryBlock->getFirstNonPHI());
  Twine localArgName = "hpvm_fpga_local_" + Arg->getName();
  AllocaInst *BufferAlloca = Builder.CreateAlloca(
      Arg->getType()->getPointerElementType(), Arg_sz, localArgName);
  LLVM_DEBUG(errs() << "AllocaInst: " << *BufferAlloca << "\n");

  // Replace all uses of original buffer with local memory buffer
  Arg->replaceAllUsesWith(BufferAlloca);

  // Split EntryBlock such that alloca remains in EntryBlock and the rest is in
  // EntryBlockEnd
  BasicBlock *EntryBlockEnd = EntryBlock->splitBasicBlock(
      BufferAlloca->getNextNode(), "loop." + Arg->getName() + ".done");

  BasicBlock *LoopBody = BasicBlock::Create(
      F->getContext(), "loop." + Arg->getName() + ".body", F);

  // Point the branch out of the entry block to go to loop_body
  ReplaceInstWithInst(EntryBlock->getTerminator(),
                      BranchInst::Create(LoopBody));

  IRBuilder<> LoopBodyBuilder(LoopBody);
  PHINode *loopVarPhi = LoopBodyBuilder.CreatePHI(
      Type::getInt64Ty(F->getContext()), 2, "loop_i_" + Arg->getName());
  // i = 0 if coming in from EntryBlock, we will add the other one later
  loopVarPhi->addIncoming(
      ConstantInt::get(Type::getInt64Ty(F->getContext()), 0), EntryBlock);
  // local buffer[i] = input buffer[i]
  Value *LocalBufferGEP = LoopBodyBuilder.CreateGEP(BufferAlloca, loopVarPhi);
  Value *InputBufferGEP = LoopBodyBuilder.CreateGEP(Arg, loopVarPhi);
  LoadInst *InputLoad = LoopBodyBuilder.CreateLoad(InputBufferGEP);
  LoopBodyBuilder.CreateStore(InputLoad, LocalBufferGEP);
  // i' = i + 1
  Value *loopVarIncrement = LoopBodyBuilder.CreateAdd(
      loopVarPhi, ConstantInt::get(Type::getInt64Ty(F->getContext()), 1));
  // cmp i', size
  Value *loopVarCmp =
      LoopBodyBuilder.CreateICmp(ICmpInst::ICMP_EQ, loopVarIncrement, Arg_sz);
  // if (i == size) exit loop, otherwise go back to loop body
  LoopBodyBuilder.CreateCondBr(loopVarCmp, EntryBlockEnd, LoopBody);
  // Add the remaining input for the phi node
  loopVarPhi->addIncoming(loopVarIncrement, LoopBody);

  LLVM_DEBUG(errs() << "Entire function: " << *F);
}

// Finds a store to a particular struct's element, returns nullptr if
// not found
// Assumes the following structure:
// - Input is a pointer to struct (may be i8* e.g. from malloc)
// - Zero or more bitcasts to the struct pointer, ending up as struct
// pointer or i8*
// - Followed by getelementptr
// - Followed by zero or more bitcasts
// - Followed by a store
static Value *findStructElemStore(llvm::Value *StructPtr, unsigned elem, size_t Offset) 
{
  using namespace llvm;

  PointerType *StructPtrTy = cast<PointerType>(StructPtr->getType());

  // Find chains that lead to a store
  // Worklist contains either:
  // - (struct pointer or bitcast of it, false)
  // - (getelementptr or bitcast of it, true)
  std::vector<std::pair<Value*, bool>> Worklist;
  Worklist.emplace_back(StructPtr->stripPointerCasts(), false);
  while (!Worklist.empty()) {
    Value *Ptr = Worklist.back().first;
    bool isElem = Worklist.back().second;;
    Worklist.pop_back();
    for (User *U : Ptr->users()) {
      if (BitCastInst *BCI = dyn_cast<BitCastInst>(U)) {
        if (BCI->getType()->isPointerTy())
          Worklist.emplace_back(BCI, isElem);
      } else if (GetElementPtrInst *GEPI = dyn_cast<GetElementPtrInst>(U)) {
        if (isElem) continue;

        // If we have getelementptr at the right index into the
        // struct, transition into looking for a store
        
        bool IsStructPtr = cast<PointerType>(Ptr->getType())->getElementType()->isStructTy();
        unsigned IdxIdx = 1 ? IsStructPtr : 0;
        unsigned IdxCount = GEPI->getNumIndices();
        assert(IdxCount == IdxIdx + 1);
        if (ConstantInt *IndexVal = cast<ConstantInt>(GEPI->idx_end() - 1)) {
          if (IsStructPtr && IndexVal->getZExtValue() == elem)
            Worklist.emplace_back(GEPI, true);
          else if (!IsStructPtr && IndexVal->getZExtValue() == Offset)
            Worklist.emplace_back(GEPI, true);
        } else {
          errs() << "Warning: non-constant struct indexing: " << *GEPI << "\n";
        }
      } else if (StoreInst *SI = dyn_cast<StoreInst>(U)) {
        // A store at the original struct's address can be a store to
        // the first element
        if (isElem || elem == 0)
          return SI->getValueOperand();
      }
    }
  }
}

// Function that finds the array size in the host code
Value * hpvm::findArraySize(int argnum, DFNode *N) {
  int portnum = argnum + 1;
  LLVM_DEBUG(errs() << "argnum: " << argnum << ", portnum: " << portnum << "\n");
  DFEdge *dfe;
  DFNode *nextNode = N;
  int i = 0;
  while (nextNode->getParent() != NULL) {
    LLVM_DEBUG(errs() << "Node: " << nextNode->getFuncPointer()->getName() << "\n");
    LLVM_DEBUG(errs() << "Parent: "
                 << nextNode->getParent()->getFuncPointer()->getName() << "\n");
    for (DFNode::indfedge_iterator ieb = nextNode->indfedge_begin(),
                                   iee = nextNode->indfedge_end();
         ieb != iee; ieb++) {
      unsigned destPos = (*ieb)->getDestPosition();
      LLVM_DEBUG(errs() << "Edge: "
                   << (*ieb)->getSourceDF()->getFuncPointer()->getName() << ": "
                   << (*ieb)->getSourcePosition() << " --> "
                   << (*ieb)->getDestDF()->getFuncPointer()->getName() << ": "
                   << (*ieb)->getDestPosition() << "\n");
      if (destPos == portnum) {
        LLVM_DEBUG(errs() << "Found DFEdge!\n");
        dfe = *ieb;
        break;
      }
    }
    nextNode = dfe->getSourceDF();
    portnum = dfe->getSourcePosition();
    LLVM_DEBUG(errs() << "i = " << i
                 << ": Source node: " << nextNode->getFuncPointer()->getName()
                 << "; portnum: " << portnum << "\n");
    if (nextNode->isDummyNode()) {
      LLVM_DEBUG(errs() << "next node is a dummy node, switching to parent!\n");
      nextNode = nextNode->getParent();
      LLVM_DEBUG(errs() << "new next node: " << nextNode->getFuncPointer()->getName()
                   << "\n");
    } else {
      LLVM_DEBUG(errs() << "unhandled case!\n");
      return nullptr;
    }
    //    assert(i < 2);
    i++;
  }
  IntrinsicInst *II = nextNode->getInstruction();
  assert(II->getIntrinsicID() == Intrinsic::hpvm_launch &&
         "Invalid intrinsic!\n");

  Value *rootFunc = II->getArgOperand(0);
  assert(isa<ConstantExpr>(rootFunc) &&
         "Launch function is not a constant expression!");
  ConstantExpr *rootFuncInst = cast<ConstantExpr>(rootFunc);
  LLVM_DEBUG(errs() << *(rootFuncInst) << "\n");
  LLVM_DEBUG(errs()
        << *(rootFuncInst->getOperand(0)->getType()->getPointerElementType())
        << "\n");
  LLVM_DEBUG(errs() << rootFuncInst->getOperand(0)
                      ->getType()
                      ->getPointerElementType()
                      ->isFunctionTy()
               << "\n");
  FunctionType *rootFuncPointerTy = cast<FunctionType>(
      rootFuncInst->getOperand(0)->getType()->getPointerElementType());
  unsigned offset = 0;
  unsigned counter = 0;
  for (auto &ArgTy : rootFuncPointerTy->params()) {
    LLVM_DEBUG(
        errs()
        << *ArgTy << " "
        << N->getFuncPointer()->getParent()->getDataLayout().getTypeSizeInBits(
               ArgTy)
        << "\n");
    offset +=
        N->getFuncPointer()->getParent()->getDataLayout().getTypeSizeInBits(
            ArgTy) /
        8;
    counter++;
    LLVM_DEBUG(errs() << "-- counter: " << counter << ", offset: " << offset
                 << "\n");
    if (counter == portnum) {
      LLVM_DEBUG(errs() << "Found the desired edge port! Breaking!\n");
      break;
    }
  }

  LLVM_DEBUG(errs() << "Offset: " << offset << "\n");
  Value *rootArgStruct = II->getArgOperand(1);
  return findStructElemStore(rootArgStruct, portnum, offset);
}

// Function that promotes a pointer arg to a private variable
void hpvm::promote_priv(Value *Arg, Value *Arg_sz, Function *F) {
  LLVM_DEBUG(errs() << "Promoting: \n"
               << Arg->getName() << "\n"
               << *(Arg->getType()) << "\n"
               << *Arg_sz << "\n");
  // Add an alloca for the array argument at the beginning of the function
  BasicBlock *EntryBlock = &(F->getEntryBlock());
  IRBuilder<> Builder(EntryBlock->getFirstNonPHI());
  Twine localArgName = "hpvm_fpga_local_" + Arg->getName();
  AllocaInst *AInst = Builder.CreateAlloca(
      Arg->getType()->getPointerElementType(), Arg_sz, localArgName);
  LLVM_DEBUG(errs() << "AllocaInst: " << *AInst << "\n");

  // Replace all uses of the original argument
  Arg->replaceAllUsesWith(AInst);
}

// Function that privatizes all the arguments of Node N
void hpvm::privatizeNodeArguments(DFNode *N) {
  Function * F = N->getFuncPointer();
  LLVM_DEBUG(errs() << "Processing argument list for function: " << F->getName() << "\n");
  int argnum = 0;
  for(auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
    Value *Arg = &*ai;
    // We only care about pointer arguments
    if (Arg->getType()->isPointerTy()) {
      LLVM_DEBUG(errs() << (*Arg) << "\n");
      if (F->getAttributes().hasAttribute(ai->getArgNo()+1, Attribute::Priv)) {
        LLVM_DEBUG(errs() << "Promoting Priv argument: " << *Arg << "\n");
				
				// Get buffer size from attribute
				unsigned BufferSize = F->getAttribute(ai->getArgNo()+1, Attribute::Priv).getBuffSize(); 
				LLVM_DEBUG(errs() << "BufferSize: " << BufferSize << "\n");
        
				// check if array size is constant
				ConstantInt *ArgSizeValue = ConstantInt::get(Type::getInt64Ty(F->getContext()), BufferSize);
        if (ArgSizeValue) {
          promote_priv(Arg, ArgSizeValue, F);
					LLVM_DEBUG(errs() << "Array Size: " << *ArgSizeValue << "\n");

        } else {
          assert(false && "Could not create constant from BufferSize");
        }
      }
    }
    argnum++;
  }
}
