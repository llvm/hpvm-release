#include "llvm/IR/Intrinsics.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/LowerMemIntrinsics.h"
#define DEBUG_TYPE "hpvmoptutils"
#include "Transforms/Utils/HPVMLoopTransforms.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Analysis/LoopInfo.h"
#include "IRCodeGen/BuildDFG.h"
#include "llvm/Transforms/Utils/UnrollLoop.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/Analysis/LoopIterator.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/ADT/Statistic.h"
#include <chrono>
#include <unordered_set>

using namespace llvm;
using namespace builddfg;
using namespace hpvm;

STATISTIC(NumFusions, "The number of succesful fusions");
STATISTIC(NumLoopsChecked, "The number of pairs of loops checked for fusion");
STATISTIC(NumFusionsAttempted,
          "The number of loops of which fusion was attempted");
STATISTIC(NumMemOpsCompared, "The number of mem ops compared");
STATISTIC(NumDepsFound, "The number of dependences from L0ToL1");
STATISTIC(NumIllegalDepsFound, "The number of illegal dependences");

bool checkDeps(std::vector<Dep> Deps, Function *F, FusionCandidate *FC0,
               PHINode *L0IV);

/*************************************************************************/
/********************** Loop Unroll Methods ******************************/
/*************************************************************************/
// Function to retrieve loop guard branch
// Needs to be replaced when moving to LLVM 10.0
BranchInst *hpvm::getLoopGuardBranch(Loop *L, ScalarEvolution *SE) {
  LLVM_DEBUG(errs() << "\nLooking for Loop Guard Branch\n");
  if (!L->isLoopSimplifyForm()) {
    LLVM_DEBUG(errs() << "Loop is not in loopsimplify form!\n");
    return nullptr;
  }

  BasicBlock *Preheader = L->getLoopPreheader();
  BasicBlock *Latch = L->getLoopLatch();
  assert(Preheader && Latch &&
         "Expecting a loop with valid preheader and latch");

  // Loop should be in rotate form.
  if (!L->isLoopExiting(Latch)) {
    LLVM_DEBUG(errs() << "Loop is not in rotate form!\n");
    return nullptr;
  }

  // Disallow loops with more than one unique exit block, as we do not verify
  // that GuardOtherSucc post dominates all exit blocks.
  BasicBlock *ExitFromLatch = L->getUniqueExitBlock();
  if (!ExitFromLatch) {
    LLVM_DEBUG(errs() << "Loops does not exit from latch!\n");
    return nullptr;
  }

  BasicBlock *GuardBB = Preheader->getUniquePredecessor();
  if (!GuardBB) {
    LLVM_DEBUG(errs() << "No GuardBB found!\n");
    return nullptr;
  }

  assert(GuardBB->getTerminator() && "Expecting valid guard terminator");

  BranchInst *GuardBI = dyn_cast<BranchInst>(GuardBB->getTerminator());
  LLVM_DEBUG(errs() << "Potential guard branch: " << *GuardBI << "\n");
  if (!GuardBI || GuardBI->isUnconditional()) {
    LLVM_DEBUG(errs() << "Coundln't find Guard branch, or it is unconditional!\n");
    return nullptr;
  }

  BasicBlock *GuardOtherSucc = (GuardBI->getSuccessor(0) == Preheader)
                                   ? GuardBI->getSuccessor(1)
                                   : GuardBI->getSuccessor(0);

  if (L->contains(GuardOtherSucc)) {
    LLVM_DEBUG(errs() << "Guard branch does not skip past the loop!\n");
    return nullptr;
  }

  // Attempt to use loop bounds comparison to determine loop bounds.
  // If no success, use other method.
  // Check guard branch condition and compare it to the loop bounds
  if (CmpInst *GuardBICond = dyn_cast<CmpInst>(GuardBI->getCondition())) {
    LLVM_DEBUG(errs() << "condition: " << *(GuardBICond) << "\n");
    CmpInst::Predicate pred = GuardBICond->getPredicate();
    LLVM_DEBUG(errs() << "predicate: " << CmpInst::getPredicateName(pred) << "\n");
    if (pred == CmpInst::ICMP_SGT) {

      Value *condOp0 = GuardBICond->getOperand(0);
      Value *condOp1 = GuardBICond->getOperand(1);
      LLVM_DEBUG(errs() << "operand 0: " << *(condOp0) << "\n");
      LLVM_DEBUG(errs() << "operand 1: " << *(condOp1) << "\n");

      Optional<Loop::LoopBounds> LB = L->getBounds(*SE);
      assert(LB.hasValue() && "unable to retrieve loop bound object!");

      Value *IValue = (LB->getInitialIVValue()).stripPointerCasts();
      Value *FValue = (LB->getFinalIVValue()).stripPointerCasts();

      LLVM_DEBUG(errs() << "IValue: " << *(IValue) << "\n");
      LLVM_DEBUG(errs() << "FValue: " << *(FValue) << "\n");

      if (CastInst *CI = dyn_cast<CastInst>(IValue)) {
        LLVM_DEBUG(errs() << "IValue is a cast inst!\n");
        IValue = CI->getOperand(0);
        LLVM_DEBUG(errs() << "IValue: " << *(IValue) << "\n");
      }
      if (CastInst *CI = dyn_cast<CastInst>(FValue)) {
        LLVM_DEBUG(errs() << "FValue is a cast inst!\n");
        FValue = CI->getOperand(0);
        LLVM_DEBUG(errs() << "FValue: " << *(FValue) << "\n");
      }

      bool IValueMatch = false;
      bool FValueMatch = false;
      // Compare IValue against both operands
      if (ConstantInt *IValueInt = dyn_cast<ConstantInt>(IValue)) {
        if (ConstantInt *condOp0Int = dyn_cast<ConstantInt>(condOp0)) {
          if (IValueInt->getSExtValue() == condOp0Int->getSExtValue()) {
            IValueMatch = true;
            LLVM_DEBUG(errs() << "IValue matches condOp0\n");
          }
        }
        if (!IValueMatch) {
          if (ConstantInt *condOp1Int = dyn_cast<ConstantInt>(condOp1)) {
            if (IValueInt->getSExtValue() == condOp1Int->getSExtValue()) {
              IValueMatch = true;
              LLVM_DEBUG(errs() << "IValue matches condOp1\n");
            }
          }
        }
      } else {
        if (IValue == condOp0 || IValue == condOp1) {
          IValueMatch = true;
          LLVM_DEBUG(errs() << "Found IValue non-constant match\n");
        }
      }

      if (ConstantInt *FValueInt = dyn_cast<ConstantInt>(FValue)) {
        if (ConstantInt *condOp0Int = dyn_cast<ConstantInt>(condOp0)) {
          if (FValueInt->getSExtValue() == condOp0Int->getSExtValue()) {
            FValueMatch = true;
            LLVM_DEBUG(errs() << "FValue matches condOp0\n");
          }
        }
        if (!FValueMatch) {
          if (ConstantInt *condOp1Int = dyn_cast<ConstantInt>(condOp1)) {
            if (FValueInt->getSExtValue() == condOp1Int->getSExtValue()) {
              FValueMatch = true;
              LLVM_DEBUG(errs() << "FValue matches condOp1\n");
            }
          }
        }
      } else {
        if (FValue == condOp0 || FValue == condOp1) {
          FValueMatch = true;
          LLVM_DEBUG(errs() << "Found FValue non-constant match\n");
        }
      }

      if (IValueMatch && FValueMatch) {
        LLVM_DEBUG(errs() << "Found the loop guard branch!\n\n");
        return GuardBI;
      } else {
        LLVM_DEBUG(errs() << "Comparison operands don't match loop start and end "
                        "values!\n\n");
      }
    } else {
      LLVM_DEBUG(errs() << "GuardBI condition predicate is invalid!\n");
    }
  } else {
    LLVM_DEBUG(errs() << "GuardBI Condition is not a CmpInst! \n");
  }

  // If we get to this point, we didn't find the loop guard branch using loop
  // bounds
  BranchInst *ExitFromLatchTerminator =
      dyn_cast<BranchInst>(ExitFromLatch->getTerminator());
  if (ExitFromLatchTerminator->isConditional()) {
    BasicBlock *ExitFromLatchSucc0 = ExitFromLatchTerminator->getSuccessor(0);
    BasicBlock *ExitFromLatchSucc1 = ExitFromLatchTerminator->getSuccessor(1);

    return (GuardOtherSucc == ExitFromLatchSucc0 ||
            GuardOtherSucc == ExitFromLatchSucc1)
               ? GuardBI
               : nullptr;
  } else {
    BasicBlock *ExitFromLatchSucc = ExitFromLatchTerminator->getSuccessor(0);

    return (GuardOtherSucc == ExitFromLatchSucc) ? GuardBI : nullptr;
  }
}

// Function that takes a loop guard branch and removes it
void hpvm::removeLoopGuardBranch(Loop *L, BranchInst *BI) {
  BasicBlock *ExitBlock = L->getUniqueExitBlock();
  assert(ExitBlock && "Couldn't get unique exit block!");
  LLVM_DEBUG(errs() << "Exit Block: " << ExitBlock->getName() << "\n");
  BasicBlock *ExitBlockSucc = (BI->getSuccessor(0) == L->getLoopPreheader())
                                  ? BI->getSuccessor(1)
                                  : BI->getSuccessor(0);
  LLVM_DEBUG(errs() << "ExitBlockSucc: " << ExitBlockSucc->getName() << "\n");
  BasicBlock *GuardBlock = BI->getParent();
  LLVM_DEBUG(errs() << "Guard Block: " << GuardBlock->getName() << "\n");
  // Remove edge to the ExetBlockSucc block
  ExitBlockSucc->removePredecessor(GuardBlock);
  BasicBlock *Preheader = L->getLoopPreheader();
  assert(Preheader && "Loop does not have a preheader!\n");
  LLVM_DEBUG(errs() << "Preheader: " << Preheader->getName() << "\n");
  // Create new unconditional branch
  BranchInst *UBI = BranchInst::Create(Preheader, BI);
  BI->eraseFromParent();
  LLVM_DEBUG(
      errs() << "Replaced loop guard branch with unconditional branch!\n\n\n");
}

// Function that gathers the loop tripcounts by using the
// NZLoop HPVM intrinsic
std::map<llvm::BasicBlock *, int> hpvm::getFuncLoopTripCounts(Function *F,
                                                              bool KeepI) {
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  std::vector<IntrinsicInst *> IItoRemove;
  std::map<BasicBlock *, int> TripCountMap;
  Function *NZLoop =
      Intrinsic::getDeclaration(F->getParent(), Intrinsic::hpvm_nz_loop);
  for (auto *User : NZLoop->users()) {
    IntrinsicInst *II = dyn_cast<IntrinsicInst>(User);
    if (II->getParent()->getParent() != F)
      continue;
    // Intrinsics in the entry block are for the sequentialized loops.
    // If they are still there it means sequentialization hasn't happened yet,
    // so we shouldn't process/remove them.
    if (II->getParent() == &F->getEntryBlock())
      continue;
    LLVM_DEBUG(errs() << *II << "\n");
    PHINode *IndVar = dyn_cast<PHINode>(II->getArgOperand(0));
    if (IndVar == nullptr) {
      LLVM_DEBUG(errs() << "Couldn't get Induction Variable from intrinsic. Marking "
                      "it for removal and skipping.\n");
      IItoRemove.push_back(II);
      continue;
    }
    assert(IndVar != nullptr && "Couldn't get PHINode from NZLoop!");
    // The PHINode correspondiong to the induction variable will be in the loop
    // header
    BasicBlock *LHeader = IndVar->getParent();
    Loop *L = LI.getLoopFor(LHeader);
    assert(L->getHeader() == LHeader && "Expecting header!");
    // Make sure the intrinsic PHI Node operand is the Induction Variable
    PHINode *IV = L->getInductionVariable(SE);
    assert(IV && "couldn't get induction variable!");
    assert(IndVar == IV &&
           "Intrinsic Operand is not the loop induction variable!");
    assert(isa<ConstantInt>(II->getOperand(1)) &&
           "Expecting second operand of NZ Loop Intrinsic to be an int!");
    TripCountMap[L->getHeader()] =
        dyn_cast<ConstantInt>(II->getOperand(1))->getZExtValue();
    if (!KeepI)
      IItoRemove.push_back(II);
  }
  for (std::vector<IntrinsicInst *>::reverse_iterator ri = IItoRemove.rbegin(),
                                                      re = IItoRemove.rend();
       ri != re; ++ri)
    (*ri)->eraseFromParent();

  return TripCountMap;
}

// Function that looks for and removes the isNZLoop HPVM intrinsic
void hpvm::removeNZLoop(Function *F) {
  std::vector<IntrinsicInst *> IItoRemove;
  Function *NZLoop =
      Intrinsic::getDeclaration(F->getParent(), Intrinsic::hpvm_nz_loop);
  for (auto *User : NZLoop->users()) {
    IntrinsicInst *II = dyn_cast<IntrinsicInst>(User);
    if (II->getParent()->getParent() != F)
      continue;
    assert(II->getIntrinsicID() == Intrinsic::hpvm_nz_loop &&
           "Unhandled intrinsic!");
    LLVM_DEBUG(errs() << "Removing Intrinsic: " << *II << "\n");
    IItoRemove.push_back(II);
  }
  for (std::vector<IntrinsicInst *>::reverse_iterator ri = IItoRemove.rbegin(),
                                                      re = IItoRemove.rend();
       ri != re; ++ri)
    (*ri)->eraseFromParent();
}

// Function that looks for and removes the loop guard branch by using the
// NZLoop HPVM intrinsic
void hpvm::removeFuncLoopGuardBranches(Function *F, bool KeepI) {
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  std::vector<IntrinsicInst *> IItoRemove;
  Function *NZLoop =
      Intrinsic::getDeclaration(F->getParent(), Intrinsic::hpvm_nz_loop);
  for (auto *User : NZLoop->users()) {
    IntrinsicInst *II = dyn_cast<IntrinsicInst>(User);
    if (II->getParent()->getParent() != F)
      continue;
    PHINode *IndVar = dyn_cast<PHINode>(II->getArgOperand(0));
    assert(IndVar != nullptr && "Couldn't get PHINode from NZLoop!");
    // The PHINode correspondiong to the induction variable will be in the loop
    // header
    BasicBlock *LHeader = IndVar->getParent();
    Loop *L = LI.getLoopFor(LHeader);
    // Make sure the intrinsic PHI Node operand is the Induction Variable
    PHINode *IV = L->getInductionVariable(SE);
    assert(IV && "couldn't get induction variable!");
    assert(IndVar == IV &&
           "Intrinsic Operand is not the loop induction variable!");
    // Find the loop guard branch and remove it if found
    BranchInst *BI = getLoopGuardBranch(L, &SE);
    if (BI) {
      LLVM_DEBUG(errs() << "Guard Branch: " << *BI << "\n");
      hpvm::removeLoopGuardBranch(L, BI);
    } else {
      LLVM_DEBUG(errs() << "We couldn't remove the loop guard branch, just remove "
                      "the intrinsic");
    }
    assert(isa<ConstantInt>(II->getOperand(1)) &&
           "Expecting second operand of NZ Loop Intrinsic to be an int!");
    if (!KeepI)
      IItoRemove.push_back(II);
  }

  for (std::vector<IntrinsicInst *>::reverse_iterator ri = IItoRemove.rbegin(),
                                                      re = IItoRemove.rend();
       ri != re; ++ri)
    (*ri)->eraseFromParent();
}

// Look for and update the isNZLoop intrinsic after unrolling
void updateNZLoop(Loop *L, LoopInfo &LI, ScalarEvolution &SE, Function *F,
                  unsigned UF) {

  std::vector<IntrinsicInst *> IItoRemove;

  // The loop has not been fully unrolled, find and update the NZLoop
  // intrinsic
  PHINode *IndVar = L->getInductionVariable(SE);
  bool found = false;
  for (auto *User : IndVar->users()) {
    if (IntrinsicInst *II = dyn_cast<IntrinsicInst>(User)) {
      if (II->getIntrinsicID() != Intrinsic::hpvm_nz_loop) {
        continue;
      }
      if (found) {
        // This is the same copy of NZLoop, most likely a result of inner-more
        // loops getting unrolled. Delete it!
        IItoRemove.push_back(II);
        continue;
      }
      found = true;
      auto TripCount =
          dyn_cast<ConstantInt>(II->getOperand(1))->getZExtValue();
      auto NewTripCount = TripCount / UF;
      LLVM_DEBUG(errs() << "Updating TripCount in Intrinsic: " << TripCount
                    << " -> " << NewTripCount << "\n");
      ConstantInt *NTC =
          ConstantInt::get(Type::getInt32Ty(F->getContext()), NewTripCount);
      II->setOperand(1, NTC);
      LLVM_DEBUG(errs() << "Updated Intrinsic: " << *NTC << "\n");
    }
  }
  assert(found && "Expecting to have found an NZLoop for this loop!");

  for (std::vector<IntrinsicInst *>::reverse_iterator ri = IItoRemove.rbegin(),
                                                      re = IItoRemove.rend();
       ri != re; ++ri)
    (*ri)->eraseFromParent();
}
/// Patched version of llvm::UnrollLoop which also fixes the external users of
/// all the Values in the original body of the unrolled loop.
static bool needToInsertPhisForLCSSA(Loop *L, std::vector<BasicBlock *> Blocks,
                                     LoopInfo *LI) {
  for (BasicBlock *BB : Blocks) {
    if (LI->getLoopFor(BB) == L)
      continue;
    for (Instruction &I : *BB) {
      for (Use &U : I.operands()) {
        if (auto Def = dyn_cast<Instruction>(U)) {
          Loop *DefLoop = LI->getLoopFor(Def->getParent());
          if (!DefLoop)
            continue;
          if (DefLoop->contains(L))
            return true;
        }
      }
    }
  }
  return false;
}
static bool isEpilogProfitable(Loop *L) {
  BasicBlock *PreHeader = L->getLoopPreheader();
  BasicBlock *Header = L->getHeader();
  assert(PreHeader && Header);
  for (const PHINode &PN : Header->phis()) {
    if (isa<ConstantInt>(PN.getIncomingValueForBlock(PreHeader)))
      return true;
  }
  return false;
}

/*
 *  API changes to LLVM UnrollLoop found so far:
 *  - Renamed fields: AllowRuntime -> Runtime
 *  - TripCount, TripMultiple are computed within the function
 *  - Arguments after are not used by hpvm-dse or hpvm-fpga
 *  but are gone, except AllowExpensiveTripCount, UnrollRemainder, ForgetAllSCEV
 */
 
LoopUnrollResult hpvmUnrollLoop(Loop *L, UnrollLoopOptions ULO, LoopInfo *LI,
                                ScalarEvolution *SE, DominatorTree *DT,
                                AssumptionCache *AC,
                                OptimizationRemarkEmitter *ORE,
                                const TargetTransformInfo *TTI,
                                unsigned TripCount, unsigned TripMultiple,
                                bool PreserveLCSSA,
                                Loop **RemainderLoop = nullptr) {

  // Comes from old default parameters
  bool PreserveCondBr = false;
  bool PreserveOnlyFirst = false;
  unsigned PeelCount = 0;

  BasicBlock *Preheader = L->getLoopPreheader();
  if (!Preheader) {
    LLVM_DEBUG(dbgs() << "  Can't unroll; loop preheader-insertion failed.\n");
    return LoopUnrollResult::Unmodified;
  }

  BasicBlock *LatchBlock = L->getLoopLatch();
  if (!LatchBlock) {
    LLVM_DEBUG(dbgs() << "  Can't unroll; loop exit-block-insertion failed.\n");
    return LoopUnrollResult::Unmodified;
  }

  // Loops with indirectbr cannot be cloned.
  if (!L->isSafeToClone()) {
    LLVM_DEBUG(dbgs() << "  Can't unroll; Loop body cannot be cloned.\n");
    return LoopUnrollResult::Unmodified;
  }

  // The current loop unroll pass can unroll loops with a single latch or header
  // that's a conditional branch exiting the loop.
  // FIXME: The implementation can be extended to work with more complicated
  // cases, e.g. loops with multiple latches.
  BasicBlock *Header = L->getHeader();
  BranchInst *HeaderBI = dyn_cast<BranchInst>(Header->getTerminator());
  BranchInst *BI = dyn_cast<BranchInst>(LatchBlock->getTerminator());

  // FIXME: Support loops without conditional latch and multiple exiting blocks.
  if (!BI ||
      (BI->isUnconditional() && (!HeaderBI || HeaderBI->isUnconditional() ||
                                 L->getExitingBlock() != Header))) {
    LLVM_DEBUG(dbgs() << "  Can't unroll; loop not terminated by a conditional "
                         "branch in the latch or header.\n");
    return LoopUnrollResult::Unmodified;
  }

  auto CheckLatchSuccessors = [&](unsigned S1, unsigned S2) {
    return BI->isConditional() && BI->getSuccessor(S1) == Header &&
           !L->contains(BI->getSuccessor(S2));
  };

  // If we have a conditional latch, it must exit the loop.
  if (BI && BI->isConditional() && !CheckLatchSuccessors(0, 1) &&
      !CheckLatchSuccessors(1, 0)) {
    LLVM_DEBUG(
        dbgs() << "Can't unroll; a conditional latch must exit the loop");
    return LoopUnrollResult::Unmodified;
  }

  auto CheckHeaderSuccessors = [&](unsigned S1, unsigned S2) {
    return HeaderBI && HeaderBI->isConditional() &&
           L->contains(HeaderBI->getSuccessor(S1)) &&
           !L->contains(HeaderBI->getSuccessor(S2));
  };

  // If we do not have a conditional latch, the header must exit the loop.
  if (BI && !BI->isConditional() && HeaderBI && HeaderBI->isConditional() &&
      !CheckHeaderSuccessors(0, 1) && !CheckHeaderSuccessors(1, 0)) {
    LLVM_DEBUG(dbgs() << "Can't unroll; conditional header must exit the loop");
    return LoopUnrollResult::Unmodified;
  }

  if (Header->hasAddressTaken()) {
    // The loop-rotate pass can be helpful to avoid this in many cases.
    LLVM_DEBUG(
        dbgs() << "  Won't unroll loop: address of header block is taken.\n");
    return LoopUnrollResult::Unmodified;
  }

  if (TripCount != 0)
    LLVM_DEBUG(dbgs() << "  Trip Count = " << TripCount << "\n");
  if (TripMultiple != 1)
    LLVM_DEBUG(dbgs() << "  Trip Multiple = " << TripMultiple << "\n");

  // Effectively "DCE" unrolled iterations that are beyond the tripcount
  // and will never be executed.
  if (TripCount != 0 && ULO.Count > TripCount)
    ULO.Count = TripCount;

  // Don't enter the unroll code if there is nothing to do.
  if (TripCount == 0 && ULO.Count < 2 && PeelCount == 0) {
    LLVM_DEBUG(dbgs() << "Won't unroll; almost nothing to do\n");
    return LoopUnrollResult::Unmodified;
  }

  assert(ULO.Count > 0);
  assert(TripMultiple > 0);
  assert(TripCount == 0 || TripCount % TripMultiple == 0);

  // Are we eliminating the loop control altogether?
  bool CompletelyUnroll = ULO.Count == TripCount;
  SmallVector<BasicBlock *, 4> ExitBlocks;
  L->getExitBlocks(ExitBlocks);
  std::vector<BasicBlock *> OriginalLoopBlocks = L->getBlocks();

  // Go through all exits of L and see if there are any phi-nodes there. We just
  // conservatively assume that they're inserted to preserve LCSSA form, which
  // means that complete unrolling might break this form. We need to either fix
  // it in-place after the transformation, or entirely rebuild LCSSA. TODO: For
  // now we just recompute LCSSA for the outer loop, but it should be possible
  // to fix it in-place.
  bool NeedToFixLCSSA = PreserveLCSSA && CompletelyUnroll &&
                        any_of(ExitBlocks, [](const BasicBlock *BB) {
                          return isa<PHINode>(BB->begin());
                        });

  // We assume a run-time trip count if the compiler cannot
  // figure out the loop trip count and the unroll-runtime
  // flag is specified.
  bool RuntimeTripCount =
      (TripCount == 0 && ULO.Count > 0 && ULO.Runtime);

  assert((!RuntimeTripCount || !PeelCount) &&
         "Did not expect runtime trip-count unrolling "
         "and peeling for the same loop");

  bool Peeled = false;
  assert(PeelCount == 0);

  /*  
  if (PeelCount) {
    Peeled = peelLoop(L, PeelCount, LI, SE, DT, AC, PreserveLCSSA);

    // Successful peeling may result in a change in the loop preheader/trip
    // counts. If we later unroll the loop, we want these to be updated.
    if (Peeled) {
      // According to our guards and profitability checks the only
      // meaningful exit should be latch block. Other exits go to deopt,
      // so we do not worry about them.
      BasicBlock *ExitingBlock = L->getLoopLatch();
      assert(ExitingBlock && "Loop without exiting block?");
      assert(L->isLoopExiting(ExitingBlock) && "Latch is not exiting?");
      Preheader = L->getLoopPreheader();
      TripCount = SE->getSmallConstantTripCount(L, ExitingBlock);
      TripMultiple = SE->getSmallConstantTripMultiple(L, ExitingBlock);
    }
  }
  */

  // Loops containing convergent instructions must have a count that divides
  // their TripMultiple.
  LLVM_DEBUG({
    bool HasConvergent = false;
    for (auto &BB : L->blocks())
      for (auto &I : *BB)
        if (auto *Call = dyn_cast<CallBase>(&I))
          HasConvergent |= Call->isConvergent();
    assert((!HasConvergent || TripMultiple % ULO.Count == 0) &&
           "Unroll count must divide trip multiple if loop contains a "
           "convergent operation.");
  });

  bool EpilogProfitability = isEpilogProfitable(L);

  if (RuntimeTripCount && TripMultiple % ULO.Count != 0 &&
      !UnrollRuntimeLoopRemainder(L, ULO.Count, ULO.AllowExpensiveTripCount,
                                  EpilogProfitability, ULO.UnrollRemainder,
                                  ULO.ForgetAllSCEV, LI, SE, DT, AC,
                                  TTI, PreserveLCSSA, RemainderLoop)) {
    if (ULO.Force)
      RuntimeTripCount = false;
    else {
      LLVM_DEBUG(dbgs() << "Won't unroll; remainder loop could not be "
                           "generated when assuming runtime trip count\n");
      return LoopUnrollResult::Unmodified;
    }
  }

  // If we know the trip count, we know the multiple...
  unsigned BreakoutTrip = 0;
  if (TripCount != 0) {
    BreakoutTrip = TripCount % ULO.Count;
    TripMultiple = 0;
  } else {
    // Figure out what multiple to use.
    BreakoutTrip = TripMultiple =
        (unsigned)GreatestCommonDivisor64(ULO.Count, TripMultiple);
  }

  using namespace ore;
  // Report the unrolling decision.
  if (CompletelyUnroll) {
    LLVM_DEBUG(dbgs() << "COMPLETELY UNROLLING loop %" << Header->getName()
                      << " with trip count " << TripCount << "!\n");
    if (ORE)
      ORE->emit([&]() {
        return OptimizationRemark(DEBUG_TYPE, "FullyUnrolled", L->getStartLoc(),
                                  L->getHeader())
               << "completely unrolled loop with "
               << NV("UnrollCount", TripCount) << " iterations";
      });
  } else if (PeelCount) {
    LLVM_DEBUG(dbgs() << "PEELING loop %" << Header->getName()
                      << " with iteration count " << PeelCount << "!\n");
    if (ORE)
      ORE->emit([&]() {
        return OptimizationRemark(DEBUG_TYPE, "Peeled", L->getStartLoc(),
                                  L->getHeader())
               << " peeled loop by " << NV("PeelCount", PeelCount)
               << " iterations";
      });
  } else {
    auto DiagBuilder = [&]() {
      OptimizationRemark Diag(DEBUG_TYPE, "PartialUnrolled", L->getStartLoc(),
                              L->getHeader());
      return Diag << "unrolled loop by a factor of "
                  << NV("UnrollCount", ULO.Count);
    };

    LLVM_DEBUG(dbgs() << "UNROLLING loop %" << Header->getName() << " by "
                      << ULO.Count);
    if (TripMultiple == 0 || BreakoutTrip != TripMultiple) {
      LLVM_DEBUG(dbgs() << " with a breakout at trip " << BreakoutTrip);
      if (ORE)
        ORE->emit([&]() {
          return DiagBuilder() << " with a breakout at trip "
                               << NV("BreakoutTrip", BreakoutTrip);
        });
    } else if (TripMultiple != 1) {
      LLVM_DEBUG(dbgs() << " with " << TripMultiple << " trips per branch");
      if (ORE)
        ORE->emit([&]() {
          return DiagBuilder()
                 << " with " << NV("TripMultiple", TripMultiple)
                 << " trips per branch";
        });
    } else if (RuntimeTripCount) {
      LLVM_DEBUG(dbgs() << " with run-time trip count");
      if (ORE)
        ORE->emit(
            [&]() { return DiagBuilder() << " with run-time trip count"; });
    }
    LLVM_DEBUG(dbgs() << "!\n");
  }

  // We are going to make changes to this loop. SCEV may be keeping cached info
  // about it, in particular about backedge taken count. The changes we make
  // are guaranteed to invalidate this information for our loop. It is tempting
  // to only invalidate the loop being unrolled, but it is incorrect as long as
  // all exiting branches from all inner loops have impact on the outer loops,
  // and if something changes inside them then any of outer loops may also
  // change. When we forget outermost loop, we also forget all contained loops
  // and this is what we need here.
  if (SE) {
    if (ULO.ForgetAllSCEV)
      SE->forgetAllLoops();
    else
      SE->forgetTopmostLoop(L);
  }

  bool ContinueOnTrue;
  bool LatchIsExiting = BI->isConditional();
  BasicBlock *LoopExit = nullptr;
  if (LatchIsExiting) {
    ContinueOnTrue = L->contains(BI->getSuccessor(0));
    LoopExit = BI->getSuccessor(ContinueOnTrue);
  } else {
    ContinueOnTrue = L->contains(HeaderBI->getSuccessor(0));
    LoopExit = HeaderBI->getSuccessor(ContinueOnTrue);
  }

  // For the first iteration of the loop, we should use the precloned values for
  // PHI nodes.  Insert associations now.
  ValueToValueMapTy LastValueMap;
  std::vector<PHINode *> OrigPHINode;
  for (BasicBlock::iterator I = Header->begin(); isa<PHINode>(I); ++I) {
    OrigPHINode.push_back(cast<PHINode>(I));
  }

  // Keep track of the relevant IIs (ivdep and NZLoop) to remove their copies
  PHINode *IV = L->getInductionVariable(*SE);
  std::vector<IntrinsicInst *> OrigIIs;
  for (BasicBlock::iterator I = Header->begin(); I != Header->end(); ++I) {
    if (IntrinsicInst *II = dyn_cast<IntrinsicInst>(I)) {
      if (II->getIntrinsicID() == Intrinsic::hpvm_ivdep) {
        OrigIIs.push_back(II);
      } else if (II->getIntrinsicID() == Intrinsic::hpvm_nz_loop) {
        if (II->getArgOperand(0) == IV)
          OrigIIs.push_back(II);
      }
    }
  }

  // If completely unrolling, remove the IIs since they won't be needed anymore.
  if (CompletelyUnroll) {
    for (auto *II : OrigIIs) {
      II->eraseFromParent();
    }
    OrigIIs.clear();
  }

  std::vector<BasicBlock *> Headers;
  std::vector<BasicBlock *> HeaderSucc;
  std::vector<BasicBlock *> Latches;
  Headers.push_back(Header);
  Latches.push_back(LatchBlock);

  if (!LatchIsExiting) {
    auto *Term = cast<BranchInst>(Header->getTerminator());
    if (Term->isUnconditional() || L->contains(Term->getSuccessor(0))) {
      assert(L->contains(Term->getSuccessor(0)));
      HeaderSucc.push_back(Term->getSuccessor(0));
    } else {
      assert(L->contains(Term->getSuccessor(1)));
      HeaderSucc.push_back(Term->getSuccessor(1));
    }
  }

  // The current on-the-fly SSA update requires blocks to be processed in
  // reverse postorder so that LastValueMap contains the correct value at each
  // exit.
  LoopBlocksDFS DFS(L);
  DFS.perform(LI);

  // Stash the DFS iterators before adding blocks to the loop.
  LoopBlocksDFS::RPOIterator BlockBegin = DFS.beginRPO();
  LoopBlocksDFS::RPOIterator BlockEnd = DFS.endRPO();

  std::vector<BasicBlock *> UnrolledLoopBlocks = L->getBlocks();

  // Loop Unrolling might create new loops. While we do preserve LoopInfo, we
  // might break loop-simplified form for these loops (as they, e.g., would
  // share the same exit blocks). We'll keep track of loops for which we can
  // break this so that later we can re-simplify them.
  SmallSetVector<Loop *, 4> LoopsToSimplify;
  for (Loop *SubLoop : *L)
    LoopsToSimplify.insert(SubLoop);

  if (Header->getParent()->isDebugInfoForProfiling())
    for (BasicBlock *BB : L->getBlocks())
      for (Instruction &I : *BB)
        if (!isa<DbgInfoIntrinsic>(&I))
          if (const DILocation *DIL = I.getDebugLoc()) {
            auto NewDIL = DIL->cloneByMultiplyingDuplicationFactor(ULO.Count);
            if (NewDIL)
              I.setDebugLoc(NewDIL.getValue());
            else
              LLVM_DEBUG(dbgs()
                         << "Failed to create new discriminator: "
                         << DIL->getFilename() << " Line: " << DIL->getLine());
          }

  for (unsigned It = 1; It != ULO.Count; ++It) {
    SmallVector<BasicBlock *, 8> NewBlocks;
    SmallDenseMap<const Loop *, Loop *, 4> NewLoops;
    NewLoops[L] = L;

    for (LoopBlocksDFS::RPOIterator BB = BlockBegin; BB != BlockEnd; ++BB) {
      ValueToValueMapTy VMap;
      BasicBlock *New = CloneBasicBlock(*BB, VMap, "." + Twine(It));
      Header->getParent()->getBasicBlockList().push_back(New);

      assert((*BB != Header || LI->getLoopFor(*BB) == L) &&
             "Header should not be in a sub-loop");
      // Tell LI about New.
      const Loop *OldLoop = addClonedBlockToLoopInfo(*BB, New, LI, NewLoops);
      if (OldLoop)
        LoopsToSimplify.insert(NewLoops[OldLoop]);

      if (*BB == Header) {
        // Loop over all of the PHI nodes in the block, changing them to use
        // the incoming values from the previous block.
        for (PHINode *OrigPHI : OrigPHINode) {
          PHINode *NewPHI = cast<PHINode>(VMap[OrigPHI]);
          Value *InVal = NewPHI->getIncomingValueForBlock(LatchBlock);
          if (Instruction *InValI = dyn_cast<Instruction>(InVal))
            if (It > 1 && L->contains(InValI))
              InVal = LastValueMap[InValI];
          VMap[OrigPHI] = InVal;
          New->getInstList().erase(NewPHI);
        }

        for (IntrinsicInst *II : OrigIIs) {
          IntrinsicInst *NewII = cast<IntrinsicInst>(VMap[II]);
          NewII->eraseFromParent();
        }
      }
      // Update our running map of newest clones
      LastValueMap[*BB] = New;
      for (ValueToValueMapTy::iterator VI = VMap.begin(), VE = VMap.end();
           VI != VE; ++VI)
        LastValueMap[VI->first] = VI->second;

      // Add phi entries for newly created values to all exit blocks.
      for (BasicBlock *Succ : successors(*BB)) {
        if (L->contains(Succ))
          continue;
        for (PHINode &PHI : Succ->phis()) {
          Value *Incoming = PHI.getIncomingValueForBlock(*BB);
          ValueToValueMapTy::iterator It = LastValueMap.find(Incoming);
          if (It != LastValueMap.end())
            Incoming = It->second;
          PHI.addIncoming(Incoming, New);
        }
      }
      // Keep track of new headers and latches as we create them, so that
      // we can insert the proper branches later.
      if (*BB == Header)
        Headers.push_back(New);
      if (*BB == LatchBlock)
        Latches.push_back(New);

      // Keep track of the successor of the new header in the current iteration.
      for (auto *Pred : predecessors(*BB))
        if (Pred == Header) {
          HeaderSucc.push_back(New);
          break;
        }

      NewBlocks.push_back(New);
      UnrolledLoopBlocks.push_back(New);

      // Update DomTree: since we just copy the loop body, and each copy has a
      // dedicated entry block (copy of the header block), this header's copy
      // dominates all copied blocks. That means, dominance relations in the
      // copied body are the same as in the original body.
      if (DT) {
        if (*BB == Header)
          DT->addNewBlock(New, Latches[It - 1]);
        else {
          auto BBDomNode = DT->getNode(*BB);
          auto BBIDom = BBDomNode->getIDom();
          BasicBlock *OriginalBBIDom = BBIDom->getBlock();
          DT->addNewBlock(
              New, cast<BasicBlock>(LastValueMap[cast<Value>(OriginalBBIDom)]));
        }
      }
    }

    // Remap all instructions in the most recent iteration
    remapInstructionsInBlocks(NewBlocks, LastValueMap);
    for (BasicBlock *NewBlock : NewBlocks) {
      for (Instruction &I : *NewBlock) {
        if (auto *AI = dyn_cast<AssumeInst>(&I))
          AC->registerAssumption(AI);
      }
    }
  }

  // Loop over the PHI nodes in the original block, setting incoming values.
  for (PHINode *PN : OrigPHINode) {
    if (CompletelyUnroll) {
      PN->replaceAllUsesWith(PN->getIncomingValueForBlock(Preheader));
      Header->getInstList().erase(PN);
    } else if (ULO.Count > 1) {
      Value *InVal = PN->removeIncomingValue(LatchBlock, false);
      // If this value was defined in the loop, take the value defined by the
      // last iteration of the loop.
      if (Instruction *InValI = dyn_cast<Instruction>(InVal)) {
        if (L->contains(InValI))
          InVal = LastValueMap[InVal];
      }
      assert(Latches.back() == LastValueMap[LatchBlock] && "bad last latch");
      PN->addIncoming(InVal, Latches.back());
    }
  }

  auto setDest = [LoopExit, ContinueOnTrue](BasicBlock *Src, BasicBlock *Dest,
                                            ArrayRef<BasicBlock *> NextBlocks,
                                            BasicBlock *BlockInLoop,
                                            bool NeedConditional) {
    auto *Term = cast<BranchInst>(Src->getTerminator());
    if (NeedConditional) {
      // Update the conditional branch's successor for the following
      // iteration.
      Term->setSuccessor(!ContinueOnTrue, Dest);
    } else {
      // Remove phi operands at this loop exit
      if (Dest != LoopExit) {
        BasicBlock *BB = Src;
        for (BasicBlock *Succ : successors(BB)) {
          // Preserve the incoming value from BB if we are jumping to the block
          // in the current loop.
          if (Succ == BlockInLoop)
            continue;
          for (PHINode &Phi : Succ->phis())
            Phi.removeIncomingValue(BB, false);
        }
      }
      // Replace the conditional branch with an unconditional one.
      BranchInst::Create(Dest, Term);
      Term->eraseFromParent();
    }
  };

  // Now that all the basic blocks for the unrolled iterations are in place,
  // set up the branches to connect them.
  if (LatchIsExiting) {
    // Set up latches to branch to the new header in the unrolled iterations or
    // the loop exit for the last latch in a fully unrolled loop.
    for (unsigned i = 0, e = Latches.size(); i != e; ++i) {
      // The branch destination.
      unsigned j = (i + 1) % e;
      BasicBlock *Dest = Headers[j];
      bool NeedConditional = true;

      if (RuntimeTripCount && j != 0) {
        NeedConditional = false;
      }

      // For a complete unroll, make the last iteration end with a branch
      // to the exit block.
      if (CompletelyUnroll) {
        if (j == 0)
          Dest = LoopExit;
        // If using trip count upper bound to completely unroll, we need to keep
        // the conditional branch except the last one because the loop may exit
        // after any iteration.
        assert(NeedConditional &&
               "NeedCondition cannot be modified by both complete "
               "unrolling and runtime unrolling");
        NeedConditional =
            (PreserveCondBr && j && !(PreserveOnlyFirst && i != 0));
      } else if (j != BreakoutTrip &&
                 (TripMultiple == 0 || j % TripMultiple != 0)) {
        // If we know the trip count or a multiple of it, we can safely use an
        // unconditional branch for some iterations.
        NeedConditional = false;
      }

      setDest(Latches[i], Dest, Headers, Headers[i], NeedConditional);
    }
  } else {
    // Setup headers to branch to their new successors in the unrolled
    // iterations.
    for (unsigned i = 0, e = Headers.size(); i != e; ++i) {
      // The branch destination.
      unsigned j = (i + 1) % e;
      BasicBlock *Dest = HeaderSucc[i];
      bool NeedConditional = true;

      if (RuntimeTripCount && j != 0)
        NeedConditional = false;

      if (CompletelyUnroll)
        // We cannot drop the conditional branch for the last condition, as we
        // may have to execute the loop body depending on the condition.
        NeedConditional = j == 0 || PreserveCondBr;
      else if (j != BreakoutTrip &&
               (TripMultiple == 0 || j % TripMultiple != 0))
        // If we know the trip count or a multiple of it, we can safely use an
        // unconditional branch for some iterations.
        NeedConditional = false;

      setDest(Headers[i], Dest, Headers, HeaderSucc[i], NeedConditional);
    }

    // Set up latches to branch to the new header in the unrolled iterations or
    // the loop exit for the last latch in a fully unrolled loop.

    for (unsigned i = 0, e = Latches.size(); i != e; ++i) {
      // The original branch was replicated in each unrolled iteration.
      BranchInst *Term = cast<BranchInst>(Latches[i]->getTerminator());

      // The branch destination.
      unsigned j = (i + 1) % e;
      BasicBlock *Dest = Headers[j];

      // When completely unrolling, the last latch becomes unreachable.
      if (CompletelyUnroll && j == 0)
        new UnreachableInst(Term->getContext(), Term);
      else
        // Replace the conditional branch with an unconditional one.
        BranchInst::Create(Dest, Term);

      Term->eraseFromParent();
    }
  }

  // Fix any external users to use last value.
  std::vector<std::pair<Instruction *, const Value *>> UsersToFix;
  for (auto LV : LastValueMap) {
    if (auto *I = dyn_cast<Instruction>(LV.first)) {
      for (auto *UC : I->users()) {
        User *U = const_cast<User *>(UC);
        if (auto *IU = dyn_cast<Instruction>(U)) {
          if (!L->contains(IU)) {
            // TODO: Should we make sure I dominates IU?
            LLVM_DEBUG(dbgs() << "Found a user outside the loop:\n  I: " << *I
                         << "\n  User: " << *IU << "\n");
            UsersToFix.push_back(std::make_pair(IU, LV.first));
          }
        }
      }
    }
  }

  for (auto UF : UsersToFix) {
    Instruction *IU = UF.first;
    const Value *Val = UF.second;
    Value *NewVal = LastValueMap[Val];
    LLVM_DEBUG(errs() << "Processing pair:\n  I: " << *Val << "\n  U: " << *IU
                 << "\n");
    for (unsigned op = 0, E = IU->getNumOperands(); op != E; ++op) {
      Value *Op = IU->getOperand(op);
      if (Op == Val) {
        IU->setOperand(op, NewVal);
        LLVM_DEBUG(dbgs() << "---- New user: " << *IU << "\n");
      }
    }
  }
  // Update dominators of blocks we might reach through exits.
  // Immediate dominator of such block might change, because we add more
  // routes which can lead to the exit: we can now reach it from the copied
  // iterations too.
  if (DT && ULO.Count > 1) {
    for (auto *BB : OriginalLoopBlocks) {
      auto *BBDomNode = DT->getNode(BB);
      SmallVector<BasicBlock *, 16> ChildrenToUpdate;
      for (auto *ChildDomNode : BBDomNode->children()) {
        auto *ChildBB = ChildDomNode->getBlock();
        if (!L->contains(ChildBB))
          ChildrenToUpdate.push_back(ChildBB);
      }
      BasicBlock *NewIDom;
      BasicBlock *&TermBlock = LatchIsExiting ? LatchBlock : Header;
      auto &TermBlocks = LatchIsExiting ? Latches : Headers;
      if (BB == TermBlock) {
        // The latch is special because we emit unconditional branches in
        // some cases where the original loop contained a conditional branch.
        // Since the latch is always at the bottom of the loop, if the latch
        // dominated an exit before unrolling, the new dominator of that exit
        // must also be a latch.  Specifically, the dominator is the first
        // latch which ends in a conditional branch, or the last latch if
        // there is no such latch.
        // For loops exiting from the header, we limit the supported loops
        // to have a single exiting block.
        NewIDom = TermBlocks.back();
        for (BasicBlock *Iter : TermBlocks) {
          Instruction *Term = Iter->getTerminator();
          if (isa<BranchInst>(Term) &&
              cast<BranchInst>(Term)->isConditional()) {
            NewIDom = Iter;
            break;
          }
        }
      } else {
        // The new idom of the block will be the nearest common dominator
        // of all copies of the previous idom. This is equivalent to the
        // nearest common dominator of the previous idom and the first latch,
        // which dominates all copies of the previous idom.
        NewIDom = DT->findNearestCommonDominator(BB, LatchBlock);
      }
      for (auto *ChildBB : ChildrenToUpdate)
        DT->changeImmediateDominator(ChildBB, NewIDom);
    }
  }

  assert(!DT || DT->verify(DominatorTree::VerificationLevel::Fast));

  DomTreeUpdater DTU(DT, DomTreeUpdater::UpdateStrategy::Eager);
  // Merge adjacent basic blocks, if possible.
  for (BasicBlock *Latch : Latches) {
    BranchInst *Term = dyn_cast<BranchInst>(Latch->getTerminator());
    assert((Term ||
            (CompletelyUnroll && !LatchIsExiting && Latch == Latches.back())) &&
           "Need a branch as terminator, except when fully unrolling with "
           "unconditional latch");
    if (Term && Term->isUnconditional()) {
      BasicBlock *Dest = Term->getSuccessor(0);
      BasicBlock *Fold = Dest->getUniquePredecessor();
      if (MergeBlockIntoPredecessor(Dest, &DTU, LI)) {
        // Dest has been folded into Fold. Update our worklists accordingly.
        std::replace(Latches.begin(), Latches.end(), Dest, Fold);
        UnrolledLoopBlocks.erase(std::remove(UnrolledLoopBlocks.begin(),
                                             UnrolledLoopBlocks.end(), Dest),
                                 UnrolledLoopBlocks.end());
      }
    }
  }

  // At this point, the code is well formed.  We now simplify the unrolled loop,
  // doing constant propagation and dead code elimination as we go.
  simplifyLoopAfterUnroll(L, !CompletelyUnroll && (ULO.Count > 1 || Peeled), LI,
                          SE, DT, AC, TTI);

  Loop *OuterL = L->getParentLoop();
  // Update LoopInfo if the loop is completely removed.
  if (CompletelyUnroll)
    LI->erase(L);

  // After complete unrolling most of the blocks should be contained in OuterL.
  // However, some of them might happen to be out of OuterL (e.g. if they
  // precede a loop exit). In this case we might need to insert PHI nodes in
  // order to preserve LCSSA form.
  // We don't need to check this if we already know that we need to fix LCSSA
  // form.
  // TODO: For now we just recompute LCSSA for the outer loop in this case, but
  // it should be possible to fix it in-place.
  if (PreserveLCSSA && OuterL && CompletelyUnroll && !NeedToFixLCSSA)
    NeedToFixLCSSA |=
        ::needToInsertPhisForLCSSA(OuterL, UnrolledLoopBlocks, LI);

  // If we have a pass and a DominatorTree we should re-simplify impacted loops
  // to ensure subsequent analyses can rely on this form. We want to simplify
  // at least one layer outside of the loop that was unrolled so that any
  // changes to the parent loop exposed by the unrolling are considered.
  if (DT) {
    if (OuterL) {
      // OuterL includes all loops for which we can break loop-simplify, so
      // it's sufficient to simplify only it (it'll recursively simplify inner
      // loops too).
      if (NeedToFixLCSSA) {
        // LCSSA must be performed on the outermost affected loop. The unrolled
        // loop's last loop latch is guaranteed to be in the outermost loop
        // after LoopInfo's been updated by LoopInfo::erase.
        Loop *LatchLoop = LI->getLoopFor(Latches.back());
        Loop *FixLCSSALoop = OuterL;
        if (!FixLCSSALoop->contains(LatchLoop))
          while (FixLCSSALoop->getParentLoop() != LatchLoop)
            FixLCSSALoop = FixLCSSALoop->getParentLoop();

        formLCSSARecursively(*FixLCSSALoop, *DT, LI, SE);
      } else if (PreserveLCSSA) {
        assert(OuterL->isLCSSAForm(*DT) &&
               "Loops should be in LCSSA form after loop-unroll.");
      }

      // TODO: That potentially might be compile-time expensive. We should try
      // to fix the loop-simplified form incrementally.
      simplifyLoop(OuterL, DT, LI, SE, AC, nullptr, PreserveLCSSA);
    } else {
      // Simplify loops for which we might've broken loop-simplify form.
      for (Loop *SubLoop : LoopsToSimplify)
        simplifyLoop(SubLoop, DT, LI, SE, AC, nullptr, PreserveLCSSA);
    }
  }

  return CompletelyUnroll ? LoopUnrollResult::FullyUnrolled
                          : LoopUnrollResult::PartiallyUnrolled;
}
// Unrolls a loop by tripcount
void hpvm::unrollLoopByCount(Loop *L, LoopInfo &LI, DominatorTree &DT,
                             ScalarEvolution &SE, AssumptionCache &AC,
                             const TargetTransformInfo &TTI,
                             unsigned _TripCount, unsigned _Count,
                             unsigned _TripMultiple, bool _AllowRuntime,
                             bool _AllowExpensiveTripCount, bool _Force,
                             bool _UnrollRemainder, bool _ForgetAllSCEV,
                             bool _PreserveLCSSA) {

  Function *F = L->getHeader()->getParent();
  OptimizationRemarkEmitter ORE(F);

  // If the tripcount is 0 but count > 0 this means we are trying to unroll
  // a loop that has a variable trip count. We should enable runtime unroll.

  _AllowRuntime |= (_TripCount == 0 && _Count > 0);
  struct UnrollLoopOptions ULO;
  ULO.Count = _Count;
  ULO.Force = _Force;
  ULO.Runtime = _AllowRuntime;
  ULO.AllowExpensiveTripCount = _AllowExpensiveTripCount;
  ULO.UnrollRemainder = _UnrollRemainder;
  ULO.ForgetAllSCEV = _ForgetAllSCEV;

  if (_Count > 1) {
    if (_PreserveLCSSA)
      formLCSSA(*L, DT, &LI, &SE);

    LoopUnrollResult Result = hpvmUnrollLoop(L, ULO, &LI, &SE, &DT, &AC, &ORE, &TTI, _TripCount, _TripMultiple,
      _PreserveLCSSA);


    // These only needs to happen when the loop is not fully unrolled.
    if (Result == LoopUnrollResult::PartiallyUnrolled) {
      // Without this, SE performs no extra analysis and can't determine the
      // recurrence for the induction variable, needed in updateNZLoop
      SE.forgetLoop(L);

      if (_AllowRuntime && (_TripCount > 0))
        updateNZLoop(L, LI, SE, F, _Count);
    
    }
  }
}

// Find loops in loop nest with trip count < threshold
bool findLoops(Loop *L, ScalarEvolution *SE,
               std::map<Loop *, std::pair<unsigned, unsigned>> &candidates,
               unsigned uf, bool pu, bool ru, bool uo) {
  bool found = false;
  bool empty = L->getSubLoops().empty();
  unsigned tripCount = SE->getSmallConstantMaxTripCount(L);
  if (tripCount > 0 && tripCount <= uf && (empty || uo)) {
    candidates[L] = {tripCount, tripCount};
    found = true;
  } else if (tripCount > 0 && pu && (tripCount % uf == 0) && (empty || uo)) {
    candidates[L] = {tripCount, uf};
    found = true;
  } else if (tripCount == 0 && ru && (empty || uo)) {
    candidates[L] = {tripCount, uf};
    found = true;
  }
  if (!empty) {
    for (auto SLB = L->begin(); SLB != L->end(); SLB++) {
      Loop *SL = *SLB;
      //			LLVM_DEBUG(errs() << "Calling find loop on " << *SL
      //<< "\n");
      found |= findLoops(SL, SE, candidates, uf, pu, ru, uo);
    }
  }
  return found;
}

// Unrolls all the loops in a function
void hpvm::unrollFuncLoops(Function *F, unsigned uf, bool pu, bool ru,
                           bool uo) {
  FunctionAnalysisManager FAM;
  LoopAnalysisManager LAM;
  ModuleAnalysisManager MAM;
  CGSCCAnalysisManager CAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  PB.registerLoopAnalyses(LAM);
  PB.registerModuleAnalyses(MAM);
  PB.registerCGSCCAnalyses(CAM);
  PB.crossRegisterProxies(LAM, FAM, CAM, MAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  auto &TTI = FAM.getResult<TargetIRAnalysis>(*F);

  std::map<BasicBlock *, int> TripCountMap = getFuncLoopTripCounts(F, true);
  //  // Are we doing partial unroll?
  //  bool partialUnroll = uf > 0;
  std::map<Loop *, std::pair<unsigned, unsigned>> candidates;
  // For all the loop nests in the function
  for (auto LIB = LI.begin(); LIB != LI.end(); LIB++) {
    Loop *L = *LIB;
    LLVM_DEBUG(errs() << L << ": " << *L << "\n");

    // Go through all the loops in this loopnest to find a Loop that would be
    // candidate for unroll
    bool foundCandidate = findLoops(L, &SE, candidates, uf, pu, ru, uo);

    // Make sure all the loops are in loopSimplify form
    if (simplifyLoop(L, &DT, &LI, &SE, &AC, nullptr, /*true*/ false)) {
      LLVM_DEBUG(errs() << "Simplified loop!\n" << *L << "\n");
    }
  }

  // Fully unroll all loop candidates
  for (auto LC : candidates) {
    auto *L = LC.first;
    auto it = TripCountMap.find(L->getHeader());
    if (it != TripCountMap.end()) {
      auto TripMultiple = it->second;
      auto TripCount = it->second;
      auto AllowRuntime = true;
      auto UnrollFactor = uf > TripCount ? TripCount : uf;
      if (TripCount % UnrollFactor != 0) {
        LLVM_DEBUG(errs() << "Not unrolling this loop TripCount \% UF != 0\n");
        continue;
      }
      hpvm::unrollLoopByCount(L, LI, DT, SE, AC, TTI, TripCount, UnrollFactor,
                              TripMultiple, AllowRuntime);

    } else {
      auto TripCount = LC.second.first;
      auto UnrollFactor = LC.second.second;

      hpvm::unrollLoopByCount(L, LI, DT, SE, AC, TTI, TripCount, UnrollFactor);
    }
  }

  // Run earlycse, simplifycfg, loopsimplify
  FunctionPassManager FPM;
  FPM.addPass(InstCombinePass());
  FPM.addPass(EarlyCSEPass(false));
  FPM.addPass(SimplifyCFGPass());
  FPM.addPass(LoopSimplifyPass());
  FPM.run(*F, FAM);
}

/*************************************************************************/
/********************** Loop Fusion Methods ******************************/
/*************************************************************************/
void dumpDeps(std::vector<Dep> Deps) {
  for (auto Dep : Deps) {
    Dep.dump();
    errs() << "\n";
  }
}

void addDepToVector(Dependence *DIDep, Instruction *Src, Instruction *Sink,
                    Dep::Direction Dir, std::vector<Dep> &Deps) {
  Dep::DType DepType = DIDep->isOutput() ? Dep::Output
                       : DIDep->isFlow() ? Dep::Flow
                       : DIDep->isAnti() ? Dep::Anti
                                         : Dep::Unknown;
  bool isLoopIndependent = DIDep->isLoopIndependent();
  Dep _Dep(Src, Sink, isLoopIndependent, DepType, Dir);
  Deps.push_back(_Dep);
  LLVM_DEBUG(errs() << "---------------------------------------------\n";
        errs() << "Adding to Deps:\n"; _Dep.dump(); DIDep->dump(errs());
        errs() << "---------------------------------------------\n");
}

std::vector<Dep> getDeps(FusionCandidate *FC0, FusionCandidate *FC1) {
  LLVM_DEBUG(errs() << "Gathering Deps!\n");
  Function *F = FC0->L->getHeader()->getParent();

  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &DI = FAM.getResult<DependenceAnalysis>(*F);
  std::vector<Dep> Deps;
  auto L0MemOps = FC0->MemOps;
  auto L1MemOps = FC1->MemOps;
  for (auto *L0Inst : L0MemOps) {
    for (auto *L1Inst : L1MemOps) {
      NumMemOpsCompared++;
      if (auto DIDep = const_cast<DependenceInfo *>(&DI)->depends(
              L0Inst, L1Inst, true)) {
        if (!DIDep->isInput()) {
          // Legality check: Loop Independent Dependences from L0 to L1 cannot
          // become Loop carried in the oposite direction. Therefore, we only
          // care about LoopIndependent Dependences. Confused dependences should
          // also be marked conservatively.
          if (DIDep->isLoopIndependent()) {
            LLVM_DEBUG(errs() << "Comparing:\n"
                         << "\t" << *L0Inst << "\n\t" << *L1Inst << "\n");
            LLVM_DEBUG(errs() << "\tDepedence is loop independent.\n");
            LLVM_DEBUG(errs() << "\t--> Creating L0 to L1.\n");
            addDepToVector(DIDep.get(), L0Inst, L1Inst, Dep::Direction::L0ToL1,
                           Deps);
            NumDepsFound++;
          } else if (DIDep->isConfused()) {
            LLVM_DEBUG(errs() << "Comparing:\n"
                         << "\t" << *L0Inst << "\n\t" << *L1Inst << "\n");
            LLVM_DEBUG(errs() << "\tDepedence is confused.\n");
            LLVM_DEBUG(errs() << "\t--> Creating L0 to L1.\n");
            addDepToVector(DIDep.get(), L0Inst, L1Inst, Dep::Direction::L0ToL1,
                           Deps);
            NumDepsFound++;
          }
        }
      }
    }
  }

  return Deps;
}

// Function that attemps the fusion by cloning the funciton and doing the fusion
// there
bool attemptFusion(Function *F, FusionCandidate *FC0, FusionCandidate *FC1,
                   std::vector<BasicBlock *> moveUp,
                   std::vector<BasicBlock *> moveDown,
                   int numIntermediateBlocks) {
  // First, Clone Function to fall-back if fusion is illegal
  ValueToValueMapTy VMap;
  Function *FClone = CloneFunction(F, VMap);
  //  FClone->viewCFG();

  // Get LoopInfo for cloned function
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*FClone);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*FClone);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*FClone);

  // Get the Fusion Candidates of the cloned funciton
  BasicBlock *L0HeaderClone = dyn_cast<BasicBlock>(VMap[FC0->Header]);
  Loop *L0Clone = LI.getLoopFor(L0HeaderClone);
  FusionCandidate FC0Clone(L0Clone);
  LLVM_DEBUG(FC0Clone.print());

  BasicBlock *L1HeaderClone = dyn_cast<BasicBlock>(VMap[FC1->Header]);
  Loop *L1Clone = LI.getLoopFor(L1HeaderClone);
  FusionCandidate FC1Clone(L1Clone);
  LLVM_DEBUG(FC1Clone.print());

  auto start = std::chrono::steady_clock::now();
  std::vector<Dep> Deps = getDeps(&FC0Clone, &FC1Clone);
  auto end = std::chrono::steady_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  LLVM_DEBUG(errs() << "*** Getting Deps took " << duration.count() << " ms\n");

  LLVM_DEBUG(errs() << "Dumping Deps:\n");
  LLVM_DEBUG(dumpDeps(Deps));

  assert(FC0Clone.Exiting == FC0Clone.Latch &&
         "Expecting latch and exiting blocks to be the same!");
  assert(FC1Clone.Exiting == FC1Clone.Latch &&
         "Expecting latch and exiting blocks to be the same!");

  // Keep track of L0 and L1 induction variables
  PHINode *L0IV = FC0Clone.L->getInductionVariable(SE);
  PHINode *L1IV = FC1Clone.L->getInductionVariable(SE);
  assert(L0IV && "Unable to get L0 induction variable!");
  assert(L1IV && "Unable to get L1 induction variable!");
  LLVM_DEBUG(errs() << "L0 IV: " << *L0IV << "\nL1 IV: " << *L1IV << "\n");

  // Keep track of L0 preheader
  BasicBlock *L0Preheader = FC0Clone.Preheader;
  LLVM_DEBUG(errs() << "L0 Preheader: " << L0Preheader->getName() << "\n");

  // Remember L1 blocks to move to L0 after fusing
  std::vector<BasicBlock *> BBL1 = FC1Clone.L->getBlocks();

  // 1) Move intermediate blocks up or down
  std::vector<BasicBlock *> moveUpClone;
  for (auto *BB : moveUp) {
    moveUpClone.push_back(dyn_cast<BasicBlock>(VMap[BB]));
  }
  std::vector<BasicBlock *> moveDownClone;
  for (auto *BB : moveDown) {
    moveDownClone.push_back(dyn_cast<BasicBlock>(VMap[BB]));
  }
  bool isUnsupported = FC0Clone.moveIBBs(&FC1Clone, moveUpClone, moveDownClone,
                                         L0Preheader, numIntermediateBlocks);
  LLVM_DEBUG(errs() << "L0 Preheader: " << L0Preheader->getName() << "\n");

  // Check if unsopported case: Need to abort fusion and delete FClCloneone
  if (isUnsupported) {
    FClone->eraseFromParent();
    return false;
  }

  // 2) Replace L0 latch terminator with unconditional branch to L1 header
  //		Fix L1 header phis accordingly
  Value *OldCond = FC0Clone.changeL0LatchTerminator(&FC1Clone);

  // 3) L1 latch terminator points to L0 header instead of L1 header
  FC0Clone.changeL1LatchTerminator(&FC1Clone, OldCond);

  // 4) Move L1 phis to L0 header
  FC0Clone.movePhis(&FC1Clone, L0Preheader, &SE);

  // 5) Move all L1 blocks to L0
  FC0Clone.moveBlocks(&FC1Clone, BBL1);
  //  FClone->viewCFG();

  // 6) Check new Dependences and make sure dependences are not violated
  start = std::chrono::steady_clock::now();
  bool isIllegal = checkDeps(Deps, FClone, &FC0Clone, L0IV);
  end = std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  LLVM_DEBUG(errs() << "*** Checking Deps took " << duration.count() << " ms\n");

  LLVM_DEBUG(errs() << (isIllegal ? "Fusion is Illegal!\n" : "Fusion is legal!\n"));
  NumFusionsAttempted++;

  FClone->eraseFromParent();
  return !isIllegal;
}
// Function that fuses the loop candidates while moving the intermediate
// blocks up or down. After fusion, check for dependences.
bool fuseLoops(FusionCandidate *FC0, FusionCandidate *FC1,
               std::vector<BasicBlock *> &moveUp,
               std::vector<BasicBlock *> &moveDown, int numIntermediateBlocks,
               Function *F, LoopInfo &LI, ScalarEvolution &SE) {
  auto start = std::chrono::steady_clock::now();
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);

  LLVM_DEBUG(errs() << "Fusing L0: " << FC0->L->getName()
               << " with L1: " << FC1->L->getName() << "\n");

  LLVM_DEBUG(errs() << "L0:\t" << *(FC0->L));
  LLVM_DEBUG(errs() << "L1:\t" << *(FC1->L));
  LLVM_DEBUG(errs() << "L0 PHIs:\n");
  for (auto &phi : FC0->L->getHeader()->phis()) {
    LLVM_DEBUG(errs() << "  - " << phi << "\n");
  }
  LLVM_DEBUG(errs() << "L1 PHIs:\n");
  for (auto &phi : FC1->L->getHeader()->phis()) {
    LLVM_DEBUG(errs() << "  - " << phi << "\n");
  }

  // Keep track of Loop 0 header, we will need it later
  BasicBlock *L0Header = FC0->L->getHeader();

  // Attempt fusion on cloned function, if legal then reattempt otherwise abort.
  bool isLegal =
      attemptFusion(F, FC0, FC1, moveUp, moveDown, numIntermediateBlocks);
  if (isLegal) {
    LLVM_DEBUG(errs() << "Fusion is legal, fusing!\n");
    LLVM_DEBUG(errs() << "Fusing L0: " << FC0->L->getName()
                 << " with L1: " << FC1->L->getName() << "\n");
    LLVM_DEBUG(FC0->print());
    LLVM_DEBUG(FC1->print());

    LLVM_DEBUG(errs() << "L1 subloops:\n");
    for (auto L : FC1->L->getSubLoops()) {
      LLVM_DEBUG(L->dump());
    }

    assert(FC0->Exiting == FC0->Latch &&
           "Expecting latch and exiting blocks to be the same!");
    assert(FC1->Exiting == FC1->Latch &&
           "Expecting latch and exiting blocks to be the same!");

    // Keep track of L0 and L1 induction variables
    PHINode *L0IV = FC0->L->getInductionVariable(SE);
    PHINode *L1IV = FC1->L->getInductionVariable(SE);
    assert(L0IV && "Unable to get L0 induction variable!");
    assert(L1IV && "Unable to get L1 induction variable!");
    LLVM_DEBUG(errs() << "L0 IV: " << *L0IV << "\nL1 IV: " << *L1IV << "\n");

    // Check for intrinsic uses of L1 (ivdep,NZLoop)
    // if found, remove them.
    std::vector<IntrinsicInst *> IItoRemove;
    for (auto *U : L1IV->users()) {
      if (auto *II = dyn_cast<IntrinsicInst>(U)) {
        if (II->getIntrinsicID() == Intrinsic::hpvm_nz_loop ||
            II->getIntrinsicID() == Intrinsic::hpvm_ivdep) {
          IItoRemove.push_back(II);
        }
      }
    }
    for (auto *II : IItoRemove) {
      II->eraseFromParent();
    }

    // Keep track of L0 preheader
    BasicBlock *L0Preheader = FC0->Preheader;
    LLVM_DEBUG(errs() << "L0 Preheader: " << L0Preheader->getName() << "\n");

    // Remember L1 blocks to move to L0 after fusing
    std::vector<BasicBlock *> BBL1 = FC1->L->getBlocks();
    std::vector<Loop *> SubLoopsL1 = FC1->L->getSubLoops();

    // 1) Move intermediate blocks up or down
    bool isUnsupported = FC0->moveIBBs(FC1, moveUp, moveDown, L0Preheader,
                                       numIntermediateBlocks);
    LLVM_DEBUG(errs() << "L0 Preheader: " << L0Preheader->getName() << "\n");

    // Check if unsopported case: Need to abort fusion and delete FClone
    if (isUnsupported) {
      llvm_unreachable("We can't be here!");
    }

    // 2) Replace L0 latch terminator with unconditional branch to L1 header
    //		Fix L1 header phis accordingly
    Value *OldCond = FC0->changeL0LatchTerminator(FC1);

    // 3) L1 latch terminator points to L0 header instead of L1 header
    FC0->changeL1LatchTerminator(FC1, OldCond);

    // 4) Move L1 phis to L0 header
    FC0->movePhis(FC1, L0Preheader, &SE);

    // 5) Move all L1 blocks to L0
    FC0->moveBlocks(FC1, BBL1);

    LLVM_DEBUG(errs() << "L1 subloops:\n");
    for (auto L : SubLoopsL1) {
      LLVM_DEBUG(L->dump());
    }
    // 6) Move L1 sub-loops to L0
    for (auto L : SubLoopsL1) {
      LLVM_DEBUG(L->dump());
      FC1->L->removeChildLoop(L);
      L->setParentLoop(FC0->L);
    }

    SE.forgetLoop(FC0->L);
    LI.erase(FC1->L);

    LLVM_DEBUG(errs() << "Loops:\n");
    for (auto loop : LI)
      LLVM_DEBUG(errs() << *loop);

    // Update FC0
    //
    FC0->update(LI.getLoopFor(L0Header));
    LLVM_DEBUG(FC0->print());

    LLVM_DEBUG(errs() << "L0:\t" << *(FC0->L));
    LLVM_DEBUG(errs() << "L0 PHIs:\n");
    for (auto &phi : FC0->L->getHeader()->phis()) {
      LLVM_DEBUG(errs() << "  - " << phi << "\n");
    }

    LLVM_DEBUG(errs() << "Done with fusion!\n");
    NumFusions++;
    auto end = std::chrono::steady_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    LLVM_DEBUG(errs() << "** Fusion attempt took " << duration.count() << " ms\n");
    return true;
  }
  auto end = std::chrono::steady_clock::now();
  auto duration =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  LLVM_DEBUG(errs() << "** Fusion attempt took " << duration.count() << " ms\n");
  return false;
}
// Find the maximum depth of all the loop nests in this function.
// TODO: see if this can be replaced by any built-in functionality in
// newer LLVM.
unsigned getMaxDepth(LoopInfo *LI) {
  LLVM_DEBUG(errs() << "Finding max depth!\n");
  unsigned maxDepth = 1;
  for (auto l : LI->getLoopsInPreorder()) {
    if (l->getLoopDepth() > maxDepth) {
      maxDepth = l->getLoopDepth();
    }
  }
  return maxDepth;
}

// Function that returns a vector with all the loops at each depth w.r.t
// parent
std::map<Loop *, std::vector<Loop *>> getLoopsAtDepth(LoopInfo &LI, int depth) {
  std::map<Loop *, std::vector<Loop *>> LoopsAtDepth;
  LLVM_DEBUG(errs() << "Getting loops at depth: " << depth << "\n");
  for (auto L : LI.getLoopsInPreorder()) {
    //    LLVM_DEBUG(errs() << "\nChecking Loop: " << L->getName() << "\n");
    int loopDepth = L->getLoopDepth();
    //    LLVM_DEBUG(errs() << "depth: " << loopDepth << "\n");
    if (loopDepth == depth) {
      LLVM_DEBUG(errs() << "\n  - Found Loop: " << L->getName() << "\n");
      Loop *parent = L->getParentLoop();
      if (parent == NULL)
        parent = 0;
      LoopsAtDepth[parent].push_back(L);
    }
  }
  return LoopsAtDepth;
}

bool compareValue(Value *Vi, Value *Vj) {
  if (ConstantInt *CVi = dyn_cast<ConstantInt>(Vi)) {
    if (ConstantInt *CVj = dyn_cast<ConstantInt>(Vj)) {
      if (CVi->getSExtValue() == CVj->getSExtValue()) {
        LLVM_DEBUG(errs() << "Values are constants and match!\n");
        return true;
      } else {
        LLVM_DEBUG(errs() << "Values are consants and don't match!\n");
        return false;
      }
    } else {
      LLVM_DEBUG(errs() << "One value is const but the other isn't!");
      return false;
    }
  } else if (Vi->hasName()) {
    if (Vj->hasName()) {
      if (Vi->getName() == Vj->getName()) {
        LLVM_DEBUG(errs() << "Values have names and match!\n");
        return true;
      } else {
        LLVM_DEBUG(errs() << "Values have names and don't match!\n");
        return false;
      }
    } else {
      LLVM_DEBUG(errs() << "One value has name but the other doesn't!");
      return false;
    }
  }
}
// Function that compares the loop bounds of two loops and returns if
// they are equivalent (i.e. the loops can be fused).
bool compareLoopBounds(Loop::LoopBounds *LBi, Loop::LoopBounds *LBj) {
  Value *initialIVValuei = &(LBi->getInitialIVValue());
  Value *stepValuei = LBi->getStepValue();
  Value *finalIVValuei = &(LBi->getFinalIVValue());
  Value *initialIVValuej = &(LBj->getInitialIVValue());
  Value *stepValuej = LBj->getStepValue();
  Value *finalIVValuej = &(LBj->getFinalIVValue());

  // compare initial value
  LLVM_DEBUG(errs() << "Comparing Initial Values: ");
  bool isEqualIV = compareValue(initialIVValuei, initialIVValuej);

  // compare final value
  LLVM_DEBUG(errs() << "Comparing Final Values: ");
  bool isEqualFV = compareValue(finalIVValuei, finalIVValuej);

  // compare step value
  LLVM_DEBUG(errs() << "Comparing Step Values: ");
  bool isEqualSV = compareValue(stepValuei, stepValuej);

  bool isEqualD = false;
  if (LBi->getDirection() == LBj->getDirection()) {
    LLVM_DEBUG(errs() << "Directions Match!\n");
    isEqualD = true;
  }
  if (isEqualD && isEqualFV && isEqualIV && isEqualSV) {
    return true;
  } else {
    return false;
  }
}

static void dumpVector(std::vector<int> v1, std::string name) {
  LLVM_DEBUG(errs() << name << ": { ");
  for (unsigned i = 0; i < v1.size(); ++i) {
    LLVM_DEBUG(errs() << v1[i] << " ");
  }
  LLVM_DEBUG(errs() << "}\n");
}

// build the Disjoint Sets of loops that can be fused
// TODO: Write a more detailed description of this function
void buildDisjointSets(const std::vector<Loop *> &loops, ScalarEvolution *SE,
                       DominatorTree *DT, PostDominatorTree *PDT,
                       std::map<int, std::vector<Loop *>> &loopSets,
                       std::vector<int> &completeSets) {
  std::vector<int> uniqueID;

  // Build initial sets: each loop has its own set.
  for (int i = 0; i < loops.size(); ++i) {
    LLVM_DEBUG(errs() << "Checking loop: " << *loops[i] << "\n");
    Optional<Loop::LoopBounds> LB = loops[i]->getBounds(*SE);
    assert(LB.hasValue() && "Unable to retrieve loop bound object!");
    uniqueID.push_back(i);
    loopSets[i].push_back(loops[i]);
  }

  LLVM_DEBUG(errs() << "Building Disjoint Sets:\n");

  LLVM_DEBUG(errs() << "Initial Sets: \n");
  for (int i = 0; i < uniqueID.size(); ++i) {
    LLVM_DEBUG(errs() << uniqueID[i] << ":\n");
    for (auto it : loopSets[i]) {
      LLVM_DEBUG(errs() << "  " << it->getName() << ": ");
      Loop::LoopBounds LB = it->getBounds(*SE).getValue();
      Value *initialIVValue = &(LB.getInitialIVValue());
      Value *stepValue = LB.getStepValue();
      Value *finalIVValue = &(LB.getFinalIVValue());

      LLVM_DEBUG(errs() << *initialIVValue << "; " << *stepValue << "; "
                   << *finalIVValue << "\n");
    }
  }
  LLVM_DEBUG(errs() << "\n");

  LLVM_DEBUG(LLVM_DEBUG(errs()
              << "*******************************************************\n");
        dumpVector(uniqueID, "unique IDs");
        dumpVector(completeSets, "completed sets");
        LLVM_DEBUG(errs()
              << "*******************************************************\n"));
  // Build the disjoint sets
  do {
    // When we are unable to combine a loop with the one we are handling right
    // now, we will place its id in this vector. This vector will be used in
    // each outerloop iteration to handle loop sets that have not been completed
    // yet.
    std::vector<int> newUniqueID;
    auto setid = uniqueID[0];
    LLVM_DEBUG(errs() << "setid: " << setid << "\n");
    Loop *li = *(loopSets[setid].begin());
    Loop::LoopBounds LBi = li->getBounds(*SE).getValue();
    completeSets.push_back(setid);
    for (int j = 1; j < uniqueID.size(); ++j) {
      int compareid = uniqueID[j];
      LLVM_DEBUG(errs()
            << "-------------------------------------------------------\n");
      LLVM_DEBUG(errs() << "compare id: " << compareid << "\n");
      Loop *lj = *(loopSets[compareid].begin());
      Loop::LoopBounds LBj = lj->getBounds(*SE).getValue();

      LLVM_DEBUG(errs() << "Comparing loops: " << li->getName() << " and "
                   << lj->getName() << "\n");

      bool isFusionCandidates = false;

      // For fusion to be legal, li must dominate lj and lj must post-dominate
      // li
      if (DT->dominates(li->getLoopPreheader(), lj->getLoopPreheader())) {
        if (PDT->dominates(lj->getLoopPreheader(), li->getLoopPreheader())) {
          isFusionCandidates = true;
          LLVM_DEBUG(errs() << "The loops form a dominance chain!\n");
        }
      }
      // Also, li and lj must have the same loop bounds
      if (isFusionCandidates) {
        if (compareLoopBounds(&LBi, &LBj)) {
          LLVM_DEBUG(errs() << "The loops have same loop bounds!\n");
        } else {
          LLVM_DEBUG(errs() << "The loops DO NOT have the same loop bounds!\n");
          isFusionCandidates = false;
        }
      } else {
        LLVM_DEBUG(errs() << "The loops DO NOT form a dominance chain!\n");
      }

      // if both conditions are satisfied, add li and lj to same set
      if (isFusionCandidates) {
        // Add loop j to set of loop i and update uniqueIDs
        loopSets[setid].push_back(lj);
        //				std::remove(loopSets[compareid].begin(),
        // loopSets[compareid].end(), lj);
        LLVM_DEBUG(errs() << "Loops are candidates for fusion!\n");
        LLVM_DEBUG(errs() << "loopSets[" << setid << "]: {");
        for (auto it : loopSets[setid]) {
          LLVM_DEBUG(errs() << it->getName() << "; ");
        }
        LLVM_DEBUG(errs() << "}\n");
        LLVM_DEBUG(errs() << "loopSets[" << compareid << "]: {");
        for (auto it : loopSets[compareid]) {
          LLVM_DEBUG(errs() << it->getName() << "; ");
        }
        LLVM_DEBUG(errs() << "}\n");
      } else {
        LLVM_DEBUG(errs() << "Loops are not candidates for fusion!\n");
        newUniqueID.push_back(compareid);
      }
    }
    uniqueID = newUniqueID;
    newUniqueID = std::vector<int>();
    LLVM_DEBUG(LLVM_DEBUG(errs()
                << "*******************************************************\n");
          dumpVector(uniqueID, "unique IDs");
          dumpVector(completeSets, "completed sets"); LLVM_DEBUG(
              errs()
              << "*******************************************************\n"));
  } while (!uniqueID.empty());

  LLVM_DEBUG(LLVM_DEBUG(errs() << "Complete Sets:\n");
        for (int i = 0; i < completeSets.size(); ++i) {
          int set = completeSets[i];
          LLVM_DEBUG(errs() << "set " << set << ":\n");
          for (auto it : loopSets[set]) {
            LLVM_DEBUG(errs() << "  " << it->getName() << ": \n");
            LLVM_DEBUG(errs() << *it);
          }
        });
}

// Function that finds loops with similar bounds at the same level
// and returns all the sets of loops that can be fused together.
std::vector<std::vector<Loop *>>
findFusableLoops(const std::vector<Loop *> &loops, Function *F, LoopInfo *LI,
                 ScalarEvolution *SE, DominatorTree *DT,
                 PostDominatorTree *PDT) {

  // This will ahve all the loop sets
  std::map<int, std::vector<Loop *>> loopSets;
  // This will have the IDs of the completed sets
  std::vector<int> completeSets;
  // First step:
  // Compare loop bounds and group loops into sets of equivalent loop bounds
  buildDisjointSets(loops, SE, DT, PDT, loopSets, completeSets);

  // Create the set of fusable loops using the header BB names
  // TODO: This is not a stable way of doing it.
  std::vector<std::vector<Loop *>> fusableLoops;
  for (int i = 0; i < completeSets.size(); ++i) {
    int set = completeSets[i];
    std::vector<Loop *> v;
    for (auto it : loopSets[set]) {
      v.push_back(it);
    }
    fusableLoops.push_back(v);
  }

  LLVM_DEBUG(
      for (int i = 0; i < fusableLoops.size(); ++i) {
        LLVM_DEBUG(errs() << "set " << i << ": {");
        for (int j = 0; j < fusableLoops[i].size(); ++j) {
          LLVM_DEBUG(errs() << fusableLoops[i][j]->getName() << "; ");
        }
        LLVM_DEBUG(errs() << "}\n");
      }

      //  F->viewCFG();
      for (int i = 0; i < fusableLoops.size(); ++i) {
        LLVM_DEBUG(errs() << "set " << i << ": \n");
        for (int j = 0; j < fusableLoops[i].size(); ++j) {
          Loop *loop = fusableLoops[i][j];
          LLVM_DEBUG(errs() << "  Loop: " << loop->getName() << "\n");
          BasicBlock *ExitBB = loop->getExitBlock();
          LLVM_DEBUG(errs() << "    Exit Block: " << ExitBB->getName() << "\n");
          BasicBlock *ExitingBB = loop->getExitingBlock();
          LLVM_DEBUG(errs() << "    Exiting Block: " << ExitingBB->getName()
                       << "\n");
          BasicBlock *HeaderBB = loop->getHeader();
          LLVM_DEBUG(errs() << "    Header Block: " << HeaderBB->getName() << "\n");
          BasicBlock *LatchBB = loop->getLoopLatch();
          LLVM_DEBUG(errs() << "    Latch Block: " << LatchBB->getName() << "\n");
          BasicBlock *PreheaderBB = loop->getLoopPreheader();
          LLVM_DEBUG(errs() << "    Preheader Block: " << PreheaderBB->getName()
                       << "\n");
          BasicBlock *PredecessorBB = loop->getLoopPredecessor();
          LLVM_DEBUG(errs() << "    Predecessor Block: " << PredecessorBB->getName()
                       << "\n");
          LLVM_DEBUG(errs() << "    Loop Blocks: ");
          for (auto it : loop->getBlocks()) {
            LLVM_DEBUG(errs() << it->getName() << "; ");
          }
          LLVM_DEBUG(errs() << "\n");
        }
      });

  //
  return fusableLoops;
}

// Fuse all loops at a certain depth
int handleDepth(Function *F, int depth) {
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  // auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &PDT = FAM.getResult<PostDominatorTreeAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  auto &DI = FAM.getResult<DependenceAnalysis>(*F);

  std::unordered_set<Loop*> FusedLoopPointers;

  LLVM_DEBUG(errs() << "Handling depth: " << depth << "\n");
  assert(depth > 0 && "Invalid depth value!");

  int numFusions = 0;

  // Determine loops at current depth with their parents
  auto LoopsAtDepth = getLoopsAtDepth(LI, depth);

  //  std::vector<Loop *> loops;
  for (auto it : LoopsAtDepth) {

    auto *parent = it.first;
    LLVM_DEBUG(errs() << "Handling parent: "
                 << ((parent != NULL) ? it.first->getName() : "0") << "\n\n");

    auto loops = it.second;
    // If there are less than 2 loops at this depth/parent, there is nothing to
    // fuse.
    if (loops.size() < 2)
      continue;

    auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);

    LLVM_DEBUG(errs() << "* There are more than one loops at this "
                    "depth/parent combo!\n");

    // Check the loops at this depth/parent, and construct n sets
    // of fusable loops by comparing the loop bounds.
    auto start = std::chrono::steady_clock::now();
    auto fusableLoops = findFusableLoops(loops, F, &LI, &SE, &DT, &PDT);
    auto end = std::chrono::steady_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    LLVM_DEBUG(errs() << "** Finding fusable loops took " << duration.count()
                 << " ms\n");

    LLVM_DEBUG(errs() << "Printing loops before fusion!\n");
    for (auto loop : LI)
      LLVM_DEBUG(errs() << *loop << "\n");

    // Go through the constructed sets
    for (size_t set = 0; set < fusableLoops.size(); ++set) {
      LLVM_DEBUG(errs() << "Processing set: " << set << "\n");
      std::vector<Loop *> fusableSet = fusableLoops[set];
      if (fusableSet.size() < 2)
        continue;

      // check legality by pair
      // 1) No SSA variable in L1 can depend on L0
      // 2) For all BBs between L0 and L1:
      //   a) if BB !dep L0 --> add to moveup
      //   b) if BB !dep L1 --> add to movedown
      //   c) if BB dep L0 and L1 dep BB --> cannot fuse!
      // 3) gather all deps between L0 and L1 and check after fusion
      for (auto l0it = fusableSet.begin(); l0it != fusableSet.end(); ++l0it) {
        if(FusedLoopPointers.find(*l0it)!=FusedLoopPointers.end()) {
          LLVM_DEBUG(errs() << "The iterator is pointing to a now fused (i.e. corrupted) loop\n");
          continue;
        }
        Loop *L0 = *l0it;
        if (L0->isInvalid()) {
          LLVM_DEBUG(errs() << "L0 is not valid!\n");
          continue;
        }

        // Build a fusion candidate object for L0
        FusionCandidate FC0(L0);

        for (auto l1it = l0it + 1; l1it != fusableSet.end(); ++l1it) {
          if(FusedLoopPointers.find(*l1it)!=FusedLoopPointers.end()) {
            LLVM_DEBUG(errs() << "The iterator is pointing to a now fused (i.e. corrupted) loop\n");
            continue;
          }
          Loop *L1 = *l1it;
          if (L1->isInvalid()) {
            LLVM_DEBUG(errs() << "L1 is not valid!\n");
            continue;
          }

          // Build a fusion candidate object for L1
          FusionCandidate FC1(L1);

          LLVM_DEBUG(FC0.print(); FC1.print());

          LLVM_DEBUG(errs() << "\nChecking L0: " << FC0.L->getName()
                       << " and L1: " << FC1.L->getName() << "\n");
          NumLoopsChecked++;

          bool invalidDependences = 0;

          // Check SSA dependences between both loops
          //          invalidDependences = checkSSADependences(&FC0, &FC1);
          auto start = std::chrono::steady_clock::now();
          invalidDependences = FC0.checkSSADependences(&FC1);
          auto end = std::chrono::steady_clock::now();
          auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(
              end - start);
          LLVM_DEBUG(errs() << "** Checking SSA Deps took " << duration.count()
                       << " ms\n");
          if (invalidDependences) {
            LLVM_DEBUG(errs() << "Unable to fuse L0: " << L0->getName()
                         << " and L1: " << L1->getName() << "\n");
            continue;
          }

          // Check blocks between L0 and L1 to see if they
          // can be moved up/down
          //  1) SSA dependences
          //  2) Memory dependences
          std::vector<BasicBlock *> moveUpBB;
          std::vector<BasicBlock *> moveDownBB;
          int numIBBs;
          start = std::chrono::steady_clock::now();
          invalidDependences = FC0.checkIntermediateBlocks(
              &FC1, moveUpBB, moveDownBB, numIBBs, F);
          end = std::chrono::steady_clock::now();
          duration = std::chrono::duration_cast<std::chrono::milliseconds>(
              end - start);
          LLVM_DEBUG(errs() << "** Checking Intermediate Blocks took "
                       << duration.count() << " ms\n");

          if (invalidDependences) {
            LLVM_DEBUG(errs() << "Unable to fuse L0: " << L0->getName()
                         << " and L1: " << L1->getName() << "\n");
            continue;
          }

          // Attempt to fuse L0 and L1
          if (fuseLoops(&FC0, &FC1, moveUpBB, moveDownBB, numIBBs, F, LI, SE)) {
            FusedLoopPointers.insert(*l1it);
            LLVM_DEBUG(errs() << "Fusion succesfull!\n");
            LLVM_DEBUG(errs() << "New FC0:\n"; FC0.print());
            numFusions++;
          } else {
            LLVM_DEBUG(errs() << "Fusion failed!\n");
            LLVM_DEBUG(errs() << "New FC0:\n"; FC0.print());
          }
        }
      }
    }
    LLVM_DEBUG(errs() << "Printing loops after fusion!\n");

    for (auto loop : LI) {
      LLVM_DEBUG(errs() << *loop << "\n");
    }
  }
  FunctionPassManager FPM;
  FPM.addPass(SimplifyCFGPass());
  FPM.addPass(LoopSimplifyPass());
  FPM.run(*F, FAM);

  return numFusions;
}

// Go through the loop nests in Function F and try to fuse all the loops
bool hpvm::fuseFuncLoops(llvm::Function *F, int lvl) {
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);

  // LLVM_DEBUG(F->viewCFG());

  // For all the loop nests in the function
  for (auto LIB = LI.begin(); LIB != LI.end(); LIB++) {
    Loop *L = *LIB;

    // Make sure all the loops are in loopSimplify form
    if (simplifyLoop(L, &DT, &LI, &SE, &AC, nullptr, false)) {
      LLVM_DEBUG(errs() << "Simplified loop!\n" << *L << "\n");
    }
  }
  LLVM_DEBUG(errs() << "Attempting to fuse loops\n");
  LLVM_DEBUG(errs() << "------------------------\n");
  LLVM_DEBUG(errs() << "------------------------\n");

  int numFusions = 0;

  if (lvl == 0) {
    // Find the maximum depth of all loop nests in this function
    int maxDepth = getMaxDepth(&LI);
    LLVM_DEBUG(errs() << "Max depth: " << maxDepth << "\n");

    // Go through all the depths and handle each depth from the
    // outermost loop depth to the innermost.
    for (int depth = 1; depth < maxDepth + 1; ++depth) {
      auto start = std::chrono::steady_clock::now();
      numFusions += handleDepth(F, depth);
      auto end = std::chrono::steady_clock::now();
      auto duration =
          std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
      errs() << "* Handling depth " << depth << " took " << duration.count()
             << " ms\n";
      //// Simplify the CFG before we exit
      // SimplifyCFGOptions Options;
      // Options.forwardSwitchCondToPhi(false);
      // Options.convertSwitchToLookupTable(false);
      // Options.needCanonicalLoops(true);
      // Options.sinkCommonInsts(false);
      // Options.setAssumptionCache(&FAM.getResult<AssumptionAnalysis>(*F));
      // simplifyFunctionCFG(*F, FAM.getResult<TargetIRAnalysis>(*F), Options);

      //    LLVM_DEBUG(F->viewCFG());
    }
  } else {
    numFusions += handleDepth(F, lvl);
  }
  // LLVM_DEBUG(F->viewCFG());

  //  F->viewCFG();
  //  LLVM_DEBUG(errs() << *F);
  if (numFusions > 0)
    return true;
  else
    return false;
}

// Function that checks for SSA dependences between two loops
// Returns number of dependences.
bool FusionCandidate::checkSSADependences(FusionCandidate *FC1) {
  LLVM_DEBUG(errs() << "Checking SSA Violations\n");
  Loop *L0 = L;
  Loop *L1 = FC1->L;
  for (BasicBlock *BB : L1->blocks()) {
    for (Instruction &I : *BB) {
      for (auto &Op : I.operands()) {
        if (Instruction *Def = dyn_cast<Instruction>(Op)) {
          if (L0->contains(Def->getParent())) {
            LLVM_DEBUG(errs() << "  Found SSA violation between:\n    L1: " << I
                         << "\n    L0: " << *Def << "\n");
            return true;
          }
        }
      }
    }
  }

  return false;
}

bool FusionCandidate::checkIntermediateBlocks(
    FusionCandidate *FC1, std::vector<BasicBlock *> &moveUpBB,
    std::vector<BasicBlock *> &moveDownBB, int &numIBBs, llvm::Function *F) {
  bool invalidDependences = false;
  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &PDT = FAM.getResult<PostDominatorTreeAnalysis>(*F);
  auto &DI = FAM.getResult<DependenceAnalysis>(*F);

  // Gather intermediate blocks
  std::vector<IntermediateBlock *> intermediateBlocks =
      getIntermediateBlocks(FC1, F, &DT, &PDT);
  numIBBs = intermediateBlocks.size();

  // check dependences between intermediate blocks and loops
  checkIBBDependences(FC1, intermediateBlocks, &DI);

  // Mark which IBB can be moved Up or Down
  for (IntermediateBlock *IBB : intermediateBlocks) {
    if (!IBB->depL0) {
      IBB->moveUp = true;
      moveUpBB.push_back(IBB->BB);
    }
    if (!IBB->depL1) {
      IBB->moveDown = true;
      moveDownBB.push_back(IBB->BB);
    }
    if (IBB->depL0 && IBB->depL1) {
      invalidDependences = true;
      IBB->fusionPreventing = true;
    }
  }

  LLVM_DEBUG(errs() << "\nPrinting Intermediate Block info:\n";
        for (IntermediateBlock *IBB
             : intermediateBlocks) {
          LLVM_DEBUG(errs() << "* BB: " << IBB->BB->getName() << "\n");
          LLVM_DEBUG(errs() << "  - MemOps:\n");
          for (auto I : IBB->MemOps)
            LLVM_DEBUG(errs() << "    + " << *I << "\n");
          LLVM_DEBUG(errs() << "  - Depends on L0: "
                       << (IBB->depL0 ? "yes\n" : "no\n"));
          LLVM_DEBUG(errs() << "  - Depends on L1: "
                       << (IBB->depL1 ? "yes\n" : "no\n"));
          if (IBB->moveUp) {
            LLVM_DEBUG(errs() << "    + Can be moved up!\n");
          }
          if (IBB->moveDown) {
            LLVM_DEBUG(errs() << "    + Can be moved down!\n");
          }
          if (IBB->fusionPreventing) {
            LLVM_DEBUG(errs() << "    + Is fusion preventing!\n");
          }
        });
  return invalidDependences;
}

// Function that returns a vector with the BBs that are
// dominated by L0 and dominate L1
std::vector<IntermediateBlock *>
FusionCandidate::getIntermediateBlocks(FusionCandidate *FC1, Function *F,
                                       DominatorTree *DT,
                                       PostDominatorTree *PDT) {
  LLVM_DEBUG(errs() << "Finding intermediate blocks\n");
  std::vector<IntermediateBlock *> intermediateBlocks;
  Loop *L0 = L;
  Loop *L1 = FC1->L;
  for (auto &BB : F->getBasicBlockList()) {
    if (L0->contains(&BB) || L1->contains(&BB))
      continue;
    if (&BB == L0->getHeader() || &BB == L1->getHeader())
      continue;
    if (DT->dominates(L0->getHeader(), &BB)) {
      if (PDT->dominates(L1->getHeader(), &BB)) {
        LLVM_DEBUG(errs() << "  BB: " << BB.getName()
                     << " is an intermediate block!\n");
        std::vector<Instruction *> MemOps;
        for (auto &I : BB)
          // TODO: For now, assume we are only considering loads and stores
          // and ignoring function calls!
          // if (isa<LoadInst>(I) || isa<StoreInst>(I))
          if (I.mayReadOrWriteMemory())
            MemOps.push_back(&I);
        IntermediateBlock *IB = new IntermediateBlock(&BB, MemOps);
        intermediateBlocks.push_back(IB);
      }
    }
  }

  return intermediateBlocks;
}

void FusionCandidate::checkIBBDependences(
    FusionCandidate *FC1, std::vector<IntermediateBlock *> &intermediateBlocks,
    DependenceInfo *DI) {
  LLVM_DEBUG(errs() << "Checking Dependence Violations\n");

  Loop *L0 = L;
  Loop *L1 = FC1->L;

  LLVM_DEBUG(errs() << "  First with L0:\n");
  for (auto it = intermediateBlocks.begin(); it != intermediateBlocks.end();
       ++it) {
    IntermediateBlock *IB = *it;
    BasicBlock *BB = IB->BB;
    LLVM_DEBUG(errs() << "    Checking BB: " << BB->getName() << "\n");
    LLVM_DEBUG(errs() << "    * First for SSA Deps\n");
    for (Instruction &I : *BB) {
      for (auto &Op : I.operands()) {
        if (Instruction *Def = dyn_cast<Instruction>(Op)) {
          if (L0->contains(Def->getParent())) {
            LLVM_DEBUG(errs() << "      Found SSA violation between:"
                         << "\n        BB: " << I << "\n        L0: " << *Def
                         << "\n");
            IB->depL0 = true;
          }
          for (auto iit = intermediateBlocks.begin(); iit != it; ++iit) {
            IntermediateBlock *IIB = *iit;
            //						LLVM_DEBUG(errs() <<
            //"Checking against
            //"
            //<< IIB->BB->getName() << "\n");
            if (IIB == IB)
              continue;
            if (IIB->depL0 && Def->getParent() == IIB->BB) {
              LLVM_DEBUG(errs() << "      Found SSA violation between:"
                           << "\n        BB: " << I << "\n        L0-dep-BB: "
                           << IIB->BB->getName() << ": " << *Def << "\n");
              IB->depL0 = true;
            }
          }
        }
      }
    }
    LLVM_DEBUG(errs() << "    * Next for Mem Deps\n");
    for (Instruction *LI : MemOps) {
      for (Instruction *I : IB->MemOps) {
        if (auto depResult = DI->depends(LI, I, 1)) {
          if (!depResult->isInput()) {
            LLVM_DEBUG(errs() << "      Found Dependence violation between:"
                         << "\n        BB: " << *I << "\n        L0: " << *LI
                         << "\n");
            LLVM_DEBUG(errs() << "          ");
            LLVM_DEBUG(depResult->dump(errs()));

            IB->depL0 = true;
          }
        }
      }
    }
    for (auto iit = intermediateBlocks.begin(); iit != it; ++iit) {
      IntermediateBlock *IIB = *iit;
      if (IIB->depL0)
        for (Instruction *LI : IIB->MemOps) {
          for (Instruction *I : IB->MemOps) {
            if (auto depResult = DI->depends(LI, I, 1)) {
              if (!depResult->isInput()) {
                LLVM_DEBUG(errs() << "      Found Dependence violation between:"
                             << "\n        BB: " << *I
                             << "\n        L0-dep-BB: " << IIB->BB->getName()
                             << ": " << *LI << "\n");
                LLVM_DEBUG(errs() << "          ");
                LLVM_DEBUG(depResult->dump(errs()));
                IB->depL0 = true;
              }
            }
          }
        }
    }
  }

  LLVM_DEBUG(errs() << "  Now with L1:\n");
  for (auto rit = intermediateBlocks.rbegin(); rit != intermediateBlocks.rend();
       ++rit) {
    IntermediateBlock *IB = *rit;
    BasicBlock *BB = IB->BB;
    LLVM_DEBUG(errs() << "    Checking BB: " << BB->getName() << "\n");
    for (Instruction &I : *BB) {
      for (auto Use : I.users()) {
        if (Instruction *User = dyn_cast<Instruction>(Use)) {
          if (L1->contains(User->getParent())) {
            LLVM_DEBUG(errs() << "      Found SSA violation between:"
                         << "\n        BB: " << I << "\n        L1: " << *User
                         << "\n");
            IB->depL1 = true;
          }
          for (auto rrit = intermediateBlocks.rbegin(); rrit != rit; ++rrit) {
            IntermediateBlock *IIB = *rrit;
            if (IIB == IB)
              continue;
            if (IIB->depL1 && User->getParent() == IIB->BB) {
              LLVM_DEBUG(errs() << "      Found SSA violation between:"
                           << "\n        BB: " << I << "\n        L1-dep-BB: "
                           << IIB->BB->getName() << ": " << *User << "\n");
              IB->depL1 = true;
            }
          }
        }
      }
    }
    LLVM_DEBUG(errs() << "    * Next for Mem Deps\n");
    for (Instruction *LI : FC1->MemOps) {
      for (Instruction *I : IB->MemOps) {
        if (auto depResult = DI->depends(LI, I, 1)) {
          if (!depResult->isInput()) {
            LLVM_DEBUG(errs() << "      Found Dependence violation between:"
                         << "\n        BB: " << *I << "\n        L1: " << *LI
                         << "\n");
            LLVM_DEBUG(errs() << "          ");
            LLVM_DEBUG(depResult->dump(errs()));

            IB->depL1 = true;
          }
        }
      }
    }
    for (auto rrit = intermediateBlocks.rbegin(); rrit != rit; ++rrit) {
      IntermediateBlock *IIB = *rrit;
      if (IIB->depL1)
        for (Instruction *LI : IIB->MemOps) {
          for (Instruction *I : IB->MemOps) {
            if (auto depResult = DI->depends(I, LI, 1)) {
              if (!depResult->isInput()) {
                LLVM_DEBUG(errs() << "      Found Dependence violation between:"
                             << "\n        BB: " << *I
                             << "\n        L1-dep-BB: " << IIB->BB->getName()
                             << ": " << *LI << "\n");
                LLVM_DEBUG(errs() << "          ");
                LLVM_DEBUG(depResult->dump(errs()));
                IB->depL1 = true;
              }
            }
          }
        }
    }
  }
}

bool FusionCandidate::moveIBBs(FusionCandidate *FC1,
                               std::vector<BasicBlock *> &moveUp,
                               std::vector<BasicBlock *> &moveDown,
                               BasicBlock *&L0Preheader,
                               int numIntermediateBlocks) {
  if (moveUp.size() > 0 && moveUp.size() == numIntermediateBlocks) {
    LLVM_DEBUG(errs() << "All blocks can be moved up!\n");
    for (auto BB : moveUp)
      LLVM_DEBUG(errs() << "  - " << BB->getName() << "\n");
    LLVM_DEBUG(errs() << "Moving Blocks Up!\n");
    for (auto BB : moveUp) {
      // First check if we are moving the top-most intermediate block.
      // If so, L0 preheader should point to it instead of L0.
      if (BB->getSinglePredecessor() == Latch) {
        assert(BB->getFirstNonPHI() == &*(BB->begin()) &&
               "We do not expect this block to have PHIs!");
        BranchInst *PT0 = dyn_cast<BranchInst>(Preheader->getTerminator());
        PT0->replaceSuccessorWith(Header, BB);
        Latch->getTerminator()->replaceSuccessorWith(BB, nullptr);
      }

      // Next check if we are moving the last intermediate block.
      // If so, block should point to L0 header.
      if (BB->getSingleSuccessor() == FC1->Header) {
        Header->replacePhiUsesWith(Preheader, BB);
        FC1->Header->replacePhiUsesWith(BB, Latch);
        //				BB->replaceSuccessorsPhiUsesWith(Preheader,
        // BB);
        BranchInst *BBT = dyn_cast<BranchInst>(BB->getTerminator());
        BBT->replaceSuccessorWith(FC1->Header, Header);
        L0Preheader = BB;
      }
    }
  } else if (moveDown.size() > 0 && moveDown.size() == numIntermediateBlocks) {
    LLVM_DEBUG(errs() << "All blocks can be moved down!\n");
    for (auto BB : moveDown)
      LLVM_DEBUG(errs() << "  - " << BB->getName() << "\n");
    LLVM_DEBUG(errs() << "Moving Blocks Down!\n");

    for (auto BB : moveDown) {
      // First check if we are moving the top-most intermediate block.
      // If so, L1 latch should point to it.
      if (BB->getSinglePredecessor() == Latch) {
        assert(BB->getFirstNonPHI() == &*(BB->begin()) &&
               "We do not expect this block to have PHIs!");
        BranchInst *PT1 = dyn_cast<BranchInst>(FC1->Latch->getTerminator());
        PT1->replaceSuccessorWith(FC1->Exit, BB);
        Latch->getTerminator()->replaceSuccessorWith(BB, nullptr);
      }

      // Next check if we are moving the last intermediate block.
      // If so, block should point to L1 Exit.
      if (BB->getSingleSuccessor() == FC1->Header) {
        FC1->Header->replacePhiUsesWith(BB, Latch);
        FC1->Exit->replacePhiUsesWith(FC1->Latch, BB);
        BranchInst *BBT = dyn_cast<BranchInst>(BB->getTerminator());
        BBT->replaceSuccessorWith(FC1->Header, FC1->Exit);
      }
    }
  } else {
    LLVM_DEBUG(errs() << "Can only handle moving blocks in one direction!\n "
                    "Skipping fusion!\n");
    return true;
  }
  return false;
}

void FusionCandidate::changeL1LatchTerminator(FusionCandidate *FC1,
                                              Value *Condition) {
  LLVM_DEBUG(errs() << "Pointing L1 latch terminator to L0 header!\n");
  BranchInst *LTI1 = dyn_cast<BranchInst>(FC1->Latch->getTerminator());
  assert(LTI1->isConditional() &&
         "Expecting latch terminator to be conditional!");
  LTI1->replaceSuccessorWith(FC1->Header, Header);
  Header->replacePhiUsesWith(Latch, FC1->Latch);

  // Set the condion of the L1 latch terminator to be the same as the old
  // condition of L0 latch terminator
  LTI1->setCondition(Condition);
  //	FC1->Header->replacePhiUsesWith()
}

Value *FusionCandidate::changeL0LatchTerminator(FusionCandidate *FC1) {
  //	FC0->Header->removePredecessor(FC0->Latch);
  LLVM_DEBUG(errs() << "Replacing L0 latch terminator with unconditional branch!\n");
  BranchInst *LTI0 = dyn_cast<BranchInst>(Latch->getTerminator());
  assert(LTI0->isConditional() &&
         "Expecting latch terminator to be conditional!");
  Value *Condition = LTI0->getCondition();
  LTI0->replaceSuccessorWith(Header, nullptr);
  BranchInst *newLatchTerminatorL0 = BranchInst::Create(FC1->Header, LTI0);
  LTI0->eraseFromParent();
  return Condition;
}

void FusionCandidate::movePhis(FusionCandidate *FC1, BasicBlock *L0Preheader,
                               ScalarEvolution *SE) {
  LLVM_DEBUG(errs() << "Moving L1 Phis to L0!\n");
  while (PHINode *phi = dyn_cast<PHINode>(&FC1->Header->front())) {
    LLVM_DEBUG(errs() << "  - " << *phi << "\n");
    if (SE->isSCEVable(phi->getType()))
      SE->forgetValue(phi);
    phi->moveBefore(Header->getFirstNonPHI());
    phi->replaceIncomingBlockWith(Latch, L0Preheader);
    LLVM_DEBUG(errs() << "  ----> " << *phi << "\n");
  }
}

void FusionCandidate::moveBlocks(FusionCandidate *FC1,
                                 const std::vector<BasicBlock *> &BBL1) {
  LLVM_DEBUG(errs() << "Moving BBs from L1 to L0:\n");

  for (BasicBlock *BB : BBL1) {
    LLVM_DEBUG(errs() << "  - BB: " << BB->getName() << "\n");
    L->addBlockEntry(BB);
    FC1->L->removeBlockFromLoop(BB);
  }
}

bool checkDeps(std::vector<Dep> Deps, Function *F, FusionCandidate *FC0,
               PHINode *L0IV) {

  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &DI = FAM.getResult<DependenceAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto accessDiffIsEqual = [&](Loop *L, Instruction *I0, Instruction *I1) {
    Value *Ptr0 = getLoadStorePointerOperand(I0);
    Value *Ptr1 = getLoadStorePointerOperand(I1);
    if (!Ptr0 || !Ptr1)
      return false;

    const SCEV *SCEVPtr0 = SE.getSCEVAtScope(Ptr0, L);
    const SCEV *SCEVPtr1 = SE.getSCEVAtScope(Ptr1, L);

    ICmpInst::Predicate Pred = ICmpInst::ICMP_EQ;
    bool IsAlwaysEQ = SE.isKnownPredicate(Pred, SCEVPtr0, SCEVPtr1);

    return IsAlwaysEQ;
  };
  bool illegal = false;

  bool is_debug = false;
  LLVM_DEBUG(is_debug = true);
  for (Dep D : Deps) {
    bool found = false;
    // Fusion is illegal if a loop-independent dependence in one direction
    // becomes a loop-carried dependence in the opposite direction.
    Instruction *Src = D.Dst;
    Instruction *Dst = D.Src;
    LLVM_DEBUG(errs() << "Checking:\n"
                 << "\t" << *Src << "\n\t" << *Dst << "\n");

    if (auto NewDIDep =
            const_cast<DependenceInfo *>(&DI)->depends(Src, Dst, true)) {
      LLVM_DEBUG(errs() << "\tNew Dependence: "; NewDIDep->dump(errs()));
      if (!accessDiffIsEqual(FC0->L, Src, Dst)) {
        //      if (!NewDIDep->isLoopIndependent()) {
        for (int i = 1; i <= NewDIDep->getLevels(); ++i) {
          unsigned Direction = NewDIDep->getDirection(i);
          if (Direction & Dependence::DVEntry::LT) {
            // If Direction is < then this is a dependence in the same direction
            found = true;
            break;
          } else if (Direction & Dependence::DVEntry::GT) {
            // This means that the dependence is in opposite direction
            for (auto *U : L0IV->users()) {
              if (auto *II = dyn_cast<IntrinsicInst>(U)) {
                if (II->getIntrinsicID() == Intrinsic::hpvm_ivdep) {
                  found = true;
                  break;
                }
              }
            }
            break;
          } else if (!(Direction & Dependence::DVEntry::EQ)) {
            // This is a confused dependence, mark it conservatively
            found = true;
            break;
          }
        }
      }
      if (found) {
        Dep::DType DepType = NewDIDep->isOutput() ? Dep::Output
                             : NewDIDep->isFlow() ? Dep::Flow
                             : NewDIDep->isAnti() ? Dep::Anti
                                                  : Dep::Unknown;
        bool isLoopIndependent = true;
        Dep NewD(Src, Dst, isLoopIndependent, DepType,
                 D.Dir == Dep::Direction::L0ToL1 ? Dep::Direction::L1ToL0
                                                 : Dep::Direction::L0ToL1);
        LLVM_DEBUG(errs() << "Loop Indepedent Dependence became Loop Carried in "
                        "opposite direction or became Loop Carried in forward "
                        "direction with IVDEP!\n";
              errs() << "\tFound Illegal Dependence!\n";
              errs() << "\tBefore Fusion:\n"; D.dump();
              errs() << "\tAfter Fusion:\n"; NewD.dump());
        illegal |= true;
        NumIllegalDepsFound++;
        if (!is_debug)
          return illegal;
      }
    }
  }

  return illegal;
}

void FusionCandidate::update(llvm::Loop *_L) {
  L = _L;
  Exit = _L->getExitBlock();
  Exiting = _L->getExitingBlock();
  Header = _L->getHeader();
  Latch = _L->getLoopLatch();
  Preheader = _L->getLoopPreheader();
  std::vector<Instruction *> LMemOps;
  for (auto BB : L->getBlocks()) {
    for (auto &I : *BB) {
      if (isa<LoadInst>(I) || isa<StoreInst>(I))
        LMemOps.push_back(&I);
    }
  }
  MemOps = LMemOps;
}

BasicBlock *hpvm::getBBbyName(StringRef Name, Function *F) {
  LLVM_DEBUG(errs() << "Finding BB with name: " << Name << "\n");
  for (auto &BB : *F) {
    if (BB.getName() == Name) {
      LLVM_DEBUG(errs() << "Found BB: " << BB);
      return &BB;
    }
  }
  assert(false && "Couldn't find the basic block by name!");
  return nullptr;
}

void DebugCFGChanges(FusionCandidate *FC0, FusionCandidate *FC1) {

  LLVM_DEBUG(errs() << "**************************************************\n");
  for (int i = 0; i < 5; ++i) {
    LLVM_DEBUG(errs() << FC0->Header->getName() << " has " << i << "predecessors? ");
    LLVM_DEBUG(errs() << (FC0->Header->hasNPredecessors(i) ? "yes\n" : "no\n"));
  }
  LLVM_DEBUG(errs() << "Printing predecessors of " << FC0->Header->getName()
               << "\n");
  pred_iterator PI = pred_begin(FC0->Header), E = pred_end(FC0->Header);
  for (; PI != E; ++PI) {
    BasicBlock *BB = *PI;
    LLVM_DEBUG(errs() << BB->getName() << " ");
  }
  LLVM_DEBUG(errs() << "\n");
  for (int i = 0; i < 5; ++i) {
    LLVM_DEBUG(errs() << FC1->Header->getName() << " has " << i << "predecessors? ");
    LLVM_DEBUG(errs() << (FC1->Header->hasNPredecessors(i) ? "yes\n" : "no\n"));
  }
  LLVM_DEBUG(errs() << "Printing predecessors of " << FC1->Header->getName()
               << "\n");
  pred_iterator PI1 = pred_begin(FC1->Header), E1 = pred_end(FC1->Header);
  for (; PI1 != E1; ++PI1) {
    BasicBlock *BB = *PI1;
    LLVM_DEBUG(errs() << BB->getName() << " ");
  }
  LLVM_DEBUG(errs() << "\n");
  LLVM_DEBUG(errs() << "**************************************************\n");
}

/*************************************************************************/
/********************** Helper Functions  ******************************/
/*************************************************************************/
void hpvm::lowerFuncMemCpy(Function *F, const TargetTransformInfo &TTI) {
  std::vector<MemCpyInst *> MemCpyInsts;
  for (auto ii = inst_begin(F), ie = inst_end(F); ii != ie; ++ii) {
    Instruction *I = &*ii;
    if (auto *MemCpyI = dyn_cast<MemCpyInst>(I)) {
      MemCpyInsts.push_back(MemCpyI);
    }
  }
  for (auto *MCI : MemCpyInsts) {
    expandMemCpyAsLoop(MCI, TTI);
    MCI->eraseFromParent();
  }

  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  auto &LI = FAM.getResult<LoopAnalysis>(*F);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  for (auto *L : LI.getLoopsInPreorder()) {
    if (L->getName().contains("load-store-loop")) {
      LLVM_DEBUG(errs() << "Found the memcpy loop, unrolling it!\n");
      auto TripCount = SE.getSmallConstantMaxTripCount(L);
      hpvm::unrollLoopByCount(L, LI, DT, SE, AC, TTI, TripCount, TripCount);
    }
  }
}
