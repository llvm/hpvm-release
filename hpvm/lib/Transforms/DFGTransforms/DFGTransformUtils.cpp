#define DEBUG_TYPE "dfgtransforms"

#include "Transforms/DFGTransforms/DFGTransformUtils.h"

#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "CoreHPVM/DFGUtils.h"
#include "CoreHPVM/DFGraph.h"

namespace hpvm {

// Creates a loop around the body of Function F. Name is used for loop BBs
// and index variable.
PHINode *createLoopFromFunction(Function *F, Instruction *RI, Value *Limit,
                                int Dim, bool InsertIVDEP) {
  std::string Name = "dim." + std::to_string(Dim);
  BasicBlock *EntryBB = &F->getEntryBlock();
  // Create Loop Header and Loop Exit blocks
  Instruction *EntryInst = EntryBB->getFirstNonPHIOrDbgOrLifetime();
  while (isa<AllocaInst>(EntryInst)) {
    EntryInst = EntryInst->getNextNode();
  }
  BasicBlock *LoopHeaderBB =
      EntryBB->splitBasicBlock(EntryInst, "loop." + Name + ".header");
  BasicBlock *ExitBB = RI->getParent();
  BasicBlock *LoopExitBB =
      ExitBB->splitBasicBlock(RI, "loop." + Name + ".exit");

  // Create PHI for index variable
  PHINode *IndVarPHI = PHINode::Create(
      Type::getInt64Ty(EntryInst->getContext()), 2, "index." + Name, EntryInst);
  IndVarPHI->addIncoming(
      ConstantInt::get(Type::getInt64Ty(EntryInst->getContext()), 0), EntryBB);
  if (InsertIVDEP) {
    // Add the ivdep intrinsic
    std::vector<Value *> Args;
    Args.push_back(IndVarPHI);
    Args.push_back(ConstantInt::get(Type::getInt32Ty(F->getContext()), 0));
    int NumArgs = 0;
    for (auto ai = F->arg_begin(); ai != F->arg_end(); ai++) {
      Argument* ArgPtr = &*ai;
      // Only add ivdep for non-private marked pointer arguments
      if (ArgPtr->getType()->isPointerTy()
       && !F->getAttributes().hasAttribute(ai->getArgNo()+1, Attribute::Priv)) {
        Args.push_back(ArgPtr);
        NumArgs++;
      }
    }
    Args[1] = ConstantInt::get(Type::getInt32Ty(F->getContext()), NumArgs);
    IRBuilder IRB(LoopHeaderBB->getFirstNonPHI());
    IRB.CreateIntrinsic(Intrinsic::hpvm_ivdep, ArrayRef<Type *>(),
                        ArrayRef<Value *>(Args));
  }

  // Create increment for index variable
  Instruction *InsertBefore = ExitBB->getTerminator();
  BinaryOperator *IndexInc = BinaryOperator::Create(
      Instruction::Add, IndVarPHI,
      ConstantInt::get(Type::getInt64Ty(EntryInst->getContext()), 1),
      "index." + Name + ".inc", InsertBefore);

  // Compare index variable with limit
  CmpInst *Cond =
      CmpInst::Create(Instruction::ICmp, CmpInst::ICMP_NE, IndexInc, Limit,
                      "cond." + Name, InsertBefore);

  // Replace the terminator instruction of loop exiting block with new
  // conditional branch which loops over body if true and branches to exit
  // otherwise
  BranchInst *BI = BranchInst::Create(LoopHeaderBB, LoopExitBB, Cond);
  ReplaceInstWithInst(InsertBefore, BI);

  // Add incoming edge to phi node for increment of index variable
  IndVarPHI->addIncoming(IndexInc, ExitBB);

  return IndVarPHI;
}

// If F has a unique ReturnInst, returns it, otherwise returns None.
Optional<Instruction *> getUniqueReturnInstruction(Function *F) {
  std::vector<Instruction *> ReturnInsts;
  for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
    if (isa<ReturnInst>(*I)) {
      ReturnInsts.push_back(&*I);
    }
  }
  if (ReturnInsts.size() > 1 || ReturnInsts.size() < 1) {
    return llvm::None;
  }
  return ReturnInsts[0];
}

// Adds a new incoming Edge from entry node to Node and inserts a bindIn
// intrinsic  if addBind is true
DFEdge *addInputEdgeAndBind(DFNode *Node, unsigned SrcPos, unsigned DstPos,
                            Type *ArgType, bool isStreaming,
                            DFGTransform::Context &C, bool addBind) {
  DFNode *Src = Node->getParent()->getChildGraph()->getEntry();
  DFNode *Dst = Node;
  // First add edge to child graph
  DFEdge *NewDFEdge =
      DFEdge::Create(Src, Dst, true, SrcPos, DstPos, ArgType, isStreaming);
  Dst->getParent()->addEdgeToDFGraph(NewDFEdge);

  LLVM_DEBUG(errs() << "Added Edge:\n");
  LLVM_DEBUG(errs() << "-- Src: "
               << NewDFEdge->getSourceDF()->getFuncPointer()->getName() << " : "
               << NewDFEdge->getSourcePosition() << "\n-- Dst: "
               << NewDFEdge->getDestDF()->getFuncPointer()->getName() << " : "
               << NewDFEdge->getDestPosition() << "\n");

  if (addBind) {
    // Next create bindin
    Function *BindInF =
        Intrinsic::getDeclaration(&C.getModule(), Intrinsic::hpvm_bind_input);
    Value *BindInArgs[]{
        Node->getInstruction(),
        ConstantInt::get(Type::getInt32Ty(C.getLLVMContext()), SrcPos),
        ConstantInt::get(Type::getInt32Ty(C.getLLVMContext()), DstPos),
        ConstantInt::get(Type::getInt1Ty(C.getLLVMContext()), isStreaming)};

    CallInst *CI = CallInst::Create(
        BindInF, BindInArgs, "",
        Node->getParent()->getFuncPointer()->getEntryBlock().getTerminator());

    LLVM_DEBUG(errs() << "Added Bind In:" << *CI << "\n");
  }
  return NewDFEdge;
}

// Adds a new incoming Edge from Node to exit node and inserts a bindOut
// intrinsic  if addBind is true
DFEdge *addOutputEdgeAndBind(DFNode *Node, unsigned SrcPos, unsigned DstPos,
                             Type *ArgType, bool isStreaming,
                             DFGTransform::Context &C, bool addBind) {
  DFNode *Src = Node;
  DFNode *Dst = Node->getParent()->getChildGraph()->getExit();
  // First add edge to child graph
  DFEdge *NewDFEdge =
      DFEdge::Create(Src, Dst, true, SrcPos, DstPos, ArgType, isStreaming);
  Dst->getParent()->addEdgeToDFGraph(NewDFEdge);

  LLVM_DEBUG(errs() << "Added Edge:\n");
  LLVM_DEBUG(errs() << "-- Src: "
               << NewDFEdge->getSourceDF()->getFuncPointer()->getName() << " : "
               << NewDFEdge->getSourcePosition() << "\n-- Dst: "
               << NewDFEdge->getDestDF()->getFuncPointer()->getName() << " : "
               << NewDFEdge->getDestPosition() << "\n");

  if (addBind) {
    // Next create bindout
    Function *BindOutF =
        Intrinsic::getDeclaration(&C.getModule(), Intrinsic::hpvm_bind_output);
    Value *BindOutArgs[]{
        Node->getInstruction(),
        ConstantInt::get(Type::getInt32Ty(C.getLLVMContext()), SrcPos),
        ConstantInt::get(Type::getInt32Ty(C.getLLVMContext()), DstPos),
        ConstantInt::get(Type::getInt1Ty(C.getLLVMContext()), isStreaming)};

    CallInst *CI = CallInst::Create(
        BindOutF, BindOutArgs, "",
        Node->getParent()->getFuncPointer()->getEntryBlock().getTerminator());

    LLVM_DEBUG(errs() << "Added Bind In:" << *CI << "\n");
  }
  return NewDFEdge;
}

// Clones the function while adding NewArgs from input vector to existing
// arguments and deletes the old function
Function *updateFuncWithNewArgs(Function *F, std::vector<Argument *> &NewArgs,
                                std::vector<Type *> &NewArgTypes, ValueToValueMapTy &VMap) {
  assert(NewArgs.size() == NewArgTypes.size() &&
         "Invalid NewArgs and NewArgTypes provided!");
  std::vector<Argument *> NewFuncArgs;
  std::vector<Type *> NewFuncArgTypes;
  for (auto &a : F->args()) {
    Argument *Arg = &a;
    NewFuncArgs.push_back(Arg);
    NewFuncArgTypes.push_back(a.getType());
  }
  for (int i = 0; i < NewArgs.size(); ++i) {
    NewFuncArgs.push_back(NewArgs[i]);
    NewFuncArgTypes.push_back(NewArgTypes[i]);
  }
  FunctionType *newFT =
      FunctionType::get(F->getReturnType(), NewFuncArgTypes, 0);
  Function *NewF = hpvmUtils::cloneFunction(F, newFT, 0, VMap, NULL, &NewFuncArgs);
  F->replaceAllUsesWith(UndefValue::get(F->getType()));
  // F->eraseFromParent();
  return NewF;
}

// Function that replaces a perfectly nested parent/child node with only the
// child node
void replaceParentWithChild(DFNode *N) {
  DFInternalNode *ParentNode = N->getParent();

  // Modify child input edges so that their sources point to the source
  // of the equivalent parent's input edges.
  for (auto E = N->indfedge_begin(); E != N->indfedge_end(); E++) {
    DFEdge *Edge = *E;

    LLVM_DEBUG(errs() << "\n\nBefore Modification: ");
    LLVM_DEBUG(Edge->dump());

    unsigned SrcPos = Edge->getSourcePosition();
    DFNode *NewSrcNode = ParentNode->getInDFEdgeAt(SrcPos)->getSourceDF();
    unsigned NewSrcPos = ParentNode->getInDFEdgeAt(SrcPos)->getSourcePosition();
    bool isStreaming = Edge->isStreamingEdge() ||
                       ParentNode->getInDFEdgeAt(SrcPos)->isStreamingEdge();
    bool EdgeType =
        Edge->getEdgeType() || ParentNode->getInDFEdgeAt(SrcPos)->getEdgeType();

    Edge->setSourceDF(NewSrcNode);
    Edge->setSourcePosition(NewSrcPos);
    Edge->setStreamingEdge(isStreaming);
    Edge->setEdgeType(EdgeType);

    LLVM_DEBUG(errs() << "After Modification: ");
    LLVM_DEBUG(Edge->dump());
  }
  // Modify child output edges so that their destinations point to the
  // destination of the equivalent parent's output edges.
  for (auto E = N->outdfedge_begin(); E != N->outdfedge_end(); E++) {
    DFEdge *Edge = *E;

    LLVM_DEBUG(errs() << "\n\nBefore Modification: ");
    LLVM_DEBUG(Edge->dump());

    unsigned DstPos = Edge->getDestPosition();
    DFNode *NewDstNode = ParentNode->getOutDFEdgeAt(DstPos)->getDestDF();
    unsigned NewDstPos = ParentNode->getOutDFEdgeAt(DstPos)->getDestPosition();
    bool isStreaming = Edge->isStreamingEdge() ||
                       ParentNode->getOutDFEdgeAt(DstPos)->isStreamingEdge();
    bool EdgeType = Edge->getEdgeType() ||
                    ParentNode->getOutDFEdgeAt(DstPos)->getEdgeType();

    N->removeSuccessor(Edge->getDestDF());
    Edge->setDestDF(NewDstNode);
    Edge->setDestPosition(NewDstPos);
    Edge->setStreamingEdge(isStreaming);
    Edge->setEdgeType(EdgeType);

    LLVM_DEBUG(errs() << "After Modification: ");
    LLVM_DEBUG(Edge->dump());
  }

  // Change child node's parent node
  N->setParent(ParentNode->getParent());
  ParentNode->getParent()->addChildToDFGraph(N);
  ParentNode->removeChildFromDFGraph(N);

  // Add all child edges to new parent
  for (auto E = N->indfedge_begin(); E != N->indfedge_end(); E++) {
    DFEdge *Edge = *E;
    ParentNode->getParent()->addEdgeToDFGraph(Edge);
  }
  for (auto E = N->outdfedge_begin(); E != N->outdfedge_end(); E++) {
    DFEdge *Edge = *E;
    ParentNode->getParent()->addEdgeToDFGraph(Edge);
  }

  // set current node's parent to its grandfather
  N->setParent(ParentNode->getParent());

  // remove child node
  hpvmUtils::removeNodeEdges(*ParentNode);
  ParentNode->getParent()->getChildGraph()->removeChildDFNode(ParentNode);

  IntrinsicInst *OldCreateNode = N->getInstruction();
  IntrinsicInst *InsertionPoint = ParentNode->getInstruction();
  Function *OldINFunc = ParentNode->getFuncPointer();

  // Invalidate current uses of OldCreateNode
  OldCreateNode->replaceAllUsesWith(UndefValue::get(OldCreateNode->getType()));
  // Replace all uses of InsertionPoint with uses of OldCreateNode 
  InsertionPoint->replaceAllUsesWith(OldCreateNode);
  // Add CreateNode for LeafNode at InsertionPoint
  OldCreateNode->moveAfter(InsertionPoint);
  // Remove current declaration of InsertionPoint
  if (InsertionPoint->getParent() != NULL)
    InsertionPoint->eraseFromParent();
  // Remove old internal node function
  OldINFunc->replaceAllUsesWith(UndefValue::get(OldINFunc->getType()));
  if (OldINFunc->getParent() != NULL)
    OldINFunc->eraseFromParent();
}

// Function that takes a Node and changes its demension to 1 with replciation
// factor of 1
void changeDimToOne(DFNode *N) {
  N->setNumOfDim(1);
  std::vector<Value *> NewDimLimits;
  NewDimLimits.push_back(ConstantInt::get(N->getDimLimits()[0]->getType(), 1));
  N->setDimLimits(NewDimLimits);
}

llvm::Function *makeEmptyFunction(llvm::FunctionType *FTy,
                                  llvm::GlobalValue::LinkageTypes Linkage,
                                  llvm::Module *M, const llvm::Twine &name) {
  using namespace llvm;

  Function *F = llvm::Function::Create(
      FTy, GlobalValue::LinkageTypes::InternalLinkage, name, M);

  // Create empty basic block with dummy return
  BasicBlock *B = BasicBlock::Create(F->getContext(), "entry", F);
  ReturnInst::Create(F->getContext(), UndefValue::get(F->getReturnType()), B);

  assert(!F->hasExternalLinkage());

  return F;
}

bool isNodeSingular(DFNode *N) {
  bool Singular = true;
  for (auto *V : N->getDimLimits()) {
    if (ConstantInt *ConstV = dyn_cast<ConstantInt>(V)) {
      if (ConstV->getSExtValue() != 1) {
        Singular = false;
      }
    } else {
      Singular = false;
    }
  }
  return Singular;
}

} // namespace hpvm
