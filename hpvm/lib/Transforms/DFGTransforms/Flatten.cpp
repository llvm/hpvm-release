//===-------------------------- Flatten.cpp
//------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that flattens an internal node into it's 
// hierarchical leaf node by adding a new dimension (with dimLimit x) 
// to the leaf node while removing the same dimension (dimLimit x)
// from the internal node 
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "dfgflatten"
#include "Transforms/DFGTransforms/Flatten.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

using namespace hpvm;
bool NodeFlattener::canRun() const {
  if (TargetNode->isRoot()) {
    LLVM_DEBUG(errs() << "Skipping flattening of root node!");
    return false;
  }
  DFGraph *ChildGraph = TargetNode->getChildGraph();  
  ChildGraph->sortChildren();
  int numLeafNodes = 0;
  for (auto ci = ChildGraph->begin(), ce = ChildGraph->end(); ci != ce; ci++) {
    DFNode *C  = *ci;
    if (C->isDummyNode()) 
      continue;
    if (C->getKind() == DFNode::LeafNode) {
      numLeafNodes++;
    } if (C->getKind() == DFNode::InternalNode) {
      LLVM_DEBUG(errs() << "Cannot flatten InternalNode having an InternalNode child\n");
      return false;
    }
  }
  if (numLeafNodes > 1) {
    LLVM_DEBUG(errs() << "Cannot flatten InternalNode with multiple LeafNode children\n");
    return false;
  }
  if (TargetNode->getParent()->isRoot()) {
    LLVM_DEBUG(errs() << "Cannot flatten InternalNodes which are direct children of RootNode\n");
    return false;
  }
  return true;       
}

void NodeFlattener::run() {
  LLVM_DEBUG(errs() << "\nNODE FLATTENING PASS\n");
  DFGraph *ChildGraph = TargetNode->getChildGraph();
  ChildGraph->sortChildren();
  int numLeafNodes = 0;
  for (auto ci = ChildGraph->begin(), ce = ChildGraph->end(); ci != ce; ci++) {
    DFNode *C = *ci;
    if (C->isDummyNode())
      continue;
    if (C->getKind() == DFNode::LeafNode) {
      numLeafNodes++;  
      ChildNode = dyn_cast<DFLeafNode>(C); 
    }  
  }
  unsigned numDimTarget = TargetNode->getNumOfDim(), numDimChild = ChildNode->getNumOfDim();
  assert(numLeafNodes == 1 && "Cannot flatten InternalNode with multiple LeafNode children\n");
  assert(numDimChild < 3 && "Cannot flatten InternalNode whose LeafNode has 3 or more dims\n");

  DimLimitsTarget = TargetNode->getDimLimits(); 
  DimLimitsChild = ChildNode->getDimLimits();
  if (numDimTarget == 0) {
    replaceParentWithChild(ChildNode);
    TargetNode->setFlattened(true);    
  } else {
    std::vector<Value *> NewDimLimitsTarget, NewDimLimitsChild;
    NewDimLimitsTarget.reserve(3);
    NewDimLimitsChild.reserve(3);
    NewDimLimitsChild.insert(NewDimLimitsChild.end(), DimLimitsChild.begin(), DimLimitsChild.end());
    NewDimLimitsChild.push_back(DimLimitsTarget[numDimTarget-1]);
    ChildNode->setDimLimits(NewDimLimitsChild);
    NewDimLimitsTarget.insert(NewDimLimitsTarget.end(), DimLimitsTarget.begin(), DimLimitsTarget.end()-1);
    TargetNode->setDimLimits(NewDimLimitsTarget);
    if (numDimTarget == 1) {
      replaceParentWithChild(ChildNode);
      TargetNode->setFlattened(true);
    }
  } 
}