//==------------ Tile.cpp - Implements HPVM tiling -------------------------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that performs tiling on an HPVM DFG node.
//
//===----------------------------------------------------------------------===//

#include "Transforms/DFGTransforms/Tile.h"

#include "llvm/IR/Constant.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"

#define DEBUG_TYPE "tile"

#include "Transforms/DFGTransforms/DFGTransformUtils.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "CoreHPVM/DFGraph.h"
#include "CoreHPVM/DFGUtils.h"
#include "CoreHPVM/HPVMUtils.h"

namespace hpvm {

// Inserts the computation of the tile node's bounds (in the parent node)
// Returns a map from dim to value (available within the parent node)
static llvm::DenseMap<unsigned, llvm::Value*>
computeTileCounts(llvm::BasicBlock &BB,
  const std::vector<Value*> &OrigCounts,
  const llvm::DenseMap<unsigned, llvm::Value*> &BlockSizes) {

  using namespace llvm;
  DenseMap<unsigned, Value*> Result;

  const Intrinsic::ID InstanceGetterID = Intrinsic::hpvm_getNodeInstanceID_x;
  Function *InstanceIdF = Intrinsic::getDeclaration(BB.getModule(),
    InstanceGetterID);
  IntegerType *CountType = cast<IntegerType>(InstanceIdF->getReturnType());

  IRBuilder<> Builder(BB.getTerminator());
  for (const auto &Pair : BlockSizes) {
    unsigned Dim = Pair.getFirst();
    Value *Div = Builder.CreateUDiv(OrigCounts[Dim], Pair.getSecond());
    Value *Mod = Builder.CreateURem(OrigCounts[Dim], Pair.getSecond());
    Value *Uneven = Builder.CreateIntCast(
      Builder.CreateICmpNE(Mod, ConstantInt::get(CountType, 0)), CountType, false);
    Value *BlockCount = Builder.CreateAdd(Div, Uneven);
    Result[Dim] = BlockCount;
  }

  return Result;
}
  
// Inserts the computation of the leaf's bounds (in the tile node)
// Returns a map from dim to value (available within the tile node)
static llvm::DenseMap<unsigned, llvm::Value*>
computeLeafBounds(llvm::BasicBlock &BB, const Tile::BlockSizesType &BlockSizes) {
  using namespace llvm;
  DenseMap<unsigned, Value*> Result;

  const Intrinsic::ID InstanceGetterID = Intrinsic::hpvm_getNodeInstanceID_x;
  Function *InstanceIdF = Intrinsic::getDeclaration(BB.getModule(),
    InstanceGetterID);
  IntegerType *CountType = cast<IntegerType>(InstanceIdF->getReturnType());
  
  // Currently just leave it constant
  for (const auto &Pair : BlockSizes) {
    Result[Pair.getFirst()] =
      ConstantInt::get(CountType, Pair.getSecond());
  }

  return Result;
}

static void InsertNodeCall(llvm::BasicBlock &BB, llvm::DFNode &Node,
  const std::vector<llvm::Value*> &Bounds) {
  using namespace llvm;
  Module *M = BB.getModule();
  const unsigned DimCount = Bounds.size();

  Constant *FP = ConstantExpr::getPointerCast(
    Node.getFuncPointer(), Type::getInt8PtrTy(M->getContext()));

  Function *CreateNodeFunc = nullptr;
  switch (DimCount) {
  case 1:
    CreateNodeFunc =
      Intrinsic::getDeclaration(M, Intrinsic::hpvm_createNode1D);
    break;
  case 2:
    CreateNodeFunc =
      Intrinsic::getDeclaration(M, Intrinsic::hpvm_createNode2D);
    break;
  case 3:
    CreateNodeFunc =
      Intrinsic::getDeclaration(M, Intrinsic::hpvm_createNode3D);
    break;
  default:
    assert(false && "Unhandled dim count");
  }
  assert(CreateNodeFunc);

  std::vector<Value*> Args;
  Args.push_back(FP);
  for (unsigned Dim = 0; Dim < DimCount; ++Dim)
    Args.push_back(Bounds[Dim]);

  CallInst *CI = CallInst::Create(CreateNodeFunc, Args,
    Node.getFuncPointer()->getName(), BB.getTerminator());
}

// Inserts the computation of the leaf's index after tiling, and returns
// a value that has the computed index.
// Should be BlockId * BlockSize + LeafId
static llvm::DenseMap<unsigned, llvm::Value*>
computeLeafIndex(llvm::BasicBlock &BB, const Tile::BlockSizesType &BlockSizes) {
  using namespace llvm;

  Function *GetNodeF = Intrinsic::getDeclaration(BB.getModule(),
    Intrinsic::hpvm_getNode);
  Function *GetParentNodeF = Intrinsic::getDeclaration(BB.getModule(),
    Intrinsic::hpvm_getParentNode);

  FunctionType *GetNodeTy = GetNodeF->getFunctionType();
  FunctionType *GetParentNodeTy = GetParentNodeF->getFunctionType();

  // Get relevant nodes for ID-getting
  Instruction *EntryInst = BB.getFirstNonPHIOrDbgOrLifetime();
  while (isa<AllocaInst>(EntryInst)) {
    EntryInst = EntryInst->getNextNode();
  }
  IRBuilder<> Builder(EntryInst);
  CallInst *LeafNode = Builder.CreateCall(GetNodeTy, GetNodeF);
  CallInst *BlockNode =
    Builder.CreateCall(GetParentNodeTy, GetParentNodeF, LeafNode);

  // Actually compute the values
  constexpr unsigned MaxDims = 3;
  DenseMap<unsigned, llvm::Value*> Result;
  for (unsigned Dim = 0; Dim < MaxDims; ++Dim) {
    if (BlockSizes.count(Dim) == 0) continue; // Not mined
    Intrinsic::ID InstanceGetterID;
    Intrinsic::ID DimGetterID;
    switch (Dim) {
    case 0:
      InstanceGetterID = Intrinsic::hpvm_getNodeInstanceID_x;
      DimGetterID = Intrinsic::hpvm_getNumNodeInstances_x;
      break;
    case 1:
      InstanceGetterID = Intrinsic::hpvm_getNodeInstanceID_y;
      DimGetterID = Intrinsic::hpvm_getNumNodeInstances_y;
      break;
    case 2:
      InstanceGetterID = Intrinsic::hpvm_getNodeInstanceID_z;
      DimGetterID = Intrinsic::hpvm_getNumNodeInstances_z;
      break;
    default:
      assert(false && "Unsupported tiling dimension");
    }

    Function *InstanceIdF = Intrinsic::getDeclaration(BB.getModule(),
      InstanceGetterID);
    FunctionType *InstanceIdTy = InstanceIdF->getFunctionType();

    Function *DimF = Intrinsic::getDeclaration(BB.getModule(),
      DimGetterID);
    FunctionType *DimTy = DimF->getFunctionType();

    CallInst *LeafId = Builder.CreateCall(InstanceIdTy, InstanceIdF, LeafNode);
    CallInst *BlockId =
      Builder.CreateCall(InstanceIdTy, InstanceIdF, BlockNode);
    Value *BlockSize = Builder.CreateCall(DimTy, DimF, LeafNode);
    Value *BlockOffset = Builder.CreateMul(BlockId, BlockSize);
    Result[Dim] = Builder.CreateAdd(BlockOffset, LeafId);
  }

  return Result;
}

// Inserts calls to get the original of the current node, from before nesting.
static llvm::Value *getOrigParent(llvm::BasicBlock &BB) {
  using namespace llvm;

  Function *GetNodeF = Intrinsic::getDeclaration(BB.getModule(),
    Intrinsic::hpvm_getNode);
  Function *GetParentNodeF = Intrinsic::getDeclaration(BB.getModule(),
    Intrinsic::hpvm_getParentNode);

  FunctionType *GetNodeTy = GetNodeF->getFunctionType();
  FunctionType *GetParentNodeTy = GetParentNodeF->getFunctionType();

  Instruction *EntryInst = BB.getFirstNonPHIOrDbgOrLifetime();
  while (isa<AllocaInst>(EntryInst)) {
    EntryInst = EntryInst->getNextNode();
  }
  IRBuilder<> Builder(EntryInst);
  CallInst *LeafNode = Builder.CreateCall(GetNodeTy, GetNodeF);
  CallInst *BlockNode =
    Builder.CreateCall(GetParentNodeTy, GetParentNodeF, LeafNode);

  return Builder.CreateCall(GetParentNodeTy, GetParentNodeF, BlockNode);
}

// Calculates the depth of each leaf in the graph hierarchy
class TileCheckVisitor final : public DFNodeVisitor {
public:
  void visit(DFLeafNode *Node) override;
};

namespace {

using namespace llvm;

struct TileCheckData {
  // Old getParentNode()s that should be replaced with the grandparent
  std::vector<Value*> GetParentsToReplace;
  // Old getInstanceId_*() that should be replaced with tiled equivalent
  // Second value is index of the dimension (0 for x, 1 for y, 2 for z)
  std::vector<std::pair<CallInst*, unsigned>> InstanceIdsToReplace;
  // Old getNumNodeInstances_*() to replace with leaf node total
  std::vector<std::pair<CallInst*, unsigned>> NumNodeInstancesToReplace;
  const char *error = nullptr; // null = no error

  bool isGood() const { return error == nullptr; }
};

}

enum class NodeSource {
  NODE,
  PARENT,
  OTHER
};

static TileCheckData getTileCheckData(llvm::Function *F,
  const Tile::BlockSizesType &BlockSizes) {
  TileCheckData Data;

  Module *M = F->getParent();
  Function *GetNodeF = Intrinsic::getDeclaration(M, Intrinsic::hpvm_getNode);
  Function *GetParentNodeF =
    Intrinsic::getDeclaration(M, Intrinsic::hpvm_getParentNode);
  Function *InstanceIdXF = Intrinsic::getDeclaration(M,
    Intrinsic::hpvm_getNodeInstanceID_x);
  Function *InstanceIdYF = Intrinsic::getDeclaration(M,
    Intrinsic::hpvm_getNodeInstanceID_y);
  Function *InstanceIdZF = Intrinsic::getDeclaration(M,
    Intrinsic::hpvm_getNodeInstanceID_z);
  Function *NumNodeInstanceXF = Intrinsic::getDeclaration(M,
    Intrinsic::hpvm_getNumNodeInstances_x);
  Function *NumNodeInstanceYF = Intrinsic::getDeclaration(M,
    Intrinsic::hpvm_getNumNodeInstances_y);
  Function *NumNodeInstanceZF = Intrinsic::getDeclaration(M,
    Intrinsic::hpvm_getNumNodeInstances_z);

  std::vector<Value*> GetNodeValues;
  std::vector<CallInst*> GetParents;
  std::vector<std::pair<CallInst*, unsigned>> InstanceIds;
  std::vector<std::pair<CallInst*, unsigned>> NumNodeInstances;

  for (inst_iterator It = inst_begin(F), E = inst_end(F); It != E; ++It) {
    if (auto *Call = dyn_cast<CallInst>(&*It)) {
      Function *CallF = Call->getCalledFunction();
      if (CallF == GetNodeF)
        GetNodeValues.push_back(Call);
      else if (CallF == InstanceIdXF)
        InstanceIds.emplace_back(Call, 0);
      else if (CallF == InstanceIdYF)
        InstanceIds.emplace_back(Call, 1);
      else if (CallF == InstanceIdZF)
        InstanceIds.emplace_back(Call, 2);
      else if (CallF == GetParentNodeF)
	GetParents.push_back(Call);
      else if (CallF == NumNodeInstanceXF)
	NumNodeInstances.emplace_back(Call, 0);
      else if (CallF == NumNodeInstanceYF)
	NumNodeInstances.emplace_back(Call, 1);
      else if (CallF == NumNodeInstanceZF)
	NumNodeInstances.emplace_back(Call, 2);
    }
  }

  // Function that checks if call argument comes from getNode or getParent
  // Only checks the first arg
  auto getArgSource = [&GetNodeValues, &GetParents](CallInst *Call) {
    Value *Arg = Call->getArgOperand(0);

    LLVM_DEBUG(errs() << "Call on " << *Arg << "\n");
    
    auto NodeIt = std::find(GetNodeValues.begin(), GetNodeValues.end(), Arg);

    if (NodeIt != GetNodeValues.end())
      return NodeSource::NODE;

    auto ParentIt = std::find(GetParents.begin(), GetParents.end(), Arg);
    return ParentIt != GetParents.end() ? NodeSource::PARENT : NodeSource::OTHER;
  };

  // Validate GetParents and find those to replace
  for (CallInst *GetParent : GetParents) {
    NodeSource Source = getArgSource(GetParent);

    switch(Source) {
    case NodeSource::NODE:
      Data.GetParentsToReplace.push_back(GetParent);
      break;
    
    case NodeSource::PARENT:
      break;

    default:
      Data.error = "Unsupported getNodeParent operand";
      return Data;
    }
  }

  // Validate instance IDs and find those to replace
  for (const auto &Pair : InstanceIds) {
    CallInst *Call = Pair.first;
    unsigned Dim = Pair.second;
    LLVM_DEBUG(errs() << "Found instance ID get: " << *Call << "\n");
    if (BlockSizes.count(Dim) == 0){
      LLVM_DEBUG(errs() << "Not mining this dim.\n");
      continue;
    }
    

    NodeSource Source = getArgSource(Call);
    switch(Source) {
    case NodeSource::NODE:
      Data.InstanceIdsToReplace.push_back(Pair);
      LLVM_DEBUG(errs() << "Was node, replace\n");
      break;
    
    case NodeSource::PARENT:
      LLVM_DEBUG(errs() << "Was parent, don't replace\n");
      break;

    default:
      Data.error = "Unsupported getInstanceId_* operand";
      LLVM_DEBUG(errs() << "Was unsupported operand\n");
      return Data;
    }
  }

  // Validate getNumDims_* and find those to replace
  for (const auto &Pair : NumNodeInstances) {
    CallInst *Call = Pair.first;
    unsigned Dim = Pair.second;
    if (BlockSizes.count(Dim) == 0) continue; // Like above

    NodeSource Source = getArgSource(Call);
    switch(Source) {
    case NodeSource::NODE:
      Data.NumNodeInstancesToReplace.push_back(Pair);
      break;
    
    case NodeSource::PARENT:
      break;

    default:
      Data.error = "Unsupported getNumNodeInstances_* operand";
      return Data;
    }
  }

  return Data;
}

// Moves a struct constructed only of arguments or constants as primitives
// Return nullptr if couldn't move, otherwise returns the struct value
static Value *tryToMoveStruct(llvm::InsertValueInst *Struct, Instruction *Pos) {
  // First enumerate the slice this instruction depends on, to make sure it's
  // movable
  // Will be enumerated in use-def order
  std::vector<Instruction*> Slice;
  {
    std::vector<Value*> Worklist {Struct};
    while (!Worklist.empty()) {
      Value *V = Worklist.back();
      Worklist.pop_back();

      if (isa<Argument>(V) || isa<Constant>(V))
	continue;

      if (InsertValueInst *Insert = dyn_cast<InsertValueInst>(V)) {
	Slice.push_back(Insert);
        Worklist.push_back(Insert->getAggregateOperand());
        Worklist.push_back(Insert->getInsertedValueOperand());
        continue;
      }

      // No other values are allowed
      return nullptr;
    }
  }

  // If we got here, this is a valid struct to move. Move the instructions in
  // in use-def order.
  Instruction *Successor = Pos;
  for (Instruction *I : Slice) {
    I->moveBefore(Successor);
    Successor = I;
  }

  return Struct;
}

// Finds a value suitable for early return, one that doesn't depend on
// computations in the leaf, either a constant, a function argument, or a struct
// value constructed from arguments or constants.
// Returns the value found, or nullptr if not found.
//
// May move instructions to the entry block in order to make them available
// early.
static Value *getEarlyReturnValue(llvm::Function *F,
  std::vector<ReturnInst*>& RetInsts) {
  using namespace llvm;

  BasicBlock &Entry = F->getEntryBlock();

  Value* ReturnThis = nullptr;
  // Find a return instruction that can be used for default return
  for (inst_iterator It = inst_begin(F), E = inst_end(F); It != E; ++It) {
    if (ReturnInst *RI = dyn_cast<ReturnInst>(&*It)) {
      RetInsts.push_back(RI);
      Value *ReturnValue = RI->getReturnValue();
      if (isa<Argument>(ReturnValue) || isa<Constant>(ReturnValue)) {
        ReturnThis = ReturnValue;
      }

      if (InsertValueInst *Struct = dyn_cast<InsertValueInst>(ReturnValue)) {
	Value *ReturnValue = tryToMoveStruct(Struct, Entry.getTerminator());
        ReturnThis = ReturnValue;
      }
    }
  }

  return ReturnThis;
}

// Makes modifications to the leaf node function as necessary:
// * Replace intrinsics so that their semantics match the pre-nesting graph.
// ** Currently replaced: getParentNode, getNodeInstanceID_x,
//    getNumNodeInstances_x
// * Add an early return in case the leaf is in a block where not all instances
//   are needed.
static void modifyLeafFunction(DFLeafNode &Node,
  const Tile::BlockSizesType& BlockSizes,
  const std::vector<Value*>& InstanceCounts) {
  
  using namespace llvm;

  Function *F = Node.getFuncPointer();
  BasicBlock &Entry = F->getEntryBlock();
  Instruction *EntryInst = Entry.getFirstNonPHIOrDbgOrLifetime();
  while (isa<AllocaInst>(EntryInst)) {
    EntryInst = EntryInst->getNextNode();
  }
  BasicBlock *OldEntry = Entry.splitBasicBlock(EntryInst, "old.entry");
  Module *M = F->getParent();

  TileCheckData Data = getTileCheckData(F, BlockSizes);
  if (!Data.isGood()) {
    errs() << Data.error << "\n";
    assert(false && "Bad leaf function intrinsic data");
  }

  // Insert index and original parent computation
  DenseMap<unsigned, Value*> FixedIds =
    computeLeafIndex(Entry, BlockSizes);
  Value *OriginalParent =
    getOrigParent(Entry);

  // Replaces uses of getParentNode with the original parent
  for (Value *GetParent : Data.GetParentsToReplace)
    GetParent->replaceAllUsesWith(OriginalParent);

  // Insert uses of FixedIds where the index of GetNode was gotten
  for (auto &Pair : Data.InstanceIdsToReplace) {
    LLVM_DEBUG(errs() << "REPLACING: " << *Pair.first << "\n");
    Pair.first->replaceAllUsesWith(FixedIds[Pair.second]);
  }

  // Use InstanceCount where number of nodes was used originally
  for (auto &Pair : Data.NumNodeInstancesToReplace)
    Pair.first->replaceAllUsesWith(InstanceCounts[Pair.second]);

  std::vector<ReturnInst*> RetInsts;
  Value *ReturnVal = getEarlyReturnValue(F, RetInsts);
  assert(ReturnVal && "Need constant or argument return in leaf node");

  // Add the actual return block, using the instruction we wanted to copy
  BasicBlock *EarlyReturn = BasicBlock::Create(F->getContext(), "early.return",
    F, OldEntry);
  {
    IRBuilder<> Builder(EarlyReturn);
    Builder.CreateRet(ReturnVal);
  }

  // Do the branching
  Entry.getTerminator()->eraseFromParent();
  IRBuilder<> Builder(&Entry);
  Value *IdValid = ConstantInt::getTrue(IntegerType::get(M->getContext(), 1));
  for (auto &Pair : FixedIds) {
    Value *DimValid =
      Builder.CreateICmpULT(Pair.second, InstanceCounts[Pair.first]);
    IdValid = Builder.CreateAnd(IdValid, DimValid);
  }
  Builder.CreateCondBr(IdValid, OldEntry, EarlyReturn);

  // Replace existing return instructions (single return required for
  // some later passes)
  assert(RetInsts.size() == 1);
  for (ReturnInst *RetInst : RetInsts) {
    IRBuilder<> Builder(RetInst);
    Builder.CreateBr(EarlyReturn);
    RetInst->eraseFromParent();
  }
  
  LLVM_DEBUG(errs() << "New entry:\n" << Entry);
}

// Sort of annoying but T should be the wider type
template <typename T, typename W>
T ceilDiv(T x, W y) {
  return x/y + (x % y != 0);
}

// Returns the instance counts, available in the nest and leaf functions.
// Needs to be called before leaf dimensions are modified, to get correct results
// Invalidates Function pointer of all arguments.
static std::pair<std::vector<Value*>, std::vector<Value*>>
threadInstanceCount(DFInternalNode &Nest, DFLeafNode &LeafNode,
  const std::vector<Value*> &OrigCounts) {
  using namespace llvm;

  assert(LeafNode.getParent() == &Nest);

  // Thread dims into nest
  // Argument positions of threaded args, to overwrite later
  DenseMap<unsigned,unsigned> NestPos;
  DenseMap<unsigned,unsigned> LeafPos;

  // Instance counts available in nest and leaf
  std::vector<Value*> NestCounts;
  std::vector<Value*> LeafCounts;
  unsigned Pos = 0;
  for (Value *Dim : OrigCounts) {
    Twine PosS(Pos);
    if (Argument *Arg = dyn_cast<Argument>(Dim)) {
      assert(Arg->getParent() == Nest.getParent()->getFuncPointer());

      // This arg will get invalidated later so we need to note it down
      NestPos[Pos] = hpvmUtils::getArgCount(Nest.getFuncPointer());
      LeafPos[Pos] = hpvmUtils::getArgCount(LeafNode.getFuncPointer());
    } else {
      assert(isa<ConstantInt>(Dim));
    }
    Value *NestDim =
      hpvmUtils::threadValue(Nest, Dim, Twine("count.nest.") + PosS);
    Value *LeafDim =
      hpvmUtils::threadValue(LeafNode, NestDim, Twine("count.leaf.") + PosS);
      NestCounts.push_back(NestDim);
      LeafCounts.push_back(LeafDim);
    
    ++Pos;
  }

  // Fix invalidated args
  for (auto &[Dim, ArgPos] : NestPos)
    NestCounts[Dim] = Nest.getFuncPointer()->arg_begin() + ArgPos;
  for (auto &[Dim, ArgPos] : LeafPos)
    LeafCounts[Dim] = LeafNode.getFuncPointer()->arg_begin() + ArgPos;

  return std::make_pair(NestCounts, LeafCounts);
}

// The dimension to tile must exist in the node, and if the dim size
// is known statically, the block size must be smaller.
static bool validBlockSize(DFNode* N, unsigned Dim, size_t BlockSize) {
  if (Dim >= N->getNumOfDim()) return false;
  if (auto *CInt = dyn_cast<ConstantInt>(N->getDimLimits()[Dim]))
    return BlockSize < CInt->getZExtValue();
  else
    return true; // It wasn't constant so we can't check the dimension size
}


bool Tile::canRun() const {
  // Check that the data required for replacing intrinsics is ok
  TileCheckData Data =
    getTileCheckData(TargetNode.getFuncPointer(), BlockSizes);
  if (!Data.isGood()) {
    LLVM_DEBUG(errs() << "Tile can't run: " << Data.error << "\n");
    return false;
  }
  

  // Check that dimensions requested for tiling are valid
  for (const auto &Pair : BlockSizes)
    if (!validBlockSize(&TargetNode, Pair.getFirst(), Pair.getSecond()))
      return false;

  return true;
}

void Tile::run() {
  using namespace llvm;

  StringRef FName = TargetNode.getFuncPointer()->getName();

  // Will change so need to save
  const std::vector<Value*> OrigCounts = TargetNode.getDimLimits();
  BasicBlock &ParentEntry =
    TargetNode.getParent()->getFuncPointer()->getEntryBlock();

  // Do all nesting
  DFInternalNode &Nest = hpvmUtils::nestNode(&TargetNode, FName + "_nest");
  BasicBlock &NestEntry = Nest.getFuncPointer()->getEntryBlock();

  // Insert tile bound computation
  DenseMap<unsigned, Value*> TileSizes = computeLeafBounds(NestEntry,
    BlockSizes);
  DenseMap<unsigned, Value*> TileSizesParent = computeLeafBounds(ParentEntry,
    BlockSizes);
  
  // Insert tile count computation
  DenseMap<unsigned, Value*> TileCounts =
    computeTileCounts(ParentEntry, OrigCounts, TileSizesParent);

  // This will invalidate all references to functions, BB, etc of the
  // nesting and leaf nodes
  auto [NestCounts, LeafCounts] =
    threadInstanceCount(Nest, TargetNode, OrigCounts);

  modifyLeafFunction(TargetNode, BlockSizes, LeafCounts);

  // Set bounds
  std::vector<Value*> &LeafBounds = TargetNode.getDimLimits();
  std::vector<Value*> &NestBounds = Nest.getDimLimits();
  for (unsigned Dim = 0; Dim < TargetNode.getNumOfDim(); ++Dim) {
    // Leaf
    if (TileSizes.count(Dim) > 0)
      LeafBounds[Dim] = TileSizes.lookup(Dim);
    else
      // Threaded original count
      LeafBounds[Dim] = NestCounts[Dim];

    // Nest
    if (TileCounts.count(Dim) > 0)
      NestBounds[Dim] = TileCounts.lookup(Dim);
    else
      NestBounds[Dim] =
        ConstantInt::get(Nest.getDimLimits()[Dim]->getType(), 1);
  }

  // May have changed
  BasicBlock &NewNestEntry = Nest.getFuncPointer()->getEntryBlock();

  // Insert node intrinsics (needed for bounds computation to not be elided)
  InsertNodeCall(NewNestEntry, TargetNode, LeafBounds);
  InsertNodeCall(ParentEntry, Nest, NestBounds);

  FunctionAnalysisManager FAM;
  LoopAnalysisManager LAM;
  ModuleAnalysisManager MAM;
  CGSCCAnalysisManager CAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  PB.registerLoopAnalyses(LAM);
  PB.registerModuleAnalyses(MAM);
  PB.registerCGSCCAnalyses(CAM);
  PB.crossRegisterProxies(LAM, FAM, CAM, MAM);
  FunctionPassManager FPM;
  FPM.addPass(EarlyCSEPass(false));
  FPM.addPass(SimplifyCFGPass());
  FPM.addPass(LoopSimplifyPass());
  FPM.addPass(InstCombinePass());

  FPM.run(*TargetNode.getFuncPointer(), FAM);
  FPM.run(*Nest.getFuncPointer(), FAM);
  FPM.run(*TargetNode.getParent()->getFuncPointer(), FAM);
}

} // namespace hpvm
