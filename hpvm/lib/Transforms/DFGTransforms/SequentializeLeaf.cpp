//==------- SequentializeLeaf.cpp - Header file for HPVM strip mining ------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that de-replicates a leaf node by transforming
// the dynamic replication factor into a loop in the node.
//
//===----------------------------------------------------------------------===//
#include "llvm/IR/Intrinsics.h"
#define DEBUG_TYPE "dfgsequentialize"
#include "Transforms/DFGTransforms/SequentializeLeaf.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

using namespace hpvm;

bool LeafSequentializer::canRun() const {
  Function *NodeFunc = TargetNode->getFuncPointer();
  Optional<Instruction *> OptionalRI = getUniqueReturnInstruction(NodeFunc);
  if (!OptionalRI.hasValue()) {
    LLVM_DEBUG(errs() << "***Cannot sequentialize a node that does not have a "
                    "unique return!\n");
    return false;
  }
  bool notOne = 0;
  for (auto *V : TargetNode->getDimLimits()) {
    if (ConstantInt *ConstV = dyn_cast<ConstantInt>(V)) {
      if (ConstV->getSExtValue() != 1) {
        notOne |= 1;
      } else {
        notOne |= 0;
      }
    } else {
      notOne |= 1;
    }
  }
  if (!notOne) {
    LLVM_DEBUG(errs() << "***Not sequentializing a node that has all dims = 1!\n");
    return false;
  }
  return true;
}

void LeafSequentializer::run() {
  LLVM_DEBUG(errs() << "\nLEAF SEQUENTIALIZATION PASS\n");
  NodeFunc = TargetNode->getFuncPointer();
  unsigned numDims = TargetNode->getNumOfDim();
  DimLimits = TargetNode->getDimLimits();

  LLVM_DEBUG(errs() << "*****************************************************\n");
  LLVM_DEBUG(errs() << "Node " << TargetNode->getFuncPointer()->getName() << " has "
               << numDims << " dimensions!\n");

  // Vector for args and argtypes for cloning
  std::vector<Argument *> NewArgs;
  std::vector<Type *> NewArgTypes;
  // Map new dimlimit args in child to dimlimit args in parent
  std::map<unsigned, unsigned> DimLimitArgMap;
  // Map dim to new child arg
  std::map<unsigned, unsigned> DimToArgMap;

  NeedToClone =
      updateNonConstantDims(NewArgs, NewArgTypes, DimLimitArgMap, DimToArgMap);

  if (NeedToClone == true) {
    hpvm::Target hint = hpvmUtils::getPreferredTarget(NodeFunc);
    OldNodeFunc = NodeFunc;
    NodeFunc = updateFuncWithNewArgs(OldNodeFunc, NewArgs, NewArgTypes, VMap);
    //		LLVM_DEBUG(errs() << "Tag: " << TargetNode->getTag() << "\n");
    //		LLVM_DEBUG(errs() << "Hint: " << TargetNode->getTargetHint() <<
    //"\n");
    hpvmUtils::addHint(NodeFunc, hint);
    TargetNode->setFuncPointer(NodeFunc);
    for (auto ArgMapEntry : DimLimitArgMap) {
      Type *ArgType = Type::getInt64Ty(C.getLLVMContext());
      DFEdge *DimEdge =
          addInputEdgeAndBind(TargetNode, ArgMapEntry.second, ArgMapEntry.first,
                              ArgType, false, C, ModifyParent);

      DimLimits[DimToArgMap[ArgMapEntry.first]] =
          NodeFunc->arg_begin() + ArgMapEntry.first;
    }
  }

  LLVM_DEBUG(errs() << "New Dim Limits:\n");
  for (auto limit : DimLimits) {
    LLVM_DEBUG(errs() << *limit << "\n");
  }

  //  LLVM_DEBUG(NodeFunc->viewCFG());

  // Before creating the loop, add the ivdep intrinsic

  Optional<Instruction *> OptionalRI = getUniqueReturnInstruction(NodeFunc);
  assert(OptionalRI.hasValue() &&
         "Node Function does not have a unique return!");
  Instruction *RI = OptionalRI.getValue();
  IndVars.reserve(numDims);
  for (int i = numDims - 1; i >= 0; --i) {
    IndVars[i] =
        createLoopFromFunction(NodeFunc, RI, DimLimits[i], i, InsertIVDEP);
  }

  replaceQueryIntrinsics(numDims);

  // At this point all the isNZLoop intrinsic operands should have been replaced
  // with the InductionVariables. We can now move them to their corresponding
  // headers.
  moveNZLoop();

  // Change the node's Dims
  changeDimToOne(TargetNode);
  // Change the node's createNode
  if (ModifyParent)
    replaceCreateNode();

  LLVM_DEBUG(errs() << "\n\nNew Parent Node Funciton: "
               << *TargetNode->getParent()->getFuncPointer() << "\n\n");
  LLVM_DEBUG(errs() << "\n\nNew Function: " << *NodeFunc << "\n\n");
  //  llvm::ViewGraph(TargetNode->getParent()->getChildGraph(), "ParentGraph");

  for (auto ib = TargetNode->indfedge_begin(), ie = TargetNode->indfedge_end();
       ib != ie; ib++) {
    DFEdge *Edge = *ib;
    LLVM_DEBUG(errs() << "Edge from: "
                 << Edge->getSourceDF()->getFuncPointer()->getName() << " : "
                 << Edge->getSourcePosition()
                 << "\n\tTo: " << Edge->getDestDF()->getFuncPointer()->getName()
                 << " : " << Edge->getDestPosition() << "\n");
  }
  LLVM_DEBUG(errs() << "*****************************************************\n");

  // Mark node as sequentialized
  TargetNode->setSequentialized(true);
}

bool LeafSequentializer::updateNonConstantDims(
    std::vector<Argument *> &NewArgs, std::vector<Type *> &NewArgTypes,
    std::map<unsigned, unsigned> &DimLimitArgMap,
    std::map<unsigned, unsigned> &DimToArgMap) {
  unsigned NumDims = DimLimits.size();
  bool NeedToClone = false;
  unsigned NewArgNo = NodeFunc->arg_size();
  for (int i = 0; i < NumDims; ++i) {
    LLVM_DEBUG(errs() << "\t- Dim " << i << " has limit: " << *DimLimits[i] << "\n");
    if (!isa<ConstantInt>(DimLimits[i])) {
      NeedToClone = true;
      LLVM_DEBUG(errs() << "Dim limit is not a constant, need to generate argument "
                      "for it!\n");

      // Create new arg for dim and added it to newarg vector
      Twine Dim = (i == 0 ? "X" : (i == 1 ? "Y" : "Z"));
      Argument *DimArg = new Argument(Type::getInt64Ty(NodeFunc->getContext()),
                                      "dim" + Dim, NodeFunc);
      NewArgs.push_back(DimArg);
      NewArgTypes.push_back(DimArg->getType());
      DimLimits[i] = DimArg;

      // Get dim arg in parent and map the argnos. Also, map new arg to dim
      assert(isa<Argument>(TargetNode->getDimLimits()[i]) &&
             "Expecting original non-constant dim limit to be an argument!");

      Argument *ParentDimArg =
          dyn_cast<Argument>(TargetNode->getDimLimits()[i]);
      LLVM_DEBUG(errs() << "Mapping Argument: " << ParentDimArg->getArgNo()
                   << " of parent\n"
                   << "\tTo Argument: " << NewArgNo << " of child!\n");
      DimToArgMap[NewArgNo] = i;
      DimLimitArgMap[NewArgNo++] = ParentDimArg->getArgNo();
    }
  }
  return NeedToClone;
}

bool LeafSequentializer::replaceGetNodeInstanceID(IntrinsicInst *II,
                                                  unsigned NumDims) {
  Function *IF = II->getCalledFunction();
  std::string getNodeInstanceBase = "llvm.hpvm.getNodeInstanceID.";
  // std::string dimString;
  // if (numDims == 1)
  //   dimString = "x";
  // else if (numDims == 2)
  //   dimString = (dim == 0 ? "y" : "x");
  // else if (numDims == 3)
  //   dimString = (dim == 0 ? "z" : dim == 1 ? "y" : "x");
  // std::string getNodeInstanceIDString = getNodeInstanceBase + dimString;
  if (IF->getName().startswith(getNodeInstanceBase)) {
    Value *GetNode = II->getArgOperand(0);
    IntrinsicInst *GetNodeII = dyn_cast<IntrinsicInst>(GetNode);
    assert(GetNodeII &&
           "Something is not right with the getNodeInstanceID call!");
    Function *GetNodeF = GetNodeII->getCalledFunction();
    StringRef DimString = IF->getName().take_back(1);
    int Dim = (DimString == "x") ? 0 : (DimString == "y") ? 1 : 2;
    if (GetNodeF->getName().equals("llvm.hpvm.getNode")) {
      LLVM_DEBUG(errs() << "Found a getNodeInstanceID." << DimString
                   << " for current node!\n");
      II->replaceAllUsesWith(IndVars[Dim]);
      return true;
    }
  }
  return false;
}

bool LeafSequentializer::replaceGetNumNodeInstances(IntrinsicInst *II,
                                                    unsigned numDims) {
  Function *IF = II->getCalledFunction();
  std::string getNodeInstanceBase = "llvm.hpvm.getNumNodeInstances.";
  // std::string dimString;
  // if (numDims == 1)
  //   dimString = "x";
  // else if (numDims == 2)
  //   dimString = (dim == 0 ? "y" : "x");
  // else if (numDims == 3)
  //   dimString = (dim == 0 ? "z" : dim == 1 ? "y" : "x");
  // std::string getNumNodeInstancesString = getNodeInstanceBase + dimString;
  if (IF->getName().startswith(getNodeInstanceBase)) {
    Value *GetNode = II->getArgOperand(0);
    IntrinsicInst *GetNodeII = dyn_cast<IntrinsicInst>(GetNode);
    assert(GetNodeII &&
           "Something is not right with the getNumNodeInstances call!");
    Function *GetNodeF = GetNodeII->getCalledFunction();
    StringRef DimString = IF->getName().take_back(1);
    int Dim = (DimString == "x") ? 0 : (DimString == "y") ? 1 : 2;
    if (GetNodeF->getName().equals("llvm.hpvm.getNode")) {
      LLVM_DEBUG(errs() << "Found a getNumNodeInstances." << DimString
                   << " for current node!\n");
      II->replaceAllUsesWith(DimLimits[Dim]);
      return true;
    }
  }
  return false;
}

void LeafSequentializer::replaceQueryIntrinsics(unsigned numDims) {

  std::vector<Instruction *> ItoRemove;
  // Loop through the instructions to find intrinsics that need to be replaced.
  for (inst_iterator I = inst_begin(NodeFunc), E = inst_end(NodeFunc); I != E;
       ++I) {
    if (IntrinsicInst *II = dyn_cast<IntrinsicInst>(&*I)) {
      if (replaceGetNodeInstanceID(II, numDims) ||
          // replaceGetNodeInstanceID(II, 1, numDims) ||
          // replaceGetNodeInstanceID(II, 2, numDims) ||
          replaceGetNumNodeInstances(II, numDims) /*||
          replaceGetNumNodeInstances(II, 1, numDims) ||
          replaceGetNumNodeInstances(II, 2, numDims)*/) {
        Value *GetNode = II->getArgOperand(0);
        IntrinsicInst *GetNodeII = dyn_cast<IntrinsicInst>(GetNode);
        ItoRemove.push_back(II);
        if (std::find(ItoRemove.begin(), ItoRemove.end(), GetNodeII) ==
            ItoRemove.end())
          ItoRemove.push_back(GetNodeII);
      }
    }
  }

  for (auto I : ItoRemove) {
    IntrinsicInst *II = dyn_cast<IntrinsicInst>(&*I);
    bool skip = false;
    if (II->getCalledFunction()->getName().equals("llvm.hpvm.getNode")) {
      // Need to make sure that this getNode is not used by getParentNode!
      for (auto U : II->users()) {
        IntrinsicInst *UI = dyn_cast<IntrinsicInst>(U);
        if (UI->getCalledFunction()->getName().equals(
                "llvm.hpvm.getParentNode")) {
          LLVM_DEBUG(errs()
                << "Will not erase getNode that is used by getParentNode!");
          skip = true;
        }
      }
    }
    if (!skip) {
      LLVM_DEBUG(errs() << "Erasing " << *I << "\n");
      I->replaceAllUsesWith(UndefValue::get((I)->getType()));
      I->eraseFromParent();
    }
  }
}

// Moves all the NZLoop intrinsics to their corresponding loop headers after
// sequentialization.
void LeafSequentializer::moveNZLoop() {
  for (auto *IndVar : IndVars) {
    for (auto *User : IndVar->users()) {
      if (auto *II = dyn_cast<IntrinsicInst>(User)) {
        if (II->getIntrinsicID() == Intrinsic::hpvm_nz_loop) {
          II->moveAfter(IndVar);
        }
      }
    }
  }
}
// void LeafSequentializer::changeDimToOne() {
//  TargetNode->setNumOfDim(1);
//  std::vector<Value *> NewDimLimits;
//  NewDimLimits.push_back(
//      ConstantInt::get(TargetNode->getDimLimits()[0]->getType(), 1));
//  TargetNode->setDimLimits(NewDimLimits);
//}

void LeafSequentializer::replaceCreateNode() {
  IntrinsicInst *OldCreateNode = TargetNode->getInstruction();
  Constant *Fp = ConstantExpr::getPointerCast(
      NodeFunc, Type::getInt8PtrTy(OldCreateNode->getContext()));
  Function *CreateNodeFunc =
      Intrinsic::getDeclaration(&C.getModule(), Intrinsic::hpvm_createNode1D);

  ArrayRef<Value *> CreateNodeArgs;
  Value *CreateNode1DArgs[] = {
      Fp, ConstantInt::get(TargetNode->getDimLimits()[0]->getType(), 1)};
  CreateNodeArgs = ArrayRef<Value *>(CreateNode1DArgs, 2);

  CallInst *CI = CallInst::Create(CreateNodeFunc, CreateNodeArgs,
                                  OldCreateNode->getName());
  IntrinsicInst *NewCreateNode = cast<IntrinsicInst>(CI);

  LLVM_DEBUG(errs() << "Replacing old create node:\n\t" << *OldCreateNode << "\n"
               << "With new create node:\n\t" << *NewCreateNode << "\n");
  OldCreateNode->replaceAllUsesWith(NewCreateNode);
  ReplaceInstWithInst(OldCreateNode, NewCreateNode);
  TargetNode->setInstruction(NewCreateNode);
}
