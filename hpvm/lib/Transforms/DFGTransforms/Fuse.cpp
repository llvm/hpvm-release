//==------------ Fuse.cpp - Implements HPVM fusion -------------------------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that performs fusion on HPVM DFG nodes.
//
//===----------------------------------------------------------------------===//

#include <unordered_set>

#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/BasicAliasAnalysis.h"
#include "llvm/Analysis/DDG.h"
#include "llvm/Analysis/DependenceAnalysis.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar/ADCE.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"

#define DEBUG_TYPE "fuse"
#include "CoreHPVM/DFGraph.h"
#include "CoreHPVM/DFGUtils.h"
#include "Transforms/DFGTransforms/DFGTransformUtils.h"
#include "Transforms/DFGTransforms/SequentializeLeaf.h"

#include "Transforms/DFGTransforms/Fuse.h"

namespace hpvm {

enum class POrder {
  NC, // Not comparable
  LT,
  EQ,
  GT
};

// Returns whether the second node depends on the first.
// Returns false if Ancestor == Descendant.
static bool depends(const llvm::DFNode *Ancestor,
  const llvm::DFNode *Descendant) 
{
  using namespace llvm;

  assert(Ancestor->getParent() == Descendant->getParent());

  // Depth-first search for descendant
  std::unordered_set<const DFNode*> Explored;
  for (DFNode::const_successor_iterator It = Ancestor->successors_begin();
       It != Ancestor->successors_end(); ++It) {
    const DFNode *Successor = *It;
    if (Explored.count(Successor) > 0) continue;
    if (Successor == Descendant) return true;
    if (depends(Successor, Descendant)) return true;
    Explored.insert(Successor);
  }
    
  return false;
}

// Returns the ordering predicate P satisfying N1 P N2
// N1 < N2 if N2 depends on N1.
static POrder topoCompare(const llvm::DFNode *N1, const llvm::DFNode *N2) 
{
  if (N1 == N2) return POrder::EQ;

  const bool N1toN2 = depends(N1, N2);
  const bool N2toN1 = depends(N2, N1);

  // They should not be descendants of each other.
  assert(!(N1toN2 && N2toN1));
  if (N1toN2) return POrder::LT;
  if (N2toN1) return POrder::GT;
  
  return POrder::NC;
}

struct DataMapping 
{
  // (Old node, in pos) -> New in pos
  llvm::DenseMap<std::pair<const DFLeafNode*, unsigned>, unsigned> In;
  // New out pos -> (Old node, out pos)
  llvm::DenseMap<unsigned, std::pair<const DFLeafNode*, unsigned>> Out;
  // (Old node, in pos) -> (Old node, out pos) - internal edge mappings
  llvm::DenseMap<std::pair<const DFLeafNode*, unsigned>, std::pair<const DFLeafNode*, unsigned>> Internal;
  
  unsigned OutCount;

  // Move constructor of ValueMap is deleted so need indirection
  std::unique_ptr<llvm::ValueToValueMapTy> toVMap(Function* NewFun) const;
};

static bool hasNode(const std::vector<DFLeafNode*> &Nodes, const DFNode *N)
{
  return std::find(Nodes.begin(), Nodes.end(), N) != Nodes.end();
}

// Creates a new node with the in-edges and out-edges from the original,
// deleting the originals
static llvm::DFLeafNode *mergeNodes(DataMapping &DataMap,
  const std::vector<DFLeafNode*> &Nodes) 
{
  using namespace llvm;
  
  assert(Nodes.size() > 1);

  DFInternalNode &Parent = *Nodes[0]->getParent();

  // Gather data for edge cloning and function type
  std::vector<std::tuple<const DFLeafNode*, unsigned, const DFEdge*>> Inputs;
  std::vector<std::tuple<const DFLeafNode*, unsigned, const DFEdge*>> Outputs;
  std::string NewName = "Fused";
  for (const DFLeafNode *Node : Nodes) {
    NewName += "_";
    NewName += Node->getFuncPointer()->getName();
    
    // In-edges
    for (auto It = Node->indfedge_begin(), End = Node->indfedge_end(); It != End;
         ++It) {
      const DFEdge *OldEdge = *It;
      DFNode *Source = OldEdge->getSourceDF();

      // Handle edges internal to the fusion
      const unsigned DestIdx = OldEdge->getDestPosition();
      if (hasNode(Nodes, OldEdge->getSourceDF())) {
        const unsigned SourceIdx = OldEdge->getSourcePosition();

        // Source being in Nodes means it's a DFLeafNode
        DataMap.Internal[{ Node, DestIdx }] = { static_cast<DFLeafNode*>(Source), SourceIdx };
        continue;
      }
      
      Inputs.emplace_back(Node, DestIdx, OldEdge);
    }

    // Out-edges
    for (auto It = Node->outdfedge_begin(), End = Node->outdfedge_end();
         It != End; ++It) {
      const DFEdge *OldEdge = *It;
      if (hasNode(Nodes, OldEdge->getDestDF())) continue; // Handled
                                                          // in inputs
      Outputs.emplace_back(Node, OldEdge->getSourcePosition(), OldEdge);
    }
  }

  // Sort so we can preserve parameter order (needed for pointers)
  std::stable_sort(Inputs.begin(), Inputs.end());
  std::stable_sort(Outputs.begin(), Outputs.end());

  // Try to avoid duplicate edges by mapping each source value to one input
  // (Source node, output position) -> input position on new node
  LLVM_DEBUG(errs() << "Node fusion edge deduplication...\n");
  llvm::DenseMap<std::pair<const DFNode*, unsigned>, std::vector<unsigned>> InputsForSource;
  unsigned NextInPos = 0;
  bool nextIsPtrSize = false;
  auto NewEnd = std::remove_if(Inputs.begin(), Inputs.end(), [&] (auto &Input) {
    const DFEdge &OldEdge = *std::get<2>(Input);
    const DFNode *Source = OldEdge.getSourceDF();
    const unsigned SourceIdx = OldEdge.getSourcePosition();
    const DFLeafNode *Dest = std::get<0>(Input);

    auto InKey = std::make_pair(Dest, OldEdge.getDestPosition());
    auto SourceKey = std::make_pair(Source, SourceIdx);
    auto It = InputsForSource.find(SourceKey);

    LLVMContext &C = Source->getFuncPointer()->getContext();
    if (It == InputsForSource.end() || nextIsPtrSize) {
      if (nextIsPtrSize)
        assert(OldEdge.getType() == Type::getInt64Ty(C));
      LLVM_DEBUG(errs() << "Adding arg " << InKey.second << " of " << Dest->getName() << "\n");
      InputsForSource[SourceKey].push_back(NextInPos);
      DataMap.In[InKey] = NextInPos;
      ++NextInPos;

      // Need to make sure size arguments are not thrown out if the
      // pointer is included
      nextIsPtrSize = OldEdge.getType()->isPointerTy();
      return false;
    } else {
      unsigned MapToThisInput = It->second.front();
      LLVM_DEBUG(errs() << "Skipping arg " << InKey.second << " of " << Dest->getName() << "\n");
      LLVM_DEBUG(errs() << "Mapping to arg " << MapToThisInput << "\n");
      DataMap.In[InKey] = MapToThisInput;
      return true;
    }
  });
  Inputs.erase(NewEnd, Inputs.end());

  std::vector<Type*> InTypes;
  std::vector<Type*> OutTypes;
  for (const auto &Tup : Inputs)
    InTypes.push_back(std::get<2>(Tup)->getType());

  // Record orphans and add them to input types
  LLVM_DEBUG(errs() << "ORPHANS:" << "\n");
  std::vector<std::pair<DFLeafNode*, unsigned>> Orphans;
  for (DFLeafNode *Node : Nodes) {
    Function *NodeF = Node->getFuncPointer();
    const size_t ArgCount = NodeF->arg_end() - NodeF->arg_begin();
    for (unsigned i = 0; i < ArgCount; ++i) {
      if (Node->getInDFEdgeAt(i) == nullptr) {
        Argument *Arg = NodeF->arg_begin() + i;
        LLVM_DEBUG(errs() << *Arg << "\n");
        InTypes.push_back(Arg->getType());
        Orphans.push_back({Node, i});
      }
    }
  }

  {
    // Need to only count an output once, in case it has multiple edges
    const DFLeafNode *PrevNode = nullptr;
    unsigned PrevIdx = 0;
    for (const auto &Tup : Outputs) {
      if (PrevNode != std::get<0>(Tup) || PrevIdx != std::get<1>(Tup))
        OutTypes.push_back(std::get<2>(Tup)->getType());
      PrevNode = std::get<0>(Tup);
      PrevIdx = std::get<1>(Tup);
    }
  }

  Function *RefFun = Nodes[0]->getFuncPointer();
  LLVMContext &C = RefFun->getContext();
  FunctionType *FType = FunctionType::get(StructType::get(C, OutTypes), InTypes, false);
  Function *NewFun = makeEmptyFunction(FType, RefFun->getLinkage(), RefFun->getParent(),
    NewName);

  LLVM_DEBUG(errs() << "Merged fun type: " << *FType << "\n");

  hpvmUtils::addHint(NewFun, hpvmUtils::getPreferredTarget(RefFun));

  // Create the new node
  DFLeafNode *NewNode = DFLeafNode::Create(nullptr, NewFun,
    Nodes[0]->getTargetHint(), Nodes[0]->getParent(), Nodes[0]->getNumOfDim(),
    Nodes[0]->getDimLimits());

  // Insert the new node
  Parent.addChildToDFGraph(NewNode);

  // Replicate input edges
  for (const auto &Tup : Inputs) {
    const DFEdge &OldEdge = *std::get<2>(Tup);
    DFNode *Source = OldEdge.getSourceDF();
    const unsigned SourceIdx = OldEdge.getSourcePosition();
    auto SourceKey = std::make_pair(Source, SourceIdx);
    assert(InputsForSource.count(SourceKey) > 0);
    assert(!InputsForSource[SourceKey].empty());

    // Pick one edge for each time an input uses the source
    unsigned DestIdx = InputsForSource[SourceKey].back();
    InputsForSource[SourceKey].pop_back();
    DFEdge *NewEdge =
      DFEdge::Create(Source, NewNode, OldEdge.getEdgeType(), SourceIdx,
                       DestIdx, OldEdge.getType(), OldEdge.isStreamingEdge());
    Parent.addEdgeToDFGraph(NewEdge);
  }

  // Create mappings for the orphaned inputs
  for (auto &Pair : Orphans) {
    DataMap.In[Pair] = NextInPos;
    ++NextInPos;
  }

  {
    // Replicate output edges - deduplicate here too
    const DFLeafNode *PrevNode = nullptr;
    unsigned PrevIdx = 0;
    unsigned OutPos = -1; // Incremented initially
    for (const auto &Tup : Outputs) {
      const DFEdge &OldEdge = *std::get<2>(Tup);
      DFNode *Dest = OldEdge.getDestDF();
      if (PrevNode != std::get<0>(Tup) || PrevIdx != std::get<1>(Tup)) {
        OutPos++;
        DataMap.Out[OutPos] = { std::get<0>(Tup), OldEdge.getSourcePosition() };
        PrevNode = std::get<0>(Tup);
        PrevIdx = std::get<1>(Tup);
      }
      assert(OutPos >= 0);
      DFEdge *NewEdge =
        DFEdge::Create(NewNode, Dest, OldEdge.getEdgeType(),
                       OutPos, OldEdge.getDestPosition(), OldEdge.getType(),
                       OldEdge.isStreamingEdge());

      Parent.addEdgeToDFGraph(NewEdge);
    }
    DataMap.OutCount = OutPos + 1;
  }

  // Remove nodes
  for (DFLeafNode *Node : Nodes) {
    hpvmUtils::removeNodeEdges(*Node);
    Parent.removeChildFromDFGraph(Node);
  }

  LLVM_DEBUG(errs() << "Fusion will have " << NextInPos << " inputs and " << DataMap.OutCount << " outputs\n");

  return NewNode;
}

std::unique_ptr<llvm::ValueToValueMapTy> DataMapping::toVMap(Function* NewFun) const 
{
  std::unique_ptr<llvm::ValueToValueMapTy> VMPtr =
    std::make_unique<llvm::ValueToValueMapTy>();
  ValueToValueMapTy &VMap = *VMPtr;
  LLVM_DEBUG(errs() << "In map has " << In.size() << " entries\n";);
  for (const auto Pair : In) {
    Argument *OldArg =
      Pair.first.first->getFuncPointer()->arg_begin() + Pair.first.second;
    assert(Pair.second < NewFun->arg_end() - NewFun->arg_begin());
    Argument *NewArg = NewFun->arg_begin() + Pair.second;
    NewArg->setName(OldArg->getName());
    VMap.insert({ OldArg, NewArg });
  }
  LLVM_DEBUG(errs() << "VMap has " << VMap.size() << " entries\n";);
  
  return VMPtr;
}

// Finds the components of a struct and the given map.
// OutMap is a double entendre since it represents output values from
// the node as well as being an output parameter of this function
// Note: Both this and tryToMoveStruct in Tile.cpp are worklists over
// structs. Might be worth factoring out.
static void findComponents(
  llvm::DenseMap<std::pair<const llvm::DFLeafNode*, unsigned>, llvm::Value*> &OutMap,
  const llvm::DFLeafNode* Node, llvm::Value *Struct) 
{
  using namespace llvm;
  LLVM_DEBUG(errs() << "findComponents: " << *Struct << "\n");

  // -1 Means that the value is the aggregate being built into the
  // struct, as opposed to an element of the struct.
  std::vector<std::pair<unsigned, Value*>> Worklist { std::make_pair(-1, Struct) };
  while (!Worklist.empty()) {
    auto &Pair = Worklist.back();
    int Idx = Pair.first;
    Value *V = Pair.second;
    Worklist.pop_back();

    // If it's a component of the struct
    if (Idx >= 0 || isa<Argument>(V) || isa<Constant>(V)) {
      if (Idx >= 0) {
        // -1 would mean it's the undefined constant used to start a struct
        assert(OutMap.count({ Node, Idx }) == 0);
        OutMap[{ Node, Idx }] = V;
      }
      continue;
    }

    if (InsertValueInst *Insert = dyn_cast<InsertValueInst>(V)) {
      ArrayRef<unsigned> Idxes = Insert->getIndices();
      assert(Idxes.size() == 1); // Should be true for HPVM-generated returns
      Worklist.push_back({ -1, Insert->getAggregateOperand() });
      Worklist.push_back({ Idxes[0], Insert->getInsertedValueOperand() });
      continue;
    }

    // No other values are allowed
    errs() << "Non-insert, non-constant aggregate definition:\n";
    errs() << *V << "\n";
    assert(false && "Non-insert, non-constant aggregate definition");
  }
}

static void moveAllocas(llvm::BasicBlock &BSrc, llvm::BasicBlock &BTarget) {
  Instruction *Pos = BTarget.getFirstNonPHIOrDbg();
  std::vector<AllocaInst*> MoveThese;
  for (Instruction &I : BSrc)
    if (auto *AI = dyn_cast<AllocaInst>(&I))
      MoveThese.push_back(AI);
  for (AllocaInst *AI : MoveThese) {
    AI->moveBefore(Pos);
  }
}

// CombinedF is intended to be an empty function with appropriate
// type. Returns the resulting VMap
// TODO: Deduplicate calls to getNodeId etc.
static std::unique_ptr<ValueToValueMapTy>
mergeFunctionBodiesInto(llvm::Function *CombinedF,
  llvm::DenseMap<BasicBlock *, unsigned> &OutBBMap,
  const std::vector<llvm::DFLeafNode*> &Nodes, const DataMapping &DataMap) 
{
  using namespace llvm;
  
  std::unique_ptr<ValueToValueMapTy> VMPtr = DataMap.toVMap(CombinedF);
  ValueToValueMapTy &VMap = *VMPtr;

  CombinedF->deleteBody();

  SmallVector<ReturnInst*, 4> Returns;
  BasicBlock *PrevEnd = nullptr;
  BasicBlock *PrevResult = nullptr; // Block from previous function
                                    // with missing terminator -
                                    // should link to next
  DenseMap<std::pair<const DFLeafNode*, unsigned>, Value*> NodeResults;
  std::vector<llvm::BasicBlock*> Starts;
  Starts.reserve(Nodes.size());
  for (const DFLeafNode *Node : Nodes) {
    Function *NodeF = Node->getFuncPointer();

    // Update VMap with internal values
    const size_t ArgCount = NodeF->arg_end() - NodeF->arg_begin();
    for (unsigned i = 0; i < ArgCount; ++i) {
      auto It = DataMap.Internal.find({ Node, i });
      if (It != DataMap.Internal.end()) {
        Argument *OldArg = NodeF->arg_begin() + i;
        Value *NewArg = NodeResults[It->second];
        LLVM_DEBUG(errs() << "Replacement: " << *OldArg << " | " << *NewArg << "\n");
        NewArg->setName(OldArg->getName() + "_internal");
        VMap[OldArg] = NewArg;
      }
    }
    
    CloneFunctionInto(CombinedF, NodeF, VMap, CloneFunctionChangeType::LocalChangesOnly, Returns);
    assert(Returns.size() > 0);

    if (PrevEnd != nullptr) {
      BasicBlock *NodeFStart = PrevEnd->getNextNode(); // First newly-inserted block
      Starts.push_back(NodeFStart);
      assert(NodeFStart);
      assert(PrevResult);
      IRBuilder<> Builder(PrevResult);
      Builder.CreateBr(NodeFStart);
      moveAllocas(*NodeFStart, *CombinedF->begin());
    } else {
      Starts.push_back(&*CombinedF->begin());
    }

    // Copy attributes
    for (auto p : VMap) {
      const Argument *OrigArg = dyn_cast<Argument>(p.first);
      if (p.second == nullptr) continue;
      Argument *NewArg = dyn_cast<Argument>(p.second);

      if (OrigArg && NewArg) {
        LLVM_DEBUG(errs() << "Copying attributes from " << *OrigArg << " to " << *NewArg << "\n");
        AttributeList OldAttrs = OrigArg->getParent()->getAttributes();
        AttrBuilder AB(OldAttrs.getParamAttributes(OrigArg->getArgNo()));
        NewArg->addAttrs(AB);
      }
    }
    
    if (Returns.size() > 1) {
      assert(false && "Multiple return not yet supported in HPVM Fusion");

      // Merge all return values, if more than one
      BasicBlock *Merger = BasicBlock::Create(CombinedF->getContext(),
        NodeF->getName() + "_merger", CombinedF);
      IRBuilder<> Builder(Merger);
      PHINode *Phi = Builder.CreatePHI(Returns[0]->getType(), Returns.size(),
        NodeF->getName() + "_merged");
      for (ReturnInst *RI : Returns) {
        IRBuilder<> RiBuilder(RI);
        Value *RetVal = RI->getReturnValue();
        Phi->addIncoming(RetVal, RI->getParent());
        RiBuilder.CreateBr(Merger);
        RI->eraseFromParent();
      }
      PrevResult = Merger;
      // To support, would need to store BB pointer
      // NodeResults[Node] = Phi;
    } else {
      PrevResult = Returns[0]->getParent();

      findComponents(NodeResults, Node, Returns[0]->getReturnValue());
      Returns[0]->eraseFromParent();
    }

    PrevEnd = &CombinedF->back();
    Returns.clear();
  }
  

  // Construct new return
  assert(PrevResult != nullptr);
  IRBuilder<> RetBuilder(PrevResult);
  Value *RetVal = UndefValue::get(CombinedF->getReturnType());
  for (unsigned I = 0; I < DataMap.OutCount; ++I) {
    auto MapIt = DataMap.Out.find(I);
    assert(MapIt != DataMap.Out.end());
    auto &Pair = *MapIt;
    auto ResIt = NodeResults.find(Pair.second);
    assert(ResIt != NodeResults.end());
    Value *ResVal = ResIt->second;
    LLVM_DEBUG(errs() << "Creating merge insert: " << *RetVal << " | " << *ResVal << " | " << I << "\n");
    RetVal = RetBuilder.CreateInsertValue(RetVal, ResVal, { I });
  }
  RetBuilder.CreateRet(RetVal);

  LLVM_DEBUG(errs() << "Merged Function:\n");
  LLVM_DEBUG(errs() << *CombinedF << "\n");

  OutBBMap.clear();
  unsigned i = 0;
  for (; i < Starts.size() - 1; ++i) {
    BasicBlock *NextStart = Starts[i+1];
    for (BasicBlock *BB = Starts[i]; BB != NextStart; BB = BB->getNextNode()) {
      assert(BB);
      OutBBMap[BB] = i;
    }
  }
  for (BasicBlock *BB = Starts.back(); BB != nullptr; BB = BB->getNextNode()) {
    OutBBMap[BB] = i;
  }

  return VMPtr;
}

static DFLeafNode *cloneLeafInto(DFGraph &G, const DFLeafNode &Old) 
{
  DFLeafNode *New = DFLeafNode::Create(Old.getInstruction(),
    Old.getFuncPointer(), Old.getTargetHint(), G.getParent(), Old.getNumOfDim(),
    Old.getDimLimits());
  G.addChildDFNode(New);

  return New;
}

// Assumes N2 succeeds N1
// Copies N1 / N2 and all edges between them
static std::vector<DFLeafNode*>
cloneLeavesInto(DFGraph &G, const DFLeafNode &N1, const DFLeafNode &N2)
{
  DFInternalNode *P = G.getParent();
  DFLeafNode *New1 = cloneLeafInto(G, N1);
  DFLeafNode *New2 = cloneLeafInto(G, N2);
  for (auto It = N1.outdfedge_begin(); It != N1.outdfedge_end(); ++It) {
    const DFEdge *E = *It;
    if (E->getDestDF() != &N2) continue;
    DFEdge *Copy = DFEdge::Create(New1, New2,
      E->getEdgeType(), E->getSourcePosition(), E->getDestPosition(),
      E->getType(), E->isStreamingEdge());
    P->addEdgeToDFGraph(Copy);
  }

  std::vector<DFLeafNode*> Nodes;
  Nodes.reserve(2);
  Nodes.push_back(New1);
  Nodes.push_back(New2);

  return Nodes;
}

// Outputs the VMap used for the merge
static DFLeafNode *doFusion(const std::vector<DFLeafNode*> &Nodes,
  std::unique_ptr<ValueToValueMapTy> &OutVMap,
  llvm::DenseMap<BasicBlock *, unsigned> &OutBBMap)
{
  DataMapping DataMap;
  DFLeafNode *NewNode = mergeNodes(DataMap, Nodes);
  OutVMap = mergeFunctionBodiesInto(NewNode->getFuncPointer(), OutBBMap, Nodes, DataMap);

  return NewNode;
}

static bool isMemOp(const llvm::Instruction &I) 
{
  using namespace llvm;
  return isa<LoadInst>(I) || isa<StoreInst>(I);
}

static std::vector<const llvm::Instruction*>
getMemOps(llvm::Function::const_iterator Begin, llvm::Function::const_iterator End)
{
  using namespace llvm;
  std::vector <const llvm::Instruction*> ops;
  for (auto It = Begin; It != End; ++It) {
    for (const Instruction &I : *It) {
      if (isMemOp(I))
        ops.push_back(&I);
    }
  }

  return ops;
}

static bool accessDiffIsEqual(ScalarEvolution &SE, DominatorTree &DT,
  const Loop &L, Instruction &I0, Instruction &I1) {
  Value *Ptr0 = getLoadStorePointerOperand(&I0);
  Value *Ptr1 = getLoadStorePointerOperand(&I1);
  if (!Ptr0 || !Ptr1)
    return false;

  const SCEV *SCEVPtr0 = SE.getSCEVAtScope(Ptr0, &L);
  const SCEV *SCEVPtr1 = SE.getSCEVAtScope(Ptr1, &L);

  ICmpInst::Predicate Pred = ICmpInst::ICMP_EQ;
  bool IsAlwaysEQ = SE.isKnownPredicate(Pred, SCEVPtr0, SCEVPtr1);

  return IsAlwaysEQ;
}
  
// Assumes no pointer aliasing
// Checks that fusion doesn't create loop carried dependencies
// true = good, false = bad
static bool depsOk(const DFLeafNode &N1, const DFLeafNode &N2) 
{
  using namespace llvm;

  // Make a copy of the nodes and edges to analyze
  // Function arg is a dummy
  DFInternalNode *P = DFInternalNode::Create(nullptr, N1.getParent()->getFuncPointer());
  DFGraph *G = P->getChildGraph();
  std::vector<DFLeafNode*> Nodes = cloneLeavesInto(*G, N1, N2);

  std::unique_ptr<ValueToValueMapTy> Output;
  llvm::DenseMap<BasicBlock *, unsigned> OutBBMap;
  DFLeafNode *NewNode = doFusion(Nodes, Output, OutBBMap);

  // Sequentialize the fused node
  Function *NodeF = NewNode->getFuncPointer();
  DFGTransform::Context C(*NodeF->getParent(), NodeF->getContext());
  ValueToValueMapTy VMap;

  std::vector<Instruction*> N0Mems, N1Mems;
  for (auto &Pair : OutBBMap) {
    assert(Pair.second == 0 || Pair.second == 1);
    std::vector<Instruction*> &Mems = Pair.second == 0 ? N0Mems : N1Mems;
    for (Instruction &I : *Pair.first) {
      if (isa<StoreInst>(I) || isa<LoadInst>(I))
        Mems.push_back(&I);
    }
  }

  assert(!isNodeSingular(NewNode));
  LeafSequentializer Seq(C, NewNode, VMap, false, false);
  if (!Seq.checkAndRun()) {
    LLVM_DEBUG(errs() << "Sequentialize failed in fusion check\n");
    return false;
  }
  NodeF = NewNode->getFuncPointer();

  // Map instructions to new ones in seq function
  if (Seq.hasCloned()) {
    for (Instruction *&I : N0Mems) {
      I = cast<Instruction>(VMap[I]);
      assert(I);
    }
    for (Instruction *&I : N1Mems) {
      I = cast<Instruction>(VMap[I]);
      assert(I);
    }
  }

  LLVM_DEBUG(errs() << "Checking for carried deps\n");
  bool NoCarries = true;
  PassBuilder PB;
  FunctionAnalysisManager FAM;
  PB.registerFunctionAnalyses(FAM);
  auto &DI = FAM.getResult<DependenceAnalysis>(*NodeF);
  auto &DT = FAM.getResult<DominatorTreeAnalysis>(*NodeF);
  auto &LI = FAM.getResult<LoopAnalysis>(*NodeF);
  auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*NodeF);

  assert(LI.end() - LI.begin() == 1);
  Loop &L = **LI.begin();
  for (Instruction *I0 : N0Mems) {
    for (Instruction *I1 : N1Mems) {
      if (std::unique_ptr<Dependence> Dep = DI.depends(I0, I1, true)) {
        if (Dep->isInput()) continue;

        // Are indices equal
        if (accessDiffIsEqual(SE, DT, L, *I0, *I1)) continue;
        
        LLVM_DEBUG(errs() << "Found bad dep:\n");
        LLVM_DEBUG(errs() << "Src: " << *Dep->getSrc() << "\n");
        LLVM_DEBUG(errs() << "Dst: " << *Dep->getDst() << "\n");
        NoCarries = false;
        LLVM_DEBUG(continue); // If debugging, want to print all deps
        break;
      }
    }
  }
  
  
  if (NoCarries)
    LLVM_DEBUG(errs() << "All good\n");
  
  // Remove the new function
  NodeF->replaceAllUsesWith(UndefValue::get(NodeF->getType()));
  NodeF->eraseFromParent();

  return NoCarries;
}

Fuse::~Fuse()
{
  for (Function *F : FunsToDelete) {
    F->replaceAllUsesWith(UndefValue::get(F->getType()));
    F->eraseFromParent();
  }
}

bool Fuse::canRun() const 
{
  using namespace llvm;

  LLVM_DEBUG(errs() << "Fuse::canRun\n");

  for (DFLeafNode *N : Nodes) {
    if (N->isDummyNode()) return false;
  }

  // Only support two nodes
  if (Nodes.size() != 2) return false;
  
  DFLeafNode *N1 = Nodes[0];
  DFLeafNode *N2 = Nodes[1];
  DFInternalNode *FirstParent = N1->getParent();
  
  // All nodes must be in the same graph
  if (Nodes.size() > 0) {
    for (DFLeafNode *N : Nodes)
      if (N->getParent() != FirstParent)
        return false;

    const std::vector<Value*> &FirstDim = Nodes[0]->getDimLimits();
    for (DFLeafNode *N : Nodes)
      if (N->getDimLimits() != FirstDim)
        return false;
  }

  POrder Ord = topoCompare(N1, N2);
  switch (Ord) {
  case POrder::LT:
    break;
  case POrder::GT:
    std::swap(N1, N2);
    break;
  case POrder::NC:
    // Horizontal fusion
    return true;
  case POrder::EQ:
    return false;
  }

  const auto SucBegin = N1->successors_begin();
  const auto SucEnd = N1->successors_end();

  // Check N2 is a successor of N1
  if (std::find(SucBegin, SucEnd, N2) == SucEnd) return false;

  // Check that there are no other dependencies from N1 to N2
  for (auto It = SucBegin; It != SucEnd; ++It)
    if (depends(*It, N2)) return false;

  if (isNodeSingular(N1)) return true; // Deps never a problem
  if (!depsOk(*N1, *N2)) return false;

  return true;
}

void Fuse::run() 
{
  using namespace llvm;

  LLVM_DEBUG(errs() << "Fuse::run\n");
  
  assert(canRun());
  if (Nodes.size() < 2) return; // Nothing to do

  // Rest of the steps assume nodes are in order, so swap if necessary
  assert(Nodes.size() == 2);
  if (topoCompare(Nodes[0], Nodes[1]) == POrder::GT)
    std::swap(Nodes[0], Nodes[1]);

  // Only used in legality check
  llvm::DenseMap<BasicBlock *, unsigned> Dummy;
  NewNode = doFusion(Nodes, ResultVMap, Dummy);

  FunctionAnalysisManager FAM;
  PassBuilder PB;
  PB.registerFunctionAnalyses(FAM);
  FunctionPassManager FPM;
  FPM.addPass(ADCEPass());

  FPM.run(*NewNode->getFuncPointer(), FAM);

  // Get rid of the old functions since it won't get processed in
  // codegen and will leave behind unselected intrinsics
  for (DFLeafNode *N : Nodes)
    FunsToDelete.push_back(N->getFuncPointer());
}

}
