//===-------------------------- StripMineTest.cpp --------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===-----------------------------------------------------------------------===//
//
// This pass tests the tiling graph transformation (Tile.h) by
// applying it to all leaf nodes.
//
//===-----------------------------------------------------------------------===//

#define DEBUG_TYPE "StripMineTest"
#include "Transforms/DFGTransforms/Tile.h"

#include <unordered_map>

#include "IRCodeGen/BuildDFG.h"
#include "CoreHPVM/DFGraph.h"

#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

namespace hpvm {

class StripMineTest : public ModulePass {
public:
  StripMineTest() : ModulePass(ID) {}
  bool runOnModule(Module &M) override;
  void getAnalysisUsage(AnalysisUsage &Info) const override {
    Info.addRequired<builddfg::BuildDFG>();
    // So that it's not regenerated before emitting code
    Info.addPreserved<builddfg::BuildDFG>();
  }
  StringRef getPassName() const override { return "Strip mine test"; }

  static char ID;
private:
  struct NestingInfo {
    unsigned int Level;
    unsigned int LeafInstances;
    unsigned int ParentInstances;
  };

  // Calculates the depth of each leaf in the graph hierarchy
  class LeafDepthVisitor final : public DFNodeVisitor {
  public:
    using DepthMapType = std::unordered_map<DFLeafNode*, NestingInfo>;

    void visit(DFInternalNode *Node) override;
    void visit(DFLeafNode *Node) override;

    const DepthMapType& result() const { return Map; }

    void clear() {
      Map.clear();
      Depth = 0;
    }

  private:
    DepthMapType Map;
    unsigned int Depth = 0; // Current depth in hierarchy
  };

  void applyStripmining(DFInternalNode *Root);

  // Check certain values that indicate that the leaf was transformed properly
  // (depth incremented, block size correct, total instances the same, etc.)
  bool checkNestingDelta(const NestingInfo& Before, const NestingInfo& After);
  void printDebugInfo(const NestingInfo& Before, const NestingInfo& After);
};

void StripMineTest::LeafDepthVisitor::visit(DFInternalNode *Node) {
  ++Depth;
  for (DFGraph::children_iterator It = Node->getChildGraph()->begin(),
       End = Node->getChildGraph()->end(); It != End; ++It) {
    (*It)->applyDFNodeVisitor(*this);
  }
  --Depth;
}

void StripMineTest::LeafDepthVisitor::visit(DFLeafNode *Node) {
  if (Node->isEntryNode() || Node->isExitNode()) return;

  NestingInfo Info { Depth, 0, 0 }; // TODO: Properly get info
  Map.emplace(Node, std::move(Info));
}

bool StripMineTest::runOnModule(Module &M) {
  DFInternalNode *Root = getAnalysis<builddfg::BuildDFG>().getRoot();

  // For getting depth before and after transformation
  LeafDepthVisitor InitialDepths, FinalDepths;

  Root->applyDFNodeVisitor(InitialDepths);
  // applyStripmining(Root);

  // TODO: Take command line input for this
  const unsigned BlockSize = 20;
  DFGTransform::Context TContext(M, M.getContext());
  for (auto &Pair : InitialDepths.result()) {
    if (Pair.first->getNumOfDim() <= 1) {
      Tile Miner(TContext, *Pair.first, BlockSize);
      Miner.checkAndRun();
    } else {
      Tile::BlockSizesType BlockSizes;
      BlockSizes[0] = BlockSize;
      BlockSizes[1] = 10;
      Tile Miner(TContext, *Pair.first, BlockSizes);
      Miner.checkAndRun();
    }
  }

  Root->applyDFNodeVisitor(FinalDepths);

  // Check that the original leaves are nested two levels deeper
  const LeafDepthVisitor::DepthMapType& FinalDepthMap = FinalDepths.result();
  unsigned Failed = 0;
  for (const std::pair<DFLeafNode*, NestingInfo>& OldPair
	 : InitialDepths.result()) {
    const NestingInfo& OldInfo = OldPair.second;
    const NestingInfo& NewInfo = FinalDepthMap.at(OldPair.first);
    if (!checkNestingDelta(OldInfo, NewInfo)) {
      ++Failed;
      printDebugInfo(OldInfo, NewInfo);
    }
  }

  size_t Count = InitialDepths.result().size();
  if (Failed == 0)
    errs() << "Nested " << Count << " nodes\n";
  else
    errs() << "Nested " << (Count - Failed) << " out of " << Count << "\n";

  return true; // Strip mining will eventually modify things
}

char StripMineTest::ID = 0;
static RegisterPass<StripMineTest> X("stripminetest",
  "Pass that applies the strip mining transform to all leaves and then validates",
  false, false);

void StripMineTest::applyStripmining(DFInternalNode *Root) {
  
}

bool StripMineTest::checkNestingDelta(
  const NestingInfo& Before, const NestingInfo& After) {
  const bool LevelIncremented = Before.Level + 1 == After.Level;
  const bool GoodBlockSize = After.LeafInstances == 1; // TODO: Actual blocks
  const unsigned int AfterInstances =
    After.LeafInstances * After.ParentInstances;
  const bool GoodInstanceCount = Before.LeafInstances == AfterInstances;

  return LevelIncremented; // LevelIncremented && GoodBlockSize && GoodInstanceCount;
}

  
void StripMineTest::printDebugInfo(const NestingInfo& Before, const NestingInfo& After) {
  errs() << "Old leaf: level=" << Before.Level <<
    ", instances=" << Before.LeafInstances << "\n";
  errs() << "New leaf: level=" << After.Level <<
    ", instances=" << After.LeafInstances <<
    ", parent instances=" << After.ParentInstances << "\n";
}

} // namespace hpvm
