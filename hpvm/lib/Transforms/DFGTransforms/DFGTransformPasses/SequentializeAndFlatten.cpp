//===-------------------------- SequentializeLeafPass.cpp
//------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===-----------------------------------------------------------------------===//
//
// This pass tests the sequentialize leaf graph transformation
// (SequentializeLeaf.cpp) by applying it to all leaf nodes.
//
//===-----------------------------------------------------------------------===//

#define DEBUG_TYPE "sequentializeandflatten"
#include "Transforms/DFGTransforms/SequentializeAndFlatten.h"

using namespace llvm;
using namespace builddfg;
using namespace dfg2llvm;
using namespace hpvm;

// class LeafSequentializer;

bool SFTraversal::Change = 0;
void SFTraversal::process(DFInternalNode *N) {
  LLVM_DEBUG(errs() << "Processing Inernal Node: " << N->getFuncPointer()->getName()
               << "\n");
  DFGTransform::Context TContext(*(N->getFuncPointer()->getParent()),
                                 N->getFuncPointer()->getContext());
  NodeFlattener *NF = new NodeFlattener(TContext, N);
  Change |= NF->checkAndRun();
}

void SFTraversal::process(DFLeafNode *N) {
  LLVM_DEBUG(errs() << "Processing Leaf Node: " << N->getFuncPointer()->getName()
               << "\n");
  if (N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node!\n");
    return;
  }
  DFGTransform::Context TContext(*(N->getFuncPointer()->getParent()),
                                 N->getFuncPointer()->getContext());
  ValueToValueMapTy VMap;
  LeafSequentializer *LS = new LeafSequentializer(TContext, N, VMap);
  Change |= LS->checkAndRun();
  LS->cleanup();
}

bool SequentializeAndFlatten::runOnModule(Module &M) {

  BuildDFG &DFG = getAnalysis<BuildDFG>();
  std::vector<DFInternalNode *> Roots = DFG.getRoots();

  SFTraversal *SFVisitor = new SFTraversal(M, DFG);

  LLVM_DEBUG(errs() << "Before Opt:\n"; Roots[0]->dump(0, 1, 0););

  int iterations = 0;
  do {
    SFTraversal::Change = 0;
    for (auto Root : Roots) {
      SFVisitor->visit(Root);
    }
    iterations++;
  } while (SFTraversal::Change == 1);
  LLVM_DEBUG(errs() << "Took " << iterations
               << " iterations to finish flattening!\n");
  LLVM_DEBUG(errs() << "After Opt:\n"; Roots[0]->dump(0, 1, 0););
  return true; // Strip mining will eventually modify things
}

bool hpvm::runSequentializeAndFlatten(Module &M, BuildDFG &DFG) {

  std::vector<DFInternalNode *> Roots = DFG.getRoots();

  SFTraversal *SFVisitor = new SFTraversal(M, DFG);

  LLVM_DEBUG(errs() << "Before Opt:\n"; Roots[0]->dump(0, 1, 0););

  int iterations = 0;
  do {
    SFTraversal::Change = 0;
    for (auto Root : Roots) {
      SFVisitor->visit(Root);
    }
    iterations++;
  } while (SFTraversal::Change == 1);
  LLVM_DEBUG(errs() << "Took " << iterations
               << " iterations to finish flattening!\n");
  LLVM_DEBUG(errs() << "After Opt:\n"; Roots[0]->dump(0, 1, 0););
  return true; // Strip mining will eventually modify things
}

char SequentializeAndFlatten::ID = 0;
static RegisterPass<SequentializeAndFlatten> X(
    "sequentializeflatten",
    "Pass that applies the sequentialize leaf and flatten transforms to graph",
    true, true);

