//===------------------------------- FuseTest.cpp --------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===-----------------------------------------------------------------------===//
//
// This pass tests the fusion graph transformation (Fuse.h) by
// fusing the first possible pair.
//
//===-----------------------------------------------------------------------===//

#define DEBUG_TYPE "FuseTest"

#include <vector>

#include "Transforms/DFGTransforms/Fuse.h"
#include "IRCodeGen/BuildDFG.h"
#include "CoreHPVM/DFGraph.h"

#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

namespace hpvm {

class FuseTest : public ModulePass {
public:
  FuseTest() : ModulePass(ID) {}
  bool runOnModule(Module &M) override;
  void getAnalysisUsage(AnalysisUsage &Info) const override {
    Info.addRequired<builddfg::BuildDFG>();
    // So that it's not regenerated before emitting code
    Info.addPreserved<builddfg::BuildDFG>();
  }
  StringRef getPassName() const override { return "Fusion test"; }

  static char ID;
private:
  class LeafCollector final : public DFTreeTraversal
  {
  public:
    void visit(DFLeafNode *Node) override 
    {
      LLVM_DEBUG(errs() << "Found leaf node: " << Node->getFuncPointer()->getName()
            << "\n");
      Nodes.push_back(Node);
    }

    const std::vector<DFLeafNode*> &result() const 
    {
      return Nodes;
    }

  private:
    std::vector<DFLeafNode*> Nodes;
  };
};



bool FuseTest::runOnModule(Module &M) 
{
  unsigned Fusions = 0;

  // Fuse until we can't fuse anymore
  errs() << "FuseTest start\n";
  while (true) {
    DFInternalNode *Root = getAnalysis<builddfg::BuildDFG>().getRoot();
    LeafCollector Collector;
    Root->applyDFNodeVisitor(Collector);
    const std::vector<DFLeafNode*> &Nodes = Collector.result();

    bool DidFuse = false;
    DFGTransform::Context TContext(M, M.getContext());
    for (auto it1 = Nodes.begin(); !DidFuse && it1 != Nodes.end(); ++it1) {
      for (auto it2 = it1 + 1; !DidFuse && it2 != Nodes.end(); ++it2) {
        DFLeafNode *N1 = *it1;
        DFLeafNode *N2 = *it2;
        StringRef name1 = N1->getFuncPointer()->getName();;
        StringRef name2 = N2->getFuncPointer()->getName();
        Fuse Fusor(TContext, { N1, N2 });
        LLVM_DEBUG(errs() << "Testing " << name1 << " and " << name2 << "\n");
        if (Fusor.canRun()) {
          LLVM_DEBUG(errs() << "Fusing " << name1 << " and " << name2 << "\n");
          Fusor.run();
          DidFuse = true;
          ++Fusions;
          it1 = Nodes.end();
          it2 = Nodes.end();
        }
      }
    }
    if (!DidFuse) break;
  }

  LLVM_DEBUG(errs() << "FuseTest: Did " << Fusions << " fusions\n");
  LLVM_DEBUG(errs() << "Dumping current status\n");
  getAnalysis<builddfg::BuildDFG>().getRoot()->dump(true, true, true);
  return true;
}

char FuseTest::ID = 0;
static RegisterPass<FuseTest> X("fusetest",
  "Test pass that looks for a fusable set of nodes and fuses them.",
  false, false);


} // namespace hpvm
