//===-------------------------- Priv.cpp --------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "ArgPriv"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Constant.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/BasicAliasAnalysis.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#include "llvm/Analysis/IVUsers.h"
#include "SupportHPVM/DFGTreeTraversal.h"
#include "Transforms/Utils/HPVMBufferingPrivatization.h"

#include <map>

using namespace llvm;
using namespace builddfg;
using namespace dfg2llvm;
using namespace hpvm;

#define LOCAL_ARRAY_SIZE 120

namespace {
// ArgPriv - The first implementation.
// This pass promotes arrays from gloabl FPGA memory to local memory if they 
// are not used outside the leaf node. 
// This is a driver pass that uses the utility functions

struct ArgPriv : public ModulePass {
  static char ID; // Pass identification, replacement for typeid
  ArgPriv() : ModulePass(ID) {}

private:
  // Member variables

  // Functions

public:
  bool runOnModule(Module &M);

  void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<BuildDFG>();
    AU.addPreserved<BuildDFG>();
  }
};

// Visitor for Code generation traversal (tree traversal for now)
class TT_AP : public DFGTreeTraversal {

private:
  //Member variables
  
  //Functions

  // Virtual Functions
  void init() {}
  void initRuntimeAPI() {}
  void process(DFInternalNode* N);
  void process(DFLeafNode* N);

public:
  // Constructor
  TT_AP(Module &_M, BuildDFG &_DFG) : 
    DFGTreeTraversal(_M, _DFG) {}

};
bool ArgPriv::runOnModule(Module &M) {
  errs() << "\nArgPriv PASS\n";

// Get the BuildDFG Analysis Results:
  // - Dataflow graph
  // - Maps from i8* hansles to DFNode and DFEdge
  BuildDFG &DFG = getAnalysis<BuildDFG>();

  std::vector<DFInternalNode*> Roots = DFG.getRoots();

  // Visitor for Code Generation Graph Traversal
  TT_AP *TTVisitor = new TT_AP(M, DFG);

  // Iterate over all the DFGs and produce code for each one of them
  for (auto rootNode: Roots) {
    // Initiate code generation for root DFNode
    TTVisitor->visit(rootNode);
  }

  delete TTVisitor;
  return true;
}

void TT_AP::process(DFInternalNode* N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName() << "\n");
}

// Code generation for leaf nodes
void TT_AP::process(DFLeafNode* N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName() << "\n");
  // Skip code generation if it is a dummy node
  if(N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node\n");
    return;
  }

  // We are only interested in bufferin the arguments of nodes which
  // are targetted on the FPGA.
  if (N->getTargetHint() !=  hpvm::FPGA_TARGET) {
    LLVM_DEBUG(errs() << "Skipping non-fpga target node!\n");
    return;
  }

  // Privatize the node's arguments.
  privatizeNodeArguments(N);
  
}

// Function to promote a private pointer argument to local memory.

}
char ArgPriv::ID = 0;
static RegisterPass<ArgPriv> X("argpriv",
    "Pass to promote privatizable arrays from global to local memory for FPGAs", 
    true /* modifes the CFG */,
    true /* transformation, not just analysis */);
