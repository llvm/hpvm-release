//===------------------------- BufferIn.cpp ------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "llvm/Support/CommandLine.h"
#define DEBUG_TYPE "BufferIn"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Constant.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/BasicAliasAnalysis.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#include "llvm/Analysis/IVUsers.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "SupportHPVM/DFGTreeTraversal.h"
#include "Transforms/Utils/HPVMBufferingPrivatization.h"
#include "Transforms/Opts/FPGATransforms.h"

#include <map>

using namespace llvm;
using namespace builddfg;
using namespace dfg2llvm;
using namespace hpvm;

// MaxIBSize - Input Buffering size threshold
static cl::opt<unsigned> MaxIBSize("ib-size",
                               cl::desc("Maximum size for input to buffer"),
                               cl::value_desc("max IB size"), cl::init(12000),
                               cl::cat(FPGATransformsCat));

namespace {
// BufferIn
// This pass copies input arrays to local memory before they are used

struct BufferIn : public ModulePass {
  static char ID; // Pass identification, replacement for typeid
  BufferIn() : ModulePass(ID) {}

private:
  // Member variables

  // Functions

public:
  bool runOnModule(Module &M);

  void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<BuildDFG>();
    AU.addPreserved<BuildDFG>();
  }
};

class TT_BI : public DFGTreeTraversal {
private:
  const DataLayout &DL;

  // Virtual Functions
  void init() {}
  void initRuntimeAPI() {}
  void process(DFInternalNode *N);
  void process(DFLeafNode *N);

public:
  TT_BI(Module &_M, BuildDFG &_DFG)
      : DFGTreeTraversal(_M, _DFG), DL(_M.getDataLayout()) {}
};

bool BufferIn::runOnModule(Module &M) {
  errs() << "\nFPGABUFFERIN PASS\n";

  // Same code as FPGABufferOutput to traverse over all DFGs and perform code
  // generation
  BuildDFG &DFG = getAnalysis<BuildDFG>();

  std::vector<DFInternalNode *> Roots = DFG.getRoots();

  // Visitor for Code Generation Graph Traversal
  TT_BI *CGTVisitor = new TT_BI(M, DFG);

  // Iterate over all the DFGs and run input buffer on each one
  for (auto rootNode : Roots) {
    CGTVisitor->visit(rootNode);
  }

  delete CGTVisitor;
  return true;
}

void TT_BI::process(DFInternalNode *N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName() << "\n");
}

void TT_BI::process(DFLeafNode *N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName() << "\n");
  // Skip dummy node
  if (N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node\n");
    return;
  }

  // We are only interested in bufferin the arguments of nodes which
  // are targetted on the FPGA.
  if (N->getTargetHint() !=  hpvm::FPGA_TARGET) {
    LLVM_DEBUG(errs() << "Skipping non-fpga target node!\n");
    return;
  }

  // Buffer the arguments of the node function.
  hpvm::bufferNodeInputs(N, DL, MaxIBSize);  

}

char BufferIn::ID = 0;
static RegisterPass<BufferIn>
    X("bufferin", "Pass to copy input to local memory on FPGA",
      true /* modifes the CFG */, true /* transformation, not just analysis */);
} // namespace
