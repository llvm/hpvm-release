//===-------------------------- LocalMem.cpp --------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "llvm/Passes/PassBuilder.h"
#define DEBUG_TYPE "UnrollAndJam"
#include "llvm/IR/Intrinsics.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Constant.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopIterator.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Transforms/Utils/UnrollLoop.h"
#include "llvm/Transforms/Utils/LoopUtils.h"
#include "llvm/Analysis/IVUsers.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Analysis/DependenceAnalysis.h"
#include "llvm/Support/GraphWriter.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "IRCodeGen/BuildDFG.h"
#include "SupportHPVM/DFGTreeTraversal.h"
#include "Transforms/Utils/HPVMLoopTransforms.h"
#include "Transforms/Opts/FPGATransforms.h"
#include <map>
#include <set>
#include <vector>

#define LOCAL_ARRAY_SIZE 120
#define THRESHOLD 3
#define PRINTDBG 1

using namespace llvm;
using namespace builddfg;
using namespace dfg2llvm;
using namespace hpvm;

static std::vector<Function *> functionWorklist;

// ut - Unroll threshold
static cl::opt<unsigned> UnrollThreshold("ut",
                               cl::desc("Loop Unroll Threshold"),
                               cl::value_desc("unroll threshold"), cl::init(3),
                               cl::cat(FPGATransformsCat));
namespace hpvmuj{

///////////////////////////////////////////////////////////////////
// main HPVM DFG traversal class for FPGA Unroll And Jam passes ///
///////////////////////////////////////////////////////////////////
class TT_UJ : public DFGTreeTraversal {

private:
  // Member variables

  // Functions

  // Virtual Functions
  void init() {}
  void initRuntimeAPI() {}
  void process(DFInternalNode *N);
  void process(DFLeafNode *N);

public:
  // Constructor
  TT_UJ(Module &_M, BuildDFG &_DFG) : DFGTreeTraversal(_M, _DFG) {}
};

//////////////////////////////////////////////
// FPGA Unroll And Jam - Unroll module pass //
//////////////////////////////////////////////
struct HPVMUnrollAndJam : public ModulePass {
  static char ID; // Pass identification, replacement for typeid
  HPVMUnrollAndJam() : ModulePass(ID) {}

private:
  // Member variables

  // Functions

public:
  bool runOnModule(Module &M);

  void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<BuildDFG>();
    AU.addPreserved<BuildDFG>();
  }

  StringRef getPassName() const { return StringRef("FUJUnroll"); }
};
// Helper functions

static void dumpVector(std::vector<int> v1, std::string name) {
  LLVM_DEBUG(errs() << name << ": { ");
  for (unsigned i = 0; i < v1.size(); ++i) {
    LLVM_DEBUG(errs() << v1[i] << " ");
  }
  LLVM_DEBUG(errs() << "}\n");
}

bool HPVMUnrollAndJam::runOnModule(Module &M) {
  errs() << "\nFUJ-Unroll PASS\n";

  // Get the BuildDFG Analysis Results:
  // - Dataflow graph
  // - Maps from i8* hansles to DFNode and DFEdge
  BuildDFG &DFG = getAnalysis<BuildDFG>();

  std::vector<DFInternalNode *> Roots = DFG.getRoots();

  // Visitor for Code Generation Graph Traversal
  TT_UJ *CGTVisitor = new TT_UJ(M, DFG);

  // Iterate over all the DFGs and produce code for each one of them
  for (auto rootNode : Roots) {
    // Initiate code generation for root DFNode
    CGTVisitor->visit(rootNode);
  }

  return true;
}

void TT_UJ::process(DFInternalNode *N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName() << "\n");
}

// Code generation for leaf nodes
void TT_UJ::process(DFLeafNode *N) {
  LLVM_DEBUG(errs() << "Analysing Node: " << N->getFuncPointer()->getName() << "\n");
  // Skip code generation if it is a dummy node
  if (N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node\n");
    return;
  }

  // We are only interested in unrolling the loops of nodes which
  // are targetted on the FPGA.
  if (N->getTargetHint() !=  hpvm::FPGA_TARGET) {
    LLVM_DEBUG(errs() << "Skipping non-fpga target node!\n");
    return;
  }

  Function *F = N->getFuncPointer();
  
  //FunctionAnalysisManager FAM;
  //PassBuilder PB;
  //PB.registerFunctionAnalyses(FAM);
  //FunctionPassManager FPM;
  //FPM.addPass(EarlyCSEPass(false));
  //FPM.addPass(SimplifyCFGPass());
  //FPM.addPass(LoopSimplifyPass());
  //FPM.run(*F, FAM);
  //auto &LI = FAM.getResult<LoopAnalysis>(*F);
  //auto &DT = FAM.getResult<DominatorTreeAnalysis>(*F);
  //auto &SE = FAM.getResult<ScalarEvolutionAnalysis>(*F);
  //auto &AC = FAM.getResult<AssumptionAnalysis>(*F);
  // Remove all the loop guard branches from this function
  removeFuncLoopGuardBranches(F);

  // Unroll the function loops that are less than threshold fully
  unrollFuncLoops(F, UnrollThreshold, 0, 0, 1);

  // Run greedy fusion on all the loop nests
  fuseFuncLoops(F);

}

char HPVMUnrollAndJam::ID = 0;
static RegisterPass<HPVMUnrollAndJam>
    Y("hpvm-unrollandjam", "Pass to unroll and fuse loops for FPGA target",
      true /* modifes the CFG */, true /* transformation, not just analysis */);
} // namespace
