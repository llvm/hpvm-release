//=== DFG2LLVM_FPGA.cpp ===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This pass is responsible for generating code for host code and kernel code
// for FPGA target using HPVM dataflow graph.
//
//===----------------------------------------------------------------------===//
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Transforms/Scalar/ADCE.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include <string>
#define ENABLE_ASSERTS
#define TARGET_PTX 32
#define GENERIC_ADDRSPACE 0
#define GLOBAL_ADDRSPACE 1
#define SHARED_ADDRSPACE 3

#define DEBUG_TYPE "DFG2LLVM_FPGA"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Bitcode/BitcodeReader.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Attributes.h"
#include "SupportHPVM/HPVMTimer.h"
#include "CoreHPVM/DFG2LLVM.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm-c/Core.h"

#include "CoreHPVM/HPVMUtils.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/IR/UseListOrder.h"
#include "llvm/IR/Mangler.h"

#include "Transforms/DFGTransforms/SequentializeLeaf.h"

#include <sstream>

#ifndef LLVM_BUILD_DIR
#error LLVM_BUILD_DIR is not defined
#endif

#define STR_VALUE(X) #X
#define STRINGIFY(X) STR_VALUE(X)
#define LLVM_BUILD_DIR_STR STRINGIFY(LLVM_BUILD_DIR)

using namespace llvm;
using namespace builddfg;
using namespace dfg2llvm;
using namespace hpvmUtils;
using namespace hpvm;

// HPVM Command line option to use timer or not
static cl::opt<bool> HPVMTimer_FPGA("hpvm-timers-fpga",
                                    cl::desc("Enable hpvm timers"));
// HPVM Command line option to enable TLP
static cl::opt<bool> HPVM_FPGA_TLP("hpvm-fpga-tlp", cl::desc("Enable TLP"),
                                   cl::init(true));
// HPVM Command line option to enable emulation
static cl::opt<bool> HPVM_FPGA_EMULATION("hpvm-fpga-emu",
                                         cl::desc("Enable Emulation"),
                                         cl::init(false));
// HPVM Command line option to disable ivdep
static cl::opt<bool>
    HPVM_FPGA_DISABLE_IVDEP("hpvm-fpga-disable-ivdep",
                            cl::desc("Disable ivdep insertion"),
                            cl::init(false));

namespace dfg2llvm {

bool EnableTLP;
bool Emulation;
const char *Board;

// Helper class declarations

// Class to maintain the tuple of host pointer, device pointer and size
// in bytes. Would have preferred to use tuple but support not yet available
namespace {
class OutputPtr {
public:
  OutputPtr(Value *_h_ptr, Value *_d_ptr, Value *_bytes)
      : h_ptr(_h_ptr), d_ptr(_d_ptr), bytes(_bytes) {}

  Value *h_ptr;
  Value *d_ptr;
  Value *bytes;
};

bool isHPVMChannelIntrinsic(Instruction *I) {
  if (!isa<IntrinsicInst>(I))
    return false;
  IntrinsicInst *II = dyn_cast<IntrinsicInst>(I);
  return II->getCalledFunction()->getName().startswith("llvm.hpvm.ch");
}

// Class to maintain important kernel info required for generating runtime calls
class Kernel {
public:
  Kernel(
      Function *_KF, DFLeafNode *_KLeafNode,
      std::map<unsigned, unsigned> _inArgMap = std::map<unsigned, unsigned>(),
      std::map<unsigned, std::pair<Value *, unsigned>> _sharedInArgMap =
          std::map<unsigned, std::pair<Value *, unsigned>>(),
      std::vector<unsigned> _outArgMap = std::vector<unsigned>(),
      unsigned _gridDim = 0,
      std::vector<Value *> _globalWGSize = std::vector<Value *>(),
      unsigned _blockDim = 0,
      std::vector<Value *> _localWGSize = std::vector<Value *>())
      : KernelFunction(_KF), KernelLeafNode(_KLeafNode), inArgMap(_inArgMap),
        sharedInArgMap(_sharedInArgMap), outArgMap(_outArgMap),
        gridDim(_gridDim), globalWGSize(_globalWGSize), blockDim(_blockDim),
        localWGSize(_localWGSize), event(-1) {

    assert(gridDim == globalWGSize.size() &&
           "gridDim should be same as the size of vector globalWGSize");
    assert(blockDim == localWGSize.size() &&
           "blockDim should be same as the size of vector localWGSize");
  }

  Function *KernelFunction;
  DFLeafNode *KernelLeafNode;
  std::map<unsigned, unsigned> inArgMap;
  // Map for shared memory arguments
  std::map<unsigned, std::pair<Value *, unsigned>> sharedInArgMap;
  // Fields for (potential) allocation node
  DFLeafNode *AllocationNode;
  Function *AllocationFunction;
  std::map<unsigned, unsigned> allocInArgMap;

  std::map<unsigned, unsigned> returnArgMap;

  std::vector<unsigned> outArgMap;
  unsigned gridDim;
  std::vector<Value *> globalWGSize;
  unsigned blockDim;
  std::vector<Value *> localWGSize;
  std::vector<int> localDimMap;

  std::map<unsigned, unsigned> &getInArgMap() { return inArgMap; }
  void setInArgMap(std::map<unsigned, unsigned> map) { inArgMap = map; }

  std::map<unsigned, std::pair<Value *, unsigned>> &getSharedInArgMap() {
    return sharedInArgMap;
  }
  void setSharedInArgMap(std::map<unsigned, std::pair<Value *, unsigned>> map) {
    sharedInArgMap = map;
  }

  std::vector<unsigned> &getOutArgMap() { return outArgMap; }
  void setOutArgMap(std::vector<unsigned> map) { outArgMap = map; }

  void setReturnArgMap(std::map<unsigned, unsigned> map) { returnArgMap = map; }
  std::map<unsigned, unsigned> &getReturnArgMap() { return returnArgMap; }
  void setLocalWGSize(std::vector<Value *> V) { localWGSize = V; }

  bool hasLocalWG() { return blockDim != 0; }

  // Set the output and input event for each kernel
  int event;
  std::set<int> waitlist;
};
} // namespace

// Helper function declarations
static void getExecuteNodeParams(Module &M, Value *&, Value *&, Value *&,
                                 Kernel *, ValueToValueMapTy &, Instruction *);
static Value *genWorkGroupPtr(Module &M, std::vector<Value *>,
                              ValueToValueMapTy &, Instruction *,
                              const Twine &WGName = "WGSize");
static std::string getFilenameFromModule(const Module &M);
static void changeDataLayout(Module &);
static void changeTargetTriple(Module &);
static std::string printType(Type *);
// static StringRef demangle(const char* name)
static StringRef getMangledName(std::string);
static StringRef getAtomicMangledName(std::string, unsigned, bool);
static void findReturnInst(Function *, std::vector<ReturnInst *> &);
static void findIntrinsicInst(Function *, Intrinsic::ID,
                              std::vector<IntrinsicInst *> &);
static StringRef getAtomicOpName(Intrinsic::ID, unsigned);
static std::string getMathFunctionName(Intrinsic::ID);

// Visitor for Code generation traversal (tree traversal for now)
class CGT_FPGA : public CodeGenTraversal {

private:
  // Member variables
  std::unique_ptr<Module> KernelM;
  //  DFNode *KernelLaunchNode = nullptr;
  //  Kernel *kernel;
  std::map<DFNode *, std::pair<Kernel *, DFNode *>> kernelLaunchMap;
  std::map<DFNode *, Kernel *> kernelMap; // TODO: merge the two maps together.

  // Map to map outputs to argument variables
  std::map<unsigned, unsigned> returnArgMap;

  // Map to keep track of which functions use channels
  std::map<Function *, bool> ChannelsMap;
  std::vector<Value *> KernelsNeedWait;

  // Kernel Module Name
  std::string KernelModuleName;
  // Bitstream File Name
  std::string BitstreamFileName;
  // HPVM Runtime API
  FunctionCallee llvm_hpvm_ocl_launch;
  FunctionCallee llvm_hpvm_ocl_wait;
  FunctionCallee llvm_hpvm_ocl_initContext;
  FunctionCallee llvm_hpvm_ocl_clearContext;
  FunctionCallee llvm_hpvm_ocl_argument_shared;
  FunctionCallee llvm_hpvm_ocl_argument_scalar;
  FunctionCallee llvm_hpvm_ocl_argument_ptr;
  FunctionCallee llvm_hpvm_ocl_output_ptr;
  FunctionCallee llvm_hpvm_ocl_getOutput;
  FunctionCallee llvm_hpvm_ocl_executeNode;

  FunctionCallee llvm_hpvm_cpu_argument_ptr;
  FunctionCallee llvm_hpvm_cpu_getDimInstance;
  FunctionCallee llvm_hpvm_cpu_getDimLimit;
  // Functions
  // std::string getKernelsModuleName(Module &M);
  std::string getFPGAFilename(const Module &);
  void fixValueAddrspace(Value *V, unsigned addrspace);
  Function *changeArgAddrspace(Function *F, std::vector<unsigned> &Args,
                               unsigned i);
  void addCLMetadata(Function *F);
  Function *transformFunctionToVoid(Function *F);
  void insertRuntimeCalls(DFInternalNode *N, Kernel *K, const Twine &FileName,
                          ValueToValueMapTy &VMap);
  void moveExecNodes(Function *F, Instruction *RI);

  // Virtual Functions
  void init() {
    HPVMTimer = HPVMTimer_FPGA;
    TargetName = "FPGA";
  }
  void initRuntimeAPI();
  void codeGen(DFInternalNode *N);
  void codeGen(DFLeafNode *N);
  void codeGenCPU(DFLeafNode *N);

  // Global event count
  int event;

public:
  // Constructor
  CGT_FPGA(Module &_M, BuildDFG &_DFG, std::string KMN = "",
           std::string BFM = "")
      : CodeGenTraversal(_M, _DFG), KernelM(CloneModule(_M)),
        KernelModuleName(KMN), BitstreamFileName(BFM), event(0) {
    LLVM_DEBUG(errs() << "In CGT_FGPA Constructor\n");
    //    KernelLaunchNode = NULL;
    LLVM_DEBUG(errs() << "Calling init()\n");
    init();
    LLVM_DEBUG(errs() << "Calling initRuntimeAPI()");
    initRuntimeAPI();
    LLVM_DEBUG(errs() << "Old module pointer: " << &_M << "\n";);
    LLVM_DEBUG(errs() << "New module pointer: " << KernelM.get() << "\n";);
    // Copying instead of creating new, in order to preserve required info
    // (metadata) Remove functions, global variables and aliases
    LLVM_DEBUG(errs() << "Extracting global variables\n");
    std::vector<GlobalVariable *> gvv = std::vector<GlobalVariable *>();
    for (Module::global_iterator mi = KernelM->global_begin(),
                                 me = KernelM->global_end();
         (mi != me); ++mi) {
      GlobalVariable *gv = &*mi;
      LLVM_DEBUG(errs() << *gv << "\n");
      gvv.push_back(gv);
    }
    for (std::vector<GlobalVariable *>::iterator vi = gvv.begin();
         vi != gvv.end(); ++vi) {
      (*vi)->replaceAllUsesWith(UndefValue::get((*vi)->getType()));
      (*vi)->eraseFromParent();
    }

    LLVM_DEBUG(errs() << "Extracting Functions\n");
    std::vector<Function *> fv = std::vector<Function *>();
    for (Module::iterator mi = KernelM->begin(), me = KernelM->end();
         (mi != me); ++mi) {
      Function *f = &*mi;
      LLVM_DEBUG(errs() << f->getName() << "\n");
      fv.push_back(f);
    }
    for (std::vector<Function *>::iterator vi = fv.begin(); vi != fv.end();
         ++vi) {
      (*vi)->replaceAllUsesWith(UndefValue::get((*vi)->getType()));
      (*vi)->eraseFromParent();
    }

    LLVM_DEBUG(errs() << "Extracting Global Aliases\n");
    std::vector<GlobalAlias *> av = std::vector<GlobalAlias *>();
    for (Module::alias_iterator mi = KernelM->alias_begin(),
                                me = KernelM->alias_end();
         (mi != me); ++mi) {
      GlobalAlias *a = &*mi;
      LLVM_DEBUG(errs() << *a << "\n");
      av.push_back(a);
    }
    for (std::vector<GlobalAlias *>::iterator vi = av.begin(); vi != av.end();
         ++vi) {
      (*vi)->replaceAllUsesWith(UndefValue::get((*vi)->getType()));
      (*vi)->eraseFromParent();
    }

    changeDataLayout(*KernelM);
    changeTargetTriple(*KernelM);

    // Check if function uses channels and populate map
    // TODO: rewrite this to use Users instead
    for (auto mi = _M.begin(); mi != _M.end(); ++mi) {
      Function *f = &*mi;
      LLVM_DEBUG(errs() << f->getName() << "\n");
      bool hasChannels = false;
      for (auto i = inst_begin(f); i != inst_end(f); ++i) {
        Instruction *I = &*i;
        LLVM_DEBUG(errs() << *I << "\n");
        if (isHPVMChannelIntrinsic(I)) {
          hasChannels = true;
          break;
        }
      }
      ChannelsMap[f] = hasChannels;
    }
    LLVM_DEBUG(errs() << "Printing Kernel Module");
    LLVM_DEBUG(errs() << *KernelM);
  }

  virtual void visit(DFInternalNode *N) override;

  void removeLLVMIntrinsics();
  Module *getKernelM() { return KernelM.get(); }
  std::string getKernelModuleName() { return KernelModuleName; }
};

// Initialize the HPVM runtime API. This makes it easier to insert these calls
void CGT_FPGA::initRuntimeAPI() {

  // Load Runtime API Module
  SMDiagnostic Err;

  std::string runtimeAPI = std::string(LLVM_BUILD_DIR_STR) +
                           "/tools/hpvm/projects/hpvm-rt/hpvm-rt.bc";

  runtimeModule = parseIRFile(runtimeAPI, Err, M.getContext());
  if (runtimeModule == nullptr) {
    LLVM_DEBUG(errs() << Err.getMessage() << " " << runtimeAPI << "\n");
    assert(false && "couldn't parse runtime");
  } else
    LLVM_DEBUG(errs() << "Successfully loaded hpvm-rt API module\n");

  // Get or insert the global declarations for launch/wait functions
  DECLARE(llvm_hpvm_ocl_launch);
  DECLARE(llvm_hpvm_ocl_wait);
  DECLARE(llvm_hpvm_ocl_initContext);
  DECLARE(llvm_hpvm_ocl_clearContext);
  DECLARE(llvm_hpvm_ocl_argument_shared);
  DECLARE(llvm_hpvm_ocl_argument_scalar);
  DECLARE(llvm_hpvm_ocl_argument_ptr);
  DECLARE(llvm_hpvm_ocl_output_ptr);
  DECLARE(llvm_hpvm_ocl_getOutput);
  DECLARE(llvm_hpvm_ocl_executeNode);

  DECLARE(llvm_hpvm_cpu_argument_ptr);
  DECLARE(llvm_hpvm_cpu_getDimInstance);
  DECLARE(llvm_hpvm_cpu_getDimLimit);
  DECLARE(llvm_hpvm_cpu_dstack_push);
  DECLARE(llvm_hpvm_cpu_dstack_pop);

  // Get or insert timerAPI functions as well if you plan to use timers
  initTimerAPI();

  // Insert init context in main
  LLVM_DEBUG(errs() << "Gen Code to initialize FPGA Timer\n");
  Function *VI = M.getFunction("llvm.hpvm.init");
  LLVM_DEBUG(errs() << *VI << "\n");
  assert(VI->getNumUses() == 1 && "__hpvm__init should only be used once");

  InitCall = cast<Instruction>(*VI->user_begin());
  initializeTimerSet(InitCall);
  switchToTimer(hpvm_TimerID_INIT_CTX, InitCall);
  Value *TargetID = getTargetID(M, hpvm::FPGA_TARGET);
  Value *Emu = ConstantInt::get(Type::getInt1Ty(M.getContext()), Emulation);
  Value *InitContextArgs[] = {TargetID, Emu};
  CallInst::Create(llvm_hpvm_ocl_initContext,
                   ArrayRef<Value *>(InitContextArgs), "", InitCall);
  switchToTimer(hpvm_TimerID_NONE, InitCall);

  // Insert print instruction at hpvm exit
  LLVM_DEBUG(errs() << "Gen Code to print FPGA Timer\n");
  Function *VC = M.getFunction("llvm.hpvm.cleanup");
  LLVM_DEBUG(errs() << *VC << "\n");
  assert(VC->getNumUses() == 1 && "__hpvm__cleanup should only be used once");

  CleanupCall = cast<Instruction>(*VC->user_begin());
  printTimerSet(CleanupCall);
}

// Generate Code to call the kernel
// The plan is to replace the internal node with a leaf node. This method is
// used to generate a function to associate with this leaf node. The function
// is responsible for all the memory allocation/transfer and invoking the
// kernel call on the device
void CGT_FPGA::insertRuntimeCalls(DFInternalNode *N, Kernel *K,
                                  const Twine &FileName,
                                  ValueToValueMapTy &VMap) {
  // Check if clone already exists. If it does, it means we have visited this
  // function before.
  //  assert(N->getGenFunc() == NULL && "Code already generated for this node");

  bool hasChannels = ChannelsMap[K->KernelFunction];
  ConstantInt *hasChannelsVal =
      ConstantInt::get(Type::getInt1Ty(M.getContext()), hasChannels);

  // Useful values
  Value *True = ConstantInt::get(Type::getInt1Ty(M.getContext()), 1);
  Value *False = ConstantInt::get(Type::getInt1Ty(M.getContext()), 0);

  // If kernel struct has not been initialized with kernel function, then fail
  assert(K != NULL && "No kernel found!!");

  LLVM_DEBUG(errs() << "Generating kernel call code\n");
  Function *F_CPU = N->getGenFuncForTarget(hpvm::FPGA_TARGET);

  BasicBlock *BB = &*F_CPU->begin();
  ReturnInst *RI = cast<ReturnInst>(BB->getTerminator());
  Instruction *InsertionPoint = RI;

  Function *KF = K->KernelLeafNode->getFuncPointer();

  LLVM_DEBUG(errs() << "Initializing commandQ\n");
  // Initialize command queue
  switchToTimer(hpvm_TimerID_SETUP, InitCall);
  Value *fileStr = getStringPointer(FileName, InitCall, "Filename");
  LLVM_DEBUG(errs() << "Kernel Filename constant: " << *fileStr << "\n");
  LLVM_DEBUG(errs() << "Generating code for kernel - "
                    << K->KernelFunction->getName() << "\n");
  Value *kernelStr =
      getStringPointer(K->KernelFunction->getName(), InitCall, "KernelName");

  Value *TLP = ConstantInt::get(Type::getInt1Ty(M.getContext()), EnableTLP);

  Value *BRD = getStringPointer(Board, InitCall, "Board");

  Value *LaunchInstArgs[] = {
      fileStr,        kernelStr, getTargetID(M, hpvm::FPGA_TARGET),
      hasChannelsVal, TLP,       BRD};

  LLVM_DEBUG(errs() << "Inserting launch call"
                    << "\n");
  CallInst *FPGA_Ctx =
      CallInst::Create(llvm_hpvm_ocl_launch, ArrayRef<Value *>(LaunchInstArgs),
                       "graph" + KF->getName(), InitCall);
  LLVM_DEBUG(errs() << *FPGA_Ctx << "\n");
  GraphIDAddr = new GlobalVariable(M, FPGA_Ctx->getType(), false,
                                   GlobalValue::CommonLinkage,
                                   Constant::getNullValue(FPGA_Ctx->getType()),
                                   "graph" + KF->getName() + ".addr");
  LLVM_DEBUG(errs() << "Store at: " << *GraphIDAddr << "\n");
  StoreInst *SI = new StoreInst(FPGA_Ctx, GraphIDAddr, InitCall);
  LLVM_DEBUG(errs() << *SI << "\n");
  switchToTimer(hpvm_TimerID_NONE, InitCall);
  switchToTimer(hpvm_TimerID_SETUP, InsertionPoint);
  Value *GraphID = new LoadInst(FPGA_Ctx->getType(), GraphIDAddr,
                                "graph." + KF->getName(), InsertionPoint);
  LLVM_DEBUG(errs() << *GraphID << "\n");

  // Iterate over the required input edges of the node and use the hpvm-rt API
  // to set inputs
  LLVM_DEBUG(
      errs() << "Iterate over input edges of node and insert hpvm api\n");
  std::vector<OutputPtr> OutputPointers;
  // Vector to hold the device memory object that need to be cleared before we
  // release context
  std::vector<Value *> DevicePointers;

  std::map<unsigned, unsigned> kernelInArgMap = K->getInArgMap();

  // Add map for args to device pointer
  std::map<unsigned, Value *> argToPtrMap;

  //  for (std::map<unsigned, unsigned>::iterator ib = kernelInArgMap.begin(),
  //                                              ie = kernelInArgMap.end();
  //       ib != ie; ++ib) {
  //    unsigned i = ib->first;
  //    Value *inputVal = getArgumentAt(F_CPU, ib->second);
  for (unsigned i = 0; i < KF->getFunctionType()->getNumParams(); ++i) {
    Value *inputVal = getInValueAt(K->KernelLeafNode, i, F_CPU, InsertionPoint);
    std::string ivalName = (inputVal->getName() != "")
                               ? inputVal->getName().str()
                               : (KF->arg_begin() + i)->getName().str();
    LLVM_DEBUG(errs() << "\tArgument " << i << " = " << *inputVal << "\n");

    // input value has been obtained.
    // Check if input is a scalar value or a pointer operand
    // For scalar values such as int, float, etc. the size is simply the size of
    // type on target machine, but for pointers, the size of data would be the
    // next integer argument
    if (inputVal->getType()->isPointerTy()) {

      switchToTimer(hpvm_TimerID_COPY_PTR, InsertionPoint);
      // Pointer Input
      // CheckAttribute
      Value *isOutput = (hasAttribute(KF, i, Attribute::Out) ||
                         hasAttribute(KF, i, Attribute::BufferOut))
                            ? True
                            : False;
      Value *isInput = ((hasAttribute(KF, i, Attribute::Out) ||
                         hasAttribute(KF, i, Attribute::BufferOut)) &&
                        !(hasAttribute(KF, i, Attribute::In) ||
                          hasAttribute(KF, i, Attribute::BufferIn)))
                           ? False
                           : True;

      //      Argument *A = getArgumentAt(KF, i);
      //      if (isOutput == True) {
      //        LLVM_DEBUG(errs() << *A << " is an OUTPUT argument\n");
      //      }
      //      if (isInput == True) {
      //        LLVM_DEBUG(errs() << *A << " is an INPUT argument\n");
      //      }

      Value *inputValI8Ptr = CastInst::CreatePointerCast(
          inputVal, Type::getInt8PtrTy(M.getContext()), ivalName + ".i8ptr",
          InsertionPoint);

      // Assert that the pointer argument size (next argument) is in the map
      assert(kernelInArgMap.find(i + 1) != kernelInArgMap.end());

      //      Value *inputSize = getArgumentAt(F_CPU, kernelInArgMap[i + 1]);
      Value *inputSize = getInValueAt(
          K->KernelLeafNode, i + 1, F_CPU,
          InsertionPoint); // getArgumentAt(F_CPU, kernelInArgMap[i + 1]);

      assert(
          inputSize->getType() == Type::getInt64Ty(M.getContext()) &&
          "Pointer type input must always be followed by size (integer type)");
      Value *setInputArgs[] = {
          GraphID,
          inputValI8Ptr,
          ConstantInt::get(Type::getInt32Ty(M.getContext()), i),
          inputSize,
          isInput,
          isOutput};
      Value *d_ptr = CallInst::Create(llvm_hpvm_ocl_argument_ptr,
                                      ArrayRef<Value *>(setInputArgs, 6), "",
                                      InsertionPoint);
      DevicePointers.push_back(d_ptr);
      // If this has out attribute, store the returned device pointer in
      // memory to read device memory later
      if (isOutput == True)
        OutputPointers.push_back(OutputPtr(inputValI8Ptr, d_ptr, inputSize));
    } else {
      switchToTimer(hpvm_TimerID_COPY_SCALAR, InsertionPoint);
      // Scalar Input
      // Store the scalar value on stack and then pass the pointer to its
      // location
      AllocaInst *inputValPtr = new AllocaInst(
          inputVal->getType(), 0, ivalName + ".ptr", InsertionPoint);
      StoreInst *SI = new StoreInst(inputVal, inputValPtr, InsertionPoint);

      Value *inputValI8Ptr = CastInst::CreatePointerCast(
          inputValPtr, Type::getInt8PtrTy(M.getContext()), ivalName + ".i8ptr",
          InsertionPoint);

      Value *setInputArgs[] = {
          GraphID, inputValI8Ptr,
          ConstantInt::get(Type::getInt32Ty(M.getContext()), i),
          ConstantExpr::getSizeOf(inputVal->getType())};
      CallInst::Create(llvm_hpvm_ocl_argument_scalar,
                       ArrayRef<Value *>(setInputArgs, 4), "", InsertionPoint);
    }
    argToPtrMap[i] = inputVal;
  }

  assert(K->getSharedInArgMap().size() == 0 &&
         "Shared Memory Arguments not supported on FPGA Backend!\n");

  LLVM_DEBUG(errs() << "Setup output edges of node and insert hpvm api\n");

  // Set output if struct is not an empty struct
  StructType *OutputTy = K->KernelLeafNode->getOutputType();
  std::vector<Value *> d_Outputs;
  if (!OutputTy->isEmptyTy()) {
    switchToTimer(hpvm_TimerID_COPY_PTR, InsertionPoint);
    // Not an empty struct
    // Iterate over all elements of the struct and put them in
    std::map<unsigned, unsigned> retArgMap = K->getReturnArgMap();
    for (unsigned i = 0; i < OutputTy->getNumElements(); i++) {
      auto it = retArgMap.find(i);
      if (it != retArgMap.end()) {
        // The returned value is part of the argment list!
        // Don't create a buffer for it!
        LLVM_DEBUG(errs() << "Output " << i << " maps to Argument "
                          << it->second << "\n");
        //				d_Outputs.push_back(argToPtrMap[it->second]);
      } else {
        unsigned outputIndex = KF->getFunctionType()->getNumParams() + i;
        Value *setOutputArgs[] = {
            GraphID,
            ConstantInt::get(Type::getInt32Ty(M.getContext()), outputIndex),
            ConstantExpr::getSizeOf(OutputTy->getElementType(i))};

        CallInst *d_Output = CallInst::Create(
            llvm_hpvm_ocl_output_ptr, ArrayRef<Value *>(setOutputArgs, 3),
            "d_output." + KF->getName(), InsertionPoint);
        d_Outputs.push_back(d_Output);
      }
    }
  }

  LLVM_DEBUG(errs() << "Printing contents of d_Outputs:\n");
  for (auto it : d_Outputs) {
    LLVM_DEBUG(errs() << "\t" << *it << "\n");
  }

  // Enqueue kernel
  // Need work dim, localworksize, globalworksize
  // Allocate size_t[numDims] space on stack. Store the work group sizes and
  // pass it as an argument to ExecNode

  switchToTimer(hpvm_TimerID_MISC, InsertionPoint);
  Value *workDim, *LocalWGPtr, *GlobalWGPtr;
  getExecuteNodeParams(M, workDim, LocalWGPtr, GlobalWGPtr, K, VMap,
                       InsertionPoint);
  Value *event = ConstantInt::get(Type::getInt32Ty(M.getContext()), K->event);
  Value *numWaitlist =
      ConstantInt::get(Type::getInt32Ty(M.getContext()), K->waitlist.size());
  Value *WLPtr;
  Type *Int32Ty = Type::getInt32Ty(M.getContext());
  Type *Int64Ty = Type::getInt64Ty(M.getContext());
  ArrayType *WLTy = ArrayType::get(Int32Ty, K->waitlist.size());
  Type *WLPtrTy = Int32Ty->getPointerTo();
  AllocaInst *WL =
      new AllocaInst(WLTy, 0, "waitlist." + KF->getName(), InsertionPoint);
  WLPtr = BitCastInst::CreatePointerCast(WL, WLPtrTy,
                                         WL->getName() + ".0", InsertionPoint);
  Value *nextEntry = WLPtr;
  int i = 0;
  for (auto E : K->waitlist) {
    StoreInst *SI =
        new StoreInst(ConstantInt::get(Int32Ty, E), nextEntry, InsertionPoint);
    if (i + 1 < K->waitlist.size()) {
      GetElementPtrInst *GEP = GetElementPtrInst::Create(
        WLTy->getElementType(), nextEntry, ArrayRef<Value *>(ConstantInt::get(Int64Ty, 1)),
          WL->getName() + "." + Twine(i + 1), InsertionPoint);
      nextEntry = GEP;
    }
    ++i;
  }

  switchToTimer(hpvm_TimerID_KERNEL, RI);
  Value *ExecNodeArgs[] = {GraphID, workDim, LocalWGPtr, GlobalWGPtr,
                           event,   WLPtr,   numWaitlist};
  CallInst *Event = CallInst::Create(llvm_hpvm_ocl_executeNode,
                                     ArrayRef<Value *>(ExecNodeArgs, 7),
                                     "event." + KF->getName(), RI);
  LLVM_DEBUG(errs() << "Execute Node Call: " << *Event << "\n");

  // If we are enabling TLP, we should issue waits after launching all the
  // kernels. We need a wait per kernel since each kernel uses its own command
  // queue. If we are not using TLP, we need to only issue one wait at the end
  // since all the kernels are usign the same command queue.
  if (TLP) {
    // if (!hasChannels)
    //// Wait for Kernel to Finish
    // CallInst::Create(llvm_hpvm_ocl_wait, ArrayRef<Value *>(GraphID), "", RI);
    // else
    // Since we are using events, we can insert all the waits at the end.
    KernelsNeedWait.push_back(GraphID);
  } else {
    if (KernelsNeedWait.size() == 0) {
      KernelsNeedWait.push_back(GraphID);
    }
  }
  switchToTimer(hpvm_TimerID_READ_OUTPUT, RI);
  // Read Output Struct if not empty
  if (!OutputTy->isEmptyTy()) {
    std::vector<Value *> h_Outputs;
    Value *KernelOutput = UndefValue::get(OutputTy);
    Value *OutputElement;
    std::map<unsigned, unsigned> retArgMap = K->getReturnArgMap();
    for (unsigned i = 0; i < OutputTy->getNumElements(); i++) {
      auto it = retArgMap.find(i);
      if (it != retArgMap.end()) {
        OutputElement = argToPtrMap[it->second];
      } else {
        Value *GetOutputArgs[] = {
            GraphID, Constant::getNullValue(Type::getInt8PtrTy(M.getContext())),
            d_Outputs[i], ConstantExpr::getSizeOf(OutputTy->getElementType(i))};
        CallInst *h_Output = CallInst::Create(
            llvm_hpvm_ocl_getOutput, ArrayRef<Value *>(GetOutputArgs, 4),
            "h_output." + KF->getName() + ".addr", RI);
        // Read each device pointer listed in output struct
        // Load the output struct
        CastInst *BI = BitCastInst::CreatePointerCast(
            h_Output, OutputTy->getElementType(i)->getPointerTo(), "output.ptr",
            RI);

        OutputElement = new LoadInst(OutputTy->getElementType(i), BI,
                                     "output." + KF->getName(), RI);
      }
      KernelOutput = InsertValueInst::Create(KernelOutput, OutputElement,
                                             ArrayRef<unsigned>(i),
                                             KF->getName() + "output", RI);
    }
    OutputMap[K->KernelLeafNode] = KernelOutput;
  }

  // Read all the pointer arguments which had side effects i.e., had out
  // attribute
  LLVM_DEBUG(errs() << "Output Pointers : " << OutputPointers.size() << "\n");
  // FIXME: Not reading output pointers anymore as we read them when data is
  // actually requested
  /*for(auto output: OutputPointers) {
          LLVM_DEBUG(errs() << "Read: " << *output.d_ptr << "\n");
          LLVM_DEBUG(errs() << "\tTo: " << *output.h_ptr << "\n");
          LLVM_DEBUG(errs() << "\t#bytes: " << *output.bytes << "\n");

          Value* GetOutputArgs[] = {GraphID, output.h_ptr, output.d_ptr,
          output.bytes}; CallInst* CI =
     CallInst::Create(llvm_hpvm_ocl_getOutput, ArrayRef<Value*>(GetOutputArgs,
     4),
          "", RI);
          }*/
  switchToTimer(hpvm_TimerID_MEM_FREE, RI);
  // Clear Context and free device memory
  LLVM_DEBUG(errs() << "Clearing context"
                    << "\n");
  switchToTimer(hpvm_TimerID_CLEAR_CTX, CleanupCall);
  // Clear Context
  LoadInst *LI =
      new LoadInst(FPGA_Ctx->getType(), GraphIDAddr, "", CleanupCall);
  CallInst::Create(llvm_hpvm_ocl_clearContext, ArrayRef<Value *>(LI), "",
                   CleanupCall);
  switchToTimer(hpvm_TimerID_NONE, CleanupCall);
}

void CGT_FPGA::moveExecNodes(Function *F_CPU, Instruction *RI) {
  int numExecNodes = 0;
  std::vector<Value *> instsToMove;
  Value *lastExecNode;
  LLVM_DEBUG(F_CPU->dump());
  for (auto I = --inst_end(F_CPU), E = inst_begin(F_CPU); I != E; --I) {
    Instruction *Inst = &*I;
    if (CallInst *CI = dyn_cast<CallInst>(Inst)) {
      LLVM_DEBUG(errs() << "\n\nCall Inst: " << *CI << "\n");
      Value *execNode = CI->getCalledOperand();
      LLVM_DEBUG(errs() << "Called Operand: " << *(execNode));
      if (llvm_hpvm_ocl_executeNode.getCallee() == execNode) {
        LLVM_DEBUG(errs() << "Found Exec Node Call.\n");
        numExecNodes++;
        if (numExecNodes == 1) {
          lastExecNode = CI;
        } else if (numExecNodes > 1) {
          Value *Graph = (&*CI->arg_begin())->get();
          LLVM_DEBUG(errs() << "Arg: " << *Graph << "\n");
          for (auto U : Graph->users()) {
            LLVM_DEBUG(errs() << "---- User: " << *U << "\n");
            assert(isa<CallInst>(U) &&
                   "Expecting graph object user to be a callinst");
            CallInst *UCI = dyn_cast<CallInst>(U);
            Value *calledFunction = UCI->getCalledOperand();
            LLVM_DEBUG(errs()
                       << "Called Function: " << *calledFunction << "\n");
            if (calledFunction == llvm_hpvm_ocl_wait.getCallee()) {
              LLVM_DEBUG(errs() << "**********Adding " << *UCI << "\n");
              instsToMove.push_back(UCI);
            } else if (calledFunction == llvm_hpvm_ocl_getOutput.getCallee()) {
              llvm_unreachable("Need to handle getOutput case!\n");
              // TODO: Need to get a forward slice (bitcast, load, insertvalue
            }
          }
          instsToMove.push_back(CI);
        }
      }
    }
  }

  LLVM_DEBUG(errs() << "\n\n");

  Value *lastInserted = lastExecNode;
  for (auto IM : instsToMove) {
    LLVM_DEBUG(errs() << *IM << "\n");
    Instruction *instToMove = dyn_cast<Instruction>(IM);
    if (CallInst *CInstToMove = dyn_cast<CallInst>(instToMove)) {
      if (numExecNodes > 2) {
        if (CInstToMove->getCalledOperand() == llvm_hpvm_ocl_wait.getCallee()) {
          LLVM_DEBUG(errs() << "Removing instruction: " << *instToMove << "\n");
          instToMove->eraseFromParent();

        } else {
          LLVM_DEBUG(errs() << "moving instruction: " << *instToMove
                            << "\n\tBefore: " << *lastExecNode << "\n");
          instToMove->moveBefore(dyn_cast<Instruction>(lastInserted));
          lastInserted = instToMove;
          if (CInstToMove->getCalledOperand() ==
              llvm_hpvm_ocl_executeNode.getCallee()) {
            numExecNodes--;
            LLVM_DEBUG(errs() << "Remaining calls to executeNode = "
                              << numExecNodes - 1 << "\n");
          }
        }
      } else {
        assert(numExecNodes == 2 && "Something is not right!");
        if (CInstToMove->getCalledOperand() !=
            llvm_hpvm_ocl_executeNode.getCallee()) {
          LLVM_DEBUG(errs() << "moving instruction: " << *instToMove
                            << "\n\tBefore: " << *lastExecNode << "\n");
          instToMove->moveBefore(dyn_cast<Instruction>(lastInserted));
          lastInserted = instToMove;
        } else {
          assert(CInstToMove->getCalledOperand() ==
                     llvm_hpvm_ocl_executeNode.getCallee() &&
                 "Expecting a call to executeNode!");
          assert(
              (isa<CallInst>(lastInserted)) &&
              (dyn_cast<CallInst>(lastInserted)->getCalledOperand() ==
                   llvm_hpvm_ocl_wait.getCallee() ||
               dyn_cast<CallInst>(lastInserted)->getCalledOperand() ==
                   llvm_hpvm_ocl_executeNode.getCallee()) &&
              "Expecting last inserted to be a call to wait or executeNode!");
          instToMove->moveAfter(dyn_cast<Instruction>(lastInserted));
          LLVM_DEBUG(errs() << "moving instruction: " << *instToMove
                            << "\n\tAfter: " << *lastInserted << "\n");
        }
      }
    }
  }
  LLVM_DEBUG(F_CPU->dump());
}

void CGT_FPGA::codeGenCPU(DFLeafNode *N) {
  // Skip code generation if it is a dummy node
  if (N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node\n");
    return;
  }

  // At this point, the CPU backend does not support code generation for
  // the case where allocation node is used, so we skip. This means that a
  // CPU version will not be created, and therefore code generation will
  // only succeed if another backend (opencl or spir) has been invoked to
  // generate a node function for the node including the allocation node.
  if (N->isAllocationNode()) {
    LLVM_DEBUG(errs() << "Skipping allocation node\n");
    return;
  }

  // Check if clone already exists. If it does, it means we have visited this
  // function before and nothing else needs to be done for this leaf node.
  //  if(N->getGenFunc() != NULL)
  //    return;

  if (!preferredTargetIncludes(N, hpvm::CPU_TARGET)) {
    LLVM_DEBUG(errs() << "No CPU hint for node "
                      << N->getFuncPointer()->getName() << " : skipping it\n");

    switch (N->getTag()) {
    case hpvm::GPU_TARGET:
      // A leaf node should not have an cpu function for GPU
      // by design of DFG2LLVM_GPU_OCL backend
      assert(!(N->hasCPUGenFuncForTarget(hpvm::GPU_TARGET)) &&
             "Leaf node not expected to have GPU GenFunc");
      break;
    default:
      break;
    }

    return;
  }

  if (N->getGenFuncForTarget(hpvm::CPU_TARGET) != NULL) {
    LLVM_DEBUG(errs() << "Already generated CPU code for this node!\n");
    return;
  }
  //  assert(N->getGenFuncForTarget(hpvm::CPU_TARGET) == NULL &&
  //         "Error: Visiting a node for which code already generated\n");

  std::vector<IntrinsicInst *> IItoRemove;
  std::vector<std::pair<IntrinsicInst *, Value *>> IItoReplace;
  BuildDFG::HandleToDFNode Leaf_HandleToDFNodeMap;

  // Get the function associated woth the dataflow node
  Function *F = N->getFuncPointer();
  LLVM_DEBUG(errs() << "Generating CPU code for function " << F->getName()
                    << "\n");

  // Clone the function, if we are seeing this function for the first time.
  Function *F_CPU;
  ValueToValueMapTy VMap;
  LLVM_DEBUG(errs() << "fun to clone:\n" << *F << "\n");
  F_CPU = CloneFunction(F, VMap);
  F_CPU->removeFromParent();
  // Insert the cloned function into the module
  M.getFunctionList().push_back(F_CPU);

  // Add the new argument to the argument list. Add arguments only if the cild
  // graph of parent node is not streaming
  if (!N->getParent()->isChildGraphStreaming())
    F_CPU = addIdxDimArgs(F_CPU);

  // Add generated function info to DFNode
  //  N->setGenFunc(F_CPU, hpvm::CPU_TARGET);
  N->addGenFunc(F_CPU, hpvm::CPU_TARGET, true);

  // Go through the arguments, and any pointer arguments with in attribute need
  // to have cpu_argument_ptr call to get the cpu ptr of the argument
  // Insert these calls in a new BB which would dominate all other BBs
  // Create new BB
  BasicBlock *EntryBB = &*F_CPU->begin();
  BasicBlock *BB =
      BasicBlock::Create(M.getContext(), "getHPVMPtrArgs", F_CPU, EntryBB);
  BranchInst *Terminator = BranchInst::Create(EntryBB, BB);
  // Insert calls
  for (Function::arg_iterator ai = F_CPU->arg_begin(), ae = F_CPU->arg_end();
       ai != ae; ++ai) {
    if (F_CPU->getAttributes().hasAttribute(ai->getArgNo() + 1,
                                            Attribute::In)) {
      assert(ai->getType()->isPointerTy() &&
             "Only pointer arguments can have hpvm in/out attributes ");
      Function::arg_iterator aiNext = ai;
      ++aiNext;
      Argument *size = &*aiNext;
      assert(size->getType() == Type::getInt64Ty(M.getContext()) &&
             "Next argument after a pointer should be an i64 type");
      CastInst *BI = BitCastInst::CreatePointerCast(
          &*ai, Type::getInt8PtrTy(M.getContext()), ai->getName() + ".i8ptr",
          Terminator);
      Value *ArgPtrCallArgs[] = {BI, size};
      CallInst::Create(llvm_hpvm_cpu_argument_ptr,
                       ArrayRef<Value *>(ArgPtrCallArgs, 2), "", Terminator);
    }
  }
  LLVM_DEBUG(errs() << *BB << "\n");

  // Go through all the instructions
  for (inst_iterator i = inst_begin(F_CPU), e = inst_end(F_CPU); i != e; ++i) {
    Instruction *I = &(*i);
    LLVM_DEBUG(errs() << *I << "\n");
    // Leaf nodes should not contain HPVM graph intrinsics or launch
    assert(!BuildDFG::isHPVMLaunchIntrinsic(I) &&
           "Launch intrinsic within a dataflow graph!");
    assert(!BuildDFG::isHPVMGraphIntrinsic(I) &&
           "HPVM graph intrinsic within a leaf dataflow node!");

    if (BuildDFG::isHPVMQueryIntrinsic(I)) {
      IntrinsicInst *II = cast<IntrinsicInst>(I);
      IntrinsicInst *ArgII;
      DFNode *ArgDFNode;

      /***********************************************************************
       *                        Handle HPVM Query intrinsics                  *
       ***********************************************************************/
      switch (II->getIntrinsicID()) {
      /**************************** llvm.hpvm.getNode() *******************/
      case Intrinsic::hpvm_getNode: {
        // add mapping <intrinsic, this node> to the node-specific map
        Leaf_HandleToDFNodeMap[II] = N;
        IItoRemove.push_back(II);
        break;
      }
      /************************* llvm.hpvm.getParentNode() ****************/
      case Intrinsic::hpvm_getParentNode: {
        // get the parent node of the arg node
        // get argument node
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        // get the parent node of the arg node
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];
        // Add mapping <intrinsic, parent node> to the node-specific map
        // the argument node must have been added to the map, orelse the
        // code could not refer to it
        Leaf_HandleToDFNodeMap[II] = ArgDFNode->getParent();
        IItoRemove.push_back(II);
        break;
      }
      /*************************** llvm.hpvm.getNumDims() *****************/
      case Intrinsic::hpvm_getNumDims: {
        // get node from map
        // get the appropriate field
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        int numOfDim = Leaf_HandleToDFNodeMap[ArgII]->getNumOfDim();
        IntegerType *IntTy = Type::getInt32Ty(M.getContext());
        ConstantInt *numOfDimConstant =
            ConstantInt::getSigned(IntTy, (int64_t)numOfDim);

        II->replaceAllUsesWith(numOfDimConstant);
        IItoRemove.push_back(II);
        break;
      }
      /*********************** llvm.hpvm.getNodeInstanceID() **************/
      case Intrinsic::hpvm_getNodeInstanceID_x:
      case Intrinsic::hpvm_getNodeInstanceID_y:
      case Intrinsic::hpvm_getNodeInstanceID_z: {
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];

        // The dfnode argument should be an ancestor of this leaf node or
        // the leaf node itself
        int parentLevel = N->getAncestorHops(ArgDFNode);
        assert((parentLevel >= 0 || ArgDFNode == (DFNode *)N) &&
               "Invalid DFNode argument to getNodeInstanceID_[xyz]!");

        // Get specified dimension
        // (dim = 0) => x
        // (dim = 1) => y
        // (dim = 2) => z
        int dim =
            (int)(II->getIntrinsicID() - Intrinsic::hpvm_getNodeInstanceID_x);
        assert((dim >= 0) && (dim < 3) &&
               "Invalid dimension for getNodeInstanceID_[xyz]. Check Intrinsic "
               "ID!");

        // For immediate ancestor, use the extra argument introduced in
        // F_CPU
        int numParamsF = F->getFunctionType()->getNumParams();
        int numParamsF_CPU = F_CPU->getFunctionType()->getNumParams();
        assert(
            (numParamsF_CPU - numParamsF == 6) &&
            "Difference of arguments between function and its clone is not 6!");

        if (parentLevel == 0) {
          // Case when the query is for this node itself
          unsigned offset = 3 + (3 - dim);
          // Traverse argument list of F_CPU in reverse order to find the
          // correct index or dim argument.
          Argument *indexVal = getArgumentFromEnd(F_CPU, offset);
          assert(indexVal && "Index argument not found. Invalid offset!");

          LLVM_DEBUG(errs() << *II << " replaced with " << *indexVal << "\n");

          II->replaceAllUsesWith(indexVal);
          IItoRemove.push_back(II);
        } else {
          // Case when query is for an ancestor
          Value *args[] = {
              ConstantInt::get(Type::getInt32Ty(II->getContext()), parentLevel),
              ConstantInt::get(Type::getInt32Ty(II->getContext()), dim)};
          CallInst *CI = CallInst::Create(llvm_hpvm_cpu_getDimInstance,
                                          ArrayRef<Value *>(args, 2),
                                          "nodeInstanceID", II);
          LLVM_DEBUG(errs() << *II << " replaced with " << *CI << "\n");
          II->replaceAllUsesWith(CI);
          IItoRemove.push_back(II);
        }
        break;
      }
      /********************** llvm.hpvm.getNumNodeInstances() *************/
      case Intrinsic::hpvm_getNumNodeInstances_x:
      case Intrinsic::hpvm_getNumNodeInstances_y:
      case Intrinsic::hpvm_getNumNodeInstances_z: {

        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];

        // The dfnode argument should be an ancestor of this leaf node or
        // the leaf node itself
        int parentLevel = N->getAncestorHops(ArgDFNode);
        assert((parentLevel >= 0 || ArgDFNode == (DFNode *)N) &&
               "Invalid DFNode argument to getNodeInstanceID_[xyz]!");

        // Get specified dimension
        // (dim = 0) => x
        // (dim = 1) => y
        // (dim = 2) => z
        int dim =
            (int)(II->getIntrinsicID() - Intrinsic::hpvm_getNumNodeInstances_x);
        assert((dim >= 0) && (dim < 3) &&
               "Invalid dimension for getNumNodeInstances_[xyz]. Check "
               "Intrinsic ID!");

        // For immediate ancestor, use the extra argument introduced in
        // F_CPU
        int numParamsF = F->getFunctionType()->getNumParams();
        int numParamsF_CPU = F_CPU->getFunctionType()->getNumParams();
        assert(
            (numParamsF_CPU - numParamsF == 6) &&
            "Difference of arguments between function and its clone is not 6!");

        if (parentLevel == 0) {
          // Case when the query is for this node itself
          unsigned offset = 3 - dim;
          // Traverse argument list of F_CPU in reverse order to find the
          // correct index or dim argument.
          Argument *limitVal = getArgumentFromEnd(F_CPU, offset);
          assert(limitVal && "Limit argument not found. Invalid offset!");

          LLVM_DEBUG(errs() << *II << " replaced with " << *limitVal << "\n");

          II->replaceAllUsesWith(limitVal);
          IItoRemove.push_back(II);
        } else {
          // Case when query is from the ancestor
          Value *args[] = {
              ConstantInt::get(Type::getInt32Ty(II->getContext()), parentLevel),
              ConstantInt::get(Type::getInt32Ty(II->getContext()), dim)};
          CallInst *CI = CallInst::Create(llvm_hpvm_cpu_getDimLimit,
                                          ArrayRef<Value *>(args, 2),
                                          "numNodeInstances", II);
          LLVM_DEBUG(errs() << *II << " replaced with " << *CI << "\n");
          II->replaceAllUsesWith(CI);
          IItoRemove.push_back(II);
        }

        break;
      }
      // Leave channels intrinsics intact. They will be handled by OpenCL
      // backend.
      case Intrinsic::hpvm_ch_read:
      case Intrinsic::hpvm_ch_write:
        break;
      default:
        LLVM_DEBUG(errs() << "Found unknown intrinsic with ID = "
                          << II->getIntrinsicID() << "\n");
        assert(false && "Unknown HPVM Intrinsic!");
        break;
      }

    } else if (BuildDFG::isHPVMIntrinsic(I)) {
      IntrinsicInst *II = dyn_cast<IntrinsicInst>(I);
      if (II->getIntrinsicID() == Intrinsic::hpvm_nz_loop) {
        IItoRemove.push_back(II);
      }
    }
  }

  // Remove them in reverse order
  for (std::vector<IntrinsicInst *>::iterator i = IItoRemove.begin();
       i != IItoRemove.end(); ++i) {
    (*i)->replaceAllUsesWith(UndefValue::get((*i)->getType()));
    (*i)->eraseFromParent();
  }

  LLVM_DEBUG(errs() << *F_CPU);
}
// Right now, only targeting the one level case. In general, device functions
// can return values so we don't need to change them
void CGT_FPGA::codeGen(DFInternalNode *N) {
  LLVM_DEBUG(errs() << "In codeGen() for Internal Node - "
                    << N->getFuncPointer()->getName() << "\n");
  //  llvm::ViewGraph(N->getChildGraph(), "DFGraph");
  //  errs () << "Inside node: " << N->getFuncPointer()->getName() << "\n";

  if (N->isRoot()) {
    LLVM_DEBUG(errs() << "We do not want to do code-gen for Root node here!\n");
    return;
  }

  if (N->hasCPUGenFuncForAnyTarget()) {
    LLVM_DEBUG(errs() << "We have already code-gen'd this node\n");
    return;
  }

  if (kernelLaunchMap.size() > 0) {

    // Creating Function here to enable launching multiple kernels from same
    // internal node
    Function *F = N->getFuncPointer();

    assert(N->getGenFuncForTarget(hpvm::FPGA_TARGET) == NULL &&
           "Code already generated for this node");

    // Create of clone of F with no instructions. Only the type is the same as F
    // without the extra arguments.
    Function *F_CPU;

    // Clone the function, if we are seeing this function for the first time. We
    // only need a clone in terms of type.
    ValueToValueMapTy VMap;

    // Create new function with the same type
    F_CPU = Function::Create(F->getFunctionType(), F->getLinkage(),
                             F->getName(), &M);

    // Loop over the arguments, copying the names of arguments over.
    Function::arg_iterator dest_iterator = F_CPU->arg_begin();
    for (Function::const_arg_iterator i = F->arg_begin(), e = F->arg_end();
         i != e; ++i) {
      dest_iterator->setName(i->getName()); // Copy the name over...
      // Increment dest iterator
      ++dest_iterator;
    }

    // Add a basic block to this empty function
    BasicBlock *BB = BasicBlock::Create(M.getContext(), "entry", F_CPU);
    ReturnInst *RI = ReturnInst::Create(
        M.getContext(), UndefValue::get(F_CPU->getReturnType()), BB);

    // FIXME: Adding Index and Dim arguments are probably not required except
    // for consistency purpose (DFG2LLVM_CPU does assume that all leaf nodes do
    // have those arguments)

    // Add Index and Dim arguments except for the root node
    if (!N->isRoot() && !N->getParent()->isChildGraphStreaming())
      F_CPU = addIdxDimArgs(F_CPU);

    BB = &*F_CPU->begin();
    RI = cast<ReturnInst>(BB->getTerminator());

    // Add the generated function info to DFNode
    //  N->setGenFunc(F_CPU, hpvm::CPU_TARGET);
    N->addGenFunc(F_CPU, hpvm::FPGA_TARGET, true);

    // Loop over the arguments, to create the VMap
    dest_iterator = F_CPU->arg_begin();
    for (Function::const_arg_iterator i = F->arg_begin(), e = F->arg_end();
         i != e; ++i) {
      // Add mapping to VMap and increment dest iterator
      VMap[&*i] = &*dest_iterator;
      ++dest_iterator;
    }

    DFGraph *childGraph = N->getChildGraph();

    for (auto ci = childGraph->begin(), ce = childGraph->end(); ci != ce;
         ++ci) {
      DFNode *C = *ci;

      if (C->isDummyNode())
        continue;
      LLVM_DEBUG(C->dump(0, 0, 0));
      if (kernelLaunchMap.find(C) != kernelLaunchMap.end()) {
        Kernel *kernel = kernelLaunchMap[C].first;
        DFNode *LaunchNode = kernelLaunchMap[C].second;
        assert(LaunchNode == N && "Kernel should always be launched by parent");
        if (LaunchNode == N) {
          LLVM_DEBUG(errs() << "Need to launch kernel!\n");

          // Copy the dim calculations if they are not all constant/coming from
          // the Internal Node Functions' arguments
          copyChildIndexCalc(C, F_CPU, VMap, RI);

          // Add the runtime calls to invoke the kernel.
          insertRuntimeCalls(N, kernel, getFPGAFilename(M), VMap);
          kernelLaunchMap.erase(C);
        } else {
          LLVM_DEBUG(
              LLVM_DEBUG(
                  errs()
                  << "Found intermediate node. Getting size parameters.\n"););
          // Keep track of the arguments order.
          std::map<unsigned, unsigned> inmap1 = N->getInArgMap();
          std::map<unsigned, unsigned> inmap2 = kernel->getInArgMap();
          // TODO: Structure assumed: one thread node, one allocation node (at
          // most), TB node
          std::map<unsigned, unsigned> inmapFinal;
          for (std::map<unsigned, unsigned>::iterator ib = inmap2.begin(),
                                                      ie = inmap2.end();
               ib != ie; ++ib) {
            inmapFinal[ib->first] = inmap1[ib->second];
          }

          kernel->setInArgMap(inmapFinal);

          // Keep track of the output arguments order.
          std::vector<unsigned> outmap1 = N->getOutArgMap();
          std::vector<unsigned> outmap2 = kernel->getOutArgMap();

          // TODO: Change when we have incoming edges to the dummy exit node
          // from more than one nodes. In this case, the number of bindings is
          // the same, but their destination position, thus the index in
          // outmap1, is not 0 ... outmap2.size()-1 The limit is the size of
          // outmap2, because this is the number of kernel output arguments for
          // which the mapping matters For now, it reasonable to assume that all
          // the kernel arguments are returned, maybe plys some others from
          // other nodes, thus outmap2.size() <= outmap1.size()
          for (unsigned i = 0; i < outmap2.size(); i++) {
            outmap1[i] = outmap2[outmap1[i]];
          }
          kernel->setOutArgMap(outmap1);

          // Track the source of local dimlimits for the kernel
          // Dimension limit can either be a constant or an argument of parent
          // function. Since Internal node would no longer exist, we need to
          // insert the localWGSize with values from the parent of N.
          std::vector<Value *> localWGSizeMapped;
          for (unsigned i = 0; i < kernel->localWGSize.size(); i++) {
            if (isa<Constant>(kernel->localWGSize[i])) {
              // if constant, use as it is
              localWGSizeMapped.push_back(kernel->localWGSize[i]);
            } else if (Argument *Arg =
                           dyn_cast<Argument>(kernel->localWGSize[i])) {
              // if argument, find the argument location in N. Use InArgMap of N
              // to find the source location in Parent of N. Retrieve the
              // argument from parent to insert in the vector.
              unsigned argNum = Arg->getArgNo();
              // This argument will be coming from the parent node, not the
              // allocation Node
              assert(N->getInArgMap().find(argNum) != N->getInArgMap().end());

              unsigned parentArgNum = N->getInArgMap()[argNum];
              Argument *A =
                  getArgumentAt(N->getParent()->getFuncPointer(), parentArgNum);
              localWGSizeMapped.push_back(A);
            } else {
              assert(false &&
                     "LocalWGsize using value which is neither argument "
                     "nor constant!");
            }
          }
          // Update localWGSize vector of kernel
          kernel->setLocalWGSize(localWGSizeMapped);
          kernelLaunchMap.erase(C);
          kernelLaunchMap[N] =
              std::pair<Kernel *, DFNode *>(kernel, LaunchNode);
        }
      } else {
        if (C->hasCPUGenFuncForTarget(
                hpvm::FPGA_TARGET)) { /* need to check isCPU */
          // Need to create call for to CPU function
          //          Function *F_CPU =
          //          C->getGenFuncForTarget(hpvm::FPGA_TARGET);
          if (KernelsNeedWait.size() != 0) {
            for (auto GraphID : KernelsNeedWait) {
              CallInst::Create(llvm_hpvm_ocl_wait, ArrayRef<Value *>(GraphID),
                               "", RI);
            }
            KernelsNeedWait.clear();
          }
          invokeChild(C, F_CPU, VMap, RI, hpvm::FPGA_TARGET);
        } else if (C->hasCPUGenFuncForTarget(hpvm::GPU_TARGET)) {
          if (KernelsNeedWait.size() != 0) {
            for (auto GraphID : KernelsNeedWait) {
              CallInst::Create(llvm_hpvm_ocl_wait, ArrayRef<Value *>(GraphID),
                               "", RI);
            }
            KernelsNeedWait.clear();
          }
          invokeChild(C, F_CPU, VMap, RI, hpvm::GPU_TARGET);
        } else {
          if (C->getKind() == DFNode::DFNodeKind::LeafNode &&
              preferredTargetIncludes(N, hpvm::CPU_TARGET)) {
            LLVM_DEBUG(errs() << "Doing codegen for CPU leaf node within FPGA "
                                 "internal node!\n");
            codeGenCPU(dyn_cast<DFLeafNode>(C));

            if (KernelsNeedWait.size() != 0) {
              for (auto GraphID : KernelsNeedWait) {
                CallInst::Create(llvm_hpvm_ocl_wait, ArrayRef<Value *>(GraphID),
                                 "", RI);
              }
              KernelsNeedWait.clear();
            }
            invokeChild(C, F_CPU, VMap, RI, hpvm::CPU_TARGET);
          }
        }
      }
    }

    // moveExecNodes(F_CPU, RI);

    if (KernelsNeedWait.size() != 0) {
      for (auto GraphID : KernelsNeedWait) {
        CallInst::Create(llvm_hpvm_ocl_wait, ArrayRef<Value *>(GraphID), "",
                         RI);
      }
      KernelsNeedWait.clear();
    }

    switchToTimer(hpvm_TimerID_MISC, RI);
    LLVM_DEBUG(errs() << "*** Generating epilogue code for the function****\n");
    // Generate code for output bindings
    // Get Exit node
    DFNode *C = N->getChildGraph()->getExit();
    // Get OutputType of this node
    StructType *OutTy = N->getOutputType();
    Value *retVal = UndefValue::get(F_CPU->getReturnType());
    // Find the kernel's output arg map, to use instead of the bindings
    //  std::vector<unsigned> outArgMap = K->getOutArgMap();
    // Find all the input edges to exit node
    for (unsigned i = 0; i < OutTy->getNumElements(); i++) {
      LLVM_DEBUG(errs() << "Output Edge " << i << "\n");
      // Find the incoming edge at the requested input port
      DFEdge *E = C->getInDFEdgeAt(i);

      assert(E && "No Binding for output element!");
      // Find the Source DFNode associated with the incoming edge
      DFNode *SrcDF = E->getSourceDF();

      LLVM_DEBUG(errs() << "Edge source -- "
                        << SrcDF->getFuncPointer()->getName() << "\n");

      // If Source DFNode is a dummyNode, edge is from parent. Get the
      // argument from argument list of this internal node
      Value *inputVal;
      if (SrcDF->isEntryNode()) {
        inputVal = getArgumentAt(F_CPU, i);
        LLVM_DEBUG(errs() << "Argument " << i << " = " << *inputVal << "\n");
      } else { // if (SrcDF->getKind() == DFNode::DFNodeKind::LeafNode) {
        // Find Output Value associated with the Source DFNode using OutputMap
        Value *CI = OutputMap[SrcDF];

        // Extract element at source position from this call instruction
        std::vector<unsigned> IndexList;
        // i is the destination of DFEdge E
        // Use the mapping instead of the bindings
        IndexList.push_back(E->getSourcePosition());
        LLVM_DEBUG(errs() << "Going to generate ExtarctVal inst from " << *CI
                          << "\n");
        ExtractValueInst *EI = ExtractValueInst::Create(CI, IndexList, "", RI);
        inputVal = EI;

      } /* else {
                       llvm_unreachable("SrcDF is internal node, need to handle
                       case!");
      // edge is from a internal node
      // Check - code should already be generated for this source dfnode
      // FIXME: Since the 2-level kernel code gen has aspecific structure, we
      // can assume the SrcDF is same as Kernel Leaf node.
      // Use outArgMap to get correct mapping
      //      SrcDF = K->KernelLeafNode;
      //      assert(OutputMap.count(SrcDF) &&
      //             "Source node call not found. Dependency violation!");
      //
      //      // Find Output Value associated with the Source DFNode using
      //      OutputMap Value *CI = OutputMap[SrcDF];
      //
      //      // Extract element at source position from this call
      instruction
      //      std::vector<unsigned> IndexList;
      //      // i is the destination of DFEdge E
      //      // Use the mapping instead of the bindings
      //      //      IndexList.push_back(E->getSourcePosition());
      //      IndexList.push_back(outArgMap[i]);
      //      LLVM_DEBUG(errs() << "Going to generate ExtarctVal inst from " <<
               *CI
      //      << "\n"); ExtractValueInst *EI = ExtractValueInst::Create(CI,
      //      IndexList, "", RI); inputVal = EI;
      } */
      std::vector<unsigned> IdxList;
      IdxList.push_back(i);
      retVal = InsertValueInst::Create(retVal, inputVal, IdxList, "", RI);
    }

    LLVM_DEBUG(errs() << "Extracted all\n");
    switchToTimer(hpvm_TimerID_NONE, RI);
    retVal->setName("output");
    ReturnInst *newRI = ReturnInst::Create(F_CPU->getContext(), retVal);
    ReplaceInstWithInst(RI, newRI);
  }
}

void CGT_FPGA::codeGen(DFLeafNode *N) {

  LLVM_DEBUG(errs() << "In codeGen() for Leaf Node - "
                    << N->getFuncPointer()->getName() << "\n");
  // Skip code generation if it is a dummy node
  if (N->isDummyNode()) {
    LLVM_DEBUG(errs() << "Skipping dummy node\n");
    return;
  }

  // Skip code generation if it is an allocation node
  if (N->isAllocationNode()) {
    LLVM_DEBUG(errs() << "Skipping allocation node\n");
    return;
  }

  // Generate code only if it has the right hint
  //  if(!checkPreferredTarget(N, hpvm::FPGA_TARGET)) {
  //   LLVM_DEBUG( errs() << "Skipping node: "<< N->getFuncPointer()->getName()
  //   <<
  //   "\n";);
  //    return;
  //  }
  if (!preferredTargetIncludes(N, hpvm::FPGA_TARGET)) {
    LLVM_DEBUG(errs() << "Skipping node: " << N->getFuncPointer()->getName()
                      << "\n";);
    return;
  }

  // Sequentialize
  {
    ValueToValueMapTy VMap;
    DFGTransform::Context TContext(*(N->getFuncPointer()->getParent()),
                                   N->getFuncPointer()->getContext());
    LeafSequentializer *LS =
        new LeafSequentializer(TContext, N, VMap, !HPVM_FPGA_DISABLE_IVDEP);
    LS->checkAndRun();
    LS->cleanup();
  }

  // Checking which node is the kernel launch
  DFNode *PNode = N->getParent();
  int pLevel = PNode->getLevel();
  int pReplFactor = PNode->getNumOfDim();

  // Choose parent node as kernel launch if:
  // (1) Parent is the top level node i.e., Root of DFG
  //                    OR
  // (2) Parent does not have multiple instances
  LLVM_DEBUG(errs() << "pLevel = " << pLevel << "\n";);
  LLVM_DEBUG(errs() << "pReplFactor = " << pReplFactor << "\n";);

  Kernel *kernel;
  DFNode *KernelLaunchNode;
  if (!pLevel || !pReplFactor) {
    LLVM_DEBUG(errs() << "*************** Kernel Gen: 1-Level Hierarchy "
                         "**************\n";);
    KernelLaunchNode = PNode;
    LLVM_DEBUG(errs() << "Setting Kernel Launch Node\n";);
    kernel = new Kernel(NULL, N, N->getInArgMap(), N->getSharedInArgMap(),
                        N->getOutArgMap(), N->getNumOfDim(), N->getDimLimits());
  } else {
    // Converting a 2-level DFG to opencl kernel
    LLVM_DEBUG(errs() << "*************** Kernel Gen: 2-Level Hierarchy "
                         "**************\n";);
    KernelLaunchNode = PNode->getParent();
    assert((PNode->getNumOfDim() == N->getNumOfDim()) &&
           "Dimension number must match");
    // Contains the instructions generating the kernel configuration parameters
    kernel = new Kernel(NULL,             // kernel function
                        N,                // kernel leaf node
                        N->getInArgMap(), // kenel argument mapping
                        N->getSharedInArgMap(),
                        N->getOutArgMap(), // kernel output mapping from the
                        // leaf to the interemediate node
                        PNode->getNumOfDim(),  // gridDim
                        PNode->getDimLimits(), // grid size
                        N->getNumOfDim(),      // blockDim
                        N->getDimLimits());    // block size
  }

  kernelLaunchMap[N] = std::pair<Kernel *, DFNode *>(kernel, KernelLaunchNode);
  kernelMap[N] = kernel;

  if (EnableTLP) {
    // set the event and waitlist of the kernel
    kernel->event = event++;
    for (DFNode::const_indfedge_iterator ieb = N->indfedge_begin(),
                                         iee = N->indfedge_end();
         ieb != iee; ++ieb) {
      DFNode *SrcDFNode = (*ieb)->getSourceDF();
      LLVM_DEBUG(errs() << "Found edge from node: "
                        << " " << SrcDFNode->getFuncPointer()->getName()
                        << "\n");
      LLVM_DEBUG(errs() << "Current Node: " << N->getFuncPointer()->getName()
                        << "\n");
      if (!SrcDFNode->isDummyNode()) {
        if (SrcDFNode->getKind() == DFNode::LeafNode) {
          assert(kernelMap.find(SrcDFNode) != kernelMap.end() &&
                 "Order of traversal is wrong!\n");
          Kernel *SrcKernel = kernelMap[SrcDFNode];
          kernel->waitlist.insert(SrcKernel->event);
        } else {
          // for every internal node, first mark the input and output events.
        }
      }
    }
  }

  std::vector<IntrinsicInst *> IItoRemove;
  std::vector<CallInst *> CItoRemove;
  std::vector<Instruction *> ItoRemove;
  BuildDFG::HandleToDFNode Leaf_HandleToDFNodeMap;

  // Get the function associated with the dataflow node
  Function *F = N->getFuncPointer();
  if (F->getName().find('_') == 0) {
    LLVM_DEBUG(errs() << "Kernel name starts with \'_\', removing it!\n");
    F->setName(F->getName().drop_front(1));
  }

  // Look up if we have visited this function before. If we have, then just
  // get the cloned function pointer from DFNode. Otherwise, create the cloned
  // function and add it to the DFNode GenFunc.
  Function *F_fpga = N->getGenFuncForTarget(hpvm::FPGA_TARGET);
  assert(F_fpga == NULL &&
         "Error: Visiting a node for which code already generated");

  // Clone the function
  ValueToValueMapTy VMap;

  Twine FName = F->getName();
  StringRef fStr = FName.getSingleStringRef();
  Twine newFName = Twine(fStr, "_fpga");
  F_fpga = CloneFunction(F, VMap);
  F_fpga->setName(newFName);
  LLVM_DEBUG(errs() << "Old Function Name: " << F->getName() << "\n";);
  LLVM_DEBUG(errs() << "New Function Name: " << F_fpga->getName() << "\n";);

  F_fpga->removeFromParent();

  // Insert the cloned function into the kernels module
  KernelM->getFunctionList().push_back(F_fpga);

  // TODO: Iterate over all the instructions of F_fpga and identify the
  // callees and clone them into this module.
  LLVM_DEBUG(errs() << *F_fpga->getType());
  LLVM_DEBUG(errs() << *F_fpga);

  // Add generated function info to DFNode
  // N->setGenFunc(F_fpga, hpvm::FPGA_TARGET);

  F_fpga = transformFunctionToVoid(F_fpga);

  kernel->setReturnArgMap(returnArgMap);
  returnArgMap.clear();
  // Add generated function info to DFNode
  // N->setGenFunc(F_fpga, hpvm::FPGA_TARGET);

  // Add generated function info to DFNode
  N->addGenFunc(F_fpga, hpvm::FPGA_TARGET, false);

  LLVM_DEBUG(errs() << "Removing all attributes \
				from Kernel Function and adding nounwind\n");
  F_fpga->removeAttributes(AttributeList::FunctionIndex,
                           F_fpga->getAttributes().getFnAttributes());
  F_fpga->addAttribute(AttributeList::FunctionIndex, Attribute::NoUnwind);

  // FIXME: For now, assume only one allocation node
  kernel->AllocationNode = NULL;

  for (DFNode::const_indfedge_iterator ieb = N->indfedge_begin(),
                                       iee = N->indfedge_end();
       ieb != iee; ++ieb) {
    DFNode *SrcDFNode = (*ieb)->getSourceDF();
    LLVM_DEBUG(errs() << "Found edge from node: "
                      << " " << SrcDFNode->getFuncPointer()->getName() << "\n");
    LLVM_DEBUG(errs() << "Current Node: " << N->getFuncPointer()->getName()
                      << "\n");
    LLVM_DEBUG(errs() << "isAllocationNode = " << SrcDFNode->isAllocationNode()
                      << "\n");
    if (!SrcDFNode->isDummyNode()) {
      if (SrcDFNode->isAllocationNode()) {
        kernel->AllocationNode = dyn_cast<DFLeafNode>(SrcDFNode);
        kernel->allocInArgMap = SrcDFNode->getInArgMap();
        break;
      } else {
        LLVM_DEBUG(
            errs()
            << "Found edge from a node that is NOT an allocation node!\n");
      }
    }
  }

  // TODO: Handling of allocation nodes for FPGA backend!
  assert(kernel->AllocationNode == NULL &&
         "FPGA backend does not currently support allocation nodes!");
  //  // Vector for shared memory arguments
  //  std::vector<unsigned> SharedMemArgs;
  //
  //  // If no allocation node was found, SharedMemArgs is empty
  //  if (kernel->AllocationNode) {
  //
  //    ValueToValueMapTy VMap;
  //    Function *F_alloc =
  //        CloneFunction(kernel->AllocationNode->getFuncPointer(), VMap);
  //    // F_alloc->removeFromParent();
  //    // Insert the cloned function into the kernels module
  //    // M.getFunctionList().push_back(F_alloc);
  //
  //    std::vector<IntrinsicInst *> HPVMMallocInstVec;
  //    findIntrinsicInst(F_alloc, Intrinsic::hpvm_malloc, HPVMMallocInstVec);
  //
  //    for (unsigned i = 0; i < HPVMMallocInstVec.size(); i++) {
  //      IntrinsicInst *II = HPVMMallocInstVec[i];
  //      assert(II->hasOneUse() && "hpvm_malloc result is used more than
  //      once"); II->replaceAllUsesWith(
  //          ConstantPointerNull::get(Type::getInt8PtrTy(M.getContext())));
  //      II->eraseFromParent();
  //    }
  //    kernel->AllocationFunction = F_alloc;
  //
  //    // This could be used to check that the allocation node has the
  //    appropriate
  //    // number of fields in its return struct
  //    /*
  //             ReturnInst *RI = ReturnInstVec[0];
  //             Value *RetVal = RI->getReturnValue();
  //             Type *RetTy = RetVal->getType();
  //             StructType *RetStructTy = dyn_cast<StructType>(RetTy);
  //             assert(RetStructTy && "Allocation node does not return a struct
  //             type"); unsigned numFields = RetStructTy->getNumElements();
  //             */
  //    std::map<unsigned, std::pair<Value *, unsigned>> sharedInMap =
  //        kernel->getSharedInArgMap();
  //    AllocationNodeProperty *APN =
  //        (AllocationNodeProperty *)kernel->AllocationNode->getProperty(
  //            DFNode::Allocation);
  //    LLVM_DEBUG(errs() << "Allocation Node Property num allocations: "
  //                 << APN->getNumAllocations() << "\n");
  //    for (auto &AllocPair : APN->getAllocationList()) {
  //      unsigned destPos = AllocPair.first->getDestPosition();
  //      unsigned srcPos = AllocPair.first->getSourcePosition();
  //      LLVM_DEBUG(
  //          errs() << "Allocation Pair: "
  //                 <<
  //                 AllocPair.first->getSourceDF()->getFuncPointer()->getName()
  //                 << "->"
  //                 <<
  //                 AllocPair.first->getDestDF()->getFuncPointer()->getName()
  //                 << "; " << *(AllocPair.second) << "\n");
  //      LLVM_DEBUG(errs() << "destPos: " << destPos << "; srcPos: " << srcPos
  //      <<
  //      "\n"); SharedMemArgs.push_back(destPos); sharedInMap[destPos] =
  //          std::pair<Value *, unsigned>(AllocPair.second, srcPos + 1);
  //      sharedInMap[destPos + 1] =
  //          std::pair<Value *, unsigned>(AllocPair.second, srcPos + 1);
  //    }
  //    kernel->setSharedInArgMap(sharedInMap);
  //  }
  //  std::sort(SharedMemArgs.begin(), SharedMemArgs.end());

  // All pointer args which are not shared memory pointers have to be moved to
  // global address space
  unsigned argIndex = 0;
  std::vector<unsigned> GlobalMemArgs;
  for (Function::arg_iterator ai = F_fpga->arg_begin(), ae = F_fpga->arg_end();
       ai != ae; ++ai) {
    if (ai->getType()->isPointerTy()) {
      GlobalMemArgs.push_back(argIndex);
      LLVM_DEBUG(errs() << "Arg is pointer and will be moved to global: "
                        << argIndex << "\n");
    }
    argIndex++;
  }

  F_fpga = changeArgAddrspace(F_fpga, GlobalMemArgs, GLOBAL_ADDRSPACE);
  LLVM_DEBUG(errs() << "After setting address space qualifier:\n"
                    << *F->getFunctionType() << "\n");

  // Go through all the instructions
  for (inst_iterator i = inst_begin(F_fpga), e = inst_end(F_fpga); i != e;
       ++i) {
    Instruction *I = &(*i);
    // Leaf nodes should not contain HPVM graph intrinsics or launch
    assert(!BuildDFG::isHPVMLaunchIntrinsic(I) &&
           "Launch intrinsic within a dataflow graph!");
    assert(!BuildDFG::isHPVMGraphIntrinsic(I) &&
           "HPVM graph intrinsic within a leaf dataflow node!");

    if (BuildDFG::isHPVMIntrinsic(I)) {
      IntrinsicInst *II = dyn_cast<IntrinsicInst>(I);
      IntrinsicInst *ArgII;
      DFNode *ArgDFNode;

      /************************ Handle HPVM Query intrinsics
       * ************************/

      switch (II->getIntrinsicID()) {
      /**************************** llvm.hpvm.getNode()
       * *****************************/
      case Intrinsic::hpvm_getNode: {
        LLVM_DEBUG(errs() << F_fpga->getName() << "\t: Handling getNode\n");
        LLVM_DEBUG(errs() << *II << "\n");
        // add mapping <intrinsic, this node> to the node-specific map
        Leaf_HandleToDFNodeMap[II] = N;
        IItoRemove.push_back(II);
      } break;
        /************************* llvm.hpvm.getParentNode()
         * **************************/
      case Intrinsic::hpvm_getParentNode: {
        LLVM_DEBUG(errs() << F_fpga->getName()
                          << "\t: Handling getParentNode\n");
        // get the parent node of the arg node
        // get argument node
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];
        // get the parent node of the arg node
        // Add mapping <intrinsic, parent node> to the node-specific map
        // the argument node must have been added to the map, orelse the
        // code could not refer to it
        Leaf_HandleToDFNodeMap[II] = ArgDFNode->getParent();

        IItoRemove.push_back(II);
      } break;
        /*************************** llvm.hpvm.getNumDims()
         * ***************************/
      case Intrinsic::hpvm_getNumDims: {
        LLVM_DEBUG(errs() << F_fpga->getName() << "\t: Handling getNumDims\n");
        // get node from map
        // get the appropriate field
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];
        int numOfDim = ArgDFNode->getNumOfDim();
        LLVM_DEBUG(errs() << "\t  Got node dimension : " << numOfDim << "\n");
        IntegerType *IntTy = Type::getInt32Ty(KernelM->getContext());
        ConstantInt *numOfDimConstant =
            ConstantInt::getSigned(IntTy, (int64_t)numOfDim);

        // Replace the result of the intrinsic with the computed value
        II->replaceAllUsesWith(numOfDimConstant);

        IItoRemove.push_back(II);
      } break;
        /*********************** llvm.hpvm.getNodeInstanceID()
         * ************************/
      case Intrinsic::hpvm_getNodeInstanceID_x:
      case Intrinsic::hpvm_getNodeInstanceID_y:
      case Intrinsic::hpvm_getNodeInstanceID_z: {
        LLVM_DEBUG(errs() << F_fpga->getName()
                          << "\t: Handling getNodeInstanceID\n");
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];
        assert(ArgDFNode && "Arg node is NULL");
        // A leaf node always has a parent
        DFNode *ParentDFNode = ArgDFNode->getParent();
        assert(ParentDFNode && "Parent node of a leaf is NULL");

        // Get the number associated with the required dimension
        // FIXME: The order is important!
        // These three intrinsics need to be consecutive x,y,z
        uint64_t dim =
            II->getIntrinsicID() - Intrinsic::hpvm_getNodeInstanceID_x;
        assert((dim >= 0) && (dim < 3) && "Invalid dimension argument");
        LLVM_DEBUG(errs() << "\t  dimension = " << dim << "\n");

        // Argument of the function to be called
        ConstantInt *DimConstant =
            ConstantInt::get(Type::getInt32Ty(KernelM->getContext()), dim);
        // ArrayRef<Value *> Args(DimConstant);

        // The following is to find which function to call
        Function *OpenCLFunction;
        int parentLevel = N->getParent()->getLevel();
        int parentReplFactor = N->getParent()->getNumOfDim();
        LLVM_DEBUG(errs() << "Parent Level = " << parentLevel << "\n");
        LLVM_DEBUG(errs() << "Parent Repl factor = " << parentReplFactor
                          << "\n");

        FunctionType *FT = FunctionType::get(
            Type::getInt64Ty(KernelM->getContext()),
            ArrayRef<Type *>(Type::getInt32Ty(KernelM->getContext())), false);

        if ((!parentLevel || !parentReplFactor) && ArgDFNode == N) {
          // We only have one level in the hierarchy or the parent node is not
          // replicated. This indicates that the parent node is the kernel
          // launch, so we need to specify a global id.
          // We can translate this only if the argument is the current node
          // itself
          LLVM_DEBUG(errs() << "Substitute with get_global_id()\n");
          LLVM_DEBUG(errs() << *II << "\n");
          OpenCLFunction = cast<Function>(
              KernelM->getOrInsertFunction("get_global_id", FT).getCallee());
          //(KernelM->getOrInsertFunction(getMangledName("get_global_id"), FT));
        } else if (Leaf_HandleToDFNodeMap[ArgII] == N) {
          // We are asking for this node's id with respect to its parent
          // this is a local id call
          OpenCLFunction = cast<Function>(
              KernelM->getOrInsertFunction("get_local_id", FT).getCallee());
          //(KernelM->getOrInsertFunction(getMangledName("get_local_id"), FT));
        } else if (Leaf_HandleToDFNodeMap[ArgII] == N->getParent()) {
          // We are asking for this node's parent's id with respect to its
          // parent: this is a group id call
          OpenCLFunction = cast<Function>(
              KernelM->getOrInsertFunction("get_group_id", FT).getCallee());
          //(KernelM->getOrInsertFunction(getMangledName("get_group_id"), FT));
        } else {
          LLVM_DEBUG(errs() << N->getFuncPointer()->getName() << "\n";);
          LLVM_DEBUG(errs() << N->getParent()->getFuncPointer()->getName()
                            << "\n";);
          LLVM_DEBUG(errs() << *II << "\n";);

          assert(false && "Unable to translate getNodeInstanceID intrinsic");
        }

        // Create call instruction, insert it before the intrinsic and truncate
        // the output to 32 bits and replace all the uses of the previous
        // instruction with the new one
        CallInst *CI = CallInst::Create(OpenCLFunction, DimConstant, "", II);
        II->replaceAllUsesWith(CI);

        IItoRemove.push_back(II);
      } break;
        /********************** llvm.hpvm.getNumNodeInstances()
         * ***********************/
      case Intrinsic::hpvm_getNumNodeInstances_x:
      case Intrinsic::hpvm_getNumNodeInstances_y:
      case Intrinsic::hpvm_getNumNodeInstances_z: {
        // TODO: think about whether this is the best way to go
        // there are hw specific registers. therefore it is good to have the
        // intrinsic but then, why do we need to keep that info in the graph?
        // (only for the kernel configuration during the call)

        LLVM_DEBUG(errs() << F_fpga->getName()
                          << "\t: Handling getNumNodeInstances\n");
        ArgII = cast<IntrinsicInst>((II->getOperand(0))->stripPointerCasts());
        ArgDFNode = Leaf_HandleToDFNodeMap[ArgII];
        // A leaf node always has a parent
        DFNode *ParentDFNode = ArgDFNode->getParent();
        assert(ParentDFNode && "Parent node of a leaf is NULL");

        // Get the number associated with the required dimension
        // FIXME: The order is important!
        // These three intrinsics need to be consecutive x,y,z
        uint64_t dim =
            II->getIntrinsicID() - Intrinsic::hpvm_getNumNodeInstances_x;
        assert((dim >= 0) && (dim < 3) && "Invalid dimension argument");
        LLVM_DEBUG(errs() << "\t  dimension = " << dim << "\n");

        // Argument of the function to be called
        ConstantInt *DimConstant =
            ConstantInt::get(Type::getInt32Ty(KernelM->getContext()), dim);
        // ArrayRef<Value *> Args(DimConstant);

        // The following is to find which function to call
        Function *OpenCLFunction;
        int parentLevel = ParentDFNode->getLevel();
        int parentReplFactor = ParentDFNode->getNumOfDim();

        FunctionType *FT =
            FunctionType::get(Type::getInt64Ty(KernelM->getContext()),
                              Type::getInt32Ty(KernelM->getContext()), false);
        if ((N == ArgDFNode) && (!parentLevel || !parentReplFactor)) {
          // We only have one level in the hierarchy or the parent node is not
          // replicated. This indicates that the parent node is the kernel
          // launch, so the instances are global_size (gridDim x blockDim)
          OpenCLFunction = cast<Function>(
              KernelM->getOrInsertFunction("get_global_size", FT).getCallee());
          // (KernelM->getOrInsertFunction(getMangledName("get_global_size"),
          // FT));
        } else if (Leaf_HandleToDFNodeMap[ArgII] == N) {
          // We are asking for this node's instances
          // this is a local size (block dim) call
          OpenCLFunction = cast<Function>(
              KernelM->getOrInsertFunction("get_local_size", FT).getCallee());
          //(KernelM->getOrInsertFunction(getMangledName("get_local_size"),
          // FT));
        } else if (Leaf_HandleToDFNodeMap[ArgII] == N->getParent()) {
          // We are asking for this node's parent's instances
          // this is a (global_size/local_size) (grid dim) call
          OpenCLFunction = cast<Function>(
              KernelM->getOrInsertFunction("get_num_groups", FT).getCallee());
          //(KernelM->getOrInsertFunction(getMangledName("get_num_groups"),
          // FT));
        } else {
          assert(false && "Unable to translate getNumNodeInstances intrinsic");
        }

        // Create call instruction, insert it before the intrinsic and truncate
        // the output to 32 bits and replace all the uses of the previous
        // instruction with the new one
        CallInst *CI = CallInst::Create(OpenCLFunction, DimConstant, "", II);
        II->replaceAllUsesWith(CI);

        IItoRemove.push_back(II);
      } break;
      case Intrinsic::hpvm_barrier: {
        LLVM_DEBUG(errs() << F_fpga->getName() << "\t: Handling barrier\n");
        LLVM_DEBUG(errs() << "Substitute with barrier()\n");
        LLVM_DEBUG(errs() << *II << "\n");
        FunctionType *FT = FunctionType::get(
            Type::getVoidTy(KernelM->getContext()),
            std::vector<Type *>(1, Type::getInt32Ty(KernelM->getContext())),
            false);
        Function *OpenCLFunction = cast<Function>(
            KernelM->getOrInsertFunction("barrier", FT).getCallee());
        //(KernelM->getOrInsertFunction(getMangledName("barrier"), FT));
        CallInst *CI =
            CallInst::Create(OpenCLFunction,
                             ArrayRef<Value *>(ConstantInt::get(
                                 Type::getInt32Ty(KernelM->getContext()), 1)),
                             "", II);
        II->replaceAllUsesWith(CI);
        IItoRemove.push_back(II);
      } break;
      case Intrinsic::hpvm_atomic_add:
      case Intrinsic::hpvm_atomic_sub:
      case Intrinsic::hpvm_atomic_xchg:
      case Intrinsic::hpvm_atomic_min:
      case Intrinsic::hpvm_atomic_max:
      case Intrinsic::hpvm_atomic_and:
      case Intrinsic::hpvm_atomic_or:
      case Intrinsic::hpvm_atomic_xor: {
        LLVM_DEBUG(errs() << *II << "\n");
        // Only have support for i32 atomic intrinsics
        assert(II->getType() == Type::getInt32Ty(II->getContext()) &&
               "Only support i32 atomic intrinsics for now");
        // Substitute with appropriate atomic builtin
        assert(II->getNumArgOperands() == 2 &&
               "Expecting 2 operands for these atomics");

        Value *Ptr = II->getArgOperand(0);
        Value *Val = II->getArgOperand(1);
        assert(
            Ptr->getType()->isPointerTy() &&
            "First argument of supported atomics is expected to be a pointer");
        PointerType *PtrTy = cast<PointerType>(Ptr->getType());
        if (PtrTy !=
            Type::getInt32PtrTy(II->getContext(), PtrTy->getAddressSpace())) {
          Ptr = CastInst::CreatePointerCast(
              Ptr,
              Type::getInt32PtrTy(II->getContext(), PtrTy->getAddressSpace()),
              "", II);
        }

        StringRef name =
            getAtomicOpName(II->getIntrinsicID(), PtrTy->getAddressSpace());

        Type *paramTypes[] = {
            Type::getInt32PtrTy(II->getContext(), PtrTy->getAddressSpace()),
            Type::getInt32Ty(KernelM->getContext())};
        FunctionType *AtomicFT = FunctionType::get(
            II->getType(), ArrayRef<Type *>(paramTypes, 2), false);
        Function *AtomicFunction = cast<Function>(
            KernelM->getOrInsertFunction(name, AtomicFT).getCallee());
        Value *atomicArgs[] = {Ptr, Val};
        CallInst *AtomicInst = CallInst::Create(
            AtomicFunction, ArrayRef<Value *>(atomicArgs, 2), "", II);

        LLVM_DEBUG(errs() << "Substitute with: " << *AtomicInst << "\n");
        II->replaceAllUsesWith(AtomicInst);
        IItoRemove.push_back(II);
      } break;
      case Intrinsic::hpvm_nz_loop: {
        LLVM_DEBUG(errs() << *II << "\n");
        IItoRemove.push_back(II);
        break;
      }
      // Leave channels and ivdep intrinsics intact. They will be handled by
      // OpenCL backend.
      case Intrinsic::hpvm_ch_read:
      case Intrinsic::hpvm_ch_write:
      case Intrinsic::hpvm_ivdep: {
        Function *calleeF =
            cast<Function>(II->getCalledOperand()->stripPointerCasts());
        // Add the declaration to kernel module
        LLVM_DEBUG(errs() << "Adding declaration to Kernel module: " << *calleeF
                          << "\n");
        KernelM->getOrInsertFunction(calleeF->getName(),
                                     calleeF->getFunctionType());
        break;
      }
      default:
        assert(false && "Unknown HPVM Intrinsic!");
        break;
      }

    } else if (MemCpyInst *MemCpyI = dyn_cast<MemCpyInst>(I)) {
      IRBuilder<> Builder(I);
      Value *Source = MemCpyI->getSource();
      // Need to use stripInBoundsOffset() to get the GEPI when it is accessing
      // 0,0 stripPointerCasts() will skip this GEPI and return the alloca
      Value *Destination = MemCpyI->getArgOperand(0);
      //->stripInBoundsOffsets(); // stripPointerCasts();
      BitCastInst *LastBitCast;
      while (!isa<AllocaInst>(Destination) &&
             !isa<GetElementPtrInst>(Destination)) {
        assert(isa<BitCastInst>(Destination) &&
               "Destination has to be bitcast!\n");
        LastBitCast = dyn_cast<BitCastInst>(Destination);
        Destination = LastBitCast->getOperand(0);
      }
      Value *Length = MemCpyI->getOperand(2);

      LLVM_DEBUG(errs() << "Found memcpy instruction: " << *I << "\n");
      LLVM_DEBUG(errs() << "Source: " << *Source << "\n");
      LLVM_DEBUG(errs() << "Destination: " << *Destination << "\n");
      // If the destination is an Alloca, create a GEPI for it.
      if (AllocaInst *AI = dyn_cast<AllocaInst>(Destination)) {
        IRBuilder<> DestBuilder(LastBitCast);
        std::vector<Value *> GEPIndex;
        GEPIndex.push_back(
            ConstantInt::get(Type::getInt64Ty(M.getContext()), 0));
        GEPIndex.push_back(
            ConstantInt::get(Type::getInt64Ty(M.getContext()), 0));
        Value *newDestGEPI =
            DestBuilder.CreateGEP(Destination, ArrayRef<Value *>(GEPIndex));
        LastBitCast->setOperand(0, newDestGEPI);
        Destination = newDestGEPI;
      }
      LLVM_DEBUG(errs() << "Destination: " << *Destination << "\n");
      LLVM_DEBUG(errs() << "Length: " << *Length << "\n");

      size_t memcpy_length;
      unsigned int memcpy_count;
      if (ConstantInt *CI = dyn_cast<ConstantInt>(Length)) {
        if (CI->getBitWidth() <= 64) {
          memcpy_length = CI->getSExtValue();
          LLVM_DEBUG(errs() << "Memcpy lenght = " << memcpy_length << "\n");
          Type *Source_Type = Source->getType()->getPointerElementType();
          LLVM_DEBUG(errs() << "Source Type : " << *Source_Type << "\n");
          memcpy_count =
              memcpy_length / (Source_Type->getPrimitiveSizeInBits() / 8);
          LLVM_DEBUG(errs() << "Memcpy count = " << memcpy_count << "\n");
          if (GetElementPtrInst *sourceGEPI =
                  dyn_cast<GetElementPtrInst>(Source)) {
            if (GetElementPtrInst *destGEPI =
                    dyn_cast<GetElementPtrInst>(Destination)) {
              Value *SourcePtrOperand = sourceGEPI->getPointerOperand();
              Value *DestPtrOperand = destGEPI->getPointerOperand();
              for (int i = 0; i < memcpy_count; ++i) {
                Constant *increment;
                LoadInst *newLoadI;
                StoreInst *newStoreI;
                // First, need to increment the correct index for both source
                // and dest This invluves checking to see how many indeces the
                // GEP has Assume for now only 1 or 2 are the viable options.

                std::vector<Value *> GEPlIndex;
                if (sourceGEPI->getNumIndices() == 1) {
                  Value *Index = sourceGEPI->getOperand(1);
                  increment = ConstantInt::get(Index->getType(), i, false);
                  Value *incAdd = Builder.CreateAdd(Index, increment);
                  LLVM_DEBUG(errs() << "Add: " << *incAdd << "\n");
                  GEPlIndex.push_back(incAdd);
                  Value *newGEPIl = Builder.CreateGEP(
                      SourcePtrOperand, ArrayRef<Value *>(GEPlIndex));
                  LLVM_DEBUG(errs() << "Load GEP: " << *newGEPIl << "\n");
                  newLoadI = Builder.CreateLoad(newGEPIl);
                  LLVM_DEBUG(errs() << "Load: " << *newLoadI << "\n");
                } else {
                  llvm_unreachable("Unhandled case where source GEPI has more "
                                   "than 1 indices!\n");
                }

                std::vector<Value *> GEPsIndex;
                if (destGEPI->getNumIndices() == 1) {

                } else if (destGEPI->getNumIndices() == 2) {
                  Value *Index0 = destGEPI->getOperand(1);
                  GEPsIndex.push_back(Index0);
                  Value *Index1 = destGEPI->getOperand(2);
                  increment = ConstantInt::get(Index1->getType(), i, false);
                  Value *incAdd = Builder.CreateAdd(Index1, increment);
                  LLVM_DEBUG(errs() << "Add: " << *incAdd << "\n");
                  GEPsIndex.push_back(incAdd);
                  Value *newGEPIs = Builder.CreateGEP(
                      DestPtrOperand, ArrayRef<Value *>(GEPsIndex));
                  LLVM_DEBUG(errs() << "Store GEP: " << *newGEPIs << "\n");
                  newStoreI = Builder.CreateStore(newLoadI, newGEPIs,
                                                  MemCpyI->isVolatile());
                  LLVM_DEBUG(errs() << "Store: " << *newStoreI << "\n");
                } else {
                  llvm_unreachable("Unhandled case where dest GEPI has more "
                                   "than 2 indices!\n");
                }
              }
              Instruction *destBitcastI =
                  dyn_cast<Instruction>(MemCpyI->getArgOperand(0));
              Instruction *sourceBitcastI =
                  dyn_cast<Instruction>(MemCpyI->getArgOperand(1));
              if (destBitcastI->getNumUses() > 1) {
                LLVM_DEBUG(errs() << "Keeping " << *destBitcastI
                                  << " as it has more users!\n");
              } else {
                if (destGEPI->getNumUses() > 1) {
                  LLVM_DEBUG(errs() << "Keeping " << *destGEPI
                                    << " as it has more users!\n");
                } else {
                  ItoRemove.push_back(destGEPI);
                  LLVM_DEBUG(errs()
                             << "Pushing " << *destGEPI << " for removal\n");
                }
                ItoRemove.push_back(destBitcastI);
                LLVM_DEBUG(errs()
                           << "Pushing " << *destBitcastI << " for removal\n");
              }
              if (sourceBitcastI->getNumUses() > 1) {
                LLVM_DEBUG(errs() << "Keeping " << *sourceBitcastI
                                  << " as it has more users!\n");
              } else {
                if (sourceGEPI->getNumUses() > 1) {
                  LLVM_DEBUG(errs() << "Keeping " << *sourceGEPI
                                    << " as it has more users!\n");
                } else {
                  ItoRemove.push_back(sourceGEPI);
                  LLVM_DEBUG(errs()
                             << "Pushing " << *sourceGEPI << " for removal\n");
                }
                ItoRemove.push_back(sourceBitcastI);
                LLVM_DEBUG(errs() << "Pushing " << *sourceBitcastI
                                  << " for removal\n");
              }
              ItoRemove.push_back(MemCpyI);
              LLVM_DEBUG(errs() << "Pushing " << *MemCpyI << " for removal\n");
            }
          }
        }
      } else {
        llvm_unreachable("MEMCPY length is not a constant, not handled!\n");
      }
      //      llvm_unreachable("HERE!");
    } else if (CallInst *CI = dyn_cast<CallInst>(I)) {
      LLVM_DEBUG(errs() << "Found a call: " << *CI << "\n");
      Function *calleeF =
          cast<Function>(CI->getCalledOperand()->stripPointerCasts());
      if (calleeF->isDeclaration()) {
        // Add the declaration to kernel module
        LLVM_DEBUG(errs() << "Adding declaration to Kernel module: " << *calleeF
                          << "\n");
        KernelM->getOrInsertFunction(calleeF->getName(),
                                     calleeF->getFunctionType());
        if (IntrinsicInst *II = dyn_cast<IntrinsicInst>(CI)) {
          // Now handle a few specific intrinsics
          // For now, sin and cos are translated to their libclc equivalent
          switch (II->getIntrinsicID()) {
          case Intrinsic::sin:
          case Intrinsic::cos:
          case Intrinsic::sqrt:
          case Intrinsic::floor: {
            LLVM_DEBUG(errs() << "Found math function: " << *II << "\n");
            // Get the builtin function
            assert(II->getType()->isFloatTy() &&
                   "Only handling sin(float) and cos(float)!");
            std::string name = getMathFunctionName(II->getIntrinsicID());

            FunctionType *MathFT = FunctionType::get(
                II->getType(), Type::getFloatTy(KernelM->getContext()), false);
            Function *MathFunction = cast<Function>(
                KernelM->getOrInsertFunction(name, MathFT).getCallee());
            CallInst *CI = CallInst::Create(MathFunction, II->getArgOperand(0),
                                            II->getName(), II);

            II->replaceAllUsesWith(CI);
            IItoRemove.push_back(II);
            break;
          }
          default:
            LLVM_DEBUG(errs() << "[WARNING] Found Intrinsic: " << *II << "\n");
          }
        }

      } else {
        // Clone the function
        ValueToValueMapTy VMap;
        Function *newCalleeF = CloneFunction(calleeF, VMap);
        newCalleeF->removeFromParent(); // TODO: MARIA check
        KernelM->getFunctionList().push_back(newCalleeF);
        // ADEL: callinst creation was wrong. Replaced with setting new called
        // func.
        CI->setCalledFunction(newCalleeF);
        //        CallInst *CInew = CallInst::Create(newCalleeF,
        //        CI->getArgOperand(0),
        //                                           CI->getName(), CI);
        //        CI->replaceAllUsesWith(CInew);
        //        CItoRemove.push_back(CI);
        //        LLVM_DEBUG(errs() << "New Call Inast: " << *CInew << "\n");
        LLVM_DEBUG(errs() << "Modified Call Inst: " << *CI << "\n");
        //        llvm_unreachable("here!");
      }
    }
  }

  // search for pattern where float is being casted to int and loaded/stored and
  // change it.
  LLVM_DEBUG(errs() << "finding pattern for replacement!\n");
  for (inst_iterator i = inst_begin(F_fpga), e = inst_end(F_fpga); i != e;
       ++i) {
    bool cont = false;
    bool keepGEPI = false;
    bool keepGEPI2 = false;
    bool keepBCI = false;
    bool keepBCI2 = false;
    bool keepLI = false;
    bool keepSI = false;

    Instruction *I = &(*i);
    GetElementPtrInst *GEPI = dyn_cast<GetElementPtrInst>(I);

    if (!GEPI) {
      // did nod find pattern start, continue
      continue;
    }
    // may have found pattern, check
    LLVM_DEBUG(errs() << "GEPI " << *GEPI << "\n");
    // print whatever we want for debug
    Value *PtrOp = GEPI->getPointerOperand();
    Type *SrcTy = GEPI->getSourceElementType();
    unsigned GEPIaddrspace = GEPI->getAddressSpace();

    if (SrcTy->isArrayTy())
      LLVM_DEBUG(errs() << *SrcTy << " is an array type! "
                        << *(SrcTy->getArrayElementType()) << "\n");
    else
      LLVM_DEBUG(errs() << *SrcTy << " is not an array type!\n");
    // check that source element type is float
    if (SrcTy->isArrayTy()) {
      if (!(SrcTy->getArrayElementType()->isFloatTy())) {
        LLVM_DEBUG(errs() << "GEPI type is array but not float!\n");
        continue;
      }
    } else if (!(SrcTy->isFPOrFPVectorTy() /*isFloatTy()*/)) {
      LLVM_DEBUG(errs() << "GEPI type is " << *SrcTy << "\n");
      // does not fit this pattern - no float GEP instruction
      continue;
    }

    if (!(GEPI->hasOneUse())) {
      // Keep GEPI around if it has other uses
      keepGEPI = true;
    }
    LLVM_DEBUG(errs() << "Found GEPI " << *GEPI << "\n");

    // See if it is a bitcast
    std::vector<BitCastInst *> BitCastInsts;
    for (User *U : GEPI->users()) {
      if (Instruction *ui = dyn_cast<Instruction>(U)) {
        LLVM_DEBUG(errs() << "--" << *ui << "\n");
        if (isa<BitCastInst>(ui)) {
          BitCastInsts.push_back(dyn_cast<BitCastInst>(ui));
          LLVM_DEBUG(errs() << "---Found bitcast use of GEP\n");
        }
      }
    }

    if (BitCastInsts.size() == 0) {
      LLVM_DEBUG(errs() << "GEPI does not have a bitcast user, continue\n");
      continue; // not in pattern
    }

    for (auto BitCastI : BitCastInsts) {
      // Otherwise, check that first operand is GEP and 2nd is i32*. 1st Operand
      // has to be the GEP, since this is a use of the GEP.
      Value *Op2 = BitCastI->getOperand(0);
      LLVM_DEBUG(errs() << "----" << *Op2 << "\n");
      //		assert(cast<Type>(Op2) && "Invalid Operand for
      // Bitcast\n"); 		Type *OpTy = cast<Type>(Op2);
      Type *OpTy = BitCastI->getDestTy();
      LLVM_DEBUG(errs() << "---- Bitcast destination type: " << *OpTy << "\n");
      //    LLVM_DEBUG(errs() << "---- " <<
      //    *(Type::getInt32PtrTy(M.getContext(),1))
      //    <<
      //    "\n");
      if (!(OpTy == Type::getInt32PtrTy(M.getContext(), GEPIaddrspace))) {
        // maybe right syntax is (Type::getInt32Ty)->getPointerTo()
        continue; // not in pattern
      }

      LLVM_DEBUG(errs() << "----Here!\n");
      // We are in GEP, bitcast.

      // user_iterator, to find the load.

      if (!(BitCastI->hasOneUse())) {
        keepBCI = true;
        keepGEPI = true;
      }

      std::vector<LoadInst *> LoadInsts;
      for (User *U : BitCastI->users()) {
        if (Instruction *ui = dyn_cast<Instruction>(U)) {
          LLVM_DEBUG(errs() << "-----" << *ui << "\n");
          if (isa<LoadInst>(ui)) {
            LoadInsts.push_back(dyn_cast<LoadInst>(ui));
            LLVM_DEBUG(errs() << "-----Found load use of bitcast\n");
          }
        }
      }

      if (LoadInsts.size() == 0) {
        LLVM_DEBUG(errs() << "Bitcast does not have a load user, continue!\n");
        continue; // not in pattern
      }

      for (auto LoadI : LoadInsts) {
        // check that we load from pointer we got from bitcast - assert - the
        // unique argument must be the use we found it from
        assert(LoadI->getPointerOperand() == BitCastI &&
               "Unexpected Load Instruction Operand\n");

        // Copy user_iterator, to find the store.

        if (!(LoadI->hasOneUse())) {
          // does not fit this pattern - more than one uses
          continue;
          // TODO: generalize: one load can have more than one store users
        }

        // it has one use
        assert(LoadI->hasOneUse() && "LoadI has a single use");
        Value::user_iterator ui = LoadI->user_begin();
        // skipped loop, because is has a single use
        StoreInst *StoreI = dyn_cast<StoreInst>(*ui);
        if (!StoreI) {
          continue; // not in pattern
        }

        // Also check that the store uses the loaded value as the value operand
        if (StoreI->getValueOperand() != LoadI) {
          continue;
        }

        LLVM_DEBUG(errs() << "-------Found store instruction\n");

        // Look for its bitcast, which is its pointer operand
        Value *StPtrOp = StoreI->getPointerOperand();
        LLVM_DEBUG(errs() << "-------" << *StPtrOp << "\n");
        BitCastInst *BitCastI2 = dyn_cast<BitCastInst>(StPtrOp);
        LLVM_DEBUG(errs() << "-------" << *BitCastI2 << "\n");
        if (!BitCastI2) {
          continue; // not in pattern
        }

        if (!(BitCastI2->hasOneUse())) {
          keepBCI2 = true;
          keepGEPI2 = true;
        }

        LLVM_DEBUG(errs() << "-------- Found Bit Cast of store!\n");
        // found bitcast. Look for the second GEP, its from operand.
        Value *BCFromOp = BitCastI2->getOperand(0);
        GetElementPtrInst *GEPI2 = dyn_cast<GetElementPtrInst>(BCFromOp);
        if (!GEPI2) {
          continue; // not in pattern
        }
        LLVM_DEBUG(errs() << "---------- " << *GEPI2 << "\n");
        if (!(GEPI2->hasOneUse())) {
          // does not fit this pattern - more than one uses
          // continue;
          // Keep GEPI around if it has other uses
          keepGEPI2 = true;
        }
        LLVM_DEBUG(errs() << "---------- Found GEPI of Bitcast!\n");

        Value *PtrOp2 = GEPI2->getPointerOperand();

        // Found GEPI2. TODO: kind of confused as o what checks I need to add
        // here, let's add them together- all the code for int-float type checks
        // is already above.

        // Assume we found pattern
        if (!keepGEPI) {
          ItoRemove.push_back(GEPI);
          LLVM_DEBUG(errs() << "Pushing " << *GEPI << " for removal\n");
        } else {
          LLVM_DEBUG(errs() << "Keeping " << *GEPI
                            << " since it has multiple uses!\n");
        }
        if (!keepBCI) {
          ItoRemove.push_back(BitCastI);
          LLVM_DEBUG(errs() << "Pushing " << *BitCastI << " for removal\n");
        } else {
          LLVM_DEBUG(errs() << "Keeping " << *BitCastI
                            << " since it has multiple uses!\n");
        }

        ItoRemove.push_back(LoadI);
        LLVM_DEBUG(errs() << "Pushing " << *LoadI << " for removal\n");
        if (!keepGEPI2) {
          ItoRemove.push_back(GEPI2);
          LLVM_DEBUG(errs() << "Pushing " << *GEPI2 << " for removal\n");
        } else {

          LLVM_DEBUG(errs() << "Keeping " << *GEPI2
                            << " since it has multiple uses!\n");
        }
        if (!keepBCI2) {
          ItoRemove.push_back(BitCastI2);
          LLVM_DEBUG(errs() << "Pushing " << *BitCastI2 << " for removal\n");
        } else {
          LLVM_DEBUG(errs() << "Keeping " << *BitCastI2
                            << " since it has multiple uses!\n");
        }
        ItoRemove.push_back(StoreI);
        LLVM_DEBUG(errs() << "Pushing " << *StoreI << " for removal\n");

        std::vector<Value *> GEPlIndex;
        if (GEPI->hasIndices()) {
          for (auto ii = GEPI->idx_begin(); ii != GEPI->idx_end(); ++ii) {
            Value *Index = dyn_cast<Value>(&*ii);
            LLVM_DEBUG(errs() << "GEP-1 Index: " << *Index << "\n");
            GEPlIndex.push_back(Index);
          }
        }
        //    ArrayRef<Value*> GEPlArrayRef(GEPlIndex);

        std::vector<Value *> GEPsIndex;
        if (GEPI2->hasIndices()) {
          for (auto ii = GEPI2->idx_begin(); ii != GEPI2->idx_end(); ++ii) {
            Value *Index = dyn_cast<Value>(&*ii);
            LLVM_DEBUG(errs() << "GEP-2 Index: " << *Index << "\n");
            GEPsIndex.push_back(Index);
          }
        }
        //    ArrayRef<Value*> GEPsArrayRef(GEPlIndex);

        //    ArrayRef<Value*>(GEPI->idx_begin(), GEPI->idx_end());
        GetElementPtrInst *newlGEP = GetElementPtrInst::Create(
            GEPI->getSourceElementType(), // Type::getFloatTy(M.getContext()),
            PtrOp,                        // operand from 1st GEP
            ArrayRef<Value *>(GEPlIndex), Twine(), StoreI);
        LLVM_DEBUG(errs() << "Adding: " << *newlGEP << "\n");
        // insert load before GEPI
        LoadInst *newLoadI =
            new LoadInst(Type::getFloatTy(M.getContext()),
                         newlGEP, // new GEP
                         Twine(), LoadI->isVolatile(), LoadI->getAlign(),
                         LoadI->getOrdering(), LoadI->getSyncScopeID(), StoreI);
        LLVM_DEBUG(errs() << "Adding: " << *newLoadI << "\n");
        // same for GEP for store, for store operand
        GetElementPtrInst *newsGEP = GetElementPtrInst::Create(
            GEPI2->getSourceElementType(), // Type::getFloatTy(M.getContext()),
            PtrOp2,                        // operand from 2nd GEP
            ArrayRef<Value *>(GEPsIndex), Twine(), StoreI);
        LLVM_DEBUG(errs() << "Adding: " << *newsGEP << "\n");
        // insert store before GEPI
        StoreInst *newStoreI = new StoreInst(
            newLoadI,
            newsGEP, // new GEP
            StoreI->isVolatile(), StoreI->getAlign(), StoreI->getOrdering(),
            StoreI->getSyncScopeID(), StoreI);
        LLVM_DEBUG(errs() << "Adding: " << *newStoreI << "\n");
      }
    }
  }

  // Remove old instructions
  LLVM_DEBUG(errs() << "Printing ItoRemove vector:\n");
  for (std::vector<Instruction *>::reverse_iterator ri = ItoRemove.rbegin();
       ri != ItoRemove.rend(); ++ri) {
    Instruction *rinst = *ri;
    LLVM_DEBUG(errs() << *rinst << " \n");
  }
  for (std::vector<Instruction *>::reverse_iterator ri = ItoRemove.rbegin();
       ri != ItoRemove.rend(); ++ri) {
    Instruction *rinst = *ri;
    LLVM_DEBUG(errs() << "Erasing: " << *rinst << " \n");
    //		(*ri)->replaceAllUsesWith(UndefValue::get((*ri)->getType()));
    (*ri)->eraseFromParent();
  }
  // We need to do this explicitly: DCE pass will not remove them because we
  // have assumed theworst memory behaviour for these function calls
  // Traverse the vector backwards, otherwise definitions are deleted while
  // their subsequent uses are still around
  for (std::vector<IntrinsicInst *>::reverse_iterator ri = IItoRemove.rbegin(),
                                                      re = IItoRemove.rend();
       ri != re; ++ri)
    (*ri)->eraseFromParent();

  for (std::vector<CallInst *>::reverse_iterator ri = CItoRemove.rbegin(),
                                                 re = CItoRemove.rend();
       ri != re; ++ri)
    (*ri)->eraseFromParent();

  addCLMetadata(F_fpga);
  kernel->KernelFunction = F_fpga;
  LLVM_DEBUG(errs() << "Identified kernel - "
                    << kernel->KernelFunction->getName() << "\n";);
  LLVM_DEBUG(errs() << *KernelM);
  ChannelsMap[F_fpga] = ChannelsMap[F];

  return;
}

bool runDFG2LLVM_FPGA(Module &M, BuildDFG &DFG, bool _EnableTLP,
                      bool _Emulation, bool OnlyHost, std::string KMN,
                      std::string BFN, const char *_Board) {
  errs() << "\nDFG2LLVM_FPGA PASS\n";
  EnableTLP = _EnableTLP;
  Emulation = _Emulation;
  Board = _Board;
  // Get the BuildDFG Analysis Results:
  // - Dataflow graph
  // - Maps from i8* hansles to DFNode and DFEdge
  LLVM_DEBUG(errs() << "Built DFG\n");
  LLVM_DEBUG(errs() << M);
  // DFInternalNode *Root = DFG.getRoot();
  std::vector<DFInternalNode *> Roots = DFG.getRoots();
  //    BuildDFG::HandleToDFNode &HandleToDFNodeMap =
  //    DFG.getHandleToDFNodeMap(); BuildDFG::HandleToDFEdge &HandleToDFEdgeMap
  //    = DFG.getHandleToDFEdgeMap();
  LLVM_DEBUG(errs() << "Got roots\n");
  // Visitor for Code Generation Graph Traversal
  CGT_FPGA *CGTVisitor = new CGT_FPGA(M, DFG, KMN, BFN);
  LLVM_DEBUG(errs() << "Initiating code generation starting at root\n");
  // Iterate over all the DFGs and produce code for each one of them
  for (auto rootNode : Roots) {
    // Initiate code generation for root DFNode
    CGTVisitor->visit(rootNode);
  }

  // This is not required. Itrinsics that do not have a use are not a problem
  // CGTVisitor->removeLLVMIntrinsics();
  // Run some canonicalization passes
  for (auto &F : M) {
    if (!F.isDeclaration()) {
      FunctionAnalysisManager FAM;
      PassBuilder PB;
      PB.registerFunctionAnalyses(FAM);
      FunctionPassManager FPM;
      FPM.addPass(EarlyCSEPass(false));
      FPM.addPass(SimplifyCFGPass());
      FPM.addPass(ADCEPass());
      FPM.run(F, FAM);
    }
  }
  if (!OnlyHost)
    hpvmUtils::writeOutputModule(*CGTVisitor->getKernelM(),
                                 CGTVisitor->getKernelModuleName());

  // TODO: Edit module epilogue to remove the HPVM intrinsic declarations
  delete CGTVisitor;

  return true;
}

bool DFG2LLVM_FPGA::runOnModule(Module &M) {
  errs() << "\nDFG2LLVM_FPGA PASS\n";

  // Get the BuildDFG Analysis Results:
  // - Dataflow graph
  // - Maps from i8* hansles to DFNode and DFEdge
  BuildDFG &DFG = getAnalysis<BuildDFG>();
  LLVM_DEBUG(errs() << "Built DFG\n");

  return runDFG2LLVM_FPGA(M, DFG, HPVM_FPGA_TLP, HPVM_FPGA_EMULATION, 0,
                          hpvmUtils::defaultKernelsModuleName(M));
}

// std::string CGT_FPGA::getKernelsModuleName(Module &M) {
//   /*SmallString<128> currentDir;
//           llvm::sys::fs::current_path(currentDir);
//           std::string fileName = getFilenameFromModule(M);
//           Twine output = Twine(currentDir) + "/Output/" + fileName + "";
//           return output.str().append(".kernels.ll");*/
//   if (KernelModuleName != "")
//     return KernelModuleName;
//   std::string mid = M.getModuleIdentifier();
//   if (mid.find(".ll") != std::string::npos) {
//     mid = mid.substr(0, mid.find(".ll"));
//   } else if (mid.find(".bc") != std::string::npos) {
//     mid = mid.substr(0, mid.find(".bc"));
//   }
//   return mid.append(".kernels.ll");
// }

Type *updatePtrTypeWithAddrSpace(PointerType *PT, unsigned addrspace) {
  Type *EltType = PT->getElementType();
  if (EltType->isPointerTy())
    EltType =
        updatePtrTypeWithAddrSpace(dyn_cast<PointerType>(EltType), addrspace);
  return PointerType::get(EltType, addrspace);
}

void CGT_FPGA::fixValueAddrspace(Value *V, unsigned addrspace) {
  assert(isa<PointerType>(V->getType()) && "Value should be of Pointer Type!");
  PointerType *OldTy = cast<PointerType>(V->getType());
  //  PointerType *NewTy = PointerType::get(OldTy->getElementType(), addrspace);
  PointerType *NewTy =
      dyn_cast<PointerType>(updatePtrTypeWithAddrSpace(OldTy, addrspace));
  V->mutateType(NewTy);
  for (Value::user_iterator ui = V->user_begin(), ue = V->user_end(); ui != ue;
       ui++) {
    // Change all uses producing pointer type in same address space to new
    // addressspace.
    if (PointerType *PTy = dyn_cast<PointerType>((*ui)->getType())) {
      if (PTy->getAddressSpace() == OldTy->getAddressSpace()) {
        fixValueAddrspace(*ui, addrspace);
      }
    }
  }
}

// Type *updatePtrTypeWithAddrSpace(PointerType *PT, unsigned addrspace) {
//  LLVM_DEBUG(errs() << *PT << "\n");
//  Type *EltType = PT->getElementType();
//  if (EltType->isPointerTy())
//    EltType =
//        updatePtrTypeWithAddrSpace(dyn_cast<PointerType>(EltType), addrspace);
//  return PointerType::get(EltType, addrspace);
//}
//
// void CGT_FPGA::fixValueAddrspace(
//    Value *V, unsigned addrspace,
//    std::map<CallInst *, std::vector<unsigned>> &CIArgMapi,
//    std::map<GetElementPtrInst *, StructType*> &GEPArgMap) {
//  assert(isa<PointerType>(V->getType()) && "Value should be of Pointer
//  Type!"); LLVM_DEBUG(errs() << *V << "\n"); PointerType *OldTy =
//  cast<PointerType>(V->getType());
//  //  PointerType *NewTy = PointerType::get(OldTy->getElementType(),
//  addrspace); PointerType *NewTy =
//      dyn_cast<PointerType>(updatePtrTypeWithAddrSpace(OldTy, addrspace));
//  V->mutateType(NewTy);
//
//  // If we are dealing with GEPIs and Structs, need to make sure the struct
//  // types are also handled.
//  if (auto GEPI = dyn_cast<GetElementPtrInst>(V)) {
//    GEPI->getType()->dump();
//    auto *SrcEltTy = GEPI->getSourceElementType();
//    if (SrcEltTy->isStructTy()) {
//      auto *SrcStructTy = dyn_cast<StructType>(SrcEltTy);
//      assert(GEPI->getNumIndices() == 2 &&
//             "Expecting struct gepi to have exactly 2 indices!");
//      auto StructIdx =
//          dyn_cast<ConstantInt>((GEPI->idx_begin() +
//          1)->get())->getZExtValue();
//      auto *StructElementTy = SrcStructTy->getTypeAtIndex(StructIdx);
//      LLVM_DEBUG(errs() << *StructElementTy << "\n");
//      if (StructElementTy != GEPI->getType()->getPointerElementType()) {
//        auto newStructElementTy = GEPI->getType()->getPointerElementType();
//        assert(StructElementTy->isPointerTy() &&
//               newStructElementTy->isPointerTy() && "Expecting pointer
//               types!");
//        LLVM_DEBUG(errs() << "Need to fix struct type due to mismatch!\n");
//        LLVM_DEBUG(errs() << (SrcStructTy->isLiteral() ? "is Literal Struct\n"
//                                                  : "is Identified
//                                                  Struct\n"));
//        assert(!SrcStructTy->isLiteral() && "Not expecting literal struct
//        here!"); auto StructElts = SrcStructTy->elements().vec();
//        StructElts[StructIdx] = newStructElementTy;
//        Twine newStructName =
//            SrcStructTy->getName() + "_addrspace_" +
//            std::to_string(addrspace);
//        StructType *newStructTy =
//            StructType::create(ArrayRef<Type *>(StructElts),
//                               newStructName.str(), SrcStructTy->isPacked());
//
//      }
//    }
//  }
//  for (Value::user_iterator ui = V->user_begin(), ue = V->user_end(); ui !=
//  ue;
//       ui++) {
//    // Change all uses producing pointer type in same address space to new
//    // addressspace.
//    if (PointerType *PTy = dyn_cast<PointerType>((*ui)->getType())) {
//      if (PTy->getAddressSpace() == OldTy->getAddressSpace()) {
//        fixValueAddrspace(*ui, addrspace, CIArgMap);
//      }
//    }
//    // If Value is used in a function call, we need to modify the called
//    // function. Creating a cloned version of the called function ensures that
//    // other callers that might be calling the original function will not
//    break. if (CallInst *CI = dyn_cast<CallInst>(*ui)) {
//      // Implementation of CallBase::getArgOperandNo taken from newer LLVM
//      // version
//      unsigned ArgNo = &ui.getUse() - CI->arg_begin();
//      LLVM_DEBUG(errs() << ArgNo << "\n");
//      CIArgMap[CI].push_back(ArgNo);
//    }
//  }
//}
//
// void expandGEPIs(Function *F) {
//  SmallVector<Instruction *, 8> InstsToRemove;
//  for (auto ii = inst_begin(F), ie = inst_end(F); ii != ie; ++ii) {
//    Instruction *I = &*ii;
//    if (GetElementPtrInst *GEPI = dyn_cast<GetElementPtrInst>(I)) {
//      if (GEPI->getNumIndices() > 2) {
//        if (GEPI->hasAllConstantIndices()) {
//          LLVM_DEBUG(errs() << "Expanding GEPI: " << *GEPI << "\n");
//          Value *PtrOperand = GEPI->getPointerOperand();
//          GetElementPtrInst *newGEPI;
//          for (auto idx = GEPI->idx_begin() + 1; idx != GEPI->idx_end();
//               ++idx) {
//            ConstantInt *IdxVal = dyn_cast<ConstantInt>(&*idx);
//            IdxVal->dump();
//            newGEPI = GetElementPtrInst::CreateInBounds(
//                PtrOperand,
//                ArrayRef<Value *>(
//                    {ConstantInt::get(Type::getInt64Ty(F->getContext()), 0),
//                     IdxVal}),
//                "", GEPI);
//            LLVM_DEBUG(errs() << "Adding: " << *newGEPI << "\n");
//            PtrOperand = newGEPI;
//          }
//          assert(GEPI->getType() == newGEPI->getType() &&
//                 "Unexpected behavior in GEPI expansion!");
//          InstsToRemove.push_back(GEPI);
//          GEPI->replaceAllUsesWith(newGEPI);
//        }
//      }
//    }
//  }
//  for (auto I : InstsToRemove) {
//    I->eraseFromParent();
//  }
//}
//
// Function *CGT_FPGA::changeArgAddrspace(Function *F, std::vector<unsigned>
// &Args,
//                                       unsigned addrspace, bool isHPVMNode) {
//  LLVM_DEBUG(errs() << "In changeArgAddrSpace()\n");
//  std::vector<Type *> ArgTypes;
//
//  expandGEPIs(F);
//
//  // Clone the function first to not modify original function
//  // needed for case when this is not an HPVM node function
//  // because we do not want to delete the original function.
//  ValueToValueMapTy VMapClone;
//  Function *FClone = llvm::CloneFunction(F, VMapClone);
//  FClone->setAttributes(F->getAttributes());
//  // Create a map to keep track of any callinsts whose arguments
//  // get assigned an address space.
//  std::map<CallInst *, std::vector<unsigned>> CIArgMap;
//  for (Function::arg_iterator ai = FClone->arg_begin(), ae =
//  FClone->arg_end();
//       ai != ae; ++ai) {
//    Argument *arg = &*ai;
//    LLVM_DEBUG(errs() << *arg << "\n");
//    unsigned argno = arg->getArgNo();
//    if (std::find(Args.begin(), Args.end(), argno) != Args.end()) {
//      fixValueAddrspace(arg, addrspace, CIArgMap);
//    }
//    ArgTypes.push_back(arg->getType());
//  }
//  FunctionType *newFT = FunctionType::get(F->getReturnType(), ArgTypes,
//  false);
//
//  // Check if any CallInsts need handling
//  for (auto cii : CIArgMap) {
//    CallInst *CI = cii.first;
//    std::vector<unsigned> ArgNos = cii.second;
//    Function *CF = CI->getCalledFunction();
//    Function *newCF = changeArgAddrspace(CF, ArgNos, addrspace, 0);
//    CI->setCalledFunction(newCF);
//  }
//
//  // F->mutateType(PTy);
//  Function *newF;
//  if (isHPVMNode) {
//    // This is an HPVM node, use the HPVM cloneFunction()
//    newF = cloneFunction(FClone, newFT, false);
//    replaceNodeFunctionInIR(*F->getParent(), F, newF);
//  } else {
//    Twine newFName =
//        FClone->getName() + "_addrspace_" + std::to_string(addrspace);
//    newF = Function::Create(newFT, FClone->getLinkage(), newFName,
//                            FClone->getParent());
//    ValueToValueMapTy VMap;
//
//    for (auto ai = FClone->arg_begin(), ae = FClone->arg_end(),
//              new_ai = newF->arg_begin();
//         ai != ae; ++ai, ++new_ai) {
//      assert(ai->getType() == new_ai->getType() && "Arguments do not match!");
//      VMap[&*ai] = &*new_ai;
//      new_ai->takeName(&*ai);
//    }
//    SmallVector<ReturnInst *, 8> Returns;
//    CloneFunctionInto(newF, FClone, VMap, false, Returns);
//  }
//  FClone->eraseFromParent();
//  LLVM_DEBUG(errs() << *newF->getFunctionType() << "\n" << *newF << "\n");
//  return newF;
//}
Function *CGT_FPGA::changeArgAddrspace(Function *F, std::vector<unsigned> &Args,
                                       unsigned addrspace) {
  LLVM_DEBUG(errs() << "In changeArdAddrSpace()\n");
  unsigned idx = 0;
  std::vector<Type *> ArgTypes;
  for (Function::arg_iterator ai = F->arg_begin(), ae = F->arg_end(); ai != ae;
       ++ai) {
    Argument *arg = &*ai;
    LLVM_DEBUG(errs() << *arg << "\n");
    unsigned argno = arg->getArgNo();
    if ((idx < Args.size()) && (argno == Args[idx])) {
      fixValueAddrspace(arg, addrspace);
      idx++;
    }
    ArgTypes.push_back(arg->getType());
  }
  FunctionType *newFT = FunctionType::get(F->getReturnType(), ArgTypes, false);

  // F->mutateType(PTy);
  Function *newF = cloneFunction(F, newFT, false);
  replaceNodeFunctionInIR(*F->getParent(), F, newF);

  LLVM_DEBUG(errs() << *newF->getFunctionType() << "\n" << *newF << "\n");
  return newF;
}

/* Add metadata to module KernelM, for OpenCL kernels */
void CGT_FPGA::addCLMetadata(Function *F) {
  // TODO: There is additional metadata used by kernel files but we skip them as
  // they are not mandatory. In future they might be useful to enable
  // optimizations

  IRBuilder<> Builder(&*F->begin());

  // Create node for "kernel_arg_type"
  SmallVector<Metadata *, 8> argTypeNames;
  argTypeNames.push_back(
      MDString::get(KernelM->getContext(), "kernel_arg_type"));

  for (Function::arg_iterator ai = F->arg_begin(), ae = F->arg_end(); ai != ae;
       ai++) {
    argTypeNames.push_back(
        MDString::get(KernelM->getContext(), printType(ai->getType())));
  }
  // All argument type names are in the vector. Create a metadata node
  // "kernel_arg_type"
  MDTuple *KernelArgTypes = MDNode::get(KernelM->getContext(), argTypeNames);

  // Create kernel metadata node containg the kernel function and the
  // "kernel_arg_type" metadata node created above
  SmallVector<Metadata *, 8> KernelMD;
  KernelMD.push_back(ValueAsMetadata::get(F));
  KernelMD.push_back(KernelArgTypes);
  MDTuple *MDKernelNode = MDNode::get(KernelM->getContext(), KernelMD);

  // Create metadata node opencl.kernels. It points to the kernel metadata node
  NamedMDNode *MDN_kernels =
      KernelM->getOrInsertNamedMetadata("opencl.kernels");
  MDN_kernels->addOperand(MDKernelNode);

  // KernelMD.push_back(MDString::get(KernelM->getContext(), "kernel"));
  // TODO: Replace 1 with the number of the kernel.
  // Add when support for multiple launces is added
  // KernelMD.push_back(ConstantInt::get(Type::getInt32Ty(KernelM->getContext()),1));
  // MDNode *MDNvvmAnnotationsNode = MDNode::get(KernelM->getContext(),
  // KernelMD); NamedMDNode *MDN_annotations =
  // KernelM->getOrInsertNamedMetadata("nvvm.annotations");
  // MDN_annotations->addOperand(MDNvvmAnnotationsNode);
}

// We need to sort the children before processing them but CodeGenTraversal
// processes children before their parent
void CGT_FPGA::visit(DFInternalNode *N) {
  N->getChildGraph()->sortChildren();
  CodeGenTraversal::visit(N);
}

/* Function to remove all remaining declarations of llvm intrinsics,
 * as they are not supported in FPGA.
 */
void CGT_FPGA::removeLLVMIntrinsics() {

  std::vector<Function *> fv = std::vector<Function *>();

  for (Module::iterator mi = KernelM->begin(), me = KernelM->end(); (mi != me);
       ++mi) {
    Function *F = &*mi;
    if (F->isDeclaration() && F->getName().startswith("llvm.")) {
      LLVM_DEBUG(errs() << "Declaration: " << F->getName() << " with "
                        << F->getNumUses() << "uses.\n");
      assert(F->hasNUses(0) && "LLVM intrinsic function still in use");
      fv.push_back(F);
    }
  }

  for (std::vector<Function *>::iterator vi = fv.begin(); vi != fv.end();
       ++vi) {
    LLVM_DEBUG(errs() << "Erasing declaration: " << (*vi)->getName() << "\n");
    (*vi)->replaceAllUsesWith(UndefValue::get((*vi)->getType()));
    (*vi)->eraseFromParent();
  }
}

// void CGT_FPGA::writeKernelsModule() {

//   //  // First need to verify the kernels module
//   //  assert(!verifyModule(*KernelM, &errs(), 0) && "Kernel Module is Broken!
//   //  Aborting!\n");

//   // In addition to deleteing all otjer functions, we also want to spice it
//   up a
//   // little bit. Do this now.
//   legacy::PassManager Passes;

//   std::error_code EC;
//   ToolOutputFile Out(getKernelsModuleName(M).c_str(), EC, sys::fs::OF_None);
//   if (EC) {
//     LLVM_DEBUG(errs() << EC.message() << "\n";);
//   }

//   Passes.add(createPrintModulePass(Out.os()));

//   Passes.run(*KernelM);

//   // Declare success.
//   Out.keep();
// }

Function *CGT_FPGA::transformFunctionToVoid(Function *F) {

  // FIXME: Maybe do that using the Node?
  StructType *FRetTy = cast<StructType>(F->getReturnType());
  assert(FRetTy && "Return Type must always be a struct");

  // Keeps return statements, because we will need to replace them
  std::vector<ReturnInst *> RItoRemove;
  std::vector<InsertValueInst *> IVItoRemove;
  findReturnInst(F, RItoRemove);

  std::vector<Type *> RetArgTypes;
  std::vector<Argument *> RetArgs;
  std::vector<Argument *> Args;

  // Check for { } return struct, which means that the function returns void
  if (FRetTy->isEmptyTy()) {

    LLVM_DEBUG(errs() << "\tFunction output struct is void\n");
    LLVM_DEBUG(errs() << "\tNo parameters added\n");

    // Replacing return statements with others returning void
    for (auto *RI : RItoRemove) {
      ReturnInst::Create((F->getContext()), 0, RI);
      RI->eraseFromParent();
    }
    LLVM_DEBUG(errs() << "\tChanged return statements to return void\n");
  } else {
    // What we need to do instead is find tha return variables, and if they
    // are arguments we add them to the map instead of creating a new return
    // arg for them.

    for (auto RI : RItoRemove) {
      Value *RV = RI->getReturnValue();
      LLVM_DEBUG(errs() << *RV << "\n");
      assert(isa<InsertValueInst>(RV) &&
             "Expecting an InsertValueInst for the return");
      InsertValueInst *IVI = dyn_cast<InsertValueInst>(RV);
      Value *insertedValue = IVI->getInsertedValueOperand();
      if (Argument *insertedArgument = dyn_cast<Argument>(insertedValue)) {
        LLVM_DEBUG(
            errs() << "Returned value is an argument! Treat it differently!\n");
        assert(IVI->getIndices().size() == 1 &&
               "Invalid indeces, expecting only 1.");
        unsigned index = IVI->getIndices()[0];
        returnArgMap[index] = insertedArgument->getArgNo();
        IVItoRemove.push_back(IVI);
      } else {
        Argument *RetArg = new Argument(
            insertedValue->getType()->getPointerTo(), "ret_arg", F);
        RetArgs.push_back(RetArg);
        RetArgTypes.push_back(RetArg->getType());
        LLVM_DEBUG(errs() << "\tCreated parameter: " << *RetArg << "\n");
        ExtractValueInst *EI = ExtractValueInst::Create(
            RV, IVI->getIndices(), RetArg->getName() + ".val", RI);
        new StoreInst(EI, RetArg, RI);
      }
      for (unsigned i = 1; i < FRetTy->getNumElements(); ++i) {
        RV = IVI->getAggregateOperand();
        assert(isa<InsertValueInst>(RV) &&
               "Expecting an InsertValueInst for the return");
        IVI = dyn_cast<InsertValueInst>(RV);
        insertedValue = IVI->getInsertedValueOperand();
        if (Argument *insertedArgument = dyn_cast<Argument>(insertedValue)) {
          LLVM_DEBUG(
              errs()
              << "Returned value is an argument! Treat it differently!\n");
          assert(IVI->getIndices().size() == 1 &&
                 "Invalid indeces, expecting only 1.");
          unsigned index = IVI->getIndices()[0];
          returnArgMap[index] = insertedArgument->getArgNo();
          IVItoRemove.push_back(IVI);
        } else {
          Argument *RetArg = new Argument(
              insertedValue->getType()->getPointerTo(), "ret_arg", F);
          RetArgs.push_back(RetArg);
          RetArgTypes.push_back(RetArg->getType());
          LLVM_DEBUG(errs() << "\tCreated parameter: " << *RetArg << "\n");
          ExtractValueInst *EI = ExtractValueInst::Create(
              RV, IVI->getIndices(), RetArg->getName() + ".val", RI);
          new StoreInst(EI, RetArg, RI);
        }
      }
      ReturnInst::Create((F->getContext()), 0, RI);
      RI->eraseFromParent();
    }

    for (auto IVI : IVItoRemove) {
      LLVM_DEBUG(errs() << "Erasing IVI: " << *IVI << "\n");
      IVI->eraseFromParent();
    }

    for (auto E : returnArgMap) {
      LLVM_DEBUG(errs() << "returnArgMap[" << E.first << "]: " << E.second
                        << " ---> " << *(F->arg_begin() + E.first) << "\n");
    }
  }
  LLVM_DEBUG(errs() << "\tReplaced return statements\n");

  // Create the argument type list with the added argument's type
  std::vector<Type *> ArgTypes;
  for (Function::const_arg_iterator ai = F->arg_begin(), ae = F->arg_end();
       ai != ae; ++ai) {
    ArgTypes.push_back(ai->getType());
  }
  for (auto *RATy : RetArgTypes) {
    ArgTypes.push_back(RATy);
  }

  // Creating Args vector to use in cloning!
  for (Function::arg_iterator ai = F->arg_begin(), ae = F->arg_end(); ai != ae;
       ++ai) {
    Args.push_back(&*ai);
  }
  for (auto *ai : RetArgs) {
    Args.push_back(ai);
  }

  // Adding new arguments to the function argument list, would not change the
  // function type. We need to change the type of this function to reflect the
  // added arguments
  Type *VoidRetType = Type::getVoidTy(F->getContext());
  FunctionType *newFT = FunctionType::get(VoidRetType, ArgTypes, F->isVarArg());

  // Change the function type
  // F->mutateType(PTy);
  Function *newF = cloneFunction(F, newFT, false, NULL, &Args);
  replaceNodeFunctionInIR(*F->getParent(), F, newF);

  return newF;
}

/******************************************************************************
 *                              Helper functions                              *
 ******************************************************************************/

// Calculate execute node parameters which include, number of diemnsions for
// dynamic instances of the kernel, local and global work group sizes.
static void getExecuteNodeParams(Module &M, Value *&workDim, Value *&LocalWGPtr,
                                 Value *&GlobalWGPtr, Kernel *kernel,
                                 ValueToValueMapTy &VMap, Instruction *IB) {

  LLVM_DEBUG(errs() << "In get Execute Node Param!\n");

  // Assign number of dimenstions a constant value
  LLVM_DEBUG(errs() << "kernel->gridDim: " << kernel->gridDim << "\n");
  workDim = ConstantInt::get(Type::getInt32Ty(M.getContext()),
                             kernel->gridDim == 0 ? 1 : kernel->gridDim);
  LLVM_DEBUG(errs() << "workDim: " << *workDim << "\n");

  LLVM_DEBUG(errs() << "kernel->haslocalWG(): " << kernel->hasLocalWG()
                    << "\n");
  // If local work group size if null
  if (!kernel->hasLocalWG()) {
    LocalWGPtr = Constant::getNullValue(Type::getInt64PtrTy(M.getContext()));
    LLVM_DEBUG(errs() << "LocalWGPtr: " << *LocalWGPtr << "\n");
  } else {
    LLVM_DEBUG(errs() << "kernel->localWGSize.size(): "
                      << kernel->localWGSize.size() << "\n");
    for (unsigned i = 0; i < kernel->localWGSize.size(); i++) {
      // Dim sizes don't have to be an Argument anymore.
      // if (isa<Argument>(kernel->localWGSize[i])) {
      LLVM_DEBUG(errs() << "localWGSize[" << i
                        << "]: " << *(kernel->localWGSize[i]) << "\n");
      kernel->localWGSize[i] = VMap[kernel->localWGSize[i]];
      LLVM_DEBUG(errs() << "localWGSize[" << i
                        << "]: " << *(kernel->localWGSize[i]) << "\n");
      //}
    }
    LocalWGPtr =
        genWorkGroupPtr(M, kernel->localWGSize, VMap, IB, "LocalWGSize");
    LLVM_DEBUG(errs() << "localWGPtr: " << *LocalWGPtr << "\n");
  }

  LLVM_DEBUG(errs() << "kernel->globalWGSize.size(): "
                    << kernel->globalWGSize.size() << "\n");
  for (unsigned i = 0; i < kernel->globalWGSize.size(); i++) {
    if (!isa<Constant>(kernel->globalWGSize[i])) {
      LLVM_DEBUG(errs() << "globalWGSize[" << i
                        << "]: " << *(kernel->globalWGSize[i]) << "\n");
      kernel->globalWGSize[i] = VMap[kernel->globalWGSize[i]];
      LLVM_DEBUG(errs() << "globalWGSize[" << i
                        << "]: " << *(kernel->globalWGSize[i]) << "\n");
    }
  }

  // If this was a 0D node, make it 1 D with dim size 1
  if (kernel->globalWGSize.size() == 0) {
    Value *ConstantOne = ConstantInt::get(Type::getInt32Ty(M.getContext()), 1);
    kernel->globalWGSize.push_back(ConstantOne);
  }

  // For OpenCL, global work group size is the total bumber of instances in each
  // dimension. So, multiply local and global dim limits.
  std::vector<Value *> globalWGSizeInsts;
  if (kernel->hasLocalWG()) {
    for (unsigned i = 0; i < kernel->gridDim; i++) {
      BinaryOperator *MulInst =
          BinaryOperator::Create(Instruction::Mul, kernel->globalWGSize[i],
                                 kernel->localWGSize[i], "", IB);
      globalWGSizeInsts.push_back(MulInst);
    }
  } else {
    globalWGSizeInsts = kernel->globalWGSize;
  }
  GlobalWGPtr = genWorkGroupPtr(M, globalWGSizeInsts, VMap, IB, "GlobalWGSize");
  LLVM_DEBUG(errs() << "Pointer to global work group: " << *GlobalWGPtr
                    << "\n");
}

// CodeGen for allocating space for Work Group on stack and returning a pointer
// to its address
static Value *genWorkGroupPtr(Module &M, std::vector<Value *> WGSize,
                              ValueToValueMapTy &VMap, Instruction *IB,
                              const Twine &WGName) {
  Value *WGPtr;
  // Get int64_t and or ease of use
  Type *Int64Ty = Type::getInt64Ty(M.getContext());

  // Work Group type is [#dim x i64]
  Type *WGTy = ArrayType::get(Int64Ty, WGSize.size());
  // Allocate space of Global work group data on stack and get pointer to
  // first element.
  AllocaInst *WG = new AllocaInst(WGTy, 0, WGName, IB);
  WGPtr = BitCastInst::CreatePointerCast(WG, Int64Ty->getPointerTo(),
                                         WG->getName() + ".0", IB);
  Value *nextDim = WGPtr;
  LLVM_DEBUG(errs() << *WGPtr << "\n");

  // Iterate over the number of dimensions and store the global work group
  // size in that dimension
  for (unsigned i = 0; i < WGSize.size(); i++) {
    assert(WGSize[i]->getType()->isIntegerTy() &&
           "Dimension not an integer type!");

    if (WGSize[i]->getType() != Int64Ty) {
      // If number of dimensions are mentioned in any other integer format,
      // generate code to extend it to i64. We need to use the mapped value in
      // the new generated function, hence the use of VMap
      // FIXME: Why are we changing the kernel WGSize vector here?
      LLVM_DEBUG(errs() << "Not i64. Zero extend required.\n");
      LLVM_DEBUG(errs() << *WGSize[i] << "\n");
      CastInst *CI =
          BitCastInst::CreateIntegerCast(WGSize[i], Int64Ty, true, "", IB);
      LLVM_DEBUG(errs() << "Bitcast done.\n");
      StoreInst *SI = new StoreInst(CI, nextDim, IB);
      LLVM_DEBUG(errs() << "Zero extend done.\n");
      LLVM_DEBUG(errs() << "\tZero extended work group size: " << *SI << "\n");
    } else {
      // Store the value representing work group size in ith dimension on
      // stack
      StoreInst *SI = new StoreInst(WGSize[i], nextDim, IB);

      LLVM_DEBUG(errs() << "\t Work group size: " << *SI << "\n");
    }
    if (i + 1 < WGSize.size()) {
      // Move to next dimension
      GetElementPtrInst *GEP = GetElementPtrInst::Create(
          nullptr, nextDim, ArrayRef<Value *>(ConstantInt::get(Int64Ty, 1)),
          WG->getName() + "." + Twine(i + 1), IB);
      LLVM_DEBUG(errs() << "\tPointer to next dimension on stack: " << *GEP
                        << "\n");
      nextDim = GEP;
    }
  }
  return WGPtr;
}

// Get generated FPGA binary name
std::string CGT_FPGA::getFPGAFilename(const Module &M) {
  if (BitstreamFileName != "")
    return BitstreamFileName;
  std::string mid = M.getModuleIdentifier();
  if (mid.find(".ll") != std::string::npos) {
    mid = mid.substr(0, mid.find(".ll"));
  } else if (mid.find(".bc") != std::string::npos) {
    mid = mid.substr(0, mid.find(".bc"));
  }
  return mid.append(".kernels.aocx");
}

// Get the name of the input file from module ID
static std::string getFilenameFromModule(const Module &M) {
  std::string moduleID = M.getModuleIdentifier();
  return moduleID.substr(moduleID.find_last_of("/") + 1);
}

// Changes the data layout of the Module to be compiled with FPGA backend
// TODO: Figure out when to call it, probably after duplicating the modules
static void changeDataLayout(Module &M) {
  std::string fpga64_layoutStr =
      "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:"
      "64:64-v16:16:16-v24:32:32-v32:32:32-v48:64:64-v64:64:64-v96:128:128-"
      "v128:128:128-v192:256:256-v256:256:256-v512:512:512-v1024:1024:1024";

  M.setDataLayout(StringRef(fpga64_layoutStr));
  return;
}

static void changeTargetTriple(Module &M) {
  std::string fpga64_TargetTriple = "unknown-unknown-unknown";
  M.setTargetTriple(StringRef(fpga64_TargetTriple));
}

// Helper function, generate a string representation of a type
static std::string printType(Type *ty) {
  std::string type_str;
  raw_string_ostream rso(type_str);
  ty->print(rso);
  return rso.str();
}

// Helper funciton to demangle function name for kernel.
// static StringRef demangle(const char* name)
//{
//        int status = -1;
//
//        std::unique_ptr<char, void(*)(void*)> res { abi::__cxa_demangle(name,
//        NULL, NULL, &status), std::free }; return (status == 0) ?
//        StringRef(res.get()) : StringRef(std::string(name));
//}

// Helper function to get mangled names of OpenCL built ins
static StringRef getMangledName(std::string name) {
  Twine mangledName = "_Z" + Twine(name.size()) + name + "j";
  return StringRef(mangledName.str());
}

// Helper function, populate a vector with all return statements in a function
static void findReturnInst(Function *F,
                           std::vector<ReturnInst *> &ReturnInstVec) {
  for (inst_iterator i = inst_begin(F), e = inst_end(F); i != e; ++i) {
    Instruction *I = &(*i);
    ReturnInst *RI = dyn_cast<ReturnInst>(I);
    if (RI) {
      ReturnInstVec.push_back(RI);
    }
  }
}

// Helper function, populate a vector with all IntrinsicID intrinsics in a
// function
static void findIntrinsicInst(Function *F, Intrinsic::ID IntrinsicID,
                              std::vector<IntrinsicInst *> &IntrinsicInstVec) {
  for (inst_iterator i = inst_begin(F), e = inst_end(F); i != e; ++i) {
    Instruction *I = &(*i);
    IntrinsicInst *II = dyn_cast<IntrinsicInst>(I);
    if (II && II->getIntrinsicID() == IntrinsicID) {
      IntrinsicInstVec.push_back(II);
    }
  }
}

// Helper function to get mangled names of OpenCL built ins for atomics
static StringRef getAtomicMangledName(std::string name, unsigned addrspace,
                                      bool sign) {
  Twine mangledName =
      "_Z" + Twine(name.size()) + name + "PU3AS" + Twine(addrspace) + "jj";
  //                      ((sign) ? "ii" : "jj");
  return StringRef(mangledName.str());
}

// Helper funtion, returns the OpenCL function name corresponding to atomic op
static StringRef getAtomicOpName(Intrinsic::ID ID, unsigned addrspace) {
  switch (ID) {
  case Intrinsic::hpvm_atomic_add:
    return getAtomicMangledName("atom_add", addrspace, true);
  case Intrinsic::hpvm_atomic_sub:
    return getAtomicMangledName("atom_sub", addrspace, true);
  case Intrinsic::hpvm_atomic_min:
    return getAtomicMangledName("atom_min", addrspace, true);
  case Intrinsic::hpvm_atomic_max:
    return getAtomicMangledName("atom_max", addrspace, true);
  case Intrinsic::hpvm_atomic_xchg:
    return getAtomicMangledName("atom_xchg", addrspace, true);
  case Intrinsic::hpvm_atomic_and:
    return getAtomicMangledName("atom_and", addrspace, true);
  case Intrinsic::hpvm_atomic_or:
    return getAtomicMangledName("atom_or", addrspace, true);
  case Intrinsic::hpvm_atomic_xor:
    return getAtomicMangledName("atom_xor", addrspace, true);
  default:
    llvm_unreachable("Unsupported atomic intrinsic!");
  };
}

static std::string getMathFunctionName(Intrinsic::ID ID) {
  switch (ID) {
  case Intrinsic::sin:
    return "sin";
  case Intrinsic::cos:
    return "cos";
  case Intrinsic::sqrt:
    return "sqrt";
  case Intrinsic::floor:
    return "floor";
  default:
    llvm_unreachable("Unsupported math function!");
  };
}

} // namespace dfg2llvm

char DFG2LLVM_FPGA::ID = 0;
static RegisterPass<DFG2LLVM_FPGA> X("dfg2llvm-fpga",
		"Dataflow Graph to LLVM for FPGA Pass",
		false /* does not modify the CFG */,
		true /* transformation,   *
					* not just analysis */);
