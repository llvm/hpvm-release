#include "CoreHPVM/DFGraph.h"
#define DEBUG_TYPE "dfgraph"

namespace llvm {

class DFNode;
class DFInternalNode;
class DFLeafNode;
class DFEdge;
class DFNodeVisitor;
class DFTreeTraversal;
class DFEdgeVisitor;
class DFGraph;

//===--------------------- DFGraph Outlined Functions --------------===//
DFGraph::DFGraph(DFInternalNode *P) {
  Parent = P;
  // Create dummy entry and exit nodes and add them to the graph
  Entry =
      DFLeafNode::Create(NULL, Parent->getFuncPointer(), hpvm::None, Parent);
  Exit = DFLeafNode::Create(NULL, Parent->getFuncPointer(), hpvm::None, Parent);
  addChildDFNode(Entry);
  addChildDFNode(Exit);
}

void DFGraph::sortChildren() { std::sort(begin(), end(), DFGraph::compareRank); }

bool DFGraph::compareRank(DFNode *A, DFNode *B) {
  // Force exit node to the end
  if (A->isExitNode()) return false;
  if (B->isExitNode()) return true;
  return A->getRank() < B->getRank();
}

bool DFGraph::isStreaming() {
  for (auto E : DFEdgeList) {
    if (E->isStreamingEdge())
      return true;
  }
  return false;
}

bool DFGraph::removeEdge(const DFEdge *E) {
  dfedge_iterator position = std::find(dfedge_begin(), dfedge_end(), E);
  if (position != dfedge_end()) // the edge was found
    DFEdgeList.erase(position);

  DFNode *S = E->getSourceDF();
  DFNode *D = E->getDestDF();
  S->removeSuccessor(D);
  S->removeOutDFEdge(E);
  D->removeInDFEdge(E);

  if (position == dfedge_end())
    DEBUG_WITH_TYPE(
        "dfgraph",
        errs() << "DFGraph::removeEdge called with non-existent edge.\n");
  return position != dfedge_end();
}

//===--------------------- DFNode Outlined Functions --------------===//
DFNode::DFNode(IntrinsicInst *_II, Function *_FuncPointer, hpvm::Target _Hint,
               DFInternalNode *_Parent, unsigned _NumOfDim,
               std::vector<Value *> _DimLimits, DFNodeKind _K)
    : II(_II), FuncPointer(_FuncPointer), Parent(_Parent), NumOfDim(_NumOfDim),
      DimLimits(_DimLimits), Kind(_K) {

  Type *Ty = FuncPointer->getFunctionType()->getReturnType();

  // Allow the return type to be void too, in the hHPVM IR. If return type is
  // void, create an empty struct type and keep that as the return type of the
  // node.
  if (Ty->isVoidTy())
    Ty = StructType::get(Ty->getContext(), true);

  // All nodes output type must always be a struct type.
  assert(isa<StructType>(Ty) && "Invalid return type of a dataflow node");

  // Check that the number of dimensions is correct
  assert(NumOfDim <= 3 && "Invalid num of dimensions for dataflow node!");

  // Check that the number of dimensions is correct
  assert(DimLimits.size() == NumOfDim &&
         "Incompatible num of dimensions and dimension limits for DFNode!");

  OutputType = cast<StructType>(Ty);
  Level = (_Parent) ? _Parent->getLevel() + 1 : 0;
  Rank = 0;

  Tag = hpvm::None;
  GenFuncs.CPUGenFunc = NULL;
  GenFuncs.GPUGenFunc = NULL;
  GenFuncs.FPGAGenFunc = NULL;
  GenFuncs.SPIRGenFunc = NULL;
  GenFuncs.CUDNNGenFunc = NULL;
  GenFuncs.PROMISEGenFunc = NULL;

  GenFuncInfo.cpu_hasCPUFunc = false;
  GenFuncInfo.gpu_hasCPUFunc = false;
  GenFuncInfo.cudnn_hasCPUFunc = false;
  GenFuncInfo.promise_hasCPUFunc = false;
  GenFuncInfo.fpga_hasCPUFunc = false;
  GenFuncInfo.spir_hasCPUFunc = false;
  Root = false;
  Hint = _Hint;
}

void DFNode::addToNodeGraph(DFInternalNode *P) {
  Parent = P;
  P->addChildToDFGraph(this);
}

void DFNode::setRank(unsigned r) {
  Rank = r;
  // Update rank of successors
  for (outdfedge_iterator i = outdfedge_begin(), e = outdfedge_end(); i != e;
       ++i) {
    DFEdge *E = *i;
    DFNode *D = E->getDestDF();
    if (D->getRank() <= r)
      D->setRank(r + 1);
  }
}

bool DFNode::isEntryNode() const {
  if (Parent == NULL)
    return false;
  return Parent->getChildGraph()->isEntry(this);
}

bool DFNode::isExitNode() const {
  if (Parent == NULL)
    return false;
  return Parent->getChildGraph()->isExit(this);
}

DFEdge *DFNode::getInDFEdgeAt(unsigned inPort) {

  // If it is not a dummy node, then check if inPort should be less than the
  // number of arguments in the associated function.
  assert((isDummyNode() ||
          inPort < FuncPointer->getFunctionType()->getNumParams()) &&
         "Invalid input port request!");

  for (indfedge_iterator i = indfedge_begin(), e = indfedge_end(); i != e;
       ++i) {
    DFEdge *E = *i;
    if (inPort == E->getDestPosition())
      return E;
  }
  return NULL;
}

DFEdge *DFNode::getExtendedInDFEdgeAt(unsigned inPort) {
  DFEdge *Ein = getInDFEdgeAt(inPort);
  DFNode *sn = Ein->getSourceDF();
  if (!sn->isEntryNode())
    return Ein;

  DFNode *pn = getParent();
  if (pn->isRoot())
    return Ein;

  DFEdge *PEin = pn->getInDFEdgeAt(inPort);
  DFInternalNode *SPN = dyn_cast<DFInternalNode>(PEin->getSourceDF());
  if (!SPN)
    return PEin;

  unsigned outPort = PEin->getSourcePosition();
  return SPN->getChildGraph()->getExit()->getInDFEdgeAt(outPort);
}

DFEdge *DFNode::getOutDFEdgeAt(unsigned outPort) {

  // Cannot perform check for the number of outputs here,
  // it depends on the node's return type

  unsigned index = 0;
  for (outdfedge_iterator i = outdfedge_begin(), e = outdfedge_end(); i != e;
       ++i) {
    DFEdge *E = *i;
    if (outPort == index) // E->getSourcePosition())
      return E;
    index++;
  }
  return NULL;
}

DFEdge *DFNode::getExtendedOutDFEdgeAt(unsigned outPort) {
  DFEdge *Eout = getOutDFEdgeAt(outPort);
  if (!Eout->getDestDF()->isExitNode())
    return Eout;

  DFNode *pn = getParent();
  if (pn->isRoot())
    return Eout;

  DFEdge *PEout = pn->getOutDFEdgeAt(outPort);
  DFInternalNode *DPN = dyn_cast<DFInternalNode>(PEout->getDestDF());
  if (!DPN)
    return PEout;

  unsigned inPort = PEout->getDestPosition();
  return DPN->getChildGraph()->getEntry()->getOutDFEdgeAt(inPort);
}

// Ignore Allocation Nodes
std::map<unsigned, unsigned> DFNode::getInArgMap() {
  std::map<unsigned, unsigned> map;
  for (unsigned i = 0; i < InDFEdges.size(); i++) {
    DFEdge *E = getInDFEdgeAt(i);
    if (E->getSourceDF()->isAllocationNode())
      continue;
    unsigned pos = E->getSourcePosition();
    map[i] = pos;
  }
  return map;
}

// Only Allocation Nodes - only detect relevant indices
std::map<unsigned, std::pair<Value *, unsigned>> DFNode::getSharedInArgMap() {
  std::map<unsigned, std::pair<Value *, unsigned>> map;
  for (unsigned i = 0; i < InDFEdges.size(); i++) {
    DFEdge *E = getInDFEdgeAt(i);
    if (!E->getSourceDF()->isAllocationNode())
      continue;
    map[i] = std::pair<Value *, unsigned>(NULL, 0);
  }
  return map;
}

// Broken, needs to be redone to be used again
std::vector<unsigned> DFNode::getOutArgMap() {
  return std::vector<unsigned>();

  std::vector<unsigned> map(100);
  for (unsigned i = 0; i < OutDFEdges.size(); i++) {
    DFEdge *E = getOutDFEdgeAt(i);
    unsigned pos = E->getDestPosition();
    map[pos] = i;
  }
  return map;
}

int DFNode::getAncestorHops(DFNode *N) {
  DFNode *temp = this;
  int hops = 0;
  while (temp != NULL) {
    if (temp == N)
      return hops;
    temp = temp->getParent();
    hops++;
  }
  // N not found among the ancestors
  // Return -1 to indicate that N is not an ancestor.
  return -1;
}

// Returns true if the node does not have any side effects
// This shall mostly be a work in progress as the conditions for determining "no
// side effects" property precisely can be large. List the checks here
// Check #1: No incoming pointer argument
// TODO: More precidse can be that no incoming edge is a pointer with out
// attribute
bool DFNode::hasSideEffects() {
  bool hasSideEffects = false;
  // Check #1: No incoming pointer argument
  for (DFEdge *E : this->InDFEdges) {
    hasSideEffects |= E->getType()->isPointerTy();
  }
  return hasSideEffects;
}

void DFNode::addGenFunc(Function *F, hpvm::Target T, bool isCPUFunc) {

    switch (T) {
    case hpvm::CPU_TARGET:
      if (GenFuncs.CPUGenFunc != NULL) {
        LLVM_DEBUG(errs() << "Warning: Second generated CPU function for node "
                     << FuncPointer->getName() << "\n");
      }
      GenFuncs.CPUGenFunc = F;
      GenFuncInfo.cpu_hasCPUFunc = isCPUFunc;
      break;
    case hpvm::GPU_TARGET:
      if (GenFuncs.GPUGenFunc != NULL) {
        LLVM_DEBUG(errs() << "Warning: Second generated GPU function for node "
                     << FuncPointer->getName() << "\n");
      }
      GenFuncs.GPUGenFunc = F;
      GenFuncInfo.gpu_hasCPUFunc = isCPUFunc;
      break;
    case hpvm::CUDNN_TARGET:
      if (GenFuncs.CUDNNGenFunc != NULL) {
        LLVM_DEBUG(errs() << "Warning: Second generated GPU function for node "
                     << FuncPointer->getName() << "\n");
      }
      GenFuncs.CUDNNGenFunc = F;
      GenFuncInfo.cudnn_hasCPUFunc = isCPUFunc;
      break;
    case hpvm::TENSOR_TARGET:
      if (GenFuncs.PROMISEGenFunc != NULL) {
        LLVM_DEBUG(errs() << "Warning: Second generated PROMISE function for node "
                     << FuncPointer->getName() << "\n");
      }
      GenFuncs.PROMISEGenFunc = F;
      GenFuncInfo.promise_hasCPUFunc = isCPUFunc;
      break;
    case hpvm::FPGA_TARGET:
      if (GenFuncs.FPGAGenFunc != NULL) {
        LLVM_DEBUG(errs() << "Warning: Second generated FPGA function for node "
                     << FuncPointer->getName() << "\n");
      }
      GenFuncs.FPGAGenFunc = F;
      GenFuncInfo.fpga_hasCPUFunc = isCPUFunc;
      break;
    case hpvm::CPU_OR_GPU_TARGET:
      assert(false && "A node function should be set with a tag specifying its \
                type, not the node hint itself\n");
    default:
      assert(false && "Unknown target for generated function\n");
    }

    Tag = hpvmUtils::getUpdatedTag(Tag, T);
  }

  bool DFNode::checkForwardPath(DFNode *N) {
    assert(!isRoot() && !N->isRoot() &&
           "This function cannot be called on Root nodes!\n");
    assert(getParent() == N->getParent() &&
           "This function can only be called on two nodes that have the same "
           "parent!\n");
    LLVM_DEBUG(errs() << "\t\tcheckForwardPath() ? " << getFuncPointer()->getName()
                 << " -> " << N->getFuncPointer()->getName() << "\n");
    for (auto si = successors_begin(); si != successors_end(); ++si) {
      DFNode *NI = *si;
      LLVM_DEBUG(errs() << "\t\t\t" << NI->getFuncPointer()->getName() << "\n");
      if (NI == N) {
        return true;
      } else if (NI->isDummyNode()) {
        continue;
      } else {
        if (NI->checkForwardPath(N))
          return true;
      }
    }
    return false;
  }

//===--------------------- DFInternalNode Outlined Functions --------------===//
void DFInternalNode::addEdgeToDFGraph(DFEdge *E) {
  // errs() << "----- ADD EDGE TO DFGRAPH\n";
  DFNode *S = E->getSourceDF();
  DFNode *D = E->getDestDF();
  // errs() << "INIT SOURCE NODE: " << S << "\n";
  // errs() << "INIT DEST NODE: " << D << "\n";

  assert(std::find(childGraph->begin(), childGraph->end(), S) !=
             childGraph->end() &&
         "Source node not found in child dataflow graph!");

  assert(std::find(childGraph->begin(), childGraph->end(), D) !=
             childGraph->end() &&
         "Destination node not found in child dataflow graph!");

  // Update Graph
  childGraph->addDFEdge(E);

  // Update source and destination nodes
  S->addSuccessor(D);
  S->addOutDFEdge(E);
  D->addInDFEdge(E);

  // errs() << "SET SOURCE NODE: " << E->getSourceDF() << "\n";
  // errs() << "SET DEST NODE: " << E->getDestDF() << "\n";

  // Update Rank
  if (D->getRank() <= S->getRank())
    D->setRank(S->getRank() + 1);
}
void DFInternalNode::dump(unsigned indent, bool recursive, bool verbose) {
  errs() << std::string(indent, ' ') << "Dumping Node: " << getName() << "\n"
         << std::string(indent, ' ') << "- Kind: Internal Node"
         << "\n"
         << std::string(indent, ' ') << "- Dims: " << getNumOfDim() << "\n";
  for (auto Val : getDimLimits()) {
    errs() << std::string(indent, ' ') << " - " << *Val << "\n";
  }
  errs() << std::string(indent, ' ') << "- Incoming Edges:\n";
  for (auto E = indfedge_begin(); E != indfedge_end(); E++) {
    DFEdge *Edge = *E;
    errs() << std::string(indent, ' ') << " - ";
    if (verbose)
      Edge->dump();
    else
      errs() << Edge->getSourceDF()->getName() << " ---> "
             << Edge->getDestDF()->getName() << "\n";
  }
  errs() << std::string(indent, ' ') << "- Outgoing Edges:\n";
  for (auto E = outdfedge_begin(); E != outdfedge_end(); E++) {
    DFEdge *Edge = *E;
    errs() << std::string(indent, ' ') << " - ";
    if (verbose)
      Edge->dump();
    else
      errs() << Edge->getSourceDF()->getName() << " ---> "
             << Edge->getDestDF()->getName() << "\n";
  }
  DFGraph *ChildGraph = getChildGraph();
  ChildGraph->sortChildren();
  errs() << std::string(indent, ' ') << "- Child Nodes:\n";
  for (auto ci = ChildGraph->begin(), ce = ChildGraph->end(); ci != ce; ++ci) {
    DFNode *C = *ci;
    errs() << std::string(indent, ' ') << " - " << C->getName() << "\n";
    if (recursive)
      C->dump(indent + 2, recursive, verbose);
  }
}

void DFLeafNode::dump(unsigned indent, bool recursive, bool verbose) {
  errs() << std::string(indent, ' ') << "Dumping Node: " << getName() << "\n"
         << std::string(indent, ' ') << "- Kind: Leaf Node"
         << "\n"
         << std::string(indent, ' ') << "- isDummy: "
         << (isDummyNode() ? (isEntryNode() ? "Yes --> Entry" : "Yes --> Exit")
                           : "No")
         << "\n"
         << std::string(indent, ' ')
         << "- isAllocation: " << (isAllocationNode() ? "Yes" : "No") << "\n"
         << std::string(indent, ' ') << "- Dims: " << getNumOfDim() << "\n";
  for (auto Val : getDimLimits()) {
    errs() << std::string(indent, ' ') << " - " << *Val << "\n";
  }
  errs() << std::string(indent, ' ') << "- Incoming Edges:\n";
  for (auto E = indfedge_begin(); E != indfedge_end(); E++) {
    DFEdge *Edge = *E;
    errs() << std::string(indent, ' ') << " - ";
    if (verbose)
      Edge->dump();
    else
      errs() << Edge->getSourceDF()->getName() << " ---> "
             << Edge->getDestDF()->getName() << "\n";
  }
  errs() << std::string(indent, ' ') << "- Outgoing Edges:\n";
  for (auto E = outdfedge_begin(); E != outdfedge_end(); E++) {
    DFEdge *Edge = *E;
    errs() << std::string(indent, ' ') << " - ";
    if (verbose)
      Edge->dump();
    else
      errs() << Edge->getSourceDF()->getName() << " ---> "
             << Edge->getDestDF()->getName() << "\n";
  }
}

//===-------------------------- Visitor Classes ---------------------------===//

void DFInternalNode::applyDFNodeVisitor(DFNodeVisitor &V) { V.visit(this); }

void DFLeafNode::applyDFNodeVisitor(DFNodeVisitor &V) { V.visit(this); }

void DFTreeTraversal::visit(DFInternalNode *N) {
    LLVM_DEBUG(errs() << "Visited Node (I) - " << N->getFuncPointer()->getName()
                 << "\n");
    for (DFGraph::children_iterator i = N->getChildGraph()->begin(),
                                    e = N->getChildGraph()->end();
         i != e; ++i) {
      DFNode *child = *i;
      child->applyDFNodeVisitor(*this);
    }
}

void DFTreeTraversal::visit(DFLeafNode *N) {
    LLVM_DEBUG(errs() << "Visited Node (L) - " << N->getFuncPointer()->getName()
                 << "\n");
}

 void FollowSuccessors::visit(DFInternalNode *N) {
    /*DFNodeListType L; // Empty List that will contain the sorted elements
    DFNodeListType S; // Set of all nodes with no incoming edges

    // Add Entry node to S.
    S.push_back(N->getChildGraph()->getEntry());

    while(!S.empty()) {
      DFNode* X = S.pop_back();
      for (DFEdge* E: in X->getOutDFEdges()) {
        DFNode* dest = E->getDestDF();
        if
      }
    }*/
    LLVM_DEBUG(errs() << "Visited Node (I) - " << N->getFuncPointer()->getName()
                 << "\n");
    for (DFInternalNode::successor_iterator i = N->successors_begin(),
                                            e = N->successors_end();
         i != e; ++i) {
      /* Traverse the graph.
       * Choose the kind of traversal we want
       * Do we do a DAG kind of traversal?
       */
    }
  }

 void FollowSuccessors::visit(DFLeafNode *N) {
    LLVM_DEBUG(errs() << "Visited Node (L) - " << N->getFuncPointer()->getName()
                 << "\n");
  }

 void ReplaceNodeFunction::visit(DFInternalNode *N) {
    LLVM_DEBUG(errs() << "Start: Replace Node Function for Node (I) - "
                 << N->getFuncPointer()->getName() << "\n");

    // Follows a bottom-up approach.
    for (DFGraph::children_iterator i = N->getChildGraph()->begin(),
                                    e = N->getChildGraph()->end();
         i != e; ++i) {
      DFNode *child = *i;
      child->applyDFNodeVisitor(*this);
    }
    // Generate code for this internal node now. This way all the cloned
    // functions for children exist.
    replaceNodeFunction(N);
    LLVM_DEBUG(errs() << "DONE: Replace Node Function for Node (I) - "
                 << N->getFuncPointer()->getName() << "\n");
  }

 void ReplaceNodeFunction::visit(DFLeafNode *N) {
    LLVM_DEBUG(errs() << "Start: Replace Node Function for Node (L) - "
                 << N->getFuncPointer()->getName() << "\n");
    replaceNodeFunction(N);
    LLVM_DEBUG(errs() << "DONE: Replace Node Function for Node (L) - "
                 << N->getFuncPointer()->getName() << "\n");
  }

}