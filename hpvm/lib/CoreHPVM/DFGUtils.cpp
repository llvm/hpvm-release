#include "CoreHPVM/DFGUtils.h"
#include "CoreHPVM/HPVMUtils.h"
#include "CoreHPVM/DFGraph.h"
#include "CoreHPVM/DFG2LLVM.h"
#define DEBUG_TYPE "dfgutils"
using namespace llvm;

namespace hpvmUtils {

// Deallocates edges
void removeNodeEdges(DFNode &Node) {
  DFGraph &Graph = *Node.getParent()->getChildGraph();

  // Copy into vector since deleting edges will modify the edge list
  std::vector<DFEdge *> DeleteThese;

  // In edges
  {
    for (auto It = Node.indfedge_begin(), End = Node.indfedge_end(); It != End;
         ++It)
      DeleteThese.push_back(*It);
  }

  // Out edges
  {
    for (auto It = Node.outdfedge_begin(), End = Node.outdfedge_end();
         It != End; ++It) {
      DeleteThese.push_back(*It);
    }
  }

  for (const DFEdge *Edge : DeleteThese)
    Graph.removeEdge(Edge);
}

void bindInput(DFInternalNode *OuterNode, DFNode *InnerNode,
                      unsigned SourcePos, unsigned DestPos, Type *InType,
                      bool IsStreaming) {
  DFEdge *Bind =
      DFEdge::Create(OuterNode->getChildGraph()->getEntry(), InnerNode, false,
                     SourcePos, DestPos, InType, IsStreaming);

  OuterNode->addEdgeToDFGraph(Bind);
}

void bindOutput(DFInternalNode *OuterNode, DFNode *InnerNode,
                       unsigned SourcePos, unsigned DestPos, Type *OutType,
                       bool IsStreaming) {
  DFEdge *Bind =
      DFEdge::Create(InnerNode, OuterNode->getChildGraph()->getExit(), false,
                     SourcePos, DestPos, OutType, IsStreaming);

  OuterNode->addEdgeToDFGraph(Bind);
}

DFInternalNode &makeSkeleton(DFNode &Node,
                                    const Twine &SkeletonName) {

  DFInternalNode &Parent = *Node.getParent();
  // Create dummy function to provide type
  Function *OldFun = Node.getFuncPointer();
  Function *NewFun = hpvmUtils::makeEmptyFunction(
      OldFun->getFunctionType(), OldFun->getLinkage(), OldFun->getParent(),
      SkeletonName);

  LLVM_DEBUG(errs() << "Making dummy function:\n" << *NewFun << "\n");

  // Add target hint to function
  hpvmUtils::addHint(NewFun, hpvmUtils::getPreferredTarget(OldFun));

  DFInternalNode *Skeleton = DFInternalNode::Create(
      Node.getInstruction(), NewFun, Node.getTargetHint(), Node.getParent(),
      Node.getNumOfDim(), Node.getDimLimits());

  Parent.addChildToDFGraph(Skeleton);

  // Copy in-edges
  {
    for (auto It = Node.indfedge_begin(), End = Node.indfedge_end(); It != End;
         ++It) {
      const DFEdge &OldEdge = **It;
      DFEdge *NewEdge =
          DFEdge::Create(OldEdge.getSourceDF(), Skeleton, OldEdge.getEdgeType(),
                         OldEdge.getSourcePosition(), OldEdge.getDestPosition(),
                         OldEdge.getType(), OldEdge.isStreamingEdge());

      Parent.addEdgeToDFGraph(NewEdge);
    }
  }

  // Copy out-edges
  {
    for (auto It = Node.outdfedge_begin(), End = Node.outdfedge_end();
         It != End; ++It) {
      const DFEdge &OldEdge = **It;
      DFEdge *NewEdge =
          DFEdge::Create(Skeleton, OldEdge.getDestDF(), OldEdge.getEdgeType(),
                         OldEdge.getSourcePosition(), OldEdge.getDestPosition(),
                         OldEdge.getType(), OldEdge.isStreamingEdge());

      Parent.addEdgeToDFGraph(NewEdge);
    }
  }

  return *Skeleton;
}

// Returns the outer node
// Does not set dim limits
DFInternalNode &nestNode(DFLeafNode *InnerNode,
                                const Twine &NesterName) {
  // Root of DFG produced in BuildDFG is an internal node, so leaf nodes should
  // have parents
  assert(InnerNode->getParent() != nullptr &&
         "Leaf node to nest must have a parent");

  DFInternalNode &Parent = *InnerNode->getParent();
  DFInternalNode *OuterNode = &makeSkeleton(*InnerNode, NesterName);
  hpvmUtils::removeNodeEdges(*InnerNode);

  // Reparent to new outer node
  Parent.removeChildFromDFGraph(InnerNode);
  InnerNode->addToNodeGraph(OuterNode);

  // Bind edges
  const FunctionType *FType = InnerNode->getFuncPointer()->getFunctionType();

  const StructType *OutType = InnerNode->getOutputType();
  const unsigned InCount = FType->getNumParams();
  const unsigned OutCount = OutType->getNumElements();

  // TODO: Assume not streaming for now, will eventually need to determine this
  // based on existing edges

  // Input binds
  for (unsigned I = 0; I < InCount; ++I)
    bindInput(OuterNode, InnerNode, I, I, FType->getParamType(I), false);

  // Output binds
  for (unsigned I = 0; I < OutCount; ++I)
    bindOutput(OuterNode, InnerNode, I, I, OutType->getElementType(I), false);

  return *OuterNode;
}

unsigned getArgCount(Function *F) {
  return std::distance(F->arg_begin(), F->arg_end());
}

// Threads an individual value from a node's input to a new input in a child
// node
// Returns the value in the child node's function
// Invalidates function pointer of InnerNode
Argument *threadInput(DFNode &InnerNode, Argument &Input,
                                   const Twine &ArgName) {
  using namespace llvm;

  DFInternalNode &OuterNode = *InnerNode.getParent();
  assert(OuterNode.getFuncPointer() == Input.getParent());

  // Create new function with new arg and set the inner node to have it
  Function *InnerF = InnerNode.getFuncPointer();
  Function *NewInnerF = dfg2llvm::addArgument(InnerF, Input.getType(), ArgName);
  InnerNode.setFuncPointer(NewInnerF);

  const unsigned NewArgNo = getArgCount(InnerF);

  clearFunction(InnerF);

  // Create the bind to thread the argument to the new input
  // TODO: When should this be streaming?
  bindInput(&OuterNode, &InnerNode, Input.getArgNo(), NewArgNo, Input.getType(),
            false);

  return NewInnerF->arg_begin() + NewArgNo;
}

// Like threadInput, but works on either constants or arguments. Useful to
// not care about specifics.
Value *threadValue(DFNode &InnerNode, Value *V,
                                const Twine &ValName) {
  using namespace llvm;
  if (Argument *A = dyn_cast<Argument>(V))
    return threadInput(InnerNode, *A, ValName);
  assert(isa<Constant>(V) && "Can only thread constants or arguments");
  return V;
}

// Inline all the functions called by a leaf node
void inlineFunctions(DFLeafNode *Leaf) {
    Function *F = Leaf->getFuncPointer();
    inlineFunctions(F);
//   std::vector<CallInst *> CallsToInline;
//   getCallsToInline(Leaf->getFuncPointer(), CallsToInline);
//   for (auto *CI : CallsToInline) {
//     Function *Callee = CI->getFunction();
//     InlineFunctionInfo IFI;
//     InlineFunction(*CI, IFI);
//   }
}

// Inline all the functions called by multiple leaf nodes
void inlineFunctions(std::vector<DFLeafNode *> Leaves) {
  for (auto *LN : Leaves) {
    inlineFunctions(LN);
  }
}

void viewDFGraph(DFGraph *G) {
  DFInternalNode *Parent = G->getParent();
  assert(Parent && "Child Graph with no parent\n");
  WriteGraph(G,
                   (Parent->isRoot() ? "Root_Dataflow_Graph"
                                     : (Parent->getFuncPointer()->getName())));
}

// Check if there exists a forward path in the graph between this node
// and node N
// TODO: Add a visitedNodes set to avoid revisiting the same node twice
bool existsForwardPath(DFNode *SN, DFNode *DN) {
  // If Src and Dst are not at the same level in the graph, we first
  // need to find the one with the smaller level (higher in the graph heirarchy)
  // and then traverse the parents of the other node until we reach the
  // one which has the same level. Once we have a Src/Dst that are at the same
  // level if they have the same parent, we call checkForwardPath() to see if
  // there exists a path between them, otherwise we recursively call this
  // function on the direct parents of the two nodes. If we get to the points
  // where the two nodes are Roots, we assume that they have a path between.
  // TODO: Add ability to check if two DFGs can be run in parallel.
  auto SrcLvl = SN->getLevel();
  auto DstLvl = DN->getLevel();
  DFNode *Src = SN;
  DFNode *Dst = DN;
  LLVM_DEBUG(errs() << "\texitForwardPath() ? " << Src->getFuncPointer()->getName()
               << " -> " << Dst->getFuncPointer()->getName() << "\n");
  if (SrcLvl < DstLvl) {
    do {
      Dst = cast<DFNode>(Dst->getParent());
    } while (Dst->getLevel() > SrcLvl);
  } else if (DstLvl < SrcLvl) {
    do {
      Src = cast<DFNode>(Src->getParent());
    } while (Src->getLevel() > DstLvl);
  }
  assert(Src->getLevel() == Dst->getLevel() &&
         "Src and Dst levels should be equal here. Something isn't right!");
  if (Src->isRoot() || Dst->isRoot()) {
    // TODO: if this is the case, we should check if the DFGs can be parallel or
    // not.
    LLVM_DEBUG(errs() << "The two nodes belong to different DFGs, assume that they "
                    "are sequential and return true!\n");
    return true;
  }
  if (Src->getParent() == Dst->getParent()) {
    return Src->checkForwardPath(Dst);
  }
  return existsForwardPath(Src->getParent(), Dst->getParent());
}

} // namespace hpvmUtils
