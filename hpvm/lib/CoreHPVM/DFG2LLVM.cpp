#include "CoreHPVM/DFG2LLVM.h"
#define DEBUG_TYPE "dfg2llvm"
using namespace llvm;

namespace dfg2llvm {
// -------------- CodeGenTraversal Implementation -----------------

/* Traverse the function argument list in reverse order to get argument at a
 * distance offset fromt he end of argument list of function F
 */
Argument *CodeGenTraversal::getArgumentFromEnd(Function *F, unsigned offset) {
  assert((F->getFunctionType()->getNumParams() >= offset && offset > 0) &&
         "Invalid offset to access arguments!");
  Function::arg_iterator e = F->arg_end();
  // Last element of argument iterator is dummy. Skip it.
  e--;
  Argument *arg;
  for (; offset != 0; e--) {
    offset--;
    arg = &*e;
  }
  return arg;
}
Value *CodeGenTraversal::getInValueAt(DFNode *Child, unsigned i,
                                      Function *ParentF_CPU,
                                      Instruction *InsertBefore) {
  // TODO: Assumption is that each input port of a node has just one
  // incoming edge. May change later on.

  // Find the incoming edge at the requested input port
  LLVM_DEBUG(errs() << "Finding incoming edge " << i << " for "
               << Child->getFuncPointer()->getName() << "\n");
  DFEdge *E = Child->getInDFEdgeAt(i);
  assert(E && "No incoming edge or binding for input element!");
  // Find the Source DFNode associated with the incoming edge
  DFNode *SrcDF = E->getSourceDF();

  // If Source DFNode is a dummyNode, edge is from parent. Get the
  // argument from argument list of this internal node
  Value *inputVal;
  if (SrcDF->isEntryNode()) {
    inputVal = getArgumentAt(ParentF_CPU, E->getSourcePosition());
    LLVM_DEBUG(errs() << "Argument " << i << " = " << *inputVal << "\n");
  } else {
    // edge is from a sibling
    // Check - code should already be generated for this source dfnode
    assert(OutputMap.count(SrcDF) &&
           "Source node call not found. Dependency violation!");

    // Find CallInst associated with the Source DFNode using OutputMap
    Value *CI = OutputMap[SrcDF];

    // Extract element at source position from this call instruction
    std::vector<unsigned> IndexList;
    IndexList.push_back(E->getSourcePosition());
    LLVM_DEBUG(errs() << "Going to generate ExtarctVal inst from " << *CI << "\n");
    ExtractValueInst *EI =
        ExtractValueInst::Create(CI, IndexList, "", InsertBefore);
    inputVal = EI;
  }
  return inputVal;
}

// Copy over any index limit calculations which may be used
// to specify dynamic nodes instances.
void CodeGenTraversal::copyChildIndexCalc(DFNode *C, Function *F_CPU,
                                          ValueToValueMapTy &VMap,
                                          Instruction *InsertBefore) {

  for (unsigned j = 0; j < C->getNumOfDim(); j++) {
    std::vector<Instruction *> toClone;
    std::queue<Value *> workList;

    workList.push(C->getDimLimits()[j]);

    while (workList.size()) {
      Value *entry = workList.front();
      workList.pop();

      // Arguments are already available in
      // the cloned function.
      if (isa<Argument>(entry))
        continue;

      // Constant values should be available in
      // the operand;
      if (isa<Constant>(entry))
        continue;

      // Only calls with scalar operands are currrently allowed
      if (CallInst *CI = dyn_cast<CallInst>(entry)) {

        for (unsigned c = 0; c < CI->getNumArgOperands(); c++) {
          Value *CArg = CI->getArgOperand(c);
          assert(!CArg->getType()->isPointerTy() &&
                 "Index calculation chain of instructions can not contain "
                 "calls with pointer operands");
        }
      }

      assert(!isa<LoadInst>(entry) && !isa<StoreInst>(entry) &&
             "Only non-memory reading/writing operations legal in dimension "
             "limit calculation");

      Instruction *I = dyn_cast<Instruction>(entry);

      if (!I) {
        LLVM_DEBUG(errs() << "Unknown value type: " << *entry << "\n");
        assert(false && "Non-argument or instruction type used in limit calc.");
      }

      toClone.push_back(I);

      for (unsigned k = 0; k < I->getNumOperands(); k++) {
        workList.push(I->getOperand(k));
      }
    }

    // Clone instructions in reverse order due to use-def chains
    for (int i = toClone.size() - 1; i >= 0; i--) {
      Instruction *I = toClone[i];

      // Already copied over value.
      if (VMap.find(&*I) != VMap.end())
        continue;

      Instruction *NewInst = I->clone();

      // Update the cloned instructions operands to use
      // the VMapped Values.
      for (unsigned o = 0; o < NewInst->getNumOperands(); o++) {

        if (isa<Constant>(NewInst->getOperand(o)))
          continue;

        assert(VMap.find(I->getOperand(o)) != VMap.end() &&
               "Copied Value use-def chain not correctly copied!");

        NewInst->setOperand(o, VMap[I->getOperand(o)]);
      }

      NewInst->insertBefore(InsertBefore);

      // Add instruction to VMap;
      VMap[&*I] = NewInst;
    }
  }

  LLVM_DEBUG(errs() << "F_CPU after cloning in index values: " << *F_CPU << "\n");
}

void CodeGenTraversal::invokeChild(DFNode *C, Function *F_CPU,
                                   ValueToValueMapTy &VMap, Instruction *IB,
                                   hpvm::Target Tag) {
  Function *CF = C->getFuncPointer();

  //  Function* CF_CPU = C->getGenFunc();
  Function *CF_CPU = C->getGenFuncForTarget(Tag);
  assert(CF_CPU != NULL &&
         "Found leaf node for which code generation has not happened yet!\n");
  assert(C->hasCPUGenFuncForTarget(Tag) &&
         "The generated function to be called from cpu backend is not an cpu "
         "function\n");
  LLVM_DEBUG(errs() << "Invoking child node" << CF_CPU->getName() << "\n");

  std::vector<Value *> Args;
  // Create argument list to pass to call instruction
  // First find the correct values using the edges
  // The remaing six values are inserted as constants for now.
  for (unsigned i = 0; i < CF->getFunctionType()->getNumParams(); i++) {
    Args.push_back(getInValueAt(C, i, F_CPU, IB));
  }

  Value *I64Zero = ConstantInt::get(Type::getInt64Ty(F_CPU->getContext()), 0);
  for (unsigned j = 0; j < 6; j++)
    Args.push_back(I64Zero);

  LLVM_DEBUG(errs() << "Gen Function type: " << *CF_CPU->getType() << "\n");
  LLVM_DEBUG(errs() << "Node Function type: " << *CF->getType() << "\n");
  LLVM_DEBUG(errs() << "Arguments: "; for (const Value *Arg
                                      : Args) errs()
                                 << *(Arg->getType()) << ", ";
        errs() << "\n");

  // Copying over index calculation.
  copyChildIndexCalc(C, F_CPU, VMap, IB);

  // Call the F_CPU function associated with this node
  CallInst *CI =
      CallInst::Create(CF_CPU, Args, CF_CPU->getName() + "_output", IB);
  LLVM_DEBUG(errs() << *CI << "\n");
  OutputMap[C] = CI;

  // Find num of dimensions this node is replicated in.
  // Based on number of dimensions, insert loop instructions
  std::string varNames[3] = {"x", "y", "z"};
  unsigned numArgs = CI->getNumArgOperands();
  for (unsigned j = 0; j < C->getNumOfDim(); j++) {
    Value *indexLimit = NULL;
    // Limit can either be a constant or an arguement of the internal node.
    // In case of constant we can use that constant value directly in the
    // new F_CPU function. In case of an argument, we need to get the mapped
    // value using VMap
    if (ConstantInt *ConstDimLimit =
            dyn_cast<ConstantInt>(C->getDimLimits()[j])) {
      indexLimit = C->getDimLimits()[j];
      LLVM_DEBUG(errs() << "In Constant case:\n"
                   << "  indexLimit type = " << *indexLimit->getType() << "\n");
      if (ConstDimLimit->getZExtValue() == 1) {
        LLVM_DEBUG(errs() << "DimLimit is 1, no need for loop!\n");
        continue;
      }
    } else if (isa<Constant>(C->getDimLimits()[j])) {
      indexLimit = C->getDimLimits()[j];
      LLVM_DEBUG(errs() << "In Constant case:\n"
                   << "  indexLimit type = " << *indexLimit->getType() << "\n");
    } else {
      indexLimit = VMap[C->getDimLimits()[j]];
      LLVM_DEBUG(errs() << "In VMap case:"
                   << "  indexLimit type = " << *indexLimit->getType() << "\n");
    }
    assert(indexLimit && "Invalid dimension limit!");
    // Insert loop
    Value *indexVar = addLoop(CI, indexLimit, varNames[j]);
    LLVM_DEBUG(errs() << "indexVar type = " << *indexVar->getType() << "\n");
    // Insert index variable and limit arguments
    CI->setArgOperand(numArgs - 6 + j, indexVar);
    CI->setArgOperand(numArgs - 3 + j, indexLimit);
  }

  if (Tag == hpvm::CPU_TARGET) {
    // Insert call to runtime to push the dim limits and instanceID on the depth
    // stack
    Value *args[] = {
        ConstantInt::get(Type::getInt32Ty(CI->getContext()),
                         C->getNumOfDim()), // numDim
        CI->getArgOperand(numArgs - 3 + 0), // limitX
        CI->getArgOperand(numArgs - 6 + 0), // iX
        CI->getArgOperand(numArgs - 3 + 1), // limitY
        CI->getArgOperand(numArgs - 6 + 1), // iY
        CI->getArgOperand(numArgs - 3 + 2), // limitZ
        CI->getArgOperand(numArgs - 6 + 2)  // iZ
    };

    CallInst *Push = CallInst::Create(llvm_hpvm_cpu_dstack_push,
                                      ArrayRef<Value *>(args, 7), "", CI);
    LLVM_DEBUG(errs() << "Push on stack: " << *Push << "\n");
    // Insert call to runtime to pop the dim limits and instanceID from the
    // depth stack
    BasicBlock::iterator i(CI);
    ++i;
    Instruction *NextI = &*i;
    // Next Instruction should also belong to the same basic block as the basic
    // block will have a terminator instruction
    assert(NextI->getParent() == CI->getParent() &&
           "Next Instruction should also belong to the same basic block!");

    CallInst *Pop = CallInst::Create(llvm_hpvm_cpu_dstack_pop, None, "", NextI);
    LLVM_DEBUG(errs() << "Pop from stack: " << *Pop << "\n");
    LLVM_DEBUG(errs() << *CI->getParent()->getParent());
  }
}
/* Add Loop around the instruction I
 * Algorithm:
 * (1) Split the basic block of instruction I into three parts, where the
 * middleblock/body would contain instruction I.
 * (2) Add phi node before instruction I. Add incoming edge to phi node from
 * predecessor
 * (3) Add increment and compare instruction to index variable
 * (4) Replace terminator/branch instruction of body with conditional branch
 * which loops over bidy if true and goes to end if false
 * (5) Update phi node of body
 */
Value *CodeGenTraversal::addLoop(Instruction *I, Value *limit,
                                 const Twine &indexName) {
  BasicBlock *Entry = I->getParent();
  BasicBlock *ForBody = Entry->splitBasicBlock(I, "for.body");

  BasicBlock::iterator i(I);
  ++i;
  Instruction *NextI = &*i;
  // Next Instruction should also belong to the same basic block as the basic
  // block will have a terminator instruction
  assert(NextI->getParent() == ForBody &&
         "Next Instruction should also belong to the same basic block!");
  BasicBlock *ForEnd = ForBody->splitBasicBlock(NextI, "for.end");

  // Add Phi Node for index variable
  PHINode *IndexPhi = PHINode::Create(Type::getInt64Ty(I->getContext()), 2,
                                      "index." + indexName, I);

  // Add incoming edge to phi
  IndexPhi->addIncoming(ConstantInt::get(Type::getInt64Ty(I->getContext()), 0),
                        Entry);
  // Increment index variable
  BinaryOperator *IndexInc = BinaryOperator::Create(
      Instruction::Add, IndexPhi,
      ConstantInt::get(Type::getInt64Ty(I->getContext()), 1),
      "index." + indexName + ".inc", ForBody->getTerminator());

  // Compare index variable with limit
  CmpInst *Cond =
      CmpInst::Create(Instruction::ICmp, CmpInst::ICMP_ULT, IndexInc, limit,
                      "cond." + indexName, ForBody->getTerminator());

  // Replace the terminator instruction of for.body with new conditional
  // branch which loops over body if true and branches to for.end otherwise
  BranchInst *BI = BranchInst::Create(ForBody, ForEnd, Cond);
  ReplaceInstWithInst(ForBody->getTerminator(), BI);

  // Add incoming edge to phi node in body
  IndexPhi->addIncoming(IndexInc, ForBody);
  return IndexPhi;
}
bool CodeGenTraversal::checkPreferredTarget(DFNode *N, hpvm::Target T) {
  Function *F = N->getFuncPointer();
  Module *M = F->getParent();
  NamedMDNode *HintNode;
  switch (T) {
  case hpvm::GPU_TARGET:
    HintNode = M->getOrInsertNamedMetadata("hpvm_hint_gpu");
    break;
  case hpvm::CUDNN_TARGET:
    HintNode = M->getOrInsertNamedMetadata("hpvm_hint_cudnn");
    break;
  case hpvm::CPU_TARGET:
    HintNode = M->getOrInsertNamedMetadata("hpvm_hint_cpu");
    break;
  case hpvm::TENSOR_TARGET:
    HintNode = M->getOrInsertNamedMetadata("hpvm_hint_promise");
    break;
  case hpvm::FPGA_TARGET:
    HintNode = M->getOrInsertNamedMetadata("hpvm_hint_fpga");
    break;
  default:
    llvm_unreachable("Target Not supported yet!");
  }
  for (unsigned i = 0; i < HintNode->getNumOperands(); i++) {
    MDNode *MetaNode = HintNode->getOperand(i);
    Value *FHint =
        dyn_cast<ValueAsMetadata>(MetaNode->getOperand(0).get())->getValue();
    if (F == FHint)
      return true;
  }
  return false;
}

hpvm::Target CodeGenTraversal::getPreferredTarget(DFNode *N) {
  return hpvmUtils::getPreferredTarget(N->getFuncPointer());
}

bool CodeGenTraversal::preferredTargetIncludes(DFNode *N, hpvm::Target T) {

  Function *F = N->getFuncPointer();
  Module *M = F->getParent();
  std::vector<NamedMDNode *> HintNode;
  switch (T) {
  case hpvm::GPU_TARGET:
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_gpu"));
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_cpu_gpu"));
    break;
  case hpvm::CPU_TARGET:
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_cpu"));
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_cpu_gpu"));
    break;
  case hpvm::FPGA_TARGET:
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_fpga"));
    break;
  case hpvm::CUDNN_TARGET:
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_cudnn"));
    break;
  case hpvm::TENSOR_TARGET:
    HintNode.push_back(M->getOrInsertNamedMetadata("hpvm_hint_promise"));
    break;
  case hpvm::CPU_OR_GPU_TARGET:
    assert(false && "Target should be one of CPU/GPU\n");
    break;
  default:
    llvm_unreachable("Target Not supported yet!");
  }

  for (unsigned h = 0; h < HintNode.size(); h++) {
    for (unsigned i = 0; i < HintNode[h]->getNumOperands(); i++) {
      MDNode *MetaNode = HintNode[h]->getOperand(i);
      Value *FHint =
          dyn_cast<ValueAsMetadata>(MetaNode->getOperand(0).get())->getValue();
      if (F == FHint)
        return true;
    }
  }
  return false;
}

// Generate Code for declaring a constant string [L x i8] and return a pointer
// to the start of it.
Value *CodeGenTraversal::getStringPointer(const Twine &S, Instruction *IB,
                                          const Twine &Name) {
  Constant *SConstant =
      ConstantDataArray::getString(M.getContext(), S.str(), true);
  Value *SGlobal =
      new GlobalVariable(M, SConstant->getType(), true,
                         GlobalValue::InternalLinkage, SConstant, Name);
  Value *Zero = ConstantInt::get(Type::getInt64Ty(M.getContext()), 0);
  Value *GEPArgs[] = {Zero, Zero};
  GetElementPtrInst *SPtr = GetElementPtrInst::Create(
      SConstant->getType(), SGlobal, ArrayRef<Value *>(GEPArgs, 2), Name + "Ptr", IB);
  return SPtr;
}

inline void renameNewArgument(Function *newF, const Twine &argName) {
  // Get Last argument in Function Arg List and rename it to given name
  Argument *lastArg = &*(newF->arg_end() - 1);
  lastArg->setName(argName);
}

// Creates a function with an additional argument of the specified type and
// name. The previous function is not deleted.
Function *addArgument(Function *F, Type *Ty, const Twine &name) {
  Argument *new_arg = new Argument(Ty, name);

  // Create the argument type list with added argument types
  std::vector<Type *> ArgTypes;
  for (Function::const_arg_iterator ai = F->arg_begin(), ae = F->arg_end();
       ai != ae; ++ai) {
    ArgTypes.push_back(ai->getType());
  }
  ArgTypes.push_back(new_arg->getType());

  // Adding new arguments to the function argument list, would not change the
  // function type. We need to change the type of this function to reflect the
  // added arguments. So, we create a clone of this function with the correct
  // type.
  FunctionType *FTy =
      FunctionType::get(F->getReturnType(), ArgTypes, F->isVarArg());
  Function *newF = Function::Create(FTy, F->getLinkage(), F->getName() + "_c",
                                    F->getParent());
  renameNewArgument(newF, name);
  newF = hpvmUtils::cloneFunction(F, newF, false);

  // Check if the function is used by a metadata node
  if (F->isUsedByMetadata()) {
    hpvmUtils::fixHintMetadata(*F->getParent(), F, newF);
  }

  return newF;
}

// Return new function with additional index and limit arguments.
// The original function is removed from the module and erased.
Function *CodeGenTraversal::addIdxDimArgs(Function *F) {
  LLVM_DEBUG(errs() << "Adding dimension and limit arguments to Function: " << F->getName());
  LLVM_DEBUG(errs() << "Function Type: " << *F->getFunctionType() << "\n");
  // Add Index and Dim arguments
  std::string names[] = {"idx_x", "idx_y", "idx_z", "dim_x", "dim_y", "dim_z"};
  Function *newF;
  for (int i = 0; i < 6; ++i) {
    newF = addArgument(F, Type::getInt64Ty(F->getContext()), names[i]);
    F->replaceAllUsesWith(UndefValue::get(F->getType()));
    F->eraseFromParent();
    F = newF;
  }
  LLVM_DEBUG(errs() << "Function Type after adding args: "
               << *newF->getFunctionType() << "\n");
  return newF;
}

// Extract elements from an aggregate value. TyList contains the type of each
// element, and names vector contains a name. IB is the instruction before which
// all the generated code would be inserted.
std::vector<Value *>
CodeGenTraversal::extractElements(Value *Aggregate, std::vector<Type *> TyList,
                                  std::vector<std::string> names,
                                  Instruction *IB) {
  // Extract input data from i8* Aggregate.addr and store them in a vector.
  // For each argument
  std::vector<Value *> Elements;
  GetElementPtrInst *GEP;
  unsigned argNum = 0;
  for (Type *Ty : TyList) {
    // BitCast: %arg.addr = bitcast i8* Aggregate.addr to <pointer-to-argType>
    CastInst *BI = BitCastInst::CreatePointerCast(Aggregate, Ty->getPointerTo(),
                                                  names[argNum] + ".addr", IB);
    // Load: %arg = load <pointer-to-argType> %arg.addr
    LoadInst *LI = new LoadInst(Ty, BI, names[argNum], IB);
    // Patch argument to call instruction
    Elements.push_back(LI);
    // errs() << "Pushing element " << *LI << "\n";
    // CI->setArgOperand(argNum, LI);

    // TODO: Minor Optimization - The last GEP statement can/should be left out
    // as no more arguments left
    // Increment using GEP: %nextArg = getelementptr <ptr-to-argType> %arg.addr,
    // i64 1 This essentially takes us to the next argument in memory
    Constant *IntOne = ConstantInt::get(Type::getInt64Ty(M.getContext()), 1);
    if (argNum < TyList.size() - 1)
      GEP = GetElementPtrInst::Create(Ty, BI, ArrayRef<Value *>(IntOne),
                                      "nextArg", IB);
    // Increment argNum and for the next iteration use result of this GEP to
    // extract next argument
    argNum++;
    Aggregate = GEP;
  }
  return Elements;
}

// Traverse the function F argument list to get argument at offset
Argument *CodeGenTraversal::getArgumentAt(Function *F, unsigned offset) {
  LLVM_DEBUG(errs() << "Finding argument " << offset << ":\n");
  assert((F->getFunctionType()->getNumParams() > offset) &&
         "Invalid offset to access arguments!");

  Function::arg_iterator ArgIt = F->arg_begin() + offset;
  Argument *Arg = &*ArgIt;
  return Arg;
}

void CodeGenTraversal::initTimerAPI() {
  DECLARE(llvm_hpvm_initializeTimerSet);
  DECLARE(llvm_hpvm_switchToTimer);
  DECLARE(llvm_hpvm_printTimerSet);
}

// Timer Routines
// Initialize the timer set
void CodeGenTraversal::initializeTimerSet(Instruction *InsertBefore) {
  // LLVM_DEBUG(errs() << "Inserting call to: " << *llvm_hpvm_initializeTimerSet <<
  // "\n");
  TIMER(TimerSet = new GlobalVariable(
            M, Type::getInt8PtrTy(M.getContext()), false,
            GlobalValue::CommonLinkage,
            Constant::getNullValue(Type::getInt8PtrTy(M.getContext())),
            Twine("hpvmTimerSet_") + TargetName);
        LLVM_DEBUG(errs() << "New global variable: " << *TimerSet << "\n");

        Value *TimerSetAddr = CallInst::Create(llvm_hpvm_initializeTimerSet,
                                               None, "", InsertBefore);
        new StoreInst(TimerSetAddr, TimerSet, InsertBefore););
}

void CodeGenTraversal::switchToTimer(enum hpvm_TimerID timer,
                                     Instruction *InsertBefore) {
  Value *switchArgs[] = {TimerSet, getTimerID(M, timer)};
  TIMER(CallInst::Create(llvm_hpvm_switchToTimer,
                         ArrayRef<Value *>(switchArgs, 2), "", InsertBefore));
}

void CodeGenTraversal::printTimerSet(Instruction *InsertBefore) {
  Value *TimerName;
  TIMER(TimerName =
            getStringPointer(TargetName + Twine("_Timer"), InsertBefore));
  Value *printArgs[] = {TimerSet, TimerName};
  TIMER(CallInst::Create(llvm_hpvm_printTimerSet,
                         ArrayRef<Value *>(printArgs, 2), "", InsertBefore));
}

 void CodeGenTraversal::visit(DFInternalNode *N) {
    // If code has already been generated for this internal node, skip the
    // children
    if (N->getGenFunc() != NULL)
      return;

    LLVM_DEBUG(errs() << "Start: Generating Code for Node (I) - "
                 << N->getFuncPointer()->getName() << "\n");

    // Follows a bottom-up approach for code generation.
    // First generate code for all the child nodes
    for (DFGraph::children_iterator i = N->getChildGraph()->begin(),
                                    e = N->getChildGraph()->end();
         i != e; ++i) {
      DFNode *child = *i;
      child->applyDFNodeVisitor(*this);
    }
    // Generate code for this internal node now. This way all the cloned
    // functions for children exist.
    codeGen(N);
    LLVM_DEBUG(errs() << "DONE: Generating Code for Node (I) - "
                 << N->getFuncPointer()->getName() << "\n");
  }

 void CodeGenTraversal::visit(DFLeafNode *N) {
    LLVM_DEBUG(errs() << "Start: Generating Code for Node (L) - "
                 << N->getFuncPointer()->getName() << "\n");
    codeGen(N);
    LLVM_DEBUG(errs() << "DONE: Generating Code for Node (L) - "
                 << N->getFuncPointer()->getName() << "\n");
  }

// Implementation of Helper Functions
ConstantInt *getTimerID(Module &M, enum hpvm_TimerID timer) {
  return ConstantInt::get(Type::getInt32Ty(M.getContext()), timer);
}

ConstantInt *getTargetID(Module &M, enum hpvm::Target T) {
  return ConstantInt::get(Type::getInt32Ty(M.getContext()), T);
}

// Find if argument has the given attribute
bool hasAttribute(Function *F, unsigned arg_index, Attribute::AttrKind AK) {
  return F->getAttributes().hasAttribute(arg_index + 1, AK);
}

}