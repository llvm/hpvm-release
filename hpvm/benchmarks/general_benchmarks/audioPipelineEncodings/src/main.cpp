#include <audio.h>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <chrono>

#include "../../include/hpvm.h"
#include "heterocc.h"

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection for CPU requires sudo
// NOTE: For CPU you may need to increase the open file limit, using
// ulimit -n 2048 (or some other number)
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE 1
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

typedef int64_t loopVar_t;

// #define SAMPLERATE 48000
// #define BLOCK_SIZE 1024
// #define NORDER 3
// #define NUM_CHANNELS (OrderToComponents(NORDER, true))
// #define NUM_SRCS 16

// #define NUM_BLOCKS 500

#define fSqrt32 sqrt(3.f)/2.f
#define fSqrt58 sqrt(5.f/8.f)
#define fSqrt152 sqrt(15.f)/2.f
#define fSqrt38 sqrt(3.f/8.f)

// Size for experiments
#define EXP_SIZE 100
#define EXP_CHANNELS 16

// For CAmbisonicBase
CAmbisonicBase::CAmbisonicBase()
    : m_nOrder(0)
    , m_b3D(0)
    , m_nChannelCount(0)
    , m_bOpt(0)
{
}

unsigned CAmbisonicBase::GetOrder() {
    return m_nOrder;
}

bool CAmbisonicBase::GetHeight() {
    return m_b3D;
}

unsigned CAmbisonicBase::GetChannelCount() {
    return m_nChannelCount;
}

bool CAmbisonicBase::Configure(unsigned nOrder, bool b3D, unsigned nMisc) {
    m_nOrder = nOrder;
    m_b3D = b3D;
    m_nChannelCount = OrderToComponents(m_nOrder, m_b3D);

    return true;
}

// For CBFormat
CBFormat::CBFormat() {
    m_nSamples = 0;
    m_nDataLength = 0;
}

unsigned CBFormat::GetSampleCount() {
    return m_nSamples;
}

bool CBFormat::Configure(unsigned nOrder, bool b3D, unsigned nSampleCount) {
    bool success = CAmbisonicBase::Configure(nOrder, b3D, nSampleCount);
    if(!success)
        return false;

    m_nSamples = nSampleCount;
    m_nDataLength = m_nSamples * m_nChannelCount;

    m_pfData.resize(m_nDataLength);
    memset(m_pfData.data(), 0, m_nDataLength * sizeof(float));
    m_ppfChannels.reset(new float*[m_nChannelCount]);

    for(unsigned niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        m_ppfChannels[niChannel] = &m_pfData[niChannel * m_nSamples];
    }

    return true;
}

void CBFormat::Reset() {
    memset(m_pfData.data(), 0, m_nDataLength * sizeof(float));
}

void CBFormat::Refresh() { }

void CBFormat::InsertStream(float* pfData, unsigned nChannel, unsigned nSamples) {
    memcpy(m_ppfChannels[nChannel], pfData, nSamples * sizeof(float));
}

void CBFormat::ExtractStream(float* pfData, unsigned nChannel, unsigned nSamples) {
    memcpy(pfData, m_ppfChannels[nChannel], nSamples * sizeof(float));
}

void CBFormat::operator = (const CBFormat &bf) {
    memcpy(m_pfData.data(), bf.m_pfData.data(), m_nDataLength * sizeof(float));
}

bool CBFormat::operator == (const CBFormat &bf) {
    if(m_b3D == bf.m_b3D && m_nOrder == bf.m_nOrder && m_nDataLength == bf.m_nDataLength)
        return true;
    else
        return false;
}

bool CBFormat::operator != (const CBFormat &bf) {
    if(m_b3D != bf.m_b3D || m_nOrder != bf.m_nOrder || m_nDataLength != bf.m_nDataLength)
        return true;
    else
        return false;
}

CBFormat& CBFormat::operator += (const CBFormat &bf) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] += bf.m_ppfChannels[niChannel][niSample];
        }
    }

    return *this;
}

CBFormat& CBFormat::operator -= (const CBFormat &bf) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] -= bf.m_ppfChannels[niChannel][niSample];
        }
    }

    return *this;
}

CBFormat& CBFormat::operator *= (const CBFormat &bf) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] *= bf.m_ppfChannels[niChannel][niSample];
        }
    }

    return *this;
}

CBFormat& CBFormat::operator /= (const CBFormat &bf) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] /= bf.m_ppfChannels[niChannel][niSample];
        }
    }

    return *this;
}

CBFormat& CBFormat::operator += (const float &fValue) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] += fValue;
        }
    }

    return *this;
}

CBFormat& CBFormat::operator -= (const float &fValue) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] -= fValue;
        }
    }

    return *this;
}

CBFormat& CBFormat::operator *= (const float &fValue) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] *= fValue;
        }
    }

    return *this;
}

CBFormat& CBFormat::operator /= (const float &fValue) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < m_nSamples; niSample++)
        {
            m_ppfChannels[niChannel][niSample] /= fValue;
        }
    }

    return *this;
}

// For CAmbisonicSourcec
CAmbisonicSource::CAmbisonicSource() {
    m_polPosition.fAzimuth = 0.f;
    m_polPosition.fElevation = 0.f;
    m_polPosition.fDistance = 1.f;
    m_fGain = 1.f;
}

bool CAmbisonicSource::Configure(unsigned nOrder, bool b3D, unsigned nMisc) {
    bool success = CAmbisonicBase::Configure(nOrder, b3D, nMisc);
    if(!success)
        return false;

    m_pfCoeff.resize( m_nChannelCount, 0 );
    // for a Basic Ambisonics decoder all of the gains are set to 1.f
    m_pfOrderWeights.resize( m_nOrder + 1, 1.f );

    return true;
}

void CAmbisonicSource::Reset() {
    //memset(m_pfCoeff, 0, m_nChannelCount * sizeof(float));
}

void CAmbisonicSource::Refresh() {
    float fCosAzim = cosf(m_polPosition.fAzimuth);
    float fSinAzim = sinf(m_polPosition.fAzimuth);
    float fCosElev = cosf(m_polPosition.fElevation);
    float fSinElev = sinf(m_polPosition.fElevation);

    float fCos2Azim = cosf(2.f * m_polPosition.fAzimuth);
    float fSin2Azim = sinf(2.f * m_polPosition.fAzimuth);
    float fSin2Elev = sinf(2.f * m_polPosition.fElevation);

    if(m_b3D)
    {
        // Uses ACN channel ordering and SN3D normalization scheme (AmbiX format)
        if(m_nOrder >= 0)
        {
            m_pfCoeff[0] = 1.f * m_pfOrderWeights[0]; // W
        }
        if(m_nOrder >= 1)
        {
            m_pfCoeff[1] = (fSinAzim * fCosElev) * m_pfOrderWeights[1]; // Y
            m_pfCoeff[2] = (fSinElev) * m_pfOrderWeights[1]; // Z
            m_pfCoeff[3] = (fCosAzim * fCosElev) * m_pfOrderWeights[1]; // X
        }
        if(m_nOrder >= 2)
        {
            m_pfCoeff[4] = fSqrt32*(fSin2Azim * powf(fCosElev, 2)) * m_pfOrderWeights[2]; // V
            m_pfCoeff[5] = fSqrt32*(fSinAzim * fSin2Elev) * m_pfOrderWeights[2]; // T
            m_pfCoeff[6] = (1.5f * powf(fSinElev, 2.f) - 0.5f) * m_pfOrderWeights[2]; // R
            m_pfCoeff[7] = fSqrt32*(fCosAzim * fSin2Elev) * m_pfOrderWeights[2]; // S
            m_pfCoeff[8] = fSqrt32*(fCos2Azim * powf(fCosElev, 2)) * m_pfOrderWeights[2]; // U
        }
        if(m_nOrder >= 3)
        {
            m_pfCoeff[9] = fSqrt58*(sinf(3.f * m_polPosition.fAzimuth) * powf(fCosElev, 3.f)) * m_pfOrderWeights[3]; // Q
            m_pfCoeff[10] = fSqrt152*(fSin2Azim * fSinElev * powf(fCosElev, 2.f)) * m_pfOrderWeights[3]; // O
            m_pfCoeff[11] = fSqrt38*(fSinAzim * fCosElev * (5.f * powf(fSinElev, 2.f) - 1.f)) * m_pfOrderWeights[3]; // M
            m_pfCoeff[12] = (fSinElev * (5.f * powf(fSinElev, 2.f) - 3.f) * 0.5f) * m_pfOrderWeights[3]; // K
            m_pfCoeff[13] = fSqrt38*(fCosAzim * fCosElev * (5.f * powf(fSinElev, 2.f) - 1.f)) * m_pfOrderWeights[3]; // L
            m_pfCoeff[14] = fSqrt152*(fCos2Azim * fSinElev * powf(fCosElev, 2.f)) * m_pfOrderWeights[3]; // N
            m_pfCoeff[15] = fSqrt58*(cosf(3.f * m_polPosition.fAzimuth) * powf(fCosElev, 3.f)) * m_pfOrderWeights[3]; // P

        }
    }
    else
    {
        if(m_nOrder >= 0)
        {
            m_pfCoeff[0] = m_pfOrderWeights[0];
        }
        if(m_nOrder >= 1)
        {
            m_pfCoeff[1] = (fCosAzim * fCosElev) * m_pfOrderWeights[1];
            m_pfCoeff[2] = (fSinAzim * fCosElev) * m_pfOrderWeights[1];
        }
        if(m_nOrder >= 2)
        {
            m_pfCoeff[3] = (fCos2Azim * powf(fCosElev, 2)) * m_pfOrderWeights[2];
            m_pfCoeff[4] = (fSin2Azim * powf(fCosElev, 2)) * m_pfOrderWeights[2];
        }
        if(m_nOrder >= 3)
        {
            m_pfCoeff[5] = (cosf(3.f * m_polPosition.fAzimuth) * powf(fCosElev, 3.f)) * m_pfOrderWeights[3];
            m_pfCoeff[6] = (sinf(3.f * m_polPosition.fAzimuth) * powf(fCosElev, 3.f)) * m_pfOrderWeights[3];
        }
    }

    for(unsigned ni = 0; ni < m_nChannelCount; ni++)
        m_pfCoeff[ni] *= m_fGain;
}

void CAmbisonicSource::SetPosition(PolarPoint polPosition) {
    m_polPosition = polPosition;
}

PolarPoint CAmbisonicSource::GetPosition() {
    return m_polPosition;
}

void CAmbisonicSource::SetOrderWeight(unsigned nOrder, float fWeight) {
    m_pfOrderWeights[nOrder] = fWeight;
}

void CAmbisonicSource::SetOrderWeightAll(float fWeight) {
    for(unsigned niOrder = 0; niOrder < m_nOrder + 1; niOrder++) {
        m_pfOrderWeights[niOrder] = fWeight;
    }
}

void CAmbisonicSource::SetCoefficient(unsigned nChannel, float fCoeff) {
    m_pfCoeff[nChannel] = fCoeff;
}

float CAmbisonicSource::GetOrderWeight(unsigned nOrder) {
    return m_pfOrderWeights[nOrder];
}

float CAmbisonicSource::GetCoefficient(unsigned nChannel) {
    return m_pfCoeff[nChannel];
}

void CAmbisonicSource::SetGain(float fGain) {
    m_fGain = fGain;
}

float CAmbisonicSource::GetGain() {
    return m_fGain;
}

// For CAmbisonicEncoder
CAmbisonicEncoder::CAmbisonicEncoder() { }

CAmbisonicEncoder::~CAmbisonicEncoder() { }

bool CAmbisonicEncoder::Configure(unsigned nOrder, bool b3D, unsigned nMisc) {
    bool success = CAmbisonicSource::Configure(nOrder, b3D, nMisc);
    if(!success)
        return false;
    //SetOrderWeight(0, 1.f / sqrtf(2.f)); // Removed as seems to break SN3D normalisation
    
    return true;
}

void CAmbisonicEncoder::Refresh() {
    CAmbisonicSource::Refresh();
}

void CAmbisonicEncoder::Process(float* pfSrc, unsigned nSamples, CBFormat* pfDst) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    for(niChannel = 0; niChannel < m_nChannelCount; niChannel++)
    {
        for(niSample = 0; niSample < nSamples; niSample++)
        {
            pfDst->m_ppfChannels[niChannel][niSample] = pfSrc[niSample] * m_pfCoeff[niChannel];
        }
    }
}

// For CAmbisonicEncoderDist
CAmbisonicEncoderDist::CAmbisonicEncoderDist() {
    m_nSampleRate = 0;
    m_fDelay = 0.f;
    m_nDelay = 0;
    m_nDelayBufferLength = 0;
    m_pfDelayBuffer = 0;
    m_nIn = 0;
    m_nOutA = 0;
    m_nOutB = 0;
    m_fRoomRadius = 5.f;
    m_fInteriorGain = 0.f;
    m_fExteriorGain = 0.f;

    Configure(DEFAULT_ORDER, DEFAULT_HEIGHT, DEFAULT_SAMPLERATE);
}

CAmbisonicEncoderDist::~CAmbisonicEncoderDist() {
    if(m_pfDelayBuffer)
        delete [] m_pfDelayBuffer;
}

bool CAmbisonicEncoderDist::Configure(unsigned nOrder, bool b3D, unsigned nSampleRate) {
    bool success = CAmbisonicEncoder::Configure(nOrder, b3D, 0);
    if(!success)
        return false;
    m_nSampleRate = nSampleRate;
    m_nDelayBufferLength = (unsigned)((float)knMaxDistance / knSpeedOfSound * m_nSampleRate + 0.5f);
    if(m_pfDelayBuffer)
        delete [] m_pfDelayBuffer;
    m_pfDelayBuffer = new float[m_nDelayBufferLength];
    Reset();
    
    return true;
}

void CAmbisonicEncoderDist::Reset() {
    memset(m_pfDelayBuffer, 0, m_nDelayBufferLength * sizeof(float));
    m_fDelay = m_polPosition.fDistance / knSpeedOfSound * m_nSampleRate + 0.5f;
    m_nDelay = (int)m_fDelay;
    m_fDelay -= m_nDelay;
    m_nIn = 0;
    m_nOutA = (m_nIn - m_nDelay + m_nDelayBufferLength) % m_nDelayBufferLength;
    m_nOutB = (m_nOutA + 1) % m_nDelayBufferLength;
}

void CAmbisonicEncoderDist::Refresh() {
    CAmbisonicEncoder::Refresh();

    m_fDelay = fabs(m_polPosition.fDistance) / knSpeedOfSound * m_nSampleRate; //TODO abs() sees float as int!
    m_nDelay = (int)m_fDelay;
    m_fDelay -= m_nDelay;
    m_nOutA = (m_nIn - m_nDelay + m_nDelayBufferLength) % m_nDelayBufferLength;
    m_nOutB = (m_nOutA + 1) % m_nDelayBufferLength;

    //Source is outside speaker array
    if(fabs(m_polPosition.fDistance) >= m_fRoomRadius)
    {
        m_fInteriorGain    = (m_fRoomRadius / fabs(m_polPosition.fDistance)) / 2.f;
        m_fExteriorGain    = m_fInteriorGain;
    }
    else
    {
        m_fInteriorGain = (2.f - fabs(m_polPosition.fDistance) / m_fRoomRadius) / 2.f;
        m_fExteriorGain = (fabs(m_polPosition.fDistance) / m_fRoomRadius) / 2.f;
    }
}

// Accessed members list:
// m_pfDelayBuffer - float*
// m_nIn - int (output)
// m_fDelay - float
// m_fInteriorGain - float
// m_pfCoeff - std::vector<float>
// m_ExteriorGain - float
// m_nDelayBufferLength - unsigned
// m_nOutA - int (output)
// m_nOutB - int (output)
void CAmbisonicEncoderDist::Process(float* pfSrc, unsigned nSamples, CBFormat* pfDst) {
    unsigned niChannel = 0;
    unsigned niSample = 0;
    float fSrcSample = 0;

    for(niSample = 0; niSample < nSamples; niSample++)
    {
        //Store
        m_pfDelayBuffer[m_nIn] = pfSrc[niSample];
        //Read
        fSrcSample = m_pfDelayBuffer[m_nOutA] * (1.f - m_fDelay)
                    + m_pfDelayBuffer[m_nOutB] * m_fDelay;

        pfDst->m_ppfChannels[kW][niSample] = fSrcSample * m_fInteriorGain * m_pfCoeff[kW];

        fSrcSample *= m_fExteriorGain;
        for(niChannel = 1; niChannel < m_nChannelCount; niChannel++)
        {
            pfDst->m_ppfChannels[niChannel][niSample] = fSrcSample * m_pfCoeff[niChannel];
        }

        m_nIn = (m_nIn + 1) % m_nDelayBufferLength;
        m_nOutA = (m_nOutA + 1) % m_nDelayBufferLength;
        m_nOutB = (m_nOutB + 1) % m_nDelayBufferLength;
    }
}

// These two structs cover non-dynamic-sized members
struct EncIn
{
  float fDelay;
  float fInteriorGain;
  float fExteriorGain;
  unsigned nDelayBufferLength;
  unsigned nChannelCount;
};

struct EncInOut 
{
  int nIn;
  int nOutA;
  int nOutB;
};

void CAmbisonicEncoderDist::SetRoomRadius(float fRoomRadius) {
    m_fRoomRadius = fRoomRadius;
}

float CAmbisonicEncoderDist::GetRoomRadius() {
    return m_fRoomRadius;
}

// For Sound
ILLIXR_AUDIO::Sound::Sound(std::string srcFilename, unsigned nOrder, bool b3D){
    amp = 1.0;
    srcFile = new std::fstream(srcFilename, std::fstream::in);

    // NOTE: This is currently only accepts mono channel 16-bit depth WAV file
    // TODO: Change brutal read from wav file
    char temp[44];
    srcFile->read((char*)temp, 44);

    // BFormat file initialization
    BFormat = new CBFormat();
    bool ok = BFormat->Configure(nOrder, true, BLOCK_SIZE);
    BFormat->Refresh();

    // Encoder initialization
    BEncoder = new CAmbisonicEncoderDist();
    ok &= BEncoder->Configure(nOrder, true, SAMPLERATE);
    BEncoder->Refresh();
    srcPos.fAzimuth = 0;
    srcPos.fElevation = 0;
    srcPos.fDistance = 0;
    BEncoder->SetPosition(srcPos);
    BEncoder->Refresh();

    // HPVM-C related initialization
    // sampleArray = new float*[NUM_BLOCKS];
    // for (int i = 0; i < NUM_BLOCKS; ++i) {
    //     sampleArray[i] = new float[BLOCK_SIZE];
    // }

    // BFormatArray = new CBFormat*[NUM_BLOCKS];
    // BEncoderArray = new CAmbisonicEncoderDist*[NUM_BLOCKS];
    // for (int i = 0; i < NUM_BLOCKS; ++i) {
    //     BFormatArray[i] = new CBFormat();
    //     bool okk = BFormatArray[i]->Configure(nOrder, true, BLOCK_SIZE);
    //     BFormatArray[i]->Refresh();

    //     BEncoderArray[i] = new CAmbisonicEncoderDist();
    //     okk &= BEncoderArray[i]->Configure(nOrder, true, SAMPLERATE);
    //     BEncoderArray[i]->Refresh();
    //     BEncoderArray[i]->SetPosition(srcPos);
    //     BEncoderArray[i]->Refresh();
    // }    
}

void ILLIXR_AUDIO::Sound::setSrcPos(PolarPoint& pos){
    srcPos.fAzimuth = pos.fAzimuth;
    srcPos.fElevation = pos.fElevation;
    srcPos.fDistance = pos.fDistance;
    BEncoder->SetPosition(srcPos);
    BEncoder->Refresh();
    // for (int i = 0; i < NUM_BLOCKS; ++i) {
    //     BEncoderArray[i]->SetPosition(srcPos);
    //     BEncoderArray[i]->Refresh();
    // }
}

void ILLIXR_AUDIO::Sound::setSrcAmp(float ampScale){
    amp = ampScale;
}

ILLIXR_AUDIO::Sound::~Sound(){
    srcFile->close();
    delete srcFile;
    delete BFormat;
    delete BEncoder;

    // for (int i = 0; i <BLOCK_SIZE; ++i) {
    //     delete[] sampleArray[i];
    // }
    // delete[] sampleArray;

    // for (int i = 0; i < NUM_BLOCKS; ++i) {
    //     delete BFormatArray[i];
    //     delete BEncoderArray[i];
    // }
    // delete[] BFormatArray;
    // delete[] BEncoderArray;
}

// For ABAudio
ILLIXR_AUDIO::ABAudio::ABAudio(std::string outputFilePath, ProcessType procTypeIn){
    processType = procTypeIn;
    if (processType == ILLIXR_AUDIO::ABAudio::ProcessType::FULL){
        outputFile = new std::ofstream(outputFilePath, std::ios_base::out| std::ios_base::binary);
        generateWAVHeader();
    }

    soundSrcs = new std::vector<Sound*>;
}

ILLIXR_AUDIO::ABAudio::~ABAudio(){
    if (processType == ILLIXR_AUDIO::ABAudio::ProcessType::FULL){
        free(outputFile);
    }
    for (unsigned int soundIdx = 0; soundIdx < soundSrcs->size(); ++soundIdx){
        free((*soundSrcs)[soundIdx]);
    }
    free(soundSrcs);
}

void ILLIXR_AUDIO::ABAudio::loadSource(){
    // Add a bunch of sound sources
    Sound* inSound;
    PolarPoint position;

    if (processType == ILLIXR_AUDIO::ABAudio::ProcessType::FULL){
        inSound = new Sound("samples/lectureSample.wav", NORDER, true);
        position.fAzimuth = -0.1;
        position.fElevation = 3.14/2;
        position.fDistance = 1;
        inSound->setSrcPos(position);
        soundSrcs->push_back(inSound);
        inSound = new Sound("samples/radioMusicSample.wav", NORDER, true);
        position.fAzimuth = 1.0;
        position.fElevation = 0;
        position.fDistance = 5;
        inSound->setSrcPos(position);
        soundSrcs->push_back(inSound);
    }else{
        for (unsigned i = 0; i < NUM_SRCS; i++) {
            inSound = new Sound("samples/lectureSample.wav", NORDER, true);
            position.fAzimuth = -0.1 * i;
            position.fElevation = 3.14/2 * i;
            position.fDistance = 1 * i;
            inSound->setSrcPos(position);
            soundSrcs->push_back(inSound);
        }
    }
}

void ILLIXR_AUDIO::ABAudio::generateWAVHeader(){
	// brute force wav header
	WAVHeader wavh;
	outputFile->write((char*)&wavh, sizeof(WAVHeader));
}

extern "C" {

/******** normalization kernel ********/
static void normalizeBody(float* __restrict__ srcSamples,
                   const float* __restrict__ srcAmps,
                   const short* __restrict__ sampleTemp,
                   long nSamples, loopVar_t j, loopVar_t k) {
  srcSamples[j*nSamples+k] = srcAmps[j] * (sampleTemp[k] / 32767.0f);
}

/******** encode kernel ********/
static void encodeBody(float* __restrict__ srcSamples,
                float* __restrict__ srcDelayBuffer,
                float* __restrict__ srcChannels,
                const float* __restrict__ srcCoeffs,
                const EncIn* __restrict__ srcInputs,
                EncInOut* __restrict__ srcInOuts,
                long nSamples, long nChannelCount, long nDelayBufferLength,
                loopVar_t j) {
  const float* pfSrc = &srcSamples[j*BLOCK_SIZE];
  float* const pfDelayBuffer = &srcDelayBuffer[j*nDelayBufferLength];
  float* const ppfChannels = &srcChannels[j*nChannelCount*nSamples];
  const float* pfCoeff = &srcCoeffs[j*nChannelCount];
  const EncIn& srcIn = srcInputs[j];
  EncInOut& srcInOut = srcInOuts[j];
  for (loopVar_t niSample = 0; niSample < nSamples; niSample++) {
    __hpvm__isNonZeroLoop(niSample, BLOCK_SIZE);

    // Some issues with backend when using struct member in for condition
    unsigned bound = srcIn.nChannelCount;

    //Store
    pfDelayBuffer[srcInOut.nIn] = pfSrc[niSample];
    //Read
    float fSrcSample = pfDelayBuffer[srcInOut.nOutA] * (1.f - srcIn.fDelay)
      + pfDelayBuffer[srcInOut.nOutB] * srcIn.fDelay;

    size_t ppfIdx = nSamples*kW + niSample;
    ppfChannels[ppfIdx] = fSrcSample * srcIn.fInteriorGain * pfCoeff[kW];

    fSrcSample *= srcIn.fExteriorGain;
    for(unsigned niChannel = 1; niChannel < bound; niChannel++)
    {
      __hpvm__isNonZeroLoop(niChannel, EXP_CHANNELS - 1);
      size_t ppfIdx = nSamples*niChannel + niSample;
      ppfChannels[ppfIdx] = fSrcSample * pfCoeff[niChannel];
    }

    srcInOut.nIn = (srcInOut.nIn + 1) % srcIn.nDelayBufferLength;
    srcInOut.nOutA = (srcInOut.nOutA + 1) % srcIn.nDelayBufferLength;
    srcInOut.nOutB = (srcInOut.nOutB + 1) % srcIn.nDelayBufferLength;
  }
}

/******** sum kernel ********/
static void incrementBody(float* __restrict__ sumChannels,
                   float* __restrict__ srcChannels,
                   long nChannelCount, long nSamples,
                   loopVar_t j, loopVar_t niChannel) {
  float* dst = sumChannels;
  const float* src = &srcChannels[j*nChannelCount*nSamples];

  for (long niSample = 0; niSample < nSamples; niSample++) {
    __hpvm__isNonZeroLoop(niSample, BLOCK_SIZE);
    size_t idx = nSamples * niChannel + niSample;
    dst[idx] += src[idx];
  }
}

void encoderPipelineHcc(
  float* __restrict__ sumChannels, size_t bytes_sumChannels,
  float* __restrict__ srcChannels, size_t bytes_srcChannels,
  float* __restrict__ srcSamples, size_t bytes_srcSamples,
  const float* __restrict__ srcAmps, size_t bytes_srcAmps,
  const float* __restrict__ srcCoeffs, size_t bytes_srcCoeffs,
  float* __restrict__ srcDelayBuffer, size_t bytes_srcDelayBuffer,
  const EncIn* __restrict__ srcInputs, size_t bytes_srcInputs,
  EncInOut* __restrict__ srcInOuts, size_t bytes_srcInOuts,
  const short* __restrict__ sampleTemp, size_t bytes_sampleTemp,
  long nDelayBufferLength, long nChannelCount, long nSamples, long soundSrcsSize) {

  __hpvm__hint(hpvm::CPU_TARGET);

  auto root = __hetero_section_begin();
#ifdef FPGA
  auto wrapper_t = __hetero_task_begin(13,
      sumChannels, bytes_sumChannels,
      srcChannels, bytes_srcChannels,
      srcSamples, bytes_srcSamples,
      srcAmps, bytes_srcAmps,
      srcCoeffs, bytes_srcCoeffs,
      srcDelayBuffer, bytes_srcDelayBuffer,
      srcInputs, bytes_srcInputs,
      srcInOuts, bytes_srcInOuts,
      sampleTemp, bytes_sampleTemp,
      nDelayBufferLength, nChannelCount, nSamples, soundSrcsSize, 5,
      sumChannels, bytes_sumChannels,
      srcChannels, bytes_srcChannels,
      srcSamples, bytes_srcSamples,
      srcDelayBuffer, bytes_srcDelayBuffer,
      srcInOuts, bytes_srcInOuts
      );
  auto wrapper_s = __hetero_section_begin();
#endif
#ifdef GPU
  auto norm_t = __hetero_task_begin(5,
    srcSamples, bytes_srcSamples,
    srcAmps, bytes_srcAmps,
    sampleTemp, bytes_sampleTemp,
    nSamples, soundSrcsSize,
    1, srcSamples, bytes_srcSamples);
  auto norm_s = __hetero_section_begin();
#endif
  for (loopVar_t j = 0; j < soundSrcsSize; ++j) {
    for (loopVar_t k = 0; k < nSamples; ++k) {
      __hetero_parallel_loop(2,
        4, srcSamples, bytes_srcSamples,
        srcAmps, bytes_srcAmps,
        sampleTemp, bytes_sampleTemp,
        nSamples,
        1, srcSamples, bytes_srcSamples);
      __hpvm__hint(hpvm::DEVICE);
      __hpvm__isNonZeroLoop(j, NUM_SRCS);
      __hpvm__isNonZeroLoop(k, BLOCK_SIZE);
      // normalize samples to -1 to 1 float, with amplitude scale
      normalizeBody(srcSamples, srcAmps, sampleTemp, nSamples, j, k);
    }
  }
#ifdef GPU
  __hetero_section_end(norm_s);
  __hetero_task_end(norm_t);
#endif

#ifdef GPU
  auto encoder_t = __hetero_task_begin(10, srcSamples, bytes_srcSamples,
    srcDelayBuffer, bytes_srcDelayBuffer,
    srcChannels, bytes_srcChannels,
    srcCoeffs, bytes_srcCoeffs,
    srcInputs, bytes_srcInputs,
    srcInOuts, bytes_srcInOuts,
    nSamples, soundSrcsSize, nChannelCount, nDelayBufferLength,
    3, srcDelayBuffer, bytes_srcDelayBuffer,
    srcChannels, bytes_srcChannels,
    srcInOuts, bytes_srcInOuts
    );
  auto encoder_s = __hetero_section_begin();
#endif

  for (loopVar_t j = 0; j < soundSrcsSize; ++j) {
    __hetero_parallel_loop(1, 9, srcSamples, bytes_srcSamples,
                           srcDelayBuffer, bytes_srcDelayBuffer,
                           srcChannels, bytes_srcChannels,
                           srcCoeffs, bytes_srcCoeffs,
                           srcInputs, bytes_srcInputs,
                           srcInOuts, bytes_srcInOuts,
                           nSamples, nChannelCount, nDelayBufferLength,
                           3, srcDelayBuffer, bytes_srcDelayBuffer,
                           srcChannels, bytes_srcChannels,
                           srcInOuts, bytes_srcInOuts);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(j, NUM_SRCS);
    // Privatizable?: srcDelayBuffer
    encodeBody(srcSamples, srcDelayBuffer, srcChannels, srcCoeffs, srcInputs,
               srcInOuts, nSamples, nChannelCount, nDelayBufferLength, j);
  }

#ifdef GPU
    __hetero_section_end(encoder_s);
    __hetero_task_end(encoder_t);
#endif

#ifdef GPU
  auto sum_t = __hetero_task_begin(5, sumChannels, bytes_sumChannels,
    srcChannels, bytes_srcChannels,
    soundSrcsSize, nChannelCount, nSamples,
    1, sumChannels, bytes_sumChannels);
  auto sum_s = __hetero_section_begin();
#endif

  auto sum_t_inner = __hetero_task_begin(5, sumChannels, bytes_sumChannels,
    srcChannels, bytes_srcChannels,
    soundSrcsSize, nChannelCount, nSamples,
    1, sumChannels, bytes_sumChannels);
  __hpvm__hint(hpvm::DEVICE);
  for (loopVar_t j = 0; j < soundSrcsSize; ++j) {
    __hpvm__isNonZeroLoop(j, NUM_SRCS);
    for (loopVar_t niChannel = 0; niChannel < nChannelCount; niChannel++) {
      __hpvm__isNonZeroLoop(niChannel, EXP_CHANNELS);
      incrementBody(sumChannels, srcChannels, nChannelCount, nSamples, j,
                    niChannel);
    }
  }
  __hetero_task_end(sum_t_inner);

#ifdef GPU
    __hetero_section_end(sum_s);
    __hetero_task_end(sum_t);
#endif
#ifdef FPGA
    __hetero_section_end(wrapper_s);
    __hetero_task_end(wrapper_t);
#endif
  __hetero_section_end(root);
}

float* allocateChannelBuffer(unsigned nChannels, unsigned nSamples) 
{
  return (float*) __hetero_malloc(sizeof(float)*nChannels*nSamples);
}
  
  
void channelsToBuffer(float* dst, float** src, unsigned nChannels, unsigned nSamples) 
{
  for (int i = 0; i < nChannels; ++i) {
    size_t startIdx = i*nSamples;
    memcpy(&dst[startIdx], src[i], sizeof(float)*nSamples);
  }
}

void bufferToChannels(float** dst, float* src, unsigned nChannels, unsigned nSamples) 
{
  for (int i = 0; i < nChannels; ++i) {
    size_t startIdx = i*nSamples;
    memcpy(dst[i], &src[startIdx], sizeof(float)*nSamples);
  }
}

#ifdef REPORT_POWER
typedef struct { double time;
#ifdef CPU
                 double energy;
#endif
#ifdef GPU
                 double power;
#endif
} performance_t;
#else
typedef float performance_t;
#endif

// This is the function launch for the encoding process
performance_t encodeProcess(ILLIXR_AUDIO::ABAudio* audioAddr
#ifdef REPORT_POWER
#ifdef CPU
, int core
#endif
#endif
) {

    assert(audioAddr->soundSrcs->size() == NUM_SRCS);
    int soundSrcsSize = NUM_SRCS;
        
    CBFormat* sumBF = new CBFormat;
    sumBF->Configure(NORDER, true, BLOCK_SIZE);

    assert(sumBF->m_nChannelCount == EXP_CHANNELS);
    int nChannelCount = EXP_CHANNELS;
    int nSamples = sumBF->m_nSamples;
    assert(nSamples == BLOCK_SIZE);

    short* sampleTemp = (short*) __hetero_malloc(sizeof(short) * BLOCK_SIZE);
    float* sumChannels = allocateChannelBuffer(nChannelCount, nSamples);
    float* srcChannels = allocateChannelBuffer(soundSrcsSize*nChannelCount, nSamples);
    float* srcSamples = (float*) __hetero_malloc(sizeof(float)*BLOCK_SIZE*soundSrcsSize);
    float* srcAmps = (float*) __hetero_malloc(sizeof(float)*soundSrcsSize);
    float* srcCoeffs = (float*) __hetero_malloc(sizeof(float)*soundSrcsSize*nChannelCount);
    float* srcDelayBuffer = nullptr; // Allocated on first processed sound
    EncIn* srcInputs = (EncIn*) __hetero_malloc(sizeof(EncIn)*soundSrcsSize);
    EncInOut* srcInOuts = (EncInOut*) __hetero_malloc(sizeof(EncInOut)*soundSrcsSize);

    size_t bytes_sampleTemp = BLOCK_SIZE*sizeof(short);
    size_t bytes_sumChannels = nChannelCount*nSamples*sizeof(float);
    size_t bytes_srcChannels = soundSrcsSize*bytes_sumChannels;
    size_t bytes_srcSamples = BLOCK_SIZE*soundSrcsSize*sizeof(float);
    size_t bytes_srcAmps = soundSrcsSize*sizeof(float);
    size_t bytes_srcCoeffs = soundSrcsSize*nChannelCount*sizeof(float);
    size_t bytes_srcDelayBuffer = 0; // Assigned when srcDelayBuffer is alloced
    size_t bytes_srcInputs = soundSrcsSize*sizeof(EncIn);
    size_t bytes_srcInOuts = soundSrcsSize*sizeof(EncInOut);

    std::cout << "BLOCK_SIZE: " << BLOCK_SIZE << std::endl;
    std::cout << "nSamples: " << nSamples << std::endl;
    std::cout << "nChannelCount: " << nChannelCount << std::endl;
    std::cout << "soundSrcsSize: " << soundSrcsSize << std::endl;

    channelsToBuffer(sumChannels, sumBF->m_ppfChannels.get(), nChannelCount, nSamples);

    // The read-in process
    int nDelayBufferLength = -1;
    for (int j = 0; j < soundSrcsSize; ++j) {
      ILLIXR_AUDIO::Sound* src = (*(audioAddr->soundSrcs))[j];
      src->srcFile->read((char*)sampleTemp, BLOCK_SIZE * sizeof(short));
      assert(src->BFormat->m_nChannelCount == nChannelCount);
      assert(src->BFormat->m_nSamples == nSamples);
      if (nDelayBufferLength == -1) {
        nDelayBufferLength = src->BEncoder->m_nDelayBufferLength;
        srcDelayBuffer = (float*) __hetero_malloc(sizeof(float)*soundSrcsSize*nDelayBufferLength);
        bytes_srcDelayBuffer = soundSrcsSize*nDelayBufferLength*sizeof(float);
      } else {
        assert(src->BEncoder->m_nDelayBufferLength == nDelayBufferLength);
      }

      channelsToBuffer(&srcChannels[j*nChannelCount*nSamples], src->BFormat->m_ppfChannels.get(),
        nChannelCount, nSamples);
      memcpy(&srcSamples[BLOCK_SIZE*j], &src->sample[0], sizeof(float)*BLOCK_SIZE);
      srcAmps[j] = src->amp;
      memcpy(&srcCoeffs[j*nChannelCount], src->BEncoder->m_pfCoeff.data(), sizeof(float)*nChannelCount);
      memcpy(&srcDelayBuffer[j*nDelayBufferLength], src->BEncoder->m_pfDelayBuffer, sizeof(float)*nDelayBufferLength);

      EncIn& inputs = srcInputs[j];
      inputs.fDelay = src->BEncoder->m_fDelay;
      inputs.fInteriorGain = src->BEncoder->m_fInteriorGain;
      inputs.fExteriorGain = src->BEncoder->m_fExteriorGain;
      inputs.nDelayBufferLength = src->BEncoder->m_nDelayBufferLength;
      inputs.nChannelCount = src->BEncoder->m_nChannelCount;
      assert(inputs.nChannelCount == EXP_CHANNELS);

      EncInOut& inOuts = srcInOuts[j];
      inOuts.nIn = src->BEncoder->m_nIn;
      inOuts.nOutA = src->BEncoder->m_nOutA;
      inOuts.nOutB = src->BEncoder->m_nOutB;
    }

    printf("\n\nLaunching audio encoding pipeline!\n");
    printf("soundSrcsSize: %d\n", soundSrcsSize);

#ifdef REPORT_POWER
  #ifdef CPU
    double start_energy = GetEnergyCPU(core);
  #endif
  #ifdef GPU
    int flag = 0;
    pthread_t thd;
    pthread_create(&thd, NULL, measure_func, &flag);
    pthread_barrier_wait(&barrier);
  #endif
#endif

    auto startTime = std::chrono::steady_clock::now();
    auto dfg = __hetero_launch((void*) encoderPipelineHcc, 13,
      sumChannels, bytes_sumChannels,
      srcChannels, bytes_srcChannels,
      srcSamples, bytes_srcSamples,
      srcAmps, bytes_srcAmps,
      srcCoeffs, bytes_srcCoeffs,
      srcDelayBuffer, bytes_srcDelayBuffer,
      srcInputs, bytes_srcInputs,
      srcInOuts, bytes_srcInOuts,
      sampleTemp, bytes_sampleTemp,
      nDelayBufferLength, nChannelCount, nSamples, soundSrcsSize, 5,
      sumChannels, bytes_sumChannels,
      srcChannels, bytes_srcChannels,
      srcSamples, bytes_srcSamples,
      srcDelayBuffer, bytes_srcDelayBuffer,
      srcInOuts, bytes_srcInOuts
      );
    __hetero_wait(dfg);

    __hetero_request_mem(sumChannels, bytes_sumChannels);
    __hetero_request_mem(srcChannels, bytes_srcChannels);
    __hetero_request_mem(srcSamples,  bytes_srcSamples);
    __hetero_request_mem(srcDelayBuffer, bytes_srcDelayBuffer);
    __hetero_request_mem(srcInOuts,   bytes_srcInOuts);

    auto elapsed = std::chrono::steady_clock::now() - startTime;
#ifdef REPORT_POWER
  #ifdef CPU
    double end_energy = GetEnergyCPU(core);
  #endif
  #ifdef GPU
    flag = 1;
    pthread_join(thd, NULL);
  #endif
#endif

    printf("\n\nPipeline execution completed!\n");

    // Perform writes back
    bufferToChannels(sumBF->m_ppfChannels.get(), sumChannels, nChannelCount, nSamples);
    for (int j = 0; j < soundSrcsSize; ++j) {
      ILLIXR_AUDIO::Sound* src = (*(audioAddr->soundSrcs))[j];
      bufferToChannels(src->BFormat->m_ppfChannels.get(), &srcChannels[j*nChannelCount*nSamples],
        nChannelCount, nSamples);

      // Read-only
      // memcpy(&src->sample[0], &srcSamples[BLOCK_SIZE*j], sizeof(float)*BLOCK_SIZE);
      // srcAmps[j] = src->amp;
      // memcpy(src->BEncoder->m_pfCoeff.data(), &srcCoeffs[j*nChannelCount], sizeof(float)*nChannelCount);
      memcpy(src->BEncoder->m_pfDelayBuffer, &srcDelayBuffer[j*nDelayBufferLength], sizeof(float)*nDelayBufferLength);

      src->BEncoder->m_nIn = srcInOuts[j].nIn;
      src->BEncoder->m_nOutA = srcInOuts[j].nOutA;
      src->BEncoder->m_nOutB = srcInOuts[j].nOutB;
    }
    
    // Debug output
    printf("Average channels (every 10 samples)\n");
    for (int i = 0; i < nChannelCount; ++i) {
      for (int s = 0; s < nSamples; s += 10) {
        std::cout << sumBF->m_ppfChannels[i][s] << std::endl;
      }
      std::cout << std::endl;
    }


    __hetero_free(sampleTemp);
    __hetero_free(sumChannels);
    __hetero_free(srcChannels);
    __hetero_free(srcSamples);
    __hetero_free(srcAmps);
    __hetero_free(srcCoeffs);
    __hetero_free(srcInputs);
    __hetero_free(srcInOuts);

  #ifdef REPORT_POWER
    performance_t res;
    res.time = std::chrono::duration<float>(elapsed).count();
    #ifdef CPU
      res.energy = end_energy - start_energy;
    #endif
    #ifdef GPU
      res.power = measuredPower;
    #endif
    return res;
  #else
    return std::chrono::duration<float>(elapsed).count();
  #endif
}
}

int main(int argc, char const *argv[])
{
    auto mainStart = std::chrono::steady_clock::now();

    using namespace ILLIXR_AUDIO;

    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <number of size 1024 blocks to process>\n";
        return 1;
    }

    const int numBlocks = atoi(argv[1]);

    ABAudio audio("output.wav", ABAudio::ProcessType::ENCODE);
    audio.loadSource();

    ABAudio* audioAddr = &audio;

    float totalTime = 0.0;
#ifdef REPORT_POWER
  #ifdef CPU
    float totalEnergy = 0.0;
    // The GetEnergyCPU() implementation relies on us not switching processors
    // so to fascilitate this we pin ourselves to the current core, this is
    // also needed as it determines the processor we're running on
    int core = sched_getcpu();
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(core, &mask);
    if (sched_setaffinity(0, sizeof(mask), &mask)) {
      perror("Error in sched_setaffinity: ");
      exit(1);
    }
  #endif
  #ifdef GPU
    float totalPower = 0.0;
    pthread_barrier_init(&barrier, NULL, 2);
  #endif
#endif
    for (int i = 0; i < numBlocks; ++i) {
    #ifdef REPORT_POWER
      #ifdef CPU
        performance_t res = encodeProcess(audioAddr, core);
        totalTime += res.time;
        totalEnergy += res.energy;
      #endif
      #ifdef GPU
        performance_t res = encodeProcess(audioAddr);
        totalTime += res.time;
        totalPower += res.power;
      #endif
    #else
        totalTime += encodeProcess(audioAddr);
    #endif
    }

    auto totalElapsed = std::chrono::steady_clock::now() - mainStart;
    std::cout << "Kernel Execution took " << totalTime << " s" << std::endl;
    std::cout << "Total Execution took "
              << std::chrono::duration<float>(totalElapsed).count() << " s\n";
#ifdef REPORT_POWER
  #ifdef CPU
    printf("\nPower usage: %lf W\n", totalEnergy / totalTime);
    printf("Energy usage: %lf J\n", totalEnergy);
  #endif
  #ifdef GPU
    // Power usage is an average power usage, so divide by the number of blocks
    // to compute the average over all blocks
    printf("\nPower usage: %lf W\n", totalPower / numBlocks);
    printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower / numBlocks, totalTime * 1000));
  #endif
#endif
    return 0;
}
