# AudioEncoding Pipeline
This benchmark is part of [ILLIXR](https://github.com/ILLIXR/ILLIXR), the
Illinios Extended Reality Benchmark Suite. The audio pipeline is responsible
for both recording and playing back spatialized audio for XR. The version here
has been parallelized using HeteroC++.

## Building
AudioEncoding Pipeline can be built using the following command, where
`<target>` and `[flags]` are described below.

```sh
make TARGET=<target> [flags]
```

The currently supported `<target>` are: `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
This version of AudioEncoding Pipeline runs on the WAV files provided in
`samples/` and prints the results to stdout. It runs for a specified number
of blocks. The command to run is

```sh
./audioEncoding-<version> <number of blocks>
```

This will print the results to stdout. Small floating-point rounding errors
between different devices is common.

## Components

### libspatialaudio
Submodule libspatialaudio provides the backend library for Ambisonics encoding,
decoding, rotation, zoom, and binauralizer(HRTF included).

### audio pipeline code

#### sound.cpp 
Describes a sound source in the ambisonics sound-field, including the input
file for the sound source and its position in the sound-field.

#### audio.cpp
Encapsulate preset processing steps of sound source reading, encoding,
rotating, zooming, and decoding.

## License
This code is available under the University of Illinois/NCSA Open Source
License. The sound samples provided in ./sample/ are available under the
Creative Commons 0 license
