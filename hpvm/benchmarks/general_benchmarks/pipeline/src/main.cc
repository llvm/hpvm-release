/***************************************************************************
 *cr
 *cr            (C) Copyright 2010 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/*
 * Main entry of dense matrix-matrix multiplication kernel
 */

#include "opencv2/core/ocl.hpp"
#include "opencv2/opencv.hpp"
#include <cassert>
#include <chrono>
#include <hpvm.h>
#include <iostream>
#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define NUM_RUNS 1
#define DEPTH 3
#define HEIGHT 640
#define WIDTH 480

#define ROWS 1280
#define COLS 1280

#ifndef DEVICE
#error "The macro 'DEVICE' must be defined to CPU_TARGET or GPU_TARGET."
#endif

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection requires sudo
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE 1
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

std::string input_window = "GPU Pipeline - Input Video";
std::string output_window = "GPU Pipeline - Edge Mapping";
std::string cpu_output_window = "CPU Pipeline - Edge Mapping";

#ifdef MIDDLE
#define POSX_IN 640
#define POSY_IN 0
#define POSX_OUT 640
#define POSY_OUT 540

#elif RIGHT
#define POSX_IN 1280
#define POSY_IN 0
#define POSX_OUT 1280
#define POSY_OUT 540

#else // LEFT
#define POSX_IN 0
#define POSY_IN 0
#define POSX_OUT 0
#define POSY_OUT 540
#endif

//#define NUM_FRAMES 20

// Definitions of sizes for edge detection kernels

#define MIN_BR 0.0f
#define MAX_BR 1.0f

// Code needs to be changed for this to vary
#define SZB 3

#define REDUCTION_TILE_SZ 1024

#define _MIN(X, Y) ((X) < (Y) ? (X) : (Y))
#define _MAX(X, Y) ((X) > (Y) ? (X) : (Y))

const unsigned AOCL_ALIGNMENT = 64;
void *alignedMalloc(size_t size) {
  void *result = NULL;
  int rc;
  rc = posix_memalign(&result, AOCL_ALIGNMENT, size);
  return result;
}

bool compare_FPGA_CPU(float *in1, float *in2, int size) {
  bool mismatch = false;
  float epsilon = 0.00001;
  for (int i = 0; i < size; i++)
    if (abs(in1[i] - in2[i]) > epsilon) {
      // printf("Mismatch at %d: %f - %f = %f\n", i, in1[i], in2[i],
      //        in1[i] - in2[i]);
      mismatch = true;
    }
  return mismatch;
}

/*
 * Gaussian smoothing of image I of size m x n
 * I : input image
 * Gs : gaussian filter
 * Is: output (smoothed image)
 * m, n : dimensions
 *
 * Need 2D grid, a thread per pixel
 * No use of separable algorithm because we need to do this in one kernel
 * No use of shared memory because
 * - we don't handle it in the CPU pass
 */

#define GAUSSIAN_SIZE 7
#define GAUSSIAN_RADIUS (GAUSSIAN_SIZE / 2)
void gaussianSmoothing_cpu(float *__restrict__ I, size_t bytesI,
                           float *__restrict__ Gs, size_t bytesGs,
                           float *__restrict__ Is, size_t bytesIs, long m,
                           long n) {

  for (int gy = 0; gy < m; ++gy) {
    for (int gx = 0; gx < n; ++gx) {
      int gloc = gx + gy * n;

      float smoothedVal = 0;
      float gval;
      int loadOffset;

      for (int i = -GAUSSIAN_RADIUS; i <= GAUSSIAN_RADIUS; i++)
        for (int j = -GAUSSIAN_RADIUS; j <= GAUSSIAN_RADIUS; j++) {

          loadOffset = gloc + i * n + j;

          if ((gy + i) < 0) // top contour
            loadOffset = gx + j;
          else if ((gy + i) > m - 1) // bottom contour
            loadOffset = (m - 1) * n + gx + j;
          else
            loadOffset = gloc + i * n + j; // within image vertically

          // Adjust so we are within image horizonally
          if ((gx + j) < 0) // left contour
            loadOffset -= (gx + j);
          else if ((gx + j) > n - 1) // right contour
            loadOffset = loadOffset - gx - j + n - 1;

          gval = I[loadOffset];
          smoothedVal +=
              gval *
              Gs[(GAUSSIAN_RADIUS + i) * GAUSSIAN_SIZE + GAUSSIAN_RADIUS + j];
        }

      Is[gloc] = smoothedVal;
    }
  }
}

/* Compute a non-linear laplacian estimate of input image I of size m x n */
/*
 * Is   : blurred imput image
 * m, n : dimensions
 * B    : structural element for dilation - erosion ([0 1 0; 1 1 1; 0 1 0])
 * L    : output (laplacian of the image)
 * Need 2D grid, a thread per pixel
 */
void laplacianEstimate_cpu(float *__restrict__ Is, size_t bytesIs,
                           float *__restrict__ B, size_t bytesB,
                           float *__restrict__ L, size_t bytesL, long m,
                           long n) {

  for (int gy = 0; gy < m; ++gy) {
    for (int gx = 0; gx < n; ++gx) {
      float imageArea[SZB * SZB];
      int i, j;

      // Data copy for dilation filter
      imageArea[1 * SZB + 1] = Is[gy * n + gx];

      if (gx == 0) {
        imageArea[0 * SZB + 0] = imageArea[1 * SZB + 0] =
            imageArea[2 * SZB + 0] = MIN_BR;
      } else {
        imageArea[1 * SZB + 0] = Is[gy * n + gx - 1];
        imageArea[0 * SZB + 0] = (gy > 0) ? Is[(gy - 1) * n + gx - 1] : MIN_BR;
        imageArea[2 * SZB + 0] =
            (gy < m - 1) ? Is[(gy + 1) * n + gx - 1] : MIN_BR;
      }

      if (gx == n - 1) {
        imageArea[0 * SZB + 2] = imageArea[1 * SZB + 2] =
            imageArea[2 * SZB + 2] = MIN_BR;
      } else {
        imageArea[1 * SZB + 2] = Is[gy * n + gx + 1];
        imageArea[0 * SZB + 2] = (gy > 0) ? Is[(gy - 1) * n + gx + 1] : MIN_BR;
        imageArea[2 * SZB + 2] =
            (gy < m - 1) ? Is[(gy + 1) * n + gx + 1] : MIN_BR;
      }

      imageArea[0 * SZB + 1] = (gy > 0) ? Is[(gy - 1) * n + gx] : MIN_BR;
      imageArea[2 * SZB + 1] = (gy < m - 1) ? Is[(gy + 1) * n + gx] : MIN_BR;

      // Compute pixel of dilated image
      float dilatedPixel = MIN_BR;
      for (i = 0; i < SZB; i++)
        for (j = 0; j < SZB; j++)
          dilatedPixel =
              _MAX(dilatedPixel, imageArea[i * SZB + j] * B[i * SZB + j]);

      // Data copy for erotion filter - only change the boundary conditions
      if (gx == 0) {
        imageArea[0 * SZB + 0] = imageArea[1 * SZB + 0] =
            imageArea[2 * SZB + 0] = MAX_BR;
      } else {
        if (gy == 0)
          imageArea[0 * SZB + 0] = MAX_BR;
        if (gy == m - 1)
          imageArea[2 * SZB + 0] = MAX_BR;
      }

      if (gx == n - 1) {
        imageArea[0 * SZB + 2] = imageArea[1 * SZB + 2] =
            imageArea[2 * SZB + 2] = MAX_BR;
      } else {
        if (gy == 0)
          imageArea[0 * SZB + 2] = MAX_BR;
        if (gy == m - 1)
          imageArea[2 * SZB + 2] = MAX_BR;
      }

      if (gy == 0)
        imageArea[0 * SZB + 1] = MAX_BR;
      if (gy == m - 1)
        imageArea[2 * SZB + 1] = MAX_BR;

      // Compute pixel of eroded image
      float erodedPixel = MAX_BR;
      for (i = 0; i < SZB; i++)
        for (j = 0; j < SZB; j++)
          erodedPixel =
              _MIN(erodedPixel, imageArea[i * SZB + j] * B[i * SZB + j]);

      float laplacian = dilatedPixel + erodedPixel - 2 * imageArea[1 * SZB + 1];
      L[gy * n + gx] = laplacian;
    }
  }
}

/* Compute the zero crossings of input image L of size m x n */
/*
 * L    : imput image (computed Laplacian)
 * m, n : dimensions
 * B    : structural element for dilation - erosion ([0 1 0; 1 1 1; 0 1 0])
 * S    : output (sign of the image)
 * Need 2D grid, a thread per pixel
 */
void computeZeroCrossings_cpu(float *__restrict__ L, size_t bytesL,
                              float *__restrict__ B, size_t bytesB,
                              float *__restrict__ S, size_t bytesS, long m,
                              long n) {

  for (int gy = 0; gy < m; ++gy) {
    for (int gx = 0; gx < n; ++gx) {
      int i, j;
      // 3x3 image area
      float imageArea[SZB][SZB];
      // Data copy for dilation filter
      imageArea[1][1] = L[gy * n + gx] > MIN_BR ? MAX_BR : MIN_BR;

      if (gx == 0) { // left most line
        imageArea[0][0] = imageArea[1][0] = imageArea[2][0] = MIN_BR;
      } else {
        imageArea[1][0] = L[gy * n + gx - 1] > MIN_BR ? MAX_BR : MIN_BR;
        imageArea[0][0] =
            (gy > 0) ? (L[(gy - 1) * n + gx - 1] > MIN_BR ? MAX_BR : MIN_BR)
                     : MIN_BR;
        imageArea[2][0] =
            (gy < m - 1) ? (L[(gy + 1) * n + gx - 1] > MIN_BR ? MAX_BR : MIN_BR)
                         : MIN_BR;
      }

      if (gx == n - 1) {
        imageArea[0][2] = imageArea[1][2] = imageArea[2][2] = MIN_BR;
      } else {
        imageArea[1][2] = L[gy * n + gx + 1] > MIN_BR ? MAX_BR : MIN_BR;
        imageArea[0][2] =
            (gy > 0) ? (L[(gy - 1) * n + gx + 1] > MIN_BR ? MAX_BR : MIN_BR)
                     : MIN_BR;
        imageArea[2][2] =
            (gy < m - 1) ? (L[(gy + 1) * n + gx + 1] > MIN_BR ? MAX_BR : MIN_BR)
                         : MIN_BR;
      }

      imageArea[0][1] =
          (gy > 0) ? (L[(gy - 1) * n + gx] > MIN_BR ? MAX_BR : MIN_BR) : MIN_BR;
      imageArea[2][1] = (gy < m - 1)
                            ? (L[(gy + 1) * n + gx] > MIN_BR ? MAX_BR : MIN_BR)
                            : MIN_BR;

      // Compute pixel of dilated image
      float dilatedPixel = MIN_BR;
      for (i = 0; i < SZB; i++)
        for (j = 0; j < SZB; j++)
          dilatedPixel = _MAX(dilatedPixel, imageArea[i][j] * B[i * SZB + j]);

      // Data copy for erotion filter - only change the boundary conditions
      if (gx == 0) {
        imageArea[0][0] = imageArea[1][0] = imageArea[2][0] = MAX_BR;
      } else {
        if (gy == 0)
          imageArea[0][0] = MAX_BR;
        if (gy == m - 1)
          imageArea[2][0] = MAX_BR;
      }

      if (gx == n - 1) {
        imageArea[0][2] = imageArea[1][2] = imageArea[2][2] = MAX_BR;
      } else {
        if (gy == 0)
          imageArea[0][2] = MAX_BR;
        if (gy == m - 1)
          imageArea[2][2] = MAX_BR;
      }

      if (gy == 0)
        imageArea[0][1] = MAX_BR;
      if (gy == m - 1)
        imageArea[2][1] = MAX_BR;

      // Compute pixel of eroded image
      float erodedPixel = MAX_BR;
      for (i = 0; i < SZB; i++)
        for (j = 0; j < SZB; j++)
          erodedPixel = _MIN(erodedPixel, imageArea[i][j] * B[i * SZB + j]);

      float pixelSign = dilatedPixel - erodedPixel;
      S[gy * n + gx] = pixelSign;
    }
  }
}

/*
 * Gradient computation using Sobel filters
 * Is   : input (smoothed image)
 * Sx, Sy: Sobel operators
 * - Sx = [-1  0  1 ; -2 0 2 ; -1 0 1 ]
 * - Sy = [-1 -2 -1 ;  0 0 0 ;  1 2 1 ]
 * m, n : dimensions
 * G: output, gradient magnitude : sqrt(Gx^2+Gy^2)
 * Need 2D grid, a thread per pixel
 * No use of separable algorithm because we need to do this in one kernel
 * No use of shared memory because
 * - we don't handle it in the CPU pass
 */

#define SOBEL_SIZE 3
#define SOBEL_RADIUS (SOBEL_SIZE / 2)

void computeGradient_cpu(float *__restrict__ Is, size_t bytesIs,
                         float *__restrict__ Sx, size_t bytesSx,
                         float *__restrict__ Sy, size_t bytesSy,
                         float *__restrict__ G, size_t bytesG, long m, long n) {

  for (int gy = 0; gy < m; ++gy) {
    for (int gx = 0; gx < n; ++gx) {

      int gloc = gx + gy * n;

      float Gx = 0;
      float Gy = 0;
      float gval;
      int loadOffset;

      for (int i = -SOBEL_RADIUS; i <= SOBEL_RADIUS; i++)
        for (int j = -SOBEL_RADIUS; j <= SOBEL_RADIUS; j++) {

          loadOffset = gloc + i * n + j;

          if ((gy + i) < 0) // top contour
            loadOffset = gx + j;
          else if ((gy + i) > m - 1) // bottom contour
            loadOffset = (m - 1) * n + gx + j;
          else
            loadOffset = gloc + i * n + j; // within image vertically

          // Adjust so we are within image horizonally
          if ((gx + j) < 0) // left contour
            loadOffset -= (gx + j);
          else if ((gx + j) > n - 1) // right contour
            loadOffset = loadOffset - gx - j + n - 1;

          gval = Is[loadOffset];
          Gx += gval * Sx[(SOBEL_RADIUS + i) * SOBEL_SIZE + SOBEL_RADIUS + j];
          Gy += gval * Sy[(SOBEL_RADIUS + i) * SOBEL_SIZE + SOBEL_RADIUS + j];
        }

      G[gloc] = sqrt(Gx * Gx + Gy * Gy);
    }
  }
}

/*
 * Reduction
 * G : input
 * maxG: output
 * m, n: input size
 * Needs a single thread block
 */
void computeMaxGradient_cpu(float *__restrict__ G, size_t bytesG,
                            float *__restrict__ maxG, size_t bytesMaxG, long m,
                            long n) {

  int max = G[0];
  for (int i = 1; i < m * n; i++) {
    if (max < G[i])
      max = G[i];
  }
  *maxG = max;
}

/* Reject the zero crossings where the gradient is below a threshold */
/*
 * S    : input (computed zero crossings)
 * m, n : dimensions
 * G: gradient of (smoothed) image
 * E    : output (edges of the image)
 * Need 2D grid, a thread per pixel
 */

#define THETA 0.1
void rejectZeroCrossings_cpu(float *__restrict__ S, size_t bytesS,
                             float *__restrict__ G, size_t bytesG,
                             float *__restrict__ maxG, size_t bytesMaxG,
                             float *__restrict__ E, size_t bytesE, long m,
                             long n) {
  float mG = *maxG;
  for (long gy = 0; gy < m; ++gy) {
    for (long gx = 0; gx < n; ++gx) {
      E[gy * n + gx] =
          ((S[gy * n + gx] > 0.0) && (G[gy * n + gx] > THETA * mG)) ? 1.0 : 0.0;
    }
  }
}

template <typename T> void printArray(T *A, int x, int y = 1) {
  std::cout << "Printing Array: " << std::endl;
  for (int i = 0; i < y; ++i) {
    for (int j = 0; j < x; ++j) {
      std::cout << A[i * x + j] << "  ";
    }
    std::cout << std::endl;
  }
}
extern "C" {

struct __attribute__((__packed__)) OutStruct {
  size_t ret;
};

struct __attribute__((__packed__)) InStruct {
  float *I;
  size_t bytesI;
  float *Is;
  size_t bytesIs;
  float *L;
  size_t bytesL;
  float *S;
  size_t bytesS;
  float *G;
  size_t bytesG;
  float *maxG;
  size_t bytesMaxG;
  float *E;
  size_t bytesE;
  float *Gs;
  size_t bytesGs;
  float *B;
  size_t bytesB;
  float *Sx;
  size_t bytesSx;
  float *Sy;
  size_t bytesSy;
  long m;
  long n;
  long block_x;
  long grid_x;
};

void packData(struct InStruct *args, float *I, size_t bytesI, float *Is,
              size_t bytesIs, float *L, size_t bytesL, float *S, size_t bytesS,
              float *G, size_t bytesG, float *maxG, size_t bytesMaxG, float *E,
              size_t bytesE, float *Gs, size_t bytesGs, float *B, size_t bytesB,
              float *Sx, size_t bytesSx, float *Sy, size_t bytesSy, long m,
              long n) {
  args->I = I;
  args->bytesI = bytesI;
  args->Is = Is;
  args->bytesIs = bytesIs;
  args->L = L;
  args->bytesL = bytesL;
  args->S = S;
  args->bytesS = bytesS;
  args->G = G;
  args->bytesG = bytesG;
  args->maxG = maxG;
  args->bytesMaxG = bytesMaxG;
  args->E = E;
  args->bytesE = bytesE;
  args->Gs = Gs;
  args->bytesGs = bytesGs;
  args->B = B;
  args->bytesB = bytesB;
  args->Sx = Sx;
  args->bytesSx = bytesSx;
  args->Sy = Sy;
  args->bytesSy = bytesSy;
  args->m = m;
  args->n = n;
}

/*
 * Gaussian smoothing of image I of size m x n
 * I : input image
 * Gs : gaussian filter
 * Is: output (smoothed image)
 * m, n : dimensions
 *
 * Need 2D grid, a thread per pixel
 * No use of separable algorithm because we need to do this in one kernel
 * No use of shared memory because
 * - we don't handle it in the CPU pass
 */

#define GAUSSIAN_SIZE 7
#define GAUSSIAN_RADIUS (GAUSSIAN_SIZE / 2)
void gaussianSmoothing(float *__restrict__ I, size_t bytesI,
                       float *__restrict__ Gs, size_t bytesGs,
                       float *__restrict__ Is,
                       size_t bytesIs /*, long m, long n*/) {

  __hpvm__hint(hpvm::DEVICE);
  __hpvm__attributes(2, I, Gs, 1, Is);

  void *thisNode = __hpvm__getNode();
  long row = __hpvm__getNodeInstanceID_x(thisNode);
  long col = __hpvm__getNodeInstanceID_y(thisNode);
  __hpvm__isNonZeroLoop(row, ROWS);
  __hpvm__isNonZeroLoop(col, COLS);

  long m = __hpvm__getNumNodeInstances_x(thisNode);
  long n = __hpvm__getNumNodeInstances_y(thisNode);

  int gloc = col + row * n;

  float smoothedVal = 0;
  float gval;
  int loadOffset;

  // if ((col < n) && (row < m)) {
  for (int i = -GAUSSIAN_RADIUS; i <= GAUSSIAN_RADIUS; i++)
    for (int j = -GAUSSIAN_RADIUS; j <= GAUSSIAN_RADIUS; j++) {

      loadOffset = gloc + i * n + j;

      if ((row + i) < 0) // top contour
        loadOffset = col + j;
      else if ((row + i) > m - 1) // bottom contour
        loadOffset = (m - 1) * n + col + j;
      else
        loadOffset = gloc + i * n + j; // within image vertically

      // Adjust so we are within image horizonally
      if ((col + j) < 0) // left contour
        loadOffset -= (col + j);
      else if ((col + j) > n - 1) // right contour
        loadOffset = loadOffset - col - j + n - 1;

      gval = I[loadOffset];
      smoothedVal +=
          gval *
          Gs[(GAUSSIAN_RADIUS + i) * GAUSSIAN_SIZE + GAUSSIAN_RADIUS + j];
    }

  Is[gloc] = smoothedVal;
  //  }
  __hpvm__return(2, Is, Is);
}

#ifdef GPU
void WrapperGaussianSmoothing(float *I, size_t bytesI, float *Gs,
                              size_t bytesGs, float *Is, size_t bytesIs, long m,
                              long n) {
  __hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(2, I, Gs, 1, Is);
  void *GSNode = __hpvm__createNodeND(2, gaussianSmoothing, m, n);
  __hpvm__bindIn(GSNode, 0, 0, 0); // Bind I
  __hpvm__bindIn(GSNode, 1, 1, 0); // Bind bytesI
  __hpvm__bindIn(GSNode, 2, 2, 0); // Bind Gs
  __hpvm__bindIn(GSNode, 3, 3, 0); // Bind bytesGs
  __hpvm__bindIn(GSNode, 4, 4, 0); // Bind Is
  __hpvm__bindIn(GSNode, 5, 5, 0); // Bind bytesIs

  __hpvm__bindOut(GSNode, 0, 0, 0); // bind output bytesIs
  __hpvm__bindOut(GSNode, 1, 1, 0); // bind output bytesIs
}
#endif

/* Compute a non-linear laplacian estimate of input image I of size m x n */
/*
 * Is   : blurred imput image
 * m, n : dimensions
 * B    : structural element for dilation - erosion ([0 1 0; 1 1 1; 0 1 0])
 * L    : output (laplacian of the image)
 * Need 2D grid, a thread per pixel
 */
void laplacianEstimate(float *__restrict__ Is, size_t bytesIs,
                       float *__restrict__ B, size_t bytesB,
                       float *__restrict__ L, size_t bytesL) {

  __hpvm__hint(hpvm::DEVICE);
  __hpvm__attributes(2, Is, B, 1, L);
  // 3x3 image area
  float imageArea[SZB * SZB];

  void *thisNode = __hpvm__getNode();
  long row = __hpvm__getNodeInstanceID_x(thisNode);
  long col = __hpvm__getNodeInstanceID_y(thisNode);
  __hpvm__isNonZeroLoop(row, ROWS);
  __hpvm__isNonZeroLoop(col, COLS);

  long m = __hpvm__getNumNodeInstances_x(thisNode);
  long n = __hpvm__getNumNodeInstances_y(thisNode);
  int i, j;

  // Data copy for dilation filter
  imageArea[1 * SZB + 1] = Is[row * n + col];

  if (col == 0) {
    imageArea[0 * SZB + 0] = imageArea[1 * SZB + 0] = imageArea[2 * SZB + 0] =
        MIN_BR;
  } else {
    imageArea[1 * SZB + 0] = Is[row * n + col - 1];
    imageArea[0 * SZB + 0] = (row > 0) ? Is[(row - 1) * n + col - 1] : MIN_BR;
    imageArea[2 * SZB + 0] =
        (row < m - 1) ? Is[(row + 1) * n + col - 1] : MIN_BR;
  }

  if (col == n - 1) {
    imageArea[0 * SZB + 2] = imageArea[1 * SZB + 2] = imageArea[2 * SZB + 2] =
        MIN_BR;
  } else {
    imageArea[1 * SZB + 2] = Is[row * n + col + 1];
    imageArea[0 * SZB + 2] = (row > 0) ? Is[(row - 1) * n + col + 1] : MIN_BR;
    imageArea[2 * SZB + 2] =
        (row < m - 1) ? Is[(row + 1) * n + col + 1] : MIN_BR;
  }

  imageArea[0 * SZB + 1] = (row > 0) ? Is[(row - 1) * n + col] : MIN_BR;
  imageArea[2 * SZB + 1] = (row < m - 1) ? Is[(row + 1) * n + col] : MIN_BR;

  // Compute pixel of dilated image
  float dilatedPixel = MIN_BR;
  for (i = 0; i < SZB; i++)
    for (j = 0; j < SZB; j++)
      dilatedPixel =
          _MAX(dilatedPixel, imageArea[i * SZB + j] * B[i * SZB + j]);

  // Data copy for erotion filter - only change the boundary conditions
  if (col == 0) {
    imageArea[0 * SZB + 0] = imageArea[1 * SZB + 0] = imageArea[2 * SZB + 0] =
        MAX_BR;
  } else {
    if (row == 0)
      imageArea[0 * SZB + 0] = MAX_BR;
    if (row == m - 1)
      imageArea[2 * SZB + 0] = MAX_BR;
  }

  if (col == n - 1) {
    imageArea[0 * SZB + 2] = imageArea[1 * SZB + 2] = imageArea[2 * SZB + 2] =
        MAX_BR;
  } else {
    if (row == 0)
      imageArea[0 * SZB + 2] = MAX_BR;
    if (row == m - 1)
      imageArea[2 * SZB + 2] = MAX_BR;
  }

  if (row == 0)
    imageArea[0 * SZB + 1] = MAX_BR;
  if (row == m - 1)
    imageArea[2 * SZB + 1] = MAX_BR;

  // Compute pixel of eroded image
  float erodedPixel = MAX_BR;
  for (i = 0; i < SZB; i++)
    for (j = 0; j < SZB; j++)
      erodedPixel = _MIN(erodedPixel, imageArea[i * SZB + j] * B[i * SZB + j]);

  float laplacian = dilatedPixel + erodedPixel - 2 * imageArea[1 * SZB + 1];
  L[row * n + col] = laplacian;

  __hpvm__return(1, L);
}

#ifdef GPU
void WrapperlaplacianEstimate(float *Is, size_t bytesIs, float *B,
                              size_t bytesB, float *L, size_t bytesL, long m,
                              long n) {
  __hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(2, Is, B, 1, L);
  void *LNode = __hpvm__createNodeND(2, laplacianEstimate, m, n);
  __hpvm__bindIn(LNode, 0, 0, 0); // Bind Is
  __hpvm__bindIn(LNode, 1, 1, 0); // Bind bytesIs
  __hpvm__bindIn(LNode, 2, 2, 0); // Bind B
  __hpvm__bindIn(LNode, 3, 3, 0); // Bind bytesB
  __hpvm__bindIn(LNode, 4, 4, 0); // Bind L
  __hpvm__bindIn(LNode, 5, 5, 0); // Bind bytesL

  __hpvm__bindOut(LNode, 0, 0, 0); // bind output bytesL
}
#endif

/* Compute the zero crossings of input image L of size m x n */
/*
 * L    : imput image (computed Laplacian)
 * m, n : dimensions
 * B    : structural element for dilation - erosion ([0 1 0; 1 1 1; 0 1 0])
 * S    : output (sign of the image)
 * Need 2D grid, a thread per pixel
 */
void computeZeroCrossings(float *__restrict__ L, size_t bytesL,
                          float *__restrict__ B, size_t bytesB,
                          float *__restrict__ S, size_t bytesS) {
  __hpvm__hint(hpvm::DEVICE);
  //__hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(2, L, B, 1, S);

  // 3x3 image area
  float imageArea[SZB][SZB];

  void *thisNode = __hpvm__getNode();
  long row = __hpvm__getNodeInstanceID_x(thisNode);
  long col = __hpvm__getNodeInstanceID_y(thisNode);
  __hpvm__isNonZeroLoop(row, ROWS);
  __hpvm__isNonZeroLoop(col, COLS);

  long m = __hpvm__getNumNodeInstances_x(thisNode);
  long n = __hpvm__getNumNodeInstances_y(thisNode);
  int i, j;

  // Data copy for dilation filter
  imageArea[1][1] = L[row * n + col] > MIN_BR ? MAX_BR : MIN_BR;

  if (col == 0) { // left most line
    imageArea[0][0] = imageArea[1][0] = imageArea[2][0] = MIN_BR;
  } else {
    imageArea[1][0] = L[row * n + col - 1] > MIN_BR ? MAX_BR : MIN_BR;
    imageArea[0][0] =
        (row > 0) ? (L[(row - 1) * n + col - 1] > MIN_BR ? MAX_BR : MIN_BR)
                  : MIN_BR;
    imageArea[2][0] =
        (row < m - 1) ? (L[(row + 1) * n + col - 1] > MIN_BR ? MAX_BR : MIN_BR)
                      : MIN_BR;
  }

  if (col == n - 1) {
    imageArea[0][2] = imageArea[1][2] = imageArea[2][2] = MIN_BR;
  } else {
    imageArea[1][2] = L[row * n + col + 1] > MIN_BR ? MAX_BR : MIN_BR;
    imageArea[0][2] =
        (row > 0) ? (L[(row - 1) * n + col + 1] > MIN_BR ? MAX_BR : MIN_BR)
                  : MIN_BR;
    imageArea[2][2] =
        (row < m - 1) ? (L[(row + 1) * n + col + 1] > MIN_BR ? MAX_BR : MIN_BR)
                      : MIN_BR;
  }

  imageArea[0][1] =
      (row > 0) ? (L[(row - 1) * n + col] > MIN_BR ? MAX_BR : MIN_BR) : MIN_BR;
  imageArea[2][1] = (row < m - 1)
                        ? (L[(row + 1) * n + col] > MIN_BR ? MAX_BR : MIN_BR)
                        : MIN_BR;

  // Compute pixel of dilated image
  float dilatedPixel = MIN_BR;
  for (i = 0; i < SZB; i++)
    for (j = 0; j < SZB; j++)
      dilatedPixel = _MAX(dilatedPixel, imageArea[i][j] * B[i * SZB + j]);

  // Data copy for erotion filter - only change the boundary conditions
  if (col == 0) {
    imageArea[0][0] = imageArea[1][0] = imageArea[2][0] = MAX_BR;
  } else {
    if (row == 0)
      imageArea[0][0] = MAX_BR;
    if (row == m - 1)
      imageArea[2][0] = MAX_BR;
  }

  if (col == n - 1) {
    imageArea[0][2] = imageArea[1][2] = imageArea[2][2] = MAX_BR;
  } else {
    if (row == 0)
      imageArea[0][2] = MAX_BR;
    if (row == m - 1)
      imageArea[2][2] = MAX_BR;
  }

  if (row == 0)
    imageArea[0][1] = MAX_BR;
  if (row == m - 1)
    imageArea[2][1] = MAX_BR;

  // Compute pixel of eroded image
  float erodedPixel = MAX_BR;
  for (i = 0; i < SZB; i++)
    for (j = 0; j < SZB; j++)
      erodedPixel = _MIN(erodedPixel, imageArea[i][j] * B[i * SZB + j]);

  float pixelSign = dilatedPixel - erodedPixel;
  S[row * n + col] = pixelSign;

  __hpvm__return(1, S);
}

#ifdef GPU
void WrapperComputeZeroCrossings(float *L, size_t bytesL, float *B,
                                 size_t bytesB, float *S, size_t bytesS, long m,
                                 long n) {
  __hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(2, L, B, 1, S);
  void *ZCNode = __hpvm__createNodeND(2, computeZeroCrossings, m, n);
  __hpvm__bindIn(ZCNode, 0, 0, 0); // Bind L
  __hpvm__bindIn(ZCNode, 1, 1, 0); // Bind bytesL
  __hpvm__bindIn(ZCNode, 2, 2, 0); // Bind B
  __hpvm__bindIn(ZCNode, 3, 3, 0); // Bind bytesB
  __hpvm__bindIn(ZCNode, 4, 4, 0); // Bind S
  __hpvm__bindIn(ZCNode, 5, 5, 0); // Bind bytesS

  __hpvm__bindOut(ZCNode, 0, 0, 0); // bind output bytesS
}
#endif

/*
 * Gradient computation using Sobel filters
 * Is   : input (smoothed image)
 * Sx, Sy: Sobel operators
 * - Sx = [-1  0  1 ; -2 0 2 ; -1 0 1 ]
 * - Sy = [-1 -2 -1 ;  0 0 0 ;  1 2 1 ]
 * m, n : dimensions
 * G: output, gradient magnitude : sqrt(col^2+row^2)
 * Need 2D grid, a thread per pixel
 * No use of separable algorithm because we need to do this in one kernel
 * No use of shared memory because
 * - we don't handle it in the CPU pass
 */

#define SOBEL_SIZE 3
#define SOBEL_RADIUS (SOBEL_SIZE / 2)

void computeGradient(float *__restrict__ Is, size_t bytesIs,
                     float *__restrict__ Sx, size_t bytesSx,
                     float *__restrict__ Sy, size_t bytesSy,
                     float *__restrict__ G, size_t bytesG) {

  __hpvm__hint(hpvm::DEVICE);
  __hpvm__attributes(3, Is, Sx, Sy, 1, G);

  void *thisNode = __hpvm__getNode();
  long row = __hpvm__getNodeInstanceID_x(thisNode);
  long col = __hpvm__getNodeInstanceID_y(thisNode);
  __hpvm__isNonZeroLoop(row, ROWS);
  __hpvm__isNonZeroLoop(col, COLS);

  long m = __hpvm__getNumNodeInstances_x(thisNode);
  long n = __hpvm__getNumNodeInstances_y(thisNode);

  int gloc = col + row * n;

  float GX = 0;
  float GY = 0;
  float gval;
  int loadOffset;

  for (int i = -SOBEL_RADIUS; i <= SOBEL_RADIUS; i++)
    for (int j = -SOBEL_RADIUS; j <= SOBEL_RADIUS; j++) {

      loadOffset = gloc + i * n + j;

      if ((row + i) < 0) // top contour
        loadOffset = col + j;
      else if ((row + i) > m - 1) // bottom contour
        loadOffset = (m - 1) * n + col + j;
      else
        loadOffset = gloc + i * n + j; // within image vertically

      // Adjust so we are within image horizonally
      if ((col + j) < 0) // left contour
        loadOffset -= (col + j);
      else if ((col + j) > n - 1) // right contour
        loadOffset = loadOffset - col - j + n - 1;

      gval = Is[loadOffset];
      GX += gval * Sx[(SOBEL_RADIUS + i) * SOBEL_SIZE + SOBEL_RADIUS + j];
      GY += gval * Sy[(SOBEL_RADIUS + i) * SOBEL_SIZE + SOBEL_RADIUS + j];
    }

  G[gloc] = sqrt(GX * GX + GY * GY);
  __hpvm__return(1, G);
}

#ifdef GPU
void WrapperComputeGradient(float *Is, size_t bytesIs, float *Sx,
                            size_t bytesSx, float *Sy, size_t bytesSy, float *G,
                            size_t bytesG, long m, long n) {
  __hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(3, Is, Sx, Sy, 1, G);
  void *CGNode = __hpvm__createNodeND(2, computeGradient, m, n);
  __hpvm__bindIn(CGNode, 0, 0, 0); // Bind Is
  __hpvm__bindIn(CGNode, 1, 1, 0); // Bind bytesIs
  __hpvm__bindIn(CGNode, 2, 2, 0); // Bind Sx
  __hpvm__bindIn(CGNode, 3, 3, 0); // Bind bytesSx
  __hpvm__bindIn(CGNode, 4, 4, 0); // Bind Sy
  __hpvm__bindIn(CGNode, 5, 5, 0); // Bind bytesSy
  __hpvm__bindIn(CGNode, 6, 6, 0); // Bind G
  __hpvm__bindIn(CGNode, 7, 7, 0); // Bind bytesG

  __hpvm__bindOut(CGNode, 0, 0, 0); // bind output bytesG
}
#endif

/*
 * Reduction
 * G : input
 * maxG: output
 * m, n: input size
 * Needs a single thread block
 */
void computeMaxGradient(float *__restrict__ G, size_t bytesG,
                        float *__restrict__ maxG, size_t bytesMaxG, long m,
                        long n) {

  __hpvm__hint(hpvm::DEVICE);
  __hpvm__attributes(1, G, 1, maxG);

  float maxVal = G[0];
  for (int i = 0; i < m * n; i++) {
    __hpvm__isNonZeroLoop(i, ROWS * COLS);
    if (G[i] > maxVal)
      maxVal = G[i];
  }

  *maxG = maxVal;

  __hpvm__return(1, maxG);
}

#ifdef GPU
void WrapperComputeMaxGradient(float *G, size_t bytesG, float *maxG,
                               size_t bytesMaxG, long m, long n) {
  __hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(2, G, maxG, 1, maxG);
  void *CMGTBNode = __hpvm__createNodeND(1, computeMaxGradient, (long)1);
  __hpvm__bindIn(CMGTBNode, 0, 0, 0); // Bind G
  __hpvm__bindIn(CMGTBNode, 1, 1, 0); // Bind bytesG
  __hpvm__bindIn(CMGTBNode, 2, 2, 0); // Bind maxG
  __hpvm__bindIn(CMGTBNode, 3, 3, 0); // Bind bytesMaxG
  __hpvm__bindIn(CMGTBNode, 4, 4, 0); // Bind m
  __hpvm__bindIn(CMGTBNode, 5, 5, 0); // Bind n

  __hpvm__bindOut(CMGTBNode, 0, 0, 0); // bind output bytesMaxG
}
#endif

/* Reject the zero crossings where the gradient is below a threshold */
/*
 * S    : input (computed zero crossings)
 * m, n : dimensions
 * G: gradient of (smoothed) image
 * E    : output (edges of the image)
 * Need 2D grid, a thread per pixel
 */

#define THETA 0.1
void rejectZeroCrossings(float *__restrict__ S, size_t bytesS,
                         float *__restrict__ G, size_t bytesG,
                         float *__restrict__ maxG, size_t bytesMaxG,
                         float *__restrict__ E, size_t bytesE) {
  __hpvm__hint(hpvm::DEVICE);
  __hpvm__attributes(3, S, G, maxG, 1, E);

  void *thisNode = __hpvm__getNode();
  long row = __hpvm__getNodeInstanceID_x(thisNode);
  long col = __hpvm__getNodeInstanceID_y(thisNode);
  __hpvm__isNonZeroLoop(row, ROWS);
  __hpvm__isNonZeroLoop(col, COLS);

  long m = __hpvm__getNumNodeInstances_x(thisNode);
  long n = __hpvm__getNumNodeInstances_y(thisNode);

  float mG = *maxG;

  E[row * n + col] =
      ((S[row * n + col] > 0.0) && (G[row * n + col] > THETA * mG)) ? 1.0 : 0.0;

  __hpvm__return(1, E);
}

#ifdef GPU
void WrapperRejectZeroCrossings(float *S, size_t bytesS, float *G,
                                size_t bytesG, float *maxG, size_t bytesMaxG,
                                float *E, size_t bytesE, long m, long n) {
  __hpvm__hint(hpvm::CPU_TARGET);
  __hpvm__attributes(3, S, G, maxG, 1, E);
  void *RZCNode = __hpvm__createNodeND(2, rejectZeroCrossings, m, n);
  __hpvm__bindIn(RZCNode, 0, 0, 0); // Bind S
  __hpvm__bindIn(RZCNode, 1, 1, 0); // Bind bytesS
  __hpvm__bindIn(RZCNode, 2, 2, 0); // Bind G
  __hpvm__bindIn(RZCNode, 3, 3, 0); // Bind bytesG
  __hpvm__bindIn(RZCNode, 4, 4, 0); // Bind maxG
  __hpvm__bindIn(RZCNode, 5, 5, 0); // Bind bytesMaxG
  __hpvm__bindIn(RZCNode, 6, 6, 0); // Bind E
  __hpvm__bindIn(RZCNode, 7, 7, 0); // Bind bytesE

  __hpvm__bindOut(RZCNode, 0, 0, 0); // bind output bytesE
}
#endif

#ifndef GPU
void DeviceLaunchNode(float *I, size_t bytesI,       // 0
                      float *Is, size_t bytesIs,     // 2
                      float *L, size_t bytesL,       // 4
                      float *S, size_t bytesS,       // 6
                      float *G, size_t bytesG,       // 8
                      float *maxG, size_t bytesMaxG, // 10
                      float *E, size_t bytesE,       // 12
                      float *Gs, size_t bytesGs,     // 14
                      float *B, size_t bytesB,       // 16
                      float *Sx, size_t bytesSx,     // 18
                      float *Sy, size_t bytesSy,     // 20
                      long m,                        // 22
                      long n                         // 23
) {
  __hpvm__attributes(5, I, Gs, B, Sx, Sy, 6, Is, L, S, G, maxG, E);
  __hpvm__hint(hpvm::CPU_TARGET);
  void *GSNode = __hpvm__createNodeND(2, gaussianSmoothing, m, n);
  void *LNode = __hpvm__createNodeND(2, laplacianEstimate, m, n);
  void *CZCNode = __hpvm__createNodeND(2, computeZeroCrossings, m, n);
  void *CGNode = __hpvm__createNodeND(2, computeGradient, m, n);
  void *CMGNode = __hpvm__createNodeND(1, computeMaxGradient, (long)1);
  void *RZCNode = __hpvm__createNodeND(2, rejectZeroCrossings, m, n);

  // Gaussian Inputs
  __hpvm__bindIn(GSNode, 0, 0, 0);  // Bind I
  __hpvm__bindIn(GSNode, 1, 1, 0);  // Bind bytesI
  __hpvm__bindIn(GSNode, 14, 2, 0); // Bind Gs
  __hpvm__bindIn(GSNode, 15, 3, 0); // Bind bytesGs
  __hpvm__bindIn(GSNode, 2, 4, 0);  // Bind Is
  __hpvm__bindIn(GSNode, 3, 5, 0);  // Bind bytesIs

  // Laplacian Inputs
  __hpvm__edge(GSNode, LNode, 1, 0, 0, 0); // Bind Is
  __hpvm__bindIn(LNode, 3, 1, 0);          // Get bytesIs
  __hpvm__bindIn(LNode, 16, 2, 0);         // Bind B
  __hpvm__bindIn(LNode, 17, 3, 0);         // Bind bytesB
  __hpvm__bindIn(LNode, 4, 4, 0);          // Bind L
  __hpvm__bindIn(LNode, 5, 5, 0);          // Bind bytesL

  // Compute ZC Inputs
  __hpvm__edge(LNode, CZCNode, 1, 0, 0, 0); // Bind L
  __hpvm__bindIn(CZCNode, 5, 1, 0);         // Get bytesL
  __hpvm__bindIn(CZCNode, 16, 2, 0);        // Bind B
  __hpvm__bindIn(CZCNode, 17, 3, 0);        // Bind bytesB
  __hpvm__bindIn(CZCNode, 6, 4, 0);         // Bind S
  __hpvm__bindIn(CZCNode, 7, 5, 0);         // Bind bytesS

  // Gradient Inputs
  __hpvm__edge(GSNode, CGNode, 1, 1, 0, 0); // Bind Is
  __hpvm__bindIn(CGNode, 3, 1, 0);          // Get bytesIs
  __hpvm__bindIn(CGNode, 18, 2, 0);         // Bind Sx
  __hpvm__bindIn(CGNode, 19, 3, 0);         // Bind bytesSx
  __hpvm__bindIn(CGNode, 20, 4, 0);         // Bind Sy
  __hpvm__bindIn(CGNode, 21, 5, 0);         // Bind bytesSy
  __hpvm__bindIn(CGNode, 8, 6, 0);          // Bind G
  __hpvm__bindIn(CGNode, 9, 7, 0);          // Bind bytesG

  // Max Gradient Inputs
  __hpvm__edge(CGNode, CMGNode, 1, 0, 0, 0); // Bind G
  __hpvm__bindIn(CMGNode, 9, 1, 0);          // Get bytesG
  __hpvm__bindIn(CMGNode, 10, 2, 0);         // Bind maxG
  __hpvm__bindIn(CMGNode, 11, 3, 0);         // Bind bytesMaxG
  __hpvm__bindIn(CMGNode, 22, 4, 0);         // Bind m
  __hpvm__bindIn(CMGNode, 23, 5, 0);         // Bind n

  // Reject ZC Inputs
  __hpvm__edge(CZCNode, RZCNode, 1, 0, 0, 0); // Bind S
  __hpvm__bindIn(RZCNode, 7, 1, 0);           // Get bytesS
  __hpvm__bindIn(RZCNode, 8, 2, 0);           // Bind G
  __hpvm__bindIn(RZCNode, 9, 3, 0);           // Bind bytesG
  __hpvm__edge(CMGNode, RZCNode, 1, 0, 4, 0); // Bind maxG
  __hpvm__bindIn(RZCNode, 11, 5, 0);          // Get bytesMaxG
  __hpvm__bindIn(RZCNode, 12, 6, 0);          // Bind E
  __hpvm__bindIn(RZCNode, 13, 7, 0);          // Bind bytesE

  __hpvm__bindOut(RZCNode, 0, 0, 0); // Bind output
}
#endif
// Pipelined Root node
void edgeDetection(float *I, size_t bytesI,       // 0
                   float *Is, size_t bytesIs,     // 2
                   float *L, size_t bytesL,       // 4
                   float *S, size_t bytesS,       // 6
                   float *G, size_t bytesG,       // 8
                   float *maxG, size_t bytesMaxG, // 10
                   float *E, size_t bytesE,       // 12
                   float *Gs, size_t bytesGs,     // 14
                   float *B, size_t bytesB,       // 16
                   float *Sx, size_t bytesSx,     // 18
                   float *Sy, size_t bytesSy,     // 20
                   long m,                        // 22
                   long n                         // 23
) {
  __hpvm__attributes(5, I, Gs, B, Sx, Sy, 6, Is, L, S, G, maxG, E);
  __hpvm__hint(hpvm::CPU_TARGET);

#ifdef GPU
  void *GSNode = __hpvm__createNodeND(0, WrapperGaussianSmoothing);
  void *LNode = __hpvm__createNodeND(0, WrapperlaplacianEstimate);
  void *CZCNode = __hpvm__createNodeND(0, WrapperComputeZeroCrossings);
  void *CGNode = __hpvm__createNodeND(0, WrapperComputeGradient);
  void *CMGNode = __hpvm__createNodeND(0, WrapperComputeMaxGradient);
  void *RZCNode = __hpvm__createNodeND(0, WrapperRejectZeroCrossings);

  // Gaussian Inputs
  __hpvm__bindIn(GSNode, 0, 0, 0);  // Bind I
  __hpvm__bindIn(GSNode, 1, 1, 0);  // Bind bytesI
  __hpvm__bindIn(GSNode, 14, 2, 0); // Bind Gs
  __hpvm__bindIn(GSNode, 15, 3, 0); // Bind bytesGs
  __hpvm__bindIn(GSNode, 2, 4, 0);  // Bind Is
  __hpvm__bindIn(GSNode, 3, 5, 0);  // Bind bytesIs
  __hpvm__bindIn(GSNode, 22, 6, 0); // Bind m
  __hpvm__bindIn(GSNode, 23, 7, 0); // Bind n

  // Laplacian Inputs
  __hpvm__edge(GSNode, LNode, 1, 0, 0, 0); // Bind Is
  __hpvm__bindIn(LNode, 3, 1, 0);          // Get bytesIs
  __hpvm__bindIn(LNode, 16, 2, 0);         // Bind B
  __hpvm__bindIn(LNode, 17, 3, 0);         // Bind bytesB
  __hpvm__bindIn(LNode, 4, 4, 0);          // Bind L
  __hpvm__bindIn(LNode, 5, 5, 0);          // Bind bytesL
  __hpvm__bindIn(LNode, 22, 6, 0);         // Bind m
  __hpvm__bindIn(LNode, 23, 7, 0);         // Bind n

  // Compute ZC Inputs
  __hpvm__edge(LNode, CZCNode, 1, 0, 0, 0); // Bind L
  __hpvm__bindIn(CZCNode, 5, 1, 0);         // Get bytesL
  __hpvm__bindIn(CZCNode, 16, 2, 0);        // Bind B
  __hpvm__bindIn(CZCNode, 17, 3, 0);        // Bind bytesB
  __hpvm__bindIn(CZCNode, 6, 4, 0);         // Bind S
  __hpvm__bindIn(CZCNode, 7, 5, 0);         // Bind bytesS
  __hpvm__bindIn(CZCNode, 22, 6, 0);        // Bind m
  __hpvm__bindIn(CZCNode, 23, 7, 0);        // Bind n

  // Gradient Inputs
  __hpvm__edge(GSNode, CGNode, 1, 1, 0, 0); // Bind Is
  __hpvm__bindIn(CGNode, 3, 1, 0);          // Get bytesIs
  __hpvm__bindIn(CGNode, 18, 2, 0);         // Bind Sx
  __hpvm__bindIn(CGNode, 19, 3, 0);         // Bind bytesSx
  __hpvm__bindIn(CGNode, 20, 4, 0);         // Bind Sy
  __hpvm__bindIn(CGNode, 21, 5, 0);         // Bind bytesSy
  __hpvm__bindIn(CGNode, 8, 6, 0);          // Bind G
  __hpvm__bindIn(CGNode, 9, 7, 0);          // Bind bytesG
  __hpvm__bindIn(CGNode, 22, 8, 0);         // Bind m
  __hpvm__bindIn(CGNode, 23, 9, 0);         // Bind n

  // Max Gradient Inputs
  __hpvm__edge(CGNode, CMGNode, 1, 0, 0, 0); // Bind G
  __hpvm__bindIn(CMGNode, 9, 1, 0);          // Get bytesG
  __hpvm__bindIn(CMGNode, 10, 2, 0);         // Bind maxG
  __hpvm__bindIn(CMGNode, 11, 3, 0);         // Bind bytesMaxG
  __hpvm__bindIn(CMGNode, 22, 4, 0);         // Bind m
  __hpvm__bindIn(CMGNode, 23, 5, 0);         // Bind n

  // Reject ZC Inputs
  __hpvm__edge(CZCNode, RZCNode, 1, 0, 0, 0); // Bind S
  __hpvm__bindIn(RZCNode, 7, 1, 0);           // Get bytesS
  __hpvm__bindIn(RZCNode, 8, 2, 0);           // Bind G
  __hpvm__bindIn(RZCNode, 9, 3, 0);           // Bind bytesG
  __hpvm__edge(CMGNode, RZCNode, 1, 0, 4, 0); // Bind maxG
  __hpvm__bindIn(RZCNode, 11, 5, 0);          // Get bytesMaxG
  __hpvm__bindIn(RZCNode, 12, 6, 0);          // Bind E
  __hpvm__bindIn(RZCNode, 13, 7, 0);          // Bind bytesE
  __hpvm__bindIn(RZCNode, 22, 8, 0);          // Bind m
  __hpvm__bindIn(RZCNode, 23, 9, 0);          // Bind n

  __hpvm__bindOut(RZCNode, 0, 0, 0); // Bind output
#else
  void *LNode = __hpvm__createNodeND(0, DeviceLaunchNode);
  __hpvm__bindIn(LNode, 0, 0, 0);
  __hpvm__bindIn(LNode, 1, 1, 0);
  __hpvm__bindIn(LNode, 2, 2, 0);
  __hpvm__bindIn(LNode, 3, 3, 0);
  __hpvm__bindIn(LNode, 4, 4, 0);
  __hpvm__bindIn(LNode, 5, 5, 0);
  __hpvm__bindIn(LNode, 6, 6, 0);
  __hpvm__bindIn(LNode, 7, 7, 0);
  __hpvm__bindIn(LNode, 8, 8, 0);
  __hpvm__bindIn(LNode, 9, 9, 0);
  __hpvm__bindIn(LNode, 10, 10, 0);
  __hpvm__bindIn(LNode, 11, 11, 0);
  __hpvm__bindIn(LNode, 12, 12, 0);
  __hpvm__bindIn(LNode, 13, 13, 0);
  __hpvm__bindIn(LNode, 14, 14, 0);
  __hpvm__bindIn(LNode, 15, 15, 0);
  __hpvm__bindIn(LNode, 16, 16, 0);
  __hpvm__bindIn(LNode, 17, 17, 0);
  __hpvm__bindIn(LNode, 18, 18, 0);
  __hpvm__bindIn(LNode, 19, 19, 0);
  __hpvm__bindIn(LNode, 20, 20, 0);
  __hpvm__bindIn(LNode, 21, 21, 0);
  __hpvm__bindIn(LNode, 22, 22, 0);
  __hpvm__bindIn(LNode, 23, 23, 0);

  __hpvm__bindOut(LNode, 0, 0, 0); // Bind output
#endif
}
}

using namespace cv;

void getNextFrame(VideoCapture &VC, Mat &F) {
  // Modified to use a temp Mat object to avoid having F reallocated.
  Mat t;
  VC >> t;
  /// Convert the image to grayscale if image colored
  if (t.channels() == 3)
    cvtColor(t, t, CV_BGR2GRAY);

  t.convertTo(F, CV_32F, 1.0 / 255.0);
}

int main(int argc, char *argv[]) {
  using ClockType = std::chrono::steady_clock;
  auto mainStart = ClockType::now();

  if (argc < 2) {
    fprintf(stderr, "Expecting input image filename\n");
    exit(-1);
  }
  char *inFile = argv[1];
  fprintf(stderr, "Running pipeline on %s\n", inFile);

  size_t I_sz;
  long block_x, grid_x;

  std::cout << "Using OpenCV" << CV_VERSION << "\n";

  /* Read in data */
  std::cout << "Reading video file: " << inFile << "\n";
  VideoCapture cap(inFile);
  if (!cap.isOpened()) {
    std::cout << "Could not open video file"
              << "\n";
    return -1;
  }

  int NUM_FRAMES = cap.get(CAP_PROP_FRAME_COUNT);
  NUM_FRAMES = 5;
  std::cout << "Number of frames = " << NUM_FRAMES << "\n";
  int width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  int height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

#ifdef DISPLAY
  namedWindow(input_window, WINDOW_AUTOSIZE);
  namedWindow(output_window, WINDOW_AUTOSIZE);
#ifdef VERIFY
  namedWindow(cpu_output_window, WINDOW_AUTOSIZE);
#endif
#endif
  // moveWindow(input_window, POSX_IN, POSY_IN);
  // moveWindow(output_window, POSX_OUT, POSY_OUT);

  size_t image_sz = height * width * sizeof(float);
  float *I_aligned = (float *)alignedMalloc(image_sz);
  std::cout << "host ptr: " << I_aligned << std::endl;
  float *Is_aligned = (float *)alignedMalloc(image_sz);
  float *L_aligned = (float *)alignedMalloc(image_sz);
  float *S_aligned = (float *)alignedMalloc(image_sz);
  float *G_aligned = (float *)alignedMalloc(image_sz);
  float *E_aligned = (float *)alignedMalloc(image_sz);

  Mat src(height, width, CV_32F, I_aligned);
  Mat Is(height, width, CV_32F, Is_aligned);
  Mat L(height, width, CV_32F, L_aligned);
  Mat S(height, width, CV_32F, S_aligned);
  Mat G(height, width, CV_32F, G_aligned);
  Mat E(height, width, CV_32F, E_aligned);
#ifdef VERIFY
  Mat Is_cpu(height, width, CV_32F); //, Is_aligned);
  Mat L_cpu(height, width, CV_32F);  //, L_aligned);
  Mat S_cpu(height, width, CV_32F);  //, S_aligned);
  Mat G_cpu(height, width, CV_32F);  //, G_aligned);
  Mat E_cpu(height, width, CV_32F);  //, E_aligned);
#endif
  size_t bytesMaxG = sizeof(float);
  float *maxG = (float *)alignedMalloc(bytesMaxG);
  float *maxG_cpu = (float *)alignedMalloc(bytesMaxG);

  size_t bytesB = 9 * sizeof(float);
  float *B = (float *)alignedMalloc(bytesB);
  memcpy(B, (float[]){1, 1, 1, 1, 1, 1, 1, 1, 1}, 9 * sizeof(float));
  printArray(B, 9);

  size_t bytesSx = 9 * sizeof(float);
  float *Sx = (float *)alignedMalloc(bytesSx);
  memcpy(Sx, (float[]){-1, 0, 1, -2, 0, 2, -1, 0, 1}, 9 * sizeof(float));
  printArray(Sx, 9);

  size_t bytesSy = 9 * sizeof(float);
  float *Sy = (float *)alignedMalloc(bytesSy);
  memcpy(Sy, (float[]){-1, -2, -1, 0, 0, 0, 1, 2, 1}, 9 * sizeof(float));
  printArray(Sy, 9);

  size_t bytesGs = 7 * 7 * sizeof(float);
  float *Gs = (float *)alignedMalloc(bytesGs);
  memcpy(Gs,
         (float[]){0.000036, 0.000363, 0.001446, 0.002291, 0.001446, 0.000363,
                   0.000036, 0.000363, 0.003676, 0.014662, 0.023226, 0.014662,
                   0.003676, 0.000363, 0.001446, 0.014662, 0.058488, 0.092651,
                   0.058488, 0.014662, 0.001446, 0.002291, 0.023226, 0.092651,
                   0.146768, 0.092651, 0.023226, 0.002291, 0.001446, 0.014662,
                   0.058488, 0.092651, 0.058488, 0.014662, 0.001446, 0.000363,
                   0.003676, 0.014662, 0.023226, 0.014662, 0.003676, 0.000363,
                   0.000036, 0.000363, 0.001446, 0.002291, 0.001446, 0.000363,
                   0.000036},
         7 * 7 * sizeof(float));
  printArray(Gs, 7, 7);

  getNextFrame(cap, src);

  std::cout << "Image dimension = " << src.size() << "\n";
  if (!src.isContinuous()) {
    std::cout << "Expecting contiguous storage of image in memory!\n";
    exit(-1);
  }

  __hpvm__init();

#ifdef DISPLAY
  // Mat in, out;
  // resize(src, in, Size(HEIGHT, WIDTH));
  // resize(E, out, Size(HEIGHT, WIDTH));
  // imshow(input_window, in);
  // imshow(output_window, out);
  //  waitKey(0);
  imshow(input_window, src);
  imshow(output_window, E);
  #ifdef VERIFY
  imshow(cpu_output_window, E_cpu);
  #endif
  //  waitKey(0);
#endif

  struct InStruct *args = (struct InStruct *)malloc(sizeof(InStruct));
  packData(args, (float *)src.data, image_sz, (float *)Is.data, image_sz,
           (float *)L.data, image_sz, (float *)S.data, image_sz,
           (float *)G.data, image_sz, maxG, bytesMaxG, (float *)E.data,
           image_sz, Gs, bytesGs, B, bytesB, Sx, bytesSx, Sy, bytesSy, height,
           width);

  float totalTime = 0.0;
#ifdef REPORT_POWER
#ifdef CPU
  float totalEnergy = 0.0;
  // The GetEnergyCPU() implementation relies on us not switching processors
  // so to fascilitate this we pin ourselves to the current core, this is
  // also needed as it determines the processor we're running on
  int core = sched_getcpu();
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(core, &mask);
  if (sched_setaffinity(0, sizeof(mask), &mask)) {
    perror("Error in sched_setaffinity: ");
    exit(1);
  }
#endif
#ifdef GPU
  float totalPower = 0.0;
  pthread_barrier_init(&barrier, NULL, 2);
#endif
#endif

  auto copiedStart = ClockType::now();

  for (unsigned j = 0; j < NUM_RUNS; j++) {
    std::cout << "Run: " << j << "\n";
    // void *DFG = __hpvm__launch(1, edgeDetection, (void *)args);

    // cap = VideoCapture(inFile);
    // getNextFrame(cap, src);

    // if (NUM_FRAMES >= 2) {
    *maxG = 0.0;
    // lvm_hpvm_track_mem(args->I, image_sz);
    llvm_hpvm_track_mem(args->Is, image_sz);
    llvm_hpvm_track_mem(args->L, image_sz);
    llvm_hpvm_track_mem(args->S, image_sz);
    llvm_hpvm_track_mem(args->G, image_sz);
    llvm_hpvm_track_mem(maxG, bytesMaxG);
    llvm_hpvm_track_mem(args->E, image_sz);
    llvm_hpvm_track_mem(Gs, bytesGs);
    llvm_hpvm_track_mem(B, bytesB);
    llvm_hpvm_track_mem(Sx, bytesSx);
    llvm_hpvm_track_mem(Sy, bytesSy);

    for (int i = 0; i < NUM_FRAMES; i++) {
      // memcpy(args->I, src.data, image_sz);

      llvm_hpvm_track_mem(args->I, image_sz);

#ifdef REPORT_POWER
  #ifdef CPU
      double start_energy = GetEnergyCPU(core);
  #endif
  #ifdef GPU
      int flag = 0;
      pthread_t thd;
      pthread_create(&thd, NULL, measure_func, &flag);
      pthread_barrier_wait(&barrier);
  #endif
#endif
  
      auto startTime = std::chrono::steady_clock::now();

      void *DFG = __hpvm__launch(0, edgeDetection, (void *)args);
      __hpvm__wait(DFG);
      //__hpvm__push(DFG, args);
      // void *ret = __hpvm__pop(DFG);
      // This is reading the result of the streaming graph
      // size_t framesize = ((OutStruct *)ret)->ret;

      // llvm_hpvm_request_mem(maxG, bytesMaxG);
      llvm_hpvm_request_mem(args->E, image_sz);

      auto elapsed = std::chrono::steady_clock::now() - startTime;
      totalTime += std::chrono::duration<float>(elapsed).count();
#ifdef REPORT_POWER
  #ifdef CPU
      double end_energy = GetEnergyCPU(core);
      totalEnergy += end_energy - start_energy;
  #endif
  #ifdef GPU
      flag = 1;
      pthread_join(thd, NULL);
      totalPower += measuredPower;
  #endif
#endif

#ifdef VERIFY
      llvm_hpvm_request_mem(args->Is, image_sz);
      llvm_hpvm_request_mem(args->L, image_sz);
      llvm_hpvm_request_mem(args->S, image_sz);
      llvm_hpvm_request_mem(args->G, image_sz);
      /*********** Check Result ************************/

      gaussianSmoothing_cpu((float *)src.data, image_sz, Gs, bytesGs,
                            (float *)Is_cpu.data, image_sz, height, width);
      if (!compare_FPGA_CPU((float *)Is.data, (float *)Is_cpu.data,
                            height * width))
        std::cout << "No mismatch in GS output!" << std::endl;
      else
        std::cout << "Mismatch in GS output!" << std::endl;

      laplacianEstimate_cpu((float *)Is_cpu.data, image_sz, B, bytesB,
                            (float *)L_cpu.data, image_sz, height, width);
      if (!compare_FPGA_CPU((float *)L.data, (float *)L_cpu.data,
                            height * width))
        std::cout << "No mismatch in LE output!" << std::endl;
      else
        std::cout << "Mismatch in LE output!" << std::endl;

      computeZeroCrossings_cpu((float *)L_cpu.data, image_sz, B, bytesB,
                               (float *)S_cpu.data, image_sz, height, width);
      if (!compare_FPGA_CPU((float *)S.data, (float *)S_cpu.data,
                            height * width))
        std::cout << "No mismatch in CZC output!" << std::endl;
      else
        std::cout << "Mismatch in CZC output!" << std::endl;

      computeGradient_cpu((float *)Is_cpu.data, image_sz, Sx, bytesSx, Sy,
                          bytesSy, (float *)G_cpu.data, image_sz, height,
                          width);
      if (!compare_FPGA_CPU((float *)G.data, (float *)G_cpu.data,
                            height * width))
        std::cout << "No mismatch in CG output!" << std::endl;
      else
        std::cout << "Mismatch in CG output!" << std::endl;

      computeMaxGradient_cpu((float *)G_cpu.data, image_sz, maxG_cpu, bytesMaxG,
                             height, width);
      if (!compare_FPGA_CPU(maxG, maxG_cpu, 1))
        std::cout << "No mismatch in CMG output!" << std::endl;
      else
        std::cout << "Mismatch in CMG output!" << std::endl;

      rejectZeroCrossings_cpu(
          (float *)S_cpu.data, image_sz, (float *)G_cpu.data, image_sz,
          maxG_cpu, bytesMaxG, (float *)E_cpu.data, image_sz, height, width);
      if (!compare_FPGA_CPU((float *)E.data, (float *)E_cpu.data,
                            height * width))
        std::cout << "No mismatch in final output!" << std::endl;
      else
        std::cout << "Mismatch in final output!" << std::endl;
      imshow(cpu_output_window, E_cpu);
      /************************************************/
#endif
      // Mat in, out;
      // resize(src, in, Size(HEIGHT, WIDTH));
      // resize(E, out, Size(HEIGHT, WIDTH));
#ifdef DISPLAY
      imshow(output_window, E);
      imshow(input_window, src);
      waitKey(1);
#endif

      llvm_hpvm_untrack_mem(args->I);

      getNextFrame(cap, src);
    }

    llvm_hpvm_untrack_mem(Is.data);
    llvm_hpvm_untrack_mem(L.data);
    llvm_hpvm_untrack_mem(S.data);
    llvm_hpvm_untrack_mem(G.data);
    llvm_hpvm_untrack_mem(maxG);
    llvm_hpvm_untrack_mem(E.data);
    llvm_hpvm_untrack_mem(Gs);
    llvm_hpvm_untrack_mem(B);
    llvm_hpvm_untrack_mem(Sx);
    llvm_hpvm_untrack_mem(Sy);
    //} else {
    //  __hpvm__push(DFG, args);
    //  __hpvm__pop(DFG);
    //}
    //__hpvm__wait(DFG);
  }
  auto timeNow = ClockType::now();
  std::chrono::duration<double> fullTime = timeNow - mainStart;
  
  std::cout << "Kernel Execution took " << totalTime << " s\n";
  std::cout << "Total Execution took " << fullTime.count() << " s\n";
#ifdef REPORT_POWER
  double execTimeSec = totalTime;
#ifdef CPU
  printf("\nPower usage: %lf W\n", totalEnergy / execTimeSec);
  printf("Energy usage: %lf J\n", totalEnergy);
#endif
#ifdef GPU
  printf("\nPower usage: %lf W\n", totalPower / (NUM_FRAMES * NUM_RUNS));
  printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower / (NUM_FRAMES * NUM_RUNS), execTimeSec * 1000));
#endif
#endif
  __hpvm__cleanup();
  return 0;
}
