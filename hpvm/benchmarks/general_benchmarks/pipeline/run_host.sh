#!/bin/bash 
#debug=echo
emulator="CL_CONTEXT_EMULATOR_DEVICE_ALTERA=1 "
declare -a bnamearr=(
    "pipeline-seq_Default"
    "pipeline-fpga1_GOPT" \
    "pipeline-fpga2_GOPT" \
    "pipeline-fpga3_GOPT" \
    "pipeline-fpga4_GOPT" \
    "pipeline-fpga5_GOPT"
)
declare -a dirrarr=(
    "seq_Default"
    "fpga1_GOPT" \
    "fpga2_GOPT" \
    "fpga3_GOPT" \
    "fpga4_GOPT" \
    "fpga5_GOPT"
)                   
for i in "${!bnamearr[@]}"
do
  for j in {1..5}
  do
    echo ./${bnamearr[$i]} datasets/formula1_scaled.mp4
    $debug ./${bnamearr[$i]} datasets/formula1_scaled.mp4 > build/${dirrarr[$i]}/output_${j}.mon 
    echo  mv profile.mon build/${dirrarr[$i]}/profile_${j}.mon 
    $debug mv profile.mon build/${dirrarr[$i]}/profile_${j}.mon 
    echo "----------------------------------------------------------------------------------------------------"
  done 
  echo "----------------------------------------------------------------------------------------------------"
  echo "####################################################################################################"
  echo "####################################################################################################"
  echo "----------------------------------------------------------------------------------------------------"
done
