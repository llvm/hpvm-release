# Usage: python3 get_kernel_times.py profile_1.mon profile_2.mon profile_3.mon

import csv
import sys

paths = sys.argv[1:]
output_string = ""

for path in paths:
    with open(path, 'rt') as infile:
        first_time = None
        last_time = None
        for row in csv.reader(infile):
            if first_time is None:
                first_time = int(row[3])
            last_time = int(row[4])
        output_string += str((last_time - first_time) / 1000000000)
        output_string += "\t"

print(output_string)
