# Edge Detection Pipeline

HPVM benchmark performing edge detection on a stream of grayscale input images.

## Dependencies

Edge detection pipeline depends on `OpenCV==2.4`. To use OpenCV 3.4, go to line
13 in `src/main.cc` and replace 

```C
#include "opencv2/ocl/ocl.hpp"
```
with
```C
#include "opencv2/core/ocl.hpp"
```

Edge detection pipeline is not tested with other versions of OpenCV.

## Building
Pipeline can be built using the following command, from this directory, where
`<target>` and `[flags]` are described below.

```sh
make TARGET=<target> [flags]
```

The currently supported `<target>` are: `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* By default, the program does not display the results to the screen, to enable
  this, use the `DISPLAY=1` flag
* To enable verification of the HPVM results compared against a sequential
  implementation, use `VERIFY=1`; this will print differences found between the
  versions (this can be common due to floating-point rounding issues). When
  displaying images is enabled, this option will also show the results from the
  sequential implementation
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
Pipeline can be run on any MP4 file, such as the one provided in `datasets/`.
The command to run the program is

```sh
./pipeline-<version> <file>
```

where `<version>` is based on the target and other flags provided when building
and `<file>` is the path to an MP4, for example `datasets/formula1_scaled.mp4`.
