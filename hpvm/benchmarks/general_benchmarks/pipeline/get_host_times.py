# Usage: python3 get_host_times.py output_1.mon output_2.mon output_3.mon

import sys

paths = sys.argv[1:]
output_string = ""

for path in paths:
    with open(path, 'rt') as infile:
        contents = infile.read()
        i = contents.find("Total elapsed: ")
        output_string += (contents[i + len("Total elapsed: "):]).strip()
        output_string += "\t"
print(output_string)
