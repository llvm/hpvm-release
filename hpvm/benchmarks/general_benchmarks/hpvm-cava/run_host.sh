#!/bin/bash 
#debug=echo
emulator="CL_CONTEXT_EMULATOR_DEVICE_ALTERA=1 "
declare -a bnamearr=(
#    "cava-hpvm-seq_Default"
    "cava-hpvm-fpga1_GOPT"
    "cava-hpvm-fpga2_GOPT"
    "cava-hpvm-fpga3_GOPT"
    "cava-hpvm-fpga4_GOPT"
    "cava-hpvm-fpga5_GOPT"
)
declare -a dirrarr=(
#    "seq_Default"
    "fpga1_GOPT" \
    "fpga2_GOPT" \
    "fpga3_GOPT" \
    "fpga4_GOPT" \
    "fpga5_GOPT"
)                   
for i in "${!bnamearr[@]}"
do
  $debug mkdir example-tulip-small/${dirrarr[$i]}
  for j in {1..5}
  do
    echo ./${bnamearr[$i]} example-tulip-small/raw_tulip-small.bin example-tulip-small/${dirrarr[$i]}/tulip-small > build/${dirrarr[$i]}/output_${j}.mon 
    $debug ./${bnamearr[$i]} example-tulip-small/raw_tulip-small.bin example-tulip-small/${dirrarr[$i]}/tulip-small > build/${dirrarr[$i]}/output_${j}.mon 
    echo  mv profile.mon build/${dirrarr[$i]}/profile_${j}.mon 
    $debug mv profile.mon build/${dirrarr[$i]}/profile_${j}.mon 
    echo "----------------------------------------------------------------------------------------------------"
  done 
  echo "----------------------------------------------------------------------------------------------------"
  echo "####################################################################################################"
  echo "####################################################################################################"
  echo "----------------------------------------------------------------------------------------------------"
done
