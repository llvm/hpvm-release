# HPVM-CAVA

Harvard Camera Pipeline with HPVM intrinsics.

## Camera Pipeline
The camera pipeline is a simple five-stage image signal processor (ISP) which
processes raw images (i.e., sensor inputs) into an image that can feed into a
vision backend (e.g., a CNN).


## Building
HPVM-CAVA can be built using the following command, from this directory, where
`<target>` and `[flags]` are described below.

```sh
make TARGET=<target> [flags]
```

The currently supported `<target>` are `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* To enable verification of the HPVM results compared against a sequential
  implementation, use `VERIFY=1`; this will print whether there were
  differences between the versions, these sometimes exist just due to
  floating-point rounding errors. With this flag, it also outputs intermediate
  results as images, as described below.
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
HPVM-CAVA is run on a raw image such as those provided in `example-tulips`
and `example-tulip-small`. The command to run the program is

```sh
./cava-hpvm-<version> <input> <prefix>
```

where `<version>` is based on the target and flags used to build HPVM-CAVA.
The `<input>` is a path to a raw image, such as
`example-tulip-small/raw_tulip-small.bin` and `example-tulips/raw_tulips.bin`.
Note that these are not a bitmap format and so may require special software to
obtain files in this format. 

The program then outputs the resulting image to `<prefix>.bin`. When
verification is enabled, it will also write images to `<prefix>_<state>.bin`
where `<stage>` are the various stages of the pipeline and these images contain
the image after that stage.

The resulting images are in the same format as the inputs, and these can all
be converted to PNGs using the `convert.sh` script found in this directory.
It is invonked as

```sh
./convert.sh <dir>
```

Where `<dir>` specifies the directory in which to find and convert `.bin` files.
Note that this script

