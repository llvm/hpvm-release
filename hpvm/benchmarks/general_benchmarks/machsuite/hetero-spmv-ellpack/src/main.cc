#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "heterocc.h"

#define NNZ 1666
#define N 494
#define L 10

#define FLOAT_EQ_TOLERANCE 0.001

#include "defs.h"
void *malloc_aligned(size_t size) {
  void *ptr = NULL;
  int err = posix_memalign((void **)&ptr, CACHELINE_SIZE, size);
  assert(err == 0 && "Failed to allocate memory!");
  return ptr;
}

void spmv(double * __restrict nzval, size_t bytes_nzval,
                  int * __restrict cols, size_t bytes_cols,
                  double * __restrict vec, size_t bytes_vec,
                  double * __restrict out, size_t bytes_out,
                  size_t matrix_dim){
    void *Section = __hetero_section_begin();
    void* Wrapper = __hetero_task_begin(
            5, 
            nzval, bytes_nzval, 
            cols, bytes_cols, 
            vec, bytes_vec, 
            out, bytes_out, 
            matrix_dim,
            1, out, bytes_out,
	    "SPMV_wrapper_task");
    void *Section_Wrapped = __hetero_section_begin();


    double Si;
    for (int i = 0; i < matrix_dim; i++) {
         __hetero_parallel_loop(/* Layer of Loop*/ 1,
            /* Num Input Pairs */ 4, 
            nzval, bytes_nzval, 
            cols, bytes_cols, 
            vec, bytes_vec, 
            out, bytes_out, 
            /* Num Output Pairs */ 1, out, bytes_out,
	    "parallel_SPMV_loop");
        __hpvm__hint(hpvm::DEVICE);
        {
            double sum = 0;
            for (int j = 0; j < L; j++) {
                Si = nzval[j + i*L] * vec[cols[j + i*L]];
                sum += Si;
            }
            out[i] = sum;
        }
      }


    __hetero_section_end(Section_Wrapped);
    __hetero_task_end(Wrapper);
    __hetero_section_end(Section);
}


int process_input_data(char *file, double *nzval, int *cols, double *vec) {
        FILE *fp;
        fp = fopen(file, "r");
        if (fp == NULL) {
                return -1;
        }

        int cur_part = -1;
        int cur_index = 0;

        ssize_t bytes_read;
        char *line = NULL;
        size_t len = 0;
        while ((bytes_read = getline(&line, &len, fp)) != -1) {
                if (line[bytes_read - 1] == '\n') {
                        line[bytes_read - 1] = '\0';
                }
                if (strcmp(line, "%%") == 0) {
                        switch (cur_part) {
                                case -1: break; // We are just starting
                                case 0: if (cur_index == N*L) break; else return -1;
                                case 1: if (cur_index == N*L) break; else return -1;
                                default: return -1;
                        }
                        cur_part++;
                        cur_index = 0;
                } else {
                        switch (cur_part) {
                                case 0: {
                                        if (cur_index >= N*L) return -1;
                                        nzval[cur_index] = strtof(line, NULL);
                                        break;
                                }
                                case 1: {
                                        if (cur_index >= N*L) return -1;
                                        cols[cur_index] = atoi(line);
                                        break;
                                }
                                case 2: {
                                        if (cur_index >= N) return -1;
                                        vec[cur_index] = strtof(line, NULL);
                                        break;
                                }
                                default: return -1;
                        }
                        cur_index++;
                }
        }

        if (cur_index != N) {
                return -1;
        }
        fclose(fp);
        return 0;
}

int process_output_data(char *file, float *out) {
        FILE *fp;
        fp = fopen(file, "r");
        if (fp == NULL) {
                return -1;
        }

        int index = -1;
        ssize_t bytes_read;
        char *line = NULL;
        size_t len = 0;
        while ((bytes_read = getline(&line, &len, fp)) != -1) {
                if (index == -1) {
                        index++;
                        continue;
                }
                if (line[bytes_read - 1] == '\n') {
                        line[bytes_read - 1] = '\0';
                }
                if (index >= N) {
                        return -1;
                }
                out[index] = strtof(line, NULL);
                index++;
        }

        if (index != N) {
                printf("%d is not %d\n", index - 1, N);
                return -1;
        }
        fclose(fp);
        return 0;
}

int float_eq(float f1, float f2) {
        return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
        // Usage: <executable> <input data> <check data>
        if (argc != 3) {
                printf("Expected parameters <input data> and <check data>\n");
                return 1;
        }

        // Prepare 4 buffer pairs
        size_t bytes_nzval = N * L * sizeof(double*);
        size_t bytes_cols = N * L * sizeof(int*);
        size_t bytes_vec = N * sizeof(double*);
        size_t bytes_out = N * sizeof(int*);

        double* nzval = (double*)malloc_aligned(bytes_nzval);
        int* cols = (int*) malloc_aligned(bytes_cols);
        double* vec = (double*)malloc_aligned(bytes_vec);
        double* out = (double*)malloc_aligned(bytes_out);

        size_t matrix_dim = N;

        // Load data into buffer
        if (process_input_data(argv[1], nzval, cols, vec) != 0) {
                printf("Invalid input data\n");
                return 1;
        }

        float *expected_output = (float*) malloc(bytes_out);
        if (process_output_data(argv[2], expected_output) != 0) {
                printf("Invalid expected output data\n");
                return 1;
        }

        // Launch kernel
        printf("Starting SPMV ELLPACK kernel!\n");
        void *DFG = __hetero_launch(
            /* Root Function */
            (void *)spmv,
            /* Num Input Pairs */ 5, 
            nzval, bytes_nzval, 
            cols, bytes_cols, 
            vec, bytes_vec, 
            out, bytes_out, 
            matrix_dim,
            /* Num Output Pairs */ 1, out, bytes_out);


        __hetero_wait(DFG);
        __hetero_request_mem(out, bytes_out);
        printf("SPMV ELLPACK kernel completed!\n");

        // Check correctness
        int failed = 0;
        for (int i = 0; i < N; i++) {
                if (!float_eq(expected_output[i], out[i])) {
                        failed = 1;
                        printf("Output does not match expected output %f at index %d with incorrect value %f\n", expected_output[i], i, out[i]);
                }
        }
        if (failed) {
                printf("SPMV ELLPACK kernel produced incorrect output\n");
        } else {
                printf("SPMV ELLPACK kernel produced expected output\n");
        }

        return 0;
}

