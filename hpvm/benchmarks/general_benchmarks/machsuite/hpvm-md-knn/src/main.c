#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "hpvm.h"
#include "utility.h"

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))

// Problem Constants
#define nAtoms        256
#define maxNeighbors  16
// LJ coefficients
#define lj1           1.5
#define lj2           2.0

#define IND(dim, i) ((dim) * nAtoms + (i))

#define FLOAT_EQ_TOLERANCE 0.001

typedef struct __attribute__((__packed__)) {
    double *forces; size_t bytes_forces;
    double *positions; size_t bytes_positions;
    int *NL; size_t bytes_NL;
    size_t dummy_output;
} RootIn;

void md(double * restrict forces, size_t bytes_forces,
        double * restrict positions, size_t bytes_positions,
        int * restrict NL, size_t bytes_NL)
{
    __hpvm__hint(DEVICE);
    __hpvm__attributes(2, positions, NL, 1, forces);

    double del[3];
    volatile double f[3];


  void *thisNode = __hpvm__getNode();
  long i = __hpvm__getNodeInstanceID_x(thisNode);
  __hpvm__isNonZeroLoop (i, nAtoms);
//    for (int i = 0; i < nAtoms; i++) {
        for (int j = 0; j < 3; j++) {
            __hpvm__isNonZeroLoop (j, 3);
            f[j] = 0;
        }
        for (int j = 0; j < maxNeighbors; j++) {
            __hpvm__isNonZeroLoop (j, maxNeighbors);
            // Get neighbor
            int jidx = NL[i*maxNeighbors + j];
            // Calc distance
            for (int k = 0; k < 3; k++) {
                __hpvm__isNonZeroLoop (k, 3);
                del[k] = positions[IND(k, i)] - positions[IND(k, jidx)];
            }
            double r2inv = 1.0 / (del[0]*del[0] + del[1]*del[1] + del[2]*del[2]);
            // Assume no cutoff and aways account for all nodes in area
            double r6inv = r2inv * r2inv * r2inv;
            double potential = r6inv * (lj1*r6inv - lj2);
            // Sum changes in force
            double force = r2inv * potential;
            for (int k = 0; k < 3; k++) {
                __hpvm__isNonZeroLoop (k, 3);
                f[k] += del[k] * force;
            }
        }

        // Update forces after all neighbors accounted for
        for (int j = 0; j < 3; j++) {
            __hpvm__isNonZeroLoop (j, 3);
            forces[IND(j, i)] = f[j];
        }
//    }
      __hpvm__return (1, forces);
}

void md_wrapper(double *forces, size_t bytes_forces,
                double *positions, size_t bytes_positions,
                int *NL, size_t bytes_NL)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(2, positions, NL, 1, forces);

    void *MdNode = __hpvm__createNodeND(1, md, (size_t) nAtoms);
    __hpvm__bindIn(MdNode, 0, 0, 0);
    __hpvm__bindIn(MdNode, 1, 1, 0);
    __hpvm__bindIn(MdNode, 2, 2, 0);
    __hpvm__bindIn(MdNode, 3, 3, 0);
    __hpvm__bindIn(MdNode, 4, 4, 0);
    __hpvm__bindIn(MdNode, 5, 5, 0);

}

void MDRoot(double *forces, size_t bytes_forces,
            double *positions, size_t bytes_positions,
            int *NL, size_t bytes_NL)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(2, positions, NL, 1, forces);

    void *MdWrapperNode = __hpvm__createNodeND(0, md_wrapper);
    __hpvm__bindIn(MdWrapperNode, 0, 0, 0);
    __hpvm__bindIn(MdWrapperNode, 1, 1, 0);
    __hpvm__bindIn(MdWrapperNode, 2, 2, 0);
    __hpvm__bindIn(MdWrapperNode, 3, 3, 0);
    __hpvm__bindIn(MdWrapperNode, 4, 4, 0);
    __hpvm__bindIn(MdWrapperNode, 5, 5, 0);

}

int process_input_data(char *file, double *positions, int *NL) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_part = -1;
    int cur_index = 0;

    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            switch (cur_part) {
                case -1: break; // We are just starting
                case 0: 
                case 1:
                case 2:
                    if (cur_index == nAtoms) break; else return -1;
                default: return -1;
            }
            cur_part++;
            cur_index = 0;
        } else {
            switch (cur_part) {
                case 0: 
                case 1:
                case 2: {
                    if (cur_index >= nAtoms) return -1;
                    positions[IND(cur_part, cur_index)] = strtof(line, NULL);
                    break;
                }
                case 3: {
                    if (cur_index >= nAtoms * maxNeighbors) return -1;
                    NL[cur_index] = atoi(line);
                    break;
                }
                default: return -1;
            }
            cur_index++;
        }
    }

    if (cur_index != nAtoms * maxNeighbors) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int process_check_data(char *file, double *forces) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_index = -1;
    int cur_part = -1;
    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            if (cur_index != -1 && cur_index != nAtoms)
                return -1;
            cur_index = 0;
            cur_part++;
        } else {
            if (cur_index >= nAtoms) return -1;
            forces[IND(cur_part, cur_index)] = strtof(line, NULL);
            cur_index++;
        }
    }

    if (cur_index != nAtoms) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int float_eq(float f1, float f2) {
    return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
    // Usage: <executable> <input data>
    if (argc != 3) {
        printf("Expected parameters <input data> and <check data>\n");
        return 1;
    }

    // Create struct containing DFG inputs / outputs
    RootIn *rootArgs = (RootIn*) malloc_aligned(sizeof(RootIn));

    rootArgs->bytes_forces = 3 * sizeof(*rootArgs->forces) * nAtoms;
    rootArgs->forces = malloc_aligned(rootArgs->bytes_forces);
    rootArgs->bytes_positions = 3 * sizeof(*rootArgs->positions) * nAtoms;
    rootArgs->positions = malloc_aligned(rootArgs->bytes_positions);
    rootArgs->bytes_NL = sizeof(*rootArgs->NL) * nAtoms * maxNeighbors;
    rootArgs->NL = malloc_aligned(rootArgs->bytes_NL);

    double *expected_forces = malloc_aligned(rootArgs->bytes_forces);

    if (process_input_data(argv[1], rootArgs->positions, rootArgs->NL) != 0) {
        return 1;
    }
    if (process_check_data(argv[2], expected_forces) != 0) {
        return 1;
    }

    __hpvm__init();

    llvm_hpvm_track_mem(rootArgs->forces, rootArgs->bytes_forces);
    llvm_hpvm_track_mem(rootArgs->positions, rootArgs->bytes_positions);
    llvm_hpvm_track_mem(rootArgs->NL, rootArgs->bytes_NL);

    printf("Starting MD KNN kernel!\n");

    void *DFG = __hpvm__launch(0, MDRoot, (void*) rootArgs);
    __hpvm__wait(DFG);

    printf("MD KNN kernel completed!\n");
    llvm_hpvm_request_mem(rootArgs->forces, rootArgs->bytes_forces);

    llvm_hpvm_untrack_mem(rootArgs->forces);
    llvm_hpvm_untrack_mem(rootArgs->positions);
    llvm_hpvm_untrack_mem(rootArgs->NL);

    // Check that the output matches with the expected output
    int failed = 0;
    for (int i = 0; i < nAtoms; i++) {
        if (!float_eq(expected_forces[IND(0, i)], rootArgs->forces[IND(0, i)]) ||
            !float_eq(expected_forces[IND(1, i)], rootArgs->forces[IND(1, i)]) ||
            !float_eq(expected_forces[IND(2, i)], rootArgs->forces[IND(2, i)]))
        {
            failed = 1;
            printf("Output does not match expected output (%f, %f, %f) at index %d with incorrect value (%f, %f, %f)\n",
                expected_forces[IND(0, i)], expected_forces[IND(1, i)], expected_forces[IND(2, i)], i,
                rootArgs->forces[IND(0, i)], rootArgs->forces[IND(1, i)], rootArgs->forces[IND(2, i)]);
        }
    }

    if (failed) {
        printf("MD KNN kernel produced incorrect output\n");
    } else {
        printf("MD KNN kernel produced expected output\n");
    }

    __hpvm__cleanup();
    return 0;
}
