#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "hpvm.h"
#include "utility.h"

#define row_size 64
#define col_size 64
#define N (row_size * col_size)
#define block_size 8

#define FLOAT_EQ_TOLERANCE 0.001

typedef struct __attribute__((__packed__)) {
	double *m1; size_t bytes_m1;
	double *m2; size_t bytes_m2;
	double *prod; size_t bytes_prod;
	size_t matrix_dim;
	size_t node_dim;
} RootIn;

void gemm(double * restrict m1, size_t bytes_m1,
	      double * restrict m2, size_t bytes_m2,
	      double * restrict prod, size_t bytes_prod,
	      size_t matrix_dim)
{
	__hpvm__hint(FPGA_TARGET);
	__hpvm__attributes(2, m1, m2, 1, prod);

//    for (int jj = 0; jj < matrix_dim; jj += block_size) {
//        for (int kk = 0; kk < matrix_dim; kk += block_size) {
  void *thisNode = __hpvm__getNode();
  long jj = __hpvm__getNodeInstanceID_x(thisNode) * block_size;
  long kk = __hpvm__getNodeInstanceID_y(thisNode) * block_size;
            for (int i = 0; i < matrix_dim; i++) {
                for (int k = 0; k < block_size; k++) {
                    int i_row = i * matrix_dim;
                    int k_row = (k + kk) * matrix_dim;
                    double temp_x = m1[i_row + k + kk];
                    for (int j = 0; j < block_size; j++) {
                        double mul = temp_x * m2[k_row + j + jj];
                        prod[i_row + j + jj] += mul;
                    }
                }
            }
//        }
//    }

	__hpvm__return(1, prod);

}

void gemm_wrapper(double *m1, size_t bytes_m1,
			      double *m2, size_t bytes_m2,
			      double *prod, size_t bytes_prod,
	      		  size_t matrix_dim, size_t node_dim)
{
	__hpvm__hint(CPU_TARGET);
	__hpvm__attributes(2, m1, m2, 1, prod);

	void *GemmNode = __hpvm__createNodeND(2, gemm, node_dim, node_dim);

	__hpvm__bindIn(GemmNode, 0, 0, 0);
	__hpvm__bindIn(GemmNode, 1, 1, 0);
	__hpvm__bindIn(GemmNode, 2, 2, 0);
	__hpvm__bindIn(GemmNode, 3, 3, 0);
	__hpvm__bindIn(GemmNode, 4, 4, 0);
	__hpvm__bindIn(GemmNode, 5, 5, 0);
	__hpvm__bindIn(GemmNode, 6, 6, 0);

}

void GEMMRoot(double *m1, size_t bytes_m1,
		      double *m2, size_t bytes_m2,
		      double *prod, size_t bytes_prod,
		      size_t matrix_dim, size_t node_dim)
{
	__hpvm__hint(CPU_TARGET);
	__hpvm__attributes(2, m1, m2, 1, prod);

	void *GemmWrapperNode = __hpvm__createNodeND(0, gemm_wrapper);

	__hpvm__bindIn(GemmWrapperNode, 0, 0, 0);
	__hpvm__bindIn(GemmWrapperNode, 1, 1, 0);
	__hpvm__bindIn(GemmWrapperNode, 2, 2, 0);
	__hpvm__bindIn(GemmWrapperNode, 3, 3, 0);
	__hpvm__bindIn(GemmWrapperNode, 4, 4, 0);
	__hpvm__bindIn(GemmWrapperNode, 5, 5, 0);
	__hpvm__bindIn(GemmWrapperNode, 6, 6, 0);
	__hpvm__bindIn(GemmWrapperNode, 7, 7, 0);

}

int process_input_data(char *file, double *m1, double *m2) {
	FILE *fp;
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}

	int cur_part = -1;
	int cur_index = 0;

	ssize_t bytes_read;
	char *line = NULL;
	size_t len = 0;
	while ((bytes_read = getline(&line, &len, fp)) != -1) {
		if (line[bytes_read - 1] == '\n') {
			line[bytes_read - 1] = '\0';
		}
		if (strcmp(line, "%%") == 0) {
			switch (cur_part) {
				case -1: break; // We are just starting
				case 0: if (cur_index == N) break; else return -1;
				default: return -1;
			}
			cur_part++;
			cur_index = 0;
		} else {
			switch (cur_part) {
				case 0: {
					if (cur_index >= N) return -1;
					m1[cur_index] = strtof(line, NULL);
					break;
				}
				case 1: {
					if (cur_index >= N) return -1;
					m2[cur_index] = strtof(line, NULL);
					break;
				}
				default: return -1;
			}
			cur_index++;
		}
	}

	if (cur_index != N) {
		return -1;
	}
	fclose(fp);
	return 0;
}

int process_output_data(char *file, double *prod) {
	FILE *fp;
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}

	int index = -1;
	ssize_t bytes_read;
	char *line = NULL;
	size_t len = 0;
	while ((bytes_read = getline(&line, &len, fp)) != -1) {
		if (index == -1) {
			index++;
			continue;
		}
		if (line[bytes_read - 1] == '\n') {
			line[bytes_read - 1] = '\0';
		}
		if (index >= N) {
			return -1;
		}
		prod[index] = strtof(line, NULL);
		index++;
	}

	if (index != N) {
		printf("%d is not %d\n", index - 1, N);
		return -1;
	}
	fclose(fp);
	return 0;
}

int float_eq(float f1, float f2) {
	return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
	// Usage: <executable> <input data> <check data>
	if (argc != 3) {
		printf("Expected parameters <input data> and <check data>\n");
		return 1;
	}

	// Create struct containing DFG inputs / outputs
	RootIn *rootArgs = (RootIn*) malloc_aligned(sizeof(RootIn));

	rootArgs->bytes_m1 = N * sizeof(*rootArgs->m1);
	rootArgs->m1 = malloc_aligned(rootArgs->bytes_m1);
	rootArgs->bytes_m2 = N * sizeof(*rootArgs->m2);
	rootArgs->m2 = malloc_aligned(rootArgs->bytes_m2);
	rootArgs->bytes_prod = N * sizeof(*rootArgs->prod);
	rootArgs->prod = malloc_aligned(rootArgs->bytes_prod);
	rootArgs->matrix_dim = row_size;
  rootArgs->node_dim = row_size/block_size;

	if (process_input_data(argv[1], rootArgs->m1, rootArgs->m2) != 0) {
		printf("Invalid input data\n");
		return 1;
	}

	double *expected_output = malloc_aligned(rootArgs->bytes_prod);
	if (process_output_data(argv[2], expected_output) != 0) {
		printf("Invalid expected output data\n");
		return 1;
	}

	__hpvm__init();

	llvm_hpvm_track_mem(rootArgs->m1, rootArgs->bytes_m1);
	llvm_hpvm_track_mem(rootArgs->m2, rootArgs->bytes_m2);
	llvm_hpvm_track_mem(rootArgs->prod, rootArgs->bytes_prod);

	printf("Starting GEMM Blocked kernel!\n");

	void *DFG = __hpvm__launch(0, GEMMRoot, (void*) rootArgs);
	__hpvm__wait(DFG);

	printf("GEMM Blocked kernel completed!\n");
	llvm_hpvm_request_mem(rootArgs->prod, rootArgs->bytes_prod);

	llvm_hpvm_untrack_mem(rootArgs->m1);
	llvm_hpvm_untrack_mem(rootArgs->m2);
	llvm_hpvm_untrack_mem(rootArgs->prod);

	int failed = 0;
	for (int i = 0; i < N; i++) {
		if (!float_eq(expected_output[i], rootArgs->prod[i])) {
			failed = 1;
			printf("Output does not match expected output %f at index %d with incorrect value %f\n", expected_output[i], i, rootArgs->prod[i]);
		}
	}
	if (failed) {
		printf("GEMM Blocked kernel produced incorrect output\n");
	} else {
		printf("GEMM Blocked kernel produced expected output\n");
	}

	__hpvm__cleanup();
	return 0;
}
