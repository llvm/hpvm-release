#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "hpvm.h"
#include "utility.h"

#define col_size 64
#define row_size 128
#define f_size 9

#define FLOAT_EQ_TOLERANCE 0.001

typedef struct __attribute__((__packed__)) {
    int *orig; size_t bytes_orig;
    int *sol; size_t bytes_sol;
    int *filter; size_t bytes_filter;
    size_t dummy_output;
} RootIn;

void stencil(int * restrict orig, size_t bytes_orig,
             int * restrict sol, size_t bytes_sol,
             int * restrict filter, size_t bytes_filter)
{
    __hpvm__hint(DEVICE);
    __hpvm__attributes(3, orig, sol, filter, 1, sol);

    int k1, k2;
    int temp, mul;

//    for (r = 0; r < row_size - 2; r++) {
//        for (c = 0; c < col_size - 2; c++) {
    void *thisNode = __hpvm__getNode();
    long r = __hpvm__getNodeInstanceID_x(thisNode);
    __hpvm__isNonZeroLoop(r, row_size-2);   // 0 <= i < 126
    long c = __hpvm__getNodeInstanceID_y(thisNode); 
    __hpvm__isNonZeroLoop(c, col_size-2);   // 0 <= i < 62
    
            temp = 0;
            for (k1 = 0; k1 < 3; k1++) {
                for (k2 = 0; k2 < 3; k2++) {
                    mul = filter[k1*3 + k2] * orig[(r+k1)*col_size + c + k2];
                    temp += mul;
                }
            }
            sol[(r*col_size) + c] = temp;
//        }
//    }

    __hpvm__return (1, sol);

}

void stencil_wrapper(int *orig, size_t bytes_orig,
                     int *sol, size_t bytes_sol,
                     int *filter, size_t bytes_filter)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(3, orig, sol, filter, 1, sol);

    void *StencilNode = __hpvm__createNodeND(2, stencil, (size_t) row_size-2, (size_t) col_size-2);

    __hpvm__bindIn(StencilNode, 0, 0, 0);
    __hpvm__bindIn(StencilNode, 1, 1, 0);
    __hpvm__bindIn(StencilNode, 2, 2, 0);
    __hpvm__bindIn(StencilNode, 3, 3, 0);
    __hpvm__bindIn(StencilNode, 4, 4, 0);
    __hpvm__bindIn(StencilNode, 5, 5, 0);

}

void StencilRoot(int *orig, size_t bytes_orig,
                 int *sol, size_t bytes_sol,
                 int *filter, size_t bytes_filter)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(3, orig, sol, filter, 1, sol);

    void *StencilWrapperNode = __hpvm__createNodeND(0, stencil_wrapper);

    __hpvm__bindIn(StencilWrapperNode, 0, 0, 0);
    __hpvm__bindIn(StencilWrapperNode, 1, 1, 0);
    __hpvm__bindIn(StencilWrapperNode, 2, 2, 0);
    __hpvm__bindIn(StencilWrapperNode, 3, 3, 0);
    __hpvm__bindIn(StencilWrapperNode, 4, 4, 0);
    __hpvm__bindIn(StencilWrapperNode, 5, 5, 0);

}

int process_input_data(char *file, int *orig, int *filter) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_part = -1;
    int cur_index = 0;

    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            switch (cur_part) {
                case -1: break; // We are just starting
                case 0: if (cur_index == row_size * col_size) break; else return -1;
                default: return -1;
            }
            cur_part++;
            cur_index = 0;
        } else {
            switch (cur_part) {
                case 0: {
                    if (cur_index >= row_size * col_size) return -1;
                    orig[cur_index] = atoi(line);
                    break;
                }
                case 1: {
                    if (cur_index >= f_size) return -1;
                    filter[cur_index] = atoi(line);
                    break;
                }
                default: return -1;
            }
            cur_index++;
        }
    }

    if (cur_index != f_size) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int process_output_data(char *file, int *sol) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int index = -1;
    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (index == -1) {
            index++;
            continue;
        }
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (index >= row_size * col_size) {
            return -1;
        }
        sol[index] = atoi(line);
        index++;
    }

    if (index != row_size * col_size) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int float_eq(float f1, float f2) {
    return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
    // Usage: <executable> <input data> <check data>
    if (argc != 3) {
        printf("Expected parameters <input data> and <check data>\n");
        return 1;
    }

    // Create struct containing DFG inputs / outputs
    RootIn *rootArgs = (RootIn*) malloc_aligned(sizeof(RootIn));

    rootArgs->bytes_orig = sizeof(*rootArgs->orig) * row_size * col_size;
    rootArgs->orig = malloc_aligned(rootArgs->bytes_orig);
    rootArgs->bytes_sol = sizeof(*rootArgs->sol) * row_size * col_size;
    rootArgs->sol = malloc_aligned(rootArgs->bytes_sol);
    rootArgs->bytes_filter = sizeof(*rootArgs->filter) * f_size;
    rootArgs->filter = malloc_aligned(rootArgs->bytes_filter);

    for (int i = 0; i < row_size * col_size; i++) {
        rootArgs->sol[i] = 0;
    }

    if (process_input_data(argv[1], rootArgs->orig, rootArgs->filter) != 0) {
        printf("Invalid input data\n");
        return 1;
    }

    int *expected_sol = (int*) malloc_aligned(rootArgs->bytes_sol);
    if (process_output_data(argv[2], expected_sol) != 0) {
        printf("Invalid expected output data\n");
        return 1;
    }

    __hpvm__init();

    llvm_hpvm_track_mem(rootArgs->orig, rootArgs->bytes_orig);
    llvm_hpvm_track_mem(rootArgs->sol, rootArgs->bytes_sol);
    llvm_hpvm_track_mem(rootArgs->filter, rootArgs->bytes_filter);

    printf("Starting stencil kernel!\n");

    void *DFG = __hpvm__launch(0, StencilRoot, (void*) rootArgs);
    __hpvm__wait(DFG);

    printf("Stencil kernel completed!\n");
    llvm_hpvm_request_mem(rootArgs->sol, rootArgs->bytes_sol);

    llvm_hpvm_untrack_mem(rootArgs->orig);
    llvm_hpvm_untrack_mem(rootArgs->sol);
    llvm_hpvm_untrack_mem(rootArgs->filter);

    int failed = 0;
    for (int i = 0; i < row_size * col_size; i++) {
        if (!float_eq(expected_sol[i], rootArgs->sol[i])) {
            failed = 1;
            printf("Output does not match expected output %d at index %d with incorrect value %d\n", expected_sol[i], i, rootArgs->sol[i]);
        }
    }
    if (failed) {
        printf("Stencil kernel produced incorrect output\n");
    } else {
        printf("Stencil kernel produced expected output\n");
    }

    __hpvm__cleanup();
    return 0;
}
