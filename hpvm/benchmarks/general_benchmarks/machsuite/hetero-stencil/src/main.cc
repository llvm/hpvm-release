#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "heterocc.h"

#define col_size 64
#define row_size 128
#define f_size 9

#define FLOAT_EQ_TOLERANCE 0.001

#include "defs.h"
void *malloc_aligned(size_t size) {
  void *ptr = NULL;
  int err = posix_memalign((void **)&ptr, CACHELINE_SIZE, size);
  assert(err == 0 && "Failed to allocate memory!");
  return ptr;
}

void stencil(int * __restrict orig, size_t bytes_orig,
             int * __restrict sol, size_t bytes_sol,
             int * __restrict filter, size_t bytes_filter)
{
    void *Section = __hetero_section_begin();

    void* Wrapper = __hetero_task_begin(
            /* Num Input Pairs */ 3, 
            orig, bytes_orig, 
            sol, bytes_sol, 
            filter, bytes_filter, 
            /* Num Output Pairs */ 1, sol, bytes_sol,
        "stencil_wrapper_task");
    void *Section_Wrapped = __hetero_section_begin();


    int k1, k2;
    int temp, mul;

    for (size_t r = 0; r < row_size-2; r++) {
        for (size_t c = 0; c < col_size-2; c++) {
            __hetero_parallel_loop(2, 3, 
                orig, bytes_orig, 
                sol, bytes_sol, 
                filter, bytes_filter, 
                1, sol, bytes_sol, "stencil_parallel_loop");
            __hpvm__hint(hpvm::DEVICE);
            {
                temp = 0;
                for (k1 = 0; k1 < 3; k1++) {
                    for (k2 = 0; k2 < 3; k2++) {
                        mul = filter[k1*3 + k2] * orig[(r+k1)*col_size + c + k2];
                        temp += mul;
                    }
                }
                sol[(r*col_size) + c] = temp;
            }
        }
    }


    __hetero_section_end(Section_Wrapped);
    __hetero_task_end(Wrapper);
    __hetero_section_end(Section);

}

int process_input_data(char *file, int *orig, int *filter) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_part = -1;
    int cur_index = 0;

    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            switch (cur_part) {
                case -1: break; // We are just starting
                case 0: if (cur_index == row_size * col_size) break; else return -1;
                default: return -1;
            }
            cur_part++;
            cur_index = 0;
        } else {
            switch (cur_part) {
                case 0: {
                    if (cur_index >= row_size * col_size) return -1;
                    orig[cur_index] = atoi(line);
                    break;
                }
                case 1: {
                    if (cur_index >= f_size) return -1;
                    filter[cur_index] = atoi(line);
                    break;
                }
                default: return -1;
            }
            cur_index++;
        }
    }

    if (cur_index != f_size) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int process_output_data(char *file, int *sol) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int index = -1;
    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (index == -1) {
            index++;
            continue;
        }
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (index >= row_size * col_size) {
            return -1;
        }
        sol[index] = atoi(line);
        index++;
    }

    if (index != row_size * col_size) {
        return -1;
    }
    fclose(fp);
    return 0;
}


int float_eq(float f1, float f2) {
    return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
    // Usage: <executable> <input data> <check data>
    if (argc != 3) {
        printf("Expected parameters <input data> and <check data>\n");
        return 1;
    }


    // Prepare buffers
    size_t bytes_orig = sizeof(int*) * row_size * col_size;
    size_t bytes_sol = sizeof(int*) * row_size * col_size;
    size_t bytes_filter = sizeof(int*) * f_size;

    int* orig = (int*)malloc_aligned(bytes_orig);
    int* sol = (int*)malloc_aligned(bytes_sol);
    int* filter = (int*)malloc_aligned(bytes_filter);

    for (int i = 0; i < row_size * col_size; i++) {
        sol[i] = 0;
    }

    if (process_input_data(argv[1], orig, filter) != 0) {
        printf("Invalid input data\n");
        return 1;
    }

    int *expected_sol = (int*) malloc_aligned(bytes_sol);
    if (process_output_data(argv[2], expected_sol) != 0) {
        printf("Invalid expected output data\n");
        return 1;
    }

    // Launch kernel
    printf("Starting stencil kernel!\n");
    void *DFG = __hetero_launch(
        (void *)stencil,
        /* Num Input Pairs */ 3, 
        orig, bytes_orig, 
        sol, bytes_sol, 
        filter, bytes_filter, 
        /* Num Output Pairs */ 1, sol, bytes_sol);


    __hetero_wait(DFG);
    __hetero_request_mem(sol, bytes_sol);
    printf("Stencil kernel completed!\n");

    // Check results
    int failed = 0;
    for (int i = 0; i < row_size * col_size; i++) {
        if (!float_eq(expected_sol[i], sol[i])) {
            failed = 1;
            printf("Output does not match expected output %d at index %d with incorrect value %d\n", expected_sol[i], i, sol[i]);
        }
    }
    if (failed) {
        printf("Stencil kernel produced incorrect output\n");
    } else {
        printf("Stencil kernel produced expected output\n");
    }

    return 0;
}
