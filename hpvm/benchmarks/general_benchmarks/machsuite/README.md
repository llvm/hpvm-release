# MachSuite Benchmarks
In this directory are several MachSuite benchmarks, ported to HPVM-C.

They can be compiled for CPU, GPU, and FPGA using the targets described in
this [README](/hpvm/benchmarks/general_benchmarks).

This benchmark suite is described in more detail in
[this paper](https://doi.org/10.1109/IISWC.2014.6983050) and the original
versions can be found [here](https://github.com/breagen/MachSuite/).
