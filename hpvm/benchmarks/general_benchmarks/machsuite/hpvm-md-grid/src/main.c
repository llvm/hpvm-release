#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "hpvm.h"
#include "utility.h"

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))

#define nAtoms        256
#define domainEdge    20.0
#define blockSide     4
#define nBlocks       (blockSide*blockSide*blockSide)
#define blockEdge     (domainEdge/((TYPE)blockSide))
#define densityFactor 10
#define lj1           1.5
#define lj2           2.0

#define ind_3(x, y, z)        ((z)+blockSide*((y)+blockSide*(x)))
#define ind_5(x, y, z, df, i) ((i)+3*((df)+densityFactor*(ind_3(x,y,z))))

#define FLOAT_EQ_TOLERANCE 0.001

typedef struct __attribute__((__packed__)) {
    int *n_points; size_t bytes_n_points;
    double *force; size_t bytes_force;
    double *position; size_t bytes_position;
    size_t dummy_output;
} RootIn;

void md(int * restrict n_points, size_t bytes_n_points,
        double * restrict force, size_t bytes_force,
        double * restrict position, size_t bytes_position)
{
    __hpvm__hint(DEVICE);
    __hpvm__attributes(3, n_points, force, position, 1, force);

    int32_t b1_x, b1_y, b1_z; // b0 is the current block, b1 is b0 or a neighboring block
    // p is a point in b0, q is a point in either b0 or b1
    double p_x, p_y, p_z, q_x, q_y, q_z;
    int32_t p_idx, q_idx;
    double dx, dy, dz, r2inv, r6inv, potential, f;

//    // Iterate over the grid, block by block
//    for (b0_x = 0; b0_x < blockSide; b0_x++) {
//    for (b0_y = 0; b0_y < blockSide; b0_y++) {
//    for (b0_z = 0; b0_z < blockSide; b0_z++) {
  void *thisNode = __hpvm__getNode();
  long b0_x = __hpvm__getNodeInstanceID_x(thisNode);
  __hpvm__isNonZeroLoop(b0_x, blockSide);
  long b0_y = __hpvm__getNodeInstanceID_y(thisNode);
  __hpvm__isNonZeroLoop(b0_y, blockSide);
  long b0_z = __hpvm__getNodeInstanceID_z(thisNode);
  __hpvm__isNonZeroLoop(b0_z, blockSide);
    // Iterate over the 3x3x3 (modulo boundary conditions) cube of blocks around b0
    for (b1_x = 0; b1_x < blockSide; b1_x++) {
        __hpvm__isNonZeroLoop(b1_x, blockSide);
    if (b1_x >= MAX(0, b0_x - 1) && b1_x < MIN(blockSide, b0_x + 2)) {
    for (b1_y = 0; b1_y < blockSide; b1_y++) {
        __hpvm__isNonZeroLoop(b1_y, blockSide);
    if (b1_y >= MAX(0, b0_y - 1) && b1_y < MIN(blockSide, b0_y + 2)) {
    for (b1_z = 0; b1_z < blockSide; b1_z++) {
        __hpvm__isNonZeroLoop(b1_z, blockSide);
    if (b1_z >= MAX(0, b0_z - 1) && b1_z < MIN(blockSide, b0_z + 2)) {
        // For all points in b0
        int p_idx_range = n_points[ind_3(b0_x, b0_y, b0_z)];
        int q_idx_range = n_points[ind_3(b1_x, b1_y, b1_z)];
        for (p_idx = 0; p_idx < p_idx_range; p_idx++) {
            // __hpvm__isNonZeroLoop(p_idx, );
            p_x = position[ind_5(b0_x, b0_y, b0_z, p_idx, 0)];
            p_y = position[ind_5(b0_x, b0_y, b0_z, p_idx, 1)];
            p_z = position[ind_5(b0_x, b0_y, b0_z, p_idx, 2)];
            double sum_x = force[ind_5(b0_x, b0_y, b0_z, p_idx, 0)];
            double sum_y = force[ind_5(b0_x, b0_y, b0_z, p_idx, 1)];
            double sum_z = force[ind_5(b0_x, b0_y, b0_z, p_idx, 2)];
            // For all points in b1
            for (q_idx = 0; q_idx < q_idx_range; q_idx++) {
                // __hpvm__isNonZeroLoop(b0_x, );
                q_x = position[ind_5(b1_x, b1_y, b1_z, q_idx, 0)];
                q_y = position[ind_5(b1_x, b1_y, b1_z, q_idx, 1)];
                q_z = position[ind_5(b1_x, b1_y, b1_z, q_idx, 2)];

                // Don't compute our own
                if (q_x != p_x || q_y != p_y || q_z != p_z) {
                    // Compute the LJ-potential
                    dx = p_x - q_x;
                    dy = p_y - q_y;
                    dz = p_z - q_z;
                    r2inv = 1.0/(dx*dx + dy*dy + dz*dz);
                    r6inv = r2inv*r2inv*r2inv;
                    potential = r6inv*(lj1*r6inv - lj2);
                    // Update forces
                    f = r2inv*potential;
                    sum_x += f*dx;
                    sum_y += f*dy;
                    sum_z += f*dz;
                }
            }
            force[ind_5(b0_x, b0_y, b0_z, p_idx, 0)] = sum_x;
            force[ind_5(b0_x, b0_y, b0_z, p_idx, 1)] = sum_y;
            force[ind_5(b0_x, b0_y, b0_z, p_idx, 2)] = sum_z;
        }
    }}}
    }}}
//    }}}
    __hpvm__return (1, force);
}

void md_wrapper(int *n_points, size_t bytes_n_points,
                double *force, size_t bytes_force,
                double *position, size_t bytes_position)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(3, n_points, force, position, 1, force);

    void *MdNode = __hpvm__createNodeND(3, md, (size_t) blockSide, (size_t) blockSide, (size_t) blockSide );
    __hpvm__bindIn(MdNode, 0, 0, 0);
    __hpvm__bindIn(MdNode, 1, 1, 0);
    __hpvm__bindIn(MdNode, 2, 2, 0);
    __hpvm__bindIn(MdNode, 3, 3, 0);
    __hpvm__bindIn(MdNode, 4, 4, 0);
    __hpvm__bindIn(MdNode, 5, 5, 0);

}

void MDRoot(int *n_points, size_t bytes_n_points,
            double *force, size_t bytes_force,
            double *position, size_t bytes_position)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(3, n_points, force, position, 1, force);

    void *MdWrapperNode = __hpvm__createNodeND(0, md_wrapper);
    __hpvm__bindIn(MdWrapperNode, 0, 0, 0);
    __hpvm__bindIn(MdWrapperNode, 1, 1, 0);
    __hpvm__bindIn(MdWrapperNode, 2, 2, 0);
    __hpvm__bindIn(MdWrapperNode, 3, 3, 0);
    __hpvm__bindIn(MdWrapperNode, 4, 4, 0);
    __hpvm__bindIn(MdWrapperNode, 5, 5, 0);

}

int process_input_data(char *file, int *n_points, double *position) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_part = -1;
    int cur_index = 0;

    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            switch (cur_part) {
                case -1: break; // We are just starting
                case 0: if (cur_index == nBlocks) break; else return -1;
                default: return -1;
            }
            cur_part++;
            cur_index = 0;
        } else {
            switch (cur_part) {
                case 0: {
                    if (cur_index >= nBlocks) return -1;
                    n_points[cur_index] = atoi(line);
                    // val[cur_index] = strtof(line, NULL);
                    break;
                }
                case 1: {
                    if (cur_index >= 3 * nBlocks * densityFactor) return -1;
                    position[cur_index] = strtof(line, NULL); break;
                    break;
                }
                default: return -1;
            }
            cur_index++;
        }
    }

    if (cur_index != 3 * nBlocks * densityFactor) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int process_check_data(char *file, double *force) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_index = -1;
    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            if (cur_index != -1) {
                return -1;
            }
            cur_index = 0;
        } else {
            if (cur_index >= 3 * nBlocks * densityFactor) return -1;
            force[cur_index] = strtof(line, NULL);
            cur_index++;
        }
    }

    if (cur_index != 3 * nBlocks * densityFactor) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int float_eq(float f1, float f2) {
    return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
    // Usage: <executable> <input data>
    if (argc != 3) {
        printf("Expected parameters <input data> and <check data>\n");
        return 1;
    }

    // Create struct containing DFG inputs / outputs
    RootIn *rootArgs = (RootIn*) malloc_aligned(sizeof(RootIn));

    rootArgs->bytes_n_points = sizeof(*rootArgs->n_points) * nBlocks;
    rootArgs->n_points = malloc_aligned(rootArgs->bytes_n_points);
    rootArgs->bytes_force = 3 * sizeof(*rootArgs->force) * nBlocks * densityFactor;
    rootArgs->force = malloc_aligned(rootArgs->bytes_force);
    rootArgs->bytes_position = 3 * sizeof(*rootArgs->position) * nBlocks * densityFactor;
    rootArgs->position = malloc_aligned(rootArgs->bytes_position);

    // Initialize force to all zeroes
    for (int i = 0; i < 3 * nBlocks * densityFactor; i++) {
        rootArgs->force[i] = 0.0;
    }

    double *expected_force = malloc_aligned(rootArgs->bytes_force);

    if (process_input_data(argv[1], rootArgs->n_points, rootArgs->position) != 0) {
        return 1;
    }
    if (process_check_data(argv[2], expected_force) != 0) {
        return 1;
    }

    __hpvm__init();

    llvm_hpvm_track_mem(rootArgs->n_points, rootArgs->bytes_n_points);
    llvm_hpvm_track_mem(rootArgs->force, rootArgs->bytes_force);
    llvm_hpvm_track_mem(rootArgs->position, rootArgs->bytes_position);

    printf("Starting MD kernel!\n");

    void *DFG = __hpvm__launch(0, MDRoot, (void*) rootArgs);
    __hpvm__wait(DFG);

    printf("MD kernel completed!\n");
    llvm_hpvm_request_mem(rootArgs->force, rootArgs->bytes_force);

    llvm_hpvm_untrack_mem(rootArgs->n_points);
    llvm_hpvm_untrack_mem(rootArgs->force);
    llvm_hpvm_untrack_mem(rootArgs->position);

    // Check that the output matches with the expected output
    int failed = 0;
    for (int i = 0; i < nBlocks * densityFactor; i++) {
        if (!float_eq(expected_force[3*i], rootArgs->force[3*i]) ||
            !float_eq(expected_force[3*i+1], rootArgs->force[3*i+1]) ||
            !float_eq(expected_force[3*i+2], rootArgs->force[3*i+2]))
        {
            failed = 1;
            printf("Output does not match expected output (%f, %f, %f) at index %d with incorrect value (%f, %f, %f)\n",
                expected_force[3*i], expected_force[3*i+1], expected_force[3*i+2], i,
                rootArgs->force[3*i], rootArgs->force[3*i+1], rootArgs->force[3*i+2]);
        }
    }

    if (failed) {
        printf("MD kernel produced incorrect output\n");
    } else {
        printf("MD kernel produced expected output\n");
    }

    __hpvm__cleanup();
    return 0;
}
