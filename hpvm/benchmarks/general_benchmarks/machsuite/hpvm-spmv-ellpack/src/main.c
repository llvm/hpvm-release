#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "hpvm.h"
#include "utility.h"

#define NNZ 1666
#define N 494
#define L 10

#define FLOAT_EQ_TOLERANCE 0.001

typedef struct __attribute__((__packed__)) {
	double *nzval; size_t bytes_nzval;
	int *cols; size_t bytes_cols;
	double *vec; size_t bytes_vec;
	double *out; size_t bytes_out;
	size_t matrix_dim;
	size_t dummy_output;
} RootIn;

void spmv(double * restrict nzval, size_t bytes_nzval,
		  int * restrict cols, size_t bytes_cols,
		  double * restrict vec, size_t bytes_vec,
		  double * restrict out, size_t bytes_out,
		  size_t matrix_dim)
{
	__hpvm__hint(DEVICE);
	__hpvm__attributes(3, nzval, cols, vec, 1, out);

    double Si;
//    for (int i = 0; i < matrix_dim; i++) {
    void *thisNode = __hpvm__getNode();
    long i = __hpvm__getNodeInstanceID_x(thisNode);
	__hpvm__isNonZeroLoop(i, N);
	
    double sum = 0;
        for (int j = 0; j < L; j++) {
			// __hpvm__isNonZeroLoop(j, L);
            Si = nzval[j + i*L] * vec[cols[j + i*L]];
            sum += Si;
        }
        out[i] = sum;
//    }
	__hpvm__return(1, out);
}

void spmv_wrapper(double *nzval, size_t bytes_nzval,
				  int *cols, size_t bytes_cols,
				  double *vec, size_t bytes_vec,
				  double *out, size_t bytes_out,
				  size_t matrix_dim)
{
	__hpvm__hint(CPU_TARGET);
	__hpvm__attributes(3, nzval, cols, vec, 1, out);

	void *SpmvNode = __hpvm__createNodeND(1, spmv, (size_t) N);

	__hpvm__bindIn(SpmvNode, 0, 0, 0);
	__hpvm__bindIn(SpmvNode, 1, 1, 0);
	__hpvm__bindIn(SpmvNode, 2, 2, 0);
	__hpvm__bindIn(SpmvNode, 3, 3, 0);
	__hpvm__bindIn(SpmvNode, 4, 4, 0);
	__hpvm__bindIn(SpmvNode, 5, 5, 0);
	__hpvm__bindIn(SpmvNode, 6, 6, 0);
	__hpvm__bindIn(SpmvNode, 7, 7, 0);
	__hpvm__bindIn(SpmvNode, 8, 8, 0);

}

void SPMVRoot(double *nzval, size_t bytes_nzval,
			  int *cols, size_t bytes_cols,
			  double *vec, size_t bytes_vec,
			  double *out, size_t bytes_out,
			  size_t matrix_dim)
{
	__hpvm__hint(CPU_TARGET);
	__hpvm__attributes(3, nzval, cols, vec, 1, out);

	void *SpmvWrapperNode = __hpvm__createNodeND(0, spmv_wrapper);

	__hpvm__bindIn(SpmvWrapperNode, 0, 0, 0);
	__hpvm__bindIn(SpmvWrapperNode, 1, 1, 0);
	__hpvm__bindIn(SpmvWrapperNode, 2, 2, 0);
	__hpvm__bindIn(SpmvWrapperNode, 3, 3, 0);
	__hpvm__bindIn(SpmvWrapperNode, 4, 4, 0);
	__hpvm__bindIn(SpmvWrapperNode, 5, 5, 0);
	__hpvm__bindIn(SpmvWrapperNode, 6, 6, 0);
	__hpvm__bindIn(SpmvWrapperNode, 7, 7, 0);
	__hpvm__bindIn(SpmvWrapperNode, 8, 8, 0);

}

int process_input_data(char *file, double *nzval, int *cols, double *vec) {
	FILE *fp;
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}

	int cur_part = -1;
	int cur_index = 0;

	ssize_t bytes_read;
	char *line = NULL;
	size_t len = 0;
	while ((bytes_read = getline(&line, &len, fp)) != -1) {
		if (line[bytes_read - 1] == '\n') {
			line[bytes_read - 1] = '\0';
		}
		if (strcmp(line, "%%") == 0) {
			switch (cur_part) {
				case -1: break; // We are just starting
				case 0: if (cur_index == N*L) break; else return -1;
				case 1: if (cur_index == N*L) break; else return -1;
				default: return -1;
			}
			cur_part++;
			cur_index = 0;
		} else {
			switch (cur_part) {
				case 0: {
					if (cur_index >= N*L) return -1;
					nzval[cur_index] = strtof(line, NULL);
					break;
				}
				case 1: {
					if (cur_index >= N*L) return -1;
					cols[cur_index] = atoi(line);
					break;
				}
				case 2: {
					if (cur_index >= N) return -1;
					vec[cur_index] = strtof(line, NULL);
					break;
				}
				default: return -1;
			}
			cur_index++;
		}
	}

	if (cur_index != N) {
		return -1;
	}
	fclose(fp);
	return 0;
}

int process_output_data(char *file, float *out) {
	FILE *fp;
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}

	int index = -1;
	ssize_t bytes_read;
	char *line = NULL;
	size_t len = 0;
	while ((bytes_read = getline(&line, &len, fp)) != -1) {
		if (index == -1) {
			index++;
			continue;
		}
		if (line[bytes_read - 1] == '\n') {
			line[bytes_read - 1] = '\0';
		}
		if (index >= N) {
			return -1;
		}
		out[index] = strtof(line, NULL);
		index++;
	}

	if (index != N) {
		printf("%d is not %d\n", index - 1, N);
		return -1;
	}
	fclose(fp);
	return 0;
}

int float_eq(float f1, float f2) {
	return (f1 < f2 + FLOAT_EQ_TOLERANCE && f1 > f2 - FLOAT_EQ_TOLERANCE);
}

int main(int argc, char *argv[]) {
	// Usage: <executable> <input data> <check data>
	if (argc != 3) {
		printf("Expected parameters <input data> and <check data>\n");
		return 1;
	}

	// Create struct containing DFG inputs / outputs
	RootIn *rootArgs = (RootIn*) malloc_aligned(sizeof(RootIn));

	rootArgs->bytes_nzval = N * L * sizeof(*rootArgs->nzval);
	rootArgs->nzval = malloc_aligned(rootArgs->bytes_nzval);
	rootArgs->bytes_cols = N * L * sizeof(*rootArgs->cols);
	rootArgs->cols = malloc_aligned(rootArgs->bytes_cols);
	rootArgs->bytes_vec = N * sizeof(*rootArgs->vec);
	rootArgs->vec = malloc_aligned(rootArgs->bytes_vec);
	rootArgs->bytes_out = N * sizeof(*rootArgs->out);
	rootArgs->out = malloc_aligned(rootArgs->bytes_out);
	rootArgs->matrix_dim = N;

	if (process_input_data(argv[1], rootArgs->nzval, rootArgs->cols, rootArgs->vec) != 0) {
		printf("Invalid input data\n");
		return 1;
	}

	float *expected_output = (float*) malloc_aligned(rootArgs->bytes_out);
	if (process_output_data(argv[2], expected_output) != 0) {
		printf("Invalid expected output data\n");
		return 1;
	}

	__hpvm__init();

	llvm_hpvm_track_mem(rootArgs->nzval, rootArgs->bytes_nzval);
	llvm_hpvm_track_mem(rootArgs->cols, rootArgs->bytes_cols);
	llvm_hpvm_track_mem(rootArgs->vec, rootArgs->bytes_vec);
	llvm_hpvm_track_mem(rootArgs->out, rootArgs->bytes_out);

	printf("Starting SPMV ELLPACK kernel!\n");

	void *DFG = __hpvm__launch(0, SPMVRoot, (void*) rootArgs);
	__hpvm__wait(DFG);

	printf("SPMV ELLPACK kernel completed!\n");
	llvm_hpvm_request_mem(rootArgs->out, rootArgs->bytes_out);

	llvm_hpvm_untrack_mem(rootArgs->nzval);
	llvm_hpvm_untrack_mem(rootArgs->cols);
	llvm_hpvm_untrack_mem(rootArgs->vec);
	llvm_hpvm_untrack_mem(rootArgs->out);

	int failed = 0;
	for (int i = 0; i < N; i++) {
		if (!float_eq(expected_output[i], rootArgs->out[i])) {
			failed = 1;
			printf("Output does not match expected output %f at index %d with incorrect value %f\n", expected_output[i], i, rootArgs->out[i]);
		}
	}
	if (failed) {
		printf("SPMV ELLPACK kernel produced incorrect output\n");
	} else {
		printf("SPMV ELLPACK kernel produced expected output\n");
	}

	__hpvm__cleanup();
	return 0;
}
