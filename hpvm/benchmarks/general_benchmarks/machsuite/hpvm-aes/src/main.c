#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdint.h>

#include "hpvm.h"
#include "utility.h"

#define KEY_SIZE 32
#define BUF_SIZE 16

#define F(x)   (((x)<<1) ^ ((((x)>>7) & 1) * 0x1b))
#define FD(x)  (((x) >> 1) ^ (((x) & 1) ? 0x8d : 0))

#define rj_sbox(x) sbox[(x)]
#define rj_xtime(x) (((x) & 0x80) ? (((x) << 1) ^ 0x1b) : ((x) << 1));

uint8_t sbox[256] = {
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5,
    0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
    0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc,
    0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a,
    0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0,
    0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b,
    0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85,
    0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
    0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17,
    0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88,
    0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c,
    0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9,
    0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6,
    0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e,
    0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94,
    0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68,
    0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
};

typedef struct __attribute__((__packed__)) {
    uint8_t *key; size_t bytes_key;
    uint8_t *enckey; size_t bytes_enckey;
    uint8_t *deckey; size_t bytes_deckey;
    uint8_t *sbox; size_t bytes_sbox;
    uint8_t *k; size_t bytes_k;
    uint8_t *buf; size_t bytes_buf;
    size_t dummy_output;
} RootIn;

void aes(uint8_t *key, size_t bytes_key,
         uint8_t *enckey, size_t bytes_enckey,
         uint8_t *deckey, size_t bytes_deckey,
         uint8_t *sbox, size_t bytes_sbox,
         uint8_t *k, size_t bytes_k,
         uint8_t *buf, size_t bytes_buf)
{
    __hpvm__hint(DEVICE);
    __hpvm__attributes(6, key, enckey, deckey, sbox, k, buf, 1, buf, 3, key, KEY_SIZE, enckey, KEY_SIZE, deckey, KEY_SIZE);

    uint8_t rcon = 1;

    for (int i = 0; i < KEY_SIZE; i++) {
        enckey[i] = deckey[i] = k[i];
    }
    for (int i = 0; i < 7; i++) {
        // aes_expandEncKey(deckey, rcon)
        deckey[0] ^= rj_sbox(deckey[29]) ^ (rcon);
        deckey[1] ^= rj_sbox(deckey[30]);
        deckey[2] ^= rj_sbox(deckey[31]);
        deckey[3] ^= rj_sbox(deckey[28]);
        rcon = F(rcon);

        for (int j = 1; j < 4; j++) {
            deckey[4*j] ^= deckey[4*j-4], deckey[4*j+1] ^= deckey[4*j-3], deckey[4*j+2] ^= deckey[4*j-2], deckey[4*j+3] ^= deckey[4*j-1];
        }

        deckey[16] ^= rj_sbox(deckey[12]);
        deckey[17] ^= rj_sbox(deckey[13]);
        deckey[18] ^= rj_sbox(deckey[14]);
        deckey[19] ^= rj_sbox(deckey[15]);

        for (int j = 5; j < 8; j++) {
            deckey[4*j] ^= deckey[4*j-4], deckey[4*j+1] ^= deckey[4*j-3], deckey[4*j+2] ^= deckey[4*j-2], deckey[4*j+3] ^= deckey[4*j-1];
        }
    }

    // aes_addRoundKey_cpy(buf, enckey, key)
    for (int i = 0; i < BUF_SIZE; i++) {
        buf[i] ^= (key[i] = enckey[i]), key[16+i] = enckey[16 + i];
    }
    rcon = 1;
    for (int i = 1; i < 14; i++) {
        // aes_subBytes(buf)
        for (int j = 0; j < BUF_SIZE; j++) {
            buf[j] = rj_sbox(buf[j]);
        }
        // aes_shiftRows(buf)
        uint8_t temp;
        temp = buf[1];  buf[1]  = buf[5];  buf[5]  = buf[9];  buf[9]  = buf[13]; buf[13] = temp;
        temp = buf[10]; buf[10] = buf[2];  buf[2]  = temp;
        temp = buf[3];  buf[3]  = buf[15]; buf[15] = buf[11]; buf[11] = buf[7];  buf[7] = temp;
        temp = buf[14]; buf[14] = buf[6];  buf[6]  = temp;
        // aes_mixColumns(buf)
        for (int j = 0; j < 4; j++) {
            uint8_t a, b, c, d, e;
            a = buf[4*j]; b = buf[4*j + 1]; c = buf[4*j + 2]; d = buf[4*j + 3];
            e = a ^ b ^ c ^ d;
            buf[4*j] ^= e ^ rj_xtime(a^b);   buf[4*j+1] ^= e ^ rj_xtime(b^c);
            buf[4*j+2] ^= e ^ rj_xtime(c^d); buf[4*j+3] ^= e ^ rj_xtime(d^a);
        }
        int offset = 16;
        if ((i & 1) == 0) {
            // aes_expandEncKey(key, rcon)
            key[0] ^= rj_sbox(key[29]) ^ (rcon);
            key[1] ^= rj_sbox(key[30]);
            key[2] ^= rj_sbox(key[31]);
            key[3] ^= rj_sbox(key[28]);
            rcon = F(rcon);

            for (int j = 1; j < 4; j++) {
                key[4*j] ^= key[4*j-4], key[4*j+1] ^= key[4*j-3], key[4*j+2] ^= key[4*j-2], key[4*j+3] ^= key[4*j-1];
            }

            key[16] ^= rj_sbox(key[12]);
            key[17] ^= rj_sbox(key[13]);
            key[18] ^= rj_sbox(key[14]);
            key[19] ^= rj_sbox(key[15]);

            for (int j = 5; j < 8; j++) {
                key[4*j] ^= key[4*j-4], key[4*j+1] ^= key[4*j-3], key[4*j+2] ^= key[4*j-2], key[4*j+3] ^= key[4*j-1];
            }
            offset = 0;
        }
        // aes_addRoundKey(buf, &key[offset])
        for (int j = 0; j < BUF_SIZE; j++) {
            buf[j] ^= key[j + offset];
        }
    }
    // aes_subBytes(buf)
    for (int j = 0; j < BUF_SIZE; j++) {
        buf[j] = rj_sbox(buf[j]);
    }
    // aes_shiftRows(buf)
    uint8_t temp;
    temp = buf[1];  buf[1]  = buf[5];  buf[5]  = buf[9];  buf[9]  = buf[13]; buf[13] = temp;
    temp = buf[10]; buf[10] = buf[2];  buf[2]  = temp;
    temp = buf[3];  buf[3]  = buf[15]; buf[15] = buf[11]; buf[11] = buf[7];  buf[7] = temp;
    temp = buf[14]; buf[14] = buf[6];  buf[6]  = temp;
    // aes_expandEncKey(key, rcon)
    key[0] ^= rj_sbox(key[29]) ^ (rcon);
    key[1] ^= rj_sbox(key[30]);
    key[2] ^= rj_sbox(key[31]);
    key[3] ^= rj_sbox(key[28]);
    rcon = F(rcon);

    for (int j = 1; j < 4; j++) {
        key[4*j] ^= key[4*j-4], key[4*j+1] ^= key[4*j-3], key[4*j+2] ^= key[4*j-2], key[4*j+3] ^= key[4*j-1];
    }

    key[16] ^= rj_sbox(key[12]);
    key[17] ^= rj_sbox(key[13]);
    key[18] ^= rj_sbox(key[14]);
    key[19] ^= rj_sbox(key[15]);

    for (int j = 5; j < 8; j++) {
        key[4*j] ^= key[4*j-4], key[4*j+1] ^= key[4*j-3], key[4*j+2] ^= key[4*j-2], key[4*j+3] ^= key[4*j-1];
    }
    // aes_addRoundKey(buf, key)
    for (int i = 0; i < BUF_SIZE; i++) {
        buf[i] ^= key[i];
    }

    __hpvm__return(1, buf);
}

void aes_wrapper(uint8_t *key, size_t bytes_key,
                 uint8_t *enckey, size_t bytes_enckey,
                 uint8_t *deckey, size_t bytes_deckey,
                 uint8_t *sbox, size_t bytes_sbox,
                 uint8_t *k, size_t bytes_k,
                 uint8_t *buf, size_t bytes_buf)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(6, key, enckey, deckey, sbox, k, buf, 1, buf, 3, key, KEY_SIZE, enckey, KEY_SIZE, deckey, KEY_SIZE);
    void *AesNode = __hpvm__createNodeND(1, aes, (size_t) 1);

    __hpvm__bindIn(AesNode, 0, 0, 0);
    __hpvm__bindIn(AesNode, 1, 1, 0);
    __hpvm__bindIn(AesNode, 2, 2, 0);
    __hpvm__bindIn(AesNode, 3, 3, 0);
    __hpvm__bindIn(AesNode, 4, 4, 0);
    __hpvm__bindIn(AesNode, 5, 5, 0);
    __hpvm__bindIn(AesNode, 6, 6, 0);
    __hpvm__bindIn(AesNode, 7, 7, 0);
    __hpvm__bindIn(AesNode, 8, 8, 0);
    __hpvm__bindIn(AesNode, 9, 9, 0);
    __hpvm__bindIn(AesNode, 10, 10, 0);
    __hpvm__bindIn(AesNode, 11, 11, 0);

    __hpvm__bindOut(AesNode, 0, 0, 0);
}

void AESRoot(uint8_t *key, size_t bytes_key,
             uint8_t *enckey, size_t bytes_enckey,
             uint8_t *deckey, size_t bytes_deckey,
             uint8_t *sbox, size_t bytes_sbox,
             uint8_t *k, size_t bytes_k,
             uint8_t *buf, size_t bytes_buf)
{
    __hpvm__hint(CPU_TARGET);
    __hpvm__attributes(6, key, enckey, deckey, sbox, k, buf, 1, buf, 3, key, KEY_SIZE, enckey, KEY_SIZE, deckey, KEY_SIZE);

    void *AesWrapperNode = __hpvm__createNodeND(0, aes_wrapper);

    __hpvm__bindIn(AesWrapperNode, 0, 0, 0);
    __hpvm__bindIn(AesWrapperNode, 1, 1, 0);
    __hpvm__bindIn(AesWrapperNode, 2, 2, 0);
    __hpvm__bindIn(AesWrapperNode, 3, 3, 0);
    __hpvm__bindIn(AesWrapperNode, 4, 4, 0);
    __hpvm__bindIn(AesWrapperNode, 5, 5, 0);
    __hpvm__bindIn(AesWrapperNode, 6, 6, 0);
    __hpvm__bindIn(AesWrapperNode, 7, 7, 0);
    __hpvm__bindIn(AesWrapperNode, 8, 8, 0);
    __hpvm__bindIn(AesWrapperNode, 9, 9, 0);
    __hpvm__bindIn(AesWrapperNode, 10, 10, 0);
    __hpvm__bindIn(AesWrapperNode, 11, 11, 0);

    __hpvm__bindOut(AesWrapperNode, 0, 0, 0); // Output value of num_matches
}

int process_input_data(char *file, uint8_t *key, uint8_t *buf) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int cur_part = -1;
    int cur_index = 0;

    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (strcmp(line, "%%") == 0) {
            switch (cur_part) {
                case -1: break; // We are just starting
                case 0: if (cur_index == KEY_SIZE) break; else return -1;
                default: return -1;
            }
            cur_part++;
            cur_index = 0;
        } else {
            switch (cur_part) {
                case 0: {
                    if (cur_index >= KEY_SIZE) return -1;
                    key[cur_index] = atoi(line);
                    break;
                }
                case 1: {
                    if (cur_index >= BUF_SIZE) return -1;
                    buf[cur_index] = atoi(line);
                    break;
                }
                default: return -1;
            }
            cur_index++;
        }
    }

    if (cur_index != BUF_SIZE) {
        return -1;
    }
    fclose(fp);
    return 0;
}

int process_output_data(char *file, uint8_t *buf) {
    FILE *fp;
    fp = fopen(file, "r");
    if (fp == NULL) {
        return -1;
    }

    int index = -1;
    ssize_t bytes_read;
    char *line = NULL;
    size_t len = 0;
    while ((bytes_read = getline(&line, &len, fp)) != -1) {
        if (index == -1) {
            index++;
            continue;
        }
        if (line[bytes_read - 1] == '\n') {
            line[bytes_read - 1] = '\0';
        }
        if (index >= BUF_SIZE) {
            return -1;
        }
        buf[index] = atoi(line);
        index++;
    }

    if (index != BUF_SIZE) {
        printf("%d is not %d\n", index - 1, BUF_SIZE);
        return -1;
    }
    fclose(fp);
    return 0;
}

int main(int argc, char *argv[]) {
    // Usage: <executable> <input data> <check data>
    if (argc != 3) {
        printf("Expected parameters <input data> and <check data>\n");
        return 1;
    }

    RootIn *rootArgs = malloc_aligned(sizeof(RootIn));

    rootArgs->bytes_key = KEY_SIZE;
    rootArgs->key = malloc_aligned(sizeof(*rootArgs->key) * rootArgs->bytes_key);
    rootArgs->bytes_enckey = KEY_SIZE;
    rootArgs->enckey = malloc_aligned(sizeof(*rootArgs->enckey) * rootArgs->bytes_enckey);
    rootArgs->bytes_deckey = KEY_SIZE;
    rootArgs->deckey = malloc_aligned(sizeof(*rootArgs->deckey) * rootArgs->bytes_deckey);
    rootArgs->bytes_sbox = 256;
    rootArgs->sbox = sbox;
    rootArgs->bytes_k = KEY_SIZE;
    rootArgs->k = malloc_aligned(sizeof(*rootArgs->k) * rootArgs->bytes_k);
    rootArgs->bytes_buf = BUF_SIZE;
    rootArgs->buf = malloc_aligned(sizeof(*rootArgs->buf) * rootArgs->bytes_buf);

    uint8_t *expected_buf = malloc_aligned(sizeof(*expected_buf) * BUF_SIZE);

    if (process_input_data(argv[1], rootArgs->k, rootArgs->buf) != 0) {
        printf("Invalid input data\n");
        return 1;
    }
    if (process_output_data(argv[2], expected_buf) != 0) {
        printf("Invalid check data\n");
        return 1;
    }

    __hpvm__init();

    llvm_hpvm_track_mem(rootArgs->key, rootArgs->bytes_key);
    llvm_hpvm_track_mem(rootArgs->enckey, rootArgs->bytes_enckey);
    llvm_hpvm_track_mem(rootArgs->deckey, rootArgs->bytes_deckey);
    llvm_hpvm_track_mem(rootArgs->sbox, rootArgs->bytes_sbox);
    llvm_hpvm_track_mem(rootArgs->k, rootArgs->bytes_k);
    llvm_hpvm_track_mem(rootArgs->buf, rootArgs->bytes_buf);

    printf("Starting AES kernel!\n");

    void *DFG = __hpvm__launch(0, AESRoot, (void*) rootArgs);
    __hpvm__wait(DFG);

    printf("AES kernel completed!\n");
    llvm_hpvm_request_mem(rootArgs->buf, rootArgs->bytes_buf);

    int failed = 0;
    for (int i = 0; i < BUF_SIZE; i++) {
        if (rootArgs->buf[i] != expected_buf[i]) {
            failed = 1;
            printf("At index %d: expected value %x but got %x\n", i, expected_buf[i], rootArgs->buf[i]);
        }
    }

    if (failed) {
        printf("AES kernel gave incorrect results!\n");
    } else {
        printf("AES kernel gave expected results\n");
    }

    llvm_hpvm_untrack_mem(rootArgs->key);
    llvm_hpvm_untrack_mem(rootArgs->enckey);
    llvm_hpvm_untrack_mem(rootArgs->deckey);
    llvm_hpvm_untrack_mem(rootArgs->sbox);
    llvm_hpvm_untrack_mem(rootArgs->k);
    llvm_hpvm_untrack_mem(rootArgs->buf);

    __hpvm__cleanup();
    return 0;
}
