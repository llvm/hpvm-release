#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <heterocc.h>

#include <chrono>

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection requires sudo
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE 1
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

#define deg_angle_of_attack (0.0f)
#define NNB (4)
#define GAMMA (1.4f)
#define ff_mach (1.2f)
#define NDIM (3)
#define RK (3)
#define VAR_DENSITY (0)
#define VAR_MOMENTUM (1)
#define VAR_DENSITY_ENERGY (VAR_MOMENTUM + NDIM)
#define NVAR (VAR_DENSITY_ENERGY + 1)
#define NELR_NUM 232544

#define FLOAT_COMPARE (1e-05)

void *alignedMalloc(size_t size) {
  void *ptr = NULL;
  if ( posix_memalign (&ptr, 64, size) )
  {
    fprintf(stderr, "Aligned Malloc failed due to insufficient memory.\n");
    exit(-1);
  }
  return ptr;
}


/*******************************************STRUCTS*******************************************************/
/*********************************************************************************************************/


struct Variables {
    float *density;
    float *momentum_x;
    float *momentum_y;
    float *momentum_z;
    float *energy;
    int n;

    Variables() {}
    void allocate(int n) {
        density = (float *) __hetero_malloc(sizeof(float) * n);
        momentum_x = (float *) __hetero_malloc(sizeof(float) * n);
        momentum_y = (float *) __hetero_malloc(sizeof(float) * n);
        momentum_z = (float *) __hetero_malloc(sizeof(float) * n);
        energy = (float *) __hetero_malloc(sizeof(float) * n);
        this -> n = n;
    }

    void allocate_cpu(int n) {
        density = (float *) malloc(sizeof(float) * n);
        momentum_x = (float *) malloc(sizeof(float) * n);
        momentum_y = (float *) malloc(sizeof(float) * n);
        momentum_z = (float *) malloc(sizeof(float) * n);
        energy = (float *) malloc(sizeof(float) * n);
        this -> n = n;
    }

    void deallocate() {
        __hetero_free(density);
        __hetero_free(momentum_x);
        __hetero_free(momentum_y);
        __hetero_free(momentum_z);
        __hetero_free(energy);
    }
};

/*******************************************UTILITY FUNCTIONS************************************************/
/***********************************************************************************************************/


void copy(Variables &dst, Variables &src, int n) {
    for (int i = 0; i < n; i++) {
        *(dst.density + i) = *(src.density + i);
        *(dst.energy + i) = *(src.energy + i);
        *(dst.momentum_x + i) = *(src.momentum_x + i);
        *(dst.momentum_y + i) = *(src.momentum_y + i);
        *(dst.momentum_z + i) = *(src.momentum_z + i);
    }
}


void compute_flux_contribution(
  float density, float* momentum, float density_energy, float pressure, float*
  velocity, float *fc_momentum_x, float *fc_momentum_y, float *fc_momentum_z,
  float *fc_density_energy) {

    fc_momentum_x[0] = velocity[0] * momentum[0] + pressure;
    fc_momentum_x[1] = velocity[0] * momentum[1];
    fc_momentum_x[2] = velocity[0] * momentum[2];

    fc_momentum_y[0] = fc_momentum_x[1];
    fc_momentum_y[1] = velocity[1] * momentum[1] + pressure;
    fc_momentum_y[2] = velocity[1] * momentum[2];

    fc_momentum_z[0] = fc_momentum_x[2];
    fc_momentum_z[1] = fc_momentum_y[2];
    fc_momentum_z[2] = velocity[2] * momentum[2] + pressure;

    float de_p = density_energy + pressure;
    fc_density_energy[0] = velocity[0] * de_p;
    fc_density_energy[1] = velocity[1] * de_p;
    fc_density_energy[2] = velocity[2] * de_p;
}


/* fill_variable(nelr, ff_variable, hv); */
void fill_variable(int nelr, const float *ff_variable, Variables &variables) {
    for (int i = 0; i < nelr; ++i) {
        variables.density[i]  = ff_variable[VAR_DENSITY];
        variables.momentum_x[i] = ff_variable[VAR_MOMENTUM + 0];
        variables.momentum_y[i] = ff_variable[VAR_MOMENTUM + 1];
        variables.momentum_z[i] = ff_variable[VAR_MOMENTUM + 2];
        variables.energy[i] = ff_variable[VAR_DENSITY_ENERGY];
    }
}


template <typename T>
void debug_print(T *arr, std::string str) {
    std::cout << ("\nPrinting " + str + ":\n");
    for (int i = 0; i < 100; i++)
        std::cout << arr[i] << " ";
    std::cout << "\n-----------------------------------------------------------\n";
}


void compute_velocity(float  density, float *momentum, float *velocity){
    velocity[0] = momentum[0] / density;
    velocity[1] = momentum[1] / density;
    velocity[2] = momentum[2] / density;
}

float compute_speed_sqd(float *velocity){
    return velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2];
}

float compute_pressure(float density, float density_energy, float speed_sqd){
    return (GAMMA - 1.0)*(density_energy - 0.5*density*speed_sqd);
}

float compute_speed_of_sound(float density, float pressure){
    //return sqrtf(float(GAMMA)*pressure/density);
    return sqrt(GAMMA*pressure/density);
}


void dump(Variables variables, int nel, int nelr) {
    {
        std::ofstream file("density");
        file << nel << " " << nelr << std::endl;
        for(int i = 0; i < nel; i++) {
            file << variables.density[i] << std::endl;
        }
    }

    {
        std::ofstream file("momentum");
        file << nel << " " << nelr << std::endl;
        for(int i = 0; i < nel; i++)
        {
            file << variables.momentum_x[i] << " ";
            file << variables.momentum_y[i] << " ";
            file << variables.momentum_z[i] << " ";      
            file << std::endl;
        }
    }
  
    {
        std::ofstream file("density_energy");
        file << nel << " " << nelr << std::endl;
        for(int i = 0; i < nel; i++) {
            file << variables.energy[i] << std::endl;
        }
    }
}


bool compareFloat(double a, double b) {
    if (abs(a - b) < FLOAT_COMPARE)
        return true;
    return false;
}


void compute_step_factor_cpu(float *v_density, float *v_momentum_x, float *v_momentum_y, float *v_momentum_z,
                                float *v_energy, float *areas, float *step_factors, int nelr) {

    for (int i = 0; i < nelr; ++i) {
        float density = v_density[i];
        float momentum[3];
        momentum[0] = v_momentum_x[i];
        momentum[1] = v_momentum_y[i];
        momentum[2] = v_momentum_z[i];

        float density_energy = v_energy[i];

        float velocity[3];
        compute_velocity(density, momentum, velocity);
        float speed_sqd = compute_speed_sqd(velocity);
        //float speed_sqd;
        //compute_speed_sqd(velocity, speed_sqd);
        float pressure = compute_pressure(density, density_energy, speed_sqd);
        float speed_of_sound = compute_speed_of_sound(density, pressure);

        // dt = float(0.5f) * sqrtf(areas[i]) /  (||v|| + c).... but when we do time stepping, this later would need to be divided by the area, so we just do it all at once
        //step_factors[i] = 0.5 / (sqrtf(areas[i]) * (sqrtf(speed_sqd) + speed_of_sound));
        step_factors[i] = 0.5 / (sqrt(areas[i]) * (sqrt(speed_sqd) + speed_of_sound));
    }
}


void compute_flux_cpu(int *elements_surrounding_elements, float *normals, float *v_density, float *v_momentum_x, float *v_momentum_y, float *v_momentum_z, float *v_energy,
                        float *ff_variable, float *fluxes_density, float *fluxes_momentum_x, float *fluxes_momentum_y, float *fluxes_momentum_z, float *fluxes_energy,
                        float *ff_flux_contribution_density_energy, float *ff_flux_contribution_momentum_x, float *ff_flux_contribution_momentum_y,
                        float *ff_flux_contribution_momentum_z, int nelr) {

    const float smoothing_coefficient = 0.2f;

    for (int i = 0; i < nelr; ++i) {

        int j, nb;
        float normal[3]; float normal_len;
        float factor;

        float density_i = v_density[i];
        float momentum_i[3];
        momentum_i[0] = v_momentum_x[i];
        momentum_i[1] = v_momentum_y[i];
        momentum_i[2] = v_momentum_z[i];

        float density_energy_i = v_energy[i];

        float velocity_i[3];
        compute_velocity(density_i, momentum_i, velocity_i);
        float speed_sqd_i = compute_speed_sqd(velocity_i);
        //float speed_sqd_i;
        //compute_speed_sqd(velocity_i, speed_sqd_i);
        //float speed_i                              = sqrtf(speed_sqd_i);
        float speed_i = sqrt(speed_sqd_i);
        float pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
        float speed_of_sound_i                     = compute_speed_of_sound(density_i, pressure_i);
        float flux_contribution_i_momentum_x[3],
              flux_contribution_i_momentum_y[3],
              flux_contribution_i_momentum_z[3];
        float flux_contribution_i_density_energy[3];
        compute_flux_contribution(density_i, momentum_i, density_energy_i,
          pressure_i, velocity_i, flux_contribution_i_momentum_x,
          flux_contribution_i_momentum_y, flux_contribution_i_momentum_z,
          flux_contribution_i_density_energy);

        float flux_i_density = 0.0;
        float flux_i_momentum[3];
        flux_i_momentum[0] = 0.0;
        flux_i_momentum[1] = 0.0;
        flux_i_momentum[2] = 0.0;
        float flux_i_density_energy = 0.0;

        float velocity_nb[3];
        float density_nb, density_energy_nb;
        float momentum_nb[3];
        float flux_contribution_nb_momentum_x[3],
              flux_contribution_nb_momentum_y[3],
              flux_contribution_nb_momentum_z[3];
        float flux_contribution_nb_density_energy[3];
        float speed_sqd_nb, speed_of_sound_nb, pressure_nb;

        // Logic not sufficient for de5net_a7
        //#pragma unroll
        for(j = 0; j < NNB; j++) {
            nb = elements_surrounding_elements[i + j*nelr];
            normal[0] = normals[i + (j + 0*NNB)*nelr];
            normal[1] = normals[i + (j + 1*NNB)*nelr];
            normal[2] = normals[i + (j + 2*NNB)*nelr];
            //normal_len = sqrtf(normal.x*normal.x + normal[1]*normal[1] + normal[2]*normal[2]);
            normal_len = sqrt(normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2]);

            if(nb >= 0) {   // a legitimate neighbor
                density_nb = v_density[nb];
                momentum_nb[0] = v_momentum_x[nb];
                momentum_nb[1] = v_momentum_y[nb];
                momentum_nb[2] = v_momentum_z[nb];
                density_energy_nb = v_energy[nb];
                
                compute_velocity(density_nb, momentum_nb, velocity_nb);
                
                speed_sqd_nb = compute_speed_sqd(velocity_nb);
                pressure_nb = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
                speed_of_sound_nb = compute_speed_of_sound(density_nb, pressure_nb);

                compute_flux_contribution(density_nb, momentum_nb,
                  density_energy_nb, pressure_nb, velocity_nb,
                  flux_contribution_nb_momentum_x,
                  flux_contribution_nb_momentum_y,
                  flux_contribution_nb_momentum_z,
                  flux_contribution_nb_density_energy);

                // artificial viscosity
                factor = -normal_len*smoothing_coefficient*0.5*(speed_i + sqrt(speed_sqd_nb) + speed_of_sound_i + speed_of_sound_nb);
                flux_i_density += factor*(density_i-density_nb);
                flux_i_density_energy += factor*(density_energy_i-density_energy_nb);
                flux_i_momentum[0] += factor*(momentum_i[0]-momentum_nb[0]);
                flux_i_momentum[1] += factor*(momentum_i[1]-momentum_nb[1]);
                flux_i_momentum[2] += factor*(momentum_i[2]-momentum_nb[2]);

                // accumulate cell-centered fluxes
                factor = 0.5*normal[0];
                flux_i_density += factor*(momentum_nb[0]+momentum_i[0]);
                flux_i_density_energy += factor*(flux_contribution_nb_density_energy[0]+flux_contribution_i_density_energy[0]);
                flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[0]+flux_contribution_i_momentum_x[0]);
                flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[0]+flux_contribution_i_momentum_y[0]);
                flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[0]+flux_contribution_i_momentum_z[0]);

                factor = 0.5*normal[1];
                flux_i_density += factor*(momentum_nb[1]+momentum_i[1]);
                flux_i_density_energy += factor*(flux_contribution_nb_density_energy[1]+flux_contribution_i_density_energy[1]);
                flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[1]+flux_contribution_i_momentum_x[1]);
                flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[1]+flux_contribution_i_momentum_y[1]);
                flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[1]+flux_contribution_i_momentum_z[1]);

                factor = 0.5*normal[2];
                flux_i_density += factor*(momentum_nb[2]+momentum_i[2]);
                flux_i_density_energy += factor*(flux_contribution_nb_density_energy[2]+flux_contribution_i_density_energy[2]);
                flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[2]+flux_contribution_i_momentum_x[2]);
                flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[2]+flux_contribution_i_momentum_y[2]);
                flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[2]+flux_contribution_i_momentum_z[2]);
            }

            else if(nb == -1) { // a wing boundary
                flux_i_momentum[0] += normal[0]*pressure_i;
                flux_i_momentum[1] += normal[1]*pressure_i;
                flux_i_momentum[2] += normal[2]*pressure_i;
            }

            else if(nb == -2) { // a far field boundary
                factor = 0.5*normal[0];
                flux_i_density += factor*(ff_variable[VAR_MOMENTUM+0]+momentum_i[0]);
                flux_i_density_energy += factor*(ff_flux_contribution_density_energy[0]+flux_contribution_i_density_energy[0]);
                flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[0] + flux_contribution_i_momentum_x[0]);
                flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[0] + flux_contribution_i_momentum_y[0]);
                flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[0] + flux_contribution_i_momentum_z[0]);

                factor = 0.5*normal[1];
                flux_i_density += factor*(ff_variable[VAR_MOMENTUM+1]+momentum_i[1]);
                flux_i_density_energy += factor*(ff_flux_contribution_density_energy[1]+flux_contribution_i_density_energy[1]);
                flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[1] + flux_contribution_i_momentum_x[1]);
                flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[1] + flux_contribution_i_momentum_y[1]);
                flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[1] + flux_contribution_i_momentum_z[1]);

                factor = 0.5*normal[2];
                flux_i_density += factor*(ff_variable[VAR_MOMENTUM+2]+momentum_i[2]);
                flux_i_density_energy += factor*(ff_flux_contribution_density_energy[2]+flux_contribution_i_density_energy[2]);
                flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[2] + flux_contribution_i_momentum_x[2]);
                flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[2] + flux_contribution_i_momentum_y[2]);
                flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[2] + flux_contribution_i_momentum_z[2]);
            }
        }

        fluxes_density[i] = flux_i_density;
        fluxes_momentum_x[i] = flux_i_momentum[0];
        fluxes_momentum_y[i] = flux_i_momentum[1];
        fluxes_momentum_z[i] = flux_i_momentum[2];
        fluxes_energy[i] = flux_i_density_energy;
    }
}


void time_step_cpu(float *old_v_density, float *old_v_momentum_x, float *old_v_momentum_y, float *old_v_momentum_z, float *old_v_energy, float *v_density,
                    float *v_momentum_x, float *v_momentum_y, float *v_momentum_z, float *v_energy, float *step_factors, float *fluxes_density,
                    float *fluxes_momentum_x, float *fluxes_momentum_y, float *fluxes_momentum_z, float *fluxes_energy, int j, int nelr) {

    for (int i = 0; i < nelr; ++i) {
        float factor = step_factors[i]/(RK+1-j);

        v_density[i] = old_v_density[i] + factor*fluxes_density[i];
        v_momentum_x[i] = old_v_momentum_x[i] + factor*fluxes_momentum_x[i];
        v_momentum_y[i] = old_v_momentum_y[i] + factor*fluxes_momentum_y[i];
        v_momentum_z[i] = old_v_momentum_z[i] + factor*fluxes_momentum_z[i];
        v_energy[i] = old_v_energy[i] + factor*fluxes_energy[i];
    }
}



/*******************************************HPVM Tasks****************************************************/
/*********************************************************************************************************/

void body_1(float *__restrict__ v_density, float *__restrict__ v_momentum_x,
    float *__restrict__ v_momentum_y, float *__restrict__ v_momentum_z,
    float *__restrict__ v_energy, float *__restrict__ areas,
    float *__restrict__ step_factors, long int nelr, long int i) {
  float density = v_density[i];
  float momentum[3];
  momentum[0] = v_momentum_x[i];
  momentum[1] = v_momentum_y[i];
  momentum[2] = v_momentum_z[i];

  float density_energy = v_energy[i];

  float velocity[3];
  compute_velocity(density, momentum, velocity);
  
  float speed_sqd = compute_speed_sqd(velocity);
  float pressure = compute_pressure(density, density_energy, speed_sqd);
  float speed_of_sound = compute_speed_of_sound(density, pressure);

  // dt = float(0.5f) * sqrtf(areas[i]) /  (||v|| + c).... but when we do time
  // stepping, this later would need to be divided by the area, so we just do
  // it all at once
  step_factors[i] = 0.5 / (sqrt(areas[i]) * (sqrt(speed_sqd) + speed_of_sound));
}

void compute_step_factor_par(
    float *__restrict__ v_density,    size_t size_v_density,
    float *__restrict__ v_momentum_x, size_t size_v_momentum_x,
    float *__restrict__ v_momentum_y, size_t size_v_momentum_y,
    float *__restrict__ v_momentum_z, size_t size_v_momentum_z,
    float *__restrict__ v_energy,     size_t size_v_energy,
    float *__restrict__ areas,        size_t size_areas,
    float *__restrict__ step_factors, size_t size_step_factors,
    long int nelr) {

  auto s = __hpvm_parallel_section_begin();

// Both GPU and FPGA need a root task
#if defined(GPU) || defined(FPGA)
  auto t = __hpvm_task_begin(8, v_density,    size_v_density,
                                v_momentum_x, size_v_momentum_x,
                                v_momentum_y, size_v_momentum_y,
                                v_momentum_z, size_v_momentum_z,
                                v_energy,     size_v_energy,
                                areas,        size_areas,
                                step_factors, size_step_factors,
                                nelr,
                             1, step_factors, size_step_factors);
  auto si = __hpvm_parallel_section_begin();
#endif

  for (long int i = 0; i < nelr; i++) {
    __hpvm_parallel_loop(1, 8, v_density,    size_v_density,
                               v_momentum_x, size_v_momentum_x,
                               v_momentum_y, size_v_momentum_y,
                               v_momentum_z, size_v_momentum_z,
                               v_energy,     size_v_energy,
                               areas,        size_areas,
                               step_factors, size_step_factors,
                               nelr,
                            1, step_factors, size_step_factors);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    body_1(v_density, v_momentum_x, v_momentum_y, v_momentum_z, v_energy,
           areas, step_factors, nelr, i);
  }
#if defined(GPU) || defined(FPGA)
  __hpvm_parallel_section_end(si);
  __hpvm_task_end(t);
#endif
  __hpvm_parallel_section_end(s);
}

static inline
void body_2(int *__restrict__ elements_surrounding_elements,
      float *__restrict__ normals, float *__restrict__ v_density,
      float *__restrict__ v_momentum_x, float *__restrict__ v_momentum_y,
      float *__restrict__ v_momentum_z, float *__restrict__ v_energy,
      float *__restrict__ ff_variable, float *__restrict__ fluxes_density,
      float *__restrict__ fluxes_momentum_x, float *__restrict__ fluxes_momentum_y,
      float *__restrict__ fluxes_momentum_z, float *__restrict__ fluxes_energy,
      float *__restrict__ ff_flux_contribution_density_energy,
      float *__restrict__ ff_flux_contribution_momentum_x,
      float *__restrict__ ff_flux_contribution_momentum_y,
      float *__restrict__ ff_flux_contribution_momentum_z,
      long int nelr, long int i) {
  #define smoothing_coefficient 0.2f

  int j, nb;
  float normal[3]; float normal_len;
  float factor;

  float density_i = v_density[i];
  float momentum_i[3];
  momentum_i[0] = v_momentum_x[i];
  momentum_i[1] = v_momentum_y[i];
  momentum_i[2] = v_momentum_z[i];

  float density_energy_i = v_energy[i];

  float velocity_i[3];
  compute_velocity(density_i, momentum_i, velocity_i);
  
  float speed_sqd_i = compute_speed_sqd(velocity_i);
  float speed_i = sqrt(speed_sqd_i);
  float pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
  float speed_of_sound_i = compute_speed_of_sound(density_i, pressure_i);
  
  float flux_contribution_i_momentum_x[3],
        flux_contribution_i_momentum_y[3],
        flux_contribution_i_momentum_z[3];
  float flux_contribution_i_density_energy[3];

  compute_flux_contribution(density_i, momentum_i, density_energy_i,
            pressure_i, velocity_i, flux_contribution_i_momentum_x,
            flux_contribution_i_momentum_y, flux_contribution_i_momentum_z,
            flux_contribution_i_density_energy);

  float flux_i_density = 0.0;
  float flux_i_momentum[3];
  flux_i_momentum[0] = 0.0;
  flux_i_momentum[1] = 0.0;
  flux_i_momentum[2] = 0.0;
  float flux_i_density_energy = 0.0;

  float velocity_nb[3];
  float density_nb, density_energy_nb;
  float momentum_nb[3];
  float flux_contribution_nb_momentum_x[3],
        flux_contribution_nb_momentum_y[3],
        flux_contribution_nb_momentum_z[3];
  float flux_contribution_nb_density_energy[3];
  float speed_sqd_nb, speed_of_sound_nb, pressure_nb;

  for(j = 0; j < NNB; j++) {
    __hpvm__isNonZeroLoop(j, NNB);
    nb = elements_surrounding_elements[i + j*nelr];
    normal[0] = normals[i + (j + 0*NNB)*nelr];
    normal[1] = normals[i + (j + 1*NNB)*nelr];
    normal[2] = normals[i + (j + 2*NNB)*nelr];
    
    normal_len = sqrt(normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2]);

    if(nb >= 0) {   // a legitimate neighbor
      density_nb = v_density[nb];
      momentum_nb[0] = v_momentum_x[nb];
      momentum_nb[1] = v_momentum_y[nb];
      momentum_nb[2] = v_momentum_z[nb];
      density_energy_nb = v_energy[nb];
      compute_velocity(density_nb, momentum_nb, velocity_nb);
      speed_sqd_nb = compute_speed_sqd(velocity_nb);
      pressure_nb = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
      speed_of_sound_nb = compute_speed_of_sound(density_nb, pressure_nb);

      compute_flux_contribution(density_nb, momentum_nb, density_energy_nb,
                pressure_nb, velocity_nb, flux_contribution_nb_momentum_x,
                flux_contribution_nb_momentum_y, flux_contribution_nb_momentum_z,
                flux_contribution_nb_density_energy);

      // artificial viscosity
      factor = -normal_len * smoothing_coefficient * 0.5 * (speed_i
              + sqrt(speed_sqd_nb) + speed_of_sound_i + speed_of_sound_nb);
      flux_i_density += factor*(density_i-density_nb);
      flux_i_density_energy += factor*(density_energy_i-density_energy_nb);
      flux_i_momentum[0] += factor*(momentum_i[0]-momentum_nb[0]);
      flux_i_momentum[1] += factor*(momentum_i[1]-momentum_nb[1]);
      flux_i_momentum[2] += factor*(momentum_i[2]-momentum_nb[2]);

      // accumulate cell-centered fluxes
      factor = 0.5*normal[0];
      flux_i_density += factor*(momentum_nb[0]+momentum_i[0]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[0]
              + flux_contribution_i_density_energy[0]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[0]
              + flux_contribution_i_momentum_x[0]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[0]
              + flux_contribution_i_momentum_y[0]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[0]
              + flux_contribution_i_momentum_z[0]);

      factor = 0.5*normal[1];
      flux_i_density += factor*(momentum_nb[1]+momentum_i[1]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[1]
              + flux_contribution_i_density_energy[1]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[1]
              + flux_contribution_i_momentum_x[1]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[1]
              + flux_contribution_i_momentum_y[1]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[1]
              + flux_contribution_i_momentum_z[1]);

      factor = 0.5*normal[2];
      flux_i_density += factor*(momentum_nb[2]+momentum_i[2]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[2]
              + flux_contribution_i_density_energy[2]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[2]
              + flux_contribution_i_momentum_x[2]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[2]
              + flux_contribution_i_momentum_y[2]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[2]
              + flux_contribution_i_momentum_z[2]);
    } else if(nb == -1) { // a wing boundary
      flux_i_momentum[0] += normal[0]*pressure_i;
      flux_i_momentum[1] += normal[1]*pressure_i;
      flux_i_momentum[2] += normal[2]*pressure_i;
    // TODO CHANGED THIS BECAUSE OF ERROR IN llvm-ocl generated code for this
    } else /*if(nb == -2)*/ { // a far field boundary
      factor = 0.5*normal[0];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+0]+momentum_i[0]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[0]
              + flux_contribution_i_density_energy[0]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[0]
              + flux_contribution_i_momentum_x[0]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[0]
              + flux_contribution_i_momentum_y[0]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[0]
              + flux_contribution_i_momentum_z[0]);

      factor = 0.5*normal[1];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+1]+momentum_i[1]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[1]
              + flux_contribution_i_density_energy[1]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[1]
              + flux_contribution_i_momentum_x[1]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[1]
              + flux_contribution_i_momentum_y[1]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[1]
              + flux_contribution_i_momentum_z[1]);

      factor = 0.5*normal[2];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+2]+momentum_i[2]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[2]
              + flux_contribution_i_density_energy[2]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[2]
              + flux_contribution_i_momentum_x[2]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[2]
              + flux_contribution_i_momentum_y[2]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[2]
              + flux_contribution_i_momentum_z[2]);
    }
  }

  fluxes_density[i] = flux_i_density;
  fluxes_momentum_x[i] = flux_i_momentum[0];
  fluxes_momentum_y[i] = flux_i_momentum[1];
  fluxes_momentum_z[i] = flux_i_momentum[2];
  fluxes_energy[i] = flux_i_density_energy;

  #undef smoothing_coefficient
}

void compute_flux_time_step_par(
    int *__restrict__ elements_surrounding_elements,
                                     size_t size_elements_surrounding_elements,
    float *__restrict__ normals,           size_t size_normals,
    float *__restrict__ v_density,         size_t size_v_density,
    float *__restrict__ v_momentum_x,      size_t size_v_momentum_x,
    float *__restrict__ v_momentum_y,      size_t size_v_momentum_y,
    float *__restrict__ v_momentum_z,      size_t size_v_momentum_z,
    float *__restrict__ v_energy,          size_t size_v_energy,
    float *__restrict__ ff_variable,       size_t size_ff_variable,
    float *__restrict__ fluxes_density,    size_t size_fluxes_density,
    float *__restrict__ fluxes_momentum_x, size_t size_fluxes_momentum_x,
    float *__restrict__ fluxes_momentum_y, size_t size_fluxes_momentum_y,
    float *__restrict__ fluxes_momentum_z, size_t size_fluxes_momentum_z,
    float *__restrict__ fluxes_energy,     size_t size_fluxes_energy,
    float *__restrict__ ff_flux_contribution_density_energy,
                               size_t size_ff_flux_contribution_density_energy,
    float *__restrict__ ff_flux_contribution_momentum_x,
                                   size_t size_ff_flux_contribution_momentum_x,
    float *__restrict__ ff_flux_contribution_momentum_y,
                                   size_t size_ff_flux_contribution_momentum_y,
    float *__restrict__ ff_flux_contribution_momentum_z,
                                   size_t size_ff_flux_contribution_momentum_z,
    float *__restrict__ old_v_density,     size_t size_old_v_density,
    float *__restrict__ old_v_momentum_x,  size_t size_old_v_momentum_x,
    float *__restrict__ old_v_momentum_y,  size_t size_old_v_momentum_y,
    float *__restrict__ old_v_momentum_z,  size_t size_old_v_momentum_z,
    float *__restrict__ old_v_energy,      size_t size_old_v_energy,
    float *__restrict__ step_factors,      size_t size_step_factors,
    long int j, long int nelr) {

  auto s = __hpvm_parallel_section_begin();

#ifdef FPGA
  auto fpga_root = __hpvm_task_begin(25,
                elements_surrounding_elements, size_elements_surrounding_elements,
                normals, size_normals, v_density, size_v_density, v_momentum_x,
                size_v_momentum_x, v_momentum_y, size_v_momentum_y,
                v_momentum_z, size_v_momentum_z, v_energy, size_v_energy,
                ff_variable, size_ff_variable, fluxes_density,
                size_fluxes_density, fluxes_momentum_x, size_fluxes_momentum_x,
                fluxes_momentum_y, size_fluxes_momentum_y, fluxes_momentum_z,
                size_fluxes_momentum_z, fluxes_energy, size_fluxes_energy,
                ff_flux_contribution_density_energy,
                size_ff_flux_contribution_density_energy,
                ff_flux_contribution_momentum_x,
                size_ff_flux_contribution_momentum_x,
                ff_flux_contribution_momentum_y,
                size_ff_flux_contribution_momentum_y,
                ff_flux_contribution_momentum_z,
                size_ff_flux_contribution_momentum_z, old_v_density,
                size_old_v_density, old_v_momentum_x, size_old_v_momentum_x,
                old_v_momentum_y, size_old_v_momentum_y, old_v_momentum_z,
                size_old_v_momentum_z, old_v_energy, size_old_v_energy,
                step_factors, size_step_factors,
                j, nelr,
                5, v_density, size_v_density, v_momentum_x, size_v_momentum_x,
                v_momentum_y, size_v_momentum_y, v_momentum_z,
                size_v_momentum_z, v_energy, size_v_energy);
  auto fpga_section = __hpvm_parallel_section_begin();
#endif

  // For the GPU backend we have to wrap these
#ifdef GPU
  auto t1 = __hpvm_task_begin(18, 
        elements_surrounding_elements, size_elements_surrounding_elements,
        normals, size_normals,
        v_density, size_v_density,
        v_momentum_x, size_v_momentum_x,
        v_momentum_y, size_v_momentum_y,
        v_momentum_z, size_v_momentum_z,
        v_energy, size_v_energy,
        ff_variable, size_ff_variable,
        fluxes_density, size_fluxes_density,
        fluxes_momentum_x, size_fluxes_momentum_x,
        fluxes_momentum_y, size_fluxes_momentum_y,
        fluxes_momentum_z, size_fluxes_momentum_z,
        fluxes_energy, size_fluxes_energy,
        ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
        ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
        ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
        ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
        nelr,
     5, fluxes_density, size_fluxes_density,
        fluxes_momentum_x, size_fluxes_momentum_x,
        fluxes_momentum_y, size_fluxes_momentum_y,
        fluxes_momentum_z, size_fluxes_momentum_z,
        fluxes_energy, size_fluxes_energy);
  auto s1 = __hpvm_parallel_section_begin();
#endif
  for (long int i = 0; i < nelr; ++i) {
    __hpvm_parallel_loop(1, 18,
        elements_surrounding_elements, size_elements_surrounding_elements,
        normals, size_normals,
        v_density, size_v_density,
        v_momentum_x, size_v_momentum_x,
        v_momentum_y, size_v_momentum_y,
        v_momentum_z, size_v_momentum_z,
        v_energy, size_v_energy,
        ff_variable, size_ff_variable,
        fluxes_density, size_fluxes_density,
        fluxes_momentum_x, size_fluxes_momentum_x,
        fluxes_momentum_y, size_fluxes_momentum_y,
        fluxes_momentum_z, size_fluxes_momentum_z,
        fluxes_energy, size_fluxes_energy,
        ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
        ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
        ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
        ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
        nelr,
     5, fluxes_density, size_fluxes_density,
        fluxes_momentum_x, size_fluxes_momentum_x,
        fluxes_momentum_y, size_fluxes_momentum_y,
        fluxes_momentum_z, size_fluxes_momentum_z,
        fluxes_energy, size_fluxes_energy);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    body_2(elements_surrounding_elements, normals, v_density, v_momentum_x,
           v_momentum_y, v_momentum_z, v_energy, ff_variable, fluxes_density,
           fluxes_momentum_x, fluxes_momentum_y, fluxes_momentum_z, fluxes_energy,
           ff_flux_contribution_density_energy, ff_flux_contribution_momentum_x,
           ff_flux_contribution_momentum_y, ff_flux_contribution_momentum_z,
           nelr, i);
  }
#ifdef GPU
  __hpvm_parallel_section_end(s1);
  __hpvm_task_end(t1);
#endif

#ifdef GPU
  auto t2 = __hpvm_task_begin(18,
        old_v_density, size_old_v_density,
        old_v_momentum_x, size_old_v_momentum_x,
        old_v_momentum_y, size_old_v_momentum_y,
        old_v_momentum_z, size_old_v_momentum_z,
        old_v_energy, size_old_v_energy,
        v_density, size_v_density,
        v_momentum_x, size_v_momentum_x,
        v_momentum_y, size_v_momentum_y,
        v_momentum_z, size_v_momentum_z,
        v_energy, size_v_energy,
        step_factors, size_step_factors,
        fluxes_density, size_fluxes_density,
        fluxes_momentum_x, size_fluxes_momentum_x,
        fluxes_momentum_y, size_fluxes_momentum_y,
        fluxes_momentum_z, size_fluxes_momentum_z,
        fluxes_energy, size_fluxes_energy,
        j, nelr,
     5, v_density, size_v_density,
        v_momentum_x, size_v_momentum_x,
        v_momentum_y, size_v_momentum_y,
        v_momentum_z, size_v_momentum_z,
        v_energy, size_v_energy);
  auto s2 = __hpvm_parallel_section_begin();
#endif
  for (long int i = 0; i < nelr; ++i) {
    __hpvm_parallel_loop(1, 18,
        old_v_density, size_old_v_density,
        old_v_momentum_x, size_old_v_momentum_x,
        old_v_momentum_y, size_old_v_momentum_y,
        old_v_momentum_z, size_old_v_momentum_z,
        old_v_energy, size_old_v_energy,
        v_density, size_v_density,
        v_momentum_x, size_v_momentum_x,
        v_momentum_y, size_v_momentum_y,
        v_momentum_z, size_v_momentum_z,
        v_energy, size_v_energy,
        step_factors, size_step_factors,
        fluxes_density, size_fluxes_density,
        fluxes_momentum_x, size_fluxes_momentum_x,
        fluxes_momentum_y, size_fluxes_momentum_y,
        fluxes_momentum_z, size_fluxes_momentum_z,
        fluxes_energy, size_fluxes_energy,
        j, nelr,
     5, v_density, size_v_density,
        v_momentum_x, size_v_momentum_x,
        v_momentum_y, size_v_momentum_y,
        v_momentum_z, size_v_momentum_z,
        v_energy, size_v_energy);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    float factor = step_factors[i]/(RK+1-j);

    v_density[i] = old_v_density[i] + factor*fluxes_density[i];
    v_momentum_x[i] = old_v_momentum_x[i] + factor*fluxes_momentum_x[i];
    v_momentum_y[i] = old_v_momentum_y[i] + factor*fluxes_momentum_y[i];
    v_momentum_z[i] = old_v_momentum_z[i] + factor*fluxes_momentum_z[i];
    v_energy[i] = old_v_energy[i] + factor*fluxes_energy[i];
  }
#ifdef GPU
  __hpvm_parallel_section_end(s2);
  __hpvm_task_end(t2);
#endif

#ifdef FPGA
  __hpvm_parallel_section_end(fpga_section);
  __hpvm_task_end(fpga_root);
#endif

  __hpvm_parallel_section_end(s);
}

/*******************************************int main()****************************************************/
/*********************************************************************************************************/

int main (int argc, char **argv) {
    auto mainStart = std::chrono::steady_clock::now();

    if (argc < 4) {
        std::cout << "Specify data file name, iterations, and block size\n";
        std::cout << "example, ./euler3d fvcorr.domn.097K 1000 16\n";
        return 0;
    }

    const char* data_file_name = argv[1];
    int iterations = atoi(argv[2]);
    int block_size = atoi(argv[3]);

    // constant in OpenCL
    float *ff_variable = NULL;
    
    float *ff_flux_contribution_momentum_x = NULL, *ff_flux_contribution_momentum_y = NULL,
                *ff_flux_contribution_momentum_z = NULL, *ff_flux_contribution_density_energy = NULL;

    float *areas = NULL;
    int *elements_surrounding_elements = NULL;
    float *normals = NULL;
    float *step_factors = NULL;

    size_t size_areas, size_elements_surrounding_elements, size_normals, size_step_factors;

    struct Variables variables_soa, old_variables_soa, fluxes_soa;

    size_t size_ff_variable = sizeof(float) * NVAR;
    size_t size_ff_flux_contribution_momentum_x = sizeof(float) * 3;
    size_t size_ff_flux_contribution_momentum_y = sizeof(float) * 3;
    size_t size_ff_flux_contribution_momentum_z = sizeof(float) * 3;
    size_t size_ff_flux_contribution_density_energy = sizeof(float) * 3;

    ff_variable = (float *) __hetero_malloc(size_ff_variable);
    ff_flux_contribution_momentum_x = (float *) __hetero_malloc(size_ff_flux_contribution_momentum_x);
    ff_flux_contribution_momentum_y = (float *) __hetero_malloc(size_ff_flux_contribution_momentum_y);
    ff_flux_contribution_momentum_z = (float *) __hetero_malloc(size_ff_flux_contribution_momentum_z);
    ff_flux_contribution_density_energy = (float *) __hetero_malloc(size_ff_flux_contribution_density_energy);

    {
        const float angle_of_attack = float(3.1415926535897931 / 180.0f) * float(deg_angle_of_attack);

        ff_variable[VAR_DENSITY] = float(1.4);

        float ff_pressure = float(1.0f);
        float ff_speed_of_sound = sqrt(GAMMA * ff_pressure / ff_variable[VAR_DENSITY]);
        float ff_speed = float(ff_mach) * ff_speed_of_sound;

        float ff_velocity[3];
        ff_velocity[0] = ff_speed * float(cos((float)angle_of_attack));
        ff_velocity[1] = ff_speed * float(sin((float)angle_of_attack));
        ff_velocity[2] = 0.0f;

        ff_variable[VAR_MOMENTUM + 0] = ff_variable[VAR_DENSITY] * ff_velocity[0];
        ff_variable[VAR_MOMENTUM + 1] = ff_variable[VAR_DENSITY] * ff_velocity[1];
        ff_variable[VAR_MOMENTUM + 2] = ff_variable[VAR_DENSITY] * ff_velocity[2];

        ff_variable[VAR_DENSITY_ENERGY] = ff_variable[VAR_DENSITY] * (float(0.5f)*(ff_speed * ff_speed)) + (ff_pressure / float(GAMMA - 1.0f));

        float ff_momentum[3];
        ff_momentum[0] = *(ff_variable + VAR_MOMENTUM + 0);
        ff_momentum[1] = *(ff_variable + VAR_MOMENTUM + 1);
        ff_momentum[2] = *(ff_variable + VAR_MOMENTUM + 2);

        compute_flux_contribution(ff_variable[VAR_DENSITY], ff_momentum, ff_variable[VAR_DENSITY_ENERGY], ff_pressure, ff_velocity,
                        ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y, ff_flux_contribution_momentum_z, ff_flux_contribution_density_energy);
    }

    int nel, nelr;

    {
        std::ifstream file(data_file_name);
        if (!file.is_open()) {
            std::cerr << "Cannot find/open file!\n";
            abort();
        }

        file >> nel;
        nelr = block_size * ((nel / block_size) + std::min(1, nel % block_size));
        std::cout<<"--cambine: nel="<<nel<<", nelr="<<nelr<<std::endl;

        size_areas = sizeof(float) * nelr;
        size_elements_surrounding_elements = sizeof(int) * NNB * nelr;
        size_normals = sizeof(float) * nelr * NDIM * NNB;
        size_step_factors = sizeof(float) * nelr;

        areas = (float *) __hetero_malloc(size_areas);
        elements_surrounding_elements  = (int *) __hetero_malloc(size_elements_surrounding_elements);
        normals = (float *) __hetero_malloc(size_normals);
        step_factors = (float *) __hetero_malloc(size_step_factors);

        // Read in data
        for(int i = 0; i < nel; i++) {
            file >> areas[i];
            for(int j = 0; j < NNB; j++) {
                file >> elements_surrounding_elements[i + j*nelr];
                if(elements_surrounding_elements[i+j*nelr] < 0) elements_surrounding_elements[i+j*nelr] = -1;
                elements_surrounding_elements[i + j*nelr]--; //it's coming in with Fortran numbering

                for(int k = 0; k < NDIM; k++) {
                    file >> normals[i + (j + k*NNB)*nelr];
                    normals[i + (j + k*NNB)*nelr] = -normals[i + (j + k*NNB)*nelr];
                }
            }
        }

        // fill in remaining data
        int last = nel-1;
        for(int i = nel; i < nelr; i++) {
            areas[i] = areas[last];
            for(int j = 0; j < NNB; j++) {
                // duplicate the last element
                elements_surrounding_elements[i + j*nelr] = elements_surrounding_elements[last + j*nelr];
                for(int k = 0; k < NDIM; k++) normals[last + (j + k*NNB)*nelr] = normals[last + (j + k*NNB)*nelr];
            }
        }
    }

    // Create arrays and set initial conditions
    variables_soa.allocate(nelr);
    old_variables_soa.allocate(nelr);
    fluxes_soa.allocate(nelr);

    size_t size_variables_soa_density = sizeof(float) * nelr;
    size_t size_variables_soa_momentum_x = sizeof(float) * nelr;
    size_t size_variables_soa_momentum_y = sizeof(float) * nelr;
    size_t size_variables_soa_momentum_z = sizeof(float) * nelr;
    size_t size_variables_soa_energy = sizeof(float) * nelr;

    size_t size_old_variables_soa_density = sizeof(float) * nelr;
    size_t size_old_variables_soa_momentum_x = sizeof(float) * nelr;
    size_t size_old_variables_soa_momentum_y = sizeof(float) * nelr;
    size_t size_old_variables_soa_momentum_z = sizeof(float) * nelr;
    size_t size_old_variables_soa_energy = sizeof(float) * nelr;

    size_t size_fluxes_soa_density = sizeof(float) * nelr;
    size_t size_fluxes_soa_momentum_x = sizeof(float) * nelr;
    size_t size_fluxes_soa_momentum_y = sizeof(float) * nelr;
    size_t size_fluxes_soa_momentum_z = sizeof(float) * nelr;
    size_t size_fluxes_soa_energy = sizeof(float) * nelr;

    fill_variable(nelr, ff_variable, variables_soa);
    fill_variable(nelr, ff_variable, old_variables_soa);
    fill_variable(nelr, ff_variable, fluxes_soa);

    std::cout << "\nStarting...\n";

    long int nelr_long = nelr;
    float totalTime = 0.0;
#ifdef REPORT_POWER
#ifdef CPU
    double totalEnergy = 0.0;
    // The GetEnergyCPU() implementation relies on us not switching processors
    // so to fascilitate this we pin ourselves to the current core, this is
    // also needed as it determines the processor we're running on
    int core = sched_getcpu();
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(core, &mask);
    if (sched_setaffinity(0, sizeof(mask), &mask)) {
      perror("Error in sched_setaffinity: ");
      exit(1);
    }
#endif
#ifdef GPU
    double totalPower = 0.0;
    pthread_barrier_init(&barrier, NULL, 2);
#endif
#endif

    // Begin iterations
    for (int i =  0; i < iterations; i++) {
        //copy(old_variables_soa, variables_soa, nelr);
        
        __hetero_copy_mem(variables_soa.density, old_variables_soa.density, size_variables_soa_density);
        __hetero_copy_mem(variables_soa.momentum_x, old_variables_soa.momentum_x, size_variables_soa_momentum_x);
        __hetero_copy_mem(variables_soa.momentum_y, old_variables_soa.momentum_y, size_variables_soa_momentum_y);
        __hetero_copy_mem(variables_soa.momentum_z, old_variables_soa.momentum_z, size_variables_soa_momentum_z);
        __hetero_copy_mem(variables_soa.energy, old_variables_soa.energy, size_variables_soa_energy);


        // Timing these two kernels and the loop as one, since there's no code
        // between them we actually probably get more accurate timing by timing
        // it as a single group, rather than each piece separately
#ifdef REPORT_POWER
#ifdef CPU
        double start_energy = GetEnergyCPU(core);
#endif
#ifdef GPU
        int flag = 0;
        pthread_t thd;
        pthread_create(&thd, NULL, measure_func, &flag);
        pthread_barrier_wait(&barrier);
#endif
#endif

        auto startTime = std::chrono::steady_clock::now();

        auto launch_1 = __hpvm_launch((void *) compute_step_factor_par, 8,
            variables_soa.density, size_variables_soa_density,
            variables_soa.momentum_x, size_variables_soa_momentum_x,
            variables_soa.momentum_y, size_variables_soa_momentum_y,
            variables_soa.momentum_z, size_variables_soa_momentum_z,
            variables_soa.energy, size_variables_soa_energy,
            areas, size_areas,
            step_factors, size_step_factors,
            nelr_long, 1,
            step_factors, size_step_factors
        );

        __hpvm_wait(launch_1);

        for (int j = 0; j < RK; j++) {

            auto launch_2 = __hpvm_launch((void *) compute_flux_time_step_par, 25,
                elements_surrounding_elements, size_elements_surrounding_elements,
                normals, size_normals,
                variables_soa.density, size_variables_soa_density,
                variables_soa.momentum_x, size_variables_soa_momentum_x,
                variables_soa.momentum_y, size_variables_soa_momentum_y,
                variables_soa.momentum_z, size_variables_soa_momentum_z,
                variables_soa.energy, size_variables_soa_energy,
                ff_variable, size_ff_variable,
                fluxes_soa.density, size_fluxes_soa_density,
                fluxes_soa.momentum_x, size_fluxes_soa_momentum_x,
                fluxes_soa.momentum_y, size_fluxes_soa_momentum_y,
                fluxes_soa.momentum_z, size_fluxes_soa_momentum_z,
                fluxes_soa.energy, size_fluxes_soa_energy,
                ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
                ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
                ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
                ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
                old_variables_soa.density, size_old_variables_soa_density,
                old_variables_soa.momentum_x, size_old_variables_soa_momentum_x,
                old_variables_soa.momentum_y, size_old_variables_soa_momentum_y,
                old_variables_soa.momentum_z, size_old_variables_soa_momentum_z,
                old_variables_soa.energy, size_old_variables_soa_energy,
                step_factors, size_step_factors,
                j, nelr_long, 5,
                variables_soa.density, size_variables_soa_density,
                variables_soa.momentum_x, size_variables_soa_momentum_x,
                variables_soa.momentum_y, size_variables_soa_momentum_y,
                variables_soa.momentum_z, size_variables_soa_momentum_z,
                variables_soa.energy, size_variables_soa_energy
            );

            __hpvm_wait(launch_2);
        }

        auto elapsed = std::chrono::steady_clock::now() - startTime;
        totalTime += std::chrono::duration<float>(elapsed).count();
#ifdef REPORT_POWER
#ifdef CPU
        double end_energy = GetEnergyCPU(core);
        totalEnergy += end_energy - start_energy;
#endif
#ifdef GPU
        flag = 1;
        pthread_join(thd, NULL);
        totalPower += measuredPower;
#endif
#endif
    }
    auto endTime = std::chrono::steady_clock::now();
    double fullTime = std::chrono::duration<float>(endTime - mainStart).count();

    printf("\nKernel Execution took %f seconds\n", totalTime);
    printf("\nTotal Execution took %f seconds\n", fullTime);
#ifdef REPORT_POWER
#ifdef CPU
    printf("\nPower usage: %lf W\n", totalEnergy / totalTime);
    printf("Energy usage: %lf J\n", totalEnergy);
#endif
#ifdef GPU
    printf("\nPower usage: %lf W\n", totalPower / iterations);
    printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower / iterations, totalTime * 1000));
#endif
#endif

    Variables variables_soa_fpga;
    variables_soa_fpga.allocate_cpu(nelr);

    std::cout << "\nSaving solution...\n";
    __hetero_request_mem(variables_soa.density, size_variables_soa_density);
    __hetero_request_mem(variables_soa.momentum_x, size_variables_soa_momentum_x);
    __hetero_request_mem(variables_soa.momentum_y, size_variables_soa_momentum_y);
    __hetero_request_mem(variables_soa.momentum_z, size_variables_soa_momentum_z);
    __hetero_request_mem(variables_soa.energy, size_variables_soa_energy);
  
    copy(variables_soa_fpga, variables_soa, nelr);
    dump(variables_soa, nel, nelr);

/*******************************************int main() - Verifying with CPU****************************************************/
/******************************************************************************************************************************/

    std::cout << "\nReinitializing and Verifying...\n";

    /*variables_soa.allocate_cpu(nelr);
    old_variables_soa.allocate_cpu(nelr);
    fluxes_soa.allocate_cpu(nelr);*/

    fill_variable(nelr, ff_variable, variables_soa);
    fill_variable(nelr, ff_variable, old_variables_soa);
    fill_variable(nelr, ff_variable, fluxes_soa);

    __hetero_free(step_factors);
    step_factors = (float *) malloc(sizeof(float) * nelr);

    for (int i = 0; i < iterations; i++) {
        copy(old_variables_soa, variables_soa, nelr);
        compute_step_factor_cpu(variables_soa.density, variables_soa.momentum_x, variables_soa.momentum_y, variables_soa.momentum_z,
                                            variables_soa.energy, areas, step_factors, nelr);

        for (int j = 0; j < RK; j++) {
            compute_flux_cpu(elements_surrounding_elements, normals, variables_soa.density, variables_soa.momentum_x, variables_soa.momentum_y, variables_soa.momentum_z,
                                    variables_soa.energy, ff_variable, fluxes_soa.density, fluxes_soa.momentum_x, fluxes_soa.momentum_y, fluxes_soa.momentum_z,
                                    fluxes_soa.energy, ff_flux_contribution_density_energy, ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y,
                                    ff_flux_contribution_momentum_z, nelr);

            time_step_cpu(old_variables_soa.density, old_variables_soa.momentum_x, old_variables_soa.momentum_y, old_variables_soa.momentum_z, old_variables_soa.energy,
                            variables_soa.density, variables_soa.momentum_x, variables_soa.momentum_y, variables_soa.momentum_z, variables_soa.energy, step_factors,
                            fluxes_soa.density, fluxes_soa.momentum_x, fluxes_soa.momentum_y, fluxes_soa.momentum_z, fluxes_soa.energy, j, nelr);
        }
    }

    bool n_verify_flag = false;

    for (int i = 0; i < nelr; i++) {
        if (!compareFloat(variables_soa_fpga.density[i], variables_soa.density[i]) || !compareFloat(variables_soa_fpga.momentum_x[i], variables_soa.momentum_x[i])
              || !compareFloat(variables_soa_fpga.momentum_y[i], variables_soa.momentum_y[i]) || !compareFloat(variables_soa_fpga.momentum_z[i], variables_soa.momentum_z[i])
              || !compareFloat(variables_soa_fpga.energy[i], variables_soa.energy[i])) {

            n_verify_flag = true;
            break;
        }
    }

    if (n_verify_flag)
        std::cout << "\nNOT VERIFIED\n";
    else
        std::cout << "\nVERIFIED\n";


    std::cout << "\nCleaning up...\n";

    __hetero_free(areas);
    free(step_factors);
    __hetero_free(elements_surrounding_elements);
    __hetero_free(normals);
    __hetero_free(ff_variable);
    __hetero_free(ff_flux_contribution_density_energy);
    __hetero_free(ff_flux_contribution_momentum_x);
    __hetero_free(ff_flux_contribution_momentum_y);
    __hetero_free(ff_flux_contribution_momentum_z);
    variables_soa.deallocate();
    old_variables_soa.deallocate();
    fluxes_soa.deallocate();
    return 0;
}
