/* Pre EULER*/
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <math.h>
#include <heterocc.h>

#include <chrono>

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection requires sudo
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE 1
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

#define deg_angle_of_attack (0.0f)
#define NNB (4)
#define GAMMA (1.4f)
#define ff_mach (1.2f)
#define NDIM (3)
#define RK (3)
#define VAR_DENSITY (0)
#define VAR_MOMENTUM (1)
#define VAR_DENSITY_ENERGY (VAR_MOMENTUM + NDIM)
#define NVAR (VAR_DENSITY_ENERGY + 1)

#define NELR_NUM 232544

#define FLOAT3_ASSIGN(f3, rx, ry, rz) do { \
    (f3).x = rx;                           \
    (f3).y = ry;                           \
    (f3).z = rz;                           \
  } while (0)

#define FLOAT_COMPARE (1e-05)

int block_size;

typedef struct {
    float x;
    float y;
    float z;
} FLOAT3;

void fill_variable(int nelr, const float *ff_variable,
                          float *variable) {
  for (int i = 0; i < nelr; ++i) {
    for (int j = 0; j < NVAR; ++j) {
      variable[i + j*nelr] = ff_variable[j];
    }
  }
}

void copy(float *dst, float *src, int n) {
    for (int i = 0; i < n; i++)
        dst[i] = src[i];
}

void compute_flux_contribution (float density, FLOAT3 momentum, float density_energy, float pressure, FLOAT3 velocity,
                                        FLOAT3 *flux_contribution_momentum_x, FLOAT3 *flux_contribution_momentum_y, FLOAT3 *flux_contribution_momentum_z, FLOAT3 *flux_contribution_density_energy) {

    flux_contribution_momentum_x -> x = velocity.x * momentum.x + pressure;
    flux_contribution_momentum_x -> y = velocity.x * momentum.y;
    flux_contribution_momentum_x -> z = velocity.x * momentum.z;

    flux_contribution_momentum_y -> x = flux_contribution_momentum_x -> y;
    flux_contribution_momentum_y -> y = velocity.y * momentum.y + pressure;
    flux_contribution_momentum_y -> z = velocity.y * momentum.z;

    flux_contribution_momentum_z -> x = flux_contribution_momentum_x -> z;
    flux_contribution_momentum_z -> y = flux_contribution_momentum_y -> z;
    flux_contribution_momentum_z -> z = velocity.z * momentum.z + pressure;

    float de_p = density_energy + pressure;
    flux_contribution_density_energy -> x = velocity.x * de_p;
    flux_contribution_density_energy -> y = velocity.y * de_p;
    flux_contribution_density_energy -> z = velocity.z * de_p;
}

void compute_flux_contribution (float density, float *momentum, float density_energy, float pressure, float *velocity,
                                        float *flux_contribution_momentum_x, float *flux_contribution_momentum_y, float *flux_contribution_momentum_z, float *flux_contribution_density_energy) {

    flux_contribution_momentum_x[0] = velocity[0] * momentum[0] + pressure;
    flux_contribution_momentum_x[1] = velocity[0] * momentum[1];
    flux_contribution_momentum_x[2] = velocity[0] * momentum[2];

    flux_contribution_momentum_y[0] = flux_contribution_momentum_x[1];
    flux_contribution_momentum_y[1] = velocity[1] * momentum[1] + pressure;
    flux_contribution_momentum_y[2] = velocity[1] * momentum[2];

    flux_contribution_momentum_z[0] = flux_contribution_momentum_x[2];
    flux_contribution_momentum_z[1] = flux_contribution_momentum_y[2];
    flux_contribution_momentum_z[2] = velocity[2] * momentum[2] + pressure;

    float de_p = density_energy + pressure;
    flux_contribution_density_energy[0] = velocity[0] * de_p;
    flux_contribution_density_energy[1] = velocity[1] * de_p;
    flux_contribution_density_energy[2] = velocity[2] * de_p;
}

void dump(float *variables, int nel, int nelr) {
  {
    std::ofstream file("density");
    file << nel << " " << nelr << std::endl;
    for(int i = 0; i < nel; i++) {
      file << variables[i + VAR_DENSITY*nelr] << std::endl;
    }
    }

  {
    std::ofstream file("momentum");
    file << nel << " " << nelr << std::endl;
    for(int i = 0; i < nel; i++) {
      for(int j = 0; j != NDIM; j++) {
        file << variables[i + (VAR_MOMENTUM+j)*nelr] << " ";
      }
      file << std::endl;
    }
    }

  {
      std::ofstream file("density_energy");
    file << nel << " " << nelr << std::endl;
    for(int i = 0; i < nel; i++) {
      file << variables[i + VAR_DENSITY_ENERGY*nelr] << std::endl;
    }
    }
}

void compute_velocity(float  density, FLOAT3 momentum, FLOAT3* velocity){
    velocity->x = momentum.x / density;
    velocity->y = momentum.y / density;
    velocity->z = momentum.z / density;
}

void compute_velocity(float  density, float *momentum, float *velocity){
    velocity[0] = momentum[0] / density;
    velocity[1] = momentum[1] / density;
    velocity[2] = momentum[2] / density;
}

float compute_speed_sqd(FLOAT3 velocity){
    return velocity.x*velocity.x + velocity.y*velocity.y + velocity.z*velocity.z;
}

float compute_speed_sqd(float *velocity){
    return velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2];
}

float compute_pressure(float density, float density_energy, float speed_sqd){
    return ((float)(GAMMA) - (float)(1.0f))*(density_energy - (float)(0.5f)*density*speed_sqd);
}

float compute_speed_of_sound(float density, float pressure){
    //return sqrtf(float(GAMMA)*pressure/density);
    return sqrt((float)(GAMMA)*pressure/density);
}

void body_1(float *__restrict__ variables, float *__restrict__ areas,
            float *__restrict__ step_factors, long int nelr, long int i) {
  float density = variables[i + VAR_DENSITY*nelr];
  float momentum[3];
  momentum[0] = variables[i + (VAR_MOMENTUM+0)*nelr];
  momentum[1] = variables[i + (VAR_MOMENTUM+1)*nelr];
  momentum[2] = variables[i + (VAR_MOMENTUM+2)*nelr];

  float density_energy = variables[i + VAR_DENSITY_ENERGY*nelr];

  float velocity[3];     compute_velocity(density, momentum, velocity);
  float speed_sqd      = compute_speed_sqd(velocity);
  float pressure       = compute_pressure(density, density_energy, speed_sqd);
  float speed_of_sound = compute_speed_of_sound(density, pressure);

  // dt = float(0.5f) * sqrtf(areas[i]) /  (||v|| + c).... but when we do
  // time stepping, this later would need to be divided by the area, so we
  // just do it all at once
  step_factors[i] = (float)(0.5f) / (sqrt(areas[i]) * (sqrt(speed_sqd) + speed_of_sound));
}

void compute_step_factor_cpu (
          float * variables,    size_t size_variables,
          float * areas,        size_t size_areas,
          float * step_factors, size_t size_step_factors,
          long int nelr) {
  for (long int i = 0; i < nelr; ++i) {
    body_1(variables, areas, step_factors, nelr, i);
  }
}

void compute_step_factor_dev (
          float *__restrict__ variables,    size_t size_variables,
          float *__restrict__ areas,        size_t size_areas,
          float *__restrict__ step_factors, size_t size_step_factors,
          long int nelr) {
  auto s = __hpvm_parallel_section_begin();
  auto t = __hpvm_task_begin(4, variables, size_variables, areas, size_areas,
                                step_factors, size_step_factors, nelr,
                             1, step_factors, size_step_factors);
  auto si = __hpvm_parallel_section_begin();


  for (long int i = 0; i < nelr; ++i) {
    __hpvm_parallel_loop(1, 4, variables, size_variables, areas, size_areas,
                               step_factors, size_step_factors, nelr,
                            1, step_factors, size_step_factors);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    body_1(variables, areas, step_factors, nelr, i);
  }

  __hpvm_parallel_section_end(si);
  __hpvm_task_end(t);
  __hpvm_parallel_section_end(s);
}

// Manually inlining seems to break things, but not inlining breaks things in
// the GPU back-end
static inline
void body_2(float *__restrict__ variables,
            float *__restrict__ flux_contribution_momentum_x,
            float *__restrict__ flux_contribution_momentum_y,
            float *__restrict__ flux_contribution_momentum_z,
            float *__restrict__ flux_contribution_density_energy,
            long int nelr, long int i) {
  float density_i = variables[i + VAR_DENSITY*nelr];
  float momentum_i[3];
  momentum_i[0] = variables[i + (VAR_MOMENTUM+0)*nelr];
  momentum_i[1] = variables[i + (VAR_MOMENTUM+1)*nelr];
  momentum_i[2] = variables[i + (VAR_MOMENTUM+2)*nelr];
  float density_energy_i = variables[i + VAR_DENSITY_ENERGY*nelr];

  float velocity_i[3];
  compute_velocity(density_i, momentum_i, velocity_i);
  float speed_sqd_i = compute_speed_sqd(velocity_i);
  float pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
  float flux_contribution_i_momentum_x[3],
        flux_contribution_i_momentum_y[3],
        flux_contribution_i_momentum_z[3];

  float flux_contribution_i_density_energy[3];
  compute_flux_contribution(density_i, momentum_i, density_energy_i,
          pressure_i, velocity_i, flux_contribution_i_momentum_x,
          flux_contribution_i_momentum_y, flux_contribution_i_momentum_z,
          flux_contribution_i_density_energy);

  flux_contribution_momentum_x[i + 0*nelr] = flux_contribution_i_momentum_x[0];
  flux_contribution_momentum_x[i + 1*nelr] = flux_contribution_i_momentum_x[1];
  flux_contribution_momentum_x[i + 2*nelr] = flux_contribution_i_momentum_x[2];

  flux_contribution_momentum_y[i + 0*nelr] = flux_contribution_i_momentum_y[0];
  flux_contribution_momentum_y[i + 1*nelr] = flux_contribution_i_momentum_y[1];
  flux_contribution_momentum_y[i + 2*nelr] = flux_contribution_i_momentum_y[2];


  flux_contribution_momentum_z[i + 0*nelr] = flux_contribution_i_momentum_z[0];
  flux_contribution_momentum_z[i + 1*nelr] = flux_contribution_i_momentum_z[1];
  flux_contribution_momentum_z[i + 2*nelr] = flux_contribution_i_momentum_z[2];

  flux_contribution_density_energy[i + 0*nelr] = flux_contribution_i_density_energy[0];
  flux_contribution_density_energy[i + 1*nelr] = flux_contribution_i_density_energy[1];
  flux_contribution_density_energy[i + 2*nelr] = flux_contribution_i_density_energy[2];
}

#define smoothing_coefficient 0.2f
static inline
void body_3_cpu(int *__restrict__ elements_surrounding_elements,
  float *__restrict__ normals, float *__restrict__ variables,
  float *__restrict__ ff_variable, float *__restrict__ fluxes,
  float *__restrict__ ff_flux_contribution_density_energy,
  float *__restrict__ ff_flux_contribution_momentum_x,
  float *__restrict__ ff_flux_contribution_momentum_y,
  float *__restrict__ ff_flux_contribution_momentum_z,
  float *__restrict__ flux_contribution_momentum_x,
  float *__restrict__ flux_contribution_momentum_y,
  float *__restrict__ flux_contribution_momentum_z,
  float *__restrict__ flux_contribution_density_energy,
  long int nelr, long int i) {

  int j, nb;
  float normal[3]; float normal_len;
  float factor;

  float density_i = variables[i + VAR_DENSITY*nelr];
  float momentum_i[3];

  momentum_i[0] = variables[i + (VAR_MOMENTUM+0)*nelr];
  momentum_i[1] = variables[i + (VAR_MOMENTUM+1)*nelr];
  momentum_i[2] = variables[i + (VAR_MOMENTUM+2)*nelr];
  float density_energy_i = variables[i + VAR_DENSITY_ENERGY*nelr];

  float velocity_i[3];
  compute_velocity(density_i, momentum_i, velocity_i);
  float speed_sqd_i = compute_speed_sqd(velocity_i);
  float speed_i = sqrt(speed_sqd_i);
  float pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
  float speed_of_sound_i = compute_speed_of_sound(density_i, pressure_i);
  float flux_contribution_i_momentum_x[3],
        flux_contribution_i_momentum_y[3],
        flux_contribution_i_momentum_z[3];
  float flux_contribution_i_density_energy[3];

  flux_contribution_i_momentum_x[0] = flux_contribution_momentum_x[i + 0*nelr];
  flux_contribution_i_momentum_x[1] = flux_contribution_momentum_x[i + 1*nelr];
  flux_contribution_i_momentum_x[2] = flux_contribution_momentum_x[i + 2*nelr];

  flux_contribution_i_momentum_y[0] = flux_contribution_momentum_y[i + 0*nelr];
  flux_contribution_i_momentum_y[1] = flux_contribution_momentum_y[i + 1*nelr];
  flux_contribution_i_momentum_y[2] = flux_contribution_momentum_y[i + 2*nelr];

  flux_contribution_i_momentum_z[0] = flux_contribution_momentum_z[i + 0*nelr];
  flux_contribution_i_momentum_z[1] = flux_contribution_momentum_z[i + 1*nelr];
  flux_contribution_i_momentum_z[2] = flux_contribution_momentum_z[i + 2*nelr];

  flux_contribution_i_density_energy[0] = flux_contribution_density_energy[i + 0*nelr];
  flux_contribution_i_density_energy[1] = flux_contribution_density_energy[i + 1*nelr];
  flux_contribution_i_density_energy[2] = flux_contribution_density_energy[i + 2*nelr];


  float flux_i_density = 0.0;
  float flux_i_momentum[3];
  flux_i_momentum[0] = 0.0;
  flux_i_momentum[1] = 0.0;
  flux_i_momentum[2] = 0.0;
  float flux_i_density_energy = 0.0;

  float velocity_nb[3];
  float density_nb, density_energy_nb;
  float momentum_nb[3];
  float flux_contribution_nb_momentum_x[3],
        flux_contribution_nb_momentum_y[3],
        flux_contribution_nb_momentum_z[3];
  float flux_contribution_nb_density_energy[3];
  float speed_sqd_nb, speed_of_sound_nb, pressure_nb;

  for(j = 0; j < NNB; j++) {
    // TODO: can't build with this here because the call is never being replaced
    // by an intrinsic
    nb = elements_surrounding_elements[i + j*nelr];
    normal[0] = normals[i + (j + 0*NNB)*nelr];
    normal[1] = normals[i + (j + 1*NNB)*nelr];
    normal[2] = normals[i + (j + 2*NNB)*nelr];
    normal_len = sqrt(normal[0]*normal[0]
                    + normal[1]*normal[1]
                    + normal[2]*normal[2]);

    if (nb >= 0) { // a legitimate neighbor
      density_nb = variables[nb + VAR_DENSITY*nelr];
      momentum_nb[0] = variables[nb + (VAR_MOMENTUM+0)*nelr];
      momentum_nb[1] = variables[nb + (VAR_MOMENTUM+1)*nelr];
      momentum_nb[2] = variables[nb + (VAR_MOMENTUM+2)*nelr];
      density_energy_nb = variables[nb + VAR_DENSITY_ENERGY*nelr];
      compute_velocity(density_nb, momentum_nb, velocity_nb);
      speed_sqd_nb = compute_speed_sqd(velocity_nb);
      pressure_nb = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
      speed_of_sound_nb = compute_speed_of_sound(density_nb, pressure_nb);

      flux_contribution_nb_momentum_x[0] = flux_contribution_momentum_x[nb + 0*nelr];
      flux_contribution_nb_momentum_x[1] = flux_contribution_momentum_x[nb + 1*nelr];
      flux_contribution_nb_momentum_x[2] = flux_contribution_momentum_x[nb + 2*nelr];

      flux_contribution_nb_momentum_y[0] = flux_contribution_momentum_y[nb + 0*nelr];
      flux_contribution_nb_momentum_y[1] = flux_contribution_momentum_y[nb + 1*nelr];
      flux_contribution_nb_momentum_y[2] = flux_contribution_momentum_y[nb + 2*nelr];

      flux_contribution_nb_momentum_z[0] = flux_contribution_momentum_z[nb + 0*nelr];
      flux_contribution_nb_momentum_z[1] = flux_contribution_momentum_z[nb + 1*nelr];
      flux_contribution_nb_momentum_z[2] = flux_contribution_momentum_z[nb + 2*nelr];

      flux_contribution_nb_density_energy[0] = flux_contribution_density_energy[nb + 0*nelr];
      flux_contribution_nb_density_energy[1] = flux_contribution_density_energy[nb + 1*nelr];
      flux_contribution_nb_density_energy[2] = flux_contribution_density_energy[nb + 2*nelr];

      // artificial viscosity
      factor = -normal_len*smoothing_coefficient*(float)(0.5f)
        * (speed_i + sqrt(speed_sqd_nb) + speed_of_sound_i + speed_of_sound_nb);
      flux_i_density += factor*(density_i-density_nb);
      flux_i_density_energy += factor*(density_energy_i-density_energy_nb);
      flux_i_momentum[0] += factor*(momentum_i[0]-momentum_nb[0]);
      flux_i_momentum[1] += factor*(momentum_i[1]-momentum_nb[1]);
      flux_i_momentum[2] += factor*(momentum_i[2]-momentum_nb[2]);

      // accumulate cell-centered fluxes
      factor = (float)(0.5f)*normal[0];
      flux_i_density += factor*(momentum_nb[0]+momentum_i[0]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[0]
                            + flux_contribution_i_density_energy[0]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[0]
                            + flux_contribution_i_momentum_x[0]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[0]
                            + flux_contribution_i_momentum_y[0]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[0]
                            + flux_contribution_i_momentum_z[0]);

      factor = (float)(0.5f)*normal[1];
      flux_i_density += factor*(momentum_nb[1]+momentum_i[1]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[1]
                            + flux_contribution_i_density_energy[1]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[1]
                            + flux_contribution_i_momentum_x[1]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[1]
                            + flux_contribution_i_momentum_y[1]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[1]
                            + flux_contribution_i_momentum_z[1]);

      factor = (float)(0.5f)*normal[2];
      flux_i_density += factor*(momentum_nb[2]+momentum_i[2]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[2]
                            + flux_contribution_i_density_energy[2]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[2]
                            + flux_contribution_i_momentum_x[2]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[2]
                            + flux_contribution_i_momentum_y[2]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[2]
                            + flux_contribution_i_momentum_z[2]);
    } else if(nb == -1) { // a wing boundary
      flux_i_momentum[0] += normal[0]*pressure_i;
      flux_i_momentum[1] += normal[1]*pressure_i;
      flux_i_momentum[2] += normal[2]*pressure_i;
    } else { //if(nb == -2) { // a far field boundary
      factor = (float)(0.5f)*normal[0];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+0]+momentum_i[0]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[0]
                            + flux_contribution_i_density_energy[0]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[0]
                            + flux_contribution_i_momentum_x[0]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[0]
                            + flux_contribution_i_momentum_y[0]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[0]
                            + flux_contribution_i_momentum_z[0]);

      factor = (float)(0.5f)*normal[1];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+1]+momentum_i[1]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[1]
                            + flux_contribution_i_density_energy[1]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[1]
                            + flux_contribution_i_momentum_x[1]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[1]
                            + flux_contribution_i_momentum_y[1]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[1]
                            + flux_contribution_i_momentum_z[1]);

      factor = (float)(0.5f)*normal[2];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+2]+momentum_i[2]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[2]
                            + flux_contribution_i_density_energy[2]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[2]
                            + flux_contribution_i_momentum_x[2]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[2]
                            + flux_contribution_i_momentum_y[2]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[2]
                            + flux_contribution_i_momentum_z[2]);

    }
  }

  fluxes[i + VAR_DENSITY*nelr] = flux_i_density;
  fluxes[i + (VAR_MOMENTUM+0)*nelr] = flux_i_momentum[0];
  fluxes[i + (VAR_MOMENTUM+1)*nelr] = flux_i_momentum[1];
  fluxes[i + (VAR_MOMENTUM+2)*nelr] = flux_i_momentum[2];
  fluxes[i + VAR_DENSITY_ENERGY*nelr] = flux_i_density_energy;
}

static inline
void body_3(int *__restrict__ elements_surrounding_elements,
            float *__restrict__ normals, float *__restrict__ variables,
            float *__restrict__ ff_variable, float *__restrict__ fluxes,
            float *__restrict__ ff_flux_contribution_density_energy,
            float *__restrict__ ff_flux_contribution_momentum_x,
            float *__restrict__ ff_flux_contribution_momentum_y,
            float *__restrict__ ff_flux_contribution_momentum_z,
            float *__restrict__ flux_contribution_momentum_x,
            float *__restrict__ flux_contribution_momentum_y,
            float *__restrict__ flux_contribution_momentum_z,
            float *__restrict__ flux_contribution_density_energy,
            long int nelr, long int i) {
  int j, nb;
  float normal[3]; float normal_len;
  float factor;

  float density_i = variables[i + VAR_DENSITY*nelr];
  float momentum_i[3];

  momentum_i[0] = variables[i + (VAR_MOMENTUM+0)*nelr];
  momentum_i[1] = variables[i + (VAR_MOMENTUM+1)*nelr];
  momentum_i[2] = variables[i + (VAR_MOMENTUM+2)*nelr];
  float density_energy_i = variables[i + VAR_DENSITY_ENERGY*nelr];

  float velocity_i[3];
  compute_velocity(density_i, momentum_i, velocity_i);
  float speed_sqd_i = compute_speed_sqd(velocity_i);
  float speed_i = sqrt(speed_sqd_i);
  float pressure_i = compute_pressure(density_i, density_energy_i, speed_sqd_i);
  float speed_of_sound_i = compute_speed_of_sound(density_i, pressure_i);
  float flux_contribution_i_momentum_x[3],
        flux_contribution_i_momentum_y[3],
        flux_contribution_i_momentum_z[3];
  float flux_contribution_i_density_energy[3];

  flux_contribution_i_momentum_x[0] = flux_contribution_momentum_x[i + 0*nelr];
  flux_contribution_i_momentum_x[1] = flux_contribution_momentum_x[i + 1*nelr];
  flux_contribution_i_momentum_x[2] = flux_contribution_momentum_x[i + 2*nelr];

  flux_contribution_i_momentum_y[0] = flux_contribution_momentum_y[i + 0*nelr];
  flux_contribution_i_momentum_y[1] = flux_contribution_momentum_y[i + 1*nelr];
  flux_contribution_i_momentum_y[2] = flux_contribution_momentum_y[i + 2*nelr];

  flux_contribution_i_momentum_z[0] = flux_contribution_momentum_z[i + 0*nelr];
  flux_contribution_i_momentum_z[1] = flux_contribution_momentum_z[i + 1*nelr];
  flux_contribution_i_momentum_z[2] = flux_contribution_momentum_z[i + 2*nelr];

  flux_contribution_i_density_energy[0] = flux_contribution_density_energy[i + 0*nelr];
  flux_contribution_i_density_energy[1] = flux_contribution_density_energy[i + 1*nelr];
  flux_contribution_i_density_energy[2] = flux_contribution_density_energy[i + 2*nelr];


  float flux_i_density = 0.0;
  float flux_i_momentum[3];
  flux_i_momentum[0] = 0.0;
  flux_i_momentum[1] = 0.0;
  flux_i_momentum[2] = 0.0;
  float flux_i_density_energy = 0.0;

  float velocity_nb[3];
  float density_nb, density_energy_nb;
  float momentum_nb[3];
  float flux_contribution_nb_momentum_x[3],
        flux_contribution_nb_momentum_y[3],
        flux_contribution_nb_momentum_z[3];
  float flux_contribution_nb_density_energy[3];
  float speed_sqd_nb, speed_of_sound_nb, pressure_nb;

  for(j = 0; j < NNB; j++) {
    __hpvm__isNonZeroLoop(j, NNB);
    // TODO: can't build with this here because the call is never being replaced
    // by an intrinsic
    nb = elements_surrounding_elements[i + j*nelr];
    normal[0] = normals[i + (j + 0*NNB)*nelr];
    normal[1] = normals[i + (j + 1*NNB)*nelr];
    normal[2] = normals[i + (j + 2*NNB)*nelr];
    normal_len = sqrt(normal[0]*normal[0]
                    + normal[1]*normal[1]
                    + normal[2]*normal[2]);

    if (nb >= 0) { // a legitimate neighbor
      density_nb = variables[nb + VAR_DENSITY*nelr];
      momentum_nb[0] = variables[nb + (VAR_MOMENTUM+0)*nelr];
      momentum_nb[1] = variables[nb + (VAR_MOMENTUM+1)*nelr];
      momentum_nb[2] = variables[nb + (VAR_MOMENTUM+2)*nelr];
      density_energy_nb = variables[nb + VAR_DENSITY_ENERGY*nelr];
      compute_velocity(density_nb, momentum_nb, velocity_nb);
      speed_sqd_nb = compute_speed_sqd(velocity_nb);
      pressure_nb = compute_pressure(density_nb, density_energy_nb, speed_sqd_nb);
      speed_of_sound_nb = compute_speed_of_sound(density_nb, pressure_nb);

      flux_contribution_nb_momentum_x[0] = flux_contribution_momentum_x[nb + 0*nelr];
      flux_contribution_nb_momentum_x[1] = flux_contribution_momentum_x[nb + 1*nelr];
      flux_contribution_nb_momentum_x[2] = flux_contribution_momentum_x[nb + 2*nelr];

      flux_contribution_nb_momentum_y[0] = flux_contribution_momentum_y[nb + 0*nelr];
      flux_contribution_nb_momentum_y[1] = flux_contribution_momentum_y[nb + 1*nelr];
      flux_contribution_nb_momentum_y[2] = flux_contribution_momentum_y[nb + 2*nelr];

      flux_contribution_nb_momentum_z[0] = flux_contribution_momentum_z[nb + 0*nelr];
      flux_contribution_nb_momentum_z[1] = flux_contribution_momentum_z[nb + 1*nelr];
      flux_contribution_nb_momentum_z[2] = flux_contribution_momentum_z[nb + 2*nelr];

      flux_contribution_nb_density_energy[0] = flux_contribution_density_energy[nb + 0*nelr];
      flux_contribution_nb_density_energy[1] = flux_contribution_density_energy[nb + 1*nelr];
      flux_contribution_nb_density_energy[2] = flux_contribution_density_energy[nb + 2*nelr];

      // artificial viscosity
      factor = -normal_len*smoothing_coefficient*(float)(0.5f)
        * (speed_i + sqrt(speed_sqd_nb) + speed_of_sound_i + speed_of_sound_nb);
      flux_i_density += factor*(density_i-density_nb);
      flux_i_density_energy += factor*(density_energy_i-density_energy_nb);
      flux_i_momentum[0] += factor*(momentum_i[0]-momentum_nb[0]);
      flux_i_momentum[1] += factor*(momentum_i[1]-momentum_nb[1]);
      flux_i_momentum[2] += factor*(momentum_i[2]-momentum_nb[2]);

      // accumulate cell-centered fluxes
      factor = (float)(0.5f)*normal[0];
      flux_i_density += factor*(momentum_nb[0]+momentum_i[0]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[0]
                            + flux_contribution_i_density_energy[0]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[0]
                            + flux_contribution_i_momentum_x[0]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[0]
                            + flux_contribution_i_momentum_y[0]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[0]
                            + flux_contribution_i_momentum_z[0]);

      factor = (float)(0.5f)*normal[1];
      flux_i_density += factor*(momentum_nb[1]+momentum_i[1]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[1]
                            + flux_contribution_i_density_energy[1]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[1]
                            + flux_contribution_i_momentum_x[1]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[1]
                            + flux_contribution_i_momentum_y[1]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[1]
                            + flux_contribution_i_momentum_z[1]);

      factor = (float)(0.5f)*normal[2];
      flux_i_density += factor*(momentum_nb[2]+momentum_i[2]);
      flux_i_density_energy += factor*(flux_contribution_nb_density_energy[2]
                            + flux_contribution_i_density_energy[2]);
      flux_i_momentum[0] += factor*(flux_contribution_nb_momentum_x[2]
                            + flux_contribution_i_momentum_x[2]);
      flux_i_momentum[1] += factor*(flux_contribution_nb_momentum_y[2]
                            + flux_contribution_i_momentum_y[2]);
      flux_i_momentum[2] += factor*(flux_contribution_nb_momentum_z[2]
                            + flux_contribution_i_momentum_z[2]);
    } else if(nb == -1) { // a wing boundary
      flux_i_momentum[0] += normal[0]*pressure_i;
      flux_i_momentum[1] += normal[1]*pressure_i;
      flux_i_momentum[2] += normal[2]*pressure_i;
    } else { //if(nb == -2) { // a far field boundary
      factor = (float)(0.5f)*normal[0];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+0]+momentum_i[0]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[0]
                            + flux_contribution_i_density_energy[0]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[0]
                            + flux_contribution_i_momentum_x[0]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[0]
                            + flux_contribution_i_momentum_y[0]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[0]
                            + flux_contribution_i_momentum_z[0]);

      factor = (float)(0.5f)*normal[1];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+1]+momentum_i[1]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[1]
                            + flux_contribution_i_density_energy[1]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[1]
                            + flux_contribution_i_momentum_x[1]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[1]
                            + flux_contribution_i_momentum_y[1]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[1]
                            + flux_contribution_i_momentum_z[1]);

      factor = (float)(0.5f)*normal[2];
      flux_i_density += factor*(ff_variable[VAR_MOMENTUM+2]+momentum_i[2]);
      flux_i_density_energy += factor*(ff_flux_contribution_density_energy[2]
                            + flux_contribution_i_density_energy[2]);
      flux_i_momentum[0] += factor*(ff_flux_contribution_momentum_x[2]
                            + flux_contribution_i_momentum_x[2]);
      flux_i_momentum[1] += factor*(ff_flux_contribution_momentum_y[2]
                            + flux_contribution_i_momentum_y[2]);
      flux_i_momentum[2] += factor*(ff_flux_contribution_momentum_z[2]
                            + flux_contribution_i_momentum_z[2]);

    }
  }

  fluxes[i + VAR_DENSITY*nelr] = flux_i_density;
  fluxes[i + (VAR_MOMENTUM+0)*nelr] = flux_i_momentum[0];
  fluxes[i + (VAR_MOMENTUM+1)*nelr] = flux_i_momentum[1];
  fluxes[i + (VAR_MOMENTUM+2)*nelr] = flux_i_momentum[2];
  fluxes[i + VAR_DENSITY_ENERGY*nelr] = flux_i_density_energy;
}
#undef smoothing_coefficient

static inline
void body_4(float *__restrict__ old_variables, float *__restrict__ variables,
            float *__restrict__ step_factors, float *__restrict__ fluxes,
            int j, long int nelr, long int i) {
  float factor = step_factors[i]/(float)(RK+1-j);
  variables[i + VAR_DENSITY*nelr] = old_variables[i + VAR_DENSITY*nelr]
                  + factor*fluxes[i + VAR_DENSITY*nelr];
  variables[i + VAR_DENSITY_ENERGY*nelr] = old_variables[i + VAR_DENSITY_ENERGY*nelr]
                  + factor*fluxes[i + VAR_DENSITY_ENERGY*nelr];
  variables[i + (VAR_MOMENTUM+0)*nelr] = old_variables[i + (VAR_MOMENTUM+0)*nelr]
                  + factor*fluxes[i + (VAR_MOMENTUM+0)*nelr];
  variables[i + (VAR_MOMENTUM+1)*nelr] = old_variables[i + (VAR_MOMENTUM+1)*nelr]
                  + factor*fluxes[i + (VAR_MOMENTUM+1)*nelr];
  variables[i + (VAR_MOMENTUM+2)*nelr] = old_variables[i + (VAR_MOMENTUM+2)*nelr]
                  + factor*fluxes[i + (VAR_MOMENTUM+2)*nelr];
}

void compute_flux_time_step_cpu(
  float *__restrict__ variables,     size_t size_variables,
  float *__restrict__ old_variables, size_t size_old_variables,
  float *__restrict__ flux_contribution_momentum_x,
  size_t size_flux_contribution_momentum_x,
  float *__restrict__ flux_contribution_momentum_y,
  size_t size_flux_contribution_momentum_y,
  float *__restrict__ flux_contribution_momentum_z,
  size_t size_flux_contribution_momentum_z,
  float *__restrict__ flux_contribution_density_energy,
  size_t size_flux_contribution_density_energy,
  int *__restrict__ elements_surrounding_elements,
  size_t size_elements_surrounding_elements,
  float *__restrict__ normals,       size_t size_normals,
  float *__restrict__ ff_variable,   size_t size_ff_variable,
  float *__restrict__ fluxes,        size_t size_fluxes,
  float *__restrict__ ff_flux_contribution_density_energy,
  size_t size_ff_flux_contribution_density_energy,
  float *__restrict__ ff_flux_contribution_momentum_x,
  size_t size_ff_flux_contribution_momentum_x,
  float *__restrict__ ff_flux_contribution_momentum_y,
  size_t size_ff_flux_contribution_momentum_y,
  float *__restrict__ ff_flux_contribution_momentum_z,
  size_t size_ff_flux_contribution_momentum_z,
  float *__restrict__ step_factors,  size_t size_step_factors,
  int j, long int nelr) {
  for (long int i = 0; i < nelr; i++) {
    body_2(variables, flux_contribution_momentum_x, flux_contribution_momentum_y,
           flux_contribution_momentum_z, flux_contribution_density_energy,
           nelr, i);
  }
  for (long int i = 0; i < nelr; i++) {
        body_3_cpu(elements_surrounding_elements, normals, variables, ff_variable,
           fluxes, ff_flux_contribution_density_energy,
           ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y,
           ff_flux_contribution_momentum_z, flux_contribution_momentum_x,
           flux_contribution_momentum_y, flux_contribution_momentum_z,
           flux_contribution_density_energy, nelr, i);
  }
  for (long int i = 0; i < nelr; i++) {
    body_4(old_variables, variables, step_factors, fluxes, j, nelr, i);
  }
}

void compute_flux_time_step(
          float *__restrict__ variables,     size_t size_variables,
          float *__restrict__ old_variables, size_t size_old_variables,
          float *__restrict__ flux_contribution_momentum_x,
                                      size_t size_flux_contribution_momentum_x,
          float *__restrict__ flux_contribution_momentum_y,
                                      size_t size_flux_contribution_momentum_y,
          float *__restrict__ flux_contribution_momentum_z,
                                      size_t size_flux_contribution_momentum_z,
          float *__restrict__ flux_contribution_density_energy,
                                  size_t size_flux_contribution_density_energy,
          int *__restrict__ elements_surrounding_elements,
                                     size_t size_elements_surrounding_elements,
          float *__restrict__ normals,       size_t size_normals,
          float *__restrict__ ff_variable,   size_t size_ff_variable,
          float *__restrict__ fluxes,        size_t size_fluxes,
          float *__restrict__ ff_flux_contribution_density_energy,
                               size_t size_ff_flux_contribution_density_energy,
          float *__restrict__ ff_flux_contribution_momentum_x,
                                   size_t size_ff_flux_contribution_momentum_x,
          float *__restrict__ ff_flux_contribution_momentum_y,
                                   size_t size_ff_flux_contribution_momentum_y,
          float *__restrict__ ff_flux_contribution_momentum_z,
                                   size_t size_ff_flux_contribution_momentum_z,
          float *__restrict__ step_factors,  size_t size_step_factors,
          int j, long int nelr) {
  auto s = __hpvm_parallel_section_begin();


  auto t1 = __hpvm_task_begin(6, variables, size_variables,
          flux_contribution_momentum_x, size_flux_contribution_momentum_x,
          flux_contribution_momentum_y, size_flux_contribution_momentum_y,
          flux_contribution_momentum_z, size_flux_contribution_momentum_z,
          flux_contribution_density_energy, size_flux_contribution_density_energy,
          nelr,
       4, flux_contribution_momentum_x, size_flux_contribution_momentum_x,
          flux_contribution_momentum_y, size_flux_contribution_momentum_y,
          flux_contribution_momentum_z, size_flux_contribution_momentum_z,
          flux_contribution_density_energy, size_flux_contribution_density_energy);
  auto s1 = __hpvm_parallel_section_begin();

  for (long int i = 0; i < nelr; i++) {
    __hpvm_parallel_loop(1, 6, variables, size_variables,
          flux_contribution_momentum_x, size_flux_contribution_momentum_x,
          flux_contribution_momentum_y, size_flux_contribution_momentum_y,
          flux_contribution_momentum_z, size_flux_contribution_momentum_z,
          flux_contribution_density_energy, size_flux_contribution_density_energy,
          nelr,
       4, flux_contribution_momentum_x, size_flux_contribution_momentum_x,
          flux_contribution_momentum_y, size_flux_contribution_momentum_y,
          flux_contribution_momentum_z, size_flux_contribution_momentum_z,
          flux_contribution_density_energy, size_flux_contribution_density_energy);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    body_2(variables, flux_contribution_momentum_x, flux_contribution_momentum_y,
           flux_contribution_momentum_z, flux_contribution_density_energy,
           nelr, i);
  }
  __hpvm_parallel_section_end(s1);
  __hpvm_task_end(t1);

  auto t2 = __hpvm_task_begin(14,
        elements_surrounding_elements, size_elements_surrounding_elements,
        normals, size_normals,
        variables, size_variables,
        ff_variable, size_ff_variable,
        fluxes, size_fluxes,
        ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
        ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
        ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
        ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
        flux_contribution_momentum_x, size_flux_contribution_momentum_x,
        flux_contribution_momentum_y, size_flux_contribution_momentum_y,
        flux_contribution_momentum_z, size_flux_contribution_momentum_z,
        flux_contribution_density_energy, size_flux_contribution_density_energy,
        nelr,
     5, flux_contribution_momentum_x, size_flux_contribution_momentum_x,
        flux_contribution_momentum_y, size_flux_contribution_momentum_y,
        flux_contribution_momentum_z, size_flux_contribution_momentum_z,
        flux_contribution_density_energy, size_flux_contribution_density_energy,
        fluxes, size_fluxes);
  auto s2 = __hpvm_parallel_section_begin();

  for (long int i = 0; i < nelr; i++) {
    __hpvm_parallel_loop(1, 14,
        elements_surrounding_elements, size_elements_surrounding_elements,
        normals, size_normals,
        variables, size_variables,
        ff_variable, size_ff_variable,
        fluxes, size_fluxes,
        ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
        ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
        ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
        ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
        flux_contribution_momentum_x, size_flux_contribution_momentum_x,
        flux_contribution_momentum_y, size_flux_contribution_momentum_y,
        flux_contribution_momentum_z, size_flux_contribution_momentum_z,
        flux_contribution_density_energy, size_flux_contribution_density_energy,
        nelr,
     5, flux_contribution_momentum_x, size_flux_contribution_momentum_x,
        flux_contribution_momentum_y, size_flux_contribution_momentum_y,
        flux_contribution_momentum_z, size_flux_contribution_momentum_z,
        flux_contribution_density_energy, size_flux_contribution_density_energy,
        fluxes, size_fluxes);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    body_3(elements_surrounding_elements, normals, variables, ff_variable,
           fluxes, ff_flux_contribution_density_energy,
           ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y,
           ff_flux_contribution_momentum_z, flux_contribution_momentum_x,
           flux_contribution_momentum_y, flux_contribution_momentum_z,
           flux_contribution_density_energy, nelr, i);
  }
  __hpvm_parallel_section_end(s2);
  __hpvm_task_end(t2);

  auto t3 = __hpvm_task_begin(6, old_variables, size_old_variables,
                                 variables,     size_variables,
                                 step_factors,  size_step_factors,
                                 fluxes,        size_fluxes,
                                 j, nelr,
                              1, variables, size_variables);
  auto s3 = __hpvm_parallel_section_begin();

  for (long int i = 0; i < nelr; i++) {
    __hpvm_parallel_loop(1, 6, old_variables, size_old_variables,
                               variables,     size_variables,
                               step_factors,  size_step_factors,
                               fluxes,        size_fluxes,
                               j, nelr,
                            1, variables, size_variables);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(i, NELR_NUM);

    body_4(old_variables, variables, step_factors, fluxes, j, nelr, i);
  }
  __hpvm_parallel_section_end(s3);
  __hpvm_task_end(t3);

  __hpvm_parallel_section_end(s);
}

static bool compareFloat(double a, double b) {
    if (abs(a - b) < FLOAT_COMPARE)
        return true;
    return false;
}

int main(int argc, char ** argv) {
    auto mainStart = std::chrono::steady_clock::now();

    if (argc < 4) {
        std::cout << "specify data file name, iterations, and block size\n";
        std::cout << "example: ./euler3d ../../data/cfd/fvcorr.domn.097K 1000 16\n";
        return 0;
    }

  const char* data_file_name = argv[1];
    int iterations = atoi(argv[2]);
    block_size = atoi(argv[3]);

  float *ff_variable = NULL;
  //FLOAT3 *ff_flux_contribution_momentum_x = NULL, *ff_flux_contribution_momentum_y = NULL,
  //      *ff_flux_contribution_momentum_z = NULL, *ff_flux_contribution_density_energy = NULL;   // CONTANT in OpenCL


  float *ff_flux_contribution_momentum_x = NULL, *ff_flux_contribution_momentum_y = NULL,
        *ff_flux_contribution_momentum_z = NULL, *ff_flux_contribution_density_energy = NULL; 

  float *areas = NULL, *normals = NULL;
  int *elements_surrounding_elements = NULL;

  float *variables = NULL, *old_variables = NULL, *fluxes = NULL, *step_factors = NULL;

  size_t size_ff_variable = NVAR*sizeof(float);
  ff_variable = (float *) __hetero_malloc(size_ff_variable);
  size_t size_ff_flux_contribution_momentum_x = sizeof(float) * 3;
  ff_flux_contribution_momentum_x = (float *) __hetero_malloc(size_ff_flux_contribution_momentum_x);
  size_t size_ff_flux_contribution_momentum_y = sizeof(float) * 3;
  ff_flux_contribution_momentum_y = (float *) __hetero_malloc(size_ff_flux_contribution_momentum_y);
  size_t size_ff_flux_contribution_momentum_z = sizeof(float) * 3;
  ff_flux_contribution_momentum_z = (float *) __hetero_malloc(size_ff_flux_contribution_momentum_z);
  size_t size_ff_flux_contribution_density_energy = sizeof(float) * 3;
  ff_flux_contribution_density_energy = (float *) __hetero_malloc(size_ff_flux_contribution_density_energy);

  auto init = [&] ()
  {
    const float angle_of_attack = float(3.1415926535897931 / 180.0f) * float(deg_angle_of_attack);
    ff_variable[VAR_DENSITY] = float(1.4);

    float ff_pressure = float(1.0f);
    float ff_speed_of_sound = sqrt(GAMMA*ff_pressure / ff_variable[VAR_DENSITY]);
    float ff_speed = float(ff_mach)*ff_speed_of_sound;

    float ff_velocity[3];
    ff_velocity[0] = ff_speed*float(cos((float)angle_of_attack));
    ff_velocity[1] = ff_speed*float(sin((float)angle_of_attack));
    ff_velocity[2] = 0.0f;

    ff_variable[VAR_MOMENTUM+0] = ff_variable[VAR_DENSITY] * ff_velocity[0];
      ff_variable[VAR_MOMENTUM+1] = ff_variable[VAR_DENSITY] * ff_velocity[1];
      ff_variable[VAR_MOMENTUM+2] = ff_variable[VAR_DENSITY] * ff_velocity[2];

    ff_variable[VAR_DENSITY_ENERGY] = ff_variable[VAR_DENSITY]*(float(0.5f)*(ff_speed*ff_speed)) + (ff_pressure / float(GAMMA-1.0f));

    float ff_momentum[3];
    ff_momentum[0] = *(ff_variable+VAR_MOMENTUM+0);
    ff_momentum[1] = *(ff_variable+VAR_MOMENTUM+1);
    ff_momentum[2] = *(ff_variable+VAR_MOMENTUM+2);

    compute_flux_contribution(ff_variable[VAR_DENSITY], ff_momentum, ff_variable[VAR_DENSITY_ENERGY], ff_pressure, ff_velocity,
        ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y, ff_flux_contribution_momentum_z, ff_flux_contribution_density_energy);
  };

  init();

  size_t size_areas, size_normals, size_elements_surrounding_elements;
  int nel;
  long int nelr;

  {
    std::ifstream file(data_file_name);
    if(!file.is_open()){
      std::cerr << "can not find/open file!\n";
      abort();
      }

    file >> nel;
    nelr = block_size*((nel / block_size )+ std::min(1, nel % block_size));
    std::cout<<"\n--cambine: nel="<<nel<<", nelr="<<nelr<<std::endl;

    size_areas = sizeof(float) * nelr;
    areas = (float *) __hetero_malloc(size_areas);
    size_elements_surrounding_elements = sizeof(int) * nelr * NNB;
    elements_surrounding_elements = (int *) __hetero_malloc(size_elements_surrounding_elements);
    size_normals = sizeof(float) * nelr * NDIM * NNB;
    normals = (float *) __hetero_malloc(size_normals);

    // read in data
    std::cout << "\nReading data\n";
    for(int i = 0; i < nel; i++) {
      file >> areas[i];
      for(int j = 0; j < NNB; j++) {
        file >> elements_surrounding_elements[i + j*nelr];
        if(elements_surrounding_elements[i+j*nelr] < 0) elements_surrounding_elements[i+j*nelr] = -1;
        elements_surrounding_elements[i + j*nelr]--; //it's coming in with Fortran numbering

        for(int k = 0; k < NDIM; k++) {
          file >> normals[i + (j + k*NNB)*nelr];
          normals[i + (j + k*NNB)*nelr] = -normals[i + (j + k*NNB)*nelr];
        }
      }
    }

    // fill in remaining data
    int last = nel-1;
    for(int i = nel; i < nelr; i++) {
      areas[i] = areas[last];
      for(int j = 0; j < NNB; j++) {
        // duplicate the last element
        elements_surrounding_elements[i + j*nelr] = elements_surrounding_elements[last + j*nelr];
        for(int k = 0; k < NDIM; k++) normals[last + (j + k*NNB)*nelr] = normals[last + (j + k*NNB)*nelr];
      }
    }
  }

  size_t size_variables = sizeof(float) * nelr * NVAR;
  variables = (float *) __hetero_malloc(size_variables);
  size_t size_old_variables = sizeof(float) * nelr * NVAR;
  old_variables = (float *) __hetero_malloc(size_old_variables);
  size_t size_fluxes = sizeof(float) * nelr * NVAR;
  fluxes = (float *) __hetero_malloc(size_fluxes);
  size_t size_step_factors = sizeof(float) * nelr;
  step_factors = (float *) __hetero_malloc(size_step_factors);

  float hv[nelr*NVAR];
  fill_variable(nelr, ff_variable, hv);
  copy(variables, hv, nelr * NVAR);
  copy(old_variables, hv, nelr * NVAR);
  copy(fluxes, hv, nelr * NVAR);

  /*for(int i = 0; i < nelr; i++)
    step_factors[i] = 0;*/

  size_t size_flux_contribution_momentum_x = sizeof(float) * nelr * NDIM;
  float *flux_contribution_momentum_x = (float *) __hetero_malloc(size_flux_contribution_momentum_x);
  size_t size_flux_contribution_momentum_y = sizeof(float) * nelr * NDIM;
  float *flux_contribution_momentum_y = (float *) __hetero_malloc(size_flux_contribution_momentum_y);
  size_t size_flux_contribution_momentum_z = sizeof(float) * nelr * NDIM;
  float *flux_contribution_momentum_z = (float *) __hetero_malloc(size_flux_contribution_momentum_z);
  size_t size_flux_contribution_density_energy = sizeof(float) * nelr * NDIM;
  float *flux_contribution_density_energy = (float *) __hetero_malloc(size_flux_contribution_density_energy);

  std::cout << "\nStarting..." << std::endl;

  float totalTime = 0.0;
#ifdef REPORT_POWER
#ifdef CPU
  double totalEnergy = 0.0;
  // The GetEnergyCPU() implementation relies on us not switching processors
  // so to fascilitate this we pin ourselves to the current core, this is
  // also needed as it determines the processor we're running on
  int core = sched_getcpu();
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(core, &mask);
  if (sched_setaffinity(0, sizeof(mask), &mask)) {
    perror("Error in sched_setaffinity: ");
    exit(1);
  }
#endif
#ifdef GPU
  double totalPower = 0.0;
  pthread_barrier_init(&barrier, NULL, 2);
#endif
#endif

  // Begin iterations
  for(int i = 0; i < iterations; i++) {
    __hetero_copy_mem(variables, old_variables, sizeof(float) * nelr * NVAR);

#ifdef REPORT_POWER
#ifdef CPU
    double start_energy = GetEnergyCPU(core);
#endif
#ifdef GPU
    int flag = 0;
    pthread_t thd;
    pthread_create(&thd, NULL, measure_func, &flag);
    pthread_barrier_wait(&barrier);
#endif
#endif
    // Timing both kernels and the loop together, since there's no interveaning
    // code (and the loop is very cheap) this probably gets us more accurate
    // timings actually
    auto startTime = std::chrono::steady_clock::now();
    
    auto launch_1 = __hpvm_launch((void *) compute_step_factor_dev, 4,
      variables, size_variables,
      areas, size_areas,
      step_factors, size_step_factors,
      nelr, 1,
      step_factors, size_step_factors
    );
    __hpvm_wait(launch_1);

    for(int j = 0; j < RK; j++) {

      auto launch_2 = __hpvm_launch((void *) compute_flux_time_step, 17,
      variables, size_variables,
      old_variables, size_old_variables,
      flux_contribution_momentum_x, size_flux_contribution_momentum_x,
      flux_contribution_momentum_y, size_flux_contribution_momentum_y,
      flux_contribution_momentum_z, size_flux_contribution_momentum_z,
      flux_contribution_density_energy, size_flux_contribution_density_energy,
      elements_surrounding_elements, size_elements_surrounding_elements,
      normals, size_normals,
      ff_variable, size_ff_variable,
      fluxes, size_fluxes,
      ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
      ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
      ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
      ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
      step_factors, size_step_factors,
      j, nelr, 10,
      flux_contribution_momentum_x, size_flux_contribution_momentum_x,
      flux_contribution_momentum_y, size_flux_contribution_momentum_y,
      flux_contribution_momentum_z, size_flux_contribution_momentum_z,
      flux_contribution_density_energy, size_flux_contribution_density_energy,
      ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
      ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
      ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
      ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
      fluxes, size_fluxes,
      variables, size_variables
      );

      __hpvm_wait(launch_2);
    }

    auto elapsed = std::chrono::steady_clock::now() - startTime;
        totalTime += std::chrono::duration<float>(elapsed).count();
#ifdef REPORT_POWER
#ifdef CPU
    double end_energy = GetEnergyCPU(core);
    totalEnergy += end_energy - start_energy;
#endif
#ifdef GPU
    flag = 1;
    pthread_join(thd, NULL);
    totalPower += measuredPower;
#endif
#endif
  }
  auto endTime = std::chrono::steady_clock::now();
  double fullTime = std::chrono::duration<float>(endTime - mainStart).count();

  printf("\nKernel Execution took %f seconds\n", totalTime);
  printf("\nTotal Execution took %f seconds\n", fullTime);
#ifdef REPORT_POWER
#ifdef CPU
  printf("\nPower usage: %lf W\n", totalEnergy / totalTime);
  printf("Energy usage: %lf J\n", totalEnergy);
#endif
#ifdef GPU
  printf("\nPower usage: %lf W\n", totalPower / iterations);
  printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower / iterations, totalTime * 1000));
#endif
#endif

  __hetero_request_mem(variables, sizeof(float) * nelr * NVAR);
  std::cout << "\nSaving solution..." << std::endl;
  dump(variables, nel, nelr);

  // Verification vs CPU
  std::unique_ptr<float[]> variables_fpga(new float[size_variables]);
  copy(variables_fpga.get(), variables, nelr * NVAR);

  init();
  fill_variable(nelr, ff_variable, hv);
  copy(variables, hv, nelr * NVAR);
  copy(old_variables, hv, nelr * NVAR);
  copy(fluxes, hv, nelr * NVAR);
  
  for(int i = 0; i < iterations; i++) {
    copy(old_variables, variables, nelr * NVAR);
    compute_step_factor_cpu(variables, size_variables, areas, size_areas,
      step_factors, size_step_factors, nelr);
    for (int j = 0; j < RK; j++) {
      compute_flux_time_step_cpu(variables, size_variables,
        old_variables, size_old_variables,
        flux_contribution_momentum_x, size_flux_contribution_momentum_x,
        flux_contribution_momentum_y, size_flux_contribution_momentum_y,
        flux_contribution_momentum_z, size_flux_contribution_momentum_z,
        flux_contribution_density_energy, size_flux_contribution_density_energy,
        elements_surrounding_elements, size_elements_surrounding_elements,
        normals, size_normals,
        ff_variable, size_ff_variable,
        fluxes, size_fluxes,
        ff_flux_contribution_density_energy, size_ff_flux_contribution_density_energy,
        ff_flux_contribution_momentum_x, size_ff_flux_contribution_momentum_x,
        ff_flux_contribution_momentum_y, size_ff_flux_contribution_momentum_y,
        ff_flux_contribution_momentum_z, size_ff_flux_contribution_momentum_z,
        step_factors, size_step_factors,
        j, nelr);
    }
  }

  bool good = true;
  for (int i = 0; i < nelr * NVAR; i++) {
    if (!compareFloat(variables[i], variables_fpga[i])) {
      good = false;
      break;
    }
  }

  if (good)
    std::cout << "\nVERIFIED\n";
  else
    std::cout << "\nNOT VERIFIED\n";
  
  std::cout << "\nCleaning up..." << std::endl;

  __hetero_free(areas);
  __hetero_free(step_factors);
  __hetero_free(elements_surrounding_elements);
  __hetero_free(normals);
  __hetero_free(variables);
  __hetero_free(ff_variable);
  __hetero_free(fluxes);
  __hetero_free(ff_flux_contribution_density_energy);
  __hetero_free(ff_flux_contribution_momentum_x);
  __hetero_free(ff_flux_contribution_momentum_y);
  __hetero_free(ff_flux_contribution_momentum_z);
  __hetero_free(old_variables);
  __hetero_free(flux_contribution_momentum_x);
  __hetero_free(flux_contribution_momentum_y);
  __hetero_free(flux_contribution_momentum_z);
  __hetero_free(flux_contribution_density_energy);

  return 0;
}
