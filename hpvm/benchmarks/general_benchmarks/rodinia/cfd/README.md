# Rodinia: Computational Fluid Dynamics

A HeteroC++ implementation of the Computational Fluid Dynamics (CFD)
algorithms from the Rodinia benchmark suite. We present both a version with
precomputed fluxes (`pre_euler_main.cpp`) and without (`euler_main.cpp`).

## Building
The two versions of CFD can be built using the following command, from this
directory, where `<target>`, `<euler>`, and `[flags]` are described below.

```sh
make TARGET=<target> EULER=<euler> [flags]
```

The currently supported `<target>` are `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

The value of `<euler>` is either `euler` (to build the version without
precomputed fluxes) or `pre_euler` (to build the version with precomputed
fluxes).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
Both versions of CFD take an input file in the format of those found in 
`/data/cfd/` of the Rodinia distribution. The command to run is

```sh
./<euler>-<version> <src> <iters> <block>
```

where `<euler>` is either `euler` or `pre_euler` and `<version>` is based on
the target and flags used to build that version fo CFD. The `<iters>` is the
number of iterations to perform and `<block>` specifies the block size to use.

The program produces several result files: `density`, `density_energy`, and
`momentum`. It will also perform the computation with a sequential version and
compare the results.
