#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <chrono>
#include <heterocc.h>
#define IN_NUM 12800000
#define OUT_NUM 1
#define HID_NUM 16

#include <chrono>

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection requires sudo
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

#define FLOAT_COMPARE (1e-5)
bool compareFloat(float a, float b) {
  return abs(a - b) < FLOAT_COMPARE;
}

typedef struct {
  int input_n;
  int hidden_n;
  int output_n;

  float *input_units;
  float *hidden_units;
  float *output_units;

  float *hidden_delta;
  float *output_delta;

  float *target;

  float **input_weights;
  float **hidden_weights;

  float **input_prev_weights;
  float **hidden_prev_weights;
} BPNN;

// void *alignedMalloc(size_t size) {
//     void *ptr = NULL;
//     if (posix_memalign (&ptr, 64, size)) {
//         fprintf(stderr, "Aligned Malloc failed.\n");
//         exit (-1);
//     }
//     return ptr;
// }

// void *alignedMalloc(size_t size) {
//     void *ptr = __hetero_malloc(size);
//     return ptr;
// }

float *alloc_1d_dbl(int n)
{
  float *new_t;

  new_t = (float *) __hetero_malloc ((unsigned) (n * sizeof (float)));
  if (new_t == NULL) {
    printf("ALLOC_1D_DBL: Couldn't allocate array of floats\n");
    return (NULL);
  }
  return (new_t);
}

float **alloc_2d_dbl(int m, int n)
{
  int i;
  float **new_t;

  new_t = (float **) __hetero_malloc ((unsigned) (m * sizeof (float *)));
  if (new_t == NULL) {
    printf("ALLOC_2D_DBL: Couldn't allocate array of dbl ptrs\n");
    return (NULL);
  }

  for (i = 0; i < m; i++) {
    new_t[i] = alloc_1d_dbl(n);
  }

  return (new_t);
}

void bpnn_randomize_weights(float **w, int m, int n)
{
  int i, j;
  //printf("randweigh\n");
  for (i = 0; i <= m; i++) {
    for (j = 0; j <= n; j++) {
     w[i][j] = (float) rand()/RAND_MAX;
    //  w[i][j] = dpn1();
    }
  }
}

void bpnn_randomize_row(float *w, int m)
{
	int i;
    //printf("randrow\n");
	for (i = 0; i <= m; i++) {
     //w[i] = (float) rand()/RAND_MAX;
	 w[i] = 0.1;
    }
}


void bpnn_zero_weights(float **w, int m, int n)
{
  int i, j;
  //printf("zerowe\n");
  for (i = 0; i <= m; i++) {
    for (j = 0; j <= n; j++) {
      w[i][j] = 0.0;
    }
  }
}

void run_bp_cpu(BPNN *net, float *iwod, float *ipwod, float *hwod, float *hpwod, float *input_weights_one_dim, float *hidden_weights_one_dim, float *output_hidden_ocl, float *output_hidden_ocl2, float *output_delta_ocl)
{
  int in, hid, out;

  in = net->input_n;
  hid = net->hidden_n;
  out = net->output_n;

  float sum;

  net->input_units[0] = 1.0;

  for (int j = 1; j <= hid; j++)
	{
	  sum = 0.0;
	  for (int k = 0; k <= in; k++)
		{
		  sum += iwod[k * (hid + 1) + j] * net->input_units[k];
		}
	  output_hidden_ocl[j] = (1.0 / (1.0 + exp(-sum)));
	}

  float sum1;

  output_hidden_ocl[0] = 1.0;

  for (int j = 1; j <= out; j++)
	{

      sum1 = 0.0;
	  for (int k = 0; k <= hid; k++)
	  {
		sum1 += hwod[k * (out + 1) + j] * output_hidden_ocl[k];
	  }
	  output_hidden_ocl2[j] = (1.0 / (1.0 + exp(-sum1)));
	}


  float o, t;

  for (int j = 1; j <= out; j++)
  {
	o = output_hidden_ocl2[j];
	t = net->target[j];
	output_delta_ocl[j] = o * (1.0 - o) * (t - o);
  }


  float h, sum2;
  for (int j = 1; j <= hid; j++)
  {
	h = output_hidden_ocl[j];
	sum2 = 0.0;
    for (int k = 1; k <= out; k++)
	{
		sum2 += output_delta_ocl[k] * hwod[j * (out + 1) + k];
	}
	net->hidden_delta[j] = h * (1.0 - h) * sum2;
  }

  float new_dw;
	//output_hidden_ocl[0] = 1.0;
  for (int j = 1; j <= out; j++)
   {
	for (int k = 0; k <= hid; k++)
	{
        //new_dw = 0.3 * output_hidden_ocl[j];
		new_dw = ((0.3 * output_delta_ocl[j] * output_hidden_ocl[k]) + (0.3 * hpwod[k * (out + 1) + j]));
		hwod[k * (out + 1) + j] += new_dw;
		hpwod[k * (out + 1) + j] = new_dw;
	}
   }


  float new_dw1;
	//net_input_units[0] = 1.0;

  for (int j = 1; j <= hid; j++)
   {
	for (int k = 0; k <= in; k++)
	{
		new_dw1 = ((0.3 * net->hidden_delta[j] * net->input_units[k]) + (0.3 * ipwod[k * (hid + 1) + j]));
		iwod[k * (hid + 1) + j] += new_dw1;
		ipwod[k * (hid + 1) + j] = new_dw1;
	}
   }

  bool chk = true;
  int m = 0;
  for (int k = 0; k <= in; k++) {
	  for (int j = 0; j <= hid; j++) {
      if (!compareFloat(iwod[m], input_weights_one_dim[m])) chk = false;
		  m++;
    }
  }

  m = 0;
  for (int k = 0; k <= hid; k++) {
	  for (int j = 0; j <= out; j++) {
      if (!compareFloat(hwod[m], hidden_weights_one_dim[m])) chk = false;
		  m++;
	  }
  }

  if(chk == true) printf("VERIFIED\n");
  else            printf("FAILED\n");
}

// Marked static inline to get rid of the function outside of the graph
static inline
void body_1(float *__restrict__ input_weights_one_dim,
            float *__restrict__ net_input_units,
            float *__restrict__ output_hidden_ocl,
            long int hid, long int in, long int j) {
  float sum = 0.0;
  for (int k = 0; k <= in; k++) {
    __hpvm__isNonZeroLoop(k, IN_NUM + 1);
    sum += input_weights_one_dim[k * (hid + 1) + j] * net_input_units[k];
  }
  output_hidden_ocl[j] = 1.0 / (1.0 + exp(-sum));
}

static inline
void body_3(float *__restrict__ hidden_weights_one_dim,
            float *__restrict__ output_hidden_ocl,
            float *__restrict__ output_hidden_ocl2,
            long int out, long int hid, long int j) {
  float sum = 0.0;
  for (int k = 0; k <= hid; k++) {
    __hpvm__isNonZeroLoop(k, HID_NUM + 1);
    sum += hidden_weights_one_dim[k * (out + 1) + j] * output_hidden_ocl[k];
  }
  output_hidden_ocl2[j] = 1.0 / (1.0 + exp(-sum));
}

void body_6(float *__restrict__ output_delta_ocl,
            float *__restrict__ output_hidden_ocl,
            float *__restrict__ hidden_prev_weights_one_dim,
            float *__restrict__ hidden_weights_one_dim,
            long int out, long int hid, long int j, long int k) {
  float new_dw = 0.3 * output_delta_ocl[j] * output_hidden_ocl[k]
               + 0.3 * hidden_prev_weights_one_dim[k * (out + 1) + j];
  hidden_weights_one_dim[k * (out + 1) + j] += new_dw;
  hidden_prev_weights_one_dim[k * (out + 1) + j] = new_dw;
}
      
void body_7(float *__restrict__ net_hidden_delta,
            float *__restrict__ net_input_units,
            float *__restrict__ input_prev_weights_one_dim,
            float *__restrict__ input_weights_one_dim,
            long int hid, long int in, long int j, long int k) {
  float new_dw = 0.3 * net_hidden_delta[j] * net_input_units[k]
               + 0.3 * input_prev_weights_one_dim[k * (hid + 1) + j];
  input_weights_one_dim[k * (hid + 1) + j] += new_dw;
  input_prev_weights_one_dim[k * (hid + 1) + j] = new_dw;
}

void Hpvmroot(float *__restrict__ net_input_units, size_t net_input_units_size,
float *__restrict__ output_hidden_ocl, size_t output_hidden_ocl_size,
float *__restrict__ input_weights_one_dim, size_t input_weights_one_dim_size,
float *__restrict__ output_hidden_ocl2, size_t output_hidden_ocl2_size,
float *__restrict__ hidden_weights_one_dim, size_t hidden_weights_one_dim_size,
float *__restrict__ output_delta_ocl, size_t output_delta_ocl_size,
float *__restrict__ net_target, size_t net_target_size,
float *__restrict__ output_err_ocl, size_t output_err_ocl_size,
float *__restrict__ net_hidden_delta, size_t net_hidden_delta_size,
float *__restrict__ hid_err_ocl, size_t hid_err_ocl_size,
float *__restrict__ hidden_prev_weights_one_dim, size_t hidden_prev_weights_one_dim_size,
float *__restrict__ input_prev_weights_one_dim, size_t input_prev_weights_one_dim_size,
long int in, long int hid, long int out, long int hidp1, long int inp1) {

  auto s = __hpvm_parallel_section_begin();

#ifdef FPGA
  auto fpga_root = __hpvm_task_begin(
                    17, net_input_units, net_input_units_size,
                        output_hidden_ocl, output_hidden_ocl_size,
                        input_weights_one_dim, input_weights_one_dim_size,
                        output_hidden_ocl2, output_hidden_ocl2_size,
                        hidden_weights_one_dim, hidden_weights_one_dim_size,
                        output_delta_ocl, output_delta_ocl_size,
                        net_target, net_target_size,
                        output_err_ocl, output_err_ocl_size,
                        net_hidden_delta, net_hidden_delta_size,
                        hid_err_ocl, hid_err_ocl_size,
                        hidden_prev_weights_one_dim, hidden_prev_weights_one_dim_size,
                        input_prev_weights_one_dim, input_prev_weights_one_dim_size,
                        in, hid, out, hidp1, inp1,
                     2, input_weights_one_dim, input_weights_one_dim_size,
                        hidden_weights_one_dim, hidden_weights_one_dim_size);
  auto fpga_section = __hpvm_parallel_section_begin();
#endif

/*
  auto t0 = __hpvm_task_begin(1, net_input_units, net_input_units_size,
                              1, net_input_units, net_input_units_size);
#ifdef GPU
  auto s0 = __hpvm_parallel_section_begin();
  auto i0 = __hpvm_task_begin(1, net_input_units, net_input_units_size,
                              1, net_input_units, net_input_units_size);
#endif
  {
    __hpvm__hint(hpvm::DEVICE);
    // net_input_units[0] = 1.0;
  }
#ifdef GPU
  __hpvm_task_end(i0);
  __hpvm_parallel_section_end(s0);
#endif
  __hpvm_task_end(t0);
*/

#ifdef GPU
  auto t1 = __hpvm_task_begin(5, input_weights_one_dim, input_weights_one_dim_size,
                                 net_input_units, net_input_units_size,
                                 output_hidden_ocl, output_hidden_ocl_size,
                                 hid,
                                 in,
                              1, output_hidden_ocl, output_hidden_ocl_size);
  auto s1 = __hpvm_parallel_section_begin();
#endif
  for (long int j = 0; j < hid; j++) {
    __hpvm_parallel_loop(1, 5, input_weights_one_dim, input_weights_one_dim_size,
                               net_input_units, net_input_units_size,
                               output_hidden_ocl, output_hidden_ocl_size,
                               hid,
                               in,
                             1, output_hidden_ocl, output_hidden_ocl_size);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(j, HID_NUM);

    body_1(input_weights_one_dim, net_input_units, output_hidden_ocl, hid, in, j+1);
  }
#ifdef GPU
  __hpvm_parallel_section_end(s1);
  __hpvm_task_end(t1);
#endif

/*
  auto t2 = __hpvm_task_begin(1, output_hidden_ocl, output_hidden_ocl_size,
                              1, output_hidden_ocl, output_hidden_ocl_size);
#ifdef GPU
  auto s2 = __hpvm_parallel_section_begin();
  auto i2 = __hpvm_task_begin(1, output_hidden_ocl, output_hidden_ocl_size,
                              1, output_hidden_ocl, output_hidden_ocl_size);
#endif
  {
    __hpvm__hint(hpvm::DEVICE);
	  // output_hidden_ocl[0] = 1.0;
  }
#ifdef GPU
  __hpvm_task_end(i2);
  __hpvm_parallel_section_end(s2);
#endif
  __hpvm_task_end(t2);
*/

#ifdef GPU
  auto t3 = __hpvm_task_begin(5, hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 output_hidden_ocl, output_hidden_ocl_size,
                                 output_hidden_ocl2, output_hidden_ocl2_size,
                                 out,
                                 hid,
                              1, output_hidden_ocl2, output_hidden_ocl2_size);
  auto s3 = __hpvm_parallel_section_begin();
#endif
  for (long int j = 0; j < out; j++) {
    __hpvm_parallel_loop(1, 5, hidden_weights_one_dim, hidden_weights_one_dim_size,
                               output_hidden_ocl, output_hidden_ocl_size,
                               output_hidden_ocl2, output_hidden_ocl2_size,
                               out,
                               hid,
                            1, output_hidden_ocl2, output_hidden_ocl2_size);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(j, OUT_NUM);

    body_3(hidden_weights_one_dim, output_hidden_ocl, output_hidden_ocl2, out, hid, j+1);
  }
#ifdef GPU
  __hpvm_parallel_section_end(s3);
  __hpvm_task_end(t3);
#endif

  auto t4 = __hpvm_task_begin(5, output_hidden_ocl2, output_hidden_ocl2_size,
                                 net_target, net_target_size,
                                 output_delta_ocl, output_delta_ocl_size,
                                 output_err_ocl, output_err_ocl_size,
                                 out,
                              2, output_delta_ocl, output_delta_ocl_size,
                                 output_err_ocl, output_err_ocl_size);
#ifdef GPU
  auto s4 = __hpvm_parallel_section_begin();
  auto i4 = __hpvm_task_begin(5, output_hidden_ocl2, output_hidden_ocl2_size,
                                 net_target, net_target_size,
                                 output_delta_ocl, output_delta_ocl_size,
                                 output_err_ocl, output_err_ocl_size,
                                 out,
                              2, output_delta_ocl, output_delta_ocl_size,
                                 output_err_ocl, output_err_ocl_size);
#endif
  {
    __hpvm__hint(hpvm::DEVICE);
    float errsum = 0.0;
    for (int j = 1; j <= out; j++) {
      __hpvm__isNonZeroLoop(j, OUT_NUM);

      float o = output_hidden_ocl2[j];
      float t = net_target[j];
      output_delta_ocl[j] = o * (1.0 - o) * (t - o);
      errsum += output_delta_ocl[j] > 0.0 ? output_delta_ocl[j] : -output_delta_ocl[j];
    }
    output_err_ocl[0] = errsum;
  }
#ifdef GPU
  __hpvm_task_end(i4);
  __hpvm_parallel_section_end(s4);
#endif
  __hpvm_task_end(t4);

  auto t5 = __hpvm_task_begin(7, output_hidden_ocl, output_hidden_ocl_size,
                                 output_delta_ocl, output_delta_ocl_size,
                                 hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 net_hidden_delta, net_hidden_delta_size,
                                 hid_err_ocl, hid_err_ocl_size,
                                 hid,
                                 out,
                              2, net_hidden_delta, net_hidden_delta_size,
                                 hid_err_ocl, hid_err_ocl_size);
#ifdef GPU
  auto s5 = __hpvm_parallel_section_begin();
  auto i5 = __hpvm_task_begin(7, output_hidden_ocl, output_hidden_ocl_size,
                                 output_delta_ocl, output_delta_ocl_size,
                                 hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 net_hidden_delta, net_hidden_delta_size,
                                 hid_err_ocl, hid_err_ocl_size,
                                 hid,
                                 out,
                              2, net_hidden_delta, net_hidden_delta_size,
                                 hid_err_ocl, hid_err_ocl_size);
#endif
  {
    __hpvm__hint(hpvm::DEVICE);
    float errsum = 0.0;
    for (int j = 1; j <= hid; j++) {
      __hpvm__isNonZeroLoop(j, HID_NUM);

      float h = output_hidden_ocl[j];
      float sum = 0.0;
      for (int k = 1; k <= out; k++) {
        __hpvm__isNonZeroLoop(k, OUT_NUM);

        sum += output_delta_ocl[k] * hidden_weights_one_dim[j * (out + 1) + k];
      }
      net_hidden_delta[j] = h * (1.0 - h) * sum;
      errsum += net_hidden_delta[j] > 0.0 ? net_hidden_delta[j] : -net_hidden_delta[j];
    }
    hid_err_ocl[0] = errsum;
  }
#ifdef GPU
  __hpvm_task_end(i5);
  __hpvm_parallel_section_end(s5);
#endif
  __hpvm_task_end(t5);

#ifdef GPU
  auto t6 = __hpvm_task_begin(7, output_delta_ocl, output_delta_ocl_size,
                                 output_hidden_ocl, output_hidden_ocl_size,
                                 hidden_prev_weights_one_dim, hidden_prev_weights_one_dim_size,
                                 hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 out,
                                 hid, hidp1,
                              2, hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 hidden_prev_weights_one_dim, hidden_prev_weights_one_dim_size);
  auto s6 = __hpvm_parallel_section_begin();
#endif
  for (long int j = 0; j < out; j++) {
    for (long int k = 0; k < hidp1; k++) {
      __hpvm_parallel_loop(2, 7, output_delta_ocl, output_delta_ocl_size,
                                 output_hidden_ocl, output_hidden_ocl_size,
                                 hidden_prev_weights_one_dim, hidden_prev_weights_one_dim_size,
                                 hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 out,
                                 hid, hidp1,
                              2, hidden_weights_one_dim, hidden_weights_one_dim_size,
                                 hidden_prev_weights_one_dim, hidden_prev_weights_one_dim_size);
      __hpvm__hint(hpvm::DEVICE);
      __hpvm__isNonZeroLoop(j, OUT_NUM);
      __hpvm__isNonZeroLoop(k, HID_NUM + 1);

      body_6(output_delta_ocl, output_hidden_ocl, hidden_prev_weights_one_dim,
             hidden_weights_one_dim, out, hid, j+1, k);
    }
  }
#ifdef GPU
  __hpvm_parallel_section_end(s6);
  __hpvm_task_end(t6);
#endif

#ifdef GPU
  auto t7 = __hpvm_task_begin(7, net_hidden_delta, net_hidden_delta_size,
                                 net_input_units,  net_input_units_size,
                                 input_prev_weights_one_dim, input_prev_weights_one_dim_size,
                                 input_weights_one_dim, input_weights_one_dim_size,
                                 hid,
                                 in, inp1,
                              2, input_weights_one_dim, input_weights_one_dim_size,
                                 input_prev_weights_one_dim, input_prev_weights_one_dim_size);
  auto s7 = __hpvm_parallel_section_begin();
#endif
  for (long int j = 0; j < hid; j++) {
    for (long int k = 0; k < inp1; k++) {
      __hpvm_parallel_loop(2, 7, net_hidden_delta, net_hidden_delta_size,
                                 net_input_units,  net_input_units_size,
                                 input_prev_weights_one_dim, input_prev_weights_one_dim_size,
                                 input_weights_one_dim, input_weights_one_dim_size,
                                 hid,
                                 in, inp1,
                              2, input_weights_one_dim, input_weights_one_dim_size,
                                 input_prev_weights_one_dim, input_prev_weights_one_dim_size);
      __hpvm__hint(hpvm::DEVICE);
      __hpvm__isNonZeroLoop(j, HID_NUM);
      __hpvm__isNonZeroLoop(k, IN_NUM + 1);

      body_7(net_hidden_delta, net_input_units, input_prev_weights_one_dim,
             input_weights_one_dim, hid, in, j+1, k);
    }
  }
#ifdef GPU
  __hpvm_parallel_section_end(s7);
  __hpvm_task_end(t7);
#endif

#ifdef FPGA
  __hpvm_parallel_section_end(fpga_section);
  __hpvm_task_end(fpga_root);
#endif

  __hpvm_parallel_section_end(s);
}


int main( int argc, char** argv)
{
  auto mainStart = std::chrono::steady_clock::now();
  int seed = 7;

  //printf("Outp\n");

  if (argc!=2){//changed from 2 arguments to 1 as only have 1 version
      fprintf(stderr, "usage: backprop <num of input elements>\n");
      return -1;
  }

  int layer_size = atoi(argv[1]);
  if (layer_size%16!=0){
      fprintf(stderr, "The number of input points must be divided by 16\n");
      return -1;
  }
  //printf("Random number generator seed: %d\n", seed);
  srand(seed);

  BPNN *net;
  float out_err, hid_err;//for cpu I think

  net = (BPNN *) __hetero_malloc (sizeof (BPNN));
  if (net == NULL) {
    printf("BPNN_CREATE: Couldn't allocate neural network\n");
    return (0);
  }

  net->input_n = layer_size;
  net->hidden_n = 16;
  net->output_n = 1;
  net->input_units = alloc_1d_dbl(layer_size + 1);
  net->hidden_units = alloc_1d_dbl(16 + 1);
  net->output_units = alloc_1d_dbl(2);

  net->hidden_delta = alloc_1d_dbl(16 + 1);
  net->output_delta = alloc_1d_dbl(2);
  net->target = alloc_1d_dbl(2);

  net->input_weights = alloc_2d_dbl(layer_size + 1, 16 + 1);
  net->hidden_weights = alloc_2d_dbl(16 + 1, 2);

  net->input_prev_weights = alloc_2d_dbl(layer_size + 1, 16 + 1);
  net->hidden_prev_weights = alloc_2d_dbl(16 + 1, 2);

  bpnn_randomize_weights(net->input_weights, layer_size, 16);
  bpnn_randomize_weights(net->hidden_weights, 16, 1);
  bpnn_zero_weights(net->input_prev_weights, layer_size, 16);
  bpnn_zero_weights(net->hidden_prev_weights, 16, 1);
  bpnn_randomize_row(net->target, 1);

  printf("Input layer size : %d\n", layer_size);

  float *units;//start of func load; dont know what func does
  int nr, i, k;

  nr = layer_size;
  units = net->input_units;

  k = 1;
  for (i = 0; i < nr; i++) {
  units[k] = (float) rand()/RAND_MAX ;
   k++;
  }//end of func load

  printf("Starting training kernel\n");

  int in, hid, out;

  in = net->input_n;
  hid = net->hidden_n;
  out = net->output_n;

  float *input_weights_one_dim;
  float *input_prev_weights_one_dim;
  float *partial_sum = NULL;//not used
  float *hidden_weights_one_dim = NULL;
  float *hidden_prev_weights_one_dim = NULL;
  float sum;
  int num_blocks = in / 16;

  input_weights_one_dim = (float *) __hetero_malloc((in + 1) * (hid + 1) * sizeof(float));
  input_prev_weights_one_dim = (float *) __hetero_malloc((in + 1) * (hid + 1) * sizeof(float));
  hidden_weights_one_dim = (float *) __hetero_malloc((hid + 1) * (out + 1) * sizeof(float));
  hidden_prev_weights_one_dim = (float *) __hetero_malloc((hid + 1) * (out + 1) * sizeof(float));

  float *iwod;
  float *ipwod;
  float *hwod;
  float *hpwod;
  iwod = (float *) malloc((in + 1) * (hid + 1) * sizeof(float));
  ipwod = (float *) malloc((in + 1) * (hid + 1) * sizeof(float));
  hwod = (float *) malloc((hid + 1) * (out + 1) * sizeof(float));
  hpwod = (float *) malloc((hid + 1) * (out + 1) * sizeof(float));

  int m = 0;
  for (int k = 0; k <= in; k++)
   {
     for (int j = 0; j <= hid; j++)
      {
        input_weights_one_dim[m]      = net->input_weights[k][j];
        iwod[m] = input_weights_one_dim[m];
	    input_prev_weights_one_dim[m] = net->input_prev_weights[k][j];
        ipwod[m] = input_prev_weights_one_dim[m];
	    m++;
      }
   }

  m = 0;
  for (int k = 0; k <= hid; k++)
  {
  	  for (int j = 0; j <= out; j++)
	  {
		  hidden_weights_one_dim[m]      = net->hidden_weights[k][j];
          hwod[m] = hidden_weights_one_dim[m];
		  hidden_prev_weights_one_dim[m] = net->hidden_prev_weights[k][j];
          hpwod[m] = hidden_prev_weights_one_dim[m];
		  m++;
	  }
  }

  float *output_hidden_ocl = (float *)__hetero_malloc((hid + 1) * sizeof(float));
  float *output_hidden_ocl2 = (float *)__hetero_malloc((hid + 1) * sizeof(float));
  float *output_delta_ocl = (float *)__hetero_malloc((out + 1) * sizeof(float));
  float *output_err_ocl = (float *)__hetero_malloc(sizeof(float));
  float *hid_err_ocl = (float *)__hetero_malloc(sizeof(float));

  size_t input_units_size = (in + 1) * sizeof(float);
  size_t output_hidden_ocl_size = (hid + 1) * sizeof(float);
  size_t input_weights_one_dim_size = (in + 1) * (hid + 1) * sizeof(float);
  size_t output_hidden_ocl2_size = (hid + 1) * sizeof(float);
  size_t hidden_weights_one_dim_size = (hid + 1) * (out + 1) * sizeof(float);
  size_t output_delta_ocl_size = (out + 1) * sizeof(float);
  size_t target_size = (out + 1) * sizeof(float);
  size_t output_err_ocl_size = sizeof(float);
  size_t hidden_delta_size = (hid + 1) * sizeof(float);
  size_t hid_err_ocl_size = sizeof(float);
  size_t hidden_prev_weights_one_dim_size = (hid + 1) * (out + 1) * sizeof(float);
  size_t input_prev_weights_one_dim_size = (in + 1) * (hid + 1) * sizeof(float);

  long int hidp1 = hid+1;
  long int inp1 = in+1;
  

  net -> input_units[0] = 1.0;
  output_hidden_ocl[0] = 1.0;

#ifdef REPORT_POWER
#ifdef CPU
  // The GetEnergyCPU() implementation relies on us not switching processors
  // so to fascilitate this we pin ourselves to the current core, this is
  // also needed as it determines the processor we're running on
  int core = sched_getcpu();
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(core, &mask);
  if (sched_setaffinity(0, sizeof(mask), &mask)) {
    perror("Error in sched_setaffinity: ");
    exit(1);
  }

  double start_energy = GetEnergyCPU(core);
#endif
#ifdef GPU
  pthread_barrier_init(&barrier, NULL, 2);

  int flag = 0;
  pthread_t thd;
  pthread_create(&thd, NULL, measure_func, &flag);
  pthread_barrier_wait(&barrier);
#endif
#endif

  auto start_time = std::chrono::steady_clock::now();

  auto launch = __hpvm_launch((void*) Hpvmroot, 17,
                              net->input_units, input_units_size,
                              output_hidden_ocl, output_hidden_ocl_size,
                              input_weights_one_dim, input_weights_one_dim_size,
                              output_hidden_ocl2, output_hidden_ocl2_size,
                              hidden_weights_one_dim, hidden_weights_one_dim_size,
                              output_delta_ocl, output_delta_ocl_size,
                              net->target, target_size,
                              output_err_ocl, output_err_ocl_size,
                              net->hidden_delta, hidden_delta_size,
                              hid_err_ocl, hid_err_ocl_size,
                              hidden_prev_weights_one_dim, hidden_prev_weights_one_dim_size,
                              input_prev_weights_one_dim, input_prev_weights_one_dim_size,
                              in,
                              hid,
                              out,
                              hidp1, inp1,
                              2,
                              input_weights_one_dim, input_weights_one_dim_size,
                              hidden_weights_one_dim, hidden_weights_one_dim_size
                              );

  __hpvm_wait(launch);

  __hetero_request_mem(input_weights_one_dim, input_weights_one_dim_size);
  __hetero_request_mem(hidden_weights_one_dim, hidden_weights_one_dim_size);

  auto endTime = std::chrono::steady_clock::now();
  auto elapsed = endTime - start_time;
  auto totalElapsed = endTime - mainStart;
#ifdef REPORT_POWER
#ifdef CPU
  double end_energy = GetEnergyCPU(core);
  double totalEnergy = end_energy - start_energy;
#endif
#ifdef GPU
  flag = 1;
  pthread_join(thd, NULL);
  double totalPower = measuredPower;
#endif
#endif

  printf("Kernel Execution took %f seconds\n", std::chrono::duration<float>(elapsed).count());
  printf("Total Execution took %f seconds\n", std::chrono::duration<float>(totalElapsed).count());
#ifdef REPORT_POWER
  double totalTime = std::chrono::duration<double>(elapsed).count();
#ifdef CPU
  printf("\nPower usage: %lf W\n", totalEnergy / totalTime);
  printf("Energy usage: %lf J\n", totalEnergy);
#endif
#ifdef GPU
  printf("\nPower usage: %lf W\n", totalPower);
  printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower, totalTime * 1000));
#endif
#endif


  run_bp_cpu(net, iwod, ipwod, hwod, hpwod, input_weights_one_dim, hidden_weights_one_dim, output_hidden_ocl, output_hidden_ocl2, output_delta_ocl);

  free(iwod);
  free(ipwod);
  free(hwod);
  free(hpwod);

  m = 0;
  for (int k = 0; k <= in; k++)
  {
	  for (int j = 0; j <= hid; j++)
	  {
		  net->input_weights[k][j] = input_weights_one_dim[m];
		  m++;
    }
  }

  m = 0;
  for (int k = 0; k <= hid; k++)
  {
	  for (int j = 0; j <= out; j++)
	  {
		  net->hidden_weights[k][j] = hidden_weights_one_dim[m];
		  m++;
	  }
  }

  __hetero_free(output_hidden_ocl);
  __hetero_free(output_hidden_ocl2);
  __hetero_free(output_delta_ocl);
  __hetero_free(output_err_ocl);
  __hetero_free(hid_err_ocl);
  __hetero_free(input_weights_one_dim);
  __hetero_free(input_prev_weights_one_dim);
  __hetero_free(hidden_weights_one_dim);
  __hetero_free(hidden_prev_weights_one_dim);

  int n1, n2, n3;
  float **w;
  FILE *pFile;

  pFile = fopen( "Outputbp.txt", "w+" );
  if (pFile == NULL) {
    printf("Failed to open Outputbp.txt, skipping\n");
  } else {
    n1 = net->input_n;  n2 = net->hidden_n;  n3 = net->output_n;
    printf("Saving %dx%dx%d network to '%s'\n", n1, n2, n3, "Outputbp.txt");

    fprintf(pFile, "%dx%dx%d\n", n1, n2, n3);

    w = net->input_weights;
    for (int i = 0; i <= n1; i++) {
      for (int j = 0; j <= n2; j++) {
        fprintf(pFile, "%.8f ", w[i][j]);
      }
      fprintf(pFile, "\n");
    }
    fprintf(pFile, "\n");

    w = net->hidden_weights;
    for (int i = 0; i <= n2; i++) {
      for (int j = 0; j <= n3; j++) {
        fprintf(pFile, "%.8f ", w[i][j]);
      }
      fprintf(pFile, "\n");
    }

    fclose(pFile);
  }


  __hetero_free((char *) net->input_units);
  __hetero_free((char *) net->hidden_units);
  __hetero_free((char *) net->output_units);

  __hetero_free((char *) net->hidden_delta);
  __hetero_free((char *) net->output_delta);
  __hetero_free((char *) net->target);

  for (i = 0; i <= net->input_n; i++) {
    __hetero_free((char *) net->input_weights[i]);
    __hetero_free((char *) net->input_prev_weights[i]);
  }

  __hetero_free((char *) net->input_weights);
  __hetero_free((char *) net->input_prev_weights);

  for (i = 0; i <= net->hidden_n; i++) {
    __hetero_free((char *) net->hidden_weights[i]);
    __hetero_free((char *) net->hidden_prev_weights[i]);
  }

  __hetero_free((char *) net->hidden_weights);
  __hetero_free((char *) net->hidden_prev_weights);

  __hetero_free((char *) net);

  return 0;
}
