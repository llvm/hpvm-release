# Rodinia: Backpropagation

A HeteroC++ implementation of the Backpropagation (BP) algorithm from the
Rodinia benchmark suite.

## Building
BP can be built using the following command, from this directory, where
`<target>` and `[flags]` are described below.

```sh
make TARGET=<target> [flags]
```

The currently supported `<target>` are `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
BP simulates performing backpropagation on a neural network.  The command to
run it is

```sh
./bp-<version> <num-nodes>
```

where `<version>` is based on the target and flags used to build BFS. The
`<num-nodes>` is the number of inputs nodes and must be divisible by 16.
The program will run both the HPVM and a sequential version and compare their
outputs and write the results of the HPVM version to `Outputbp.txt`.
