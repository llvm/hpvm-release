# Rodinia: Breadth-First Search

A HeteroC++ implementation of the Breadth-First Search (BFS) algorithm from
the Rodinia benchmark suite.

## Building
BFS can be built using the following command, from this directory, where
`<target>` and `[flags]` are described below.

```sh
make TARGET=<target> [flags]
```

The currently supported `<target>` are `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
BFS is run on a graph, for example those found in `/data/bfs/` of the Rodinia
distribution. The command to run BFS is

```sh
./bfs-<version> <input-graph>
```

where `<version>` is based on the target and flags used to build BFS. The
`<input>` is a path to the input graph.

The program will print the number of iterations it took to reach all nodes and
write the distances/cost for each node to the file `bfs_output_device.txt`. It
also executes a sequential version of the algorith, will compare the results,
and outputs the results from this sequential version to `bfs_output_cpu.txt`.
