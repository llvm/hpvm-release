#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>

#include "../../../../include/heterocc.h"

#include <chrono>

#define NODE_COUNT 1000000

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection requires sudo
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE 1
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

struct Node {
  int starting;
  int no_of_edges;
};

void *alignedMalloc(size_t size) {
  void *ptr = NULL;
  if (posix_memalign(&ptr, 64, size)) {
    fprintf(stderr, "Aligned Malloc failed.\n");
    exit(-1);
  }
  return ptr;
}

bool compare_results(int *cost, int *cost_ref, int no_of_nodes) {
  for (int i = 0; i < no_of_nodes; i++)
    if (cost[i] != cost_ref[i])
      return false;

  return true;
}

void write_to_file(char *filename, int *input, int no_of_nodes) {

  FILE *fid;
  int i, j;

  fid = fopen(filename, "w");
  if (fid == NULL) {
    printf("The file, %s, was not created/opened for writing\n", filename);
    return;
  }

  // matrix is saved column major in memory (MATLAB)
  for (i = 0; i < no_of_nodes; i++) {
    fprintf(fid, "%d\n", (int)input[i]);
  }

  fclose(fid);
}

void run_bfs_cpu(int no_of_nodes, Node *graph_nodes, int *graph_edges,
                 char *graph_mask, char *updating_graph_mask,
                 char *graph_visited, int *cost_ref) {
  char stop;

  do {
    stop = false;
    for (int tid = 0; tid < no_of_nodes; tid++) {
      if (graph_mask[tid] == true) {
        graph_mask[tid] = false;
        for (int i = graph_nodes[tid].starting;
             i < (graph_nodes[tid].no_of_edges + graph_nodes[tid].starting);
             i++) {
          int id =
              graph_edges[i]; //--cambine: node id is connected with node tid
          if (!graph_visited[id]) { //--cambine: if node id has not been
                                    //visited, enter the body below
            cost_ref[id] = cost_ref[tid] + 1;
            updating_graph_mask[id] = true;
          }
        }
      }
    }

    for (int tid = 0; tid < no_of_nodes; tid++) {
      if (updating_graph_mask[tid] == true) {
        graph_mask[tid] = true;
        graph_visited[tid] = true;
        stop = true;
        updating_graph_mask[tid] = false;
      }
    }
  } while (stop);
}

void body_1(Node *__restrict__ graph_nodes, int *__restrict__ graph_edges,
            char *__restrict__ graph_mask,
            char *__restrict__ updating_graph_mask,
            char *__restrict__ graph_visited, int *__restrict__ cost,
            long int node) {
  if (graph_mask[node]) {
    graph_mask[node] = false;
    for (int edge = graph_nodes[node].starting;
         edge < graph_nodes[node].no_of_edges + graph_nodes[node].starting;
         edge++) {
      int id = graph_edges[edge];
      if (!graph_visited[id]) {
        cost[id] = cost[node] + 1;
        updating_graph_mask[id] = true;
      }
    }
  }
}

void body_2(char *__restrict__ graph_mask,
            char *__restrict__ updating_graph_mask,
            char *__restrict__ graph_visited, char *__restrict__ over,
            long int node) {
  if (updating_graph_mask[node]) {
    graph_mask[node] = true;
    graph_visited[node] = true;
    *over = true;
    updating_graph_mask[node] = false;
  }
}

void run_bfs(Node *__restrict__ graph_nodes, size_t size_graph_nodes,
             int *__restrict__ graph_edges, size_t size_graph_edges,
             char *__restrict__ graph_mask, size_t size_graph_mask,
             char *__restrict__ updating_graph_mask,
             size_t size_updating_graph_mask, char *__restrict__ graph_visited,
             size_t size_graph_visited, char *__restrict__ over,
             size_t size_over, int *__restrict__ cost, size_t size_cost,
             long int no_of_nodes) {

  auto s = __hpvm_parallel_section_begin();

#ifdef FPGA
  auto fpga_root = __hpvm_task_begin(
      8, graph_nodes, size_graph_nodes, graph_edges, size_graph_edges,
      graph_mask, size_graph_mask, updating_graph_mask,
      size_updating_graph_mask, graph_visited, size_graph_visited, over,
      size_over, cost, size_cost, no_of_nodes, 5, graph_mask, size_graph_mask,
      graph_visited, size_graph_visited, over, size_over, cost, size_cost,
      updating_graph_mask, size_updating_graph_mask);

  auto fpga_section = __hpvm_parallel_section_begin();
#endif

#ifdef GPU
  auto t1 = __hpvm_task_begin(
      7, graph_nodes, size_graph_nodes, graph_edges, size_graph_edges,
      graph_mask, size_graph_mask, updating_graph_mask,
      size_updating_graph_mask, graph_visited, size_graph_visited, cost,
      size_cost, no_of_nodes, 3, graph_mask, size_graph_mask, cost, size_cost,
      updating_graph_mask, size_updating_graph_mask);
  auto s1 = __hpvm_parallel_section_begin();
#endif
  for (long int node = 0; node < no_of_nodes; node++) {
    __hpvm_parallel_loop(
        1, 7, graph_nodes, size_graph_nodes, graph_edges, size_graph_edges,
        graph_mask, size_graph_mask, updating_graph_mask,
        size_updating_graph_mask, graph_visited, size_graph_visited, cost,
        size_cost, no_of_nodes, 3, graph_mask, size_graph_mask, cost, size_cost,
        updating_graph_mask, size_updating_graph_mask);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(node, NODE_COUNT);

    body_1(graph_nodes, graph_edges, graph_mask, updating_graph_mask,
           graph_visited, cost, node);
  }
#ifdef GPU
  __hpvm_parallel_section_end(s1);

  __hpvm_task_end(t1);
#endif

#ifdef GPU
  auto t2 = __hpvm_task_begin(
      5, graph_mask, size_graph_mask, updating_graph_mask,
      size_updating_graph_mask, graph_visited, size_graph_visited, over,
      size_over, no_of_nodes, 4, graph_mask, size_graph_mask, graph_visited,
      size_graph_visited, over, size_over, updating_graph_mask,
      size_updating_graph_mask);

  auto s2 = __hpvm_parallel_section_begin();
#endif
  for (long int node = 0; node < no_of_nodes; node++) {
    __hpvm_parallel_loop(1, 5, graph_mask, size_graph_mask, updating_graph_mask,
                         size_updating_graph_mask, graph_visited,
                         size_graph_visited, over, size_over, no_of_nodes, 4,
                         graph_mask, size_graph_mask, graph_visited,
                         size_graph_visited, over, size_over,
                         updating_graph_mask, size_updating_graph_mask);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(node, NODE_COUNT);

    body_2(graph_mask, updating_graph_mask, graph_visited, over, node);
  }
#ifdef GPU
  __hpvm_parallel_section_end(s2);

  __hpvm_task_end(t2);
#endif
#ifdef FPGA
  __hpvm_parallel_section_end(fpga_section);
  __hpvm_task_end(fpga_root);
#endif
  __hpvm_parallel_section_end(s);
}

void Usage(int argc, char *argv[]) {
  fprintf(stderr, "Usage: %s <input_file>\n", argv[0]);
}

int main(int argc, char *argv[]) {
  auto mainStart = std::chrono::steady_clock::now();

  int no_of_nodes;
  int size_edge_list;
  FILE *fp;

  Node *graph_nodes = NULL;
  char *graph_mask = NULL, *updating_graph_mask = NULL, *graph_visited = NULL;

  char *input_f;
  if (argc != 2) {
    Usage(argc, argv);
    return 0;
  }

  input_f = argv[1];
  printf("\nReading File\n");

  fp = fopen(input_f, "r");
  if (!fp) {
    printf("Error Reading Graph file\n");
    return 0;
  }

  int source = 0;

  if (!fscanf(fp, "%d", &no_of_nodes)) {
    printf("Error in fscanf(&no_of_nodes)\n");
    return 0;
  }

  size_t size_graph_nodes = sizeof(Node) * no_of_nodes;
  size_t size_graph_mask = sizeof(char) * no_of_nodes;
  size_t size_updating_graph_mask = sizeof(char) * no_of_nodes;
  size_t size_graph_visited = sizeof(char) * no_of_nodes;

  graph_nodes = (Node *)__hetero_malloc(size_graph_nodes);
  graph_mask = (char *)__hetero_malloc(size_graph_mask);
  updating_graph_mask = (char *)__hetero_malloc(size_updating_graph_mask);
  graph_visited = (char *)__hetero_malloc(size_graph_visited);

  int start, edgeno;

  for (int i = 0; i < no_of_nodes; i++) {
    if (!fscanf(fp, "%d %d", &start, &edgeno)) {
      printf("Error in fscanf(&start, &edgeno)\n");
      return 0;
    }

    graph_nodes[i].starting = start;
    graph_nodes[i].no_of_edges = edgeno;
    graph_mask[i] = false;
    updating_graph_mask[i] = false;
    graph_visited[i] = false;
  }

  if (!fscanf(fp, "%d", &source)) {
    printf("Error in fscanf(&source)\n");
    return 0;
  }

  source = 0;

  graph_mask[source] = true;
  graph_visited[source] = true;

  if (!fscanf(fp, "%d", &size_edge_list)) {
    printf("Error in fscanf(&size_edge_list)\n");
    return 0;
  }

  int id, cost_temp;

  size_t size_graph_edges = sizeof(int) * size_edge_list;
  int *graph_edges = (int *)__hetero_malloc(size_graph_edges);

  for (int i = 0; i < size_edge_list; i++) {
    if (!fscanf(fp, "%d", &id)) {
      printf("Error in fscanf(&id)\n");
      return 0;
    }
    if (!fscanf(fp, "%d", &cost_temp)) {
      printf("Error in fscanf(&cost)\n");
      return 0;
    }

    graph_edges[i] = id;
  }

  if (fp)
    fclose(fp);

  size_t size_cost = sizeof(int) * no_of_nodes;
  int *cost = (int *)__hetero_malloc(size_cost);
  int *cost_ref = (int *)malloc(sizeof(int) * no_of_nodes);

  for (int i = 0; i < no_of_nodes; i++) {
    cost[i] = -1;
    cost_ref[i] = -1;
  }

  cost[source] = 0;
  cost_ref[source] = 0;

  // Host variable written by BFS_2
  size_t size_over = sizeof(char) * 1;
  char *over = (char *)__hetero_malloc(size_over);

  int k = 0;
  float totalTime = 0.0;
  printf("\nStarting Dynamic Replication version\n");

#ifdef REPORT_POWER
#ifdef CPU
  double totalEnergy = 0.0;
  // The GetEnergyCPU() implementation relies on us not switching processors
  // so to fascilitate this we pin ourselves to the current core, this is
  // also needed as it determines the processor we're running on
  int core = sched_getcpu();
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(core, &mask);
  if (sched_setaffinity(0, sizeof(mask), &mask)) {
    perror("Error in sched_setaffinity: ");
    exit(1);
  }
#endif
#ifdef GPU
  double totalPower = 0.0;
  pthread_barrier_init(&barrier, NULL, 2);
#endif
#endif

  do {
    *over = false;
#ifdef REPORT_POWER
#ifdef CPU
    double start_energy = GetEnergyCPU(core);
#endif
#ifdef GPU
    int flag = 0;
    pthread_t thd;
    pthread_create(&thd, NULL, measure_func, &flag);
    pthread_barrier_wait(&barrier);
#endif
#endif
    auto startTime = std::chrono::steady_clock::now();

    auto launch = __hpvm_launch(
        (void *)run_bfs, 8, graph_nodes, size_graph_nodes, graph_edges,
        size_graph_edges, graph_mask, size_graph_mask, updating_graph_mask,
        size_updating_graph_mask, graph_visited, size_graph_visited, over,
        size_over, cost, size_cost, no_of_nodes, 2, 
        over, size_over, cost, size_cost);

    __hpvm_wait(launch);

    auto elapsed = std::chrono::steady_clock::now() - startTime;
    totalTime += std::chrono::duration<float>(elapsed).count();
#ifdef REPORT_POWER
#ifdef CPU
    double end_energy = GetEnergyCPU(core);
    totalEnergy += end_energy - start_energy;
#endif
#ifdef GPU
    flag = 1;
    pthread_join(thd, NULL);
    totalPower += measuredPower;
#endif
#endif

    k++;
    __hetero_request_mem(over, size_over);

  } while (*over);
  
  auto endTime = std::chrono::steady_clock::now();
  double fullTime = std::chrono::duration<float>(endTime - mainStart).count();

  __hetero_request_mem(cost, size_cost);

  printf("\nKernel was executed %d times\n", k);
  printf("\nKernel Execution took %f seconds\n", totalTime);
  printf("\nTotal Execution took %f seconds\n", fullTime);

#ifdef REPORT_POWER
#ifdef CPU
  printf("\nPower usage: %lf W\n", totalEnergy / totalTime);
  printf("Energy usage: %lf J\n", totalEnergy);
#endif
#ifdef GPU
  printf("\nPower usage: %lf W\n", totalPower / k);
  printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower / k, totalTime * 1000));
#endif
#endif

  // Reinitializing memory
  for (int i = 0; i < no_of_nodes; i++) {
    graph_mask[i] = false;
    updating_graph_mask[i] = false;
    graph_visited[i] = false;
    // printf("%d %d \n", i, cost[i]);
  }

  source = 0;
  graph_mask[source] = true;
  graph_visited[source] = true;
  run_bfs_cpu(no_of_nodes, graph_nodes, graph_edges, graph_mask,
              updating_graph_mask, graph_visited, cost_ref);

  bool ans = compare_results(cost_ref, cost, no_of_nodes);
  if (ans)
    printf("\nVERIFIED\n");
  else
    fprintf(stderr, "\nINCORRECT CALCULATION\n");

  write_to_file("bfs_output_device.txt", cost, no_of_nodes);
  write_to_file("bfs_output_cpu.txt", cost_ref, no_of_nodes);

  __hetero_free(graph_nodes);
  __hetero_free(graph_mask);
  __hetero_free(updating_graph_mask);
  __hetero_free(graph_visited);
  __hetero_free(graph_edges);
  __hetero_free(over);
  __hetero_free(cost);
  free(cost_ref);

  return 0;
}
