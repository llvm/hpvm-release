# Rodinia Benchmarks
In this directory are several Rodinia benchmarks, ported to HeteroC++ in a
hardware agnostic manner.

They all compile for CPU, GPU, and FPGA, and directions for each are provided
in the README in the directory for the benchmark.

Many of these require input files in certain formats. Examples of these files
are distributed with the
[Rodinia benchmark suite](http://www.cs.virginia.edu/rodinia/doku.php).
