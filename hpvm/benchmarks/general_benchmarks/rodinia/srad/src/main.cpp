#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include <heterocc.h>

#include <chrono>

// Wrapping everything for collecting power information in this ifdef since
// running with power-collection requires sudo
#ifdef REPORT_POWER
#ifdef CPU
  #include <power_cpu.h>
  #define _GNU_SOURCE 1
  #include <sched.h>
  #include <stdio.h>
#endif
#ifdef GPU
  #include <power_gpu.h>

  volatile double measuredPower;
  pthread_barrier_t barrier;
  void* measure_func(void* in) {
    volatile int* flag = (volatile int*) in;
    // My understanding of how initContext works is that the it will always
    // pick the first Nvidia device, so we want device ID 0
    const int DeviceID = 0; 
    measuredPower = GetPowerGPU(flag, DeviceID, &barrier);
    return NULL;
  }
#endif
#ifdef FPGA
  #warning "FPGA Power Reporting Should be Done Through Quartus"
#endif
#endif

#define NE_NUM 229916
#define Nr_NUM 502
#define Nc_NUM 458

void resize (float* input,
    int input_rows,
    int input_cols,
    float* output,
    int output_rows,
    int output_cols) {

  int i, j;
  int i2, j2;

  // column major

  for(j = 0, j2 = 0; j < output_cols; j++, j2++){
    if(j2 >= input_cols){
      j2 = j2 - input_cols;
    }
    for(i = 0, i2 = 0; i < output_rows; i++, i2++){
      if(i2 >= input_rows){
        i2 = i2 - input_rows;
      }
      output[j*output_rows+i] = input[j2*input_rows+i2];
    }
  }
}


void read_graphics (char* filename,
        float* input,
        int data_rows,
        int data_cols) {

  FILE* fid;
  int i, j;
  char c;
  int temp;

  fid = fopen(filename, "r");
  if( fid == NULL ){
          printf( "The file, %s,  was not opened for reading\n",
                  filename);
          return;
  }

  i = 0;
  while(i<3){
    c = fgetc(fid);
    if(c == '\n'){
      i = i+1;
    }
  };

  // matrix is saved column major in memory (MATLAB)
  for(i=0; i<data_rows; i++){
    for(j=0; j<data_cols; j++){
      fscanf(fid, "%d", &temp);
      input[j*data_rows+i] = (float)temp;
    }
  }

  fclose(fid);
}


void write_graphics (char* filename,
        float* input,
        int data_rows,
        int data_cols,
        int data_range) {

  FILE* fid;
  int i, j;

  fid = fopen(filename, "w");
  if( fid == NULL ){
          printf( "The file, %s, was not created/opened for writing\n",
                  filename);
          return;
  }

  fprintf(fid, "P2\n");
  fprintf(fid, "%d %d\n", data_cols, data_rows);
  fprintf(fid, "%d\n", data_range);

  // matrix is saved column major in memory (MATLAB)
  for(i=0; i<data_rows; i++){
    for(j=0; j<data_cols; j++){
      fprintf(fid, "%d ", (int)input[j*data_rows+i]);
    }
    fprintf(fid, "\n");
  }

  fclose(fid);

}

void prepare_kernel_cpu(float *image, float *sums, float *sums2, long int Ne) {
  for (long int ei = 0; ei < Ne; ++ei) {
        sums[ei] = image[ei];
        sums2[ei] = image[ei] * image[ei];
  }
}

void reduce_kernel_cpu(float *sums, float *sums2, long int Ne) {
  float sum_1 = 0, sum_2 = 0;
  for (long int i = 0; i < Ne; ++i) {
    sum_1 += sums[i];
    sum_2 += sums2[i];
  }
  sums[0] = sum_1;
  sums2[0] = sum_2;
}

void srad1_kernel_cpu(int *iN, int *iS, int *jE, int *jW, float *dN, float *dS, float *dW, float *dE,
                float *q0sqr, float *c, float *image, float *sums, float *sums2, int Nr, int Nc) {

  float total = *sums;
  float total2 = *sums2;
  float meanROI = (float)(total / (Nr * Nc));
  float meanROI2 = meanROI * meanROI;
  float varROI = (float)(total2 / (Nr * Nc) - meanROI2);
  *q0sqr = varROI/meanROI2;

  for (int col = 0; col < Nc; ++col) {
    for (int row = 0; row < Nr; ++row) {
      int ei = col * Nr + row;

      float Jc;
      float dN_loc, dS_loc, dW_loc, dE_loc;
      float c_loc;
      float G2, L, num, den, qsqr;

      // directional derivatives, ICOV, diffusion coefficient
      Jc = image[ei];                 // get value of the current element

      // directional derivatives (every element of IMAGE)(try to copy to shared memory or temp files)
      dN_loc = image[iN[row] + Nr * col] - Jc;      // north direction derivative
      dS_loc = image[iS[row] + Nr * col] - Jc;      // south direction derivative
      dW_loc = image[row + Nr * jW[col]] - Jc;      // west direction derivative
      dE_loc = image[row + Nr * jE[col]] - Jc;      // east direction derivative

      // normalized discrete gradient mag squared (equ 52,53)
      G2 = (dN_loc * dN_loc + dS_loc * dS_loc + dW_loc * dW_loc + dE_loc * dE_loc) / (Jc * Jc);

      // normalized discrete laplacian (equ 54)
      L = (dN_loc + dS_loc + dW_loc + dE_loc) / Jc; // laplacian (based on derivatives)

      // ICOV (equ 31/35)
      num = (0.5 * G2) - ((1.0 / 16.0) * (L * L));  // num (based on gradient and laplacian)
      den = 1 + (0.25 * L);             // den (based on laplacian)
      qsqr = num / (den * den);           // qsqr (based on num and den)

      // diffusion coefficient (equ 33) (every element of IMAGE)
      den = (qsqr - *q0sqr) / (*q0sqr * (1 + *q0sqr));  // den (based on qsqr and q0sqr)
      c_loc = 1.0 / (1.0 + den);            // diffusion coefficient (based on den)

      // saturate diffusion coefficient to 0-1 range
      if (c_loc < 0)
      {
        c_loc = 0;
      }
      else if (c_loc > 1)
      {
        c_loc = 1;
      }

      // save data to global memory
      dN[ei] = dN_loc;
      dS[ei] = dS_loc;
      dW[ei] = dW_loc;
      dE[ei] = dE_loc;
      c[ei]  = c_loc;
    }
  }
}


void srad2_kernel_cpu (int *iS, int *jE, float *dN, float *dS, float *dW, float *dE, float *c, float *image, int Nr, int Nc, double lambda) {
  for (int col = 0; col < Nc; ++col) {
    for (int row = 0; row < Nr; ++row) {
      int ei = col * Nr + row;

      float cN, cS, cW, cE;
      float D;

      // diffusion coefficient
      cN = c[ei];                 // north diffusion coefficient
      cS = c[iS[row] + Nr*col];           // south diffusion coefficient
      cW = c[ei];                 // west diffusion coefficient
      cE = c[row + Nr * jE[col]];         // east diffusion coefficient

      // divergence (equ 58)
      D = cN * dN[ei] + cS * dS[ei] + cW * dW[ei] + cE * dE[ei];

      // image update (equ 61) (every element of IMAGE)
      image[ei] = image[ei] + 0.25 * (float)lambda * D;
      // updates image (based on input time step and divergence)
    }
  }
}

// ========== Parallel Kernels ==========

static inline
void srad1_body(float *__restrict__ image, int   *__restrict__ iN,
                int   *__restrict__ iS,    int   *__restrict__ jE,
                int   *__restrict__ jW,    float *__restrict__ dN,
                float *__restrict__ dS,    float *__restrict__ dW,
                float *__restrict__ dE,    float *__restrict__ q0sqr,
                float *__restrict__ c, long int Nr, long int Nc,
                long int col, long int row) {
  int ei = col * Nr + row;

  float Jc;
  float dN_loc, dS_loc, dW_loc, dE_loc;
  float c_loc;
  float G2, L, num, den, qsqr;

  // directional derivatives, ICOV, diffusion coefficient
  Jc = image[ei];                 // get value of the current element

  // directional derivatives (every element of IMAGE)(try to copy to shared memory or temp files)
  dN_loc = image[iN[row] + Nr * col] - Jc;      // north direction derivative
  dS_loc = image[iS[row] + Nr * col] - Jc;      // south direction derivative
  dW_loc = image[row + Nr * jW[col]] - Jc;      // west direction derivative
  dE_loc = image[row + Nr * jE[col]] - Jc;      // east direction derivative

  // normalized discrete gradient mag squared (equ 52,53)
  G2 = (dN_loc * dN_loc + dS_loc * dS_loc + dW_loc * dW_loc + dE_loc * dE_loc) / (Jc * Jc);

  // normalized discrete laplacian (equ 54)
  L = (dN_loc + dS_loc + dW_loc + dE_loc) / Jc; // laplacian (based on derivatives)

  // ICOV (equ 31/35)
  num = (0.5 * G2) - ((1.0 / 16.0) * (L * L));  // num (based on gradient and laplacian)
  den = 1 + (0.25 * L);             // den (based on laplacian)
  qsqr = num / (den * den);           // qsqr (based on num and den)

  // diffusion coefficient (equ 33) (every element of IMAGE)
  den = (qsqr - *q0sqr) / (*q0sqr * (1 + *q0sqr));  // den (based on qsqr and q0sqr)
  c_loc = 1.0 / (1.0 + den);            // diffusion coefficient (based on den)

  // saturate diffusion coefficient to 0-1 range
  if (c_loc < 0) c_loc = 0;
  else if (c_loc > 1) c_loc = 1;

  // save data to global memory
  dN[ei] = dN_loc;
  dS[ei] = dS_loc;
  dW[ei] = dW_loc;
  dE[ei] = dE_loc;
  c[ei]  = c_loc;
}

void srad2_body(float *__restrict__ image, int   *__restrict__ iS,
                int   *__restrict__ jE,    float *__restrict__ dN,
                float *__restrict__ dS,    float *__restrict__ dW,
                float *__restrict__ dE,    float *__restrict__ c,
                long int Nr, long int Nc, double lambda,
                long int col, long int row) {
  long ei = col * Nr + row;

  float cN, cS, cW, cE;
  float D;

  // diffusion coefficient
  cN = c[ei];                 // north diffusion coefficient
  cS = c[iS[row] + Nr*col];           // south diffusion coefficient
  cW = c[ei];                 // west diffusion coefficient
  cE = c[row + Nr * jE[col]];         // east diffusion coefficient

  // divergence (equ 58)
  D = cN * dN[ei] + cS * dS[ei] + cW * dW[ei] + cE * dE[ei];

  // image update (equ 61) (every element of IMAGE)
  image[ei] = image[ei] + 0.25 * (float)lambda * D;
  // updates image (based on input time step and divergence)
}

void srad_kernels (float *__restrict__ image, size_t size,
                   int   *__restrict__ iN,    size_t size_iN,
                   int   *__restrict__ iS,    size_t size_iS,
                   int   *__restrict__ jE,    size_t size_jE,
                   int   *__restrict__ jW,    size_t size_jW,
                   float *__restrict__ dN,    size_t size_dN,
                   float *__restrict__ dS,    size_t size_dS,
                   float *__restrict__ dW,    size_t size_dW,
                   float *__restrict__ dE,    size_t size_dE,
                   float *__restrict__ q0sqr, size_t size_q0sqr,
                   float *__restrict__ c,     size_t size_c,
                   float *__restrict__ sums,  size_t size_sums,
                   float *__restrict__ sums2, size_t size_sums2,
                   long int Nr, long int Nc, long int Ne, double lambda) {
  auto s = __hpvm_parallel_section_begin();

#ifdef FPGA
  auto fpga_root = __hpvm_task_begin(
                      17, image, size, iN, size_iN, iS, size_iS, jE, size_jE,
                      jW, size_jW, dN, size_dN, dS, size_dS, dW, size_dW, dE,
                      size_dE, q0sqr, size_q0sqr, c, size_c, sums, size_sums,
                      sums2, size_sums2, Nr, Nc, Ne, lambda, 1, image, size);
  auto fpga_section = __hpvm_parallel_section_begin();
#endif

#ifdef GPU
  auto t1 = __hpvm_task_begin(4, image, size, sums, size_sums, sums2, size_sums2,
                                 Ne,
                              2, sums, size_sums, sums2, size_sums2);
  auto s1 = __hpvm_parallel_section_begin();
#endif
  for (long int ei = 0; ei < Ne; ei++) {
    __hpvm_parallel_loop(1, 4, image, size, sums, size_sums, sums2, size_sums2,
                               Ne,
                            2, sums, size_sums, sums2, size_sums2);
    __hpvm__hint(hpvm::DEVICE);
    __hpvm__isNonZeroLoop(ei, NE_NUM);
    sums[ei]  = image[ei];
    sums2[ei] = image[ei] * image[ei];
  }
#ifdef GPU
  __hpvm_parallel_section_end(s1);
  __hpvm_task_end(t1);
#endif

  // Because parallel reduce kernels are complicated, just making it sequential
  // and with that, pulling the non-loop part of the SRAD1 kernel into here
  auto t2 = __hpvm_task_begin(6, sums, size_sums, sums2, size_sums2, q0sqr, size_q0sqr,
                                 Nr, Nc, Ne,
                              1, q0sqr, size_q0sqr);
#ifdef GPU
  auto s2 = __hpvm_parallel_section_begin();
  auto i2 = __hpvm_task_begin(6, sums, size_sums, sums2, size_sums2,
                                 q0sqr, size_q0sqr, Nr, Nc, Ne,
                              1, q0sqr, size_q0sqr);
#endif
  {
    __hpvm__hint(hpvm::DEVICE);
    float sum_1 = 0, sum_2 = 0;
    for (long int i = 0; i < Ne; i++) {
      __hpvm__isNonZeroLoop(i, NE_NUM);
      sum_1 += sums[i];
      sum_2 += sums2[i];
    }
    
    float total = sum_1;
    float total2 = sum_2;
    float meanROI = (float)(total / (Nr * Nc));
    float meanROI2 = meanROI * meanROI;
    float varROI = (float)(total2 / (Nr * Nc) - meanROI2);
    *q0sqr = varROI/meanROI2;
  }
#ifdef GPU
  __hpvm_task_end(i2);
  __hpvm_parallel_section_end(s2);
#endif
  __hpvm_task_end(t2);

#ifdef GPU
  auto t3 = __hpvm_task_begin(13, image, size, iN, size_iN, iS, size_iS,
                                  jE, size_jE, jW, size_jW, dN, size_dN,
                                  dS, size_dS, dW, size_dW, dE, size_dE,
                                  q0sqr, size_q0sqr, c, size_c, Nr, Nc,
                               5, dN, size_dN, dS, size_dS, dW, size_dW,
                                  dE, size_dE, c, size_c);
  auto s3 = __hpvm_parallel_section_begin();
#endif
  for (long int col = 0; col < Nc; col++) {
    for (long int row = 0; row < Nr; row++) {
      __hpvm_parallel_loop(2, 13, image, size, iN, size_iN, iS, size_iS,
                                  jE, size_jE, jW, size_jW, dN, size_dN,
                                  dS, size_dS, dW, size_dW, dE, size_dE,
                                  q0sqr, size_q0sqr, c, size_c, Nr, Nc,
                               5, dN, size_dN, dS, size_dS, dW, size_dW,
                                  dE, size_dE, c, size_c);
      __hpvm__hint(hpvm::DEVICE);
      __hpvm__isNonZeroLoop(col, Nc_NUM);
      __hpvm__isNonZeroLoop(row, Nr_NUM);

      srad1_body(image, iN, iS, jE, jW, dN, dS, dW, dE, q0sqr, c, Nr, Nc, col, row);
    }
  }
#ifdef GPU
  __hpvm_parallel_section_end(s3);
  __hpvm_task_end(t3);
#endif

#ifdef GPU
  auto t4 = __hpvm_task_begin(11, image, size, iS, size_iS, jE, size_jE,
                                  dN, size_dN, dS, size_dS, dW, size_dW,
                                  dE, size_dE, c, size_c, Nr, Nc, lambda,
                               1, image, size);
  auto s4 = __hpvm_parallel_section_begin();
#endif
  for (long int col = 0; col < Nc; col++) {
    for (long int row = 0; row < Nr; row++) {
      __hpvm_parallel_loop(2, 11, image, size, iS, size_iS, jE, size_jE,
                                  dN, size_dN, dS, size_dS, dW, size_dW,
                                  dE, size_dE, c, size_c, Nr, Nc, lambda,
                               1, image, size);
      __hpvm__hint(hpvm::DEVICE);
      __hpvm__isNonZeroLoop(col, Nc_NUM);
      __hpvm__isNonZeroLoop(row, Nr_NUM);

      srad2_body(image, iS, jE, dN, dS, dW, dE, c, Nr, Nc, lambda, col, row);
    }
  }
#ifdef GPU
  __hpvm_parallel_section_end(s4);
  __hpvm_task_end(t4);
#endif

#ifdef FPGA
  __hpvm_parallel_section_end(fpga_section);
  __hpvm_task_end(fpga_root);
#endif

  __hpvm_parallel_section_end(s);
}

void debugPrintImage(float *image, int Nr, int Nc) {
  for (int col = 0; col < Nc; ++col) {
    for (int row = 0; row < Nr; ++row) {
      printf("%f", image[col * Nr + row]);
      printf(" ");
    }
    printf("\n");
  }
  printf("\n-----------------------------------------------------------\n");
}



int main(int argc, char* argv []) {

  auto mainStart = std::chrono::steady_clock::now();

    float* image_ori; // original input image
  int image_ori_rows;
  int image_ori_cols;
  int image_ori_elem;

    float* image;   // input image
  int Nr,Nc;
  long int Ne;

    int niter;
  double lambda;

    int r1,r2,c1,c2;
  int NeROI;

    int* iN = NULL;
  int* iS = NULL;
  int* jE = NULL;
  int* jW = NULL;

  // counters
  int i;    // image row
  int j;    // image col

  // memory sizes
  int size_i;
    int size_j;

    if(argc != 5)
  {
    printf("\nERROR: wrong number of arguments\n");
    return 0;
  }
  else
  {
    niter = atoi(argv[1]);
    lambda = atof(argv[2]);
    Nr = atoi(argv[3]);           // it is 502 in the original image
    Nc = atoi(argv[4]);           // it is 458 in the original image
  }

  image_ori_rows = 502;
  image_ori_cols = 458;
  image_ori_elem = image_ori_rows * image_ori_cols;

  size_t size_iN, size_iS, size_jW, size_jE, size, size_dN, size_dS, size_dE, size_dW, size_c, size_sums, size_sums2;
  
  image_ori = (float*)malloc(sizeof(float) * image_ori_elem);

  read_graphics((char *)("image.pgm"), image_ori, image_ori_rows, image_ori_cols);
  printf("\nReading image... \n");
  //debugPrintImage(image_ori, image_ori_rows, image_ori_cols);

  Ne = (long int)(Nr * Nc);
  size = sizeof(float) * Ne;
  image = (float*) __hetero_malloc(size);

  resize(image_ori, image_ori_rows, image_ori_cols, image, Nr, Nc);
  printf("\nResizing image...\n");
  //printf("\n%f\n", (float)lambda);
  //debugPrintImage(image, Nr, Nc);

  r1 = 0;
  r2 = Nr - 1;
  c1 = 0;
  c2 = Nc - 1;

  size_iN  = sizeof(int) * Nr;
  size_iS  = sizeof(int) * Nr;
  size_jW = sizeof(int) * Nc;
  size_jE  = sizeof(int) * Nc;

  iN = (int *)__hetero_malloc(size_iN) ;              // north surrounding element
  iS = (int *)__hetero_malloc(size_iS) ;              // south surrounding element
  jW = (int *)__hetero_malloc(size_jW) ;              // west surrounding element
  jE = (int *)__hetero_malloc(size_jE) ;              // east surrounding element

  // N/S/W/E indices of surrounding pixels (every element of IMAGE)
  for (i = 0; i < Nr; i++)
  {
    iN[i] = i - 1;                      // holds index of IMAGE row above
    iS[i] = i + 1;                      // holds index of IMAGE row below
  }
  for (j = 0; j < Nc; j++)
  {
    jW[j] = j - 1;                      // holds index of IMAGE column on the left
    jE[j] = j + 1;                      // holds index of IMAGE column on the right
  }

  // N/S/W/E boundary conditions, fix surrounding indices outside boundary of image
  iN[0]      = 0;                     // changes IMAGE top row index from -1 to 0
  iS[Nr - 1] = Nr-1;                      // changes IMAGE bottom row index from Nr to Nr-1
  jW[0]      = 0;                     // changes IMAGE leftmost column index from -1 to 0
  jE[Nc - 1] = Nc-1;

  size_dN = sizeof(float) * Ne;
  size_dS = sizeof(float) * Ne;
  size_dE = sizeof(float) * Ne;
  size_dW = sizeof(float) * Ne;

  // allocate memory for derivatives
  float *dN = (float*) __hetero_malloc(size_dN);
  float *dS = (float*) __hetero_malloc(size_dS);
  float *dW = (float*) __hetero_malloc(size_dW);
  float *dE = (float*) __hetero_malloc(size_dE);

  size_c = sizeof(float) * Ne;
  size_sums = sizeof(float) * Ne;
  size_sums2 = sizeof(float) * Ne;

  // allocate memory for coefficient
  float *c = (float*) __hetero_malloc(size_c);

  // allocate memory for partial sums
  float* sums = (float*) __hetero_malloc(size_sums);
  float* sums2 = (float*) __hetero_malloc(size_sums2);

  // EXTRACT kernel (in host)
  for (int i = 0; i < Ne; i++)
    image[i] = exp(image[i]/255);

  // local variables
  //float total, total2, meanROI, meanROI2, varROI;
  size_t size_q0sqr = sizeof(float) * 1;
  float *q0sqr = (float*) __hetero_malloc(size_q0sqr);

  float totalTime = 0.0;  

#ifdef REPORT_POWER
#ifdef CPU
  double totalEnergy = 0.0;
  // The GetEnergyCPU() implementation relies on us not switching processors
  // so to fascilitate this we pin ourselves to the current core, this is
  // also needed as it determines the processor we're running on
  int core = sched_getcpu();
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(core, &mask);
  if (sched_setaffinity(0, sizeof(mask), &mask)) {
    perror("Error in sched_setaffinity: ");
    exit(1);
  }
#endif
#ifdef GPU
  double totalPower = 0.0;
  pthread_barrier_init(&barrier, NULL, 2);
#endif
#endif

  for (int iter = 0; iter < niter; iter++) {
#ifdef REPORT_POWER
#ifdef CPU
    double start_energy = GetEnergyCPU(core);
#endif
#ifdef GPU
    int flag = 0;
    pthread_t thd;
    pthread_create(&thd, NULL, measure_func, &flag);
    pthread_barrier_wait(&barrier);
#endif
#endif
    auto startTime = std::chrono::steady_clock::now();
    auto launch = __hpvm_launch((void*) srad_kernels, 17,
              image, size,
              iN, size_iN,
              iS, size_iS,
              jE, size_jE,
              jW, size_jW,
              dN, size_dN,
              dS, size_dS,
              dW, size_dW,
              dE, size_dE,
              q0sqr, size_q0sqr,
              c, size_c,
              sums, size_sums,
              sums2, size_sums2,
              Nr,
              Nc,
              Ne,
              lambda,
              1,
              image, size
            );

    __hpvm_wait(launch);

    auto elapsed = std::chrono::steady_clock::now() - startTime;
        totalTime += std::chrono::duration<float>(elapsed).count();
#ifdef REPORT_POWER
#ifdef CPU
    double end_energy = GetEnergyCPU(core);
    totalEnergy += end_energy - start_energy;
#endif
#ifdef GPU
    flag = 1;
    pthread_join(thd, NULL);
    totalPower += measuredPower;
#endif
#endif
  }

  __hetero_request_mem(image, size);

  auto endTime = std::chrono::steady_clock::now();
  double fullTime = std::chrono::duration<float>(endTime - mainStart).count();

  printf("\nKernel Execution took %f seconds\n", totalTime);
  printf("\nTotal Execution took %f seconds\n", fullTime);
#ifdef REPORT_POWER
#ifdef CPU
  printf("\nPower usage: %lf W\n", totalEnergy / totalTime);
  printf("Energy usage: %lf J\n", totalEnergy);
#endif
#ifdef GPU
  printf("\nPower usage: %lf W\n", totalPower / niter);
  printf("Energy usage: %lf J\n", GetEnergyGPU(totalPower / niter, totalTime * 1000));
#endif
#endif

  // COMPRESS kernel (in host)
  for (int i = 0; i < Ne; i++)
    image[i] = log(image[i]) * 255;

  write_graphics((char *)("image_out_device.pgm"), image, Nr, Nc, 255);

  printf("\nReinitializing and Verifying...\n");

  float *image_cpu = (float*) malloc(size);
  resize(image_ori, image_ori_rows, image_ori_cols, image_cpu, Nr, Nc);

  __hetero_free(sums); sums = (float*) malloc(size_sums);
  __hetero_free(sums2); sums2 = (float*) malloc(size_sums2);
  __hetero_free(dN); dN = (float*) malloc(size_dN);
  __hetero_free(dS); dS = (float*) malloc(size_dS);
  __hetero_free(dW); dW = (float*) malloc(size_dW);
  __hetero_free(dE); dE = (float*) malloc(size_dE);
  __hetero_free(c); c = (float*) malloc(size_c);
  __hetero_free(q0sqr); q0sqr = (float*) malloc(size_q0sqr);

  // EXTRACT kernel (in host)
  for (int i = 0; i < Ne; i++)
    image_cpu[i] = exp(image_cpu[i]/255);

  for (int iter = 0; iter < niter; iter++) {
    prepare_kernel_cpu(image_cpu, sums, sums2, Ne);
    reduce_kernel_cpu(sums, sums2, Ne);
    srad1_kernel_cpu(iN, iS, jE, jW, dN, dS, dW, dE, q0sqr, c, image_cpu, sums, sums2, Nr, Nc);
    srad2_kernel_cpu(iS, jE, dN, dS, dW, dE, c, image_cpu, Nr, Nc, lambda);
  }

  // COMPRESS kernel (in host)
  for (int i = 0; i < Ne; i++)
    image_cpu[i] = log(image_cpu[i]) * 255;

  write_graphics((char *)("image_out_cpu.pgm"), image_cpu, Nr, Nc, 255);

  bool n_verify_flag = false;
  for (int i = 0; i < Ne; i++) {
    if (!(abs(image[i] - image_cpu[i]) < 0.5)) {
      n_verify_flag = true;
      break;
    }
  }

  if (n_verify_flag)
    printf("\nNOT VERIFIED\n");
  else
    printf("\nVERIFIED\n");

  free(dN);
  free(dS);
  free(dE);
  free(dW);
  free(c);
  free(sums);
  free(sums2);
  free(q0sqr);

  printf("\nImage Written \n \n");
  //debugPrintImage(image, Nr, Nc);

  free(image_ori);
  free(image_cpu);
  __hetero_free(image);
  __hetero_free(iN);
  __hetero_free(iS);
  __hetero_free(jW);
  __hetero_free(jE);

  return 0;
}
