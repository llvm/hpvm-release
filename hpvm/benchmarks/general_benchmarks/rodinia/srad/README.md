# Rodinia: Speckle Reducing Antisotropic Diffusion

A HeteroC++ implementation of the Speckle Reducing Antisotropic Diffusion
(SRAD) algorithm from the Rodinia benchmark suite.

## Building
SRAD can be built using the following command, from this directory, where
`<target>` and `[flags]` are described below.

```sh
make TARGET=<target> [flags]
```

The currently supported `<target>` are `seq` (CPU), `gpu` (GPU), and `fpga`
(FPGA).

There are several optional `[flags]` for the make command:
* For the fpga target, you can compile for emulation using `EMULATION=1`
* For CPU and GPU targets, you can enable power usage logging with `POWER=1`

## Running
SRAD is run on a graph, for example those found in `/data/bfs/` of the Rodinia
distribution. The command to run BFS is

```sh
./srad-<version> <iters> <lambda> <rows> <cols>
```

where `<version>` is based on the target and flags used to build BFS. The
`<iters>` is the number of iterations to perform, `<lambda>` is an argument
of the algorithm, and `<rows>` and `<cols>` specifies the size of the resulting
image. To run the algorithm you must have an `image.pgm` file in this directory,
an example file can be found in `/data/srad/` of the Rodinia distribution.

The program will process the `image.pgm` file and output a
`image_out_device.pgm` and `image_out_cpu.pgm` files where the former is
computed by the HPVM version and the later is written by a sequential version
of the algorithm.  It will also report whether the results match, note that the
versions often differ slightly, as a result of floating-point rounding errors.

These `.pgm` files are an image format, they can be opened by `xdg-open` on
many Linux systems.
