# HPVM Benchmarks
This directory contains benchmarks for the HPVM compiler infrastructure.

The `general_benchmarks/` directory contains benchmarks for the HPVM targets.
These programs are written in C/C++ with sections written in HPVM-C or
HeteroC++. These are intended to be compiled to the various HPVM targets (CPU,
GPU, FPGA).  More details on these benchmarks and how to build and run them can
be found in the following
[README](/hpvm/benchmarks/general_benchmarks/README.md).

The `dnn_benchmarks/` directory contains benchmarks for HPVM's tensor
extensions and ApproxHPVM features.
