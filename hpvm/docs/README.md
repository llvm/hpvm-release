# Building docs

We use Sphinx for generating the API and reference documentation.

## Instructions

Install Sphinx needed to build the documentation by entering:

```bash
pip3 install -r requirements.txt
```

Enter `make html` in this (`hpvm/docs`) directory to build the HTML documentation.
The documentation will be generated in `./build/html` subdirectory .

- Launch a webserver in `./build/html`, such as 
  ```bash
  cd ./build/html
  python -m http.server
  ```
  to view the generated HTML documentation, 
