User Guides
==============

This tutorial provides an introduction to HPVM for new users of the HVPM project.
It begins with instructions on how to build HPVM,
then works through examples on programming in HPVM and compiling down to various types of hardware.

Building HPVM
-------------
First, checkout the HPVM repository using:

.. code-block:: shell

  git clone --recursive -b main https://gitlab.engr.illinois.edu/llvm/hpvm-release.git
  cd hpvm/

Next, build hpvm using the provided installer as follows:

.. code-block:: shell

  ./install.sh -j<n> DCMAKE_BUILD_TYPE=Release

This script will download the required LLVM release and build HPVM.
In this, ```<n>``` should be replaced with the number of threads to build with.
Note that HPVM requires nearly 16 GB of RAM to build single-threaded and this
increases (though not linearly) as the number of threads is increased. If you
encounter Out of Memory or similar errors during compilation, try decreasing
the number of threads.

For more details on building HPVM, check out the :doc:`detailed build instructions <../build-hpvm>`.

.. _user-tutorials:

Tutorials
---------
Once you have built HPVM, please refer to the following tutorials for more information on using HPVM.

.. raw:: html

  <div class="sphx-glr-thumbcontainer">

.. only:: html

 .. figure:: /_static/sphinx-gallery-default.png

     :doc:`1-heterocpp`

.. raw:: html

  </div>


.. raw:: html

  <div class="sphx-glr-thumbcontainer">

.. only:: html

 .. figure:: /_static/sphinx-gallery-default.png

     :doc:`2-tensor-ext`

.. raw:: html

  </div>


.. raw:: html

  <div class="sphx-glr-thumbcontainer">

.. only:: html

 .. figure:: /_static/sphinx-gallery-default.png

     :doc:`3-approxtuner`

.. raw:: html

  </div>
  <div class="sphx-glr-clear"/>

.. toctree::
   :hidden:

   /tutorials/1-heterocpp
   /tutorials/2-tensor-ext
   /tutorials/3-approxtuner

For more detailed usages, check out the :doc:`components</developerdocs/components/index>`
and :doc:`developer documentation</developerdocs/index>`.

.. _how-to-guides:

How-to Guides
-------------
.. toctree::
   :titlesonly:
   :glob:

   ../how_to_guides/*
