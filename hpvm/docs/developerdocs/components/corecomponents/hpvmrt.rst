HPVM Runtime System
===================

The HPVM Runtime System handles interfacing with the appropriate device run time based on 
what device is being used. The runtime system introduces unified methods that set up kernel
arguments, copy data between host and device, launch kernels, wait on kernels, etc. These
methods are used by the device back ends (described :doc:`here </developerdocs/components/corecomponents/hpvm-backends>`)
when performing code-generation. Internally, the runtime system interfaces with the
appropriate OpenCL runtime depending what target devices are being used during run time of
a given application.

One feature that the HPVM Runtime introduces is the ability to automatically determine when
data needs to be copied between host and devices, and when copies are not necessary. This
optimizes the execution of a program, making sure that unnecessary copy operations are not 
perfomred. Internally, the Runtime uses a memory tracker to keep track of where the latest
copy of a given memory pointer is residing, and accordingly makes descisions on when the
memory needs to be copied.
