Hetero-C++ Front End
=====================

Overview
^^^^^^^^
Hetero-C++ (denoted hcc) is a parallel dialect for C/C++ which enables describing hierarchical Task-level and Data-level parallelism. Programmers describe units of computations using marker functions which denote the starting and ending point boundaries for computations as well as the input and outputs for each unit of computation.

Users can compile the same Hetero-C++ program to offload computation to CPU, GPU or FPGA by compiling through the HPVM compiler infrastructure.  Internally, Hetero-C++ performs various code transformations to generate the HPVM-C intrinsics which describe the HPVM Dataflow Graph in LLVM Intermediate Representation.





Hetero-C++ Program
^^^^^^^^^^^^^^^^^^^
The Hetero C/C++ programming model expresses parallelism using a hierarchical graphical description consisting of Tasks and Data Parallel Nodes. Tasks denote single instance computations which can be executed potentially in parallel with other Tasks and Data Parallel Nodes. Data Parallel Nodes correspond to Loop nests at the source code level. Up to a 3 level deep loop nest can be marked as a Data Parallel Node where each loop dimension can have a dynamic replication factor (i.e. determined at runtime). Each iteration on this loop iteration space can be executed in parallel.

We will refer to both Task Nodes and Data Parallel Nodes as Computational Nodes from further on. 

Computational nodes form a Directed Acyclic Dataflow Graph based on the explicitly specified inputs and outputs to each computational node. Each node can itself either be a computational node or another nested DAG. Therefore we form a hierarchical Dataflow graph, where nodes can have dynamic replication.  Each computational node can be explicitly marked for a particular heterogenous compute device using the appropriate API. 


To invoke the execution of the DAG, users specify Hetero-C++ host code which simply specifies the instances of the arguments to be passed into the DAG. This **launch code** returns a handle to the DAG and can be used to call a blocking wait call to wait for the completion of the DAG. 

Hetero-C++ transforms the specified Tasks and Data Parallel Loop into HPVM IR Nodes by extracting the body of the nodes and generating appropriate HPVM IR intrinsics and Dataflow edges to form the HPVM DAG. Similarly, the Hetero-C++ host code generates additional HPVM runtime methods which manage the execution of the DAG.

References
^^^^^^^^^^

The Hetero-C++ specification can be viewed at :doc:`/developerdocs/specifications/hetero-c`.

To view a simple Hetero-C++ program through an example, refer to :doc:`/tutorials/1-heterocpp`.

For a more detailed tutorial on Hetero-C++, please refer to :doc:`/how_to_guides/writing-heteroc-program`.




