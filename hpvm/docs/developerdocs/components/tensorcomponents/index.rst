HPVM Tensor Extensions
================================

The following figure illustrates the interaction between the tensor components of HPVM:

.. image:: ../../../_static/approxtuner-workflow.svg

The documentation of these components are listed below,
which explains their role, usage, and other details.

.. toctree::
   :maxdepth: 1
   :glob:

   *
   Predictive Tuner<https://predtuner.readthedocs.io/en/latest/index.html>
