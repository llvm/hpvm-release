#ifndef LEAF_FINDER_H
#define LEAF_FINDER_H

#include <sstream>

#include "SupportHPVM/DFGTreeTraversal.h"

namespace hpvm {

// Collects leaf nodes from a DFG, optionally filtered by a list of node
// function name substrings
class LeafFinder : public dfg2llvm::DFGTreeTraversal {
private:
  std::vector<DFLeafNode *> Leaves;
  hpvm::Target TargetDevice;
  std::vector<std::string> Nodes;

public:
  LeafFinder(Module &_M, BuildDFG &_DFG, hpvm::Target _TargetDevice,
             std::string _Nodes = "")
      : DFGTreeTraversal(_M, _DFG), TargetDevice(_TargetDevice) {
    // Are we interested in all the nodes (_Nodes = "") or a sepcific set of
    // nodes specified in _Nodes
    if (_Nodes.compare("") != 0) {
      std::stringstream s_stream(_Nodes);
      while (s_stream.good()) {
        std::string substr;
        getline(s_stream, substr, ',');
        Nodes.push_back(substr);
      }
    }
  }
  void process(DFInternalNode *N) {}
  void process(DFLeafNode *N) {
    if (!N->isDummyNode()) {
      if (hasNode(N->getName().str()))
        if (hpvmUtils::getPreferredTarget(N->getFuncPointer()) ==
            TargetDevice) {
          Leaves.push_back(N);
        }
    }
  }
  const std::vector<DFLeafNode *> &getLeaves() { return Leaves; }
  bool hasNode(std::string NodeName) {
    if (Nodes.empty())
      return true;
    for (auto name : Nodes) {
      std::string MangledName = name + "_c";
      if (MangledName.compare(NodeName) == 0) {
        return true;
      }
    }
    return false;
  }
};

} // namespace hpvm

#endif
