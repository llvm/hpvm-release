## HPVM Include

This directory contains the C++ header files for the `HPVM`infrastruture. This are divided into the following categories:

- `CoreHPVM`: Consists of the IR definitions and the common Code Generation files used by the `HPVM` backend targets. Additionally, it defines various utilities which can be used within the specific target backend passes.
- `IRCodeGen`: Defines the transformation passes which generate the `HPVM` IR and Dataflow graph representation from `LLVM` bitcode files written using `HPVM-C`.
- `SupportHPVM`: Provides various utilities needed by the various `HPVM` transformation passes including a `DFG` Tree Traversal class.
- `Transforms`: Defines the transformation passes which perform either DFG to DFG transformations, or transformations on `LLVM-IR`.
