//==------- Clear DFG.h - Header file for Pass that clears HPVM DFG --------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the GenHPVM pass responsible for converting HPVM-C to
// HPVM intrinsics. Note that this pass relies on memory-to-register optimiza-
// tion pass to execute before this executes.
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/Module.h"
#include "llvm/Pass.h"

#include "IRCodeGen/BuildDFG.h"

using namespace llvm;

// Utility function for clearing DFG.
void runClearDFG(Module &M, builddfg::BuildDFG &DFG);

namespace {
// ClearDFG - The first implementation.
struct ClearDFG : public ModulePass {
  static char ID; // Pass identification, replacement for typeid
  ClearDFG() : ModulePass(ID) {}

private:
  // Member variables

  // Functions

public:
  bool runOnModule(Module &M);

  void getAnalysisUsage(AnalysisUsage &AU) const { AU.addRequired<builddfg::BuildDFG>(); }
};

} // namespace genhpvm
