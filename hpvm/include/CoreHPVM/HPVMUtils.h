//
//===---- DFG2LLVM.h - Header file for "HPVM Dataflow Graph to Target" ----===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This filed defines utility functions used for target-specific code generation
// for different nodes of dataflow graphs.
//
//===----------------------------------------------------------------------===//

#ifndef HPVM_UTILS_HEADER
#define HPVM_UTILS_HEADER

#include <assert.h>
#include <iostream>

#include "HPVMHint.h"
#include "CoreHPVM/HPVMHint.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/IntrinsicsHPVM.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/Pass.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/ValueMapper.h"

namespace hpvmUtils {
// Helper Functions

bool isHPVMCreateNodeIntrinsic(llvm::Instruction *I);

bool isHPVMCreateNodeCall(llvm::Instruction *I);

bool isHPVMLaunchCall(llvm::Instruction *I);

// Creates a new createNode intrinsic, similar to II but with different
// associated function F instead
llvm::IntrinsicInst *
createIdenticalCreateNodeIntrinsicWithDifferentFunction(llvm::Function *F,
                                                        llvm::IntrinsicInst *II);

// Fix HPVM hints for this function
void fixHintMetadata(llvm::Module &M, llvm::Function *F, llvm::Function *G);

// Assuming that the changed function is a node function, it is only used as a
// first operand of createNode*. It is enough to iterate through all createNode*
// calls in the program.
void replaceNodeFunctionInIR(llvm::Module &M, llvm::Function *F, llvm::Function *G);

// Create new function F' as a copy of old function F with a new signature and
// input VMAP. The following two most used cases are handled by this function.
// 1. When some extra arguments need to be added to this function
//    - Here we can map the old function arguments to
//      new ones
// 2. When each pointer argument needs an additional size argument
//    - Here, in the absence of VMap, we map the arguments in order, skipping
//      over extra pointer arguments.
// The function returns the list of return llvm::Instructions to the caller to fix in
// case the return type is also changed.
llvm::Function *cloneFunction(llvm::Function *F, llvm::FunctionType *newFT,
                               bool isAddingPtrSizeArg,
                               llvm::SmallVectorImpl<llvm::ReturnInst *> *Returns = NULL,
                               std::vector<llvm::Argument *> *Args = NULL);

llvm::Function *cloneFunction(llvm::Function *F, llvm::FunctionType *newFT,
                               bool isAddingPtrSizeArg, llvm::ValueToValueMapTy &VMap,
                               llvm::SmallVectorImpl<llvm::ReturnInst *> *Returns = NULL,
                               std::vector<llvm::Argument *> *Args = NULL);

// Overloaded version of cloneFunction
llvm::Function *cloneFunction(llvm::Function *F, llvm::Function *newF,
                               bool isAddingPtrSizeArg,
                               llvm::SmallVectorImpl<llvm::ReturnInst *> *Returns = NULL);

//------------------- Helper Functions For Handling Hints -------------------//

// Return true if 1st arg (tag) contains 2nd (target)
bool tagIncludesTarget(hpvm::Target Tag, hpvm::Target T);

bool isSingleTargetTag(hpvm::Target T);

// Add the specified target to the given tag
hpvm::Target getUpdatedTag(hpvm::Target Tag, hpvm::Target T);

// This functions add the hint as metadata in hpvm code
void addHint(llvm::Function *F, hpvm::Target T);

// This function removes the hint as metadata in hpvm code
void removeHint(llvm::Function *F, hpvm::Target T);

hpvm::Target getPreferredTarget(llvm::Function *F);

llvm::Function *
makeEmptyFunction(llvm::FunctionType *FTy,
                  llvm::GlobalValue::LinkageTypes Linkage, llvm::Module *M,
                  const llvm::Twine &name);

// Function that writes out a llvm::Module
void writeOutputModule(llvm::Module &M, std::string MName);

std::string defaultKernelsModuleName(llvm::Module &M);
// Recursively find all the calls to inline in a particular function
void getCallsToInline(llvm::Function *F,
                             std::vector<llvm::CallInst *> &CallsToInline);

// Inline all the functions called by a leaf node
void inlineFunctions(llvm::Function *F);

// Method to print a fatal error and exit
void fatalError(const std::string &msg);

// Method to print a warning message
void warning(const std::string &msg);


void clearFunction(llvm::Function *F);

} // namespace hpvmUtils

#endif // HPVM_UTILS_HEADER
