//===---- DFG2LLVM.h - Header file for "HPVM Dataflow Graph to Target" ----===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This defines different classes for traversing Dataflow Graph for code
// generation for different nodes for different targets.
//
//===----------------------------------------------------------------------===//

#ifndef __DFG2LLVM_H__
#define __DFG2LLVM_H__

#include "IRCodeGen/BuildDFG.h"
#include "CoreHPVM/HPVMHint.h"
#include "SupportHPVM/HPVMTimer.h"
#include "CoreHPVM/HPVMUtils.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/Pass.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include <queue>

using namespace llvm;
using namespace builddfg;

#define TIMER(X)                                                               \
  do {                                                                         \
    if (HPVMTimer) {                                                           \
      X;                                                                       \
    }                                                                          \
  } while (0)
#define DECLARE(X)                                                             \
  X = M.getOrInsertFunction(                                                   \
      getMangledName(#X),                                                      \
      runtimeModule->getFunction(getMangledName(#X))->getFunctionType());

namespace dfg2llvm {
// Helper Functions
ConstantInt *getTimerID(Module &, enum hpvm_TimerID);
ConstantInt *getTargetID(Module &M, enum hpvm::Target T);

bool hasAttribute(Function *, unsigned, Attribute::AttrKind);

//  void addArgument(Function*, Type*, const Twine& Name = "");
Function *addArgument(Function *, Type *, const Twine &Name = "");

// DFG2LLVM abstract class implementation
class DFG2LLVM : public ModulePass {
protected:
  DFG2LLVM(char ID) : ModulePass(ID) {}

  // Member variables

  // Functions

public:
  // Pure Virtual Functions
  virtual bool runOnModule(Module &M) = 0;

  // Functions
  void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<BuildDFG>();
    AU.addPreserved<BuildDFG>();
  }
};

// DFG2LLVM_CPU - The first implementation.
struct DFG2LLVM_CPU : public DFG2LLVM {
  static char ID; // Pass identification, replacement for typeid
  DFG2LLVM_CPU() : DFG2LLVM(ID) {}

private:
  // Member variables

  // Functions

public:
  bool runOnModule(Module &M);
};

// Utility function that invokes the HPVM CPU backend.
bool runDFG2LLVM_CPU(Module &M, BuildDFG &DFG);

// Utility function that invokes the HPVM GPU backend.
bool runDFG2LLVM_GPU(Module &M, BuildDFG &DFG, const std::string &KernelModuleName);

// Utility function that invokes the HPVM FPGA backend.
bool runDFG2LLVM_FPGA(Module &M, BuildDFG &DFG, bool EnableTLP = true,
                      bool Emulation = false, bool OnlyHost = false,
                      std::string KMN = "", std::string BFN = "", const char *_Board="a10gx");

// DFG2LLVM_FPGA - The first implementation.
struct DFG2LLVM_FPGA : public DFG2LLVM {
  static char ID; // Pass identification, replacement for typeid
  DFG2LLVM_FPGA() : DFG2LLVM(ID) {}

private:
public:
  bool runOnModule(Module &M);
  StringRef getPassName() const { return StringRef("DFG2LLVM-FPGA"); }
};

// Abstract Visitor for Code generation traversal (tree traversal for now)
class CodeGenTraversal : public DFNodeVisitor {

protected:
  // Member variables
  Module &M;
  BuildDFG &DFG;
  bool HPVMTimer = false;
  std::string TargetName = "None";

  // Map from Old function associated with DFNode to new cloned function with
  // extra index and dimension arguments. This map also serves to find out if
  // we already have an index and dim extended function copy or not (i.e.,
  // "Have we visited this function before?")
  DenseMap<DFNode *, Value *> OutputMap;

  // HPVM Runtime API
  std::unique_ptr<Module> runtimeModule;

  FunctionCallee llvm_hpvm_initializeTimerSet;
  FunctionCallee llvm_hpvm_switchToTimer;
  FunctionCallee llvm_hpvm_printTimerSet;
  FunctionCallee llvm_hpvm_cpu_dstack_push;
  FunctionCallee llvm_hpvm_cpu_dstack_pop;
  GlobalVariable *TimerSet;
  GlobalVariable *GraphIDAddr;
  Instruction *InitCall;
  Instruction *CleanupCall;

  // Functions
  Value *getStringPointer(const Twine &S, Instruction *InsertBefore,
                                 const Twine &Name = "");
  //  void addIdxDimArgs(Function* F);
  Function *addIdxDimArgs(Function *F);
  std::vector<Value *> extractElements(Value *, std::vector<Type *>,
                                              std::vector<std::string>,
                                              Instruction *);
  Argument *getArgumentAt(Function *F, unsigned offset);
  void initTimerAPI();

  // Pure Virtual Functions
  virtual void init() = 0;
  virtual void initRuntimeAPI() = 0;
  virtual void codeGen(DFInternalNode *N) = 0;
  virtual void codeGen(DFLeafNode *N) = 0;

  // Virtual Functions
  virtual void initializeTimerSet(Instruction *);
  virtual void switchToTimer(enum hpvm_TimerID, Instruction *);
  virtual void printTimerSet(Instruction *);

  virtual ~CodeGenTraversal() {}

  Value *addLoop(Instruction *I, Value *limit,
                        const Twine &indexName = "");
  void invokeChild(DFNode *C, Function *F, ValueToValueMapTy &VMap,
                          Instruction *InsertBefore, hpvm::Target Tag);
  Value *getInValueAt(DFNode *Child, unsigned i, Function *ParentF_CPU,
                             Instruction *InsertBefore);
  Argument *getArgumentFromEnd(Function *F, unsigned offset);

  void copyChildIndexCalc(DFNode *C, Function *F_CPU,
                                 ValueToValueMapTy &VMap,
                                 Instruction *InsertBefore);

  std::string getMangledName(std::string Name) {
    // if (!Name.compare("llvm_hpvm_ocl_launch"))
    // return "_Z20llvm_hpvm_ocl_launchPKcS0_N4hpvm6TargetEb";
    // if (!Name.compare("llvm_hpvm_ocl_executeNode"))
    // return "_Z25llvm_hpvm_ocl_executeNodePvjPKmS1_";
    // else
    return Name;
  }

public:
  // Constructor
  CodeGenTraversal(Module &_M, BuildDFG &_DFG) : M(_M), DFG(_DFG) {}

  static bool checkPreferredTarget(DFNode *N, hpvm::Target T);
  static bool preferredTargetIncludes(DFNode *N, hpvm::Target T);
  hpvm::Target getPreferredTarget(DFNode *N);

  virtual void visit(DFInternalNode *N);

  virtual void visit(DFLeafNode *N);
};

} // namespace dfg2llvm

#endif
