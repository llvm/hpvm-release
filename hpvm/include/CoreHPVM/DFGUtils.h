#ifndef HPVM_DFG_UTILS_HEADER
#define HPVM_DFG_UTILS_HEADER

#include "CoreHPVM/HPVMUtils.h"
#include "CoreHPVM/DFGraph.h"

namespace hpvmUtils {

// Deallocates edges
// TODO: Only pass in Node
void removeNodeEdges(llvm::DFNode &Node);

void bindInput(llvm::DFInternalNode *OuterNode, llvm::DFNode *InnerNode,
                      unsigned SourcePos, unsigned DestPos, llvm::Type *InType,
                      bool IsStreaming);

void bindOutput(llvm::DFInternalNode *OuterNode, llvm::DFNode *InnerNode,
                       unsigned SourcePos, unsigned DestPos, llvm::Type *OutType,
                       bool IsStreaming);

llvm::DFInternalNode &makeSkeleton(llvm::DFNode &Node,
                                    const llvm::Twine &SkeletonName);

// Returns the outer node
// Does not set dim limits
llvm::DFInternalNode &nestNode(llvm::DFLeafNode *InnerNode,
                                const llvm::Twine &NesterName);

unsigned getArgCount(llvm::Function *F);

// Threads an individual value from a node's input to a new input in a child
// node
// Returns the value in the child node's function
// Invalidates function pointer of InnerNode
llvm::Argument *threadInput(llvm::DFNode &InnerNode, llvm::Argument &Input,
                                   const llvm::Twine &ArgName);

// Like threadInput, but works on either constants or arguments. Useful to
// not care about specifics.
llvm::Value *threadValue(llvm::DFNode &InnerNode, llvm::Value *V,
                                const llvm::Twine &ValName);

// Inline all the functions called by a leaf node
void inlineFunctions(llvm::DFLeafNode *Leaf);

// Inline all the functions called by multiple leaf nodes
void inlineFunctions(std::vector<llvm::DFLeafNode *> Leaves);

void viewDFGraph(llvm::DFGraph *G);

// Check if there exists a forward path in the graph between this node
// and node N
bool existsForwardPath(llvm::DFNode *SN, llvm::DFNode *DN);


} // namespace hpvmUtils

#endif // HPVM_DFG_UTILS_HEADER
