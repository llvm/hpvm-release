#ifndef HPVM_LOOP_TRANSFORMS_H
#define HPVM_LOOP_TRANSFORMS_H
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/Analysis/DependenceAnalysis.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Pass.h"
#include "llvm/Analysis/ScalarEvolution.h"

namespace hpvm {
// FUnction that removes memcpy intrinsics from a function's body
void lowerFuncMemCpy(llvm::Function *F, const llvm::TargetTransformInfo &TTI);
// Function to retrieve loop guard branch
// Needs to be replaced when moving to LLVM 10.0
llvm::BranchInst *getLoopGuardBranch(llvm::Loop *L, llvm::ScalarEvolution *SE);

// Function that looks for and removes the isNZLoop HPVM intrinsic
void removeNZLoop(llvm::Function *F);

// Function that looks for and removes the loop guard branch by using the
// NZLoop HPVM intrinsic
void removeFuncLoopGuardBranches(llvm::Function *F, bool KeepI = 0);

// Function that gathers the loop tripcounts by using the
// NZLoop HPVM intrinsic
std::map<llvm::BasicBlock *, int> getFuncLoopTripCounts(llvm::Function *F,
                                                        bool KeepI = 0);

// Function that takes a loop guard branch and removes it
void removeLoopGuardBranch(llvm::Loop *L, llvm::BranchInst *BI);

// Unrolls a loop by tripcount
void unrollLoopByCount(llvm::Loop *L, llvm::LoopInfo &LI,
                       llvm::DominatorTree &DT, llvm::ScalarEvolution &SE,
                       llvm::AssumptionCache &AC, const llvm::TargetTransformInfo &TTI,
                       unsigned _TripCount, unsigned _Count,
                       unsigned _TripMultiple = 1, bool _AllowRuntime = 0,
                       bool _AllowExpensiveTripCount = 0, bool _Force = 0,
                       bool _UnrollRemainder = 0, bool _ForgetAllSCEV = 0,
                       bool _PreserveLCSSA = 0);

// Unrolls all the loops in a function
void unrollFuncLoops(llvm::Function *F, unsigned uf, bool pu = false,
                     bool ru = false, bool uo = false);

// Try to fuse loops of a function F
bool fuseFuncLoops(llvm::Function *F, int lvl=0);

struct IntermediateBlock {
  llvm::BasicBlock *BB;
  std::vector<llvm::Instruction *> MemOps;
  bool depL0;
  bool depL1;
  bool moveUp;
  bool moveDown;
  bool fusionPreventing;

  IntermediateBlock(llvm::BasicBlock *_BB = nullptr,
                    std::vector<llvm::Instruction *> _MemOps =
                        std::vector<llvm::Instruction *>())
      : BB(_BB), MemOps(_MemOps), depL0(false), depL1(false), moveUp(false),
        moveDown(false), fusionPreventing(false) {}
};

struct Dep {
  llvm::Instruction *Src;
  llvm::Instruction *Dst;
  bool isLoopIndependent;
  enum DType { Output, Flow, Anti, Unknown };
  enum Direction {L0ToL1, L1ToL0};
  DType type;
  Direction Dir;

  Dep(llvm::Instruction *_Src, llvm::Instruction *_Dst,
         bool _isLoopIndependent, DType _type, Direction _Dir)
      : Src(_Src), Dst(_Dst), isLoopIndependent(_isLoopIndependent),
        type(_type), Dir(_Dir) {}
  std::string getTypeAsString() {
    switch (type) {
    case Output:
      return "Out";
      break;
    case Flow:
      return "Flow";
      break;
    case Anti:
      return "Anti";
      break;
    case Unknown:
      return "Unknown";
      break;
    }
  }
  std::string getDirAsString() {
    switch (Dir) {
      case L0ToL1:
      return "L0 To L1";
      break;
      case L1ToL0:
      return "L1 To L0";
      break;
    }
  }
  void dump() {
    llvm::errs() << "\tSource: " << *Src << "\n";
    llvm::errs() << "\tDestination: " << *Dst << "\n";
    llvm::errs() << "\tDirection: " << getDirAsString() << "\n";
    llvm::errs() << "\tType: " << getTypeAsString() << "\n";
    llvm::errs() << "\tIs Loop Independent: "
                 << (isLoopIndependent ? "Yes" : "No") << "\n";
  }
};

class FusionCandidate {
public:
  llvm::Loop *L;
  llvm::BasicBlock *Exit;
  llvm::BasicBlock *Exiting;
  llvm::BasicBlock *Header;
  llvm::BasicBlock *Latch;
  llvm::BasicBlock *Preheader;
  std::vector<llvm::Instruction *> MemOps;

  FusionCandidate(llvm::Loop *_L) : L(_L) {
    Exit = L->getExitBlock();
    Exiting = L->getExitingBlock();
    Header = L->getHeader();
    Latch = L->getLoopLatch();
    Preheader = L->getLoopPreheader();
    std::vector<llvm::Instruction *> LMemOps;
    for (auto BB : L->getBlocks()) {
      for (auto &I : *BB) {
        if (llvm::isa<llvm::LoadInst>(I) || llvm::isa<llvm::StoreInst>(I))
          LMemOps.push_back(&I);
      }
    }

    MemOps = LMemOps;
  }

  void print() {
    llvm::errs() << "Printing Fusion Candidate\n";
    llvm::errs() << "* L: " << *L;
    llvm::errs() << "* Exit: " << Exit->getName() << "\n";
    llvm::errs() << "* Exiting: " << Exiting->getName() << "\n";
    llvm::errs() << "* Header: " << Header->getName() << "\n";
    llvm::errs() << "* Latch: " << Latch->getName() << "\n";
    llvm::errs() << "* Preheader: " << Preheader->getName() << "\n";
    llvm::errs() << "* MemOps:\n";
    for (llvm::Instruction *I : MemOps) {
      llvm::errs() << "  - " << *I << "\n";
    }
  }

  void update(llvm::Loop *_L);

  // check SSA Deps between two Fusion Candidates
  bool checkSSADependences(FusionCandidate *FC1);

  // check intermediate blocks between fusion candidates
  bool checkIntermediateBlocks(FusionCandidate *FC1,
                               std::vector<llvm::BasicBlock *> &moveUpBB,
                               std::vector<llvm::BasicBlock *> &moveDownBB,
                               int &numIBBs, llvm::Function *F);
  // Function that returns a vector with the BBs that are
  // dominated by L0 and dominate L1
  std::vector<IntermediateBlock *>
  getIntermediateBlocks(FusionCandidate *FC1, llvm::Function *F,
                        llvm::DominatorTree *DT, llvm::PostDominatorTree *PDT);

  void checkIBBDependences(FusionCandidate *FC1,
                           std::vector<IntermediateBlock *> &intermediateBlocks,
                           llvm::DependenceInfo *DI);

  bool moveIBBs(FusionCandidate *FC1, std::vector<llvm::BasicBlock *> &moveUp,
                std::vector<llvm::BasicBlock *> &moveDown,
                llvm::BasicBlock *&L0Preheader, int numIntermediateBlocks);
  llvm::Value *changeL0LatchTerminator(FusionCandidate *FC1);
  void changeL1LatchTerminator(FusionCandidate *FC1, llvm::Value *Condition);
  void movePhis(FusionCandidate *FC1, llvm::BasicBlock *L0Preheader,
                llvm::ScalarEvolution *SE);
  void moveBlocks(FusionCandidate *FC1,
                  const std::vector<llvm::BasicBlock *> &BBL1);
};

llvm::BasicBlock *getBBbyName(llvm::StringRef Name, llvm::Function *F);
} // namespace hpvm
#endif
