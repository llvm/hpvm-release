#ifndef HPVM_BUFFERING_PRIVATIZATION_H
#define HPVM_BUFFERING_PRIVATIZATION_H

#include "llvm/IR/Function.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/AssumptionCache.h"
#include "llvm/IR/Dominators.h"
#include "CoreHPVM/DFGraph.h"

namespace hpvm {
// Function that buffers Argument and replaces uses with new local variable
void buffer_in(llvm::Value *Arg, llvm::Value *Arg_sz, llvm::Function *F);
// Function that goes through all arguments of an HPVM node function, and buffers
// the ones that are read only and have a constant size with # elements < threshold
void bufferNodeInputs(llvm::DFNode *N, const llvm::DataLayout &DL, unsigned MaxIBSize);
// Function that finds the array size in the host code
llvm::Value *findArraySize(int argnum, llvm::DFNode *N);
// Function that promotes a pointer arg to a private variable
void promote_priv(llvm::Value *Arg, llvm::Value *Arg_sz, llvm::Function *F);
// Function that privatizes all the arguments of Node N
void privatizeNodeArguments(llvm::DFNode *N);
// Function that checks if argument users are all Loads (used for BufferIn).
bool checkArgUsers(llvm::Value *Arg);
} // namespace hpvm
#endif
