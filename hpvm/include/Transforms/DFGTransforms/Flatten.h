//==------- Flatten.h - Header file for HPVM strip mining ------------------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that flattens a set of nested nodes, by first
// dereplicating the leaf node, then generating a loop in the leaf node
// corresponding the internal node's replicating factor. Finally, the leaf
// node is moved out of the internal node..
//
//===----------------------------------------------------------------------===//

#ifndef FLATTEN_H
#define FLATTEN_H

#include "llvm/IR/Constant.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/IRBuilder.h"

#include "Transforms/DFGTransforms/DFGTransform.h"
#include "Transforms/DFGTransforms/DFGTransformUtils.h"
#include "CoreHPVM/DFGraph.h"
#include "CoreHPVM/HPVMUtils.h"

#include <stddef.h>

namespace hpvm {

class NodeFlattener final : public DFGTransform {
public:
  NodeFlattener(DFGTransform::Context &C, DFInternalNode *InternalNode)
      : DFGTransform(C), TargetNode(InternalNode) {}

  bool canRun() const override;

  // Invalidates this transform (don't run multiple times)
  void run() override;

private:
  // member variables
  DFInternalNode *TargetNode;
  Function *NodeFunc;
  std::vector<PHINode *> IndexVars;
  std::vector<Value *> DimLimitsChild, DimLimitsTarget;
  DFLeafNode *ChildNode;
  Function *ChildNodeFunc;

  // member functions
  void replaceQueryIntrinsics(unsigned);
  void replaceCreateNode();
  bool replaceGetNodeInstanceID(IntrinsicInst *II, unsigned dim, unsigned);
  bool replaceGetNumNodeInstances(IntrinsicInst *II, unsigned dim, unsigned);
  bool updateNonConstantDims(std::vector<Argument *> &NewArgs,
                             std::vector<Type *> &NewArgTypes,
                             std::map<unsigned, unsigned> &DimLimitArgMap,
                             std::map<unsigned, unsigned> &DimToArgMap);
};
} // namespace hpvm

#endif // #ifndef STRIP_MINE_HEADER
