//==------------ Tile.h - Header file for HPVM tiling ----------------------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that performs tiling on an HPVM DFG node.
//
//===----------------------------------------------------------------------===//

#ifndef TILE_HEADER
#define TILE_HEADER

#include <array>
#include <stddef.h>

#include "Transforms/DFGTransforms/DFGTransform.h"
#include "llvm/ADT/DenseMap.h"

namespace llvm {
  class Value;

  class DFLeafNode;
}

namespace hpvm {

// Performs tiling, or strip mining if the leaf node is 1-dimesional
class Tile final : public DFGTransform {
public:
  // Type of values used to specify tiling. Indices are the same as in the
  // dimensions vector of leaf nodes.
  using BlockSizesType = llvm::DenseMap<unsigned, unsigned>;

  // Accepts a map from dimension to block width
  Tile(DFGTransform::Context& C, DFLeafNode &TargetNode,
    const BlockSizesType& BlockSizes)
    : DFGTransform(C)
    , TargetNode(TargetNode)
    , BlockSizes(BlockSizes)
  {
  }

  Tile(DFGTransform::Context& C, DFLeafNode &TargetNode,
    unsigned BlockSize)
    : DFGTransform(C)
    , TargetNode(TargetNode)
    , BlockSizes(1)
  {
    BlockSizes[0] = BlockSize;
  }

  bool canRun() const override;

  // Invalidates this transform (don't run multiple times)
  // References to TargetNode remain valid (the original leaf node is moved to
  // its new location in the graph)
  void run() override;
private:
  DFLeafNode &TargetNode;
  BlockSizesType BlockSizes;
};

} // namespace hpvm

#endif // #ifndef TILE_HEADER
