//==------- DFGTransform.h - Header file for HPVM DFG transformations ------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the base class for transformations on the HPVM DFG, which
// are designed to be composable for purposes of search-based optimization.
//
//===----------------------------------------------------------------------===//

#ifndef DFG_TRANSFORM_HEADER
#define DFG_TRANSFORM_HEADER

namespace llvm {
  class LLVMContext;
  class Module;
}

namespace hpvm {
using namespace llvm;

// Class for modular DFG transformations, representing a single modification.
// Logically, it takes a valid DFG and produces a new valid DFG. For purposes
// of optimization, a transformation should preserve program semantics.
// The actual behavior is to modify a DFG in-place, with any parameters passed
// in constructors.
class DFGTransform {
public:
  class Context {
  public:
    Context(Module &_M, LLVMContext &_C) : M(_M), C(_C) {}

    Module& getModule() { return M; }
    const Module& getModule() const { return M; }
    LLVMContext& getLLVMContext() { return C; }
    const LLVMContext& getLLVMContext() const { return C; }
  private:
    Module &M;
		LLVMContext &C;
  };

  DFGTransform(Context &C) : C(C) {}
  virtual ~DFGTransform() = default;

  // Note: If there is trouble with non-const accessors, may need to change
  // signature to allow non-const.
  virtual bool canRun() const = 0;

  // Modify target DFGs, which should remain valid after modification. Will
  // typically invalidate the transform since the graph may be modified.
  // Should not assert if canRun succeeds.
  virtual void run() = 0;

  // Runs if canRun() returns true, and returns whether it ran.
  bool checkAndRun() {
    if (!canRun()) return false;
    run();
    return true;
  }
protected:
  Context &C;
};

} // namespace hpvm

#endif // #ifndef DFG_TRANSFORM_HEADER
