//==------- DFGTransformUtils.h - Header File for Utility FUnctions --------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides utility functions to support the DFGTransform classes.
//
//===----------------------------------------------------------------------===//
#ifndef DFG_TRANSFORM_UTILS_H
#define DFG_TRANSFORM_UTILS_H

#include <vector>

#include "llvm/IR/GlobalValue.h"
#include "Transforms/DFGTransforms/DFGTransform.h"
#include "llvm/Transforms/Utils/ValueMapper.h"

namespace llvm {
class Argument;
class Function;
class FunctionTypes;
class Instruction;
class PHINode;
class Twine;
class Type;
class Value;

template <typename> class Optional;

class DFEdge;
class DFInternalNode;
class DFLeafNode;
class DFNode;
} // namespace llvm

namespace hpvm {

// Creates a loop around the body of Function F. Name is used for loop BBs
// and index variable.
llvm::PHINode *createLoopFromFunction(llvm::Function *F, llvm::Instruction *RI,
                                      llvm::Value *Limit, int Dim,
                                      bool InsertIVDEP);

// If F has a unique ReturnInst, returns it, otherwise returns None.
llvm::Optional<llvm::Instruction *>
getUniqueReturnInstruction(llvm::Function *F);

// Adds a new incoming Edge from entry node to Node and inserts a bindIn
// intrinsic  if addBind is true
DFEdge *addInputEdgeAndBind(DFNode *Node, unsigned SrcPos, unsigned DstPos,
                            Type *ArgType, bool isStreaming,
                            DFGTransform::Context &C, bool addBind);

// Adds a new incoming Edge from Node to exit node and inserts a bindOut
// intrinsic  if addBind is true
DFEdge *addOutputEdgeAndBind(DFNode *Node, unsigned SrcPos, unsigned DstPos,
                             Type *ArgType, bool isStreaming,
                             DFGTransform::Context &C, bool addBind);

// Clones the function and while adding NewArgs from input vector to existing
// arguments
Function *updateFuncWithNewArgs(Function *F, std::vector<Argument *> &NewArgs,
                                std::vector<Type *> &NewArgTypes, llvm::ValueToValueMapTy &VMap);

// Function that replaces a perfectly nested parent/child node with only the
// child node
void replaceParentWithChild(DFNode *N);

// Function that takes a Node and changes its demension to 1 with replciation
// factor of 1
void changeDimToOne(DFNode *N);

llvm::Function *makeEmptyFunction(llvm::FunctionType *FTy,
                                  llvm::GlobalValue::LinkageTypes Linkage,
                                  llvm::Module *M, const llvm::Twine &name);

// checks if a node has all dims = 1
bool isNodeSingular(DFNode *N);

} // namespace hpvm

#endif // #ifndef DFG_TRANSFORM_UTILS_H
