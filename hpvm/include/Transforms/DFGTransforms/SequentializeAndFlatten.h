//===-------------------------- SequentializeLeafPass.h -------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===-----------------------------------------------------------------------===//
//
// This pass tests the sequentialize leaf graph transformation
// (SequentializeLeaf.cpp) by applying it to all leaf nodes.
//
//===-----------------------------------------------------------------------===//
#include "llvm/Support/GraphWriter.h"
#include "Transforms/DFGTransforms/SequentializeLeaf.h"
#include "Transforms/DFGTransforms/Flatten.h"
#include "SupportHPVM/DFGTreeTraversal.h"

#include <unordered_map>

#include "IRCodeGen/BuildDFG.h"
#include "CoreHPVM/DFGraph.h"

#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
using namespace llvm;
using namespace builddfg;
using namespace dfg2llvm;
namespace hpvm {
class SFTraversal : public DFGTreeTraversal {
public:
  SFTraversal(Module &_M, BuildDFG &_DFG) : DFGTreeTraversal(_M, _DFG) {
    Change = 0;
  }
  void process(DFInternalNode *N);
  void process(DFLeafNode *N);
  static bool Change;
};
class SequentializeAndFlatten : public ModulePass {
public:
  SequentializeAndFlatten() : ModulePass(ID) {}
  bool runOnModule(Module &M) override;
  void getAnalysisUsage(AnalysisUsage &Info) const override {
    Info.addRequired<builddfg::BuildDFG>();
    Info.addPreserved<builddfg::BuildDFG>();
  }
  StringRef getPassName() const override {
    return "Sequentialize HPVM Leaf Node";
  }
  static char ID;

private:
  void applyLeafSequentializer(DFInternalNode *Root);
  friend class SFTraversal;
};

bool runSequentializeAndFlatten(Module &M, BuildDFG &DFG);
} // namespace hpvm
