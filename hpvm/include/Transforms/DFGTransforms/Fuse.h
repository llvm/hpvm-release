//===-------------- Fuse.h - Header file for HPVM fusion ------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that performs fusion on HPVM DFG nodes.
//
//===----------------------------------------------------------------------===//

#ifndef HPVM_DFGTRANSFORMS_FUSION_H
#define HPVM_DFGTRANSFORMS_FUSION_H

#include <memory>
#include <vector>

// For ValueToValueMapTy
#include "llvm/Transforms/Utils/ValueMapper.h"

#include "Transforms/DFGTransforms/DFGTransform.h"

namespace llvm {
  class DFLeafNode;
  class DFNode;
}

namespace hpvm {

// Fuses the nodes given as a parameter together, if possible
// Deletes the fused functions on deconstruction
class Fuse final : public DFGTransform 
{
public:
  Fuse(DFGTransform::Context &C, std::vector<DFLeafNode*> Nodes)
    : DFGTransform(C)
    , Nodes(std::move(Nodes))
  {
  }

  ~Fuse(); // Has to be defined in Fuse.cpp since ValueToValueMapTy is incomplete

  bool canRun() const override;
  void run() override;

  std::unique_ptr<ValueToValueMapTy>&& getResVmap() 
  {
    return std::move(ResultVMap);
  }

  DFLeafNode *getNewNode() const { return NewNode; }
  
private:
  std::vector<DFLeafNode*> Nodes;
  std::unique_ptr<ValueToValueMapTy> ResultVMap;
  DFLeafNode *NewNode = nullptr;
  std::vector<llvm::Function *> FunsToDelete;
};

}

#endif // #ifndef HPVM_DFGTRANSFORMS_FUSION_H
