//==------- DereplicateLeaf.h - Header file for HPVM strip mining ----------==//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Provides a DFGTransform that de-replicates a leaf node by transforming
// the dynamic replication factor into a loop in the node.
//
//===----------------------------------------------------------------------===//

#ifndef LEAF_DEREPLICATOR_H
#define LEAF_DEREPLICATOR_H

#include "llvm/IR/Constant.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/IRBuilder.h"

#include "Transforms/DFGTransforms/DFGTransform.h"
#include "Transforms/DFGTransforms/DFGTransformUtils.h"
#include "CoreHPVM/DFGraph.h"
#include "CoreHPVM/HPVMUtils.h"

#include <stddef.h>

namespace hpvm {

class LeafSequentializer final : public DFGTransform {
public:
  LeafSequentializer(DFGTransform::Context &_C, DFLeafNode *LeafNode, ValueToValueMapTy &_VMap,
                     bool _InsertIVDEP = false, bool ModifyParent = true)
    : DFGTransform(_C), TargetNode(LeafNode), InsertIVDEP(_InsertIVDEP), VMap(_VMap), NeedToClone(false), OldNodeFunc(NULL), ModifyParent(ModifyParent) {}

  ~LeafSequentializer() 
  {
    cleanup();
  }

  bool canRun() const override;

  // Invalidates this transform (don't run multiple times)
  void run() override;

  bool hasCloned() {return NeedToClone;}
  void cleanup() {
    if (OldNodeFunc) {
      OldNodeFunc->eraseFromParent();
      OldNodeFunc = nullptr;
    }
  }

private:
  // member variables
  DFLeafNode *TargetNode;
  Function *NodeFunc;
  Function *OldNodeFunc;
  bool InsertIVDEP;
  bool ModifyParent;
  std::vector<PHINode *> IndVars;
  std::vector<Value *> DimLimits;
  ValueToValueMapTy &VMap;
  bool NeedToClone;
  // member functions
  //  void changeDimToOne();
  void replaceCreateNode();
  void replaceQueryIntrinsics(unsigned);
  bool replaceGetNodeInstanceID(IntrinsicInst *II, unsigned);
  bool replaceGetNumNodeInstances(IntrinsicInst *II, unsigned);
  bool updateNonConstantDims(std::vector<Argument *> &NewArgs,
                             std::vector<Type *> &NewArgTypes,
                             std::map<unsigned, unsigned> &DimLimitArgMap,
                             std::map<unsigned, unsigned> &DimToArgMap);
  void moveNZLoop();

};
} // namespace hpvm

#endif // #ifndef STRIP_MINE_HEADER
